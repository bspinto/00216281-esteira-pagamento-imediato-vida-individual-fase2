CREATE PROCEDURE dbo.SEGS14965_SPS (
	@proposta_id INT
	,@indeferido INT OUTPUT
	)
AS
/*
	Nome_do_desenvolvedor (nova consultoria) - 20/07/2020
	Demanda: 00216281-esteira-pagamento-imediato-vida-individual-fase2
	Descri��o: (Procedure para verificar indeferimento autom�tico - vida fase 2)
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14965_SPS  @USUARIO = 'TESTE' ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

	-- (fim) Declara��o e tratamento de variaveis 
	-----------
	BEGIN TRY
		-- Bloco de cria��o de tabelas temporarias (inicio)
		SET @indeferido = 0

		IF EXISTS (
				SELECT TOP 1 1
				FROM seguros_db.dbo.sinistro_parametro_indeferimento_automatico_tb a WITH (NOLOCK)
				WHERE a.proposta_id = @proposta_id
				)
		BEGIN
			SET @indeferido = 1
		END

		SELECT @indeferido

		-- (fim) Bloco de codifica��o da procedure
		-----------				
		SET NOCOUNT OFF

		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO


