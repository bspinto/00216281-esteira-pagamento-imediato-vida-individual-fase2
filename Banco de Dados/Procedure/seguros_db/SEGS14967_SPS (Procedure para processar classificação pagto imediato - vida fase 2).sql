CREATE PROCEDURE dbo.SEGS14967_SPS (@USUARIO VARCHAR(20))
AS
/*
	(nova consultoria) - 09/07/2020
	Demanda: 00216281-esteira-pagamento-imediato-vida-individual-fase2
	Descri��o: (Procedure para processar classifica��o pagto imediato - vida fase 2)
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
    IF OBJECT_ID('tempdb..#classificacao_tmp') IS NOT NULL 
    BEGIN 
      DROP TABLE #classificacao_tmp 
    END
    
   CREATE TABLE #classificacao_tmp ( ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL
    ,indice INT
    ,situacao INT
    ,proposta_id INT
    ,evento_sinistro_id INT
    ,tp_cobertura_id INT
    ,produto_id INT
    ,ramo_id INT
    ,dt_ocorrencia smalldatetime
    ,evento_id INT
    ,sinistro_id NUMERIC(11)
    ,fluxo_validado INT
    ,pagamento_imediato INT
    ,deferido INT
    ,fase INT
    ,vigencia_dias INT
    ,regra  INT
    ,tp_componente_id  INT
    ,sub_grupo_id  INT
    ,val_is NUMERIC(15,2)
    --,proposta_bb INT
    ,dt_inicio_vigencia SMALLDATETIME
    ,dt_fim_vigencia SMALLDATETIME
    ,exibir_popup INT
    ,tp_sinistro_parametro_id INT
    --,sinistrado_cliente_id INT
    ,log_classificacao VARCHAR(MAX)
    ) 
    
    INSERT INTO #classificacao_tmp (indice
      ,situacao 
      ,proposta_id 
      ,evento_sinistro_id 
      ,tp_cobertura_id 
      ,produto_id 
      ,ramo_id 
      ,dt_ocorrencia 
      ,evento_id 
      ,sinistro_id 
      ,fluxo_validado 
      ,pagamento_imediato 
      ,Deferido 
      ,fase 
      ,vigencia_dias 
      ,regra  
      ,tp_componente_id  
      ,sub_grupo_id  
      ,val_is 
      --,proposta_bb 
      ,dt_inicio_vigencia 
      ,dt_fim_vigencia 
      ,exibir_popup 
      ,tp_sinistro_parametro_id 
      --,sinistrado_cliente_id INT
      ,log_classificacao VARCHAR(MAX)                      
    ) 
    SELECT TOP 1 1 --indice
      ,1 --situacao (1deferido)
      ,a.proposta_id --proposta_id 
      ,28 --evento_sinistro_id 
      ,5 --tp_cobertura_id 
      ,a.produto_id --produto_id 
      ,a.ramo_id --ramo_id
      ,getdate() --dt_ocorrencia 
      ,0 --evento_id 
      ,0 --sinistro_id 
      ,1 --fluxo_validado  (1sim)
      ,1 --pagamento_imediato (1sim)
      ,1 --Deferido (1sim)
      ,2 --fase 
      ,200 --vigencia_dias (dias)
      ,1 --regra (numero qualquer)
      ,0 --tp_componente_id 
      ,0 --sub_grupo_id  
      ,100.00 --val_is 
      --,b.proposta_bb --proposta_bb 
      ,getdate()-200 --dt_inicio_vigencia 
      ,getdate()+200 --dt_fim_vigencia 
      ,1 --exibir_popup (1sim)
      ,3 --tp_sinistro_parametro_id (fase2)
      --,0 --sinistrado_cliente_id 
      ,'' --log_classificacao
      FROM seguros_db.dbo.proposta_tb a WITH(NOLOCK)
      --INNER JOIN seguros_db.dbo.proposta_adesao_tb b WITH(NOLOCK)
      --  ON b.proposta_id = a.proposta_id
      WHERE a.produto_id = 722   
                  
    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14967_SPS @USUARIO = 'TESTE' ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

	-- Declara��o e tratamento de variaveis (inicio)
	DECLARE @indice_contador AS INT
		,@ID_contador AS INT
		,@indice_max AS INT
		,@indice AS INT
		,@ID_max AS INT
		,@ID INT

	-- (fim) Declara��o e tratamento de variaveis 
	-----------
	BEGIN TRY
		-- Bloco de cria��o de tabelas temporarias (inicio)
		IF OBJECT_ID('tempdb..#evento_SEGBR_sinistro_detalhe_atual_tmp') IS NOT NULL
		BEGIN
			DROP TABLE #evento_SEGBR_sinistro_detalhe_atual_tmp
		END

		CREATE TABLE #evento_SEGBR_sinistro_detalhe_atual_tmp (
			--tabela temporaria utilizada dentro de outras procedures chamadas.
			id INT identity(1, 1) PRIMARY KEY NOT NULL
			,evento_id INT NULL
			,tp_detalhe VARCHAR(10) NULL
			,seq_detalhe SMALLINT NULL
			,descricao VARCHAR(300) NULL
			,usuario VARCHAR(20) NULL
			,dt_inclusao SMALLDATETIME NULL
			)

		-- (fim) Bloco de cria��o de tabelas temporarias 			
		-----------	
		-- alterando para n�o se aplica todos os registros
		UPDATE a
		SET a.deferido = 0
			,a.fluxo_validado = 0
			,a.pagamento_imediato = 0
		FROM #classificacao_tmp a

		--determinando a qual fase pertence o sinistro		
		UPDATE a
		SET a.fase = CASE s.nome
				WHEN 'Esteira Pagamento Imediato � Vida Individual'
					THEN 1
				WHEN 'Esteira Pagamento Imediato � Vida Individual Fase 2'
					THEN 2
				ELSE 0
				END
			,a.tp_sinistro_parametro_id = s.tp_sinistro_parametro_id
		FROM #classificacao_tmp a
		LEFT JOIN seguros_db.dbo.sinistro_parametro_chave_tb b WITH (NOLOCK)
			ON a.produto_id = b.produto_id
				AND a.ramo_id = b.ramo_id
				AND a.evento_sinistro_id = b.evento_sinistro_id
				AND a.tp_cobertura_id = b.tp_cobertura_id
		LEFT JOIN seguros_db.dbo.tp_sinistro_parametro_tb s WITH (NOLOCK)
			ON b.tp_sinistro_parametro_id = s.tp_sinistro_parametro_id
		WHERE b.dt_fim_vigencia IS NULL

		--apagando registros de produtos com coberturas marcados na fase 0 que tamb�m existam cadastrados na fase 2 com outra cobertura.
		DELETE a
		FROM #classificacao_tmp a
		WHERE a.fase = 0
			AND EXISTS (
				SELECT 1
				FROM #classificacao_tmp b
				WHERE b.fase = 2
				  AND a.produto_id = b.produto_id
					AND a.ramo_id = b.ramo_id
					AND a.evento_sinistro_id = b.evento_sinistro_id
					AND a.sinistro_id = b.sinistro_id					
				)

		--marcando deferimento da fase 1
		UPDATE a
		SET a.deferido = b.resultado
		FROM #classificacao_tmp a
		INNER JOIN seguros_db.dbo.sinistro_classificacao_parametro_tb b WITH (NOLOCK)
			ON b.sinistro_id = a.sinistro_id
		WHERE a.fase = 1

		--validando fluxo para propostas da fase 2
		UPDATE a
		SET fluxo_validado = (
				SELECT COUNT(s.sinistro_parametro_chave_id)
				FROM seguros_db.dbo.proposta_tb p WITH (NOLOCK)
				INNER JOIN seguros_db.dbo.sinistro_parametro_chave_tb s WITH (NOLOCK)
					ON p.produto_id = s.produto_id
						AND p.ramo_id = s.ramo_id
				WHERE p.proposta_id = a.proposta_id
					AND s.evento_sinistro_id = a.evento_sinistro_id
					AND s.tp_cobertura_id = a.tp_cobertura_id
					AND s.dt_fim_vigencia IS NULL
				)
		FROM #classificacao_tmp a
		WHERE fase = 2

		UPDATE a
		SET a.fluxo_validado = 1
			,a.pagamento_imediato = 1
		FROM #classificacao_tmp a
		WHERE a.fase = 2
			AND a.fluxo_validado > 0

		-- Classifica para o pagamento imediato
		SELECT @indice_contador = MIN(a.indice)
			,@indice_max = MAX(a.indice)
		FROM #classificacao_tmp a

		WHILE @indice_contador <= @indice_max
		BEGIN
			SET @indice = @indice_contador

			IF EXISTS (
					SELECT TOP 1 1
					FROM #classificacao_tmp a
					WHERE a.indice = @indice
						AND fluxo_validado = 1
						AND fase = 2
					)
			BEGIN
				SELECT @ID_contador = MIN(a.ID)
					,@ID_max = MAX(a.ID)
				FROM #classificacao_tmp a
				WHERE a.indice = @indice

				WHILE @ID_contador <= @ID_max
				BEGIN
					SET @ID = @ID_contador

					EXEC seguros_db.dbo.SEGS14966_SPS @id = @id
						,@usuario = @usuario

					SET @ID_contador = @ID_contador + 1
				END
			END

			SET @indice_contador = @indice_contador + 1
		END

		--Aplicar a regra do acumulo (regra 6)
		EXEC seguros_db.dbo.SEGS14973_SPS
		
	 --inserte dados na tabela real de classifica��o
   INSERT INTO seguros_db.dbo.sinistro_classificacao_parametro_tb (tp_sinistro_parametro_id,  
                   sinistro_id,   
                      resultado,   
                      dt_inclusao,   
                      usuario)   
      SELECT DISTINCT a.tp_sinistro_parametro_id
            ,a.sinistro_id
            ,a.deferido
            ,getdate()
            ,@usuario
      FROM #classificacao_tmp a
      WHERE fase = 2		

		--Trata exibi��o do popup	e atualiza dados na tabela temporaria de classifica��o
		EXEC seguros_db.dbo.SEGS14977_SPS
		
		--Gravar classifica��o do pagto imediato � vida fase 2
		EXEC seguros_db.dbo.SEGS14970_SPI @usuario = @usuario		

		--Gravar detalhamento da classifica��o do pagto imediato � vida fase 2
		INSERT INTO seguros_db.dbo.sinistro_classificacao_detalhe_tb (
			indice
			,situacao
			,proposta_id
			,evento_sinistro_id
			,tp_cobertura_id
			,produto_id
			,ramo_id
			,dt_ocorrencia
			,evento_id
			,sinistro_id
			,fluxo_validado
			,pagamento_imediato
			,deferido
			,fase
			,vigencia_dias
			,regra
			,tp_componente_id
			,sub_grupo_id
			,val_is
			,dt_inicio_vigencia
			,dt_fim_vigencia
			,exibir_popup
			,tp_sinistro_parametro_id
			,log_classificacao
			,dt_inclusao
			,usuario
			)
		SELECT indice
			,situacao
			,proposta_id
			,evento_sinistro_id
			,tp_cobertura_id
			,produto_id
			,ramo_id
			,dt_ocorrencia
			,evento_id
			,sinistro_id
			,fluxo_validado
			,pagamento_imediato
			,deferido
			,fase
			,vigencia_dias
			,regra
			,tp_componente_id
			,sub_grupo_id
			,val_is
			,dt_inicio_vigencia
			,dt_fim_vigencia
			,exibir_popup
			,tp_sinistro_parametro_id
			,log_classificacao
			,getdate()
			,@USUARIO
		FROM #classificacao_tmp

		--(fim) Bloco de codifica��o da procedure
		-----------				
		SET NOCOUNT OFF

		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO


