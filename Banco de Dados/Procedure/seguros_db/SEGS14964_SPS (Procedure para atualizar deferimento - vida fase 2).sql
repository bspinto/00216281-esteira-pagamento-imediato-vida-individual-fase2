CREATE PROCEDURE dbo.SEGS14964_SPS (
	@id AS INT
	,@deferido AS INT
	,@regra AS INT = NULL
	)
AS
/*
    Nova consultoria - 09/07/2020
	Demanda: C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL-FASE2
	Descri��o: (Procedure para atualizar deferimento - vida fase 2)
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*            
  BEGIN TRAN
   IF OBJECT_ID('tempdb..#classificacao_tmp') IS NOT NULL 
      BEGIN 
        DROP TABLE #classificacao_tmp 
      END
    
   CREATE TABLE #classificacao_tmp ( ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL
    ,indice INT
    ,situacao INT
    ,proposta_id INT
    ,evento_sinistro_id INT
    ,tp_cobertura_id INT
    ,produto_id INT
    ,ramo_id INT
    ,dt_ocorrencia smalldatetime
    ,evento_id INT
    ,sinistro_id INT
    ,fluxo_validado INT
    ,pagamento_imediato INT
    ,Deferido INT --deferido
    ,fase INT
    ,vigencia_dias INT
    ,regra  INT
    ,tp_componente_id  INT
    ,sub_grupo_id  INT
    ,val_is NUMERIC(15,2)
    ,proposta_bb INT
    ,dt_inicio_vigencia SMALLDATETIME
    ,dt_fim_vigencia SMALLDATETIME
    ,exibir_popup INT
    ,tp_sinistro_parametro_id INT
    ) 
    
    INSERT INTO #classificacao_tmp (indice
      ,situacao 
      ,proposta_id 
      ,evento_sinistro_id 
      ,tp_cobertura_id 
      ,produto_id 
      ,ramo_id 
      ,dt_ocorrencia 
      ,evento_id 
      ,sinistro_id 
      ,fluxo_validado 
      ,pagamento_imediato 
      ,Deferido 
      ,fase 
      ,vigencia_dias 
      ,regra  
      ,tp_componente_id  
      ,sub_grupo_id  
      ,val_is 
      ,proposta_bb 
      ,dt_inicio_vigencia 
      ,dt_fim_vigencia 
      ,exibir_popup 
      ,tp_sinistro_parametro_id 
    ) 
    SELECT TOP 1 1 --indice
      ,1 --situacao (1deferido)
      ,a.proposta_id --proposta_id 
      ,28 --evento_sinistro_id 
      ,5 --tp_cobertura_id 
      ,a.produto_id --produto_id 
      ,a.ramo_id --ramo_id
      ,getdate() --dt_ocorrencia 
      ,0 --evento_id 
      ,0 --sinistro_id 
      ,1 --fluxo_validado  (1sim)
      ,1 --pagamento_imediato (1sim)
      ,1 --Deferido (1sim)
      ,2 --fase 
      ,200 --vigencia_dias (dias)
      ,1 --regra (numero qualquer)
      ,0 --tp_componente_id 
      ,0 --sub_grupo_id  
      ,100.00 --val_is 
      ,b.proposta_bb --proposta_bb 
      ,getdate()-200 --dt_inicio_vigencia 
      ,getdate()+200 --dt_fim_vigencia 
      ,1 --exibir_popup (1sim)
      ,3 --tp_sinistro_parametro_id (fase2)
      FROM seguros_db.dbo.proposta_tb a WITH(NOLOCK)
      INNER JOIN seguros_db.dbo.proposta_adesao_tb b WITH(NOLOCK)
        ON b.proposta_id = a.proposta_id
      WHERE a.produto_id = 722      
  
    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14964_SPS 	@id = 1 ,@deferido = 1 ,@regra = NULL     ELSE 		  SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  

*/
BEGIN
	SET NOCOUNT ON

	BEGIN TRY
		UPDATE a
		SET deferido = @deferido
			,regra = @regra
		FROM #classificacao_tmp a
		WHERE a.id = @id

		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO


