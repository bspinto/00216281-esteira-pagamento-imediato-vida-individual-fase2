CREATE PROCEDURE dbo.SEGS14973_SPS
AS
/*
	(nova consultoria) - 23/07/2020
	Demanda: 00216281-esteira-pagamento-imediato-vida-individual-fase2
	Descri��o: Procedure para calcular acumulo - vida fase 2
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
   IF OBJECT_ID('tempdb..#classificacao_tmp') IS NOT NULL 
      BEGIN 
        DROP TABLE #classificacao_tmp 
      END
    
   CREATE TABLE #classificacao_tmp ( ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL
    ,indice INT
    ,situacao INT
    ,proposta_id INT
    ,evento_sinistro_id INT
    ,tp_cobertura_id INT
    ,produto_id INT
    ,ramo_id INT
    ,dt_ocorrencia smalldatetime
    ,evento_id INT
    ,sinistro_id INT
    ,fluxo_validado INT
    ,pagamento_imediato INT
    ,Deferido INT --deferido
    ,fase INT
    ,vigencia_dias INT
    ,regra  INT
    ,tp_componente_id  INT
    ,sub_grupo_id  INT
    ,val_is NUMERIC(15,2)
    ,proposta_bb INT
    ,dt_inicio_vigencia SMALLDATETIME
    ,dt_fim_vigencia SMALLDATETIME
    ,exibir_popup INT
    ,tp_sinistro_parametro_id INT
    ) 
    
    INSERT INTO #classificacao_tmp (indice
      ,situacao 
      ,proposta_id 
      ,evento_sinistro_id 
      ,tp_cobertura_id 
      ,produto_id 
      ,ramo_id 
      ,dt_ocorrencia 
      ,evento_id 
      ,sinistro_id 
      ,fluxo_validado 
      ,pagamento_imediato 
      ,Deferido 
      ,fase 
      ,vigencia_dias 
      ,regra  
      ,tp_componente_id  
      ,sub_grupo_id  
      ,val_is 
      ,proposta_bb 
      ,dt_inicio_vigencia 
      ,dt_fim_vigencia 
      ,exibir_popup 
      ,tp_sinistro_parametro_id 
    ) 
    SELECT TOP 1 1 --indice
      ,1 --situacao (1deferido)
      ,a.proposta_id --proposta_id 
      ,28 --evento_sinistro_id 
      ,5 --tp_cobertura_id 
      ,a.produto_id --produto_id 
      ,a.ramo_id --ramo_id
      ,getdate() --dt_ocorrencia 
      ,0 --evento_id 
      ,0 --sinistro_id 
      ,1 --fluxo_validado  (1sim)
      ,1 --pagamento_imediato (1sim)
      ,1 --Deferido (1sim)
      ,2 --fase 
      ,200 --vigencia_dias (dias)
      ,1 --regra (numero qualquer)
      ,0 --tp_componente_id 
      ,0 --sub_grupo_id  
      ,100.00 --val_is 
      ,b.proposta_bb --proposta_bb 
      ,getdate()-200 --dt_inicio_vigencia 
      ,getdate()+200 --dt_fim_vigencia 
      ,1 --exibir_popup (1sim)
      ,3 --tp_sinistro_parametro_id (fase2)
      FROM seguros_db.dbo.proposta_tb a WITH(NOLOCK)
      INNER JOIN seguros_db.dbo.proposta_adesao_tb b WITH(NOLOCK)
        ON b.proposta_id = a.proposta_id
      WHERE a.produto_id = 722   
	
	IF OBJECT_ID('tempdb..#evento_SEGBR_sinistro_detalhe_atual_tmp') IS NOT NULL
		BEGIN
			DROP TABLE #evento_SEGBR_sinistro_detalhe_atual_tmp
		END
		
		CREATE TABLE #evento_SEGBR_sinistro_detalhe_atual_tmp (
			id INT identity(1, 1) PRIMARY KEY NOT NULL
			,evento_id INT
			,tp_detalhe VARCHAR(10)
			,seq_detalhe SMALLINT
		  ,descricao VARCHAR(300)
			,usuario VARCHAR(20)
			,dt_inclusao SMALLDATETIME
			)  			           
          
    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14973_SPS ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

	-- Declara��o e tratamento de variaveis (inicio)
	DECLARE @val_acumulo AS NUMERIC(15, 2)
	DECLARE @val_acumulo_max AS NUMERIC(15, 2) = 50000
	DECLARE @atualiza_detalhe AS INT = 0
	DECLARE @log_classificacao VARCHAR(MAX) = ''
	
	-----------
	BEGIN TRY
		-- Bloco de codifica��o da procedure (inicio)		
		
		--existe regra 6 para verficar?
		IF EXISTS (
				SELECT TOP 1 1
				FROM #classificacao_tmp a
				WHERE a.regra = 6
				)
		BEGIN
		  --existe algum regra 6 deferida ?
		  IF EXISTS (
				  SELECT TOP 1 1
				  FROM #classificacao_tmp a
				  WHERE a.regra = 6
				  AND a.deferido = 1
				  )
		  BEGIN
			  SELECT @val_acumulo = SUM(a.val_is)
			  FROM #classificacao_tmp a
			  WHERE a.regra = 6
			  AND a.deferido = 1

			  IF @val_acumulo > @val_acumulo_max
			  BEGIN
			    SET @log_classificacao = 'Deferido por acumulo (IS): N / '
				  SET @atualiza_detalhe = 1
			  END
			  ELSE 
			  BEGIN
				  UPDATE a
				  SET log_classificacao = ISNULL(log_classificacao,'') + 'Deferido por acumulo (IS): S / '
				  FROM #classificacao_tmp a
				  WHERE a.regra = 6
				  AND a.deferido = 1
			  END
		  END

		  IF @atualiza_detalhe = 1
		  BEGIN
			  UPDATE a
			  SET deferido = 0
			  ,log_classificacao = ISNULL(log_classificacao,'') + @log_classificacao
			  FROM #classificacao_tmp a			
			  WHERE a.regra = 6
			  AND a.deferido = 1
  			  
			  --removendo gravacao do historico de sinistros da regra 6 que foram deferidos/indeferidos pela regra por�m indeferidos pelo acumulo
			  DELETE e
			  FROM #evento_SEGBR_sinistro_detalhe_atual_tmp e
			  INNER JOIN #classificacao_tmp a
			  ON a.evento_id = e.evento_id
			  WHERE a.regra = 6
			  AND isnull(a.deferido,0) = 0
			  AND e.seq_detalhe >= (select seq_detalhe from #evento_SEGBR_sinistro_detalhe_atual_tmp e2 where e2.evento_id = a.evento_id
									AND UPPER(RTRIM(LTRIM(e2.descricao))) = RTRIM(LTRIM('CLASSIFICA��O DO SINISTRO:')))
			  
			  --removendo gravacao do historico de sinistros da fase 2 que j� haviam sido deferidos antes da verificacao do acumulo
			  DELETE e
			  FROM #evento_SEGBR_sinistro_detalhe_atual_tmp e
			  INNER JOIN #classificacao_tmp a
			  ON a.evento_id = e.evento_id
			  WHERE a.fase = 2
			  AND isnull(a.deferido,0) = 1
			  AND e.seq_detalhe >= (select seq_detalhe from #evento_SEGBR_sinistro_detalhe_atual_tmp e2 where e2.evento_id = a.evento_id
									AND UPPER(RTRIM(LTRIM(e2.descricao))) = RTRIM(LTRIM('CLASSIFICA��O DO SINISTRO:')))

		  END
    END
    

		-- (fim) Bloco de codifica��o da procedure
		-----------				
		SET NOCOUNT OFF

		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO


