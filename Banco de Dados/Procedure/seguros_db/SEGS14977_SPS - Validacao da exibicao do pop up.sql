CREATE PROCEDURE dbo.SEGS14977_SPS 
  
AS     
    
 /**************************************************************************************************    
 Data Cria��o  : 30/07/2020 
 Projeto       : 00216281-esteira-pagamento-imediato-vida-individual-fase2
 Autores       : Sergio Ricardo
 Descri��o     : Procedure criada para verifica��o se pop up de aviso de deferimento imediato do aviso
				 ser� exibido ou n�o.
 '**************************************************************************************************/    
 
 -- BLOCO DE TESTE 
/*   
CREATE TABLE #classificacao_tmp ( ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL
 ,indice INT 
 ,situacao INT 
 ,proposta_id INT 
 ,evento_sinistro_id INT 
 ,tp_cobertura_id INT 
 ,produto_id INT 
 ,ramo_id INT 
 ,dt_ocorrencia smalldatetime 
 ,evento_id INT 
 ,sinistro_id INT 
 ,fluxo_validado INT 
 ,pagamento_imediato INT 
 ,deferido INT 
 ,fase INT 
 ,vigencia_dias INT 
 ,regra INT 
 ,tp_componente_id INT 
 ,sub_grupo_id INT 
 ,val_is NUMERIC(15,2) 
 ,exibir_popup INT
 )


INSERT INTO #classificacao_tmp (indice, situacao, proposta_id, evento_sinistro_id, tp_cobertura_id, produto_id, ramo_id, dt_ocorrencia, evento_id, sinistro_id, fluxo_validado, pagamento_imediato, Deferido, fase, vigencia_dias, regra, tp_componente_id, sub_grupo_id, val_is)
SELECT 1, 0, 5211139, 28, 5, 136, 93, '20200615', 46650822, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0
UNION 
SELECT 2, 0, 20464016, 28, 5, 718, 77, '20200615', 46650823, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0 
 
EXEC seguros_db.dbo.SEGS14977_SPS 

*/

BEGIN
	SET NOCOUNT ON

	BEGIN TRY
		 
		 DECLARE @fase_1_deferido INT,
				 @fase_1_indeferido INT,
				 @fase_2_deferido INT,
				 @fase_2_indeferido INT,
				 @fase_3	INT,
				 @apuracao INT
		
		SET @apuracao = 0
				
		IF OBJECT_ID('tempdb..#classificacao_tmp') IS NOT NULL 
		BEGIN 
			SELECT @fase_1_deferido = SUM(CASE WHEN t.fase = 1 
												 THEN CASE WHEN t.deferido = 1
														   THEN 1
														   ELSE 0
													   END
												 ELSE 0
											 END),
				   @fase_1_indeferido = SUM(CASE WHEN t.fase = 1 
												   THEN CASE WHEN t.deferido = 1
															 THEN 0
															 ELSE 1
														 END
												   ELSE 0
											   END),
				   @fase_2_deferido = SUM(CASE WHEN t.fase = 2 
												   THEN CASE WHEN t.deferido = 1
															 THEN 1
															 ELSE 0
														 END
												   ELSE 0
											   END),
				   @fase_2_indeferido = SUM(CASE WHEN t.fase = 2 
												   THEN CASE WHEN t.deferido = 1
															 THEN 0
															 ELSE 1
														 END
												   ELSE 0
											   END),
				   @fase_3 = SUM(CASE WHEN t.fase <> 1 AND t.fase <> 2
										THEN 1
										ELSE 0
									END)
			  FROM #classificacao_tmp t

			SELECT @apuracao = CASE WHEN ((@fase_1_deferido > 0) AND (@fase_1_indeferido = 0))
									THEN 1
									ELSE CASE WHEN @fase_1_indeferido = 0
											  THEN CASE WHEN ((@fase_2_deferido > 0) AND (@fase_2_indeferido = 0) AND (@fase_3 = 0))
														THEN 1
														ELSE 0
													END
											  ELSE 0
										  END
								END
		END
		
		UPDATE cla
		   SET cla.enviar_relatorio = CASE WHEN @apuracao = 1 
										   THEN CASE WHEN cla.resultado = 1
													 THEN 'S'
													 ELSE 'N'
												 END
										   ELSE 'N'
									   END
		  FROM seguros_db.dbo.sinistro_classificacao_parametro_tb cla
		 INNER JOIN #classificacao_tmp t
		    ON cla.sinistro_id = t.sinistro_id
		
		UPDATE t 
		   SET t.exibir_popup = @apuracao
		   FROM #classificacao_tmp t
		   
		UPDATE cla
           SET cla.enviar_relatorio = 'N'
          FROM seguros_db.dbo.sinistro_classificacao_parametro_tb cla WITH (NOLOCK)
         INNER JOIN #classificacao_tmp t
            ON cla.sinistro_id = t.sinistro_id   
         WHERE t.fase = 2
         AND EXISTS(SELECT TOP 1 1 FROM #classificacao_tmp x WHERE x.deferido = 0)
		 
		UPDATE t
           SET t.exibir_popup = 0
          FROM seguros_db.dbo.sinistro_classificacao_parametro_tb cla WITH (NOLOCK)
         INNER JOIN #classificacao_tmp t
            ON cla.sinistro_id = t.sinistro_id   
         WHERE t.fase = 2
         AND EXISTS(SELECT TOP 1 1 FROM #classificacao_tmp x WHERE x.deferido = 0)
		
		
		SELECT @apuracao 'Exibe Alerta'
		
		RETURN 
						
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END	
GO


 









