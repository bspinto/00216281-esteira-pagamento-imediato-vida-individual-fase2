CREATE PROCEDURE dbo.SEGS14966_SPS (
	@id INT
	,@USUARIO VARCHAR(20)
	)
AS
/*
    Nova consultoria - 09/07/2020
	Demanda: C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL-FASE2
	Descri��o: (Procedure para classifica��o do pagto autom�tico - vida fase 2)
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
   IF OBJECT_ID('tempdb..#classificacao_tmp') IS NOT NULL 
      BEGIN 
        DROP TABLE #classificacao_tmp 
      END
    
   CREATE TABLE #classificacao_tmp ( ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL
    ,indice INT
    ,situacao INT
    ,proposta_id INT
    ,evento_sinistro_id INT
    ,tp_cobertura_id INT
    ,produto_id INT
    ,ramo_id INT
    ,dt_ocorrencia smalldatetime
    ,evento_id INT
    ,sinistro_id NUMERIC(11)
    ,fluxo_validado INT
    ,pagamento_imediato INT
    ,deferido INT
    ,fase INT
    ,vigencia_dias INT
    ,regra  INT
    ,tp_componente_id  INT
    ,sub_grupo_id  INT
    ,val_is NUMERIC(15,2)
    --,proposta_bb INT
    ,dt_inicio_vigencia SMALLDATETIME
    ,dt_fim_vigencia SMALLDATETIME
    ,exibir_popup INT
    ,tp_sinistro_parametro_id INT
    --,sinistrado_cliente_id INT
    ,log_classificacao 
    ) 
    
    INSERT INTO #classificacao_tmp (indice
      ,situacao 
      ,proposta_id 
      ,evento_sinistro_id 
      ,tp_cobertura_id 
      ,produto_id 
      ,ramo_id 
      ,dt_ocorrencia 
      ,evento_id 
      ,sinistro_id 
      ,fluxo_validado 
      ,pagamento_imediato 
      ,Deferido 
      ,fase 
      ,vigencia_dias 
      ,regra  
      ,tp_componente_id  
      ,sub_grupo_id  
      ,val_is 
      --,proposta_bb 
      ,dt_inicio_vigencia 
      ,dt_fim_vigencia 
      ,exibir_popup 
      ,tp_sinistro_parametro_id 
      --,sinistrado_cliente_id INT
      ,log_classificacao VARCHAR(MAX)                      
    ) 
    SELECT TOP 1 1 --indice
      ,1 --situacao (1deferido)
      ,a.proposta_id --proposta_id 
      ,28 --evento_sinistro_id 
      ,5 --tp_cobertura_id 
      ,a.produto_id --produto_id 
      ,a.ramo_id --ramo_id
      ,getdate() --dt_ocorrencia 
      ,0 --evento_id 
      ,0 --sinistro_id 
      ,1 --fluxo_validado  (1sim)
      ,1 --pagamento_imediato (1sim)
      ,1 --Deferido (1sim)
      ,2 --fase 
      ,200 --vigencia_dias (dias)
      ,1 --regra (numero qualquer)
      ,0 --tp_componente_id 
      ,0 --sub_grupo_id  
      ,100.00 --val_is 
      --,b.proposta_bb --proposta_bb 
      ,getdate()-200 --dt_inicio_vigencia 
      ,getdate()+200 --dt_fim_vigencia 
      ,1 --exibir_popup (1sim)
      ,3 --tp_sinistro_parametro_id (fase2)
      --,0 --sinistrado_cliente_id 
      ,'' --log_classificacao
      FROM seguros_db.dbo.proposta_tb a WITH(NOLOCK)
      --INNER JOIN seguros_db.dbo.proposta_adesao_tb b WITH(NOLOCK)
      --  ON b.proposta_id = a.proposta_id
      WHERE a.produto_id = 722       
        
				
	  IF OBJECT_ID('tempdb..#evento_SEGBR_sinistro_detalhe_atual_tmp') IS NOT NULL
		BEGIN
			DROP TABLE #evento_SEGBR_sinistro_detalhe_atual_tmp
		END
		
		CREATE TABLE #evento_SEGBR_sinistro_detalhe_atual_tmp (
			id INT identity(1, 1) PRIMARY KEY NOT NULL
			,evento_id INT
			,tp_detalhe VARCHAR(10)
			,seq_detalhe SMALLINT
		  ,descricao VARCHAR(300)
			,usuario VARCHAR(20)
			,dt_inclusao SMALLDATETIME
			)  			           
  
    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14966_SPS @id = 1, @usuario = 'TESTE'    ELSE 		SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

	-- Declara��o e tratamento de variaveis (inicio)    
	DECLARE @produto_id AS INT
	DECLARE @ramo_id AS INT
	DECLARE @proposta_id AS INT
	DECLARE @evento_sinistro_id AS INT
	DECLARE @tp_cobertura_id AS INT
	DECLARE @dt_ocorrencia AS SMALLDATETIME
	DECLARE @evento_id AS NUMERIC(11, 0)
	---
	DECLARE @PgtoImediato AS SMALLINT = 1 -- (1-Sim / N�o)    
	DECLARE @nome_regra AS VARCHAR(40)
	DECLARE @valor AS CHAR(300)
	DECLARE @vigencia_dias INT
	DECLARE @val_is AS NUMERIC(15, 2)
	DECLARE @tp_componente_id AS INT
	DECLARE @sub_grupo_id AS INT
	DECLARE @tipo_ramo AS INT
	DECLARE @val_is_max1 AS NUMERIC(15, 2) = 10000
	DECLARE @regra_min_id INT
	DECLARE @regra_max_id INT
	DECLARE @exec_regra6 INT = 0
	DECLARE @log_classificacao VARCHAR(MAX) = ''
	DECLARE @deferido INT = 1

	SELECT @produto_id = a.produto_id
		,@ramo_id = a.ramo_id
		,@proposta_id = a.proposta_id
		,@evento_sinistro_id = a.evento_sinistro_id
		,@tp_cobertura_id = a.tp_cobertura_id
		,@dt_ocorrencia = a.dt_ocorrencia
		,@evento_id = a.evento_id
		,@tp_componente_id = a.tp_componente_id
		,@sub_grupo_id = IIF(@produto_id IN (115, 126, 714, 150, 222, 123, 226), NULL, 0)
	FROM #classificacao_tmp a
	WHERE ID = @ID

	BEGIN TRY
		IF OBJECT_ID('TEMPDB..#regra_tmp') IS NOT NULL
		BEGIN
			DROP TABLE #regra_tmp
		END

		CREATE TABLE #regra_tmp (
			id INT identity(1, 1) PRIMARY KEY NOT NULL
			,nome_regra VARCHAR(40) NULL
			,valor VARCHAR(300) NULL
			,operador VARCHAR(100) NULL
			)

		IF OBJECT_ID('tempdb..#consultar_coberturas_sps_tmp') IS NOT NULL
		BEGIN
			DROP TABLE #consultar_coberturas_sps_tmp
		END

		CREATE TABLE #consultar_coberturas_sps_tmp (
			tp_cobertura_id INT NULL
			,nome_cobertura VARCHAR(200) NULL
			,val_is NUMERIC(15, 2) NULL
			,dt_inicio_vigencia SMALLDATETIME NULL
			,dt_fim_vigencia SMALLDATETIME NULL
			,cod_objeto_segurado INT NULL
			,texto_franquia VARCHAR(100) NULL
			)

        IF OBJECT_ID('TEMPDB..#DETALHAMENTO') IS NOT NULL
		BEGIN
			DROP TABLE #DETALHAMENTO
		END

		CREATE TABLE #DETALHAMENTO (
			--tabela temporaria utilizada dentro de outras procedures chamadas.
			id INT IDENTITY(1, 1) NOT NULL
			,linha VARCHAR(60) NULL -- TAMANHO MAXIMO ACEITO NO DETALHAMENTO
			,regra INT NULL
			)

		--INICIALIZANDO O DETALHAMENTO  
		INSERT INTO #DETALHAMENTO (linha)
		VALUES ('--------------------------')
			,('')
			,('')
			,('CLASSIFICA��O DO SINISTRO:')
			,('--------------------------')
			,('') -- GRAVA��O DA CLASSIFICA��O, ID 6   
			,('')
			,('Informa��es:')
			,('')

		-----------------------------------------------------------------------------------------------------------------------------------------------------    
		--###################################################################################################################################################    
		--INICIO DA CLASSIFICA��O    
		-----------------------------------------------------------------------------------------------------------------------------------------------------        
		--INDEFERIMENTO AUTOMATICO
		SELECT @log_classificacao = a.log_classificacao
		FROM #classificacao_tmp a
		WHERE a.id = @id 
		
		IF EXISTS (
				SELECT TOP 1 1
				FROM seguros_db.dbo.sinistro_parametro_indeferimento_automatico_tb a WITH (NOLOCK)
				WHERE a.proposta_id = @proposta_id
				)
		BEGIN
			SET @log_classificacao = @log_classificacao + 'Indeferimento automatico: S / '						
			SET @deferido = 0
		END
		ELSE
		BEGIN
			SET @log_classificacao = @log_classificacao + 'Indeferimento automatico: N / '
			
			UPDATE a
			SET a.log_classificacao = @log_classificacao
			FROM #classificacao_tmp a
		  WHERE a.id = @id  


			INSERT INTO #regra_tmp (
				nome_regra
				,valor
				,operador
				)
			SELECT a.nome
				,b.valor
				,b.operador
			FROM seguros_db.DBO.sinistro_parametro_regra_tb a WITH (NOLOCK)
			INNER JOIN seguros_db.DBO.sinistro_parametro_chave_regra_tb b WITH (NOLOCK)
				ON a.sinistro_parametro_regra_id = b.sinistro_parametro_regra_id
			INNER JOIN seguros_db.DBO.sinistro_parametro_chave_TB c WITH (NOLOCK)
				ON b.sinistro_parametro_chave_id = c.sinistro_parametro_chave_id
			WHERE c.produto_id = @produto_id
				AND c.ramo_id = @ramo_id
				AND c.evento_sinistro_id = @evento_sinistro_id
				AND c.tp_cobertura_id = @tp_cobertura_id
				AND c.dt_fim_vigencia IS NULL

			SELECT @regra_min_id = MIN(ID)
				,@regra_max_id = MAX(ID)
			FROM #regra_tmp

			WHILE @regra_min_id <= @regra_max_id
			BEGIN
				SELECT @nome_regra = a.nome_regra
					,@valor = a.valor
				FROM #regra_tmp a
				WHERE a.id = @regra_min_id

				IF @valor = 'S'
					AND (
						@nome_regra = 'verifica_vigencia_diferenciada'
						OR @nome_regra = 'verifica_vigencia_normal'
						OR @nome_regra = 'verifica_vigencia_SEGP1285'
						OR @nome_regra = 'verifica_vigencia_assembleia'
						OR @nome_regra = 'verifica_vigencia_proposta_bb_ant'
						)
				BEGIN
					EXEC seguros_db.dbo.SEGS14968_SPS @nome_regra = @nome_regra
						,@proposta_id = @proposta_id
						,@dt_ocorrencia = @dt_ocorrencia
				END

				SET @regra_min_id = @regra_min_id + 1
			END

			SELECT @deferido = a.deferido
			     , @log_classificacao = log_classificacao		
			     , @vigencia_dias = vigencia_dias	    
			FROM #classificacao_tmp a 
			WHERE a.id = @id
			   
			IF @deferido = 0 
			BEGIN --Se for indeferido.
				SET @log_classificacao = @log_classificacao + ' Deferido por vigencia: N / '
				SET @deferido = 0
			END
			ELSE
			BEGIN --obtendo dias de vigencia e status do deferimento  
				SET @log_classificacao = @log_classificacao + ' Deferido por vigencia: S / '
				--obtendo valor da IS
				SET @tipo_ramo = 1

				INSERT INTO #consultar_coberturas_sps_tmp (
					tp_cobertura_id
					,nome_cobertura
					,val_is
					,dt_inicio_vigencia
					,dt_fim_vigencia
					,cod_objeto_segurado
					,texto_franquia
					)
				EXEC seguros_db.dbo.consultar_coberturas_sps @dt_ocorrencia = @dt_ocorrencia
					,@proposta_id = @proposta_id
					,@tp_componente_id = @tp_componente_id
					,@sub_grupo_id = @sub_grupo_id
					,@tipo_ramo = @tipo_ramo

				SELECT @val_is = val_is
				FROM #consultar_coberturas_sps_tmp
				WHERE tp_cobertura_id = 5

				UPDATE a
				SET val_is = @val_is
				FROM #classificacao_tmp a
				WHERE a.id = @id

				--corrigir para pegar o valor do parametro da regra 5 e o operador
				--REGRA 5                        
				IF @vigencia_dias > 364
				BEGIN
					IF EXISTS (
							SELECT TOP 1 1
							FROM #regra_tmp
							WHERE nome_regra = 'verifica_vigencia_1ano_x_valor_is'
								AND (
									(
										operador = '>'
										AND @val_is > valor
										)
									OR (
										operador = '<'
										AND @val_is < valor
										)
									OR (
										operador = '='
										AND @val_is = valor
										)
									OR (
										operador = '>='
										AND @val_is >= valor
										)
									OR (
										operador = '<='
										AND @val_is <= valor
										)
									)
							)
					BEGIN
						SET @log_classificacao = @log_classificacao + 'Deferido por verifica_vigencia_1ano_x_valor_is: S / '
  					SET @deferido = 1
					END
					ELSE
					BEGIN
						SET @log_classificacao = @log_classificacao + 'Deferido por verifica_vigencia_1ano_x_valor_is: N / '
  					SET @deferido = 0
					END
				END
				ELSE
				BEGIN
					SET @exec_regra6 = 1
				END

				--REGRA 6
				IF @exec_regra6 = 1
					AND EXISTS (
						SELECT TOP 1 1
						FROM #regra_tmp
						WHERE valor = 'S'
							AND nome_regra = 'verifica_vigencia_60dias_x_valor_is'
						)
				BEGIN
					IF @vigencia_dias > 60
					BEGIN
						IF @val_is <= @val_is_max1 --REGRA 6
						BEGIN
							SET @log_classificacao = @log_classificacao + 'Deferido por verifica_vigencia_60dias_x_valor_is (IS): S / '
							SET @deferido = 1
						END
						ELSE
						BEGIN
							SET @log_classificacao = @log_classificacao + 'Deferido por verifica_vigencia_60dias_x_valor_is (IS): N / '
  						SET @deferido = 0
						END
					END
					ELSE
					BEGIN
					  SET @log_classificacao = @log_classificacao + 'Deferido por verifica_vigencia_60dias_x_valor_is (Dias): N / '					  
						SET @deferido = 0
					END
				END
			END
		END

		-----------------------------------------------------------------------------------------------------------------------------------------------------    
		--###################################################################################################################################################    
		--FIM DA CLASSIFICA��O    
		-----------------------------------------------------------------------------------------------------------------------------------------------------    
		UPDATE a
		SET deferido = @deferido
			,regra = iif(@exec_regra6 = 1, 6, NULL)
			,a.log_classificacao = @log_classificacao
		FROM #classificacao_tmp a
		WHERE a.id = @id
    	  
		IF @deferido = 1
		BEGIN
			--ATUALIZA��O DA CLASSIFICA��O  
			UPDATE A
			SET LINHA = 'O SINISTRO FOI CLASSIFICADO COMO DEFERIDO'
				,REGRA = IIF(@exec_regra6 = 1, 6, 0)
			FROM #DETALHAMENTO A
			WHERE ID = 6

			INSERT INTO #DETALHAMENTO (linha)
			VALUES ('A data de ocorr�ncia do evento ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy'))
				,('est� dentro do per�odo de vig�ncia da proposta.')
				,('')
		END
		ELSE
		BEGIN
			--ATUALIZA��O DA CLASSIFICA��O  
			UPDATE A
			SET LINHA = 'O SINISTRO FOI CLASSIFICADO COMO INDEFERIDO'
				,REGRA = IIF(@exec_regra6 = 1, 6, 0)
			FROM #DETALHAMENTO A
			WHERE ID = 6

			INSERT INTO #DETALHAMENTO (linha)
			VALUES ('A data de ocorr�ncia ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy') + ' do aviso')
				,('n�o esta dentro do per�odo de vig�ncia da ap�lice.')
				,('')
		END

		--FINALIZANDO A CRIACAO DO DETALHAMENTO  
		INSERT INTO #DETALHAMENTO (linha)
		VALUES ('')
			,('--------------------------')

		DECLARE @MAX_SEQ_DETALHE AS INT

		--' TP_DETALHAMENTO:  
		--' 0- Anota��o  
		--' 1- Exig�ncia  
		--' 2- Recibo  
		--' 3- Indeferimento "A Exig�ncia dever� ser emitida Manualmente no SEGUR."  
		--' 4- Solicita��o de cancelamento  
		--' 5- Comunica��es feitas pelo GTR  
		--INICIANDO A GRAVA��O DO DETALHAMENTO  
		IF @deferido = 1
		BEGIN
			--PRODUTOS AVISADOS PELO SEGP0794 GRAVAM NAS TABELAS DE EVENTO DO SINISTRO   
			SELECT @MAX_SEQ_DETALHE = MAX(seq_detalhe)
			FROM SEGUROS_DB.DBO.evento_SEGBR_sinistro_detalhe_atual_tb WITH (NOLOCK)
			WHERE EVENTO_ID = @evento_id

			--GRAVANDO O DETALHAMENTO  
			INSERT INTO #evento_SEGBR_sinistro_detalhe_atual_tmp (
				evento_id
				,tp_detalhe
				,seq_detalhe
				,descricao
				,usuario
				,dt_inclusao
				)
			SELECT @evento_id AS evento_id
				,'ANOTACAO' AS tp_detalhe
				,ISNULL(@MAX_SEQ_DETALHE, 0) + ID AS seq_detalhe
				,LINHA AS descricao
				,@USUARIO AS usuario
				,GETDATE() AS dt_inclusao
			FROM #DETALHAMENTO WITH (NOLOCK)
		END

		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO


