CREATE PROCEDURE dbo.SEGS14736_SPS (  
     @tp_ramo_id INT,  
     @nome_tp_parametro_sinistro VARCHAR(200) = NULL  
     )  
    
AS       
      
 /**************************************************************************************************      
 Data Cria��o  : 29/04/2020   
 Projeto       : c00216281-esteira-pagamento-imediato-vida-individual Complemente Fase 1  
 Autores       : Sergio Ricardo  
 Descri��o     : Procedure criada para apurar oa avisos de sinistro que entraram na classificacao  
     de esteira de pagamento e ainda n�o foram enviados na relacao  
 '**************************************************************************************************/      
   
 -- BLOCO DE TESTE   
/*     
 DECLARE @SINISTRO_ID INT  
  
  BEGIN TRAN  
    IF @@TRANCOUNT > 0   
   EXEC seguros_db.dbo.SEGS14736_SPS 1  
    ELSE   
  SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'  
  ROLLBACK    
*/  
  
BEGIN  
 SET NOCOUNT ON  
  
 BEGIN TRY  
  IF @tp_ramo_id > 0  
     BEGIN  
	   DECLARE @dt_sistema SMALLDATETIME,
			   @max_seq INT, 
			   @it INT,
			   @seq INT,
			   @query NVARCHAR(4000),
			   @beneficiarios NVARCHAR(4000),
			   @parametros NVARCHAR(100),
			   @sinistro_id NUMERIC(11,0),
			   @proposta_id NUMERIC(9,0)
	     
	   SELECT @dt_sistema = GETDATE()
	  
	   IF OBJECT_ID('tempdb..#relacao_sinistros_classificados') IS NOT NULL  
	   BEGIN  
		DROP TABLE #relacao_sinistros_classificados  
	   END  
	  
	   CREATE TABLE #relacao_sinistros_classificados (  
		id INT IDENTITY(1, 1) NOT NULL,  
		dt_ocorrencia_sinistro SMALLDATETIME NULL,  
		dt_aviso_sinistro SMALLDATETIME NULL,  
		sinistro_id NUMERIC(11,0) NULL,  
		cpf_sinistrado VARCHAR(11) NULL,  
		valor_estimativa NUMERIC(15,2) NULL,  
		qtd_sinistros INT NULL,  
		produto VARCHAR(100) NULL,  
		plano VARCHAR(100) NULL, 
		evento VARCHAR(100) NULL,  
		prazo_total INT NULL,  
		situacao VARCHAR(50) NULL,  
		nome_sinistrado VARCHAR(60) NULL,  
		ddd_solicitante VARCHAR(04) NULL,  
		tel_solicitante VARCHAR(10) NULL,  
		nome_solicitante VARCHAR(60) NULL,
		proposta_id NUMERIC(9,0) NULL  
		)

		IF OBJECT_ID('tempdb..#relacao_sinistros_beneficiarios') IS NOT NULL  
		BEGIN  
			DROP TABLE #relacao_sinistros_beneficiarios  
		END  
		
		CREATE TABLE #relacao_sinistros_beneficiarios ( 
			sinistro_id NUMERIC(11,0) NULL,  
			proposta_id NUMERIC(9,0) NULL,
			nome_beneficiario VARCHAR(7000) NULL,
			seq_beneficiario INT NULL
		)    
	      
	   INSERT INTO #relacao_sinistros_classificados(dt_ocorrencia_sinistro, dt_aviso_sinistro, sinistro_id, produto, situacao, prazo_total, proposta_id)   
		  SELECT s.dt_ocorrencia_sinistro,  
			  s.dt_aviso_sinistro,  
			  scp.sinistro_id,  
			  CONVERT(VARCHAR(04), pr.produto_id)+ ' - ' + pr.nome,
			  sp.msg_parametro_classificacao + ' - Fase 1',  
			  DATEDIFF(D,s.dt_aviso_sinistro,GETDATE()),
			  p.proposta_id 
			FROM seguros_db.dbo.tp_sinistro_parametro_tb sp WITH (NOLOCK)  
		   INNER JOIN seguros_db.dbo.sinistro_classificacao_parametro_tb scp WITH (NOLOCK)   
		   ON sp.tp_sinistro_parametro_id = scp.tp_sinistro_parametro_id
		   AND sp.tp_sinistro_parametro_id <> 3 --VFOSANTOS (Nova Tendencia) - Excluindo os sinistros dos produtos da fase 2 do primeiro SELECT
		   INNER JOIN seguros_db.dbo.sinistro_tb s WITH (NOLOCK)   
		   ON scp.sinistro_id = s.sinistro_id  
		   INNER JOIN seguros_db.dbo.proposta_tb p WITH (NOLOCK)   
		   ON s.proposta_id = p.proposta_id  
		   INNER JOIN seguros_db.dbo.produto_tb pr WITH (NOLOCK)   
		   ON p.produto_id = pr.produto_id           
		   WHERE sp.tp_ramo_id = @tp_ramo_id  
			 AND ISNULL(sp.envia_relatorio_diario,'N') = 'S'  
			 AND scp.resultado = 1  
			 AND scp.enviado_planilha IS NULL
		UNION
		SELECT s.dt_ocorrencia_sinistro,  
			  s.dt_aviso_sinistro,  
			  scp.sinistro_id,  
			  CONVERT(VARCHAR(04), pr.produto_id)+ ' - ' + pr.nome,
			  sp.msg_parametro_classificacao + ' - Fase 2',  
			  DATEDIFF(D,s.dt_aviso_sinistro,GETDATE()),
			  p.proposta_id  
			FROM seguros_db.dbo.tp_sinistro_parametro_tb sp WITH (NOLOCK)  
		   INNER JOIN seguros_db.dbo.sinistro_classificacao_parametro_tb scp WITH (NOLOCK)   
		   ON sp.tp_sinistro_parametro_id = scp.tp_sinistro_parametro_id
		   AND sp.tp_sinistro_parametro_id = 3 --VFOSANTOS (Nova Tendencia) - Sele��o dos sinistro dos produtos da fase 2
		   INNER JOIN seguros_db.dbo.sinistro_tb s WITH (NOLOCK)   
		   ON scp.sinistro_id = s.sinistro_id  
		   INNER JOIN seguros_db.dbo.proposta_tb p WITH (NOLOCK)   
		   ON s.proposta_id = p.proposta_id  
		   INNER JOIN seguros_db.dbo.produto_tb pr WITH (NOLOCK)   
		   ON p.produto_id = pr.produto_id           
		   WHERE sp.tp_ramo_id = @tp_ramo_id  
			 AND ISNULL(sp.envia_relatorio_diario,'N') = 'S'  
			 AND scp.resultado = 1  
			 AND scp.enviado_planilha IS NULL
			 AND scp.enviar_relatorio = 'S'

		IF (SELECT COUNT(t.sinistro_id)
			  FROM #relacao_sinistros_classificados t) = 0
		   BEGIN
				SELECT 0 colunas
		
			    SELECT t.dt_ocorrencia_sinistro,  
					   t.dt_aviso_sinistro,  
					   t.sinistro_id,  
					   t.cpf_sinistrado,  
					   t.valor_estimativa,  
					   t.qtd_sinistros,  
					   t.produto,
					   t.plano, 
					   t.evento,  
					   t.prazo_total,  
					   t.situacao,  
					   t.nome_sinistrado,  
					   t.ddd_solicitante,  
					   t.tel_solicitante,  
					   t.nome_solicitante,
					   t.proposta_id
			      FROM #relacao_sinistros_classificados t
			      
			      RETURN
		     END	  

		UPDATE a
		SET plano = CONVERT(VARCHAR(04), plano_tb.plano_id)+ ' - ' + plano_tb.nome
		FROM #relacao_sinistros_classificados a
  INNER JOIN seguros_db.dbo.proposta_tb prop WITH(NOLOCK)
		  ON prop.proposta_id = a.proposta_id  
  INNER JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb WITH (NOLOCK)   
          ON escolha_plano_tb.proposta_id = prop.proposta_id  
  INNER JOIN seguros_db.dbo.plano_tb plano_tb WITH (NOLOCK)   
          ON plano_tb.plano_id = escolha_plano_tb.plano_id  
         AND plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia  
         AND plano_tb.produto_id = escolha_plano_tb.produto_id  
       WHERE prop.produto_id = 1237
		 AND escolha_plano_tb.lock = (SELECT TOP(1) lock   
                                        FROM seguros_db.dbo.escolha_plano_tb WITH (NOLOCK)    
									   WHERE proposta_id = prop.proposta_id   
                                         AND escolha_plano_tb.dt_escolha <= @dt_sistema  
                                    ORDER BY dt_escolha DESC)   
  
		
		DECLARE @proposta_sinistro AS TABLE (sinistro_id NUMERIC(11,0) NOT NULL,
											 proposta_id NUMERIC(11,0) NOT NULL)
		
		INSERT INTO @proposta_sinistro (sinistro_id, proposta_id)
		SELECT t.sinistro_id,
			   t.proposta_id
		  FROM #relacao_sinistros_classificados t
		  
		SET @sinistro_id = 0  
		  
		SELECT TOP 1 @sinistro_id = p.sinistro_id,
					 @proposta_id = p.proposta_id 
		  FROM @proposta_sinistro p

		WHILE ISNULL(@sinistro_id,0) > 0 
			BEGIN	
				INSERT INTO #relacao_sinistros_beneficiarios (sinistro_id, 
															  proposta_id, 
															  seq_beneficiario, 
															  nome_beneficiario)
													   SELECT @sinistro_id,
															  @proposta_id,
															  0 seq_beneficiario,
															  CONVERT(VARCHAR(7000),descricao) beneficiario
													     FROM seguros_db.dbo.texto_beneficiario_tb  WITH (NOLOCK)  
														WHERE proposta_id =  @proposta_id
														UNION 
													   SELECT @sinistro_id,
															  @proposta_id,
															  ROW_NUMBER( )OVER (PARTITION BY proposta_id ORDER BY nome ) seq_beneficiario,
															  CONVERT(VARCHAR(7000),nome) beneficiario
														 FROM seguros_db.dbo.beneficiario_tb  WITH (NOLOCK)  
														WHERE proposta_id =  @proposta_id
														  AND dt_fim_vigencia IS NULL  
														ORDER BY seq_beneficiario
				
				IF (SELECT MIN(s.seq_beneficiario)
				      FROM #relacao_sinistros_beneficiarios s 
				     WHERE s.sinistro_id = @sinistro_id
				      AND s.proposta_id = @proposta_id) = 0
				  BEGIN
						UPDATE s
						   SET s.seq_beneficiario = s.seq_beneficiario + 1
						  FROM #relacao_sinistros_beneficiarios s 
						 WHERE s.sinistro_id = @sinistro_id
						   AND s.proposta_id = @proposta_id 
				    END    
					   			
				DELETE p 
				  FROM @proposta_sinistro p
				 WHERE p.sinistro_id = @sinistro_id
				 
				SET @sinistro_id = 0
			     	        
				SELECT TOP 1 @sinistro_id = p.sinistro_id,
							 @proposta_id = p.proposta_id 
				  FROM @proposta_sinistro p
			   END

		UPDATE a
		SET a.plano = ' '
		FROM #relacao_sinistros_classificados a
		WHERE a.plano IS NULL

		SELECT @max_seq = max(a.seq_beneficiario)
		FROM #relacao_sinistros_beneficiarios a

		SET @it = 1

		IF @max_seq > 0 
		BEGIN
			WHILE @it <= @max_seq 
			BEGIN
				SET @query = 'ALTER TABLE #relacao_sinistros_classificados
							   ADD beneficiario_' + CAST(@it AS VARCHAR(3)) + ' VARCHAR(7000)'

				EXECUTE sp_executesql @query

				SET @query = 'UPDATE a
							  SET a.beneficiario_'+ CAST(@it AS VARCHAR(3)) + ' = ''''
							  FROM #relacao_sinistros_classificados a'

				EXECUTE sp_executesql @query
				
				SET @it = @it + 1
			END
		END
				  
	           
	   UPDATE t  
		  SET t.cpf_sinistrado = ISNULL(e.cpf_sinistrado, c.cpf_cnpj),  
		   t.nome_sinistrado = ISNULL(e.nome_sinistrado, c.nome),  
		   t.evento = CONVERT(VARCHAR(03), ev.evento_sinistro_id) + ' - ' + ev.nome,  
		   t.ddd_solicitante = e.ddd_solicitante,  
		   t.tel_solicitante = e.telefone_solicitante,  
		   t.nome_solicitante = e.nome_solicitante  
		 FROM #relacao_sinistros_classificados t  
		INNER JOIN seguros_db.dbo.sinistro_tb s WITH (NOLOCK)       
		ON t.sinistro_id = s.sinistro_id  
		INNER JOIN seguros_db.dbo.evento_sinistro_tb ev WITH (NOLOCK)       
		ON s.evento_sinistro_id = ev.evento_sinistro_id  
		INNER JOIN seguros_db.dbo.cliente_tb c WITH (NOLOCK)       
		ON s.cliente_id = c.cliente_id      
		 LEFT JOIN seguros_db.dbo.evento_SEGBR_sinistro_atual_tb e WITH (NOLOCK)  
		ON e.sinistro_id = s.sinistro_id  
		  AND e.evento_id = (SELECT MIN(i.evento_id)  
			 FROM seguros_db.dbo.evento_SEGBR_sinistro_atual_tb i WITH (NOLOCK)  
			   WHERE i.sinistro_id = s.sinistro_id  
			  AND i.nome_sinistrado IS NOT NULL)  
	  
	   UPDATE t  
		  SET t.nome_solicitante = sl.nome,  
		   t.tel_solicitante = sl.telefone,  
		   t.ddd_solicitante = sl.ddd  
		 FROM #relacao_sinistros_classificados t  
		INNER JOIN seguros_db.dbo.sinistro_tb s WITH (NOLOCK)       
		ON t.sinistro_id = s.sinistro_id      
		INNER JOIN seguros_db.dbo.solicitante_sinistro_tb sl WITH (NOLOCK)       
		ON s.solicitante_id = sl.solicitante_id  
		WHERE t.nome_solicitante IS NULL     
	         
	   DECLARE @Busca_Clientes AS TABLE (Id INT NOT NULL,  
				 cliente_id INT NOT NULL)  
	  
	   INSERT INTO @Busca_Clientes (Id, cliente_id)  
	   SELECT t.Id,  
		   c.cliente_id  
		 FROM #relacao_sinistros_classificados t  
		INNER JOIN seguros_db.dbo.cliente_tb c WITH (NOLOCK)   
		ON t.cpf_sinistrado = c.cpf_cnpj  
	  
	   DECLARE @Busca_Propostas AS TABLE (Id INT NOT NULL,  
				 proposta_id NUMERIC (9,0))  
	             
	   INSERT INTO @Busca_Propostas (Id, proposta_id)  
	   SELECT c.Id,  
		   p.proposta_id  
		 FROM @Busca_Clientes c  
		INNER JOIN seguros_db.dbo.proposta_tb p WITH (NOLOCK)   
		ON c.cliente_id = p.prop_cliente_id  
	  
	   INSERT INTO @Busca_Propostas (Id, proposta_id)  
	   SELECT c.Id,  
		   p.proposta_id  
		 FROM @Busca_Clientes c  
		INNER JOIN seguros_db.dbo.proposta_complementar_tb p WITH (NOLOCK)   
		ON c.cliente_id = p.prop_cliente_id        
	  
	   DECLARE @Busca_Sinistros AS TABLE (Id INT NOT NULL,  
				 sinistro_id NUMERIC (11,0))  
	  
	   INSERT INTO @Busca_Sinistros (Id, sinistro_id)  
	   SELECT p.Id,  
		   s.sinistro_id  
		 FROM @Busca_Propostas p  
		INNER JOIN seguros_db.dbo.sinistro_tb s WITH (NOLOCK)   
		ON p.proposta_id = s.proposta_id  
	  
	   UPDATE t  
		  SET t.qtd_sinistros = cont.Qtde  
		 FROM #relacao_sinistros_classificados t  
		OUTER APPLY (SELECT s.Id,  
			 COUNT(s.sinistro_id) Qtde  
		   FROM @Busca_Sinistros s   
			 GROUP BY s.Id) cont  
		WHERE t.Id = cont.Id          
	      
	   DECLARE @Apura_Is AS TABLE (Id INT NOT NULL,  
			  proposta_id NUMERIC (9,0),  
			  sinistro_id NUMERIC (11,0),  
			  dt_ocorrencia_sinistro SMALLDATETIME,  
			  tp_componente_id INT,  
			  val_is NUMERIC(15,2))  
	  
	   INSERT INTO @Apura_Is (Id, tp_componente_id, proposta_id, sinistro_id, dt_ocorrencia_sinistro)  
	   SELECT t.Id,  
		   CASE WHEN p1.proposta_id IS NOT NULL THEN 1  
		  WHEN p2.proposta_id IS NOT NULL THEN 3  
		  ELSE 1  
		 END,  
		   s.proposta_id,  
		   t.sinistro_id,  
		   t.dt_ocorrencia_sinistro   
		 FROM #relacao_sinistros_classificados t  
		INNER JOIN seguros_db.dbo.sinistro_tb s WITH (NOLOCK)   
		ON t.sinistro_id = s.sinistro_id  
		 LEFT JOIN seguros_db.dbo.proposta_tb p1 WITH (NOLOCK)    
		ON s.proposta_id = p1.proposta_id  
		 LEFT JOIN seguros_db.dbo.proposta_complementar_tb p2 WITH (NOLOCK)    
		ON s.proposta_id = p2.proposta_id  
	               
		UPDATE t   
		SET t.val_is = esc.val_is   
		  FROM @Apura_Is t  
		 INNER JOIN seguros_db.dbo.escolha_plano_tp_cob_tb esc (NOLOCK)      
		 ON t.proposta_id = esc.proposta_id         
		 INNER JOIN seguros_db.dbo.tp_cob_comp_tb tp_cob_comp_tb (NOLOCK)      
		 ON tp_cob_comp_tb.tp_cob_comp_id = esc.tp_cob_comp_id        
		AND tp_cob_comp_tb.tp_componente_id = t.tp_componente_id        
		 INNER JOIN seguros_db.dbo.sinistro_cobertura_tb sc WITH (NOLOCK)    
		 ON t.sinistro_id = sc.sinistro_id  
		 WHERE CONVERT(VARCHAR(8), esc.dt_escolha, 112) <=  t.dt_ocorrencia_sinistro  
		AND (esc.dt_fim_vigencia_cob > t.dt_ocorrencia_sinistro OR esc.dt_fim_vigencia_cob IS NULL )        
		AND tp_cob_comp_tb.tp_cobertura_id = sc.tp_cobertura_id  
	  
		UPDATE t   
		SET t.val_is = ep.imp_segurada  
		  FROM @Apura_Is t  
		 INNER JOIN seguros_db.dbo.escolha_plano_tb ep WITH (NOLOCK)  
		 ON t.proposta_id = ep.proposta_id  
		 INNER JOIN seguros_db.dbo.plano_tb p WITH (NOLOCK)   
		 ON p.plano_id = ep.plano_id             
		AND p.produto_id = ep.produto_id                        
		AND ep.dt_inicio_vigencia = p.dt_inicio_vigencia                                                     
		 INNER JOIN seguros_db.dbo.tp_cob_comp_plano_tb ccp WITH (NOLOCK)   
		 ON p.tp_plano_id = ccp.tp_plano_id  
		 INNER JOIN seguros_db.dbo.tp_cob_comp_tb cc WITH (NOLOCK)   
		 ON cc.tp_cob_comp_id = ccp.tp_cob_comp_id                        
		 INNER JOIN seguros_db.dbo.sinistro_cobertura_tb sc WITH (NOLOCK)    
		 ON t.sinistro_id = sc.sinistro_id  
		 WHERE CONVERT(VARCHAR(8), ep.dt_escolha, 112) <= t.dt_ocorrencia_sinistro  
		AND (ep.dt_fim_vigencia >= t.dt_ocorrencia_sinistro OR ep.dt_fim_vigencia IS NULL)                        
		AND cc.tp_componente_id = t.tp_componente_id  
		AND cc.tp_cobertura_id = sc.tp_cobertura_id  
	  
	   UPDATE t  
		  SET t.valor_estimativa = a.val_is  
		 FROM #relacao_sinistros_classificados t  
		INNER JOIN @Apura_Is a  
		ON t.Id = a.Id 
		
		SET @it = 1
		SET @beneficiarios = ''
		
		IF @max_seq > 0 --select final para atualizar os beneficiarios
		BEGIN
			WHILE @it <= @max_seq 
			BEGIN
				SET @query = 'UPDATE a
							  SET a.beneficiario_'+ CAST(@it AS VARCHAR(3)) + ' = b.nome_beneficiario
							  FROM #relacao_sinistros_classificados a
							  INNER JOIN #relacao_sinistros_beneficiarios b
							  ON a.sinistro_id = b.sinistro_id
							  AND a.proposta_id = b.proposta_id
							  WHERE b.seq_beneficiario = ' + CAST(@it AS VARCHAR(3)) + ' '
				EXECUTE sp_executesql @query
				
				SET @beneficiarios = @beneficiarios + ',t.beneficiario_'+ CAST(@it AS VARCHAR(3)) + ' '
				
				SET @it = @it + 1				
			END
		END 
	    
	    SET @parametros = '@max_seq INT'
	           
		SET @query = 'SELECT @max_seq colunas
		
					  SELECT t.dt_ocorrencia_sinistro,  
							 t.dt_aviso_sinistro,  
							 t.sinistro_id,  
							 t.cpf_sinistrado,  
							 t.valor_estimativa,  
							 t.qtd_sinistros,  
							 t.produto,
							 t.plano, 
							 t.evento,  
							 t.prazo_total,  
							 t.situacao,  
							 t.nome_sinistrado,  
							 t.ddd_solicitante,  
							 t.tel_solicitante,  
							 t.nome_solicitante,
							 t.proposta_id' + @beneficiarios +
					   'FROM #relacao_sinistros_classificados t   
					   ORDER BY t.dt_aviso_sinistro'
					   
		EXECUTE sp_executesql @query, @parametros, @max_seq = @max_seq 
       END  
  ELSE  
     BEGIN  
		SELECT CASE WHEN (@@SERVERNAME = 'SISAB003' OR @@SERVERNAME = 'SISAS003\ABS')   
		   THEN tp.lista_destino_producao  
		   ELSE tp.lista_destino_homologacao  
		  END lista_emails    
		  FROM seguros_db.dbo.tp_sinistro_parametro_tb tp WITH (NOLOCK)  
		 WHERE tp.nome = @nome_tp_parametro_sinistro  
       END  
 END TRY  
  
 BEGIN CATCH  
  DECLARE @ErrorMessage NVARCHAR(4000)  
  DECLARE @ErrorSeverity INT  
  DECLARE @ErrorState INT  
  
  SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()  
   ,@ErrorSeverity = ERROR_SEVERITY()  
   ,@ErrorState = ERROR_STATE()  
  
  RAISERROR (  
    @ErrorMessage  
    ,@ErrorSeverity  
    ,@ErrorState  
    )  
 END CATCH  
END   
  