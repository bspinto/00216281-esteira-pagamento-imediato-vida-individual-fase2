CREATE PROCEDURE dbo.SEGS14970_SPI ( @USUARIO VARCHAR(20)
  )
AS
/*
	Nome_do_desenvolvedor (nova consultoria) - 20/07/2020
	Demanda: 00216281-esteira-pagamento-imediato-vida-individual-fase2
	Descri��o: (Procedure para gravar classifica��o do pagto imediato � vida fase 2)
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
   IF OBJECT_ID('tempdb..#classificacao_tmp') IS NOT NULL 
      BEGIN 
        DROP TABLE #classificacao_tmp 
      END
    
   CREATE TABLE #classificacao_tmp ( ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL
    ,indice INT
    ,situacao INT
    ,proposta_id INT
    ,evento_sinistro_id INT
    ,tp_cobertura_id INT
    ,produto_id INT
    ,ramo_id INT
    ,dt_ocorrencia smalldatetime
    ,evento_id INT
    ,sinistro_id INT
    ,fluxo_validado INT
    ,pagamento_imediato INT
    ,Deferido INT --deferido
    ,fase INT
    ,vigencia_dias INT
    ,regra  INT
    ,tp_componente_id  INT
    ,sub_grupo_id  INT
    ,val_is NUMERIC(15,2)
    ,proposta_bb INT
    ,dt_inicio_vigencia SMALLDATETIME
    ,dt_fim_vigencia SMALLDATETIME
    ,exibir_popup INT
    ,tp_sinistro_parametro_id INT
    ) 
    
    INSERT INTO #classificacao_tmp (indice
      ,situacao 
      ,proposta_id 
      ,evento_sinistro_id 
      ,tp_cobertura_id 
      ,produto_id 
      ,ramo_id 
      ,dt_ocorrencia 
      ,evento_id 
      ,sinistro_id 
      ,fluxo_validado 
      ,pagamento_imediato 
      ,Deferido 
      ,fase 
      ,vigencia_dias 
      ,regra  
      ,tp_componente_id  
      ,sub_grupo_id  
      ,val_is 
      ,proposta_bb 
      ,dt_inicio_vigencia 
      ,dt_fim_vigencia 
      ,exibir_popup 
      ,tp_sinistro_parametro_id 
    ) 
    SELECT TOP 1 1 --indice
      ,1 --situacao (1deferido)
      ,a.proposta_id --proposta_id 
      ,28 --evento_sinistro_id 
      ,5 --tp_cobertura_id 
      ,a.produto_id --produto_id 
      ,a.ramo_id --ramo_id
      ,getdate() --dt_ocorrencia 
      ,0 --evento_id 
      ,0 --sinistro_id 
      ,1 --fluxo_validado  (1sim)
      ,1 --pagamento_imediato (1sim)
      ,1 --Deferido (1sim)
      ,2 --fase 
      ,200 --vigencia_dias (dias)
      ,1 --regra (numero qualquer)
      ,0 --tp_componente_id 
      ,0 --sub_grupo_id  
      ,100.00 --val_is 
      ,b.proposta_bb --proposta_bb 
      ,getdate()-200 --dt_inicio_vigencia 
      ,getdate()+200 --dt_fim_vigencia 
      ,1 --exibir_popup (1sim)
      ,3 --tp_sinistro_parametro_id (fase2)
      FROM seguros_db.dbo.proposta_tb a WITH(NOLOCK)
      INNER JOIN seguros_db.dbo.proposta_adesao_tb b WITH(NOLOCK)
        ON b.proposta_id = a.proposta_id
      WHERE a.produto_id = 722      
      
		IF OBJECT_ID('tempdb..#evento_SEGBR_sinistro_detalhe_atual_tmp') IS NOT NULL
		BEGIN
			DROP TABLE #evento_SEGBR_sinistro_detalhe_atual_tmp
		END
		
		CREATE TABLE #evento_SEGBR_sinistro_detalhe_atual_tmp (
			id INT identity(1, 1) PRIMARY KEY NOT NULL
			,evento_id INT
			,tp_detalhe VARCHAR(10)
			,seq_detalhe SMALLINT
		  ,descricao VARCHAR(300)
			,usuario VARCHAR(20)
			,dt_inclusao SMALLDATETIME
			)  
			
		INSERT INTO #evento_SEGBR_sinistro_detalhe_atual_tmp (
		  evento_id 
			,tp_detalhe 
			,seq_detalhe 
		  ,descricao 
			,usuario 
			,dt_inclusao 
			)
			VALUES (
			(SELECT  MAX(evento_id) FROM seguros_db.dbo.evento_SEGBR_sinistro_detalhe_atual_tb) --evento_id
			  ,1 --tp_detalhe 
			  ,1 --seq_detalhe 
		    ,NULL -- descricao 
			  ,'TESTE' --usuario 
			  ,GETDATE() --dt_inclusao
			)
		  
    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14970_SPI @USUARIO = 'TESTE' ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

	BEGIN TRY  
		-- (fim) Bloco de cria��o de tabelas temporarias 			
		-----------	
		-- Bloco de codifica��o da procedure (inicio)		  
		  IF EXISTS(SELECT TOP 1 1 FROM #classificacao_tmp WHERE fase = 2 and exibir_popup = 1)
		  BEGIN
	      INSERT INTO seguros_db.dbo.evento_SEGBR_sinistro_detalhe_atual_tb (
		      evento_id
		      ,tp_detalhe
		      ,seq_detalhe
		      ,descricao
		      ,usuario
		      ,dt_inclusao
		      )		  
	      SELECT evento_id
		      ,tp_detalhe
		      ,seq_detalhe
		      ,descricao
		      ,usuario
		      ,dt_inclusao
	      FROM #evento_SEGBR_sinistro_detalhe_atual_tmp WITH (NOLOCK)		
	    END                     	    
		-- (fim) Bloco de codifica��o da procedure
		-----------				
		SET NOCOUNT OFF

		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO

			
	