CREATE PROCEDURE DBO.SEGS14969_SPS @PROPOSTA_ID INT
AS
BEGIN

		BEGIN TRY
				
				--criaca��o de temporaria utilizada internamente
				IF OBJECT_ID('TEMPDB..#VIGENCIA_176') IS NOT NULL
								BEGIN
									DROP TABLE #VIGENCIA_176
								END

					  
				CREATE TABLE #VIGENCIA_176 (
									id INT IDENTITY(1, 1) NOT NULL PRIMARY KEY
									,dt_inicio_vigencia SMALLDATETIME NULL
									,dt_fim_vigencia SMALLDATETIME NULL
									,situacao CHAR(1) NULL --(I)Emitida / (C)Cancelada
									,proposta_id INT NULL
									) 
					
				IF (SELECT 1 FROM seguros_db.dbo.proposta_adesao_tb WHERE proposta_id = @proposta_id) = 1          
				BEGIN   
					 INSERT INTO #VIGENCIA_176 (
								 dt_inicio_vigencia,
								 dt_fim_vigencia,
								 situacao,
								 proposta_id
								)
						  SELECT ISNULL(adesao.dt_inicio_vigencia,'19000101') as dt_inicio_vigencia,
								 ISNULL(endosso_tb.dt_fim_vigencia_end, adesao.dt_fim_vigencia) as dt_fim_vigencia,
								 proposta.situacao,
								 proposta.proposta_id
							FROM seguros_db.dbo.proposta_tb proposta WITH (NOLOCK)
							JOIN seguros_db.dbo.proposta_adesao_tb adesao WITH (NOLOCK)
							  ON proposta.proposta_id = adesao.proposta_id
					   LEFT JOIN seguros_db.dbo.endosso_tb endosso_tb WITH(NOLOCK)        
							  ON endosso_tb.proposta_id = adesao.proposta_id          
							 AND (	(endosso_tb.tp_endosso_id = 314 -- Endosso de Renovacao        
															  AND endosso_tb.endosso_id = 
													(  SELECT MAX(aux.endosso_id)        
															 FROM seguros_db.dbo.endosso_tb aux WITH(NOLOCK)        
															WHERE aux.proposta_id = adesao.proposta_id        
															  AND aux.tp_endosso_id = 314 -- Endosso de Renovacao      
													)                 
									)
							  OR    ( proposta.Produto_ID in(1225 ,1231, 136)    
															  AND endosso_tb.endosso_id = 
													(  SELECT MAX(aux2.endosso_id)          
															 FROM seguros_db.dbo.endosso_financeiro_tb aux2 WITH(NOLOCK)          
															WHERE aux2.proposta_id = adesao.proposta_id 
													)        
									)   
								)           
					  
						   WHERE proposta.proposta_id = @proposta_id
							 AND proposta.situacao in ('i','c')
				END
				ELSE IF (SELECT 1 FROM seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb with(nolock)      
							INNER JOIN seguros_db.dbo.apolice_tb apolice_tb with(nolock)  
									ON apolice_tb.proposta_id         = proposta_adesao_tb.proposta_id      
								 WHERE proposta_adesao_tb.proposta_id = @proposta_id) = 1        
				BEGIN	
						  
					 INSERT INTO #VIGENCIA_176 (
								 dt_inicio_vigencia,
								 dt_fim_vigencia,
								 situacao,
								 proposta_id )
						  SELECT apolice_tb.dt_inicio_vigencia,              
								 ISNULL(apolice_tb.dt_fim_vigencia,'19000101') as dt_fim_vigencia,
								 proposta_tb.situacao,
								 proposta_tb.proposta_id
							FROM seguros_db.dbo.proposta_tb proposta_tb WITH(NOLOCK)              
					  INNER JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb WITH(NOLOCK)              
							  ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                      
					  INNER JOIN seguros_db.dbo.apolice_tb apolice_tb WITH(NOLOCK)              
							  ON apolice_tb.proposta_id = proposta_fechada_tb.proposta_id              
						   WHERE proposta_tb.proposta_id = @proposta_id 
				 END
				 ELSE
				 BEGIN
					   
					INSERT INTO #VIGENCIA_176 (
								dt_inicio_vigencia,
								dt_fim_vigencia,
								situacao,
								proposta_id )
						 SELECT                                        
								proposta_fechada_tb.dt_inicio_vig,              
								proposta_fechada_tb.dt_fim_vig,              
								proposta_tb.situacao,
								proposta_tb.proposta_id				
						   FROM seguros_db.dbo.proposta_tb proposta_tb WITH(NOLOCK)              
					 INNER JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb WITH(NOLOCK)              
							 ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                        
						  WHERE proposta_tb.proposta_id = @proposta_id      
					   
			END         
				 
			SELECT DT_INICIO_VIGENCIA,DT_FIM_VIGENCIA, SITUACAO, PROPOSTA_ID FROM #VIGENCIA_176
			
			
		END TRY

		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT

			SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
				,@ErrorSeverity = ERROR_SEVERITY()
				,@ErrorState = ERROR_STATE()

			RAISERROR (
					@ErrorMessage
					,@ErrorSeverity
					,@ErrorState
					)
		END CATCH
END
GO
