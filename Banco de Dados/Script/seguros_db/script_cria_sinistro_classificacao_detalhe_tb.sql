/*
	Ricardo Morais (ntendencia) - 20/07/2020
	Demanda: 00216281-esteira-pagamento-imediato-vida-individual-fase2
	Nome do script: script_cria_sinistro_classificacao_detalhe_tb
	Descri��o: Script para criar tabela sinistro_classificacao_detalhe_tb
	Objetivo: criar tabela
	Banco: seguros_db
	Ambiente: AB e ABS
*/

IF OBJECT_ID ('dbo.sinistro_classificacao_detalhe_tb', 'U') IS NULL

 BEGIN

         CREATE TABLE dbo.sinistro_classificacao_detalhe_tb

         ( sinistro_classificacao_detalhe_id INT IDENTITY(1,1) NOT NULL

          ,indice INT NULL

          ,situacao INT NULL

          ,proposta_id NUMERIC(9,0) NOT NULL

          ,evento_sinistro_id INT NULL

          ,tp_cobertura_id INT NULL

          ,produto_id INT NULL

          ,ramo_id INT NULL

          ,dt_ocorrencia smalldatetime NOT NULL

          ,evento_id INT NULL

          ,sinistro_id NUMERIC(11) NULL

          ,fluxo_validado INT NULL

          ,pagamento_imediato INT NULL

          ,deferido INT NOT NULL

          ,fase INT NULL

          ,vigencia_dias INT NULL

          ,regra INT NULL

          ,tp_componente_id INT NULL

          ,sub_grupo_id INT NULL

          ,val_is NUMERIC(15,2) NULL

          ,dt_inicio_vigencia SMALLDATETIME NULL

          ,dt_fim_vigencia SMALLDATETIME NULL

          ,exibir_popup INT NULL

          ,tp_sinistro_parametro_id INT NULL

          ,log_classificacao VARCHAR(1000) NULL

          ,dt_inclusao ud_dt_inclusao NOT NULL

          ,dt_alteracao ud_dt_alteracao NULL

          ,usuario ud_usuario NOT NULL

           ,lock TIMESTAMP NULL

           ,CONSTRAINT PK_sinistro_classificacao_detalhe PRIMARY KEY CLUSTERED (sinistro_classificacao_detalhe_id ASC)

           ,CONSTRAINT FK_sinistro_classificacao_detalhe_X_produto_P FOREIGN KEY (produto_id) REFERENCES dbo.produto_tb(produto_id)

         )

   END
   
   GO
   
   