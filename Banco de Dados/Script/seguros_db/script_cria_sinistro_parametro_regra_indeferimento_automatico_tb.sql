/*
	Nome_do_desenvolvedor (ntendencia) - 20/07/2020
	Demanda: 00216281-esteira-pagamento-imediato-vida-individual-fase2
	Nome do script: script_cria_sinistro_parametro_regra_indeferimento_automatico_tb
	Descri��o: Script para criar tabela sinistro_parametro_regra_indeferimento_automatico_tb
	Objetivo: criar
	Banco: seguros_db
	Ambiente: AB e ABS
*/



IF OBJECT_ID ('dbo.sinistro_parametro_indeferimento_automatico_tb', 'U') IS NULL
BEGIN
    CREATE TABLE dbo.sinistro_parametro_indeferimento_automatico_tb
    ( sinistro_parametro_indeferimento_automatico_id INT IDENTITY(1,1) NOT NULL
      ,proposta_id NUMERIC(9,0) NOT NULL
      ,produto_id INT NULL
      ,dt_inclusao ud_dt_inclusao NOT NULL
      ,dt_alteracao ud_dt_alteracao NULL
      ,usuario ud_usuario NOT NULL
      ,lock TIMESTAMP NULL
      ,CONSTRAINT PK_sinistro_parametro_indeferimento_automatico PRIMARY KEY CLUSTERED (sinistro_parametro_indeferimento_automatico_id ASC)
      ,CONSTRAINT FK_sinistro_parametro_indeferimento_automatico_x_produto_P FOREIGN KEY (produto_id) REFERENCES produto_tb(produto_id)
    )
    CREATE UNIQUE INDEX UK001_sinistro_parametro_indeferimento_automatico ON sinistro_parametro_indeferimento_automatico_tb (proposta_id ASC);
  END
