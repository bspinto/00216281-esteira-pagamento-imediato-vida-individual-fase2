DECLARE @total_registro INT
DECLARE @total_registro_afetados  INT
DECLARE @mensagem VARCHAR(500)

/*qtd registro que serao afetados*/
SELECT @total_registro = COUNT(tp_sinistro_parametro_tb.tp_sinistro_parametro_id)
  FROM seguros_db.dbo.tp_sinistro_parametro_tb tp_sinistro_parametro_tb
 WHERE tp_sinistro_parametro_tb.tp_ramo_id = 1 
	   
	UPDATE tp_sinistro_parametro_tb
	   SET tp_sinistro_parametro_tb.lista_destino_producao = tp_sinistro_parametro_tb.lista_destino_producao + '; chcarvalho@brasilseg.com.br; RAPAULA@brasilseg.com.br'
	  FROM seguros_db.dbo.tp_sinistro_parametro_tb tp_sinistro_parametro_tb
	 WHERE tp_sinistro_parametro_tb.tp_ramo_id = 1 
	   
SET @total_registro_afetados= @@ROWCOUNT

IF ((@@SERVERNAME = 'SISAB003' OR @@SERVERNAME = 'SISAS003\ABS')  AND @total_registro <> @total_registro_afetados) 
 BEGIN
	SET @mensagem='qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10),@total_registro) + 
										 'qtd de registros afetados: ' + CONVERT(VARCHAR(10),@total_registro_afetados)
	RAISERROR (@mensagem,16,1) 
 END	
