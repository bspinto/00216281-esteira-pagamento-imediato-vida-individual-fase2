/*-----------------------------------------------------------------------------------------------------            
	AUTOR: RICARDO (NTENDENCIA)
	DEMANDA: c00216281-esteira-pagamento-imediato-vida-individual - Fase 2
	TIPO: Script
	NOME: script_insert_tp_parametro_tb
	OBJETIVO: Script para inserir na tp_parametro_tb parametriacao de pgto imediato
	BANCO: seguros_db	
	BASE: AB/ABS    
*/-----------------------------------------------------------------------------------------------------  
DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)
DECLARE @usuario VARCHAR(20)
DECLARE @tp_sinistro_parametro_id INT

SELECT @total_registro_afetados = 0

/*qtd registro que serao afetados*/
SELECT @total_registro = 21

SELECT @usuario = 'C00216281'

---========================================
--SINISTRO_PARAMETRO_CHAVE_REGRA_TB
---========================================

			UPDATE CR  
			   SET CR.OPERADOR = '<='
			  FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_REGRA_TB CR
		INNER JOIN SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB PR WITH(NOLOCK)
			    ON CR.SINISTRO_PARAMETRO_REGRA_ID = PR.SINISTRO_PARAMETRO_REGRA_ID
			 WHERE pr.nome = 'verifica_vigencia_1ano_x_valor_is' and VALOR = '200000.00' AND OPERADOR = '>='
			
			
			   SET @total_registro_afetados = @total_registro_afetados + @@rowcount   
			   -- 21 registros




SELECT @total_registro as '@total_registro'
	,@total_registro_afetados as '@total_registro_afetados'

IF ((@@SERVERNAME = 'SISAB003' OR @@SERVERNAME = 'SISAS003\ABS')  AND @total_registro <> @total_registro_afetados) 
BEGIN
	SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + convert(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + convert(VARCHAR(10), @total_registro_afetados)

	RAISERROR (
			@mensagem
			,16
			,1
			)
END
GO


