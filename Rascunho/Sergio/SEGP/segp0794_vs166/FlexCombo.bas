Attribute VB_Name = "FlexCombo"
Option Explicit

      Declare Function CallWindowProc Lib "user32" Alias _
         "CallWindowProcA" (ByVal lpPrevWndFunc As Long, _
         ByVal hwnd As Long, ByVal Msg As Long, _
         ByVal wParam As Long, ByVal lparam As Long) As Long

      Declare Function SetWindowLong Lib "user32" Alias _
         "SetWindowLongA" (ByVal hwnd As Long, _
         ByVal nIndex As Long, ByVal dwNewLong As Long) As Long

      Private Const GWL_WNDPROC = -4
      Private IsHooked As Boolean
      Private Const WM_SIZE = &H5
      Private Const WM_PAINT = &HF
      Private lpPrevWndProc As Long
      Public gHW As Long

      Public Sub Hook()
          If IsHooked Then
          ' Do not hook it twice without unhooking,
          ' or you will not be able to unhook it.
          Else
          lpPrevWndProc = SetWindowLong(gHW, GWL_WNDPROC, _
             AddressOf WindowProc)
          IsHooked = True
          End If
      End Sub

      Public Sub Unhook()
          Dim temp As Long
          temp = SetWindowLong(gHW, GWL_WNDPROC, lpPrevWndProc)
          IsHooked = False
      End Sub

      Function WindowProc(ByVal hw As Long, ByVal uMsg As _
         Long, ByVal wParam As Long, ByVal lparam As Long) As Long
          WindowProc = CallWindowProc(lpPrevWndProc, hw, _
             uMsg, wParam, lparam)
             
          ' The interior of the control is repainted, but not resized.
          If uMsg = WM_SIZE Or uMsg = WM_PAINT Then
          
          If SEGP0794_7_1.form_quest = "pnt" Then
          
                SEGP0794_7_1.cmbResposta.Width = SEGP0794_7_1.flexPntAlerta.CellWidth
             SEGP0794_7_1.cmbResposta.Left = SEGP0794_7_1.flexPntAlerta.CellLeft + _
                SEGP0794_7_1.flexPntAlerta.Left
             SEGP0794_7_1.cmbResposta.Top = SEGP0794_7_1.flexPntAlerta.CellTop + _
                SEGP0794_7_1.flexPntAlerta.Top
            
            Else
                
             SEGP0794_7_2.cmbResposta.Width = SEGP0794_7_2.flexQuest.CellWidth
             SEGP0794_7_2.cmbResposta.Left = SEGP0794_7_2.flexQuest.CellLeft + _
                SEGP0794_7_2.flexQuest.Left
             SEGP0794_7_2.cmbResposta.Top = SEGP0794_7_2.flexQuest.CellTop + _
                SEGP0794_7_2.flexQuest.Top
            
            End If
          End If
      End Function

