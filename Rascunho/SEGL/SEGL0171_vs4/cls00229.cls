VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00229"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarsSql As String 'local copy
Private mvarlConexao As Long 'local copy
Private mvarAmbiente As New cls00228 'local copy

Public Property Set Ambiente(ByVal vData As cls00228)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Ambiente = Form1
    Set mvarAmbiente = vData
End Property

Public Property Get Ambiente() As cls00228
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Ambiente
    Set Ambiente = mvarAmbiente
End Property

Public Property Let lConexao(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lConexao = 5
    mvarlConexao = vData
End Property

Public Property Get lConexao() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lConexao
    lConexao = mvarlConexao
End Property

Public Property Let sSql(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sSql = 5
    mvarsSql = vData
End Property

Public Property Get sSql() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sSql
    sSql = mvarsSql
End Property

Public Sub CriarConexao(ByVal sSiglaSistema As String, _
                        ByVal sSiglaRecurso As String, _
                        ByVal sDescricaoRecurso As String, _
                        ByVal iAmbienteId As Integer, _
                        ByVal sUsuario As String)

    Ambiente.sSiglaSistema = sSiglaSistema
    Ambiente.sSiglaRecurso = sSiglaRecurso
    Ambiente.sDescricaoRecurso = sDescricaoRecurso
    Ambiente.lAmbienteId = iAmbienteId
    Ambiente.sUsuario = sUsuario

    lConexao = AbrirConexao(Ambiente.sSiglaSistema, _
                            Ambiente.lAmbienteId, _
                            Ambiente.sSiglaRecurso, _
                            Ambiente.sDescricaoRecurso)

    Exit Sub

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0001.clsConexao.CriarConexao - " & Err.Description)

End Sub

Public Sub DestruirConexao()

    If Not FecharConexao(lConexao) Then
        Call Err.Raise(vbObjectError + 2000, , "SEGL0100.DestruirConexao - N�o foi poss�vel finalizar a conex�o")
    End If
      
End Sub

Public Sub IniciarTransacao()

    If Not AbrirTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 2001, , "SEGL0100.IniciarTransacao - N�o foi poss�vel criar uma transa��o")
    End If

End Sub

Public Sub CommitTransacao()

    If Not ConfirmarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 2001, , "SEGL0100.CommitTransacao - N�o foi poss�vel confirmar a transa��o")
    End If

End Sub

Public Sub RollbackTransacao()

    If Not RetornarTransacao(lConexao) Then
        Call Err.Raise(vbObjectError + 2002, , "SEGL0100.RollbackTransacao - N�o foi poss�vel retornar a transa��o")
    End If
  
End Sub

Public Function ExecutarQuery(bRecordSet As Boolean) As Object

    On Error GoTo Trata_Erro

    If bRecordSet Then

        Set ExecutarQuery = Conexao_ExecutarSQL(Ambiente.sSiglaSistema, _
                                                Ambiente.lAmbienteId, _
                                                Ambiente.sSiglaRecurso, _
                                                Ambiente.sDescricaoRecurso, _
                                                sSql, _
                                                lConexao, _
                                                True)

    Else

        Call Conexao_ExecutarSQL(Ambiente.sSiglaSistema, _
                                 Ambiente.lAmbienteId, _
                                 Ambiente.sSiglaRecurso, _
                                 Ambiente.sDescricaoRecurso, _
                                 sSql, _
                                 lConexao, _
                                 False)

    End If
 
    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0001.clsConexao.ExecutarQuery - " & Err.Description)

End Function

Public Function ExecutarBCP(ByVal sBCP As String, _
                            Optional ByVal bAssincrona As Boolean = False) As Boolean

    On Error GoTo Trata_Erro

    Call ExecutaBCP(Ambiente.sSiglaSistema, _
                    Ambiente.lAmbienteId, _
                    Ambiente.sSiglaRecurso, _
                    Ambiente.sDescricaoRecurso, _
                    sBCP, _
                    bAssincrona)

    Exit Function

Trata_Erro:

    Call Err.Raise(Err.Number, , "SEGL0001.clsConexao.ExecutarBCP - " & Err.Description)

End Function
