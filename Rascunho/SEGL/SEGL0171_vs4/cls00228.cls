VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00228"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarsSiglaRecurso As String 'local copy
Private mvarsSiglaSistema As String 'local copy
Private mvarsDescricaoRecurso As String 'local copy
Private mvarsUsuario As String 'local copy
Private mvarlAmbienteId As Long 'local copy

Public Property Let lAmbienteId(ByVal vData As Long)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.lAmbienteId = Form1
    mvarlAmbienteId = vData
End Property

Public Property Get lAmbienteId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lAmbienteId
    lAmbienteId = mvarlAmbienteId
End Property

Public Property Let sUsuario(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sUsuario = 5
    mvarsUsuario = vData
End Property

Public Property Get sUsuario() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sUsuario
    sUsuario = mvarsUsuario
End Property

Public Property Let sDescricaoRecurso(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sDescricaoRecurso = 5
    mvarsDescricaoRecurso = vData
End Property

Public Property Get sDescricaoRecurso() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sDescricaoRecurso
    sDescricaoRecurso = mvarsDescricaoRecurso
End Property

Public Property Let sSiglaSistema(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sSiglaSistema = 5
    mvarsSiglaSistema = vData
End Property

Public Property Get sSiglaSistema() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sSiglaSistema
    sSiglaSistema = mvarsSiglaSistema
End Property

Public Property Let sSiglaRecurso(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sSiglaRecurso = 5
    mvarsSiglaRecurso = vData
End Property

Public Property Get sSiglaRecurso() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sSiglaRecurso
    sSiglaRecurso = mvarsSiglaRecurso
End Property
