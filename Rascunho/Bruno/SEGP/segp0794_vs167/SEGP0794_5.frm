VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form SEGP0794_5 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0794 - Aviso de Sinistro pela CA"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7275
   Icon            =   "SEGP0794_5.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   7275
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdTeste 
      Caption         =   "Teste"
      Height          =   375
      Left            =   240
      TabIndex        =   47
      Top             =   6600
      Width           =   1935
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados do Solicitante"
      Height          =   6480
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Width           =   7260
      Begin VB.ComboBox cmbUF 
         Enabled         =   0   'False
         Height          =   315
         Left            =   3000
         Style           =   2  'Dropdown List
         TabIndex        =   44
         Top             =   2520
         Width           =   825
      End
      Begin MSMask.MaskEdBox mskCpfSolicitante 
         Height          =   330
         Left            =   4395
         TabIndex        =   43
         Top             =   2475
         Width           =   2670
         _ExtentX        =   4710
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   11
         Mask            =   "###########"
         PromptChar      =   "_"
      End
      Begin VB.ComboBox cmbMunicipio 
         Height          =   315
         Left            =   3000
         TabIndex        =   5
         Text            =   "cmbMunicipio"
         Top             =   1845
         Width           =   4065
      End
      Begin VB.ComboBox cmbGrauParentesco 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   27
         Top             =   6000
         Width           =   6855
      End
      Begin VB.ComboBox cmbTipoTelefone 
         Enabled         =   0   'False
         Height          =   315
         Index           =   4
         Left            =   4395
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   4605
         Width           =   2670
      End
      Begin VB.ComboBox cmbTipoTelefone 
         Enabled         =   0   'False
         Height          =   315
         Index           =   3
         Left            =   4395
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   4245
         Width           =   2670
      End
      Begin VB.ComboBox cmbTipoTelefone 
         Enabled         =   0   'False
         Height          =   315
         Index           =   2
         Left            =   4395
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   3885
         Width           =   2670
      End
      Begin VB.ComboBox cmbTipoTelefone 
         Enabled         =   0   'False
         Height          =   315
         Index           =   1
         Left            =   4395
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   3525
         Width           =   2670
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   4
         Left            =   3045
         MaxLength       =   4
         TabIndex        =   24
         Top             =   4605
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   3045
         MaxLength       =   4
         TabIndex        =   20
         Top             =   4245
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   3045
         MaxLength       =   4
         TabIndex        =   16
         Top             =   3885
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   3045
         MaxLength       =   4
         TabIndex        =   12
         Top             =   3525
         Width           =   915
      End
      Begin VB.ComboBox cmbTipoTelefone 
         Enabled         =   0   'False
         Height          =   315
         Index           =   0
         ItemData        =   "SEGP0794_5.frx":0442
         Left            =   4395
         List            =   "SEGP0794_5.frx":0444
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   3165
         Width           =   2670
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   3045
         MaxLength       =   4
         TabIndex        =   8
         Top             =   3165
         Width           =   915
      End
      Begin VB.TextBox txtCEPSolicitante 
         Height          =   330
         Left            =   240
         MaxLength       =   9
         TabIndex        =   1
         Top             =   2475
         Width           =   1635
      End
      Begin VB.TextBox txtBairroSolicitante 
         Height          =   330
         Left            =   225
         MaxLength       =   30
         TabIndex        =   4
         Top             =   1845
         Width           =   2625
      End
      Begin VB.TextBox txtEnderecoSolicitante 
         Height          =   330
         Left            =   225
         MaxLength       =   60
         TabIndex        =   3
         Top             =   1165
         Width           =   6810
      End
      Begin VB.TextBox txtEmailSolicitante 
         Height          =   330
         Left            =   225
         MaxLength       =   60
         TabIndex        =   26
         Top             =   5280
         Width           =   6810
      End
      Begin VB.TextBox txtNomeSolicitante 
         Height          =   330
         Left            =   225
         MaxLength       =   60
         TabIndex        =   0
         Top             =   540
         Width           =   6810
      End
      Begin VB.CommandButton cmdPesquisarCEP 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   380
         Left            =   1935
         TabIndex        =   2
         Top             =   2455
         Width           =   440
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   0
         Left            =   255
         TabIndex        =   6
         Top             =   3165
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   0
         Left            =   1200
         TabIndex        =   7
         Top             =   3165
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   1
         Left            =   255
         TabIndex        =   10
         Top             =   3525
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   2
         Left            =   255
         TabIndex        =   14
         Top             =   3885
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   3
         Left            =   255
         TabIndex        =   18
         Top             =   4245
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDDD 
         Height          =   330
         Index           =   4
         Left            =   255
         TabIndex        =   22
         Top             =   4605
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   2
         Mask            =   "##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   1
         Left            =   1200
         TabIndex        =   11
         Top             =   3525
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   2
         Left            =   1200
         TabIndex        =   15
         Top             =   3885
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   3
         Left            =   1200
         TabIndex        =   19
         Top             =   4245
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskTelefone 
         Height          =   330
         Index           =   4
         Left            =   1215
         TabIndex        =   23
         Top             =   4605
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Mask            =   "9####-####"
         PromptChar      =   "_"
      End
      Begin VB.Label Label11 
         Caption         =   "CPF:"
         Height          =   255
         Left            =   4440
         TabIndex        =   45
         Top             =   2250
         Width           =   375
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Grau de Parentesco:"
         Height          =   195
         Left            =   240
         TabIndex        =   46
         Top             =   5760
         Width           =   1470
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Telefone:"
         Height          =   195
         Left            =   4395
         TabIndex        =   42
         Top             =   2940
         Width           =   1035
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Ramal:"
         Height          =   195
         Left            =   3045
         TabIndex        =   41
         Top             =   2940
         Width           =   495
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "CEP:"
         Height          =   195
         Left            =   225
         TabIndex        =   40
         Top             =   2250
         Width           =   360
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "U.F.:"
         Height          =   195
         Left            =   3000
         TabIndex        =   39
         Top             =   2250
         Width           =   345
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Munic�pio:"
         Height          =   195
         Left            =   3000
         TabIndex        =   38
         Top             =   1580
         Width           =   750
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Bairro:"
         Height          =   195
         Left            =   225
         TabIndex        =   37
         Top             =   1580
         Width           =   450
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Endere�o:"
         Height          =   195
         Left            =   225
         TabIndex        =   36
         Top             =   920
         Width           =   735
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "E-mail:"
         Height          =   195
         Left            =   225
         TabIndex        =   35
         Top             =   5055
         Width           =   465
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Telefone:"
         Height          =   195
         Left            =   1245
         TabIndex        =   34
         Top             =   2940
         Width           =   675
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "D.D.D.:"
         Height          =   195
         Left            =   240
         TabIndex        =   33
         Top             =   2940
         Width           =   555
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Nome:"
         Height          =   195
         Left            =   225
         TabIndex        =   32
         Top             =   315
         Width           =   465
      End
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   4590
      TabIndex        =   28
      Top             =   6600
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   5985
      TabIndex        =   30
      Top             =   6600
      Width           =   1275
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   3195
      TabIndex        =   29
      Top             =   6600
      Width           =   1275
   End
End
Attribute VB_Name = "SEGP0794_5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public municipio_id As Integer  'Para armazenar o municipio_id do solicitante

Private Sub cmbMunicipio_Click()
    If cmbMunicipio.ListIndex <> -1 Then
        municipio_id = cmbMunicipio.ItemData(cmbMunicipio.ListIndex)
        cmbUF.Text = obtemEstado(municipio_id, cmbMunicipio.Text)
    End If
End Sub

'Maur�cio(Stefanini), em 18/10/2005 - FLOWBR 95250
Private Sub cmbMunicipio_LostFocus()
    If cmbMunicipio.ListIndex <> -1 Then
        municipio_id = cmbMunicipio.ItemData(cmbMunicipio.ListIndex)
        cmbUF.Text = obtemEstado(municipio_id, cmbMunicipio.Text)
    End If

End Sub

Private Function obtemEstado(municipio_id As Integer, Municipio As String) As String
Dim SQL As String
Dim rs As ADODB.Recordset

        obtemEstado = ""
        SQL = ""
        SQL = SQL & "SELECT estado "
        SQL = SQL & "FROM municipio_tb with (nolock) "
        SQL = SQL & "WHERE rtrim(nome) = '" & Trim(Municipio) & "'"
        SQL = SQL & "AND municipio_id = " & municipio_id
        
        Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                  glAmbiente_id, _
                  App.Title, _
                  App.FileDescription, _
                  SQL, _
                  True)
        If Not rs.EOF Then
            obtemEstado = Trim(rs!estado)
        End If
End Function

Private Sub cmdContinuar_Click()
    Dim Cont As Integer
    With SEGP0794_7
        
        If txtNomeSolicitante.Text <> "" Then
            'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
            '.txtNomeSolicitante.Text = txtNomeSolicitante.Text
            .txtNomeSolicitante.Text = MudaAspaSimples(txtNomeSolicitante.Text)
            'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : fim
        Else
            MsgBox "� necess�rio indicar o nome do Solicitante.", vbInformation
            txtNomeSolicitante.SetFocus
            Exit Sub
        End If
        
        If txtEnderecoSolicitante.Text <> "" Then
            .txtEnderecoSolicitante.Text = txtEnderecoSolicitante.Text
        Else
            MsgBox "� necess�rio indicar o endere�o do Solicitante.", vbInformation
            txtEnderecoSolicitante.SetFocus
            Exit Sub
        End If
        
        If txtBairroSolicitante.Text <> "" Then
            .txtBairroSolicitante.Text = txtBairroSolicitante.Text
        Else
            MsgBox "� necess�rio indicar o bairro do Solicitante.", vbInformation
            txtBairroSolicitante.SetFocus
            Exit Sub
        End If
        
        If cmbMunicipio.ListIndex <> -1 Then
           .txtMunicipioSolicitante.Text = cmbMunicipio.Text
        Else
            MsgBox "� necess�rio indicar o municipio do Solicitante.", vbInformation
            cmbMunicipio.SetFocus
            Exit Sub
        End If
        
        If txtCEPSolicitante.Text <> "" Then
            .txtCEPSolicitante.Text = txtCEPSolicitante.Text
        Else
            MsgBox "� necess�rio indicar o CEP do Solicitante.", vbInformation
            txtCEPSolicitante.SetFocus
            Exit Sub
        End If
        
        '07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante (inicio)
        '06/05/2020 - VFOSANTOS (Ntendencia) - Altera��o para que o campo CPF do solicitante seja obrigatorio
        If Not CPF_Ok(mskCpfSolicitante.Text) Or Trim(mskCpfSolicitante.Text) = "" Or mskCpfSolicitante.Text = "___________" Then
            MsgBox "CPF do Solicitante informado n�o � valido.", vbInformation
            mskCpfSolicitante.SetFocus
            Exit Sub
        Else
            .mskCpfSolicitante.Text = mskCpfSolicitante.Text
        End If
        '07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante (fim)
        
        If mskDDD(0).Text <> "__" And Len(Replace(mskDDD(0).Text, "_", "")) = 2 Then
            .txtDDD(0).Text = mskDDD(0).Text
        Else
            MsgBox "� necess�rio indicar o DDD v�lido de contato de Solicitante.", vbInformation
            mskDDD(0).SetFocus
            Exit Sub
        End If
        
'25/12/2012: Nova - Mauro Vianna : adequa��o para telefone de 9 d�gitos
'        If mskTelefone(0).Text <> "____-____" Then
        If mskTelefone(0).Text <> "_____-____" Then
            .txtTelefone(0).Text = mskTelefone(0).Text
        Else
            MsgBox "� necess�rio indicar o telefone de contato de Solicitante.", vbInformation
            mskTelefone(0).SetFocus
            Exit Sub
        End If
        
               
        If cmbTipoTelefone(0).ListIndex <> -1 Then
            .txtTipoTelefone(0).Text = cmbTipoTelefone(0).Text
        Else
            MsgBox "� necess�rio indicar o tipo de telefone de contato de Solicitante.", vbInformation
            cmbTipoTelefone(0).SetFocus
            Exit Sub
        End If
        
        If cmbGrauParentesco.ListIndex <> -1 Then
            .txtGrauParentesco.Text = cmbGrauParentesco.ItemData(cmbGrauParentesco.ListIndex) & "-" & cmbGrauParentesco.Text
        Else
            MsgBox "� necess�rio indicar o parentesco do Solicitante.", vbInformation
            cmbGrauParentesco.SetFocus
            Exit Sub
        End If
        
        '==========================================================================================
        'Ricardo Toledo - Confitec : INC000002881273 : 09/11/2011 : Inicio
        '==========================================================================================
        'Se o usu�rio informar um telefone, obrigatoriamente ter� que informar o tipo de telefone
        'pois, conforme o INCIDENTE, ocasiona erro no momento da grava��o do aviso de sinistro
        
        'For Cont = 0 To 4
        '    .txtDDD(Cont).Text = Replace(mskDDD(Cont).Text, "_", "")
        '    .txtTelefone(Cont).Text = Replace(mskTelefone(Cont).Text, "_", "")
        '    .txtRamal(Cont).Text = txtRamal(Cont).Text
        '    .txtTipoTelefone(Cont).Text = cmbTipoTelefone(Cont).Text
        'Next Cont
        
        For Cont = 0 To 4
            If (Replace(mskDDD(Cont).Text, "_", "") <> "" Or Replace(Replace(mskTelefone(Cont).Text, "_", ""), "-", "") <> "") And Trim(cmbTipoTelefone(Cont).Text) = "" Then
                MsgBox "� necess�rio indicar tipo de telefone.", vbInformation
                cmbTipoTelefone(Cont).SetFocus
                Exit Sub
            Else
                .txtDDD(Cont).Text = Replace(mskDDD(Cont).Text, "_", "")
                .txtTelefone(Cont).Text = Replace(mskTelefone(Cont).Text, "_", "")
                .txtRamal(Cont).Text = txtRamal(Cont).Text
                .txtTipoTelefone(Cont).Text = cmbTipoTelefone(Cont).Text
            End If
        Next Cont
        'Ricardo Toledo - Confitec : INC000002881273 : 09/11/2011 : Fim
        '==========================================================================================
        
        municipio_id = VerificaMunicipio(Trim(cmbMunicipio.Text), Trim(cmbUF.Text))
        .txtEmailSolicitante.Text = txtEmailSolicitante.Text
        .txtUFSolicitante.Text = cmbUF.Text
    End With
    Me.Hide
    SEGP0794_6.Show
End Sub

Private Sub cmdPesquisarCEP_Click()
Dim SQL As String
Dim rs As ADODB.Recordset

Dim i As Integer
Dim municipio_id As String
Dim Municipio As String
Dim UF As String
Dim chvlocal_dne As String
Dim municipio_descricao As String


 If txtCEPSolicitante.Text <> "" Then
     Me.MousePointer = vbHourglass
     
     'Obtem o endere�o, de acordo com o CEP
     SQL = ""
     SQL = SQL & " SELECT [Sigla da UF] UF, [Nome oficial da Localidade] municipio, [Chave da Localidade no DNE] chave_correio,  "
     SQL = SQL & "   [Tipo Oficial do Logradouro] tp_logra, [Nome Oficial do Logradouro] logra,"
     SQL = SQL & "      [CEP do Logradouro] CEP, [Bairro inicial do Logradouro] bairro"
     SQL = SQL & " FROM dado_basico_db..DNE_GU_LOGRADOUROS with (nolock) "
     SQL = SQL & " WHERE [CEP do Logradouro] = '" & Replace(txtCEPSolicitante.Text, "-", "") & "'"
     
     Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription, _
                          SQL, _
                          True)
     If Not rs.EOF Then
        txtEnderecoSolicitante.Text = Trim(rs!tp_logra) & " " & Trim(rs!logra)
        If AchouBase(Trim(rs!Municipio), Trim(rs!UF), municipio_descricao) Then
            cmbMunicipio.Text = Trim(municipio_descricao)
        Else
            municipio_id = InsereMunicipio(rs!Municipio, rs!UF, rs!chave_correio)
            MontacomboMunicipio
            cmbMunicipio.Text = Trim(rs!Municipio)
        End If
        txtBairroSolicitante.Text = IIf(IsNull(rs!Bairro), "", Trim(rs!Bairro))
        cmbUF.Text = rs!UF
        Municipio = rs!Municipio
        UF = rs!UF
        chvlocal_dne = rs!chave_correio
        
        txtNomeSolicitante.SetFocus
        
        'Verifica se existe na municpio_tb
        SQL = ""
        SQL = SQL & " SELECT municipio_id, estado, nome, tp_pessoa_id, nome, municipio_ibge_id, "
        SQL = SQL & "        municipio_BACEN_id, municipio_BACEN_dv, chvlocal_dne"
        SQL = SQL & " FROM municipio_tb with (nolock)"
        
                'Raimundo - GPTI - 19/02/2009
        'Flow 768836
        SQL = SQL & " WHERE (nome = '" & Replace(Trim(Municipio), "'", "`") & "' "
        'Raimundo - Fim
                
        SQL = SQL & " AND estado = '" & Trim(UF) & "') "
        SQL = SQL & " OR (chvlocal_dne = '" & chvlocal_dne & "')"
        
        Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription, _
                          SQL, _
                          True)
        If Not rs.EOF Then
            
            'Caso n�o tenha o ID da base dos Correios
            If IsNull(rs!chvlocal_dne) Then
               
                SQL = ""
                SQL = SQL & "EXEC municipio_spu " & rs!municipio_id & "," & rs!tp_pessoa_id & ","
                SQL = SQL & "'" & Trim(rs!Nome) & "','" & Trim(rs!estado) & "','" & cUserName & "',"
                SQL = SQL & IIf(IsNull(rs!municipio_ibge_id), "null", rs!municipio_ibge_id) & "," & IIf(IsNull(rs!municipio_bacen_id), "null", rs!municipio_bacen_id) & ","
                SQL = SQL & IIf(IsNull(rs!municipio_bacen_dv), "null", rs!municipio_bacen_dv) & ", '" & Format(chvlocal_dne, "00000000") & "'"
                
                Call ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription, _
                          SQL, _
                          False)
            
            End If
            
        Else
            'Caso n�o exista em municipio_tb
            municipio_id = InsereMunicipio(Municipio, UF, chvlocal_dne)
            MontacomboMunicipio
            cmbMunicipio.Text = Municipio
        End If
        
        ' Flow 203212 - Marcelo - 30/01/2007
        For i = 0 To cmbMunicipio.ListCount - 1
            If cmbMunicipio.List(i) = Trim(Municipio) Then
                cmbMunicipio.ListIndex = i
                If cmbUF.Text = Trim(UF) Then
                    Exit For
                End If
            End If
        Next
        
     End If
     Me.MousePointer = vbDefault
 End If
 
End Sub

Private Function AchouBase(Municipio As String, UF As String, ByRef municipio_descricao As String) As Boolean
Dim SQL As String
Dim rs As ADODB.Recordset

    SQL = ""
    SQL = SQL & " SELECT nome "
    SQL = SQL & " FROM municipio_tb with (nolock)"
                
   'Raimundo - GPTI - 19/02/2009
   'Flow 768836
    SQL = SQL & " WHERE rtrim(nome) = '" & Replace(Trim(Municipio), "'", "`") & "' "
   'Raimundo - fim
        
    SQL = SQL & " AND rtrim(estado) = '" & Trim(UF) & "'"
        
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                      glAmbiente_id, _
                      App.Title, _
                      App.FileDescription, _
                      SQL, _
                      True)
    If Not rs.EOF Then
        AchouBase = True
        municipio_descricao = rs!Nome
    Else
        AchouBase = False
    End If
    
    rs.Close
    
End Function
Private Function InsereMunicipio(Municipio As String, UF As String, chvlocal_dne As String) As Integer
Dim SQL As String
Dim rs As ADODB.Recordset
Dim municipio_id As Integer

    'Comentado devido erro de l�gica
    '****************************************************************
    '*  Desenvolvido por:   GPTI                                    *
    '*  Programado por:     Thiago Andre                            *
    '*  Data:               23/09/2008                              *
    '*  Projeto:    Flow 548040 - Incluir cidade no SEGBR           *
    '****************************************************************

'        SQL = ""
'        SQL = SQL & "SELECT isnull(max(municipio_id),0) ultimo "
'        SQL = SQL & "FROM municipio_tb with(nolock)"
'        SQL = SQL & "WHERE estado = '" & UF & "'"
'        SQL = SQL & "AND municipio_id <> 999"
'
'        Set rs = ExecutarSQL(gsSIGLASISTEMA, _
'                  glAmbiente_id, _
'                  App.Title, _
'                  App.FileDescription, _
'                  SQL, _
'                  True)
'        If Not rs.EOF Then
'            municipio_id = CLng(rs!ultimo) + 1
'            If Len(CStr(municipio_id)) > 3 Then
'                municipio_id = 999
'            End If
'        Else
'            municipio_id = 1
'        End If

        SQL = ""
        SQL = SQL & "SET NOCOUNT ON " & vbNewLine
        SQL = SQL & "DECLARE @COUNT INT " & vbNewLine
        SQL = SQL & "DECLARE @NUM NUMERIC(3,0) " & vbNewLine
        SQL = SQL & "SET @COUNT = 1 " & vbNewLine
        SQL = SQL & "WHILE @COUNT < 1000 " & vbNewLine
        SQL = SQL & " BEGIN " & vbNewLine
        SQL = SQL & "  SET @NUM = NULL " & vbNewLine
        SQL = SQL & "  SELECT @NUM = MUNICIPIO_ID FROM MUNICIPIO_TB with (nolock) WHERE MUNICIPIO_ID = @COUNT AND ESTADO = '" & UF & "'" & vbNewLine
        SQL = SQL & "  IF @NUM IS NULL " & vbNewLine
        SQL = SQL & "   BEGIN " & vbNewLine
        SQL = SQL & "    SET @NUM = @COUNT " & vbNewLine
        SQL = SQL & "    BREAK " & vbNewLine
        SQL = SQL & "   END " & vbNewLine
        SQL = SQL & "  SET @COUNT = @COUNT + 1 " & vbNewLine
        SQL = SQL & " END " & vbNewLine
        SQL = SQL & "SELECT @NUM " & vbNewLine

        Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                  glAmbiente_id, _
                  App.Title, _
                  App.FileDescription, _
                  SQL, _
                  True)
        If Not rs.EOF Then
            municipio_id = CInt(rs(0))
            
            If municipio_id = 999 Then
               MsgBox "N�o � mais poss�vel adicionar munic�pios.", vbInformation + vbOKOnly, "Aten��o"
               InsereMunicipio = 1
               Exit Function
            End If
        Else
            municipio_id = 1
        End If

        SQL = ""
        SQL = SQL & "EXEC municipio_spi " & municipio_id & "," & 1 & ","
        SQL = SQL & "'" & Trim(Municipio) & "','" & UF & "','" & cUserName & "',"
        SQL = SQL & "null" & "," & "null" & ","
        SQL = SQL & "null" & ", '" & Format(chvlocal_dne, "00000000") & "'"
        
        Call ExecutarSQL(gsSIGLASISTEMA, _
                  glAmbiente_id, _
                  App.Title, _
                  App.FileDescription, _
                  SQL, _
                  False)
                  
        InsereMunicipio = municipio_id
End Function

'marreta auto preencher
Private Sub cmdTeste_Click()
    txtNomeSolicitante.Text = "Teste"
    txtEmailSolicitante.Text = "teste@ntendencia.com.br"
    txtCEPSolicitante.Text = "25870000"
    txtBairroSolicitante.Text = "bairro"
    txtEnderecoSolicitante.Text = "rua a"
    cmbGrauParentesco.ListIndex = 1
    cmbMunicipio.ListIndex = 1
    mskCpfSolicitante.Text = "08631048780"
    mskDDD(0).Text = "11"
    mskTelefone(0).Text = "12345-1234"
    cmbTipoTelefone(0).ListIndex = 1
    cmdContinuar.SetFocus
End Sub

Private Sub Form_Activate()
    txtCEPSolicitante.SetFocus
End Sub

Private Sub Form_Load()
Dim Index As Integer

For Index = 0 To 4
    With cmbTipoTelefone(Index)
        .Clear
        .AddItem "Residencial"
        .ItemData(.NewIndex) = 1
        .AddItem "Comercial"
        .ItemData(.NewIndex) = 2
        .AddItem "Celular"
        .ItemData(.NewIndex) = 3
        .AddItem "Fax"
        .ItemData(.NewIndex) = 4
        .AddItem "Recado"
        .ItemData(.NewIndex) = 5
    End With
Next Index
Call MontacomboMunicipio
Call MontacomboUF
Call MontacomboGrauParentesco

End Sub

Private Sub MontacomboMunicipio()
    Dim SQL As String
    Dim rs As ADODB.Recordset
    
    SQL = ""
    SQL = SQL & " select   municipio_id, estado, nome"
    SQL = SQL & " from     municipio_tb with (nolock)"
    SQL = SQL & " order by  nome"
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)
    
    With cmbMunicipio
        .Clear
        
        If Not rs.EOF Then
           While Not rs.EOF
                 .AddItem Trim(rs(2))
                 .ItemData(.NewIndex) = rs(0)
                 rs.MoveNext
           Wend
        End If
        rs.Close
    End With

End Sub


Private Sub MontacomboGrauParentesco()
    Dim SQL As String
    Dim rst_Grau As ADODB.Recordset
    
    SQL = ""
    SQL = SQL & " select    distinct tp_componente_id , nome"
    SQL = SQL & " from      tp_componente_tb with (nolock)"
    SQL = SQL & " order by  nome"
    
    Set rst_Grau = ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, True)
    
    With cmbGrauParentesco
        .Clear
        
        If Not rst_Grau.EOF Then
           While Not rst_Grau.EOF
                 .AddItem rst_Grau(1)
                 .ItemData(.NewIndex) = rst_Grau(0)
                 rst_Grau.MoveNext
           Wend
        End If
        
        rst_Grau.Close
    End With

End Sub

Private Sub MontacomboUF()
    Dim SQL As String
    Dim rs As ADODB.Recordset
    
    SQL = ""
    SQL = SQL & " select   distinct(estado)"
    SQL = SQL & " from     municipio_tb with (nolock)"
    SQL = SQL & " order by estado"
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)
    
    With cmbUF
        .Clear
        
        If Not rs.EOF Then
           While Not rs.EOF
                 .AddItem rs(0)
                 rs.MoveNext
           Wend
        End If
        rs.Close
    End With

End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub cmdVoltar_Click()
    
If propostasAvisadas = "" And propostasReanalise = "" Then
    Me.Hide
    SEGP0794_9.Show
ElseIf propostasReanalise <> "" Then
    Me.Hide
    SEGP0794_4_1.Show
Else
    Me.Hide
    SEGP0794_4.Show
End If

End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Sair Me
End Sub

Private Sub mskDDD_LostFocus(Index As Integer)
    If mskDDD(Index).Text <> "__" And Len(mskDDD(Index).Text) = 2 Then
        mskTelefone(Index).Enabled = True
        txtRamal(Index).Enabled = True
        cmbTipoTelefone(Index).Enabled = True
        mskTelefone(Index).SetFocus
    Else
        mskTelefone(Index).Enabled = False
        txtRamal(Index).Enabled = False
        cmbTipoTelefone(Index).Enabled = False
    End If
End Sub

Private Sub mskTelefone_LostFocus(Index As Integer)
    '30/05/2012 Nova - Mauro Vianna: Telefone 9 d�gitos
    If Right(mskTelefone(Index).Text, 1) = "_" And Right(mskTelefone(Index).Text, 2) <> "__" Then
        mskTelefone(Index).Text = " " & Left(mskTelefone(Index).Text, 4) + "-" + Mid(mskTelefone(Index).Text, 5, 1) + Mid(mskTelefone(Index).Text, 7, 3)
    End If
End Sub

Private Sub txtBairroSolicitante_LostFocus()
    If txtBairroSolicitante.Text <> "" Then
        txtBairroSolicitante.Text = UCase(txtBairroSolicitante.Text)
    End If
End Sub

Private Sub txtCEPSolicitante_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 And txtCEPSolicitante.Text <> "" Then
        SendKeys "{TAB}"
  End If
End Sub

'07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante (inicio)
Private Sub mskCPFSolicitante_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 And mskCpfSolicitante.Text <> "" Then
        SendKeys "{TAB}"
  End If
End Sub
'07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante (fim)

Private Sub txtCEPSolicitante_LostFocus()
    If txtCEPSolicitante.Text <> "" Then
        If Val(txtCEPSolicitante.Text) <> 0 Then
            If Len(txtCEPSolicitante.Text) = 8 Then
                txtCEPSolicitante.Text = Format(txtCEPSolicitante.Text, "00000-000")
            Else
                If Mid(txtCEPSolicitante.Text, 6, 1) <> "-" Then
                    MsgBox "O CEP informado � inv�lido. Deve conter 8 n�meros.", vbInformation
                    txtCEPSolicitante.SetFocus
                End If
            End If
        Else
            MsgBox "O CEP informado � inv�lido. O valor n�o � num�rico.", vbInformation
            txtCEPSolicitante.SetFocus
        End If
    End If
End Sub

Private Sub txtEnderecoSolicitante_LostFocus()
    If txtEnderecoSolicitante.Text <> "" Then
        txtEnderecoSolicitante.Text = UCase(txtEnderecoSolicitante.Text)
    End If
End Sub

Private Sub txtNomeSolicitante_LostFocus()
    If txtNomeSolicitante.Text <> "" Then
        'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
        'txtNomeSolicitante.Text = UCase(txtNomeSolicitante.Text)
        txtNomeSolicitante.Text = MudaAspaSimples(UCase(txtNomeSolicitante.Text))
        'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : fim
    End If
End Sub


Private Function VerificaMunicipio(Municipio As String, UF As String) As Integer
Dim SQL As String
Dim rs As ADODB.Recordset
    
    SQL = ""
    SQL = SQL & " select   municipio_id"
    SQL = SQL & " from     municipio_tb with (nolock)"
    SQL = SQL & " where rtrim(nome) = '" & Trim(Municipio) & "'"
    SQL = SQL & " and rtrim(estado) = '" & Trim(UF) & "'"
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)
    If Not rs.EOF Then
        VerificaMunicipio = rs(0)
    Else
        VerificaMunicipio = 0
    End If
    
End Function
