VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form SEGP0794_9 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0794 - Aviso de Sinistro pela CA"
   ClientHeight    =   3270
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8895
   Icon            =   "SEGP0794_9.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3270
   ScaleWidth      =   8895
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   6090
      TabIndex        =   5
      Top             =   2685
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   7485
      TabIndex        =   7
      Top             =   2685
      Width           =   1275
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   4695
      TabIndex        =   6
      Top             =   2685
      Width           =   1275
   End
   Begin VB.Frame frmCabecalho 
      Height          =   2415
      Left            =   0
      TabIndex        =   8
      Top             =   120
      Width           =   8880
      Begin MSMask.MaskEdBox mskDtNascimento 
         Height          =   330
         Left            =   5625
         TabIndex        =   3
         Top             =   1215
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskCPF 
         Height          =   330
         Left            =   90
         TabIndex        =   1
         Top             =   1200
         Width           =   1770
         _ExtentX        =   3122
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   14
         Mask            =   "###.###.###-##"
         PromptChar      =   "_"
      End
      Begin VB.ComboBox cboSexo 
         Height          =   315
         ItemData        =   "SEGP0794_9.frx":0442
         Left            =   1935
         List            =   "SEGP0794_9.frx":044C
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1215
         Width           =   3360
      End
      Begin VB.TextBox txtNomeSegurado 
         Height          =   330
         Left            =   120
         MaxLength       =   60
         TabIndex        =   0
         Top             =   585
         Width           =   8580
      End
      Begin MSMask.MaskEdBox txtPropostaBB 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   1
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   4
         Top             =   1890
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   9
         PromptChar      =   "_"
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Proposta BB:"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   1680
         Width           =   930
      End
      Begin VB.Label Label4 
         Caption         =   "Dados do Segurado"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   0
         Width           =   1455
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Sexo:"
         Height          =   195
         Left            =   1935
         TabIndex        =   12
         Top             =   990
         Width           =   405
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Data de Nascimento:"
         Height          =   195
         Index           =   1
         Left            =   5625
         TabIndex        =   11
         Top             =   990
         Width           =   1500
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "C.P.F.:"
         Height          =   195
         Index           =   1
         Left            =   90
         TabIndex        =   10
         Top             =   990
         Width           =   480
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Nome:"
         Height          =   195
         Left            =   90
         TabIndex        =   9
         Top             =   360
         Width           =   465
      End
   End
End
Attribute VB_Name = "SEGP0794_9"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Para ser setado pelos forms que o chamam
Public origem As Integer



Private Sub cmdContinuar_Click()
    If txtNomeSegurado.Text = "" Then
        MsgBox "Indique o nome do Segurado.", vbCritical, App.Title
        txtNomeSegurado.SetFocus
        Exit Sub
    End If
    
    If mskCPF.Text = "___.___.___-__" Or Len(mskCPF.ClipText) <> 11 Then
        MsgBox "CPF do Segurado n�o informado ou inv�lido.", vbCritical, App.Title
         mskCPF.SetFocus
        Exit Sub
    End If
    
    If cboSexo.ListIndex = -1 Then
        MsgBox "Indique o sexo do Segurado.", vbCritical, App.Title
        cboSexo.SetFocus
        Exit Sub
    End If
    
    If mskDtNascimento.Text = "__/__/____" Then
        MsgBox "Indique a Data de nascimento do Segurado.", vbCritical, App.Title
         mskDtNascimento.SetFocus
        Exit Sub
    End If
       
    If Not IsDate(mskDtNascimento.Text) Then
         MsgBox "Data de nascimento do Segurado inv�lida.", vbCritical, App.Title
         mskDtNascimento.SetFocus
        Exit Sub
    End If
    
    If Not IsNumeric(txtPropostaBB.Text) Then
         MsgBox "Proposta BB inv�lida. O campo deve ser n�merico.", vbCritical, App.Title
         txtPropostaBB.SetFocus
        Exit Sub
    End If
    Me.Hide
    SEGP0794_5.Show
    
End Sub

Private Sub cmdVoltar_Click()

Select Case origem

Case 3 'Caso n�o tenha encontrado o sinistrado
    Me.Hide
    SEGP0794_3.Show

Case 4 'Caso n�o tenha proposta a avisar
    Me.Hide
    SEGP0794_4.Show
End Select

End Sub

Private Sub Form_Activate()
    txtNomeSegurado.SetFocus
End Sub

Private Sub txtNomeSegurado_LostFocus()
    If txtNomeSegurado.Text <> "" Then
        txtNomeSegurado.Text = UCase(txtNomeSegurado.Text)
    End If
End Sub



