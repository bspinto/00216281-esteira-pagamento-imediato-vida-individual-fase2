Attribute VB_Name = "AvisoSinistro"
Private Type SinistroAnterior
    apolice As Long
    sinistro_id As String
    evento_sinistro As String
    dt_ocorrencia As String
    situacao_sinistro As String
End Type

Public Type CoberturaAfetada
    tp_cobertura_id As String
    cobertura_bb As Integer
    Descricao As String
    val_estimado As String
    'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
    pagamento_imediato As Boolean
    'C00216281 - FIM
End Type

Public Type Aviso_Sinistro
    Sinistro As Object
    cobertura_afetada() As CoberturaAfetada
    cobertura_atingida As Boolean
End Type

Public Type estimativa
 val_inicial As Double
 perc_estimativa As Double
 utiliza_percentual_subevento As String
 Descricao As String
 atinge As Boolean
 cobertura_bb As Integer
End Type

Public Type Proposta_Avisada
   proposta_id As String
   sinistro_id As String
   produto As String
   produto_id As Integer
   ramo_id As Integer
   subramo_id As Integer
   'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
   Situacao As Integer
   'C00216281 - FIM
   
End Type

Public RespostaSaida As Boolean
Public RsProposta As ADODB.Recordset

Public TIPO_RAMO As Integer
Public Tabelao(400) As String

Public propostasAvisadas As String 'para as propostas avisadas
Public propostasReanalise As String 'para as propostas preanalise
Public clientesPropostas As String 'para o cliente da proposta

Private Sinistro() As SinistroAnterior 'Para o historico de sinistros do segurado

Public numCon As Long 'para a conex�o com transacao

Public Avisos_Sinistros() As Aviso_Sinistro
Public Proposta_Avisada() As Proposta_Avisada 'para as informa��es finais das propostas avisadas

Public Evento_Segbr_id As Long
Public Localizacao As String

Public Const Evento_bb_id = 1100 'evento da CA

Public Const entidade_id = 12
Public Const banco_id = 1
Public Const canal_comunicacao = 3
Public Const processa_reintegracao_is = "N"
Public Const situacao_aviso = "N"

Public cProdutoBBProtecao As String

Private strClienteIdSelecionado As String

Public Sub Sair(Formulario As Form)
    SEGP0794_sair.Show vbModal
    If RespostaSaida = True Then
        End
    Else
        Formulario.Show
    End If
End Sub

Public Sub Main()
 Dim rs As ADODB.Recordset
  Dim SQL As String
  'carolina.marinho
  Dim cod_empresa As String

    If Not Trata_Parametros(Command) Then
       'Call FinalizarAplicacao 'marreta
    End If

'cristovao.rodrigues teste
cUserName = "00216281_BSP" 'marreta
glAmbiente_id = 3 'marreta
    
    SQL = "Select dt_operacional, "
    SQL = SQL & "status_sistema,  "
    SQL = SQL & "dt_contabilizacao "
    SQL = SQL & "from seguros_db.dbo.parametro_geral_tb with (nolock) "
    
     Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    If Not rs.EOF Then
        Data_Sistema = Format(rs(0), "dd/mm/yyyy")
    End If
    
    'R�mulo Ribeiro
    'Data: 18/08/2010
    'Obtem o c�digo da seguradora
    Dim oSeguradoraSusep As Object
    
    Set oSeguradoraSusep = CreateObject("SEGL0022.cls00118")
    sCodSusep = oSeguradoraSusep.ObterCodSeguradoraSusep(gsSIGLASISTEMA, _
                                                         App.Title, _
                                                         App.FileDescription, _
                                                         glAmbiente_id)
                                                         
    'carolina.marinho
    'VERIFICAR SE ESTA RODANDO NO AMBIENTE RE
    If Not VerificaAmbiente Then
        SEGP0794_1.optTipoRamo(0).Enabled = False
    End If
    
    SEGP0794_1.Show
  
End Sub

Public Sub LimpaTudo()
Dim i As Long

 With SEGP0794_2
      .cboTipoPesquisa.ListIndex = -1
      .txtValorPesquisa.Text = ""
      .GrdResultadoPesquisa.Rows = 1
      .montacabecalhoGrid
      .GrdResultadoPesquisa.Rows = 2
      .GrdResultadoPesquisa.FixedRows = 1
 End With

 With SEGP0794_3
      .txtNomeSinistrado.Text = ""
      .mskCPF.Text = "___.___.___-__"
      .mskDtNascimento.Text = "__/__/____"
      .cboSexo.ListIndex = -1
      .mskDataOcorrencia.Text = "__/__/____"
      .cboEvento.ListIndex = -1
      .cboSubEvento.ListIndex = -1
      For i = 0 To 9
        .txtExigencia(i).Text = ""
      Next
      .mskHora = "__:__"
      .txtLocal = ""
 End With
  
 With SEGP0794_4_1
      For i = 0 To 4
        .txtMotivo(i).Text = ""
      Next i
 End With
 
 With SEGP0794_5
      .txtNomeSolicitante.Text = ""
      .txtEnderecoSolicitante.Text = ""
      .txtBairroSolicitante.Text = ""
      .txtCEPSolicitante.Text = ""
      .mskCpfSolicitante.Text = "___________" '07/11/2019 - (ntendencia) - Aviso de Sinistro WEB
      .txtEmailSolicitante.Text = ""
      .cmbGrauParentesco.ListIndex = -1
      .cmbMunicipio.ListIndex = -1
      .cmbUF.ListIndex = -1
      .municipio_id = 0
      For i = 0 To 4
        .mskDDD(i).Text = "__"
        ' Francisco H. Berrocal | Erro de Execu��o 302: INC000003919147
        '.mskTelefone(i).Text = "____-____"
        .mskTelefone(i).Text = "_____-____"
        .txtRamal(i).Text = ""
        .cmbTipoTelefone(i).ListIndex = -1
      Next i
 End With
 
 With SEGP0794_6
      .txtPesquisaAgencia.Text = ""
      .cmdPesquisarAgencia_Click
 End With

With SEGP0794_9
    .txtNomeSegurado.Text = ""
    .mskCPF.Text = "___.___.___-__"
    .mskDtNascimento.Text = "__/__/____"
    .cboSexo.ListIndex = -1
    .txtPropostaBB.Text = ""
End With

End Sub

Public Function getEstimativaGenerica(Evento_sinistro_id As Integer, _
                                      tp_componente_id As Integer, _
                                      dt_ocorrencia As String) As estimativa
Dim SQL As String
Dim rs As ADODB.Recordset
Dim ramo_id As Integer
Dim subramo_id As Long
    
     SQL = "select val_inicial, isnull(utiliza_percentual_subevento,'N') utiliza_percentual_subevento, nome, a.cobertura_bb"
     SQL = SQL & " from produto_estimativa_sinistro_tb a with (nolock), tp_cobertura_tb b with (nolock) "
     SQL = SQL & " where produto_id = 0 "
     SQL = SQL & " and a.tp_cobertura_id = b.tp_cobertura_id "
     SQL = SQL & " and a.tp_componente_id = " & tp_componente_id
     SQL = SQL & " and evento_sinistro_id = " & Evento_sinistro_id
     SQL = SQL & " and a.tp_cobertura_id = 1 "
     SQL = SQL & " and convert(char(8),dt_inicio_vigencia, 112) <= '" & dt_ocorrencia & "'"
     SQL = SQL & " and (convert(char(8), dt_fim_vigencia, 112) >= '" & dt_ocorrencia & "'"
     SQL = SQL & "     or dt_fim_vigencia is null)"

    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    
    If Not rs.EOF Then
         If Not IsNull(rs("val_inicial")) Then
             getEstimativaGenerica.val_inicial = CDbl(rs("val_inicial"))
         End If
         getEstimativaGenerica.Descricao = rs("NOME")
         getEstimativaGenerica.atinge = True
         getEstimativaGenerica.cobertura_bb = rs("cobertura_bb")
         getEstimativaGenerica.utiliza_percentual_subevento = rs!utiliza_percentual_subevento
    Else
        getEstimativaGenerica.atinge = False
    
    End If
    
                                  

End Function
Public Function getEstimativa(proposta As Long, _
                                tp_cobertura_id As Long, _
                                Evento_sinistro_id As Long, _
                                SubEvento_sinistro_id As Long, _
                                tp_componente_id As Integer, _
                                dtOcorrenciaSinistro As String) As estimativa
Dim SQL As String
Dim rs  As ADODB.Recordset
Dim rs1 As ADODB.Recordset
Dim ramo_id As Integer
Dim subramo_id As Long
Dim produto_id As Integer
Dim perc_estimativa_subevento As Double

    'Seleciona as informa��es da proposta
    SQL = ""
    SQL = SQL & " select a.ramo_id, b.subramo_id, b.produto_id "
    SQL = SQL & " from apolice_tb a with (nolock), proposta_tb b with (nolock)"
    SQL = SQL & " where a.proposta_id = b.proposta_id "
    SQL = SQL & " and b.proposta_id = " & proposta
    SQL = SQL & " union "
    SQL = SQL & " select a.ramo_id, b.subramo_id, b.produto_id"
    SQL = SQL & " from proposta_adesao_tb a with (nolock), proposta_tb b with (nolock)"
    SQL = SQL & " where a.proposta_id = b.proposta_id"
    SQL = SQL & " and b.proposta_id = " & proposta
                                  
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    If Not rs.EOF Then
        ramo_id = rs!ramo_id
        subramo_id = IIf(IsNull(rs!subramo_id), 0, rs!subramo_id)
        produto_id = rs!produto_id
    End If
    
    rs.Close
    
     SQL = "select val_inicial, perc_estimativa, isnull(utiliza_percentual_subevento,'N') utiliza_percentual_subevento, nome, a.cobertura_bb"
     SQL = SQL & " from produto_estimativa_sinistro_tb a with (nolock),"
     SQL = SQL & " tp_cobertura_tb b with (nolock)"
     SQL = SQL & " where produto_id = " & produto_id
     SQL = SQL & " and a.tp_cobertura_id = b.tp_cobertura_id"
     SQL = SQL & " and ramo_id = " & ramo_id
     SQL = SQL & " and subramo_id = " & subramo_id
     SQL = SQL & " and tp_componente_id = " & tp_componente_id
     SQL = SQL & " and evento_sinistro_id = " & Evento_sinistro_id
     SQL = SQL & " and a.tp_cobertura_id = " & tp_cobertura_id
     SQL = SQL & " and convert(char(8),dt_inicio_vigencia, 112) <= '" & dtOcorrenciaSinistro & "'"
     SQL = SQL & " and (convert(char(8), dt_fim_vigencia, 112) >= '" & dtOcorrenciaSinistro & "'"
     SQL = SQL & "     or dt_fim_vigencia is null)"
         SQL = SQL & " and a.situacao = 'A'" 'Carlos.Lima - Confitec RJ - Sala Sistemas Complementares - 12/12/2019

    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
                         
    
     If Not rs.EOF Then
         If rs!utiliza_percentual_subevento = "S" Then
            SQL = "SELECT isnull(perc_estimativa,0) perc_estimativa "
            SQL = SQL & " FROM  evento_subevento_sinistro_tb with (nolock)"
            SQL = SQL & " WHERE evento_sinistro_id = " & Evento_sinistro_id & " and "
            SQL = SQL & "       subevento_sinistro_id = " & SubEvento_sinistro_id

            Set rs1 = ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  SQL, _
                                  True)
                                 
            If Not rs1.EOF Then
               perc_estimativa_subevento = rs1!perc_estimativa
            Else
               perc_estimativa_subevento = 0
            End If
            rs1.Close
         End If
               
         If Not IsNull(rs!val_inicial) Then
             getEstimativa.val_inicial = CDbl(rs!val_inicial)
         End If
         
         If rs!utiliza_percentual_subevento = "S" Then
            getEstimativa.perc_estimativa = perc_estimativa_subevento
         Else
            If Not IsNull(rs!perc_estimativa) Then
               getEstimativa.perc_estimativa = CDbl(rs!perc_estimativa)
            End If
         End If
         
         getEstimativa.Descricao = rs!Nome
         getEstimativa.atinge = True
                ''INICIO - MU00416967 -  melhoria de aviso de sinistro'INICIO melhoria de aviso de sinistro
         getEstimativa.cobertura_bb = IIf(IsNull(rs!cobertura_bb), 0, rs!cobertura_bb)
                ''FIM - MU00416967 -  melhoria de aviso de sinistro
         getEstimativa.utiliza_percentual_subevento = rs!utiliza_percentual_subevento
    Else
        getEstimativa.atinge = False
    
    End If
    
    rs.Close
    

End Function
Public Function ObtemEstimativaAbertura(proposta As Long, _
                                tp_cobertura_id As Long, _
                                Evento_sinistro_id As Long, _
                                SubEvento_sinistro_id As Long, _
                                tp_componente_id As Integer, _
                                dtOcorrenciaSinistro As String, _
                                val_is As Double) As Currency

Dim SQL                   As String
Dim rs                    As ADODB.Recordset
Dim ramo_id               As Integer
Dim subramo_id            As Long
Dim produto_id            As Integer
Dim val_estimativa        As Currency
Dim Retorno               As Integer

Dim oEstimativa           As Object

    'Seleciona as informa��es da proposta
    SQL = ""
    SQL = SQL & " select a.ramo_id, b.subramo_id, b.produto_id "
    SQL = SQL & " from apolice_tb a with(nolock), proposta_tb b with (nolock)"
    SQL = SQL & " where a.proposta_id = b.proposta_id "
    SQL = SQL & " and b.proposta_id = " & proposta
    SQL = SQL & " union "
    SQL = SQL & " select a.ramo_id, b.subramo_id, b.produto_id"
    SQL = SQL & " from proposta_adesao_tb a with (nolock), proposta_tb b with (nolock)"
    SQL = SQL & " where a.proposta_id = b.proposta_id"
    SQL = SQL & " and b.proposta_id = " & proposta
                                  
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    If Not rs.EOF Then
        ramo_id = rs!ramo_id
        subramo_id = IIf(IsNull(rs!subramo_id), 0, rs!subramo_id)
        produto_id = rs!produto_id
    End If
    
    rs.Close
  

   Set oEstimativa = CreateObject("SEGL0144.cls00326")
   
oEstimativa.mvarSiglaSistema = gsSIGLASISTEMA
oEstimativa.mvarSiglaRecurso = App.Title
oEstimativa.mvarDescricaoRecurso = App.FileDescription
oEstimativa.mvarAmbienteId = glAmbiente_id

    
 ObtemEstimativaAbertura = oEstimativa.ObtemEstimativaAberturaVida(produto_id, _
                                                                   ramo_id, _
                                                                   subramo_id, _
                                                                   tp_cobertura_id, _
                                                                   Evento_sinistro_id, _
                                                                   SubEvento_sinistro_id, _
                                                                   tp_componente_id, _
                                                                   dtOcorrenciaSinistro, _
                                                                   val_is)


Set oEstimativa = Nothing
    

End Function
Public Function ObtemEstimativaReabertura(sinistro_id As String, tp_cobertura_id As Integer) As Currency
Dim oEstimativa           As Object

Set oEstimativa = CreateObject("SEGL0144.cls00326")
    
oEstimativa.mvarSiglaSistema = gsSIGLASISTEMA
oEstimativa.mvarSiglaRecurso = App.Title
oEstimativa.mvarDescricaoRecurso = App.FileDescription
oEstimativa.mvarAmbienteId = glAmbiente_id
    
ObtemEstimativaReabertura = oEstimativa.ObtemEstimativaReabertura(sinistro_id, _
                                                                  tp_cobertura_id)
Set oEstimativa = Nothing
    

End Function

Public Sub GravarCoberturas(evento_id As String, coberturas() As CoberturaAfetada)
Dim i As Long
Dim SQL As String
Dim rc As ADODB.Recordset

    For i = 1 To UBound(coberturas)
        SQL = "exec evento_SEGBR_sinistro_cobertura_spi "
        SQL = SQL & evento_id & ","
        SQL = SQL & coberturas(i).tp_cobertura_id & ","
        SQL = SQL & "'" & IIf(Not IsNull(coberturas(i).val_estimado), TrocaVirgulaPorPonto(coberturas(i).val_estimado), "") & "', "
        SQL = SQL & "'" & cUserName & "',"
        SQL = SQL & coberturas(i).cobertura_bb & ", "
        'INICIO - MU00416967 -  melhoria de aviso de sinistro -- IM00330639  -- 07052018
        SQL = SQL & "'" & coberturas(i).Descricao & "', "
        SQL = SQL & "'" & IIf(Not IsNull(coberturas(i).val_estimado), TrocaVirgulaPorPonto(coberturas(i).val_estimado), "") & "'"
        
        
                
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, _
                            numCon, _
                            False)
    Next i
End Sub

Public Function descricao_tp_telefone(tp_telefone As String) As String

    Select Case tp_telefone
        Case "1"
            descricao_tp_telefone = "Residencial"
        Case "2"
            descricao_tp_telefone = "Comercial"
        Case "3"
            descricao_tp_telefone = "Celular"
        Case "4"
            descricao_tp_telefone = "Fax"
        Case "5"
            descricao_tp_telefone = "Recado"
        Case Else
            descricao_tp_telefone = ""
        End Select

End Function

Public Sub GravarTabelao(sin As Sinistro, coberturas() As CoberturaAfetada, cob_atingida As Boolean)
Dim i As Long
Dim SQL As String
Dim qtd_cob As Long
Dim maxTabelao As Integer
Dim rc As ADODB.Recordset
Dim rsDoc As ADODB.Recordset
Dim rsSinistro_BB As ADODB.Recordset
Dim sSinistro_bb As String

On Error GoTo Trata_Erro


    If sin.Ind_reanalise = "S" Then
    
        Tabelao(0) = "AVISO DE SINISTRO"
        Tabelao(1) = "-----------------"
        Tabelao(2) = ""
        Tabelao(3) = "SOLICITA��O DE REAN�LISE"
        Tabelao(4) = "--------------"
        Tabelao(5) = ""
        Tabelao(6) = "Motivo(s) apresentado(s):"
        For i = 7 To 11
            Tabelao(i) = SEGP0794_4_1.txtMotivo(i - 7).Text
        Next i
        
        Tabelao(12) = "Observa��es:"
        For i = 13 To 22
            Tabelao(i) = SEGP0794_3.txtExigencia(i - 13).Text
        Next i

        maxTabelao = 22
        
        
    Else
    
        Tabelao(0) = "AVISO DE SINISTRO"
        Tabelao(1) = "-----------------"
        Tabelao(2) = ""
        Tabelao(3) = "DADOS DO AVISO"
        Tabelao(4) = "--------------"
        Tabelao(5) = ""
        Tabelao(6) = "N�mero do Aviso Alian�a: " & sin.sinistro_id
        
        ' Demanda 4354230 - Incluir Sinistro_BB
        ' ccapeli - Confitec - 14/01/2011
        sSinistro_bb = " "
        vSql = "SELECT sinistro_bb FROM sinistro_bb_tb with (NOLOCK) WHERE sinistro_id = " & sin.sinistro_id & " and encerrado='n' "
        Set rsSinistro_BB = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            True)
        If Not rsSinistro_BB.EOF Then
            sSinistro_bb = rsSinistro_BB!Sinistro_bb
        End If
        rsSinistro_BB.Close
        
        Tabelao(7) = "N�mero do Aviso BB: " & sSinistro_bb
        Tabelao(8) = "Data da comunica��o: " & Data_Sistema
        Tabelao(9) = "Aviso pela Central de Atendimento"
        Tabelao(10) = "Atendente : " & BuscaNome(gsCPF)
        Tabelao(11) = ""
        Tabelao(12) = "DADOS DO SEGURADO"
        'eduardo.amaral (Nova Consultoria) 13/05/2013 Demanda: 16794492 - BB Microsseguro - Mudan�a de Escopo Sinistro
        If sin.produto_id = 1218 And SEGP0794_4.FlagMicrosseguro Then
            Tabelao(13) = "Nome do Sinistrado: " & SEGP0794_4_aviso_micro_seguro.txtNomeSinistrado
        Else
            '------------------------------------------------------------------
            'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : INICIO
            'Tabelao(13) = "Nome do Sinistrado: " & sin.SinistradoNomeCliente
            Tabelao(13) = "Nome do Sinistrado: " & MudaAspaSimples(sin.SinistradoNomeCliente)
            'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : fim
            '------------------------------------------------------------------
        End If
        
        'sergio.so - 15/12/2010 - Corre��o para tratamento de Titular / Conjuge
        'Tabelao(13) = "Tipo Sinistrado: " & IIf(sin.SeguradoComponente = 1, "Titular", "Conjuge")
        'Tabelao(14) = "CPF/CGC: " & sin.SeguradoCPF_CNPJ
        'sergio.eo - 15/12/2010 - Corre��o para tratamento de Titular / Conjuge
        
        'sergio.sn - 04/02/2011 - Corre��o para tratamento de Titular / Conjuge
                
                
        'Joaquim Ma. 10.02.2011
        'N�o retirar a valida��o
        'eduardo.amaral (Nova Consultoria) 02/03/2012 Demanda: 12363945 - BB Seguro Vida Empresa FLEX
        'Altera��o realizada para usar apenas SeguradoComponente e n�o mais IndTpSinistrado
        'Tabelao(14) = "Tipo Sinistrado: " & IIf(sin.IndTpSinistrado = "T", "Titular", "Conjuge")
        If sin.produto_id = 1218 And SEGP0794_4.FlagMicrosseguro Then
            Tabelao(14) = "Tipo Sinistrado: " & SEGP0794_4_aviso_micro_seguro.cboSinistrado
        ElseIf CInt(sin.SeguradoComponente) <> 1 And CInt(sin.SeguradoComponente) <> 3 Then
            Tabelao(14) = "Tipo Sinistrado: " & sin.NomeTipoComponente
        Else
            Tabelao(14) = "Tipo Sinistrado: " & IIf(CInt(sin.SeguradoComponente) = 1, "Titular", "Conjuge")
        End If
        'fim eduardo.amaral(Nova Consultoria)
        If sin.produto_id = 1218 And SEGP0794_4.FlagMicrosseguro Then
            Tabelao(15) = "CPF/CGC: " & SEGP0794_4_aviso_micro_seguro.mskCPF.ClipText
        Else
            Tabelao(15) = "CPF/CGC: " & sin.SinistradoCPF
        End If

        'eduardo.amaral (Nova Consultoria)

        'sergio.en - 04/02/2011 - Corre��o para tratamento de Titular / Conjuge
        
        Tabelao(16) = "C�digo do Produto AB: " & sin.produto_id
        Tabelao(17) = "Nome do Produto AB: " & sin.NomeProduto
        Tabelao(18) = "Proposta BB: " & sin.PropostaBB
        Tabelao(19) = "Ramo: " & sin.ramo_id & " Ap�lice: " & sin.Apolice_id
        Tabelao(20) = ""
        Tabelao(21) = "OCORR�NCIA"
        Tabelao(22) = "----------"
        Tabelao(23) = ""
        Tabelao(24) = "Data da ocorr�ncia: " & Right(sin.dt_ocorrencia, 2) & "/" & Mid(sin.dt_ocorrencia, 5, 2) & "/" & Left(sin.dt_ocorrencia, 4)
        Tabelao(25) = "Evento causador do sinistro: " & sin.Causa
        Tabelao(26) = "Sub Evento causador do sinistro: " & sin.SubCausa
        Tabelao(27) = "Observa��o da ocorr�ncia:"
        
        For i = 28 To 37
            Tabelao(i) = SEGP0794_3.txtExigencia(i - 28).Text
        Next i
        
        Tabelao(38) = ""
        If Not cob_atingida Then
            i = 38
        Else
            qtd_cob = UBound(coberturas)
            If qtd_cob > 0 Then
                Tabelao(39) = "Cobertura(s) Afetada(s):"
            
                For i = 40 To 39 + qtd_cob
                    Tabelao(i) = coberturas(i - 39).tp_cobertura_id & " - " & coberturas(i - 39).Descricao
                Next i
            End If
        End If
        
        Tabelao(i) = ""
        Tabelao(i + 1) = "DADOS DO SOLICITANTE"
        Tabelao(i + 2) = "--------------------"
        Tabelao(i + 3) = ""
        '------------------------------------------------------------------
        'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : INICIO
        'Tabelao(i + 4) = "Nome: " & sin.SolicitanteNome
        Tabelao(i + 4) = "Nome: " & MudaAspaSimples(sin.SolicitanteNome)
        'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : FIM
        '------------------------------------------------------------------
        Tabelao(i + 5) = "Endere�o: " & sin.SolicitanteEndereco
        Tabelao(i + 6) = "Bairro: " & sin.SolicitanteBairro
        Tabelao(i + 7) = "Munic�pio: " & sin.SolicitanteMunicipio
        Tabelao(i + 8) = "U.F.: " & sin.SolicitanteEstado
        Tabelao(i + 9) = "CPF.: " & sin.SolicitanteCPF
        Tabelao(i + 10) = "DDD: " & sin.ddd1 & _
                 " Telefone: " & sin.telefone1 & _
                 " Ramal: " & sin.ramal1 & _
                 " Telefone: " & descricao_tp_telefone(sin.tp_telefone1)
        Tabelao(i + 11) = "DDD: " & sin.ddd2 & _
                 " Telefone: " & sin.telefone2 & _
                 " Ramal: " & sin.ramal2 & _
                 " Telefone: " & descricao_tp_telefone(sin.tp_telefone2)
        Tabelao(i + 12) = "DDD: " & sin.ddd3 & _
                 " Telefone: " & sin.telefone3 & _
                 " Ramal: " & sin.ramal3 & _
                 " Telefone: " & descricao_tp_telefone(sin.tp_telefone3)
        Tabelao(i + 13) = "DDD: " & sin.ddd4 & _
                 " Telefone: " & sin.telefone4 & _
                 " Ramal: " & sin.ramal4 & _
                 " Telefone: " & descricao_tp_telefone(sin.tp_telefone4)
        Tabelao(i + 14) = "DDD: " & sin.ddd5 & _
                 " Telefone: " & sin.telefone5 & _
                 " Ramal: " & sin.ramal5 & _
                 " Telefone: " & descricao_tp_telefone(sin.tp_telefone5)
        Tabelao(i + 15) = ""
        Tabelao(i + 16) = "E-mail: " & sin.SolicitanteEmail
        Tabelao(i + 17) = ""
        
        Tabelao(i + 18) = "AG�NCIA DE CONTATO"
        Tabelao(i + 19) = "------------------"
        Tabelao(i + 20) = ""
        Tabelao(i + 21) = "C�digo da ag�ncia: " & sin.AgenciaAviso & " DV: " & sin.AgenciaDvAviso
        Tabelao(i + 22) = "Nome: " & sin.AgenciaNome
               
        maxTabelao = i + 22
        
    End If
     
    Tabelao(maxTabelao) = ""
    maxTabelao = maxTabelao + 1
    maxTabelao = maxTabelao + 1
    vSql = "SELECT * FROM evento_segbr_sinistro_documento_tb with  (NOLOCK) WHERE EVENTO_ID = " & sin.evento_id
    Set rsDoc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            True)
    If Not rsDoc.EOF Then
        Tabelao(maxTabelao) = "DOCUMENTOS SOLICITADOS"
        maxTabelao = maxTabelao + 1
        Tabelao(maxTabelao) = "--------------------------"
        maxTabelao = maxTabelao + 1
        Do While Not rsDoc.EOF
            'msg = msg & vbCrLf
            Tabelao(maxTabelao) = rsDoc!DOCUMENTO_ID & " " & rsDoc!Descricao
            maxTabelao = maxTabelao + 1
            rsDoc.MoveNext
        Loop
    End If
    rsDoc.Close
    
    
     'Informa��es de Sinistros Anteriores do segurado
    If temSinistrosAnteriores(sin.SinistradoCPF, 1, sin.SinistradoClienteId) Then
        Tabelao(maxTabelao) = ""
        maxTabelao = maxTabelao + 1
        Tabelao(maxTabelao) = "INFORMA��ES COMPLEMENTARES"
        maxTabelao = maxTabelao + 1
        Tabelao(maxTabelao) = "--------------------------"
        maxTabelao = maxTabelao + 1
        Tabelao(maxTabelao) = "Sinistros anteriores:"
        maxTabelao = maxTabelao + 1
        For i = 1 To UBound(Sinistro)
            Tabelao(maxTabelao) = "Sinistro AB: " & Sinistro(i).sinistro_id
            maxTabelao = maxTabelao + 1
            Tabelao(maxTabelao) = "Situa��o: " & Sinistro(i).situacao_sinistro
            maxTabelao = maxTabelao + 1
            Tabelao(maxTabelao) = "Evento Ocorr�ncia: " & Sinistro(i).evento_sinistro
            maxTabelao = maxTabelao + 1
            Tabelao(maxTabelao) = "Data Ocorr�ncia: " & Sinistro(i).dt_ocorrencia
            maxTabelao = maxTabelao + 1
            Tabelao(maxTabelao) = ""
            maxTabelao = maxTabelao + 1
        Next i
    End If
    
    Tabelao(maxTabelao) = "--------------------------"
    
    
    
    'Grava na evento_SEGBR_sinistro_detalhe_tb
    For i = 0 To maxTabelao
        SQL = "exec evento_SEGBR_sinistro_detalhe_spi "
        SQL = SQL & sin.evento_id & ","
        SQL = SQL & "'" & "ANOTACAO" & "',"
        SQL = SQL & i + 1 & ","
        SQL = SQL & "'" & Tabelao(i) & "',"
        SQL = SQL & "'" & cUserName & "'"
        
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, numCon, _
                            False)
    Next i
    
Exit Sub
Resume
Trata_Erro:
MsgBox ("erro :" & Err.Description)
End Sub

Private Function BuscaNome(gsCPF As String) As String
Dim SQL As String
Dim rs As ADODB.Recordset

    SQL = "select nome from segab_db..usuario_tb with  (nolock) where cpf = '" & gsCPF & "'"
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, True)
                            

    If Not rs.EOF Then
        BuscaNome = rs(0)
    Else
        BuscaNome = gsCPF
    End If
    


End Function

Private Function temSinistrosAnteriores(CPF_CNPJ As String, tp_ramo_id As Integer, Optional cliente_id As String) As Boolean
Dim SQL As String
Dim rs As ADODB.Recordset
Dim QTD As Long

 temSinistrosAnteriores = False
 
        SQL = "set nocount on exec sinistros_anteriores_sps '" & CPF_CNPJ & "'," & tp_ramo_id
        
        If cliente_id <> "" Then
           SQL = SQL & ",'" & cliente_id & "'"
        End If
 
        Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL)
                                
        ReDim Sinistro(0) As SinistroAnterior
        QTD = 0
        While Not rs.EOF
            temSinistrosAnteriores = True
            QTD = QTD + 1
            ReDim Preserve Sinistro(0 To QTD) As SinistroAnterior
            Sinistro(QTD).sinistro_id = IIf(Not IsNull(rs("Sinistro_id")), rs("Sinistro_id"), "")
            Sinistro(QTD).situacao_sinistro = IIf(Not IsNull(rs("situacao")), rs("situacao"), "")
            Sinistro(QTD).evento_sinistro = IIf(Not IsNull(rs("evento_sinistro_id")), rs("evento_sinistro_id") & " - " & rs("nome"), "")
            Sinistro(QTD).dt_ocorrencia = IIf(Not IsNull(rs("dt_ocorrencia_sinistro")), Format(rs("dt_ocorrencia_sinistro"), "dd/mm/yyyy"), "")
            Sinistro(QTD).apolice = IIf(Not IsNull(rs("apolice_id")), rs("apolice_id"), 0)
            rs.MoveNext
        Wend

End Function

Public Sub GravarDescricaoOcorrencia(sin As Sinistro)
Dim i As Long
Dim SQL As String
Dim rc As ADODB.Recordset
Dim Linha As Long
    
    '----Grava na evento_SEGBR_sinistro_descri��o_tb
    l = 0
    
    '---Se for reanalise, gravar o motivo da reabertura
    If sin.Ind_reanalise = "S" Then
        For i = 0 To 4
        
            If Trim(SEGP0794_4_1.txtMotivo(i).Text) <> "" Then
                SQL = "exec evento_SEGBR_sinistro_descricao_spi "
                SQL = SQL & sin.evento_id & ","
                SQL = SQL & l + 1 & ","
                SQL = SQL & "'" & SEGP0794_4_1.txtMotivo(i).Text & "',"
                SQL = SQL & "'" & cUserName & "'"
                
                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    SQL, numCon, _
                                    False)
                
               
                             
                l = l + 1
           End If
        Next i
    Else
    
    '---Se n�o for reanalise, gravar as observa��es
        For i = 0 To 9
        
            's� insere se tiver algo
            If Trim(SEGP0794_3.txtExigencia(i).Text) <> "" Then
                SQL = "exec evento_SEGBR_sinistro_descricao_spi "
                SQL = SQL & sin.evento_id & ","
                SQL = SQL & l + 1 & ","
                SQL = SQL & "'" & SEGP0794_3.txtExigencia(i).Text & "',"
                SQL = SQL & "'" & cUserName & "'"
                
                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    SQL, numCon, _
                                    False)
                l = l + 1
            End If
        Next i
        
        'Se n�o tem observa��es, colocar a descri��o do evento de sinsitro
        If l = 0 Then
            SQL = "exec evento_SEGBR_sinistro_descricao_spi "
            SQL = SQL & sin.evento_id & ","
            SQL = SQL & l + 1 & ","
            SQL = SQL & "'" & SEGP0794_3.cboEvento.Text & "',"
            SQL = SQL & "'" & cUserName & "'"
            
            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, numCon, _
                                False)
        End If
    End If
End Sub

Public Sub GravaGTREmissaoLayout(sin As Sinistro)

Dim SQL As String

    SQL = " exec     "
    SQL = SQL & sin.evento_id & ","
    SQL = SQL & sin.Evento_Segbr_id & ","
    SQL = SQL & "'" & cUserName & "'"
        
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, numCon, _
                                False)
    
End Sub


Public Function Busca_Numero_Sinistro(ByRef sin As Sinistro) As Boolean
Dim rc As ADODB.Recordset
Dim vSql As String

    Busca_Numero_Sinistro = False
    On Error GoTo Trata_Erro
    
   '******************************************************************************************'
   'Anderson Fiuza GPTI FLOW:2707850 29/01/2010
   'Substitui�ao da procedure chave_sinistro_spu pela smqs0006_spu chamada pela procedure abaixo,
   'pois na hora de retornar o numero do sinistro_id est� apresentando erro de overflow,
   'ocasionando o "pulo" de sequencial de sinistro.
   '******************************************************************************************'
    
    vSql = "SET nocount on "
    vSql = vSql & "EXEC seguros_db..SEGS8151_SPS '" & Format(Trim(Str(sin.ramo_id)), "00") & "'," & _
                        sin.Sucursal_id & "," & sin.Seguradora_id & ",'" & cUserName & "'"
    
    Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            True)
    If Not rc.EOF Then
        sin.sinistro_id = rc(0)
    End If
    Busca_Numero_Sinistro = True
    rc.Close
    Exit Function

Trata_Erro:
    TrataErroGeral "Busca N�mero Sinistro"
    Busca_Numero_Sinistro = False
    Exit Function
End Function

Public Function Busca_Numero_Remessa(ByRef sin As Sinistro) As Boolean
Dim rc As ADODB.Recordset
Dim vSql As String
Dim Chave As String
Dim num_remessa As Long

    Busca_Numero_Remessa = False
    On Error GoTo Trata_Erro
    
    Chave = "NUMERO REMESSA MQ"

    vSql = "SET nocount on "
    vSql = vSql & "EXEC seguros_db..chave_spu '" & Chave & "'," & _
                        "'MQ_USER','" & num_remessa & "'"
    
    Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            True)
    'LUPALUDETO - 02/12/2010
    If Not rc.EOF Then
        sin.NumRemessa = Format(rc(0), "0000000000000000")
        Busca_Numero_Remessa = True
    Else
        Busca_Numero_Remessa = False
    End If
    
    rc.Close
    Exit Function
Resume
Trata_Erro:
    TrataErroGeral "Busca Numero Remessa"
    Busca_Numero_Remessa = False
End Function

Public Sub Obtem_Evento_SEGBR()
Dim rc As ADODB.Recordset
Dim vSql As String

    
    On Error GoTo Trata_Erro
    
    'Preenche as vari�veis para o aviso de sinistro
    vSql = " select evento_SEGBR_id, prox_localizacao_processo " & _
           " from evento_segbr_tb with (nolock)" & _
           " where evento_bb_id = " & Evento_bb_id
    
    Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            True)
    If Not rc.EOF Then
         Evento_Segbr_id = rc(0)
         Localizacao = rc(1)
    End If
    rc.Close
    
    Exit Sub

Trata_Erro:
    TrataErroGeral "Obtem_Evento_SEGBR", "Aviso Sinistro"
    Exit Sub
End Sub

Public Property Get ClienteIdSelecionado() As Variant
    ClienteIdSelecionado = strClienteIdSelecionado
End Property

Public Property Let ClienteIdSelecionado(ByVal vNewValue As Variant)
    strClienteIdSelecionado = IIf(vNewValue = "", Null, vNewValue)
End Property


'carolina.marinho 06/09/2010 - Tratamento ABS
Public Function VerificaAmbiente() As Boolean
    Dim rc As ADODB.Recordset
    Dim rc2 As ADODB.Recordset
    Dim SQL As String
    
      SQL = "      SELECT cod_empresa" & vbNewLine
      SQL = SQL & "  FROM controle_sistema_db..empresa_ambiente_tb with (nolock)" & vbNewLine
      SQL = SQL & " WHERE ambiente_destino_id = " & glAmbiente_id & vbNewLine
      
      Set rc = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, _
                        True)
                        
    VerificaAmbiente = False
    
    If Not rc.EOF Then
        
        SQL = ""
        SQL = SQL & "SELECT DISTINCT cod_empresa" & vbNewLine
        SQL = SQL & "  From ramo_tb with  (nolock) " & vbNewLine
        SQL = SQL & " Where tp_ramo_id = 1" & vbNewLine
        SQL = SQL & "   AND ramo_id <> 85" & vbNewLine 'RAMO DESCONHECIDO
   
        Set rc2 = ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, _
                             True)
        
        If Not rc2.EOF Then
            If Trim(rc2(0)) = Trim(rc(0)) Then
                VerificaAmbiente = True
            End If
        End If
        
        rc2.Close
    End If
    rc.Close

      
End Function
