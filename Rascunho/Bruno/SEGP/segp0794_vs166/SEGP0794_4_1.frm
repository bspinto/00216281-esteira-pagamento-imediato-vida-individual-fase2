VERSION 5.00
Begin VB.Form SEGP0794_4_1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0794 - Aviso de Sinistro pela CA"
   ClientHeight    =   3000
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9090
   Icon            =   "SEGP0794_4_1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3000
   ScaleWidth      =   9090
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   4800
      TabIndex        =   5
      Top             =   2400
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   7590
      TabIndex        =   7
      Top             =   2400
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   6195
      TabIndex        =   6
      Top             =   2400
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   "Motivo de Rean�lise"
      Height          =   2055
      Left            =   0
      TabIndex        =   8
      Top             =   120
      Width           =   9045
      Begin VB.TextBox txtMotivo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   120
         MaxLength       =   70
         TabIndex        =   4
         Top             =   1470
         Width           =   8775
      End
      Begin VB.TextBox txtMotivo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   120
         MaxLength       =   70
         TabIndex        =   3
         Top             =   1200
         Width           =   8775
      End
      Begin VB.TextBox txtMotivo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   120
         MaxLength       =   70
         TabIndex        =   2
         Top             =   915
         Width           =   8775
      End
      Begin VB.TextBox txtMotivo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   120
         MaxLength       =   70
         TabIndex        =   1
         Top             =   630
         Width           =   8775
      End
      Begin VB.TextBox txtMotivo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   120
         MaxLength       =   70
         TabIndex        =   0
         Top             =   360
         Width           =   8775
      End
   End
End
Attribute VB_Name = "SEGP0794_4_1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmdContinuar_Click()
    If Trim(txtMotivo(0).Text) = "" Then
        MsgBox "� preciso especificar o motivo de rean�lise", vbCritical, "Aviso de Sinistro"
        txtMotivo(0).SetFocus
        Exit Sub
    End If
    'Gerson - CWI - Mensagem Vinda de SEGP0794_3.cmdContinuar_Click
    FlagCredito = MsgBox("Solicitante autoriza cr�dito em conta? ", vbQuestion + vbYesNo) = vbYes
    
    Me.Hide
    SEGP0794_5.Show
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Sair Me
End Sub

Private Sub cmdVoltar_Click()
    SEGP0794_4.Show
    Me.Hide
End Sub

Private Sub Form_Load()
    CentraFrm Me
End Sub

