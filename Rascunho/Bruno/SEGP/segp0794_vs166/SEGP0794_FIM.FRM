VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form SEGP0794_fim 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sinistro Avisado"
   ClientHeight    =   3960
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6720
   Icon            =   "SEGP0794_fim.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3960
   ScaleWidth      =   6720
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnPontosDeAlerta 
      Caption         =   "Pontos de Alerta"
      Height          =   420
      Left            =   3720
      TabIndex        =   5
      Top             =   3120
      Visible         =   0   'False
      Width           =   1365
   End
   Begin MSComctlLib.StatusBar stbQtd 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   3
      Top             =   3615
      Width           =   6720
      _ExtentX        =   11853
      _ExtentY        =   609
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Documentos ..."
      Height          =   420
      Left            =   5160
      TabIndex        =   2
      Top             =   3120
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Height          =   2415
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   6495
      Begin MSFlexGridLib.MSFlexGrid grdPropAvis 
         Height          =   2055
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   6255
         _ExtentX        =   11033
         _ExtentY        =   3625
         _Version        =   393216
         Cols            =   4
         FixedCols       =   2
         FormatString    =   $"SEGP0794_fim.frx":0442
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Aviso de Sinistro efetuado com sucesso!"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1200
      TabIndex        =   4
      Top             =   140
      Width           =   4335
   End
End
Attribute VB_Name = "SEGP0794_fim"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdOK_Click()
    
    SEGP0794_final2.Show vbModal
    LimpaTudo
    Me.Hide
    SEGP0794_1.Show
    
End Sub

Private Sub Form_Load()
    Dim SQLl As String
    Dim rsPntAlerta As ADODB.Recordset

    SQLl = "exec SEGURO_FRAUDE_DB..SPFS0247_SPS " & SEGP0794_7.sinistro_id


    Set rsPntAlerta = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQLl, True)
                                                        
    If Not rsPntAlerta.EOF Then
        'btnPontosDeAlerta.Visible = True
        
        'C�digo implementado por Paulo Fantin  a pedido de (Edgar J�nior CWI Software - CMMI N�vel 3 Gestor T�cnico - Unidade SP)
        'autorizado por RONALDO CASSEB FULANO...
        'data 05/12/2012 14:56
        'Autorizado por RONALDO CASSEB
        'Wesley Andrade ciente.
        btnPontosDeAlerta.Visible = True
        btnPontosDeAlerta.Enabled = True
        cmdOK.Enabled = False
    End If
End Sub

Private Sub btnPontosDeAlerta_Click()
            
    Me.Hide
    SEGP0794_7_1.Show

End Sub

'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
Public Sub VerificarPagtoImediato()
Dim bExibeMensagemPagtoImediato As Boolean

   If grdPropAvis.Rows > 1 Then
      bExibeMensagemPagtoImediato = True
   Else
      bExibeMensagemPagtoImediato = False
   End If
   
   For i = 1 To grdPropAvis.Rows - 1
       If grdPropAvis.TextMatrix(i, 0) <> "Deferido no Aviso" Then
          bExibeMensagemPagtoImediato = False
       End If
   Next i

   If bExibeMensagemPagtoImediato = True Then
      frmMsgPgtoImediato.Show vbModal, Me
   End If

End Sub
'C00216281 - FIM
