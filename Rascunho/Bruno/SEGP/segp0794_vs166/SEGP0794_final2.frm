VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{629074EB-AE57-4B76-8AF4-1B62557ED9A6}#1.0#0"; "GRIDDINAMICO.OCX"
Begin VB.Form SEGP0794_final2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Aviso de Sinistro - Documentos"
   ClientHeight    =   8025
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11415
   Icon            =   "SEGP0794_final2.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8025
   ScaleWidth      =   11415
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkEmail 
      Caption         =   "Enviar lista de documentos para Solicitante"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   600
      TabIndex        =   3
      Top             =   7200
      Width           =   4095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   465
      Left            =   9720
      TabIndex        =   0
      Top             =   7080
      Width           =   1455
   End
   Begin MSComctlLib.StatusBar stbQtd 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   2
      Top             =   7680
      Width           =   11415
      _ExtentX        =   20135
      _ExtentY        =   609
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin GridFrancisco.GridDinamico GridDinamico1 
      Height          =   5910
      Left            =   180
      TabIndex        =   4
      Top             =   120
      Width           =   11220
      _ExtentX        =   19791
      _ExtentY        =   10425
      BorderStyle     =   1
      AllowUserResizing=   3
      EditData        =   0   'False
      Highlight       =   1
      ShowTip         =   0   'False
      SortOnHeader    =   0   'False
      BackColor       =   -2147483643
      BackColorBkg    =   -2147483633
      BackColorFixed  =   -2147483633
      BackColorSel    =   -2147483635
      FixedCols       =   1
      FixedRows       =   1
      FocusRect       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      ForeColorFixed  =   -2147483630
      ForeColorSel    =   -2147483634
      GridColor       =   -2147483630
      GridColorFixed  =   12632256
      GridLine        =   1
      GridLinesFixed  =   2
      MousePointer    =   0
      Redraw          =   -1  'True
      Rows            =   2
      TextStyle       =   0
      TextStyleFixed  =   0
      Cols            =   2
      RowHeightMin    =   0
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      Caption         =   $"SEGP0794_final2.frx":0442
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   960
      Left            =   120
      TabIndex        =   1
      Top             =   6040
      Width           =   11220
   End
End
Attribute VB_Name = "SEGP0794_final2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim produto() As String
Dim ramo_id() As String
Dim subramo_id() As String
Dim Evento_sinistro_id As Integer
Dim SubEvento_sinistro_id As Long
Dim Conex As Long

Private Sub CarregaCabecalho()
    With GridDinamico1
        .Rows = 1
        .Cols = 4
        
        .TextMatrix(0, 0) = "QTD"
        .TextMatrix(0, 1) = "C�pia/Original        "
        .TextMatrix(0, 2) = "Documento" & Space(110)
        .TextMatrix(0, 3) = "Local para obter a documenta��o "
        .AutoFit H�brido
    
    End With
End Sub

Private Sub cmdNovoAviso_Click()
    Me.Hide
    SEGP0828_1.Show
End Sub

Private Sub cmdOK_Click()
    
    If chkEmail.value = 1 Then
        EnviaMensagem (SEGP0794_7.txtEmailSolicitante.Text)
    End If
    
    Unload Me
End Sub

Private Sub EnviaMensagem(destinatario As String)
Dim Msg As String
Dim SQL As String
Dim i As Long
On Error GoTo Erro

    Me.MousePointer = vbHourglass
    
    With Avisos_Sinistros(1).Sinistro
        Msg = ""
        Msg = Msg & "Prezado(a) " & .SolicitanteNome & "," & vbCrLf & vbCrLf
        Msg = Msg & "Segue a lista de documentos necess�ria para a an�lise do sinistro comunicado. " & vbCrLf
        Msg = Msg & vbCrLf
        Msg = Msg & "Estes documentos dever�o ser entregues � ag�ncia de contato, informada no momento do aviso:" & vbCrLf & vbCrLf
        Msg = Msg & "Ag�ncia de contato: " & .AgenciaAviso & "-" & .AgenciaDvAviso & vbCrLf
        Msg = Msg & "Nome: " & .AgenciaNome & vbCrLf
        Msg = Msg & "Endere�o: " & .AgenciaEndereco & vbCrLf
        Msg = Msg & vbCrLf
        Msg = Msg & "---------------------------------------------- " & vbCrLf
        Msg = Msg & "Documentos: " & vbCrLf
        Msg = Msg & vbCrLf
        
        For i = 1 To GridDinamico1.Rows - 1
            Msg = Msg & GridDinamico1.TextMatrix(i, 0) & " " & LCase(GridDinamico1.TextMatrix(i, 2)) & " (" & UCase(GridDinamico1.TextMatrix(i, 1)) & ") " & vbCrLf
            Msg = Msg & vbCrLf
        Next i
        Msg = Msg & "---------------------------------------------- " & vbCrLf
        Msg = Msg & "A SEGURADORA RESERVA-SE O DIREITO DE SOLICITAR QUALQUER DOCUMENTO QUE JULGAR " & vbCrLf
        Msg = Msg & "NECESS�RIO PARA A EFETIVA COMPROVA��O DA COBERTURA DO SINISTRO" & vbCrLf
        Msg = Msg & vbCrLf
        Msg = Msg & "Atenciosamente,"
        Msg = Msg & vbCrLf
        Msg = Msg & vbCrLf
        Msg = Msg & "Alian�a do Brasil" & vbCrLf
        Msg = Msg & "Central de Atendimento aos Clientes" & vbCrLf
        Msg = Msg & "Tel.: 0800 729 70000 " & vbCrLf
        Msg = Msg & "----------------------------------------------" & vbCrLf
        Msg = Msg & "Comunica��o Corporativa Alian�a do Brasil"
    End With
    
    '(INI) Demanda 18638396.
    If TIPO_RAMO = 1 And Val(SEGP0794_7.txtCodigoAgencia.Text) > 0 Then
        SQL = "set nocount on exec envia_email_sp '" & destinatario & _
              "; age" & Right("0000" & SEGP0794_7.txtCodigoAgencia.Text, 4) & "@bb.com.br" & _
              "', '" & "Documenta��o B�sica para o aviso de sinistro " & "', '" & Msg & "'"
    Else
    '(FIM) Demanda 18638396.
        SQL = "set nocount on exec envia_email_sp '" & destinatario & _
              "', '" & "Documenta��o B�sica para o aviso de sinistro " & "', '" & Msg & "'"
    End If
    
    Call ExecutarSQL(gsSIGLASISTEMA, _
                     glAmbiente_id, _
                     App.Title, _
                     App.FileDescription, _
                     SQL, False)
   Me.MousePointer = vbDefault
    
   Exit Sub
   
Erro:
   Me.MousePointer = vbDefault
  MsgBox "N�o foi poss�vel enviar o email.", vbCritical, "Envio de Email"
  Exit Sub
         
End Sub

Private Sub Form_Load()

Dim i As Long

    ReDim produto(0) As String
    ReDim ramo(0) As String
    ReDim subramo(0) As String
    
    
    If SEGP0794_7.txtEmailSolicitante.Text <> "" Then
    
        chkEmail.Enabled = True

    Else

        chkEmail.Enabled = False
        
    End If
    
    For i = 1 To UBound(Proposta_Avisada)
        ReDim Preserve produto(i) As String
        ReDim Preserve ramo_id(i) As String
        ReDim Preserve subramo_id(i) As String
        produto(i) = Proposta_Avisada(i).produto_id
        ramo_id(i) = Proposta_Avisada(i).ramo_id
        subramo_id(i) = Proposta_Avisada(i).subramo_id
    Next i
    Evento_sinistro_id = SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex)
    If SEGP0794_3.cboSubEvento.ListIndex = -1 Then
       SubEvento_sinistro_id = 0
    Else
       SubEvento_sinistro_id = SEGP0794_3.cboSubEvento.ItemData(SEGP0794_3.cboSubEvento.ListIndex)
    End If
    CarregaCabecalho
    CarregaGrid
End Sub


Private Sub CarregaGrid()
    On Error GoTo TrataErro
    Dim rs      As ADODB.Recordset
    Dim SQL As String
    Dim bBol As Boolean
    Dim i As Long
    
    Conex = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
    
    SQL = ""
    SQL = SQL & " CREATE TABLE #temp_documento " & vbCrLf
    SQL = SQL & "     (  tp_copia varchar(60), " & vbCrLf
    SQL = SQL & "       descricao varchar(1000), " & vbCrLf
    SQL = SQL & "        local varchar(1000))" & vbCrLf
    'Maur�cio(Stefanini), em 18/10/2005 - a pedido de Cl�ber (Stefanini) - FLOWBR 92939
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, Conex, False)
                             
    
    'sergio.sn - 15/10/2010 - Verifica se existe documento cadastrado - se n�o houver, cadastra
    
     Dim rsCarregaDocumentos As ADODB.Recordset
     
    For i = 1 To UBound(Proposta_Avisada)
            
            SQL = ""
            SQL = SQL & " SELECT evento_id " & vbNewLine
            SQL = SQL & "   FROM evento_segbr_sinistro_atual_tb with (nolock) " & vbNewLine
            SQL = SQL & "  WHERE sinistro_id = " & Proposta_Avisada(i).sinistro_id & vbNewLine
            SQL = SQL & "    AND evento_bb_id = 1100 " & vbNewLine
            SQL = SQL & "    AND proposta_id = " & Proposta_Avisada(i).proposta_id & vbNewLine
            SQL = SQL & "    AND ramo_id = " & CStr(Proposta_Avisada(i).ramo_id) & vbNewLine
            
            
             
            Set rsCarregaDocumentos = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         SQL, Conex, True)
                                         
                                         
            If Not rsCarregaDocumentos.EOF Then
            
                SQL = " SELECT * " & vbNewLine
                SQL = SQL & "  FROM evento_segbr_sinistro_documento_tb essd with (nolock) " & vbNewLine
                SQL = SQL & " WHERE essd.evento_id = " & CStr(rsCarregaDocumentos("evento_id")) & vbNewLine
                
                
                Set rsCarregaDocumentos = Nothing
                
                Set rsCarregaDocumentos = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                             glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             SQL, Conex, True)
                                             
                If rsCarregaDocumentos.EOF Then
                
                    IncluirDocumentos CInt(i), Proposta_Avisada(i).subramo_id
                
                End If
            
            End If
    
    Next i
    'sergio.en - 14/10/2010
    
                                 
                                 

    For i = 1 To UBound(produto)
    
       Call CarregaDocumentos(produto(i), ramo_id(i), subramo_id(i))
       
    Next i
    
    Set rs = VerificaDocumentos
    
    If rs.EOF Then
        Call CarregaDocumentos(0, 0, 0)
        rs.Close
        Set rs = VerificaDocumentos
    End If
    
    While Not rs.EOF
        GravaLinha rs("quantidade"), rs("c�pia"), rs("documento"), rs("local")
        rs.MoveNext
    Wend
    
    SQL = "drop table #temp_documento "
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, Conex, False)
    
    
    bBol = FecharConexao(Conex)
    
    
    If GridDinamico1.Rows > 1 Then
        GridDinamico1.FixedRows = 1
    End If
    
    Exit Sub
    
TrataErro:
     TrataErroGeral "Carrega Grid de Documentos", "Aviso de Sinistro"
     Exit Sub
End Sub


Private Function VerificaDocumentos() As ADODB.Recordset
Dim SQL As String

    SQL = ""
    SQL = SQL & " set nocount on select 'Quantidade' = count(*), " & vbCrLf
    SQL = SQL & " 'C�pia' = tp_copia, " & vbCrLf
    SQL = SQL & " 'Documento' = descricao, " & vbCrLf
    SQL = SQL & " 'Local' = local " & vbCrLf
    SQL = SQL & " from #temp_documento " & vbCrLf
    SQL = SQL & " group by tp_copia, descricao, local" & vbCrLf
    SQL = SQL & " order by descricao" & vbCrLf
    
    Set VerificaDocumentos = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, Conex, True)

End Function

'Private Sub CarregaDocumentos(produto As String, ramo As String, subramo As String)
'Dim sql As String
'    sql = ""
'    sql = sql & " insert INTO #temp_documento " & vbCrLf
'    sql = sql & " (tp_copia, descricao, local) " & vbCrLf
'    sql = sql & " SELECT     isnull(tc.nome, '-'),"
'    sql = sql & "           d.descricao,"
'    sql = sql & "           isnull(od.nome, '-')"
'    sql = sql & " FROM       seguros_db..documento_aviso_sinistro_tb pds"
'    sql = sql & " INNER JOIN documento_tb d"
'    sql = sql & " ON         pds.documento_id = d.documento_id"
'    sql = sql & " LEFT JOIN origem_documento_sinistro_tb  od"
'    sql = sql & " ON d.origem_documento_sinistro_id = od.origem_documento_sinistro_id"
'    sql = sql & " LEFT JOIN tp_copia_tb tc"
'    sql = sql & " ON pds.tp_copia = tc.tp_copia    "
'    sql = sql & " WHERE      pds.produto_id = " & produto & vbCrLf
'    sql = sql & " AND        pds.ramo_id = " & ramo & vbCrLf
'    sql = sql & " AND        pds.evento_sinistro_id = " & Evento_sinistro_id & vbCrLf
'
'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                   glAmbiente_id, _
'                   App.Title, _
'                   App.FileDescription, _
'                   sql, Conex, False)
'
'End Sub

    'sergio.sn - 13/10/2010
    ' ALTERA��O PARA FOR�AR A GRAVA��O DE DOCUMENTO Formul�rio "aviso de sinistro" assinado
Private Sub CarregaDocumentos(produto As String, ramo As String, subramo As String)

Dim SQL As String


Dim rsCarregaDocumentos As ADODB.Recordset


    SQL = ""
    SQL = SQL & " SELECT   DISTINCT   isnull(tc.nome, '-')," & vbNewLine
    SQL = SQL & "           d.descricao," & vbNewLine
    SQL = SQL & "           isnull(od.nome, '-')" & vbNewLine
    SQL = SQL & " FROM       seguros_db..documento_aviso_sinistro_tb pds with (nolock)" & vbNewLine
    SQL = SQL & " INNER JOIN documento_tb d with (nolock)" & vbNewLine
    SQL = SQL & " ON         pds.documento_id = d.documento_id" & vbNewLine
    SQL = SQL & " LEFT JOIN origem_documento_sinistro_tb  od with (nolock)" & vbNewLine
    SQL = SQL & " ON d.origem_documento_sinistro_id = od.origem_documento_sinistro_id" & vbNewLine
    SQL = SQL & " LEFT JOIN tp_copia_tb tc with (nolock)" & vbNewLine
    SQL = SQL & " ON pds.tp_copia = tc.tp_copia    " & vbNewLine
    SQL = SQL & " WHERE      pds.produto_id = " & produto & vbCrLf
    SQL = SQL & " AND        pds.ramo_id = " & ramo & vbCrLf
    SQL = SQL & " AND        pds.evento_sinistro_id = " & Evento_sinistro_id & vbCrLf
    
    Set rsCarregaDocumentos = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
                         
                         
    If Not rsCarregaDocumentos.EOF Then
    
    
        SQL = ""
        SQL = SQL & " insert INTO #temp_documento " & vbCrLf
        SQL = SQL & " (tp_copia, descricao, local) " & vbCrLf
        SQL = SQL & " SELECT     DISTINCT isnull(tc.nome, '-'),"
        SQL = SQL & "           d.descricao,"
        SQL = SQL & "           isnull(od.nome, '-')"
        SQL = SQL & " FROM       seguros_db..documento_aviso_sinistro_tb pds with (nolock)"
        SQL = SQL & " INNER JOIN documento_tb d with (nolock)"
        SQL = SQL & " ON         pds.documento_id = d.documento_id"
        SQL = SQL & " LEFT JOIN origem_documento_sinistro_tb  od with (nolock)"
        SQL = SQL & " ON d.origem_documento_sinistro_id = od.origem_documento_sinistro_id"
        SQL = SQL & " LEFT JOIN tp_copia_tb tc with (nolock)"
        SQL = SQL & " ON pds.tp_copia = tc.tp_copia    "
        SQL = SQL & " WHERE      pds.produto_id = " & produto & vbCrLf
        SQL = SQL & " AND        pds.ramo_id = " & ramo & vbCrLf
        SQL = SQL & " AND        pds.evento_sinistro_id = " & Evento_sinistro_id & vbCrLf
    
    
    Else
    
       
        'TIPO DE C�PIA: Original
        'FORMULARIO: Formul�rio "aviso de sinistro" assinado
        'NOME: Documento encaminhado pela Seguradora no kit de sinistro, que dever� ser datado e assinado
        '      pelo solicitante (na aus�ncia deste, o documento pode ser encontrado no Banco do Brasil)
    
        SQL = ""
        SQL = SQL & " insert INTO #temp_documento " & vbCrLf
        SQL = SQL & " (tp_copia, descricao, local) " & vbCrLf
        SQL = SQL & " SELECT  DISTINCT    isnull(tc.nome, '-'),"
        SQL = SQL & "           d.descricao,"
        SQL = SQL & "           isnull(od.nome, '-')"
        SQL = SQL & " FROM       seguros_db..documento_aviso_sinistro_tb pds with (nolock)"
        SQL = SQL & " INNER JOIN documento_tb d with (nolock)"
        SQL = SQL & " ON         pds.documento_id = d.documento_id"
        SQL = SQL & " LEFT JOIN origem_documento_sinistro_tb  od with (nolock)"
        SQL = SQL & " ON d.origem_documento_sinistro_id = od.origem_documento_sinistro_id"
        SQL = SQL & " LEFT JOIN tp_copia_tb tc with (nolock)"
        SQL = SQL & " ON pds.tp_copia = tc.tp_copia    "
        SQL = SQL & " WHERE      pds.produto_id = 0 "
        SQL = SQL & " AND        pds.ramo_id = 0 "
        SQL = SQL & " AND        d.documento_id = 1 "
        
        
        'TIPO DE C�PIA: Original
        'FORMULARIO: Formul�rio "aviso de sinistro" assinado
        'NOME: Documento encaminhado pela Seguradora no kit de sinistro, que dever� ser datado e assinado
        '      pelo solicitante (na aus�ncia deste, o documento pode ser encontrado no Banco do Brasil)
        
    
    End If

    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                   glAmbiente_id, _
                   App.Title, _
                   App.FileDescription, _
                   SQL, Conex, False)

End Sub

'sergio.en - 13/10/2010
    
Private Sub GravaLinha(quantidade As Integer, _
                        tipo_documento As String, _
                        Descricao As String, _
                        Locali As String)

For i = 1 To Len(Descricao) Step 70
    If i + 71 > Len(Descricao) Then
        Linha = Linha & Mid(Descricao, i, Len(Descricao) - i + 1)
    Else
        Linha = Linha & Mid(Descricao, i, 70) & Chr(13)
    End If
    
Next i
    With GridDinamico1
    .Rows = .Rows + 1
    .TextMatrix(.Rows - 1, 0) = quantidade
    .TextMatrix(.Rows - 1, 1) = tipo_documento
    .TextMatrix(.Rows - 1, 2) = Linha
    .RowHeight(.Rows - 1) = (i / 71) * 240
    .TextMatrix(.Rows - 1, 3) = Locali
    End With
End Sub

'sergio.sn - 15/10/2010
Private Sub IncluirDocumentos(Indice As Integer, intSubRamo As Integer)


    Dim rsPesquisa As ADODB.Recordset

    '#####################################################################
    'inclui documentos
    'Se n�o for cobertura generica, obtem os documentos de acordo com o produto, ramo, evento e cobertura
    Set AvisoRegras = CreateObject("SEGL0144.cls00326")

    AvisoRegras.mvarAmbienteId = glAmbiente_id
    AvisoRegras.mvarSiglaSistema = gsSIGLASISTEMA
    AvisoRegras.mvarSiglaRecurso = App.Title
    AvisoRegras.mvarDescricaoRecurso = App.FileDescription
    
    Set rsPesquisa = AvisoRegras.ObterDocumentos(Avisos_Sinistros(Indice).Sinistro.produto_id, _
                                               Avisos_Sinistros(Indice).Sinistro.ramo_id, _
                                               intSubRamo, _
                                                CStr(TIPO_RAMO), _
                                                CLng(Avisos_Sinistros(Indice).Sinistro.Causa_id), _
                                                sCoberturasJaInseridas, True)
                                               
    Set AvisoRegras = Nothing
    
    If Not rsPesquisa.EOF Then
           
        Do While Not rsPesquisa.EOF
            SQL = "exec evento_segbr_sinistro_documento_spi " & Avisos_Sinistros(Indice).Sinistro.evento_id & "," & _
                                                        rsPesquisa("documento_id") & ",'" & _
                                                        rsPesquisa("documento") & "','" & _
                                                        cUserName & "'"
                                                        
            Debug.Print SQL
            
            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, _
                            numCon, _
                            False)
            rsPesquisa.MoveNext
        Loop
    End If
    rsPesquisa.Close

End Sub

'sergio.en - 15/10/2010
