VERSION 5.00
Begin VB.Form frmMsgPgtoImediato 
   Caption         =   "Classifica��o do Sinistro"
   ClientHeight    =   2280
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5820
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2280
   ScaleWidth      =   5820
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnOK 
      Caption         =   "OK"
      Height          =   420
      Left            =   2370
      TabIndex        =   0
      Top             =   1650
      Width           =   1200
   End
   Begin VB.TextBox txtMsgPagtoImediato 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   135
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   1
      Top             =   135
      Width           =   5550
   End
End
Attribute VB_Name = "frmMsgPgtoImediato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
Private Sub btnOK_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
   
   txtMsgPagtoImediato.Locked = True
   txtMsgPagtoImediato.SetFocus
   
End Sub

'C00216281 - FIM
Private Sub Form_Load()
Dim sTexto As String

   sTexto = "Sinistro Pr�-Aprovado, necess�rio o envio" & vbCrLf & _
            "somente dos documentos" & vbCrLf & _
            "(Certid�o de �bito e documentos de benefici�rios)" & vbCrLf & _
            "atrav�s do site BBSeguros.com.br."
                                 
   txtMsgPagtoImediato.Text = sTexto
                                 
End Sub
