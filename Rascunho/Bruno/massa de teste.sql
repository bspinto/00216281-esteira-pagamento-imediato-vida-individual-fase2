--MASSA DE TESTE (CONSULTA 1)
DECLARE @produto_id AS INT = --11 -- OUROVIDA GARANTIA
                    --12 -- OURO VIDA ANTIGO
                    --14 -- OUROVIDA PRODUTOR RURAL
                    --121 -- OURO VIDA
                    --135 -- OURO VIDA 2000
                    --716 -- BB SEGURO VIDA MULHER
                    --718 -- BB SEGURO PRESTAMISTA CONSORCIO
                    721 -- BB SEGURO VIDA
                    --722 -- BB SEGURO VIDA AGRICULTURA FAMILIAR
                    --1174 -- BB SEGURO VIDA MULHER � PA
                    --1175 -- OURO VIDA 2000 � PA
                    --1196 -- OURO VIDA ESTILO
                    --1198 -- BB Seguro Cr�dito Protegido
                    --1205 -- BB Seguro Cr�dito Protegido (Estoque)
                    --1208 -- BB Seguro Cr�dito Protegido - Cart�o de Cr�dito
                    --1211 -- BB Seguro Prote��o Financeira - Cart�o de Cr�dito
                    --1217 -- BB Seguro Amparo Familiar
                    --1235 -- BB Seguro Vida Completo
                    --1236 -- BB Seguro Vida Mulher Mais
                    --1237 -- BB Seguro Vida Estilo

select TOP 100
a.proposta_id
,a.produto_id
,a.prop_cliente_id
,a.situacao
into #proposta_tb
FROM seguros_db.dbo.proposta_tb a
WHERE a.produto_id = @produto_id                    
  AND a.situacao <> 'T'
ORDER BY dt_contratacao DESC

SELECT DISTINCT 'C�digo' = c.cliente_id
	,'Nome' = c.nome
	,'CPF/CGC' = pf.cpf
	,'Sexo' = CASE 
		WHEN pf.sexo = 'F'
			THEN 'Feminino'
		WHEN pf.sexo = 'M'
			THEN 'Masculino'
		ELSE 'Indefinido'
		END
	,'Data de Nascimento' = convert(CHAR(10), pf.dt_nascimento, 103)
	,'Documento' = isnull(c.doc_identidade, '')
	,'Tipo Doc.' = isnull(c.tp_doc_identidade, '')
	,'Expedi��o Doc.' = isnull(c.exp_doc_identidade, '')
	,'Data Emiss�o Doc.' = isnull(convert(CHAR(10), c.dt_emissao_identidade, 103), '')
	,'Proposta' = p.proposta_id
	,'Produto' = p.produto_id
FROM cliente_tb c WITH (NOLOCK)
	,pessoa_fisica_tb pf WITH (NOLOCK)
	,#proposta_tb p WITH (NOLOCK)
WHERE c.cliente_id = pf.pf_cliente_id
	AND c.cliente_id = p.prop_cliente_id
	AND p.situacao <> 'T'

UNION

SELECT DISTINCT 'C�digo' = c.cliente_id
	,'Nome' = c.nome
	,'CPF/CGC' = pj.cgc
	,'Sexo' = 'Indefinido'
	,'Data de Nascimento' = ''
	,'Documento' = isnull(c.doc_identidade, '')
	,'Tipo Doc.' = isnull(c.tp_doc_identidade, '')
	,'Expedi��o Doc.' = isnull(c.exp_doc_identidade, '')
	,'Data Emiss�o Doc.' = isnull(convert(CHAR(10), c.dt_emissao_identidade, 103), '')
	,'Proposta' = p.proposta_id
	,'Produto' = p.produto_id
FROM cliente_tb c WITH (NOLOCK)
	,pessoa_juridica_tb pj WITH (NOLOCK)
	,#proposta_tb p WITH (NOLOCK)
WHERE C.Cliente_id = pj.pj_cliente_id
	AND c.cliente_id = p.prop_cliente_id
	AND p.situacao <> 'T'

