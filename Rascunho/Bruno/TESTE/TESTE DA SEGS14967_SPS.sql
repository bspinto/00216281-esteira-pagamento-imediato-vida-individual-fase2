WHILE @@TRANCOUNT > 0 ROLLBACK
WHILE @@TRANCOUNT = 0 BEGIN TRAN

SET NOCOUNT ON 

IF OBJECT_ID('tempdb..#classificacao_tmp') IS NOT NULL 
BEGIN DROP TABLE #classificacao_tmp END

CREATE TABLE #classificacao_tmp ( ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL
 ,indice INT
 ,situacao INT
 ,proposta_id INT
 ,evento_sinistro_id INT
 ,tp_cobertura_id INT
 ,produto_id INT
 ,ramo_id INT
 ,dt_ocorrencia smalldatetime
 ,evento_id INT
 ,sinistro_id NUMERIC(11)
 ,fluxo_validado INT
 ,pagamento_imediato INT
 ,deferido INT
 ,fase INT
 ,vigencia_dias INT
 ,regra INT
 ,tp_componente_id INT
 ,sub_grupo_id INT
 ,val_is NUMERIC(15,2)
 ,dt_inicio_vigencia SMALLDATETIME
 ,dt_fim_vigencia SMALLDATETIME
 ,exibir_popup INT
 ,tp_sinistro_parametro_id INT
 )

INSERT INTO #classificacao_tmp (indice, situacao, proposta_id, evento_sinistro_id, tp_cobertura_id, produto_id, ramo_id, dt_ocorrencia, evento_id, sinistro_id, fluxo_validado, pagamento_imediato, Deferido, fase, vigencia_dias, regra, tp_componente_id, sub_grupo_id, val_is, dt_inicio_vigencia ,dt_fim_vigencia ,exibir_popup, tp_sinistro_parametro_id )
Values ( 1,0,255660246,28,5,721,93, '20200812',46652036,93202000633,0 ,0 ,0 ,0 ,0 ,0 ,1,0,0,NULL,NULL,NULL,0)

IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14967_SPS '00216281_BSP'


--IF @@TRANCOUNT > 0 EXEC desenv_db.dbo.SEGS14967_SPS '00216281_BSP'


--CREATE PROCEDURE dbo.SEGS14967_SPS (
DECLARE @USUARIO VARCHAR(20)= 'TESTE'	
	SET NOCOUNT ON

	-- Declara��o e tratamento de variaveis (inicio)
	DECLARE @indice_contador AS INT
	,@ID_contador as INT
	,@indice_max AS INT
	,@indice AS INT
	,@ID_max AS INT
	,@ID INT
				

    -- Bloco de cria��o de tabelas temporarias (inicio)
		IF OBJECT_ID('tempdb..#evento_SEGBR_sinistro_detalhe_atual_tmp') IS NOT NULL
		BEGIN
			DROP TABLE #evento_SEGBR_sinistro_detalhe_atual_tmp
		END
		
		CREATE TABLE #evento_SEGBR_sinistro_detalhe_atual_tmp ( --tabela temporaria utilizada dentro de outras procedures chamadas.
			id INT identity(1, 1) PRIMARY KEY NOT NULL
			,evento_id INT null
			,tp_detalhe VARCHAR(10) null
			,seq_detalhe SMALLINT null
		  ,descricao VARCHAR(300) null
			,usuario VARCHAR(20) null
			,dt_inclusao SMALLDATETIME null
			)
			
		IF OBJECT_ID('TEMPDB..#DETALHAMENTO') IS NOT NULL
		BEGIN
			DROP TABLE #DETALHAMENTO
		END

		CREATE TABLE #DETALHAMENTO ( --tabela temporaria utilizada dentro de outras procedures chamadas.
			id INT IDENTITY(1, 1) NOT NULL
			,linha VARCHAR(60) NULL -- TAMANHO MAXIMO ACEITO NO DETALHAMENTO
			,regra INT NULL
			)
			
			
		-- (fim) Bloco de cria��o de tabelas temporarias 			
		-----------	
		-- alterando para n�o se aplica todos os registros
		UPDATE a
		SET a.deferido = 0
		,a.fluxo_validado = 0
		,a.pagamento_imediato = 0		
		FROM #classificacao_tmp a				
				
		--determinando a qual fase pertence o sinistro		
    UPDATE a
    SET a.fase = CASE s.nome
		                WHEN 'Esteira Pagamento Imediato � Vida Individual'	THEN 1
		                WHEN 'Esteira Pagamento Imediato � Vida Individual Fase 2' THEN 2
		                ELSE 0
		           END
		   ,a.tp_sinistro_parametro_id = s.tp_sinistro_parametro_id 
    FROM #classificacao_tmp a
    LEFT JOIN seguros_db.dbo.sinistro_parametro_chave_tb b WITH (NOLOCK)
	    ON a.produto_id = b.produto_id
      and a.ramo_id = b.ramo_id
      and a.evento_sinistro_id = b.evento_sinistro_id
      and a.tp_cobertura_id = b.tp_cobertura_id
	  LEFT JOIN seguros_db.dbo.tp_sinistro_parametro_tb s WITH(NOLOCK)
		    ON b.tp_sinistro_parametro_id = s.tp_sinistro_parametro_id
		WHERE b.dt_fim_vigencia IS NULL
		--marcando deferimento da fase 1
		
		---- deixando em standby
		--UPDATE a
		--SET a.deferido = 1
		--FROM #classificacao_tmp a
		--INNER JOIN seguros_db.dbo.evento_SEGBR_sinistro_detalhe_atual_tb b WITH (NOLOCK)
		--ON b.evento_id = a.evento_id		
		--WHERE a.fase = 1
		--AND b.descricao = 'SINISTRO FOI CLASSIFICADO COMO DEFERIDO' 
		
		UPDATE a
		SET a.deferido = b.resultado
		FROM #classificacao_tmp a
		INNER JOIN seguros_db.dbo.sinistro_classificacao_parametro_tb b WITH(NOLOCK)
		ON b.sinistro_id = a.sinistro_id
		WHERE a.fase = 1		
		    
		--validando fluxo para propostas da fase 2
    UPDATE a
    SET fluxo_validado = (
		    SELECT COUNT(s.sinistro_parametro_chave_id)
		    FROM seguros_db.dbo.proposta_tb p WITH (NOLOCK)
		    INNER JOIN seguros_db.dbo.sinistro_parametro_chave_tb s WITH (NOLOCK)
			    ON p.produto_id = s.produto_id
				    AND p.ramo_id = s.ramo_id
		    WHERE p.proposta_id = a.proposta_id
			    AND s.evento_sinistro_id = a.evento_sinistro_id
			    AND s.tp_cobertura_id = a.tp_cobertura_id
			    AND s.dt_fim_vigencia IS NULL
		    )
    FROM #classificacao_tmp a
    WHERE fase = 2
    
    UPDATE a 
      SET a.fluxo_validado = 1
      ,a.pagamento_imediato = 1
    FROM #classificacao_tmp a
    WHERE a.fase = 2
    AND a.fluxo_validado > 0
		      
		-- Classifica para o pagamento imediato
    SELECT @indice_contador = MIN(a.indice)
		  , @indice_max = MAX(a.indice)
		FROM #classificacao_tmp a 
		
		
	  WHILE @indice_contador <= @indice_max
		BEGIN		    
		  SET @indice = @indice_contador
		  		  		     
		  IF EXISTS(SELECT TOP 1 1 FROM #classificacao_tmp a WHERE a.indice = @indice AND fluxo_validado = 1 AND fase = 2 )
		  BEGIN
		    
        SELECT @ID_contador = MIN(a.ID)
		      , @ID_max = MAX(a.ID)
		    FROM #classificacao_tmp a 
		    WHERE a.indice = @indice
		    
		    WHILE @ID_contador <= @ID_max   
		    BEGIN
		      SET @ID = @ID_contador
		      
		      		      	        	        
	       EXEC desenv_db.dbo.SEGS14966_SPS @id = @id, @usuario = @usuario
	        
	       
		      SET @ID_contador = @ID_contador + 1 
		    END		    
		  END
		    

      SET @indice_contador = @indice_contador + 1 		
		END		
		
		----Aplicar a regra do acumulo (regra 6)
    EXEC seguros_db.dbo.SEGS14973_SPS 
				
		----Trata exibi��o do popup	
		EXEC seguros_db.dbo.SEGS14977_SPS
		
		----Gravar classifica��o do pagto imediato � vida fase 2
		EXEC seguros_db.dbo.SEGS14970_SPI @usuario = @usuario
												
	