WHILE @@TRANCOUNT > 0 ROLLBACK
SELECT @@TRANCOUNT
WHILE @@TRANCOUNT = 0 BEGIN TRAN
SELECT @@TRANCOUNT


IF OBJECT_ID('tempdb..#classificacao_tmp') IS NOT NULL 
BEGIN DROP TABLE #classificacao_tmp END

CREATE TABLE #classificacao_tmp ( ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL
 ,indice INT
 ,situacao INT
 ,proposta_id INT
 ,evento_sinistro_id INT
 ,tp_cobertura_id INT
 ,produto_id INT
 ,ramo_id INT
 ,dt_ocorrencia smalldatetime
 ,evento_id INT
 ,sinistro_id NUMERIC(11)
 ,fluxo_validado INT
 ,pagamento_imediato INT
 ,deferido INT
 ,fase INT
 ,vigencia_dias INT
 ,regra INT
 ,tp_componente_id INT
 ,sub_grupo_id INT
 ,val_is NUMERIC(15,2)
    --,proposta_bb INT
 ,dt_inicio_vigencia SMALLDATETIME
 ,dt_fim_vigencia SMALLDATETIME
 ,exibir_popup INT
 ,tp_sinistro_parametro_id INT
    --,sinistrado_cliente_id INT
 )


INSERT INTO #classificacao_tmp (indice, situacao, proposta_id, evento_sinistro_id, tp_cobertura_id, produto_id, ramo_id, dt_ocorrencia, evento_id, sinistro_id, fluxo_validado, pagamento_imediato, Deferido, fase, vigencia_dias, regra, tp_componente_id, sub_grupo_id, val_is, dt_inicio_vigencia ,dt_fim_vigencia ,exibir_popup, tp_sinistro_parametro_id )
Values ( 1,0,252178131,28,5,1198,77, '20200805',46651901,77202000783,0 ,0 ,0 ,0 ,0 ,0 ,1,0,0,NULL,NULL,NULL,0)

  IF OBJECT_ID('TEMPDB..#DETALHAMENTO') IS NOT NULL  
  BEGIN  
   DROP TABLE #DETALHAMENTO  
  END  
  
  CREATE TABLE #DETALHAMENTO (  
   id INT IDENTITY(1, 1) NOT NULL  
   ,linha VARCHAR(60) NULL -- TAMANHO MAXIMO ACEITO NO DETALHAMENTO  
   ,regra INT NULL  
   )       
     
   IF OBJECT_ID('tempdb..#evento_SEGBR_sinistro_detalhe_atual_tmp') IS NOT NULL  
  BEGIN  
   DROP TABLE #evento_SEGBR_sinistro_detalhe_atual_tmp  
  END  
    
  CREATE TABLE #evento_SEGBR_sinistro_detalhe_atual_tmp (  
   id INT identity(1, 1) PRIMARY KEY NOT NULL  
   ,evento_id INT  
   ,tp_detalhe VARCHAR(10)  
   ,seq_detalhe SMALLINT  
    ,descricao VARCHAR(300)  
   ,usuario VARCHAR(20)  
   ,dt_inclusao SMALLDATETIME  
   )                  
            
 
--IF @@TRANCOUNT > 0 EXEC desenv_db.dbo.SEGS14966_SPS 1, '00216281_BSP'


--CREATE PROCEDURE dbo.SEGS14966_SPS (  
  DECLARE @id INT  = 1
 ,@USUARIO VARCHAR(20) = 'TESTE'  
    
   -- Declaração e tratamento de variaveis (inicio)      
   DECLARE @produto_id AS INT    
   DECLARE @ramo_id AS INT    
   DECLARE @proposta_id AS INT    
   DECLARE @evento_sinistro_id AS INT    
   DECLARE @tp_cobertura_id AS INT    
   DECLARE @dt_ocorrencia AS SMALLDATETIME    
   DECLARE @evento_id AS NUMERIC(11, 0)  
   ---  
   DECLARE @PgtoImediato AS SMALLINT = 1 -- (1-Sim / Não)      
   DECLARE @nome_regra as VARCHAR(40)  
   DECLARE @valor AS CHAR(300)  
   DECLARE @vigencia_dias INT  
   DECLARE @val_is AS NUMERIC(15,2)  
   DECLARE @tp_componente_id AS INT  
   DECLARE @sub_grupo_id AS INT  
   DECLARE @tipo_ramo AS INT  
   DECLARE @val_is_max1 AS NUMERIC(15,2) = 10000  
   DECLARE @regra_min_id INT  
   DECLARE @regra_max_id INT  
   DECLARE @exec_regra6 int = 0   
    
   SELECT @produto_id = a.produto_id   
              ,@ramo_id = a.ramo_id  
               ,@proposta_id = a.proposta_id  
               ,@evento_sinistro_id = a.evento_sinistro_id  
               ,@tp_cobertura_id = a.tp_cobertura_id  
               ,@dt_ocorrencia = a.dt_ocorrencia  
               ,@evento_id = a.evento_id  
               ,@tp_componente_id = a.tp_componente_id   
                ,@sub_grupo_id = IIF(@produto_id IN (115, 126, 714, 150, 222,123, 226), NULL, 0)  
           FROM #classificacao_tmp a   
           WHERE ID = @ID 
           
           select @tp_componente_id, @sub_grupo_id 
  

    IF OBJECT_ID('TEMPDB..#regra_tmp') IS NOT NULL    
    BEGIN  
     DROP TABLE #regra_tmp
    END    
      
    CREATE TABLE #regra_tmp (  
    id int identity(1,1) primary key not null  
    ,nome_regra varchar(40) null  
    ,valor varchar(300) null  
    ,operador varchar(100) null  
    )  
   
  IF OBJECT_ID('tempdb..#consultar_coberturas_sps_tmp') IS NOT NULL  
  BEGIN  
   DROP TABLE #consultar_coberturas_sps_tmp  
  END  
     
    CREATE TABLE #consultar_coberturas_sps_tmp (  
     tp_cobertura_id INT null  
     ,nome_cobertura VARCHAR(200) null  
     ,val_is NUMERIC(15, 2) null  
     ,dt_inicio_vigencia SMALLDATETIME null  
     ,dt_fim_vigencia SMALLDATETIME null  
     ,cod_objeto_segurado INT null  
     ,texto_franquia VARCHAR(100) null  
     )  
   
   
    --VALIDAÇÃO DOS PARÂMENTROS      
    DECLARE @foi_indeferido SMALLINT = 0      
      
    --INICIALIZANDO O DETALHAMENTO    
    INSERT INTO #DETALHAMENTO (linha)    
    SELECT '--------------------------'    
      
    INSERT INTO #DETALHAMENTO (linha)    
    SELECT '' --    
      
    INSERT INTO #DETALHAMENTO (linha)    
    SELECT '' --    
      
    INSERT INTO #DETALHAMENTO (linha)    
    SELECT 'CLASSIFICAÇÃO DO SINISTRO:'    
      
    INSERT INTO #DETALHAMENTO (linha)    
    SELECT '--------------------------'    
      
    INSERT INTO #DETALHAMENTO (linha)    
    SELECT '' -- GRAVAÇÃO DA CLASSIFICAÇÃO, ID 6     
      
    INSERT INTO #DETALHAMENTO (linha)    
    SELECT '' --    
      
    INSERT INTO #DETALHAMENTO (linha)    
    SELECT 'Informações:'    
      
    INSERT INTO #DETALHAMENTO (linha)    
    SELECT '' --        
    -----------------------------------------------------------------------------------------------------------------------------------------------------      
    --###################################################################################################################################################      
    --INICIO DA CLASSIFICAÇÃO      
    -----------------------------------------------------------------------------------------------------------------------------------------------------          
     
    --INDEFERIMENTO AUTOMATICO  
    IF EXISTS (  
    SELECT TOP 1 1  
    FROM seguros_db.dbo.sinistro_parametro_indeferimento_automatico_tb a WITH (NOLOCK)  
    WHERE a.proposta_id = @proposta_id  
    )  
    BEGIN  
    SELECT 111
      EXEC seguros_db.dbo.SEGS14964_SPS @id = @id, @deferido = 0  
    END  
    ELSE   
    BEGIN          
      SELECT 2     
     INSERT INTO #regra_tmp   
     (nome_regra, valor, operador)  
      SELECT a.nome  
       ,b.valor   
       ,b.operador  
      FROM seguros_db.DBO.sinistro_parametro_regra_tb a with(nolock)  
      INNER JOIN seguros_db.DBO.sinistro_parametro_chave_regra_tb b WITH (NOLOCK)  
        ON a.sinistro_parametro_regra_id = b.sinistro_parametro_regra_id  
      INNER JOIN seguros_db.DBO.sinistro_parametro_chave_TB c WITH (NOLOCK)  
        ON b.sinistro_parametro_chave_id = c.sinistro_parametro_chave_id  
      WHERE c.produto_id = @produto_id  
      AND c.ramo_id = @ramo_id  
      AND c.evento_sinistro_id = @evento_sinistro_id  
      AND c.tp_cobertura_id = @tp_cobertura_id  
      AND c.dt_fim_vigencia IS NULL  
              
      SELECT @regra_min_id = MIN(ID), @regra_max_id = MAX(ID) from #regra_tmp  
      
      SELECT @regra_min_id, @regra_max_id, @foi_indeferido
        
      WHILE @regra_min_id <= @regra_max_id AND @foi_indeferido = 0  
      BEGIN 
        SELECT 3 
        SELECT @nome_regra = a.nome_regra  
        ,@valor = a.valor   
        FROM #regra_tmp a  
        WHERE a.id = @regra_min_id  
          
        IF @valor = 'S' AND ( @nome_regra = 'verifica_vigencia_diferenciada'  
          OR  @nome_regra = 'verifica_vigencia_normal'  
          OR @nome_regra = 'verifica_vigencia_SEGP1285'  
          OR @nome_regra = 'verifica_vigencia_assembleia'  
          OR @nome_regra = 'verifica_vigencia_proposta_bb_ant'  
          )  
        BEGIN  
          SELECT 4
          EXEC seguros_db.dbo.SEGS14968_SPS @NOME_REGRA = @NOME_REGRA, @proposta_id = @proposta_id, @dt_ocorrencia = @dt_ocorrencia  
        END  
  
        SET @regra_min_id = @regra_min_id + 1          
      END  
        
      --obtendo dias de vigencia e status do deferimento  
      SELECT @vigencia_dias = a.vigencia_dias   
             ,@foi_indeferido = case a.deferido WHEN 0 THEN 1  
                                             WHEN 1 THEN 0   
                                             ELSE 0  
                               END  
      FROM #classificacao_tmp a  
      WHERE a.id = @id  
                 
      IF @foi_indeferido = 1  
      BEGIN  
        SELECT 5
        EXEC seguros_db.dbo.SEGS14964_SPS @id = @id, @deferido = 0  
      END  
      ELSE  
      BEGIN     
        
      
        --obtendo valor da IS  
        SET @tipo_ramo = 1          
        
        SELECT @dt_ocorrencia , @proposta_id , @tp_componente_id, @sub_grupo_id ,@tipo_ramo 
              
        INSERT INTO #consultar_coberturas_sps_tmp (tp_cobertura_id     ,nome_cobertura      ,val_is      ,dt_inicio_vigencia      ,dt_fim_vigencia      ,cod_objeto_segurado      ,texto_franquia)  
        EXEC seguros_db.dbo.consultar_coberturas_sps @dt_ocorrencia = @dt_ocorrencia, @proposta_id = @proposta_id, @tp_componente_id = @tp_componente_id, @sub_grupo_id = @sub_grupo_id ,@tipo_ramo = @tipo_ramo  
          
        SELECT @val_is = val_is   
        FROM #consultar_coberturas_sps_tmp  
          
        UPDATE a  
        SET val_is = @val_is  
        FROM #classificacao_tmp a  
        WHERE a.id = @id  


              
        --corrigir para pegar o valor do parametro da regra 5 e o operador  
        --REGRA 5    
                              
        IF @vigencia_dias > 364   
        AND EXISTS( SELECT TOP 1 1   
                    FROM #regra_tmp   
                    WHERE nome_regra = 'verifica_vigencia_1ano_x_valor_is'  
                    AND ((operador = '>' AND @val_is > valor)   
                      OR (operador = '<' AND @val_is < valor)   
                      OR (operador = '=' AND @val_is = valor)   
                      OR (operador = '>=' AND @val_is >= valor)   
                      OR (operador = '<=' AND @val_is <= valor))  
                   )  
        BEGIN         
           select 7
           EXEC seguros_db.dbo.SEGS14964_SPS @id = @id, @deferido = 1  
        END  
        ELSE  
        BEGIN  
          select 8
          SET @exec_regra6 = 1  
        END         
          
        --REGRA 6  
        IF @exec_regra6 = 1 AND EXISTS(SELECT TOP 1 1 FROM #regra_tmp WHERE valor = 'S' AND nome_regra = 'verifica_vigencia_60dias_x_valor_is')  
        BEGIN  
          IF @val_is <= @val_is_max1 --REGRA 6  
          BEGIN  
            EXEC seguros_db.dbo.SEGS14964_SPS @id = @id, @deferido = 1, @regra = 6               
          END                                  
          ELSE  
   BEGIN  
            EXEC seguros_db.dbo.SEGS14964_SPS @id = @id, @deferido = 0, @regra = 6  
          END            
        END  
          
      END  
    END  
    -----------------------------------------------------------------------------------------------------------------------------------------------------      
    --###################################################################################################################################################      
    --FIM DA CLASSIFICAÇÃO      
    -----------------------------------------------------------------------------------------------------------------------------------------------------      
    IF @foi_indeferido = 0  
    BEGIN  
     --ATUALIZAÇÃO DA CLASSIFICAÇÃO    
     UPDATE A  
     SET LINHA = 'O SINISTRO FOI CLASSIFICADO COMO DEFERIDO'  
     ,REGRA = IIF(@exec_regra6 = 1, 6, 0)    
     FROM #DETALHAMENTO A  
     WHERE ID = 6  
    END  
    ELSE  
    BEGIN  
     --ATUALIZAÇÃO DA CLASSIFICAÇÃO    
     UPDATE A  
     SET LINHA = 'O SINISTRO FOI CLASSIFICADO COMO INDEFERIDO'  
     ,REGRA = IIF(@exec_regra6 = 1, 6, 0)  
     FROM #DETALHAMENTO A  
     WHERE ID = 6  
    END  
  
    --FINALIZANDO A CRIACAO DO DETALHAMENTO    
    INSERT INTO #DETALHAMENTO (linha)  
    SELECT ''  
  
    INSERT INTO #DETALHAMENTO (linha)  
    SELECT '--------------------------'   
      
    DECLARE @MAX_SEQ_DETALHE as INT  
    --' TP_DETALHAMENTO:    
    --' 0- Anotação    
    --' 1- Exigência    
    --' 2- Recibo    
    --' 3- Indeferimento "A Exigência deverá ser emitida Manualmente no SEGUR."    
    --' 4- Solicitação de cancelamento    
    --' 5- Comunicações feitas pelo GTR    
  
    --INICIANDO A GRAVAÇÃO DO DETALHAMENTO    
    IF @PgtoImediato = 1  
    BEGIN  
     --PRODUTOS AVISADOS PELO SEGP0794 GRAVAM NAS TABELAS DE EVENTO DO SINISTRO     
     SELECT @MAX_SEQ_DETALHE = MAX(seq_detalhe)  
     FROM SEGUROS_DB.DBO.evento_SEGBR_sinistro_detalhe_atual_tb WITH (NOLOCK)  
     WHERE EVENTO_ID = @evento_id  
  
     --GRAVANDO O DETALHAMENTO    
     INSERT INTO #evento_SEGBR_sinistro_detalhe_atual_tmp (  
      evento_id  
      ,tp_detalhe  
      ,seq_detalhe  
      ,descricao  
      ,usuario  
      ,dt_inclusao  
      )  
     SELECT @evento_id AS evento_id  
      ,'ANOTACAO' AS tp_detalhe  
      ,ISNULL(@MAX_SEQ_DETALHE, 0) + ID AS seq_detalhe  
      ,LINHA AS descricao  
      ,@USUARIO AS usuario  
      ,GETDATE() AS dt_inclusao  
     FROM #DETALHAMENTO WITH (NOLOCK)  
    END  
  