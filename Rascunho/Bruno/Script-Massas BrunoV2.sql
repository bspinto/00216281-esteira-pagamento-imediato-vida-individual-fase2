WHILE @@TRANCOUNT > 0 ROLLBACK
WHILE @@TRANCOUNT = 0 BEGIN TRAN

DECLARE @PRODUTO_ID INT
DECLARE @NOME_REGRA VARCHAR(40)
DECLARE @PROPOSTA_ID int
DECLARE @DT_OCORRENCIA SMALLDATETIME
DECLARE @VAL_IS NUMERIC(15, 2)
DECLARE @DEFERIDO CHAR(1)

--MASSA DE TESTE (VIGENCIA DIFERENCIADA 1)
--PARAMETROS
SET @PRODUTO_ID = 1196
SET @DT_OCORRENCIA =  '20200226'
SET @DEFERIDO = 'S' --S OU N
--verifica_vigencia_diferenciada
--verifica_vigencia_normal
--verifica_vigencia_SEGP1285
--verifica_vigencia_assembleia
--verifica_vigencia_proposta_bb_ant
IF @PRODUTO_ID IN (11,14,722)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_diferenciada'
END
ELSE 
IF @PRODUTO_ID IN (12,121,135,716,1196,1198,1205,1208,1211,1217,1235,1236,1237)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_normal'
END
ELSE 
IF @PRODUTO_ID IN (721)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_SEGP1285'
END
ELSE 
IF @PRODUTO_ID IN (718)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_assembleia'
END
ELSE 
IF @PRODUTO_ID IN (1174,1175)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_proposta_bb_ant'
END

--CRIACAO DE TEMPORARIA PARA VERIFICAR A VIGENCIA
IF OBJECT_ID('TEMPDB..#CLASSIFICACAO_TMP') IS NOT NULL  
       BEGIN  
        DROP TABLE #CLASSIFICACAO_TMP  
       END  
CREATE TABLE #CLASSIFICACAO_TMP (DEFERIDO INT , VIGENCIA_DIAS INT, DT_INICIO_VIGENCIA SMALLDATETIME, DT_FIM_VIGENCIA SMALLDATETIME, PROPOSTA_ID int 
								  ,log_classificacao VARCHAR(1000), val_is NUMERIC(15, 2))


IF OBJECT_ID('tempdb..#consultar_coberturas_sps_tmp') IS NOT NULL
		BEGIN
			DROP TABLE #consultar_coberturas_sps_tmp
		END

		CREATE TABLE #consultar_coberturas_sps_tmp (
			tp_cobertura_id INT NULL
			,nome_cobertura VARCHAR(200) NULL
			,val_is NUMERIC(15, 2) NULL
			,dt_inicio_vigencia SMALLDATETIME NULL
			,dt_fim_vigencia SMALLDATETIME NULL
			,cod_objeto_segurado INT NULL
			,texto_franquia VARCHAR(100) NULL
			)

			
IF OBJECT_ID('tempdb..#PROPOSTAS') IS NOT NULL
		BEGIN
			DROP TABLE #PROPOSTAS
		END

		CREATE TABLE #PROPOSTAS (proposta_id int)

SET NOCOUNT ON 

SELECT prop.proposta_id,adesao.DT_FIM_VIGENCIA
      INTO #PROPOSTAS_ATIVAS
	  FROM seguros_db.dbo.proposta_tb prop WITH(NOLOCK)  
      JOIN seguros_db.dbo.proposta_adesao_tb adesao WITH(NOLOCK)  
        ON prop.proposta_id = adesao.proposta_id  
     WHERE prop.produto_id = @produto_id
       AND situacao = 'i'


	IF @DEFERIDO = 'S'
	BEGIN
	    
		  insert INTO #PROPOSTAS (proposta_id)
		 SELECT TOP 300 adesao.proposta_id
		  FROM #PROPOSTAS_ATIVAS adesao
		  --propostas deferidas
		  WHERE @DT_OCORRENCIA < isnull(adesao.DT_FIM_VIGENCIA,getdate())
		   AND NOT EXISTS(SELECT 1 FROM seguros_db.dbo.sinistro_parametro_indeferimento_automatico_tb a WITH (NOLOCK) WHERE a.proposta_id = adesao.proposta_id)
		   order by dt_fim_vigencia DESC
	END
	ELSE IF @DEFERIDO = 'N'
	BEGIN
		insert INTO #PROPOSTAS (proposta_id)
		 SELECT TOP 300 adesao.proposta_id
		  FROM #PROPOSTAS_ATIVAS adesao
		  --propostas indeferidas
		  WHERE @DT_OCORRENCIA > isnull(adesao.DT_FIM_VIGENCIA,getdate())
		  AND NOT EXISTS(SELECT 1 FROM seguros_db.dbo.sinistro_parametro_indeferimento_automatico_tb a WITH (NOLOCK) WHERE a.proposta_id = adesao.proposta_id)
		  order by dt_fim_vigencia DESC
	END
 
SELECT TOP 300 prop.proposta_id
      INTO #PROPOSTAS_PLANO
	  FROM #PROPOSTAS prop
		WHERE EXISTS (  
select 1 from proposta_tb proposta_tb with (nolock)
join seguros_db.dbo.escolha_plano_tb escolha_plano_tb with (nolock)      
     on escolha_plano_tb.proposta_id = proposta_tb.proposta_id
	 and escolha_plano_tb.dt_fim_vigencia is null      
    --and escolha_plano_tb.dt_escolha = (select max(dt_escolha)    
    --      from seguros_db.dbo.escolha_plano_tb escolha_plano_max with (nolock)     
    --      where escolha_plano_max.proposta_id = proposta_tb.proposta_id )    
   join seguros_db.dbo.plano_tb plano_tb with (nolock)    
     on plano_tb.produto_id = escolha_plano_tb.produto_id       
    and plano_tb.plano_id = escolha_plano_tb.plano_id          
    and plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia      
   join seguros_db.dbo.tp_cob_comp_plano_tb tp_cob_comp_plano_tb with (nolock)    
     on tp_cob_comp_plano_tb.tp_plano_id = plano_tb.tp_plano_id      
   join seguros_db.dbo.tp_cob_comp_tb tp_cob_comp_tb with (nolock)    
     on tp_cob_comp_tb.tp_cob_comp_id = tp_cob_comp_plano_tb.tp_cob_comp_id      
    and tp_cob_comp_tb.tp_componente_id = 1
	and tp_cob_comp_tb.tp_cobertura_id = 5
    and tp_cob_comp_tb.class_tp_cobertura = 'B' 
    and escolha_plano_tb.dt_escolha < @DT_OCORRENCIA   
	   )	   
	   
-- Cursor para percorrer os registros
DECLARE CURSOR_VIG_DIF CURSOR FOR

SELECT proposta_id
      from 
	  #PROPOSTAS_PLANO
 
--Abrindo Cursor
OPEN CURSOR_VIG_DIF
 
-- Lendo a próxima linha
FETCH NEXT FROM CURSOR_VIG_DIF INTO @PROPOSTA_ID
 
-- Percorrendo linhas do cursor (enquanto houverem)
WHILE @@FETCH_STATUS = 0
BEGIN


INSERT INTO #CLASSIFICACAO_TMP (PROPOSTA_ID,log_classificacao) VALUES (@PROPOSTA_ID,' ')
EXEC DESENV_DB.DBO.SEGS14968_SPS @NOME_REGRA , @PROPOSTA_ID  , @DT_OCORRENCIA  
--SELECT @NOME_REGRA , @PROPOSTA_ID , @DT_OCORRENCIA

INSERT INTO #consultar_coberturas_sps_tmp (
					tp_cobertura_id
					,nome_cobertura
					,val_is
					,dt_inicio_vigencia
					,dt_fim_vigencia
					,cod_objeto_segurado
					,texto_franquia
					)
				EXEC seguros_db.dbo.consultar_coberturas_sps @dt_ocorrencia = @dt_ocorrencia
					,@proposta_id = @proposta_id
					,@tp_componente_id = 1
					,@sub_grupo_id = null
					,@tipo_ramo = 1

				SELECT @val_is = val_is
				FROM #consultar_coberturas_sps_tmp

				UPDATE a
				SET val_is = @val_is
				FROM #classificacao_tmp a
				WHERE a.proposta_id = @proposta_id
 
 
-- Lendo a próxima linha
FETCH NEXT FROM CURSOR_VIG_DIF INTO @PROPOSTA_ID
END
 
-- Fechando Cursor para leitura
CLOSE CURSOR_VIG_DIF
 
-- Finalizado o cursor
DEALLOCATE CURSOR_VIG_DIF

--DEFERIDO
SELECT proposta_id, @produto_id as produto_id, @dt_ocorrencia as dt_ocorrencia, 
val_is, deferido, vigencia_dias,dt_inicio_vigencia,dt_fim_vigencia, log_classificacao 
FROM #CLASSIFICACAO_TMP (NOLOCK) where vigencia_dias > = 365 and val_is > 10000.00

--indeferido regra 6
--SELECT proposta_id, @produto_id as produto_id, @dt_ocorrencia as dt_ocorrencia, 
--val_is, deferido, vigencia_dias,dt_inicio_vigencia,dt_fim_vigencia, log_classificacao 
--FROM #CLASSIFICACAO_TMP (NOLOCK) where vigencia_dias < = 320 and val_is < 200000.00


--INDEFERIDO IS
--SELECT proposta_id, @produto_id as produto_id, @dt_ocorrencia as dt_ocorrencia, 
--val_is, deferido, vigencia_dias,dt_inicio_vigencia,dt_fim_vigencia, log_classificacao 
--FROM #CLASSIFICACAO_TMP (NOLOCK) where vigencia_dias > = 365 and val_is > 200000.00

--SELECT proposta_id, 11 as produto_id, '20200101' as dt_ocorrencia, 
--val_is, deferido, vigencia_dias,dt_inicio_vigencia,dt_fim_vigencia, log_classificacao 
--FROM #CLASSIFICACAO_TMP (NOLOCK) where vigencia_dias >= 365 and val_is < 200000.00

WHILE @@TRANCOUNT > 0 ROLLBACK

--tratamento valor de IS
/*
USE desenv_db
GO

ALTER PROCEDURE dbo.VAL_IS_BSP
AS
BEGIN
	IF OBJECT_ID('TEMPDB..#t') IS NOT NULL
	BEGIN
		DROP TABLE #t
	END

	CREATE TABLE #t (
		id INT identity(1, 1)
		,proposta_id INT
		)

	INSERT INTO #t (proposta_id)
	VALUES (233894747)
		,(221106949)
		,(227981086)
		,(229051499)
		,(230638661)
		,(231446590)

	DECLARE @proposta_id INT
		,@id INT = 1
		,@dt_ocorrencia SMALLDATETIME = '20200826'

	WHILE @id < 7
	BEGIN
		SELECT @proposta_id = proposta_id
		FROM #t
		WHERE id = @id

		IF EXISTS (
				SELECT TOP 1 1
				FROM seguros_db.dbo.escolha_plano_tp_cob_tb a WITH (NOLOCK)
				WHERE proposta_id = @proposta_id
				)
		BEGIN
			UPDATE a
			SET val_is = 9000
			FROM SEGUROS_DB..proposta_tb proposta_tb(NOLOCK)
			INNER JOIN SEGUROS_DB..escolha_plano_tp_cob_tb a(NOLOCK)
				ON proposta_tb.proposta_id = a.proposta_id
			INNER JOIN SEGUROS_DB..tp_cob_comp_tb tp_cob_comp_tb(NOLOCK)
				ON tp_cob_comp_tb.tp_cob_comp_id = a.tp_cob_comp_id
					AND tp_cob_comp_tb.tp_componente_id = 1
			INNER JOIN SEGUROS_DB..tp_cobertura_tb tp_cobertura_tb(NOLOCK)
				ON tp_cobertura_tb.tp_cobertura_id = tp_cob_comp_tb.tp_cobertura_id
			LEFT JOIN SEGUROS_DB..cancelamento_proposta_tb cancelamento_proposta_tb(NOLOCK)
				ON cancelamento_proposta_tb.proposta_id = proposta_tb.proposta_id
					AND cancelamento_proposta_tb.dt_inicio_cancelamento = (
						SELECT MAX(c.dt_inicio_cancelamento)
						FROM SEGUROS_DB..cancelamento_proposta_tb c(NOLOCK)
						WHERE c.proposta_id = proposta_tb.proposta_id
						)
			WHERE (
					proposta_tb.situacao IN ('e', 'a', 'i', 'p', 'v', 'l', 'm')
					OR (
						proposta_tb.situacao = 'c'
						AND cancelamento_proposta_tb.dt_inicio_cancelamento > @dt_ocorrencia
						)
					)
				AND CONVERT(VARCHAR(8), a.dt_escolha, 112) <= @dt_ocorrencia
				AND (
					a.dt_fim_vigencia_cob >= @dt_ocorrencia
					OR a.dt_fim_vigencia_cob IS NULL
					) -- Cesar Santos CONFITEC - (SBRJ009952) 01/07/2020 Fim    
				AND proposta_tb.proposta_id = @proposta_id
		END

		IF EXISTS (
				SELECT TOP 1 1
				FROM seguros_db.dbo.escolha_tp_cob_generico_tb a WITH (NOLOCK)
				WHERE proposta_id = @proposta_id
				)
		BEGIN
			UPDATE a
			SET val_is = 9000
			FROM SEGUROS_DB.dbo.proposta_tb proposta_tb WITH (NOLOCK)
			INNER JOIN SEGUROS_DB.dbo.escolha_tp_cob_generico_tb a WITH (NOLOCK)
				ON a.proposta_id = proposta_tb.proposta_id
			INNER JOIN SEGUROS_DB.dbo.tp_cobertura_tb tp_cobertura_tb WITH (NOLOCK)
				ON tp_cobertura_tb.tp_cobertura_id = a.tp_cobertura_id
			LEFT JOIN SEGUROS_DB.dbo.cancelamento_proposta_tb cancelamento_proposta_tb WITH (NOLOCK)
				ON cancelamento_proposta_tb.proposta_id = proposta_tb.proposta_id
					AND cancelamento_proposta_tb.dt_inicio_cancelamento = (
						SELECT MAX(c.dt_inicio_cancelamento)
						FROM SEGUROS_DB.dbo.cancelamento_proposta_tb c WITH (NOLOCK)
						WHERE c.proposta_id = proposta_tb.proposta_id
						)
			WHERE (
					proposta_tb.situacao IN ('e', 'a', 'i', 'p', 'v', 'l', 'm')
					OR (
						proposta_tb.situacao = 'c'
						AND cancelamento_proposta_tb.dt_inicio_cancelamento > @dt_ocorrencia
						)
					)
				AND a.dt_inicio_vigencia_esc <= @dt_ocorrencia
				AND (
					a.dt_fim_vigencia_esc IS NULL
					OR a.dt_fim_vigencia_esc >= @dt_ocorrencia
					)
				AND proposta_tb.proposta_id = @proposta_id
		END

		IF EXISTS (
				SELECT TOP 1 1
				FROM seguros_db.dbo.escolha_plano_tb a WITH (NOLOCK)
				WHERE proposta_id = @proposta_id
				)
		BEGIN
			UPDATE a
			SET a.imp_segurada = 9000
			FROM SEGUROS_DB..escolha_plano_tb a(NOLOCK)
			INNER JOIN SEGUROS_DB..plano_tb p(NOLOCK)
				ON p.plano_id = a.plano_id
					AND p.produto_id = a.produto_id
					AND a.dt_inicio_vigencia = p.dt_inicio_vigencia
			INNER JOIN SEGUROS_DB..tp_cob_comp_plano_tb ccp(NOLOCK)
				ON p.tp_plano_id = ccp.tp_plano_id
			INNER JOIN SEGUROS_DB..tp_cob_comp_tb cc(NOLOCK)
				ON cc.tp_cob_comp_id = ccp.tp_cob_comp_id
			INNER JOIN SEGUROS_DB..tp_cobertura_tb tp(NOLOCK)
				ON cc.tp_cobertura_id = tp.tp_cobertura_id
			-- Cesar Santos CONFITEC - (SBRJ009952) 01/07/2020 inicio   
			LEFT JOIN SEGUROS_DB.dbo.proposta_tb proposta_tb
				ON proposta_tb.proposta_id = a.proposta_id
			LEFT JOIN SEGUROS_DB.dbo.cancelamento_proposta_tb cancelamento_proposta_tb(NOLOCK)
				ON cancelamento_proposta_tb.proposta_id = proposta_tb.proposta_id
					AND cancelamento_proposta_tb.dt_inicio_cancelamento = (
						SELECT MAX(c.dt_inicio_cancelamento)
						FROM SEGUROS_DB..cancelamento_proposta_tb c(NOLOCK)
						WHERE c.proposta_id = proposta_tb.proposta_id
						)
			WHERE a.proposta_id = @proposta_id
				AND CONVERT(VARCHAR(8), a.dt_escolha, 112) <= @dt_Ocorrencia
				AND (
					a.dt_fim_vigencia >= @dt_Ocorrencia
					OR a.dt_fim_vigencia IS NULL
					)
				AND tp_componente_id = 1
				-- Cesar Santos CONFITEC - (SBRJ009952) 01/07/2020 inicio   
				AND (
					proposta_tb.situacao IN ('e', 'a', 'i', 'p', 'v', 'l', 'm')
					OR (
						proposta_tb.situacao = 'c'
						AND cancelamento_proposta_tb.dt_inicio_cancelamento > @dt_ocorrencia
						)
					)
		END

		SET @id = @id + 1
	END
END

WHILE @@TRANCOUNT > 0
	ROLLBACK

WHILE @@TRANCOUNT = 0
	BEGIN TRANSACTION

IF @@TRANCOUNT > 0	EXEC desenv_db.dbo.VAL_IS_BSP
--commit
*/

--tratamento tempo de vigencia para verifica_vigencia_normal
/*
USE desenv_db
GO

ALTER PROC dbo.atualiza_dt_inicio_vigencia_bsp @proposta_id INT, @dias int
AS
BEGIN 
  DECLARE @TABELAS AS VARCHAR(300) = ''
  IF EXISTS(SELECT 1 FROM seguros_db.dbo.proposta_adesao_tb a WITH (NOLOCK) WHERE a.proposta_id = @proposta_id)
  BEGIN
    SET @TABELAS = @TABELAS + 'proposta_adesao_tb /'
    UPDATE a
    SET dt_inicio_vigencia = GETDATE() - @dias
    ,dt_fim_vigencia = NULL
    FROM seguros_db.dbo.proposta_adesao_tb a
    WHERE a.proposta_id = @proposta_id 
  END

  IF EXISTS(SELECT 1 FROM seguros_db.dbo.apolice_tb a WITH (NOLOCK) WHERE a.proposta_id = @proposta_id)
  BEGIN
    SET @TABELAS = @TABELAS + 'apolice_tb /'
    UPDATE a
    SET dt_inicio_vigencia = GETDATE() - @dias
      , dt_fim_vigencia = NULL
    FROM seguros_db.dbo.apolice_tb a
    WHERE a.proposta_id = @proposta_id 
  END
  
  IF EXISTS(SELECT 1 FROM seguros_db.dbo.proposta_fechada_tb a WITH (NOLOCK) WHERE a.proposta_id = @proposta_id)
  BEGIN
    SET @TABELAS = @TABELAS + 'proposta_fechada_tb /'  
    UPDATE a
    SET dt_inicio_vig = GETDATE() - @dias
      , dt_fim_vig = NULL
    FROM seguros_db.dbo.proposta_fechada_tb a
    WHERE a.proposta_id = @proposta_id 
  END  
  
 if EXISTS(SELECT 1 FROM seguros_db.dbo.endosso_tb a with (nolock) where a.tp_endosso_id = 314 AND a.proposta_id = @proposta_id) 
 BEGIN
   DECLARE @ID_MAX INT
   SELECT @ID_MAX = ENDOSSO_ID FROM seguros_db.dbo.endosso_tb a with (nolock) where a.tp_endosso_id = 314 AND a.proposta_id = @proposta_id
   
   SET @TABELAS = @TABELAS + 'endosso_tb /'  
   
   UPDATE a
    SET a.dt_inicio_vigencia_end = GETDATE() - @dias
      , a.dt_fim_vigencia_end = NULL
    FROM seguros_db.dbo.endosso_tb a
    WHERE a.proposta_id = @proposta_id 
 END 
  
  SELECT @TABELAS
END
go 

WHILE @@TRANCOUNT > 0 	ROLLBACK
WHILE @@TRANCOUNT = 0 	BEGIN TRANSACTION


IF @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_dt_inicio_vigencia_bsp 233633181, 200
IF @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_dt_inicio_vigencia_bsp 225783921, 200
IF @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_dt_inicio_vigencia_bsp 233416730, 200
IF @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_dt_inicio_vigencia_bsp 242862545, 200
IF @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_dt_inicio_vigencia_bsp 243378422, 200
IF @@TRANCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_dt_inicio_vigencia_bsp 255129598, 200
if @@tranCOUNT > 0 EXEC DESENV_DB.dbo.atualiza_dt_inicio_vigencia_bsp 254466162, 200
--COMMIT
*/

/*
USE DESENV_DB
GO 

WHILE @@TRANCOUNT > 0 	ROLLBACK
WHILE @@TRANCOUNT = 0 	BEGIN TRANSACTION

if @@trancount > 0 exec desenv_db.DBO.TESTE_RICARDO8 98202000061
--commit
/*