SELECT top 10 p.proposta_id
,'C�digo' = c.cliente_id
	,'Nome' = c.nome
	,'CPF/CGC' = pf.cpf
	,'Sexo' = CASE 
		WHEN pf.sexo = 'F'
			THEN 'Feminino'
		WHEN pf.sexo = 'M'
			THEN 'Masculino'
		ELSE 'Indefinido'
		END
	,'Data de Nascimento' = convert(CHAR(10), pf.dt_nascimento, 103)
	,'Documento' = isnull(c.doc_identidade, '')
	,'Tipo Doc.' = isnull(c.tp_doc_identidade, '')
	,'Expedi��o Doc.' = isnull(c.exp_doc_identidade, '')
	,'Data Emiss�o Doc.' = isnull(convert(CHAR(10), c.dt_emissao_identidade, 103), '')
	,'Proposta' = p.proposta_id
	,'Produto' = p.produto_id
FROM cliente_tb c WITH (NOLOCK)
	,pessoa_fisica_tb pf WITH (NOLOCK)
	,proposta_tb p WITH (NOLOCK)
WHERE c.cliente_id = pf.pf_cliente_id
	AND c.cliente_id = p.prop_cliente_id
	AND p.situacao <> 'T'

UNION

SELECT top 10 p.proposta_id,
'C�digo' = c.cliente_id
	,'Nome' = c.nome
	,'CPF/CGC' = pj.cgc
	,'Sexo' = 'Indefinido'
	,'Data de Nascimento' = ''
	,'Documento' = isnull(c.doc_identidade, '')
	,'Tipo Doc.' = isnull(c.tp_doc_identidade, '')
	,'Expedi��o Doc.' = isnull(c.exp_doc_identidade, '')
	,'Data Emiss�o Doc.' = isnull(convert(CHAR(10), c.dt_emissao_identidade, 103), '')
	,'Proposta' = p.proposta_id
	,'Produto' = p.produto_id
FROM cliente_tb c WITH (NOLOCK)
	,pessoa_juridica_tb pj WITH (NOLOCK)
	,proposta_tb p WITH (NOLOCK)
WHERE C.Cliente_id = pj.pj_cliente_id
	AND c.cliente_id = p.prop_cliente_id

	AND p.situacao <> 'T'


--IS
 exec consultar_coberturas_sps '20200722',11294570,1 ,null  , 1
 
 
 --@TIPO_RAMO = 2 OR @TIPO_RAMO IS NULL 
              isnull(tp_cobertura_tb.nome, tp_cobertura_tb.descricao),                      
              escolha_tp_cob_generico_tb.val_is ,                      
              escolha_tp_cob_generico_tb.dt_inicio_vigencia_esc,                      
              escolha_tp_cob_generico_tb.dt_fim_vigencia_esc,                
              escolha_tp_cob_generico_tb.cod_objeto_segurado,          
              escolha_tp_cob_generico_tb.texto_franquia                
      FROM SEGUROS_DB..proposta_tb proposta_tb (nolock)                        
      JOIN SEGUROS_DB..escolha_tp_cob_generico_tb escolha_tp_cob_generico_tb (NOLOCK) ON escolha_tp_cob_generico_tb.proposta_id = proposta_tb.proposta_id                       
      JOIN SEGUROS_DB..tp_cobertura_tb tp_cobertura_tb (NOLOCK) ON tp_cobertura_tb.tp_cobertura_id = escolha_tp_cob_generico_tb.tp_cobertura_id                      
      LEFT JOIN SEGUROS_DB..cancelamento_proposta_tb  cancelamento_proposta_tb (NOLOCK)ON cancelamento_proposta_tb.proposta_id = proposta_tb.proposta_id                      
                                             AND cancelamento_proposta_tb.dt_inicio_cancelamento = (SELECT MAX(c.dt_inicio_cancelamento)                      
                                          FROM SEGUROS_DB..cancelamento_proposta_tb c  (nolock)           
                                                                                                    WHERE c.proposta_id = proposta_tb.proposta_id)                      
     WHERE (proposta_tb.situacao IN ('e','a','i','p','v','l','m')                      
        OR  (proposta_tb.situacao = 'c'                         AND   cancelamento_proposta_tb.dt_inicio_cancelamento > @dt_ocorrencia))                      
       AND escolha_tp_cob_generico_tb.dt_inicio_vigencia_esc <= @dt_ocorrencia                      
--petrauskas confitec flow 17900291  
--       AND escolha_tp_cob_generico_tb.dt_fim_vigencia_esc IS NULL  
       AND (   escolha_tp_cob_generico_tb.dt_fim_vigencia_esc IS NULL  
            OR escolha_tp_cob_generico_tb.dt_fim_vigencia_esc >= @dt_ocorrencia)  
       AND proposta_tb.proposta_id = @proposta_id                      
      AND escolha_tp_cob_generico_tb.cod_objeto_segurado  = @cod_objeto_segurado      

IF @TIPO_RAMO = 1
		OR @TIPO_RAMO IS NULL
				SELECT tp_cobertura_tb.tp_cobertura_id
			,ISNULL(tp_cobertura_tb.nome, tp_cobertura_tb.descricao)
			,escolha_plano_tp_cob_tb.val_is
			,escolha_plano_tp_cob_tb.dt_escolha
			,escolha_plano_tp_cob_tb.dt_fim_vigencia_cob
			,NULL
		FROM SEGUROS_DB..proposta_tb proposta_tb(NOLOCK)
		INNER JOIN SEGUROS_DB..escolha_plano_tp_cob_tb escolha_plano_tp_cob_tb(NOLOCK)
			ON proposta_tb.proposta_id = escolha_plano_tp_cob_tb.proposta_id
		INNER JOIN SEGUROS_DB..tp_cob_comp_tb tp_cob_comp_tb(NOLOCK)
			ON tp_cob_comp_tb.tp_cob_comp_id = escolha_plano_tp_cob_tb.tp_cob_comp_id
				AND tp_cob_comp_tb.tp_componente_id = @tp_componente_id
		INNER JOIN SEGUROS_DB..tp_cobertura_tb tp_cobertura_tb(NOLOCK)
			ON tp_cobertura_tb.tp_cobertura_id = tp_cob_comp_tb.tp_cobertura_id
		LEFT JOIN SEGUROS_DB..cancelamento_proposta_tb cancelamento_proposta_tb(NOLOCK)
			ON cancelamento_proposta_tb.proposta_id = proposta_tb.proposta_id
				AND cancelamento_proposta_tb.dt_inicio_cancelamento = (
					SELECT MAX(c.dt_inicio_cancelamento)
					FROM SEGUROS_DB..cancelamento_proposta_tb c(NOLOCK)
					WHERE c.proposta_id = proposta_tb.proposta_id
					)
		WHERE (
				proposta_tb.situacao IN ('e', 'a', 'i', 'p', 'v', 'l', 'm')
				OR (
					proposta_tb.situacao = 'c'
					AND cancelamento_proposta_tb.dt_inicio_cancelamento > @dt_ocorrencia
					)
				)
			---------------------------------------------------------------------------------------------------------------------------------------------------------------  
			--Ricardo Toledo (Confitec) : 16/04/2015 : INC000004565655 : inicio  
			--O sistema não estava identificando a cobertura da proposta porque a data de escolha do plano está com horário. Com essa formatação vai comparar apenas a data  
			--AND escolha_plano_tp_cob_tb.dt_escolha <=  @dt_ocorrencia                      
			AND CONVERT(VARCHAR(8), escolha_plano_tp_cob_tb.dt_escolha, 112) <= @dt_ocorrencia
			--Ricardo Toledo (Confitec) : 16/04/2015 : INC000004565655 : fim  
			---------------------------------------------------------------------------------------------------------------------------------------------------------------  
			AND (
				escolha_plano_tp_cob_tb.dt_fim_vigencia_cob >= @dt_ocorrencia
				OR escolha_plano_tp_cob_tb.dt_fim_vigencia_cob IS NULL
				) -- Cesar Santos CONFITEC - (SBRJ009952) 01/07/2020 Fim    
			AND proposta_tb.proposta_id = @proposta_id
		-- FLOW 704824 - JFILHO - CONFITEC - 22/01/2009: Inserindo tratamento de erros na procedure  
		-- INICIO - MU00416967 - Melhorias_SEGP0794_Aviso_Sinistro -- 29/01/2018  
		
		UNION
		
		SELECT escolha_tp_cob_generico.tp_cobertura_id
			,isnull(tp_cobertura_tb.nome, tp_cobertura_tb.descricao)
			,escolha_tp_cob_generico.val_is
			,escolha_tp_cob_generico.dt_inicio_vigencia_esc
			,escolha_tp_cob_generico.dt_fim_vigencia_esc
			,escolha_tp_cob_generico.cod_objeto_segurado
		FROM SEGUROS_DB.dbo.proposta_tb proposta_tb WITH (NOLOCK)
		INNER JOIN SEGUROS_DB.dbo.escolha_tp_cob_generico_tb escolha_tp_cob_generico WITH (NOLOCK)
			ON escolha_tp_cob_generico.proposta_id = proposta_tb.proposta_id
		INNER JOIN SEGUROS_DB.dbo.tp_cobertura_tb tp_cobertura_tb WITH (NOLOCK)
			ON tp_cobertura_tb.tp_cobertura_id = escolha_tp_cob_generico.tp_cobertura_id
		LEFT JOIN SEGUROS_DB.dbo.cancelamento_proposta_tb cancelamento_proposta_tb WITH (NOLOCK)
			ON cancelamento_proposta_tb.proposta_id = proposta_tb.proposta_id
				AND cancelamento_proposta_tb.dt_inicio_cancelamento = (
					SELECT MAX(c.dt_inicio_cancelamento)
					FROM SEGUROS_DB.dbo.cancelamento_proposta_tb c WITH (NOLOCK)
					WHERE c.proposta_id = proposta_tb.proposta_id
					)
		WHERE (
				proposta_tb.situacao IN ('e', 'a', 'i', 'p', 'v', 'l', 'm')
				OR (
					proposta_tb.situacao = 'c'
					AND cancelamento_proposta_tb.dt_inicio_cancelamento > @dt_ocorrencia
					)
				)
			AND escolha_tp_cob_generico.dt_inicio_vigencia_esc <= @dt_ocorrencia
			AND (
				escolha_tp_cob_generico.dt_fim_vigencia_esc IS NULL
				OR escolha_tp_cob_generico.dt_fim_vigencia_esc >= @dt_ocorrencia
				)
			AND proposta_tb.proposta_id = @proposta_id
			
analise val_is
,escolha_tp_cob_generico_tb.val_is
,escolha_tp_cob_res_tb.val_is		
,escolha_tp_cob_emp_tb.val_is
,escolha_tp_cob_aceito_tb.val_is
,escolha_tp_cob_avulso_tb.val_is
,escolha_tp_cob_cond_tb.val_is
,escolha_tp_cob_maq_tb.val_is
,escolha_plano_tp_cob_tb.val_is
,escolha_tp_cob_generico.val_is
,escolha_tp_cob_vida_tb.val_is
,escolha_tp_cob_vida_tb.val_is
,escolha_tp_cob_vida_aceito_tb.val_is
,escolha_sub_grp_tp_cob_comp_tb.val_is
	

