

DECLARE @PRODUTO_ID INT
DECLARE @NOME_REGRA VARCHAR(40)
DECLARE @PROPOSTA_ID int
DECLARE @DT_OCORRENCIA SMALLDATETIME
DECLARE @VAL_IS NUMERIC(15, 2)
DECLARE @DEFERIDO CHAR(1)

--MASSA DE TESTE (VIGENCIA DIFERENCIADA 1)
--PARAMETROS
SET @PRODUTO_ID = 1174
SET @DT_OCORRENCIA =  '20160801'
SET @DEFERIDO = 'S' --S OU N
--verifica_vigencia_diferenciada
--verifica_vigencia_normal
--verifica_vigencia_SEGP1285
--verifica_vigencia_assembleia
--verifica_vigencia_proposta_bb_ant
IF @PRODUTO_ID IN (11,14,722)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_diferenciada'
END
ELSE 
IF @PRODUTO_ID IN (12,121,135,716,1196,1198,1205,1208,1211,1217,1235,1236,1237)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_normal'
END
ELSE 
IF @PRODUTO_ID IN (721)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_SEGP1285'
END
ELSE 
IF @PRODUTO_ID IN (718)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_assembleia'
END
ELSE 
IF @PRODUTO_ID IN (1174,1175)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_proposta_bb_ant'
	--adicionar esta condicao na consulta "AND ISNULL(adesao.proposta_bb_anterior,0) = 0"
END

--CRIACAO DE TEMPORARIA PARA VERIFICAR A VIGENCIA
IF OBJECT_ID('TEMPDB..#CLASSIFICACAO_TMP') IS NOT NULL  
       BEGIN  
        DROP TABLE #CLASSIFICACAO_TMP  
       END  
CREATE TABLE #CLASSIFICACAO_TMP (DEFERIDO INT , VIGENCIA_DIAS INT, DT_INICIO_VIGENCIA SMALLDATETIME, DT_FIM_VIGENCIA SMALLDATETIME, PROPOSTA_ID int 
								  ,log_classificacao VARCHAR(1000), val_is NUMERIC(15, 2))


IF OBJECT_ID('tempdb..#consultar_coberturas_sps_tmp') IS NOT NULL
		BEGIN
			DROP TABLE #consultar_coberturas_sps_tmp
		END

		CREATE TABLE #consultar_coberturas_sps_tmp (
			tp_cobertura_id INT NULL
			,nome_cobertura VARCHAR(200) NULL
			,val_is NUMERIC(15, 2) NULL
			,dt_inicio_vigencia SMALLDATETIME NULL
			,dt_fim_vigencia SMALLDATETIME NULL
			,cod_objeto_segurado INT NULL
			,texto_franquia VARCHAR(100) NULL
			)

			
IF OBJECT_ID('tempdb..#PROPOSTAS') IS NOT NULL
		BEGIN
			DROP TABLE #PROPOSTAS
		END

		CREATE TABLE #PROPOSTAS (proposta_id int)

SET NOCOUNT ON 

SELECT prop.proposta_id,isnull(adesao.DT_FIM_VIGENCIA,fechada.dt_fim_vig) as dt_fim_vigencia
      INTO #PROPOSTAS_ATIVAS
	  FROM seguros_db.dbo.proposta_tb prop WITH(NOLOCK)  
   LEFT JOIN seguros_db.dbo.proposta_adesao_tb adesao WITH(NOLOCK)  
        ON prop.proposta_id = adesao.proposta_id
	LEFT JOIN seguros_db.dbo.proposta_fechada_tb fechada WITH(NOLOCK)  
        ON prop.proposta_id = fechada.proposta_id  
     WHERE prop.produto_id = @produto_id
       AND situacao = 'i'


	IF @DEFERIDO = 'S'
	BEGIN
	    
		  insert INTO #PROPOSTAS (proposta_id)
		 SELECT TOP 1000 adesao.proposta_id
		  FROM #PROPOSTAS_ATIVAS adesao
		  --propostas deferidas
		  WHERE @DT_OCORRENCIA < isnull(adesao.DT_FIM_VIGENCIA,getdate())
		   order by dt_fim_vigencia DESC
	END
	ELSE IF @DEFERIDO = 'N'
	BEGIN
		insert INTO #PROPOSTAS (proposta_id)
		 SELECT TOP 1000 adesao.proposta_id
		  FROM #PROPOSTAS_ATIVAS adesao
		  --propostas indeferidas
		  where @dt_ocorrencia > isnull(adesao.dt_fim_vigencia,getdate())
		  order by dt_fim_vigencia desc
	END
 
SELECT TOP 1000 prop.proposta_id
      INTO #PROPOSTAS_PLANO
	  FROM #PROPOSTAS prop
		WHERE EXISTS (  
select 1 from proposta_tb proposta_tb with (nolock)
join seguros_db.dbo.escolha_plano_tb escolha_plano_tb with (nolock)      
     on escolha_plano_tb.proposta_id = proposta_tb.proposta_id
	 and escolha_plano_tb.dt_fim_vigencia is null      
    --and escolha_plano_tb.dt_escolha = (select max(dt_escolha)    
    --      from seguros_db.dbo.escolha_plano_tb escolha_plano_max with (nolock)     
    --      where escolha_plano_max.proposta_id = proposta_tb.proposta_id )    
   join seguros_db.dbo.plano_tb plano_tb with (nolock)    
     on plano_tb.produto_id = escolha_plano_tb.produto_id       
    and plano_tb.plano_id = escolha_plano_tb.plano_id          
    and plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia      
   join seguros_db.dbo.tp_cob_comp_plano_tb tp_cob_comp_plano_tb with (nolock)    
     on tp_cob_comp_plano_tb.tp_plano_id = plano_tb.tp_plano_id      
   join seguros_db.dbo.tp_cob_comp_tb tp_cob_comp_tb with (nolock)    
     on tp_cob_comp_tb.tp_cob_comp_id = tp_cob_comp_plano_tb.tp_cob_comp_id      
    and tp_cob_comp_tb.tp_componente_id = 1
	and tp_cob_comp_tb.tp_cobertura_id = 5
    and tp_cob_comp_tb.class_tp_cobertura = 'B'     
	   )	   
	   

	    
-- Cursor para percorrer os registros
DECLARE CURSOR_VIG_DIF CURSOR FOR

SELECT proposta_id
      from 
	  #PROPOSTAS_PLANO
 
--Abrindo Cursor
OPEN CURSOR_VIG_DIF
 
-- Lendo a próxima linha
FETCH NEXT FROM CURSOR_VIG_DIF INTO @PROPOSTA_ID
 
-- Percorrendo linhas do cursor (enquanto houverem)
WHILE @@FETCH_STATUS = 0
BEGIN


INSERT INTO #CLASSIFICACAO_TMP (PROPOSTA_ID,log_classificacao) VALUES (@PROPOSTA_ID,' ')
EXEC seguros_db.DBO.SEGS14968_SPS @NOME_REGRA , @PROPOSTA_ID  , @DT_OCORRENCIA  
--SELECT @NOME_REGRA , @PROPOSTA_ID , @DT_OCORRENCIA

INSERT INTO #consultar_coberturas_sps_tmp (
					tp_cobertura_id
					,nome_cobertura
					,val_is
					,dt_inicio_vigencia
					,dt_fim_vigencia
					,cod_objeto_segurado
					,texto_franquia
					)
				EXEC seguros_db.dbo.consultar_coberturas_sps @dt_ocorrencia = @dt_ocorrencia
					,@proposta_id = @proposta_id
					,@tp_componente_id = 1
					,@sub_grupo_id = null
					,@tipo_ramo = 1

				SELECT @val_is = val_is
				FROM #consultar_coberturas_sps_tmp

				UPDATE a
				SET val_is = @val_is
				FROM #classificacao_tmp a
				WHERE a.proposta_id = @proposta_id
 
 
-- Lendo a próxima linha
FETCH NEXT FROM CURSOR_VIG_DIF INTO @PROPOSTA_ID
END
 
-- Fechando Cursor para leitura
CLOSE CURSOR_VIG_DIF
 
-- Finalizado o cursor
DEALLOCATE CURSOR_VIG_DIF


SELECT proposta_id, @produto_id as produto_id, @dt_ocorrencia as dt_ocorrencia, 
val_is, deferido, vigencia_dias,dt_inicio_vigencia,dt_fim_vigencia, log_classificacao 
FROM #CLASSIFICACAO_TMP (NOLOCK) where vigencia_dias > = 365 and val_is < 200000.00

SELECT proposta_id, 11 as produto_id, '20200101' as dt_ocorrencia, 
val_is, deferido, vigencia_dias,dt_inicio_vigencia,dt_fim_vigencia, log_classificacao 
FROM #CLASSIFICACAO_TMP (NOLOCK) where vigencia_dias >= 365 and val_is < 200000.00

