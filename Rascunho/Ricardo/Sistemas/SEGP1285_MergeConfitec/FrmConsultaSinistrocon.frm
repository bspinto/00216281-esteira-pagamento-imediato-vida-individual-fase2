VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FrmConsultaSinistrocon 
   Caption         =   "SEGP0001 - Consulta Sinistro - VIDA"
   ClientHeight    =   10440
   ClientLeft      =   11730
   ClientTop       =   1350
   ClientWidth     =   8745
   ForeColor       =   &H00000000&
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   10440
   ScaleWidth      =   8745
   Begin VB.CommandButton btnConsultaHistProposta 
      Caption         =   "Consulta Hist�rico da Proposta"
      Enabled         =   0   'False
      Height          =   375
      Left            =   120
      TabIndex        =   52
      Top             =   4365
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.CommandButton btnConsultaPropostaInicial 
      Caption         =   "Consulta proposta inicial"
      Height          =   372
      Left            =   4185
      TabIndex        =   39
      Top             =   4365
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.CommandButton btnConsultaApolice 
      Caption         =   "Consulta Ap�lice"
      Height          =   372
      Left            =   6705
      TabIndex        =   38
      Top             =   4365
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton btnAnaliseTecnica 
      Caption         =   "An�li&se T�cnica"
      Height          =   375
      Left            =   2520
      TabIndex        =   36
      Top             =   3915
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.CommandButton BtnCancelar 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   7935
      TabIndex        =   34
      Top             =   9735
      Width           =   765
   End
   Begin VB.PictureBox pbxBotoesVisualizacaoImagem 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   105
      ScaleHeight     =   375
      ScaleWidth      =   7755
      TabIndex        =   33
      Top             =   9735
      Width           =   7755
      Begin VB.CommandButton cmd_prazo_seguradora 
         Caption         =   "Prazo Seguradora"
         Height          =   375
         Left            =   5310
         TabIndex        =   42
         Top             =   0
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.CommandButton BtnOk 
         Caption         =   "&Aplicar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6780
         TabIndex        =   30
         Top             =   0
         Width           =   915
      End
      Begin VB.CommandButton btnSinistro 
         Caption         =   "Sinistro"
         Height          =   375
         Left            =   -15
         TabIndex        =   24
         Top             =   0
         Visible         =   0   'False
         Width           =   825
      End
      Begin VB.CommandButton btnAgenda 
         Caption         =   "Agenda"
         Height          =   375
         Left            =   780
         TabIndex        =   25
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton btnDetalhar 
         Caption         =   "Detalhar"
         Height          =   375
         Left            =   1620
         TabIndex        =   26
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton btnPagamento 
         Caption         =   "Pagamento"
         Height          =   375
         Left            =   2460
         TabIndex        =   27
         Top             =   0
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.CommandButton btnAprovar 
         Caption         =   "Aprovar"
         Height          =   375
         Left            =   3420
         TabIndex        =   28
         Top             =   0
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.CommandButton btnAssessoria 
         Caption         =   "Assessoria"
         Height          =   375
         Left            =   4320
         TabIndex        =   29
         Top             =   0
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin VB.TextBox txtDIGISIN 
      Height          =   285
      Left            =   12000
      TabIndex        =   32
      Top             =   4560
      Visible         =   0   'False
      Width           =   5295
   End
   Begin VB.CommandButton btnConsultaProposta 
      Caption         =   "Consulta Proposta"
      Height          =   372
      Left            =   6705
      TabIndex        =   23
      Top             =   3915
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton btnConsultaImagemProposta 
      Caption         =   "Consulta imagem das propostas"
      Height          =   372
      Left            =   4200
      TabIndex        =   22
      Top             =   3915
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.Frame fmeDetalhamento_Visualizacao 
      Caption         =   "Detalhamento:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4875
      Left            =   120
      TabIndex        =   13
      Top             =   4815
      Visible         =   0   'False
      Width           =   8475
      Begin VB.PictureBox pct_prazos 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         HasDC           =   0   'False
         Height          =   795
         Left            =   4260
         ScaleHeight     =   795
         ScaleWidth      =   4215
         TabIndex        =   43
         Top             =   240
         Visible         =   0   'False
         Width           =   4215
         Begin VB.TextBox txtPrazoTotal 
            Enabled         =   0   'False
            Height          =   285
            Left            =   3540
            TabIndex        =   47
            Text            =   "0"
            Top             =   0
            Width           =   615
         End
         Begin VB.TextBox txtPrazoSegurado 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1560
            TabIndex        =   46
            Text            =   "0"
            Top             =   0
            Width           =   615
         End
         Begin VB.TextBox txtPrazoSeguradora 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1560
            TabIndex        =   45
            Text            =   "0"
            Top             =   360
            Width           =   615
         End
         Begin VB.TextBox txt_prazo_atual 
            Enabled         =   0   'False
            Height          =   285
            Left            =   3540
            Locked          =   -1  'True
            TabIndex        =   44
            Text            =   "0"
            Top             =   360
            Width           =   615
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Prazo total:"
            Height          =   255
            Left            =   1980
            TabIndex        =   51
            Top             =   15
            Width           =   1455
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Prazo segurado:"
            Height          =   255
            Left            =   0
            TabIndex        =   50
            Top             =   15
            Width           =   1455
         End
         Begin VB.Label Label4 
            Caption         =   "Prazo seguradora:"
            Height          =   255
            Left            =   180
            TabIndex        =   49
            Top             =   375
            Width           =   1455
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Prazo atual:"
            Height          =   195
            Left            =   2460
            TabIndex        =   48
            Top             =   405
            Width           =   975
         End
      End
      Begin VB.CheckBox chkRestrito 
         Caption         =   "Restrito ?"
         Height          =   210
         Left            =   2400
         TabIndex        =   18
         Top             =   650
         Value           =   1  'Checked
         Width           =   1905
      End
      Begin VB.CheckBox chkppe 
         Caption         =   "PPE ?"
         Height          =   210
         Left            =   1320
         TabIndex        =   41
         Top             =   630
         Visible         =   0   'False
         Width           =   1905
      End
      Begin VB.OptionButton optAnotacao 
         Caption         =   "Anota��o"
         Height          =   210
         Left            =   120
         TabIndex        =   20
         Top             =   650
         Value           =   -1  'True
         Width           =   1035
      End
      Begin VB.OptionButton optRequisicao 
         Caption         =   "Exig�ncia para o corretor"
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   360
         Visible         =   0   'False
         Width           =   2085
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   120
         MaxLength       =   68
         TabIndex        =   17
         Top             =   1080
         Visible         =   0   'False
         Width           =   8340
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   120
         MaxLength       =   68
         TabIndex        =   16
         Top             =   1365
         Visible         =   0   'False
         Width           =   8340
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   120
         MaxLength       =   68
         TabIndex        =   15
         Top             =   1635
         Visible         =   0   'False
         Width           =   8340
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   120
         MaxLength       =   68
         TabIndex        =   14
         Top             =   1920
         Visible         =   0   'False
         Width           =   8340
      End
      Begin RichTextLib.RichTextBox txtDetalhamento_Visualizar 
         Height          =   3615
         Left            =   120
         TabIndex        =   21
         Top             =   1080
         Width           =   8295
         _ExtentX        =   14631
         _ExtentY        =   6376
         _Version        =   393217
         Enabled         =   -1  'True
         ScrollBars      =   2
         TextRTF         =   $"FrmConsultaSinistrocon.frx":0000
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.PictureBox picSombra 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3660
         Left            =   120
         ScaleHeight     =   3600
         ScaleWidth      =   8220
         TabIndex        =   31
         Top             =   1080
         Visible         =   0   'False
         Width           =   8280
      End
      Begin RichTextLib.RichTextBox txtDetalhamento_Anterior 
         Height          =   1575
         Left            =   6600
         TabIndex        =   35
         Top             =   3840
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   2778
         _Version        =   393217
         Enabled         =   -1  'True
         ScrollBars      =   2
         TextRTF         =   $"FrmConsultaSinistrocon.frx":0080
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Framecriterios 
      Caption         =   "Crit�rios para Sele��o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   8535
      Begin VB.CheckBox chkTecnicosAlcadaMenor 
         Caption         =   "Somente t�cnicos com al�ada menor"
         Height          =   195
         Left            =   4200
         TabIndex        =   37
         Top             =   960
         Visible         =   0   'False
         Width           =   2920
      End
      Begin VB.ComboBox cmbSitSinistroAvaliador 
         Height          =   315
         ItemData        =   "FrmConsultaSinistrocon.frx":0100
         Left            =   4320
         List            =   "FrmConsultaSinistrocon.frx":0102
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1185
         Width           =   2415
      End
      Begin VB.ComboBox cmbTecnico 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1185
         Width           =   3975
      End
      Begin VB.TextBox txtConteudo 
         Height          =   315
         Left            =   120
         TabIndex        =   1
         Top             =   1185
         Width           =   3975
      End
      Begin VB.CommandButton btnPesquisar 
         Caption         =   "&Pesquisar"
         Height          =   375
         Left            =   6975
         TabIndex        =   4
         Top             =   1185
         Width           =   1335
      End
      Begin VB.ComboBox cmbCriterioPesquisa 
         Height          =   315
         ItemData        =   "FrmConsultaSinistrocon.frx":0104
         Left            =   120
         List            =   "FrmConsultaSinistrocon.frx":0106
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   480
         Width           =   3975
      End
      Begin VB.Label lblSituacaoSinistro 
         Caption         =   "Situa��o do Sinistro:"
         Height          =   255
         Left            =   4320
         TabIndex        =   12
         Top             =   960
         Width           =   2295
      End
      Begin VB.Label lblUsuario 
         Caption         =   "Usu�rio:"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   960
         Width           =   975
      End
      Begin VB.Label lblPesquisaPor 
         Caption         =   "Pesquisar por:"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label lblConteudo 
         Caption         =   "Conte�do"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   960
         Width           =   1215
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   10155
      Width           =   8745
      _ExtentX        =   15425
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid grdSelecao 
      Height          =   1935
      Left            =   120
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   1920
      Width           =   8535
      _ExtentX        =   15055
      _ExtentY        =   3413
      _Version        =   393216
      Cols            =   20
      FillStyle       =   1
      SelectionMode   =   1
      FormatString    =   $"FrmConsultaSinistrocon.frx":0108
   End
   Begin VB.Label lblMensagem 
      Caption         =   "Sinistro com erro de comunica��o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   255
      Left            =   120
      TabIndex        =   40
      Top             =   3960
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.Label lblSelecionados 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   6000
      Width           =   4215
   End
End
Attribute VB_Name = "FrmConsultaSinistrocon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private lConexaoLocal                           As Integer
Private bytModoOperacao                         As Byte '1-Inclusao / 2-Alteracao / 3-Consulta
Private bValida_Tecnico                         As Boolean
Private sSinistro_Situacao                      As String
Private sDetalhamento_Visualizar                As String
Private bTp_Detalhamento                        As Byte
Private bTp_exigencia                           As Byte
Private sRestrito                               As String
Private bSelecionou                             As Boolean
Private programaSegp                                As Boolean

' Jos� Moreira (Nova Consultoria) - 26/12/2012 - 14620257 - Melhorias no processo de subscri��o
' Permitir abertura de proposta via chamada em outro programa
Public AbrirConsultaSinistro                       As Boolean
Dim sSinistro                                   As String

Public Tecnico_atual            As String 'cristovao.rodrigues 11/01/2013
Public bUsuario_Utilizador      As Boolean 'FLAVIO.BEZERRA 21/05/2013 - Variavel utilizada para identificar se o usuario foi o bloqueador do sinistro.
Private usuario_is_tecnico_ativo  As Boolean  'FLOW 18225198 - Petrauskas Jan/2016

'Isabeli Silva - Confitec SP - 2017-10-17
'MU-2017-036534 Cadastramento dos Corretores Exclusivos no SEGBR
Dim isCorretor As Boolean
Dim CorretorProposta As Boolean


Private Function ordenar_resultado(Optional ByVal intRamoID As Integer, Optional ByVal intProdutoID As Integer) As Boolean
'07/2019 - ordenar resultados
'verifica se � rural e se tipo de consulta permite a ordena��o

    Dim bRet As Boolean
    bRet = False
    
    If (bytTipoRamo = bytTipoRamo_Rural Or _
       (bytTipoRamo = bytTipoRamo_RE And (intRamoID = 61 Or intRamoID = 68)) Or _
       (bytTipoRamo = bytTipoRamo_Vida And ((intRamoID = 61 Or intRamoID = 68) Or (intProdutoID = 1226 Or intProdutoID = 1227)))) Then
       
      Select Case cmbCriterioPesquisa.Text
          Case "N�mero Sinistro":     bRet = True
          Case "N�mero Sinistro BB":  bRet = True
          Case "Proposta": bRet = True
          Case "Proposta BB":  bRet = True
      End Select
    End If
    ordenar_resultado = bRet
End Function

Private Function Valor_buscado(ByRef rs As ADODB.Recordset) As Boolean
'07/2019 - ordenar resultados
'verifica se valor retornado � igual ao valor buscado
Dim rsSinPropBB As New ADODB.Recordset
Dim strSql As String

Dim bRet As Boolean
bRet = False

Select Case cmbCriterioPesquisa.Text
    Case "N�mero Sinistro"
        bRet = Val(rs.Fields!sinistro_id & "") = Val(txtConteudo.Text)

    Case "N�mero Sinistro BB"
        strSql = ""
        strSql = strSql & " select sinistro_bb " & vbNewLine
        strSql = strSql & "   from seguros_db.dbo.sinistro_bb_tb with (nolock) " & vbNewLine
        strSql = strSql & "  where sinistro_id = " & rs.Fields!sinistro_id & vbNewLine
        strSql = strSql & "    and sinistro_bb = " & txtConteudo.Text & vbNewLine
        strSql = strSql & "    and dt_fim_vigencia is null " & vbNewLine
            
        Set rsSinPropBB = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, strSql, True)
        
        If Not rsSinPropBB.EOF Then
            bRet = Val(rsSinPropBB.Fields!Sinistro_BB & "") = Val(txtConteudo.Text)
        Else
            rsSinPropBB.Close
            Set rsSinPropBB = Nothing
            
            strSql = ""
            strSql = strSql & " select sinistro_bb " & vbNewLine
            strSql = strSql & "   from seguros_db.dbo.sinistro_bb_tb with (nolock) " & vbNewLine
            strSql = strSql & "  where sinistro_id = " & rs.Fields!sinistro_id & vbNewLine
            strSql = strSql & "    and sinistro_bb = " & txtConteudo.Text & vbNewLine
            
            Set rsSinPropBB = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, strSql, True)
            
            If Not rsSinPropBB.EOF Then
                bRet = Val(rsSinPropBB.Fields!Sinistro_BB & "") = Val(txtConteudo.Text)
            End If
        End If
        
        rsSinPropBB.Close
        Set rsSinPropBB = Nothing

    Case "Proposta"
        bRet = Val(rs.Fields!Proposta_id & "") = Val(txtConteudo.Text)
    Case "Proposta BB"
    
        strSql = ""
        strSql = strSql & " select proposta_bb from seguros_db.dbo.proposta_fechada_tb with (nolock) where proposta_id = " & rs.Fields!Proposta_id & vbNewLine
        strSql = strSql & "  union " & vbNewLine
        strSql = strSql & " select proposta_bb from seguros_db.dbo.proposta_adesao_tb with (nolock) where proposta_id = " & rs.Fields!Proposta_id & vbNewLine
            
        Set rsSinPropBB = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, strSql, True)
            
        If Not rsSinPropBB.EOF Then
            bRet = Val(rsSinPropBB.Fields!proposta_bb & "") = Val(txtConteudo.Text)
        End If
        
        rsSinPropBB.Close
        Set rsSinPropBB = Nothing
End Select

Valor_buscado = bRet
End Function
'

Private Sub btnAgenda_Click()
'OK
    Dim sSQL                                        As String
    Dim rsRecordSet                                 As ADODB.Recordset
    Dim iTecnico                                    As Integer
    Dim dRet                                        As Double
    Dim sTabela                                     As String
    Dim sParametro                                  As String
    Dim glAmbienteTemp                              As Integer
        
    On Error GoTo Erro
        
    sSQL = ""
    sSQL = sSQL & "Select Tecnico_ID"
    sSQL = sSQL & "  From Sinistro_Tecnico_tb  WITH (NOLOCK)  " 'mathayde
    sSQL = sSQL & " Where Sinistro_ID = " & gbldSinistro_ID
    sSQL = sSQL & "   And Dt_Fim_Vigencia Is Null "
    Set rsRecordSet = ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSQL, _
                                  True)
    
    If Not rsRecordSet.EOF Then
        iTecnico = rsRecordSet(0)
    Else
        MsgBox "T�cnico n�o encontrado.", 64, "Mensagem ao Usu�rio"
        Exit Sub
    End If
    rsRecordSet.Close
        

    sParametro = gbldSinistro_ID & ", " & iTecnico & ", " & Chr(34) & sCPF & Chr(34)
    sTabela = """##ajuda" & gsMac & """"

    sSQL = ""
    sSQL = sSQL & "Create Table " & sTabela & " (" & vbNewLine
    sSQL = sSQL & "                              Componente VarChar(300) Not Null" & vbNewLine
    sSQL = sSQL & "                            , Estado Char(1) Not Null )" & vbNewLine 'mathayde
    
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexao _
                      , False

     
    glAmbienteTemp = glAmbiente_id_seg2
    glAmbiente_id_seg2 = glAmbiente_id
    'mathayde
    'FLAVIO.BEZERRA - IN�CIO - 14/02/2013
    dRet = ExecutaPrograma("SEGP1132.exe" _
                         , "SEGP1132.exe" _
                         , gbldSinistro_ID & "," & iTecnico & "," & Chr(34) & gsCPF & Chr(34) _
                         , vbNormalFocus, sAplicacao)
    
    'dRet = Shell(gsPastaLocalSegbr & "SEGP1132.exe" & " " & gbldSinistro_ID & "," & iTecnico & "," & Chr(34) & gsCPF & Chr(34) & " " & Monta_Parametros("SEGP1132.exe"), vbNormalFocus)
    'FLAVIO.BEZERRA - FIM - 14/02/2013
    glAmbiente_id_seg2 = glAmbienteTemp
    
    sSQL = ""
    sSQL = sSQL & "Drop Table " & sTabela
    
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexao _
                      , False

    Exit Sub

Erro:
  Call TrataErroGeral("btnAgenda_Click", Me.name)
  Call TerminaSEGBR

End Sub

Private Sub btnAnaliseTecnica_Click()
'FLAVIO.BEZERRA - IN�CIO - 07/01/2013
    Dim Parametro As String

    Parametro = grdSelecao.TextMatrix(grdSelecao.RowSel, 1) & " " & glAmbiente_id
    
    Call Shell(App.PATH & "\SPFP0002.exe " & Parametro)
'FLAVIO.BEZERRA - FIM - 07/01/2013
End Sub

Private Sub btnAprovar_Click()
'OK
    Dim lAmbiente                                   As Integer
    Dim sSQL                                        As String
    Dim rsRecordSet                                 As ADODB.Recordset
    
    On Error GoTo Erro
    
    If BtnOk.Enabled Then
        MsgBox "O detalhamento foi alterado. Clique em Aplicar antes de aprovar pagamentos."
        Exit Sub
    End If

    
'    If strTipoProcesso <> strTipoProcesso_Avisar And _
'       strTipoProcesso <> strTipoProcesso_Consulta And _
'       strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
'       strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
'        If Not GravaDados_BloqueiaSinistro(True, gbldSinistro_ID) Then Exit Sub     'FLAVIO.BEZERRA - 17/04/2013
'    End If
'    MousePointer = vbHourglass 'AKIO.OKUNO - 23/10/2012 - INSERIDO EM ValidaDados_Pagamento_Pendente

    If ValidaDados_Pagamento_Pendente Then
'        MousePointer = vbNormal 'AKIO.OKUNO - 23/10/2012 - INSERIDO EM ValidaDados_Pagamento_Pendente
        MsgBox "N�o foram encontrados pagamentos pendentes!", vbOKOnly, "Aten��o!"
    Else
        glAmbiente_id_seg2 = glAmbiente_id
        
        'FLAVIO.BEZERRA - IN�CIO - 18/02/2013
'        dRet = ExecutaAplicacao("SEGP0247" _
'                              , 0 _
'                              , "IS " & Format(gbldSinistro_ID, "00000000000") & _
'                                Format(gbllRamo_ID, "000") & _
'                                Format(gbllApolice_ID, "000000000") & _
'                                Format(gbllSucursal_Seguradora_ID, "00000") & _
'                                Format(gbllSeguradora_Cod_Susep, "00000") _
'                              , "" _
'                               )
        dRet = ExecutaPrograma("SEGP0247" _
                        , "SEGP0247" & ".EXE" _
                              , "IS " & Format(gbldSinistro_ID, "00000000000") & _
                                Format(gbllRamo_ID, "000") & _
                                Format(gbllApolice_ID, "000000000") & _
                                Format(gbllSucursal_Seguradora_ID, "00000") & _
                                Format(gbllSeguradora_Cod_Susep, "00000") _
                        , vbNormalFocus _
                        , "SEGP0247")
        'FLAVIO.BEZERRA - FIM - 18/02/2013
        glAmbiente_id_seg2 = lAmbiente
             
    End If
    
    'Screen.MousePointer = Normal  'AKIO.OKUNO 22/10/2012
'    Screen.MousePointer = vbNormal 'AKIO.OKUNO - 23/10/2012 - INSERIDO EM ValidaDados_Pagamento_Pendente

'msalema - 23/04/2013
'If Not GravaDados_BloqueiaSinistro(False, gbldSinistro_ID) Then Exit Sub
'msalema - 23/04/2013


    Exit Sub

Erro:
    Call TrataErroGeral("btnAprovar_Click", Me.name)
    Call TerminaSEGBR
  
End Sub

Private Sub btnAssessoria_Click()
''    mathayde
''    frmAtribuicaoAssessoria.Show 1

    Dim iParametroParaChamadaAuxiliar As Integer
    Dim sSinitroID As String

    sSinitroID = gbldSinistro_ID


    'FLAVIO.BEZERRA - IN�CIO - 15/02/2013
    '    If strTipoProcesso = strTipoProcesso_Consulta Or _
         '       strTipoProcesso = strTipoProcesso_Encerrar Or _
         '       strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
         '       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
    '        iParametroParaChamadaAuxiliar = 3
    '    Else
    '        iParametroParaChamadaAuxiliar = 1
    '    End If

    'ExecutaAplicacao "SEGP1298", 1, sSinitroID, ""
    If PegaJanela(gblAuxiliarExecutando) = 0 Or gblAuxiliarExecutando = 0 Then
        ExecutaAplicacao "SEGP1298", 2, sSinitroID, Format(Data_Sistema, "dd/mm/yyyy")
    End If
    'FLAVIO.BEZERRA - FIM - 15/02/2013
End Sub

Private Sub btnConsultaApolice_Click()
    Dim sPropostaId As String
        
    If Dir(App.PATH + "\SEGP0223.exe") = "SEGP0223.exe" Then
       sPropostaId = grdSelecao.TextMatrix(grdSelecao.Row, 5)
       
       Call ExecutaPrograma("SEGP0223.exe", App.PATH & "\" & "SEGP0223.exe", _
                        Format(sPropostaId, "000000000") & "," & gsCPF, vbNormalFocus, 0)
       Me.WindowState = vbMinimized
    Else
         MsgBox "Programa SEGP0223.EXE n�o encontrado para a consulta de ap�lice!", vbCritical, "SEGP1285 - Avalia��o de sinistro com imagens"
    End If
    
End Sub
Private Sub btnConsultaImagemProposta_Click()
'ok
    Dim sPrograma                               As String
    Dim dRet                                    As Double
    
    On Error GoTo TrataErro
    
    Me.WindowState = 1
    
    'Chamando o link com o SCI
'    sPrograma = "SCIP0018.EXE" 'AKIO.OKUNO - 10/10/2012
    sPrograma = "SCIP0018"
    'FLAVIO.BEZERRA - IN�CIO - 18/02/2013
'    dRet = ExecutaAplicacao(sPrograma _
'                          , 0 _
'                          , Format(grdSelecao.TextMatrix(grdSelecao.Row, 4), "000000000") & "," & _
'                            grdSelecao.TextMatrix(grdSelecao.Row, 12) & "," & _
'                            Chr(34) & gsCPF & Chr(34) _
'                          , "" _
'                           )
    dRet = ExecutaPrograma(sPrograma _
                        , sPrograma & ".EXE" _
                          , Format(grdSelecao.TextMatrix(grdSelecao.Row, 5), "000000000") & ", " & _
                            grdSelecao.TextMatrix(grdSelecao.Row, 13) & ", " & _
                            Chr(34) & gsCPF & Chr(34) _
                        , vbNormalFocus _
                        , sPrograma)
    'FLAVIO.BEZERRA - FIM - 18/02/2013
    Exit Sub
    
TrataErro:
    Call TrataErroGeral("btnConsultaImagemProposta_Click", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub btnConsultaProposta_Click()
'ok
    Dim sPrograma                               As String
    Dim glAmbienteTemp                          As Integer
    Dim dRet                                    As Double
    
    On Error GoTo TrataErro
    
    Select Case bytTipoRamo 'bytTipo_Ramo - MATHAYDE
        Case bytTipoRamo_Vida 'bytTipo_Ramo_Vida - MATHAYDE
            sPrograma = "SEGP0176"
        Case bytTipoRamo_RE ' bytTipo_Ramo_RE - MATHAYDE
            sPrograma = "SEGP0162"
        Case bytTipoRamo_Rural 'bytTipo_Ramo_Rural - MATHAYDE
            'MATHAYDE
            sPrograma = "SEGP0162"
    End Select
    
    If LenB(Trim(sPrograma)) <> 0 Then
        If ValidaPermissao(gsCPF, Mid$(sPrograma, 1, 8)) = False Then
            MsgBox "Usu�rio n�o tem acesso ao recurso " & Mid$(sPrograma, 1, 8) & ".", vbInformation, "Aten��o"
        Else
            glAmbienteTemp = glAmbiente_id_seg2
            glAmbiente_id_seg2 = glAmbiente_id
            
            'FLAVIO.BEZERRA - IN�CIO - 18/02/2013
            'gsCPF = "09970561774"       'AKIO
'            dRet = ExecutaAplicacao(sPrograma, 0, Format(grdSelecao.TextMatrix(grdSelecao.Row, 4), "000000000") & "," & Chr(34) & gsCPF & Chr(34))
            dRetorno = ExecutaPrograma(sPrograma _
                                    , sPrograma & ".EXE" _
                                    , Format(grdSelecao.TextMatrix(grdSelecao.Row, 5), "000000000") & "," & Chr(34) & gsCPF & Chr(34) _
                                    , vbNormalFocus _
                                    , sPrograma)
            glAmbiente_id_seg2 = glAmbienteTemp
            btnConsultaProposta.Enabled = False
            'FLAVIO.BEZERRA - FIM - 18/02/2013
        End If
    Else
        MsgBox "Usu�rio n�o tem acesso ao recurso " & Mid$(sProgramaExecutado, 1, 8) & ".", vbInformation, "Aten��o"
    End If
    Exit Sub
    
TrataErro:
    Call TrataErroGeral("btnConsultaProposta", Me.name)
    Call FinalizarAplicacao
    
End Sub

Private Function ValidaPermissao(ByVal sCPF As String, ByVal sPrograma As String) As Boolean
    Dim sSQL                                    As String
    Dim rsRecordSet                             As ADODB.Recordset
    Dim sServidor                               As String
    
    On Error GoTo Erro
        
    If glAmbiente_id <> 2 And glAmbiente_id <> 3 Then
        sServidor = "ABSS."
    End If
    sSQL = ""
    'Ricardo Toledo (Confitec) : IM00019174 : 28/03/2017 : INI
    sSQL = sSQL & "Exec " & sServidor & "SegAB_Db.dbo.Valida_Permissao_login_Rede_SPS '" & Mid(Trim(sCPF), 1, 11) & "'"
    'sSQL = sSQL & "Exec " & sServidor & "SegAB_Db.dbo.Valida_Permissao_login_Rede_SPS '" & sCPF & "'"
    'Ricardo Toledo (Confitec) : IM00019174 : 28/03/2017 : FIM
    sSQL = sSQL & "                                             , '" & sPrograma & "'"
    sSQL = sSQL & "                                             , " & glAmbiente_id
    sSQL = sSQL & ""
    
    Set rsRecordSet = ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSQL, _
                                  True)
                    
    If rsRecordSet.EOF Then
        ValidaPermissao = False
    Else
        ValidaPermissao = True
    End If
    
    Exit Function

Erro:
    Call TrataErroGeral("ValidaPermissao", Me.name)
    Call FinalizarAplicacao


End Function

Public Function GravaDados_BloqueiaSinistro(bBloqueia As Boolean, dSinistro_ID As Double) As Boolean  'FLAVIO.BEZERRA - 17/04/2013
    Dim sSQL                                    As String
    Dim RsBloqueado                             As String       'FLAVIO.BEZERRA - 17/04/2013
    
    On Error GoTo TrataErro
    
    GravaDados_BloqueiaSinistro = True
    'FLAVIO.BEZERRA - INICIO - 17/04/2013
    'If bBloqueia Then
    If bBloqueia And Not bUsuario_Utilizador Then 'FLAVIO.BEZERRA - 21/05/2013
      
'+----------------------------------------------------------------------
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'|
        sSQL = "SET NOCOUNT ON  Exec SEGS13114_SPS  " & gbldSinistro_ID
        
'        sSQL = "SELECT lock_aviso FROM sinistro_tb  WITH (NOLOCK)  " & _
'               " WHERE sinistro_id = " & gbldSinistro_ID
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'+----------------------------------------------------------------------
               
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    sSQL, _
                                    lConexaoLocal, _
                                    True)
        
        If rs(0) = "s" Then
            MsgBox "Este sinistro est� sendo atualizado por outro usu�rio!", vbOKOnly
            GravaDados_BloqueiaSinistro = False
            Screen.MousePointer = vbNormal      'AKIO.OKUNO - 06/05/2013
            
            Exit Function
        End If
        rs.Close
    End If
    'FLAVIO.BEZERRA - FIM - 17/04/2013
    
    If bBloqueia Then
    'msalema - 23/04/2013
        'sSQL = "EXEC sinistro_marca_spu " & dSinistro_ID & ",0,0,0,0"
        sSQL = "EXEC SEGS11074_SPU " & dSinistro_ID & ",0,0,0,0"
        bUsuario_Utilizador = True 'FLAVIO.BEZERRA - 21/05/2013
    'msalema - 23/04/2013
    Else
        sSQL = "EXEC sinistro_desmarca_spu " & _
                    " @sinistro_id = " & dSinistro_ID & _
                    ", @apolice_id = 0, @sucursal_seguradora_id = 0, @seguradora_cod_susep = 0, @ramo_id = 0"
                    
        'RCA 485 - ignora se status_sistema estiver bloqueado
        sSQL = sSQL & ", @ignorabloqueio = 1"
                
        bUsuario_Utilizador = False 'FLAVIO.BEZERRA - 21/05/2013
    End If
    Conexao_ExecutarSQL gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        sSQL, _
                        lConexaoLocal, _
                        False
    Exit Function
    
TrataErro:
    Call TrataErroGeral("GravaDados_BloqueiaSinistro", Me.name)
    Call FinalizarAplicacao

End Function

Private Sub btnConsultaPropostaInicial_Click()
    Dim sPropostaId As String
    
    If Dir(App.PATH + "\SEGP0176.exe") = "SEGP0176.exe" Then
        Dim sSQL As String
        Dim rsProposta As Recordset
        
        sSQL = "EXEC SEGS11699_SPS " & grdSelecao.TextMatrix(grdSelecao.Row, 5)
        Set rsProposta = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                   glAmbiente_id, _
                                   App.Title, _
                                   App.FileDescription, _
                                   sSQL, _
                                   lConexaoLocal, True)
        sPropostaId = rsProposta(0)
        
        Call ExecutaPrograma("SEGP0176.exe", App.PATH & "\" & "SEGP0176.exe", _
                  "CLIP" & glAmbiente_id & "," & Format(sPropostaId, "000000000") & "," & gsCPF, vbNormalFocus, 0)
        Me.WindowState = vbMinimized
    Else
         MsgBox "Programa SEGP0176.EXE n�o encontrado para a consulta de ap�lice!", vbCritical, "SEGP1285 - Avalia��o de sinistro com imagens"
    End If
    
End Sub
Private Sub btnDetalhar_Click()
'OK
    Dim sSQL As String
    Dim Linha As String

    On Error GoTo Erro

    If btnDetalhar.Caption = "Cancelar" Then
        If txtDetalhamento_Visualizar.Text <> sDetalhamento_Visualizar Then
            If MsgBox("Abandona as altera��es efetuadas ?", vbYesNo) = vbYes Then
                InicializaInterface_Detalhamento False

                If strTipoProcesso <> strTipoProcesso_Avisar And _
                   strTipoProcesso <> strTipoProcesso_Consulta And _
                   strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
                   strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
                    If Not GravaDados_BloqueiaSinistro(False, grdSelecao.TextMatrix(grdSelecao.Row, 1)) Then Exit Sub    'FLAVIO.BEZERRA - 17/04/2013
                End If

                'Retorna o texto original
                '                If sDetalhamento_Visualizar <> "" Then                                     'AKIO.OKUNO - 04/10/2012
                '                    Linha = Format(Data_Sistema, "dd/mm/yyyy")                             'AKIO.OKUNO - 04/10/2012
                '                    Linha = Linha & "  -  USU�RIO:   " & cUserName & Chr(10) & Chr(10)     'AKIO.OKUNO - 04/10/2012
                '                    Linha = Linha & sDetalhamento_Visualizar & String(125, "-") & Chr(10)  'AKIO.OKUNO - 04/10/2012
                '                Else                                                                       'AKIO.OKUNO - 04/10/2012
                Linha = ""                                                             'AKIO.OKUNO - 04/10/2012
                txtDetalhamento_Anterior.Text = ""                                     'FLAVIO.BEZERRA - 21/05/2013
                '                    Linha = sDetalhamento_Visualizar                                        'AKIO.OKUNO - 04/10/2012
                '                End If                                                                     'AKIO.OKUNO - 04/10/2012
                With txtDetalhamento_Visualizar
                    '.Text = vDetalhamento_VisualizarAnterior & Linha & Chr(10)
                    .Text = gblsDetalhamento_VisualizarAnt & Linha & Chr(10)     'FLAVIO.BEZERRA - 14/01/2013
                    .SelStart = 0
                    .SelLength = Len(.Text)
                    .SelColor = vbGrayText
                    .SelLength = 0
                End With
            Else                'AKIO.OKUNO - 04/10/2012
                GoTo Saida      'AKIO.OKUNO - 04/10/2012
            End If
        Else
            InicializaInterface_Detalhamento False

            If strTipoProcesso <> strTipoProcesso_Avisar And _
               strTipoProcesso <> strTipoProcesso_Consulta And _
               strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
               strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
                If Not GravaDados_BloqueiaSinistro(False, grdSelecao.TextMatrix(grdSelecao.Row, 1)) Then Exit Sub    'FLAVIO.BEZERRA - 17/04/2013
            End If

            With txtDetalhamento_Visualizar
                If vDetalhamento_Visualizar <> "" Then
                    Linha = Format(Data_Sistema, "dd/mm/yyyy")
                    Linha = Linha & "  -  USU�RIO:   " & cUserName & Chr(10) & Chr(10)
                    Linha = Linha & sDetalhamento_Visualizar & String(125, "-") & Chr(10)
                    .Text = vDetalhamento_VisualizarAnterior & Linha & Chr(10)
                Else
                    .Text = vDetalhamento_VisualizarAnterior
                End If

                .SelStart = 0
                .SelLength = Len(.Text)
                .SelColor = vbGrayText
                .SelLength = 0
            End With
        End If
        optAnotacao_Click
        btnSinistro.Caption = "Sinistro"
        btnPagamento.Visible = True
        btnAprovar.Visible = True
    Else    ' Detalhar
        '        InicializaInterface_Detalhamento True  'MSALEMA - 10/05/2013

        If strTipoProcesso <> strTipoProcesso_Avisar And _
           strTipoProcesso <> strTipoProcesso_Consulta And _
           strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
           strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
            If Not GravaDados_BloqueiaSinistro(True, grdSelecao.TextMatrix(grdSelecao.Row, 1)) Then Exit Sub    'FLAVIO.BEZERRA - 17/04/2013
        End If

        InicializaInterface_Detalhamento True  'MSALEMA - 10/05/2013

        txtDetalhamento_Visualizar.Text = txtDetalhamento_Visualizar.Text & txtDetalhamento_Anterior.Text  'cristovao.rodrigues 03/01/2013

        txtDetalhamento_Visualizar.Locked = False

        btnSinistro.Caption = "OK"
        btnPagamento.Visible = False
        btnAprovar.Visible = False
    End If

Saida:        'AKIO.OKUNO - 04/10/2012
    Exit Sub
    Resume
Erro:
    Call TrataErroGeral("btnDetalhar_Click", Me.name)
    Call FinalizarAplicacao


End Sub

Private Sub btnOK_Click()

'PegaDadosProposta

    Select Case strTipoProcesso
    Case strTipoProcesso_Avaliar_Com_Imagem

        'Paulo Pelegrini - MU-2017-049845 - 16/07/2018 (INI)
        If frm_hist_prazo_regulacao.Visible = True Then
            FrmConsultaSinistrocon.txtDetalhamento_Visualizar.Text = CarregaDados_DetalhamentoSinistro
            frm_hist_prazo_regulacao.Hide
        End If
        'Paulo Pelegrini - MU-2017-049845 - 16/07/2018 (FIM)

        ' Gravar as altera��es efetuadas
        If GravaDados_Detalhamento Then
            ' Mostra mensagem de sinistro alterado
            frmFim.Show vbModal
            BtnOk.Enabled = False
            gblsDetalhamento_VisualizarAnt = ""             'FLAVIO.BEZERRA - 21/05/2013
            txtDetalhamento_Anterior = ""                   'FLAVIO.BEZERRA - 21/05/2013
            CarregaDadosTela_Detalhamento                   'FLAVIO.BEZERRA - 21/05/2013
            '               grdSelecao_DblClick            'AKIO.OKUNO - 17/04/2013
        End If
        'msalema - 23/04/2013
        If Not GravaDados_BloqueiaSinistro(False, gbldSinistro_ID) Then Exit Sub
        'msalema - 23/04/2013
    Case Else
        With grdSelecao
            If .Rows = 1 Then
                StatusBar1.SimpleText = "N�o existe(m) item(s) para esta sele��o."
            Else
                If EstadoConexao(lConexaoLocal) = adStateClosed Then
                    lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
                End If

                vNunCx = lConexaoLocal      'AKIO.OKUNO - 30/04/2013

                'AKIO.OKUNO - INICIO - 29/09/2012
                gbllApolice_ID = "0" & .TextMatrix(.Row, 4)
                gbllProposta_ID = "0" & .TextMatrix(.Row, 5)
                gbllRamo_ID = "0" & .TextMatrix(.Row, 14)
                If strTipoProcesso <> strTipoProcesso_Avisar Then
                    If strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then
                        gbldSinistro_ID = "0" & .TextMatrix(.Row, 1)
                    End If
                End If
                gbldDt_Ocorrencia_Sinistro = IIf(.TextMatrix(.Row, 15) = "", Data_Sistema, .TextMatrix(.Row, 15))
                gbllProduto_ID = "0" & .TextMatrix(.Row, 13)
                'AKIO.OKUNO - FIM - 29/09/2012

                'AKIO.OKUNO - 21/04/2013 - INICIO
                'TRASNPORTADO PARA DENTRO DO IF ABAIXO
                '                    If strTipoProcesso <> strTipoProcesso_Avisar And _
                                     '                       strTipoProcesso <> strTipoProcesso_Consulta And _
                                     '                       strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
                                     '                       strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
                '                        If Not GravaDados_BloqueiaSinistro(True, gbldSinistro_ID) Then Exit Sub 'FLAVIO.BEZERRA - 17/04/2013
                '                    End If
                'AKIO.OKUNO - 21/04/2013 - FIM
                If ValidaDados_SinistroSelecionado Then
                    'AKIO.OKUNO - INICIO - RETIRADO DE FORA DO IF E INSERIDO PARA DENTRO DA VALIDA��O, POIS SOMENTE PODER� OCORRER O BLOQUEIO SE A VALIDA��O OCORRER COM SUCESSO. - 21/04/2013
                    If strTipoProcesso <> strTipoProcesso_Avisar And _
                       strTipoProcesso <> strTipoProcesso_Consulta And _
                       strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
                       strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
                        If Not GravaDados_BloqueiaSinistro(True, gbldSinistro_ID) Then Exit Sub
                    End If
                    'AKIO.OKUNO - FIM - 21/04/2013
                    FrmConsultaSinistro.Show
                Else
                    With txtConteudo
                        .SelStart = 0
                        .SelLength = Len(.Text)
                        If .Visible Then
                            .SetFocus
                        End If
                    End With
                End If
            End If
        End With
    End Select
End Sub

Private Sub PegaDadosProposta()

'gbldSinistro_ID = 0
'gbllSucursal_Seguradora_ID = .TextMatrix(.Rows - 1, 5)
'gbllSeguradora_Cod_Susep = seguradora_cod_susep
'gbllApolice_ID = .TextMatrix(.Rows - 1, 3)
'gbllRamo_ID = .TextMatrix(.Rows - 1, 7)
'gbllProposta_ID = .TextMatrix(.Rows - 1, 4)
'Evento_Sinistro_ID = 0
'SubEvento_Sinistro_ID = 0
'Dt_Ocorrencia_Sinistro = Null
'Dt_Entrada_Seguradora = null
'Dt_Aviso_Sinistro = null
'Dt_Inclusao= null
'Agencia_ID = null
'Solicitante_ID= 0
'Situacao
'Endereco = null
'Bairro = null
'Municipio
'Estado = null

End Sub

Public Function GravaDados_Detalhamento(Optional Motivo_encerramento As String) As Boolean
    
    ' bTp_Detalhamento:
    '------------------
    ' 0- Anota��o
    ' 1- Exig�ncia
    ' 2- Recibo
    ' 3- Indeferimento  MsgBox "A Exig�ncia dever� ser emitida Manualmente no SEGUR.", vbOKOnly + vbInformation
    ' 4- Solicita��o de cancelamento
    ' 5- Comunica��es feitas pelo GTR
    
    '---------------------------'
    ' vTp_exigencia:            '
    '---------------------------'
    ' 0-Documentos(evento 1130) '
    ' 1-Outros(evento 1131)     '
    '---------------------------'
    Dim Testa_Detalhe                           As String
    Dim rsRecordSet                             As ADODB.Recordset
    Dim Linha_id                                As Integer
    Dim Linha                                   As String
    Dim Pos_final                               As Integer
    Dim Pos_inicial                             As Integer
    Dim Itens_historico(4)                      As String
    Dim Evento_SEGBR_id                         As String

    On Error GoTo Trata_Erro
    
    GravaDados_Detalhamento = True
    
    If gbllProduto_ID >= 711 And gbllProduto_ID <= 715 Then
        sDetalhamento_Visualizar = sDetalhamento_Visualizar & "Proposta Contratada Via Internet"
    End If
    
    Testa_Detalhe = Replace(sDetalhamento_Visualizar, Chr(13), "")
    Testa_Detalhe = Replace(Testa_Detalhe, Chr(10), "")
    If Trim(Testa_Detalhe) = "" Then
        If Motivo_encerramento <> "" Then
            MsgBox "� obrigat�rio o preenchimento do detalhamento !", vbOKOnly + vbCritical
            GravaDados_Detalhamento = False
        End If
        Exit Function ' Sai da rotina
    End If

    sSQL = ""
    'mathayde
    'sSQL = sSQL & "Exec " & Ambiente & ".Sinistro_Detalhamento_SPI " & vbNewLine
    sSQL = sSQL & "SET NOCOUNT On" & vbNewLine      'AKIO.OKUNO - 24/09/2012
    sSQL = sSQL & "Exec seguros_db.dbo.Sinistro_Detalhamento_SPI " & vbNewLine
    sSQL = sSQL & "     " & gbldSinistro_ID & vbNewLine
    sSQL = sSQL & "   , " & gbllApolice_ID & vbNewLine
    sSQL = sSQL & "   , " & gbllSucursal_Seguradora_ID & vbNewLine
    sSQL = sSQL & "   , " & gbllSeguradora_Cod_Susep & vbNewLine
    sSQL = sSQL & "   , " & gbllRamo_ID & vbNewLine
    sSQL = sSQL & "   , '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
    sSQL = sSQL & "   , " & bTp_Detalhamento & vbNewLine
    sSQL = sSQL & "   , '" & sRestrito & "'" & vbNewLine
    sSQL = sSQL & "   , '" & cUserName & "'"
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         sSQL, _
                                         lConexaoLocal, True)
       
    Detalhamento_id = rsRecordSet.Fields(0)
    
    Set rsRecordSet = Nothing


    Converte_vbNewLine_para_10 sDetalhamento_Visualizar
        
    If Right(sDetalhamento_Visualizar, 1) <> Chr(10) Then
        sDetalhamento_Visualizar = sDetalhamento_Visualizar & Chr(10)
    End If
        
    ' Trata a primeira linha
    Pos_final = InStr(Pos_inicial + 1, sDetalhamento_Visualizar, Chr(10))
    Pos_inicial = 0
    Linha_id = 1
    
    While Pos_final <> 0
        Linha = Mid(sDetalhamento_Visualizar, Pos_inicial + 1, Pos_final - Pos_inicial - 1)
        Pos_inicial = Pos_final
        Pos_final = InStr(Pos_inicial + 1, sDetalhamento_Visualizar, Chr(10))
            
        sSQL = ""
        'mathayde
        'sSQL = sSQL & "Exec " & Ambiente & ".Sinistro_Linha_Detalhe_SPI " & vbNewLine
        sSQL = sSQL & "Exec seguros_db.dbo.Sinistro_Linha_Detalhe_SPI " & vbNewLine
        sSQL = sSQL & "     " & gbldSinistro_ID & vbNewLine
        sSQL = sSQL & "   , " & gbllApolice_ID & vbNewLine
        sSQL = sSQL & "   , " & gbllSucursal_Seguradora_ID & vbNewLine
        sSQL = sSQL & "   , " & gbllSeguradora_Cod_Susep & vbNewLine
        sSQL = sSQL & "   , " & gbllRamo_ID & vbNewLine
        sSQL = sSQL & "   , " & Detalhamento_id & vbNewLine
        sSQL = sSQL & "   , " & Linha_id & vbNewLine
        sSQL = sSQL & "   , '" & MudaAspaSimples(Linha) & "'" & vbNewLine
        sSQL = sSQL & "   , '" & cUserName & "'"
        
                          Conexao_ExecutarSQL gsSIGLASISTEMA, _
                                             glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             sSQL, _
                                             lConexaoLocal, False
        Linha_id = Linha_id + 1
    Wend
        
    If (bTp_Detalhamento = 1) Or (bTp_Detalhamento = 3) Or (bTp_Detalhamento = 4) Then
        If bTp_Detalhamento = 1 Then
            If bTp_exigencia = 0 Then
               ' Pedido de complementa��o de documenta��o - com anexo
               Evento_SEGBR_id = "10032"
            Else
               ' Pedido de complementa��o de documenta��o - sem anexo
               Evento_SEGBR_id = "10127"
            End If
            
            If bGTR_Implantado Then
                MsgBox "A Exig�ncia ser� emitido Autom�ticamente pelo GTR.", vbOKOnly + vbInformation
            Else
                GravaDados_Detalhamento = False
                MsgBox "A Exig�ncia dever� ser emitida Manualmente no SEGUR.", vbOKOnly + vbInformation
                BtnOk.Enabled = False       'AKIO.OKUNO - 05/11/2012
                Exit Function
            End If
            
            Motivo_encerramento = "NULL"
        ElseIf bTp_Detalhamento = 3 Then ' (indeferimento)
            If Motivo_encerramento = "9" Then
               ' Indeferimento Administrativo
               Evento_SEGBR_id = "10131"
            Else
               ' Elabora��o de carta de indeferimento
               Evento_SEGBR_id = "10027"
            End If
        Else ' bTp_Detalhamento = 4 (Solicita��o de cancelamento)
            ' Solicita��o de cancelamento de aviso de sinistro
            Evento_SEGBR_id = "10102"
        End If
        ' Grava no hist�rico o envio da exig�ncia
        If bGTR_Implantado Then
        
'VER PROCESSO DE INCLUS�O DO EVENTO
'                If Not Inclui_Evento(Evento_SEGBR_id, sSinistro_Situacao, Motivo_encerramento, Itens_historico, "", Detalhamento_ID) Then
'                    GravaDados_Detalhamento = False
'                    MsgBox "Erro na Inclus�o do Evento de Exig�ncia", vbOKOnly + vbCritical
'                    Exit Function
'                End If
        End If
    End If
    
    sDetalhamento_Visualizar = ""

    Exit Function
    
Trata_Erro:
    TrataErroGeral "GravaDados_Detalhamento", Me.name
    GravaDados_Detalhamento = False

End Function


Private Sub CarregaDadosTela_Detalhamento()
    On Error GoTo Erro

    
    txtDetalhamento_Visualizar = ""
    
    MousePointer = vbHourglass
    
'    SelecionaDados_Sinistro
    
'    SelecionaDados_Detalhamento        'AKIO.OKUNO 24/09/2012
    SelecionaDados_Detalhamento lConexaoLocal
    
    txtDetalhamento_Visualizar = CarregaDados_DetalhamentoSinistro

'    DestroiInterface
    
    bCarregou_AbaDetalhamento = True
    
    sDetalhamento_Visualizar = txtDetalhamento_Visualizar.Text

    MousePointer = vbNormal
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosTela_Detalhamento", Me.name)
    Call FinalizarAplicacao
    
End Sub

Private Sub DestroiInterface()
    Dim sSQL                                    As String
    
    sSQL = ""
    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Principal'),0) >0" & vbNewLine
    sSQL = sSQL & " if OBJECT_ID('tempdb..#Sinistro_Principal') IS NOT NULL " & vbNewLine
    sSQL = sSQL & " BEGIN" & vbNewLine
    sSQL = sSQL & "     DROP TABLE #Sinistro_Principal" & vbNewLine
    sSQL = sSQL & " END" & vbNewLine
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)

End Sub


Public Sub CarregaDadosTela_PrazoRegulacao()
'+-------------------------------------------------
'| FLOW 18225198 - INICIO
'| Petrauskas Jan/2016
'|

  If bytTipoRamo = bytTipoRamo_Vida Or bytTipoRamo = bytTipoRamo_Rural Or (gbllRamo_ID = 61) Or (gbllRamo_ID = 68) Or (gbllProduto_ID = 1226) Or (gbllProduto_ID = 1227) Then
    txtPrazoSeguradora.Text = "":   txtPrazoSeguradora.ToolTipText = ""
    txtPrazoSegurado.Text = "":     txtPrazoSegurado.ToolTipText = ""
    txtPrazoTotal.Text = "":        txtPrazoTotal.ToolTipText = ""
    txt_prazo_atual.Text = "":      txt_prazo_atual.ToolTipText = "":
    
    If grdSelecao.Rows > 1 Then
      
      sSQL = "SELECT prazo_seguradora, prazo_segurado, prazo_atual, dt_atualizacao " & vbCrLf _
           & "FROM seguros_db.dbo.prazos_contagem_sinistro_tb WITH (NOLOCK) " & vbCrLf _
           & "WHERE sinistro_id = " & grdSelecao.TextMatrix(grdSelecao.Row, 1)
    
      Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            sSQL, _
                                            lConexaoLocal, _
                                            True)
      If Not rsRecordSet.EOF Then
        txtPrazoSeguradora.Text = Format(rsRecordSet!prazo_seguradora, "#,##0")
        txtPrazoSegurado.Text = Format(rsRecordSet!prazo_segurado, "#,##0")
        txtPrazoTotal.Text = Format(rsRecordSet!prazo_seguradora + rsRecordSet!prazo_segurado, "#,##0")
        txt_prazo_atual.Text = Format(rsRecordSet!prazo_atual, "#,##0")
        
        txtPrazoSeguradora.ToolTipText = "Atualizado em " & Format(rsRecordSet!dt_atualizacao, "dd/mm/yyyy")
        txtPrazoSegurado.ToolTipText = txtPrazoSeguradora.ToolTipText
        txtPrazoTotal.ToolTipText = txtPrazoSeguradora.ToolTipText
        txt_prazo_atual.ToolTipText = txtPrazoSeguradora.ToolTipText
        
      End If
      rsRecordSet.Close: Set rsRecordSet = Nothing

    End If
  Else
'|
'| FLOW 18225198 - FIM
'+-------------------------------------------------
  
    With grdSelecao
        If .Rows > 1 Then
            sSQL = "" & vbNewLine
            sSQL = sSQL & "Set nocount ON" & Chr(13)
            sSQL = sSQL & "Exec SEGS9786_SPS " & .TextMatrix(.Row, 1)

            Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                  glAmbiente_id, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  sSQL, _
                                                  lConexaoLocal, _
                                                  True)

            If Not rsRecordSet.EOF Then
                txtPrazoSeguradora.Text = rsRecordSet.Fields!total_AB
                txtPrazoSegurado.Text = rsRecordSet.Fields!total_BB
                txtPrazoTotal.Text = Str(Val(txtPrazoSeguradora.Text) + Val(txtPrazoSegurado.Text))
            End If

            Set rsRecordSet = Nothing
        End If
    End With
End If
    MousePointer = vbNormal
    
    Exit Sub
    
TrataErro:
    Call TrataErroGeral("CarregaDadosTela_PrazoRegulacao", Me.name)
    Call FinalizarAplicacao
    
End Sub
Private Function ValidaDados_ParcelaAtraso_Hoje() As Boolean
    Dim sSQL                                    As String
    Dim rsRecordSet                             As ADODB.Recordset
        
    On Error GoTo TrataErro
    
    sSQL = ""
    sSQL = sSQL & "Select Count(Num_Cobranca) as Qtd "
'    sSQL = sSQL & "  From Agendamento_Cobranca_Tb  WITH (NOLOCK)  "       'AKIO.OKUNO - 17/04/2013
    sSQL = sSQL & "  From Agendamento_Cobranca_Atual_Tb  WITH (NOLOCK)  "  'AKIO.OKUNO - 17/04/2013
    sSQL = sSQL & " Where Proposta_ID = " & gbllProposta_ID
    Select Case bytTipoRamo
        Case bytTipoRamo_Vida
            sSQL = sSQL & "   and Dt_Agendamento <= '" & Format(gbldDt_Ocorrencia, "yyyymmdd") & "'"
        Case bytTipoRamo_RE
            sSQL = sSQL & "   and Dt_Agendamento <= '" & Format(Data_Sistema, "yyyymmdd") & "'"
    End Select
    sSQL = sSQL & "   and Dt_Recebimento is Null"
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    ValidaDados_ParcelaAtraso_Hoje = False
    
    If Not rsRecordSet.EOF Then
        If rsRecordSet.Fields!QTD > 0 Then
            ValidaDados_ParcelaAtraso_Hoje = True
        End If
    End If
    
    Set rsRecordSet = Nothing
    Exit Function
    
TrataErro:
    Call TrataErroGeral("ValidaDados_ParcelaAtraso_Hoje", Me.name)
    Call FinalizarAplicacao


End Function

Private Function ValidaDados_ParcelaAtraso_DataOcorrencia() As Boolean
    Dim sSQL                                    As String
    Dim rsRecordSet                             As ADODB.Recordset
        
    On Error GoTo TrataErro
    
    sSQL = ""
    sSQL = sSQL & "Select Count(Num_Cobranca) as Qtd "
    sSQL = sSQL & "  From Cobranca_Inadimplente_Tb  WITH (NOLOCK)  "
    sSQL = sSQL & " Where Proposta_ID = " & gbllProposta_ID
    sSQL = sSQL & "   and Dt_Cobranca <= '" & Format(gbldDt_Ocorrencia, "yyyymmdd") & "'"
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    ValidaDados_ParcelaAtraso_DataOcorrencia = False
    
    If Not rsRecordSet.EOF Then
        If rsRecordSet.Fields!QTD > 0 Then
            ValidaDados_ParcelaAtraso_DataOcorrencia = True
        End If
    End If
    
    Set rsRecordSet = Nothing
    Exit Function
    
TrataErro:
    Call TrataErroGeral("ValidaDados_ParcelaAtraso_DataOcorrencia", Me.name)
    Call FinalizarAplicacao


End Function

Private Function ValidaDados_Pagamento_Pendente() As Boolean
    Dim sSQL                                    As String
    Dim rsRecordSet                             As ADODB.Recordset
        
    On Error GoTo TrataErro
    
    MousePointer = vbHourglass  'AKIO.OKUNO - 23/10/2012
    
    sSQL = "" & vbNewLine
    sSQL = sSQL & "Select Sinistro_ID " & vbNewLine
    sSQL = sSQL & "  From Pgto_Sinistro_Tb  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & " Where Sinistro_ID = " & gbldSinistro_ID & vbNewLine
    sSQL = sSQL & "   and Situacao_OP = 'n'" & vbNewLine
    sSQL = sSQL & "   and Chave_Autorizacao_Sinistro is Not Null "
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    ValidaDados_Pagamento_Pendente = False
    
    If rsRecordSet.EOF Then
        ValidaDados_Pagamento_Pendente = True
    End If
    
    Set rsRecordSet = Nothing
    
    MousePointer = vbNormal     'AKIO.OKUNO - 23/10/2012
    
    Exit Function
    
TrataErro:
    Call TrataErroGeral("ValidaDados_Pagamento_Pendente", Me.name)
    Call FinalizarAplicacao


End Function


Private Function ValidaDados_EstimativaRejeitada() As Boolean
    Dim sSQL                                    As String
    Dim rsRecordSet                             As ADODB.Recordset
        
    On Error GoTo TrataErro
    
    sSQL = "" & vbNewLine
    sSQL = sSQL & "Select Count(Sinistro_Id) as Qtd " & vbNewLine
    sSQL = sSQL & "  from Pgto_Sinistro_TB  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & " Where Sinistro_Id = " & gbldSinistro_ID & vbNewLine
    sSQL = sSQL & "  and   Situacao_Op = 'R'" & vbNewLine
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    ValidaDados_EstimativaRejeitada = False
    
    If Not rsRecordSet.EOF Then
        If rsRecordSet.Fields!QTD > 0 Then
            ValidaDados_EstimativaRejeitada = True
        End If
    End If
    
    Set rsRecordSet = Nothing
    Exit Function
    
TrataErro:
    Call TrataErroGeral("ValidaDados_EstimativaRejeitada", Me.name)
    Call FinalizarAplicacao
    
End Function

Private Sub btnPagamento_Click()
'OK
    Dim sSQL As String
    Dim Valor_Estimado_Cobertura As Double
    Dim Num_Averbacao As String
    Dim rsRecordSet As ADODB.Recordset

    If BtnOk.Enabled Then
        MsgBox "O detalhamento foi alterado. Clique em 'Aplicar' antes de autorizar os pagamentos."
        Exit Sub
    End If

    'O SEGP0246 FAZIA ESTA VALIDA��O DA COBERTURA AFETADA
    '    SelecionaDados_Sinistro
    '
    '    SelecionaDados_AbaEstimativa_CoberturaAfetada
    '
    '    DestroiInterface

    If ValidaDados_EstimativaRejeitada Then
        mensagem_erro 6, "Existem pagamentos rejeitados. Cancele-os antes de autorizar."
        Exit Sub
    End If
    'REVER
    '    If (vTranspInternacional) And (Not Avulso) Then
    '        SQL = "SELECT num_averbacao "
    '        SQL = SQL & " FROM sinistro_re_tb  WITH (NOLOCK)  "
    '        SQL = SQL & " WHERE sinistro_id = " & Sin.sinistro_id
    '        Set rs = rdocn.OpenResultset(SQL)
    '        Num_Averbacao = ""
    '        If Not rs.EOF Then
    '            Num_Averbacao = "" & rs(0)
    '        End If
    '        rs.Close
    '
    '        If (Val(Sin.NumEndosso) = 0) Or (Val(Num_Averbacao) = 0) Then
    '            MsgBox "Para autorizar pagamentos � necess�rio informar o endosso " & _
                 '            "e averba��o definitivos."
    '            Exit Function
    '        End If
    '    End If
    '
    If bytTipoRamo = bytTipoRamo_RE Then
        '        If Existe_Resseguro(False) Then
        '            If Consulta_Pendencia_Analise_Resseguro(True) = False Then
        '                Exit Function
        '            End If
        '        End If
    End If

    If ValidaDados_ParcelaAtraso_Hoje Then
        MsgBox "Existe(m) parcela(s) em atraso para esta proposta, anterior(es) a data de hoje."
    Else
        If ValidaDados_ParcelaAtraso_DataOcorrencia Then
            MsgBox "Existe(m) parcela(s) em atraso para esta proposta, anterior(es) a data de ocorr�ncia do sinistro."
        End If
    End If

    MousePointer = vbHourglass

    bCarregou_Form_Aviso = False


    With grdSelecao
        If .Rows > 1 Then
            If strTipoProcesso <> strTipoProcesso_Avisar And _
               strTipoProcesso <> strTipoProcesso_Consulta And _
               strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
               strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
                If Not GravaDados_BloqueiaSinistro(True, gbldSinistro_ID) Then Exit Sub              'FLAVIO.BEZERRA - 17/04/2013
                gbldSinistro_ID = .TextMatrix(.Row, 1)
                frmEstimativa.Show 1
            End If
        End If
    End With

    MousePointer = vbNormal

    bSelecionou = False

End Sub

Private Sub btnSinistro_Click()

'ok
    Dim i As Integer
    Dim Temp_tp_detalhamento As Integer, Temp_restrito As String
    Dim bPreencheu_Exigencias As Boolean

    If btnSinistro.Caption = "Sinistro" Then
        If BtnOk.Enabled Then
            MsgBox "O detalhamento foi alterado. Clique em Aplicar antes de acessar o sinistro."
            Exit Sub
        End If

        MousePointer = 11

        If strTipoProcesso <> strTipoProcesso_Avisar And _
           strTipoProcesso <> strTipoProcesso_Consulta And _
           strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
           strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
            If Not GravaDados_BloqueiaSinistro(True, gbldSinistro_ID) Then Exit Sub  'FLAVIO.BEZERRA - 17/04/2013
        End If


        With FrmConsultaSinistro
            .Show
        End With

        MousePointer = 0

        '        Me.Enabled = False     'AKIO.OKUNO - 24/09/2012 - Conforme solicitado no report n.79
        'mathayde
        'Me.Hide
        Me.Hide         'AKIO.OKUNO - 24/09/2012 - Conforme solicitado no report n.79

        selecionou = False
    Else    ' Bot�o de Ok
        Temp_tp_detalhamento = IIf(optAnotacao.Value, 0, 1)
        Temp_restrito = IIf(chkRestrito.Value = 1, "S", "N")

        If optAnotacao.Value = True Then
            If ((txtDetalhamento_Visualizar = sDetalhamento_Visualizar) And (Temp_tp_detalhamento = bTp_Detalhamento) And (Temp_restrito = sRestrito)) Or LenB(Trim(txtDetalhamento_Visualizar)) = 0 Then
                mensagem_erro 6, "Nenhuma altera��o encontrada. Clique em cancelar !"
                Exit Sub
            End If
        Else
            bPreencheu_Exigencias = False
            For i = 0 To 3
                If Trim(txtExigencia(i).Text) <> "" Then
                    txtExigencia(i).Text = MudaAspaSimples(txtExigencia(i).Text)
                    bPreencheu_Exigencias = True
                End If
                Detalhe_Temp = txtExigencia(i).Text & Chr(10)

            Next i
            If ((Detalhe_Temp = sDetalhamento_Visualizar) And (Temp_tp_detalhamento = bTp_Detalhamento) And (Temp_restrito = sRestrito)) Or (Not bPreencheu_Exigencias) Then
                mensagem_erro 6, "Nenhuma altera��o encontrada. Clique em Cancelar !"
                Exit Sub
            End If
        End If

        bTp_Detalhamento = IIf(optAnotacao.Value, 0, 1)
        sRestrito = IIf(chkRestrito.Value = 1, "S", "N")
        '
        InicializaInterface_Detalhamento False

        If strTipoProcesso <> strTipoProcesso_Avisar And _
           strTipoProcesso <> strTipoProcesso_Consulta And _
           strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
           strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
            'msalema - 23/04/2013
            'If Not GravaDados_BloqueiaSinistro(False, gbldSinistro_ID) Then Exit Sub    'FLAVIO.BEZERRA - 17/04/2013
            'msalema - 23/04/2013
        End If

        btnPagamento.Visible = True
        btnAprovar.Visible = True
        Linha = ""
        If optAnotacao.Value = True Then
            txtDetalhamento_Visualizar.Text = MudaAspaSimples(txtDetalhamento_Visualizar.Text)
            numlinhas = mlcount(Me, txtDetalhamento_Visualizar.Text, 70)
            If numlinhas > 0 Then
                For i = 1 To numlinhas
                    Linha = Linha & AcertaLinha(Me, txtDetalhamento_Visualizar.Text, 70, i)
                    If i < numlinhas Then Linha = Linha & Chr(10)
                Next
            End If
        Else
            For i = 0 To 3
                txtExigencia(i).Text = MudaAspaSimples(txtExigencia(i).Text)
                Linha = Linha & txtExigencia(i).Text & Chr(10)
            Next
        End If
        While Right(Linha, 1) = Chr(10)
            Linha = Left(Linha, Len(Linha) - 1)
        Wend
        Linha = Linha & Chr(10)
        sDetalhamento_Visualizar = Linha
        'AKIO.OKUNO - INICIO - 26/10/2012
        '        Linha = Format(Data_Sistema, "dd/mm/yyyy") & _
                 '                "  -  USU�RIO:   " & cUserName & Chr(10) & Chr(10) & _
                 '                Linha & String(125, "-") & Chr(10)

        'FLAVIO.BEZERRA - IN�CIO - 21/01/2013
        '        Linha = Format(Data_Sistema, "dd/mm/yyyy") & _
                 '                "  -  USU�RIO:   " & cUserName & _
                 '                IIf(optRequisicao.Value, " (EXIG�NCIA PARA O CORRETOR)", "") & _
                 '                IIf(chkRestrito.Value = 1, " *** RESTRITO ***", "") & _
                 '                Chr(10) & Chr(10) & _
                 '                Linha & Chr(10) & _
                 '                String(70, "-") & Chr(10)

        Linha = Format(Data_Sistema, "dd/mm/yyyy") & _
              "  -  USU�RIO:   " & cUserName & _
                IIf(chkRestrito.Value = 1, " *** RESTRITO ***", "") & _
                Chr(10) & Chr(10) & _
                Linha & Chr(10) & _
                String(70, "-") & Chr(10)
        'FLAVIO.BEZERRA - FIM - 21/01/2013

        'AKIO.OKUNO - FIM - 26/10/2012
        txtDetalhamento_Anterior.Text = txtDetalhamento_Visualizar.Text    'cristovao.rodrigues 03/01/2013
        txtDetalhamento_Visualizar.Text = gblsDetalhamento_VisualizarAnt & RTrim(Linha) & Chr(10)

        With txtDetalhamento_Visualizar
            .SelStart = 0
            .SelLength = Len(.Text)
            .SelColor = vbGrayText
            .SelLength = 0
        End With
        optAnotacao_Click   'AKIO.OKUNO - 24/09/2012
        btnSinistro.Caption = "Sinistro"
        BtnOk.Enabled = True

        txtDetalhamento_Visualizar.SelStart = Len(txtDetalhamento_Visualizar.Text) - 1

    End If


End Sub

'cristovao.rodrigues 11/01/2013
Private Sub chkTecnicosAlcadaMenor_Click()
    Screen.MousePointer = vbHourglass
    DoEvents
    CarregaDadosCombo_Tecnicos
    Screen.MousePointer = vbDefault
    DoEvents
End Sub

Private Sub cmbCriterioPesquisa_Click()

MousePointer = vbHourglass
    
    If cmbCriterioPesquisa.Text = "Avisos Pendentes de Avalia��o" Or _
       cmbCriterioPesquisa.Text = "T�cnico Respons�vel" Or _
       cmbCriterioPesquisa.Text = "Sinistros Pendentes de Aprova��o" Or _
       cmbCriterioPesquisa.Text = "T�cnicos - Aprova��o" Then
                
        'cristovao.rodrigues 11/01/2013
        chkTecnicosAlcadaMenor.Visible = False
        cmbSitSinistroAvaliador.Visible = False
        lblSituacaoSinistro.Visible = False
        
        txtConteudo.Visible = False
        lblConteudo.Visible = False
        CarregaDadosCombo_Tecnicos
        cmbTecnico.Visible = True
        lblUsuario.Visible = True
        Select Case cmbCriterioPesquisa
            Case "Sinistros Pendentes de Aprova��o"
                'cmbTecnico.Visible = False cristovao.rodrigues 11/01/2013 comentado
                'lblUsuario.Visible = False
                chkTecnicosAlcadaMenor.Visible = True 'cristovao.rodrigues 11/01/2013 incluido
                chkTecnicosAlcadaMenor.Top = 1185
            Case "T�cnico Respons�vel"
                lblSituacaoSinistro.Visible = True
                cmbSitSinistroAvaliador.Visible = True
            Case Else
                lblSituacaoSinistro.Visible = False
                cmbSitSinistroAvaliador.Visible = False
        End Select
    Else
    
        txtConteudo.Visible = True
        lblConteudo.Visible = True
        cmbTecnico.Visible = False
        lblUsuario.Visible = False
        lblSituacaoSinistro.Visible = False
        cmbSitSinistroAvaliador.Visible = False
        chkTecnicosAlcadaMenor.Visible = False 'cristovao.rodrigues 11/01/2013
    End If
    
    MousePointer = vbNormal

End Sub

Private Sub cmbCriterioPesquisa_LostFocus()
    
'    MousePointer = vbHourglass
'
'    If cmbCriterioPesquisa.Text = "Avisos Pendentes de Avalia��o" Or _
'       cmbCriterioPesquisa.Text = "T�cnico Respons�vel" Or _
'       cmbCriterioPesquisa.Text = "Sinistros Pendentes de Aprova��o" Or _
'       cmbCriterioPesquisa.Text = "T�cnicos - Aprova��o" Then
'
'        txtConteudo.Visible = False
'        lblConteudo.Visible = False
'        CarregaDadosCombo_Tecnicos
'        cmbTecnico.Visible = True
'        lblUsuario.Visible = True
'        Select Case cmbCriterioPesquisa
'            Case "Sinistros Pendentes de Aprova��o"
'                cmbTecnico.Visible = False
'                lblUsuario.Visible = False
'            Case "T�cnico Respons�vel"
'                lblSituacaoSinistro.Visible = True
'                cmbSitSinistroAvaliador.Visible = True
'            Case Else
'                lblSituacaoSinistro.Visible = False
'                cmbSitSinistroAvaliador.Visible = False
'        End Select
'    Else
'
'        txtConteudo.Visible = True
'        lblConteudo.Visible = True
'        cmbTecnico.Visible = False
'        lblUsuario.Visible = False
'        lblSituacaoSinistro.Visible = False
'        cmbSitSinistroAvaliador.Visible = False
'
'    End If
'
'    MousePointer = vbNormal
'
End Sub


Private Sub cmd_prazo_seguradora_Click()
'-----------------------------------------------------------
' FLOW 18225198 - Carrega novo form de hist�rico de prazos
' Petrauskas Jan/2016
'-----------------------------------------------------------

    On Error GoTo jpErro

    'Paulo Pelegrini - MU-2017-049845 - 22/06/2018 (INI)
    If frm_hist_prazo_regulacao.Visible = True Then
        Exit Sub
    End If
    'Paulo Pelegrini - MU-2017-049845 - 22/06/2018 (FIM)

    Load frm_hist_prazo_regulacao
    frm_hist_prazo_regulacao.p_conexao_id = lConexaoLocal
    frm_hist_prazo_regulacao.p_usuario_ativo = usuario_is_tecnico_ativo

    'tem que ser �ltima propriedade a carregar, ele carrega o grid
    frm_hist_prazo_regulacao.p_sinistro_id = gbldSinistro_ID
    frm_hist_prazo_regulacao.p_tipo_ramo = bytTipoRamo

    'Ajustar posicionamento do form
    If Me.Left + Me.Width + frm_hist_prazo_regulacao.Width <= Screen.Width Then
        frm_hist_prazo_regulacao.Left = Me.Left + Me.Width
    ElseIf Me.Left > frm_hist_prazo_regulacao.Width Then
        frm_hist_prazo_regulacao.Left = Me.Left - frm_hist_prazo_regulacao.Width
    ElseIf Me.Left > 1000 Then
        frm_hist_prazo_regulacao.Left = 0
    Else
        frm_hist_prazo_regulacao.Left = Me.Left + Me.Width
    End If

    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        ' Allways On Top
        lngontop = SetWindowPos(Me.hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, (SWP_NOMOVE Or SWP_NOSIZE))
    End If

    'Paulo Pelegrini - MU-2017-049845 - 22/06/2018 (INI)
    'frm_hist_prazo_regulacao.Show vbModal
    frm_hist_prazo_regulacao.Show
    'Paulo Pelegrini - MU-2017-049845 - 22/06/2018(FIM)

    If frm_hist_prazo_regulacao.p_alterou_prazo Then
        Call CarregaDadosTela_PrazoRegulacao
    End If

    'Paulo Pelegrini - MU-2017-049845 - 22/06/2018 (INI)
    'Unload frm_hist_prazo_regulacao
    'Paulo Pelegrini - MU-2017-049845 - 22/06/2018 (FIM)
    
    'genjunior - IM00804023
    frm_hist_prazo_regulacao.chk_eventos_somente_ativos.Value = vbChecked
    'fim genjunior

    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        ' Allways On Top
        lngontop = SetWindowPos(Me.hwnd, HWND_TOPMOST, 0, 0, 0, 0, (SWP_NOMOVE Or SWP_NOSIZE))
    End If

    Exit Sub

jpErro:
    Call TrataErroGeral("cmd_prazo_seguradora_Click", Me.name)
    Err.Clear

End Sub

Private Sub Form_Resize()
    'AKIO.OKUNO - INICIO - 18/04/2013
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        ' Allways On Top
        lngontop = SetWindowPos(Me.hwnd, HWND_TOPMOST, 0, 0, 0, 0, (SWP_NOMOVE Or SWP_NOSIZE))
    End If
    'AKIO.OKUNO - FIM - 18/04/2013
End Sub


Private Sub Form_Unload(Cancel As Integer)
    btnCancelar_Click   'AKIO.OKUNO - 04/10/2012
End Sub

Private Sub grdSelecao_Click()
'Demanda 18211992  - Edilson Silva - 14/07/2014 - Inicio
Dim rs As ADODB.Recordset
Dim SQL As String
If IsNumeric(grdSelecao.TextMatrix(grdSelecao.RowSel, 1)) = True Then
    chkppe.Visible = True
    
    'Demanda "MU-2017-031327 - Adequa��o de Valida��o Segurado PPE" - Daniel Manga - Confitec
    'SQL = ""
    'SQL = SQL & "SELECT " & vbNewLine
    'SQL = SQL & "CASE ISNULL(p.ppe_id,0)WHEN 0 THEN 'n' ELSE 's' END ppe" & vbNewLine
    'SQL = SQL & "FROM seguros_db.dbo.sinistro_tb s   WITH (NOLOCK) " & vbNewLine
    'SQL = SQL & "JOIN seguros_db.dbo.cliente_tb c  WITH (NOLOCK)  ON c.cliente_id = s.cliente_id" & vbNewLine
    'SQL = SQL & "LEFT JOIN seguros_db.dbo.ppe_tb p  WITH (NOLOCK)  ON p.cpf = c.cpf_cnpj" & vbNewLine
    'SQL = SQL & "WHERE s.sinistro_id = " & grdSelecao.TextMatrix(grdSelecao.RowSel, 1)

    SQL = ""
    SQL = SQL & "   SELECT DISTINCT" & vbNewLine
    SQL = SQL & "          CASE WHEN (" & vbNewLine
    SQL = SQL & "                     (ISNULL(SERASA_TITULAR_TB.serasa_titular_id, 0) = 0) AND" & vbNewLine
    SQL = SQL & "                     (ISNULL(SERASA_RELACIONADO_TB.serasa_relacionado_id, 0) = 0)" & vbNewLine
    SQL = SQL & "                    ) THEN 'n' ELSE 's' END ppe" & vbNewLine
    SQL = SQL & "     FROM seguros_db.dbo.sinistro_tb SINISTRO_TB WITH (NOLOCK)" & vbNewLine
    SQL = SQL & "     JOIN seguros_db.dbo.cliente_tb CLIENTE_TB WITH (NOLOCK)" & vbNewLine
    SQL = SQL & "       ON CLIENTE_TB.cliente_id = SINISTRO_TB.cliente_id" & vbNewLine
    SQL = SQL & "LEFT JOIN seguros_db.dbo.serasa_titular_tb SERASA_TITULAR_TB WITH (NOLOCK)" & vbNewLine
    SQL = SQL & "       ON SERASA_TITULAR_TB.cpf_cnpj = CLIENTE_TB.cpf_cnpj" & vbNewLine
    SQL = SQL & "LEFT JOIN seguros_db.dbo.serasa_relacionado_tb SERASA_RELACIONADO_TB WITH (NOLOCK)" & vbNewLine
    SQL = SQL & "       ON SERASA_RELACIONADO_TB.cpf_cnpj = CLIENTE_TB.cpf_cnpj" & vbNewLine
    SQL = SQL & "    WHERE SINISTRO_TB.sinistro_id = " & grdSelecao.TextMatrix(grdSelecao.RowSel, 1)

    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, True)
    If Not rs.EOF Then
    If rs.Fields!ppe = "s" Then
        chkppe.Value = 1
    Else
        chkppe.Value = 0
    End If
    End If
    rs.Close
    Set rs = Nothing
End If
'Demanda 18211992 - Edilson Silva - 14/07/2014 - Fim


    'Paulo Pelegrini - MU-2017-049845 - 16/07/2018 (INI)
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        Unload frm_hist_prazo_regulacao 'IM00448383 - Paulo Pelegrini - 14/08/2018
    End If
    'Paulo Pelegrini - MU-2017-049845 - 16/07/2018 (FIM)


End Sub

Private Sub grdSelecao_DblClick()
    Dim bProcessa As Boolean                     'AKIO.OKUNO - 30/09/2012
    Dim datApolice_Dt_Emissao As Date    'AKIO.OKUNO - 21/04/2013
    Dim bContratou As Boolean                     'AKIO.OKUNO - 21/04/2013
    Dim produto_Id As Integer                     'Sergio Ricardo - Nova Consultoria - 21/02/2014

    If grdSelecao.Row = 0 Then Exit Sub

    bProcessa = False                                           'AKIO.OKUNO - 30/09/2012
    pct_prazos.Visible = False


    'Paulo Pelegrini - MU-2017-063260 - 16/07/2018 (INI)
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        If bytTipoRamo = bytTipoRamo_Vida Then
            SQL = ""
            SQL = SQL & "select 1 from proposta_comentario_tb with (nolock) where proposta_id = " & grdSelecao.TextMatrix(grdSelecao.RowSel, 5)

            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         SQL, _
                                         lConexaoLocal, True)
            If Not rs.EOF Then
                MsgBox "Existem informa��es registradas no hist�rico da proposta!", vbInformation
            End If
            rs.Close
            Set rs = Nothing

            btnConsultaHistProposta.Enabled = True
            btnConsultaHistProposta.Visible = True
            
            If FrmConsultaSinistrocon.Visible Then
                btnConsultaHistProposta.SetFocus
            End If
        Else
            btnConsultaHistProposta.Enabled = False
            btnConsultaHistProposta.Visible = False
        End If
    End If
    'Paulo Pelegrini - MU-2017-063260 - 16/07/2018 (FIM)


    'AKIO.OKUNO - INICIO - 29/09/2012
    '    gbldSinistro_ID = grdSelecao.TextMatrix(grdSelecao.Row, 1)

    'FLAVIO.BEZERRA - 15/05/2013
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem And BtnOk.Enabled Then
        If MsgBox("O detalhamento foi alterado. Deseja abandonar as altera��es ?", vbYesNo) = vbNo Then
            Exit Sub
        Else
            BtnOk.Enabled = False
            If strTipoProcesso <> strTipoProcesso_Avisar And _
               strTipoProcesso <> strTipoProcesso_Consulta And _
               strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
               strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
                Call GravaDados_BloqueiaSinistro(False, grdSelecao.TextMatrix(grdSelecao.Row, 1))    'FLAVIO.BEZERRA - 15/05/2013
            End If
        End If
    End If
    'FLAVIO.BEZERRA - 15/05/2013

    'genjunior - hi agro - exibi��o de lista de documentos para sinistros do rural que ainda n�o foram efetivados no SEGBR
    '08/05/2018
    If (grdSelecao.TextMatrix(grdSelecao.Row, 1) = "0" Or grdSelecao.TextMatrix(grdSelecao.Row, 1) = "") And (strTipoProcesso <> strTipoProcesso_Avisar And strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro) Then
        If MsgBox("O sinistro est� em fase de processamento e estar� dispon�vel em breve, ap�s sua efetiva��o no SEGBR." & vbNewLine & "Gostaria de visualizar os documentos pendentes de entrega?", vbYesNo, "SEGP1285") = vbYes Then
            'INICIO petrauskas, 07/08/2018, incluido tratamento ON TOP e Else para o mensage box
            If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
                lngontop = SetWindowPos(Me.hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, (SWP_NOMOVE Or SWP_NOSIZE))
            End If
            frmDocProtocolo.Show vbModal
            If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
                lngontop = SetWindowPos(Me.hwnd, HWND_TOPMOST, 0, 0, 0, 0, (SWP_NOMOVE Or SWP_NOSIZE))
            End If
            Exit Sub
        Else
            Exit Sub
            'FIM petrauskas, 07/08/2018
        End If
    End If

    'fim genjunior
    'Sergio Ricardo - Nova Consultoria - 21/02/2014
    If grdSelecao.Rows > 1 Then
        produto_Id = grdSelecao.TextMatrix(grdSelecao.Row, 13)

        If ((bytTipoRamo = bytTipoRamo_Vida) And (strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem)) Then
            btnConsultaApolice.Visible = True
            If ((produto_Id = 11) Or (produto_Id = 14) Or (produto_Id = 722)) Then
                btnConsultaPropostaInicial.Visible = True
            Else
                btnConsultaPropostaInicial.Visible = False
            End If
        Else
            btnConsultaApolice.Visible = False
            btnConsultaPropostaInicial.Visible = False
        End If
    Else
        btnConsultaApolice.Visible = False
        btnConsultaPropostaInicial.Visible = False
    End If

    StatusBar1.SimpleText = ""                      'FLAVIO.BEZERRA - 05/02/2013
    lblSelecionados = ""                            'FLAVIO.BEZERRA - 05/02/2013
    gblsDetalhamento_VisualizarAnt = ""             'FLAVIO.BEZERRA - 05/02/2013
    txtDetalhamento_Anterior = ""                   'FLAVIO.BEZERRA - 05/02/2013

    With grdSelecao
        If .Rows > 1 Then

            'FLAVIO.BEZERRA - INICIO - 15/01/2013
            If strTipoProcesso <> strTipoProcesso_Avisar And strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then  'FLAVIO.BEZERRA - 27/02/2013
                '            If strTipoProcesso <> strTipoProcesso_Avisar And _
                             '               strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
                             '               strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem _
                             '               Then                                                                                                   'FLAVIO.BEZERRA - 27/02/2013
                'If Trim(.TextMatrix(.Row, 0)) <> "" Then
                'AKIO.OKUNO - INICIO - 17/04/2013
                '                If ValidaCosseguro(.TextMatrix(.Row, 3), .TextMatrix(.Row, 5), _
                                 Mid(Replace(.TextMatrix(.Row, 7), " ", ""), 1, 2)) Then
                If .TextMatrix(.Row, 16) = "A" Then
                    'AKIO.OKUNO - FIM - 17/04/2013
                    sOperacaoCosseguro = "C"
                    'End If
                Else
                    sOperacaoCosseguro = ""
                End If
            End If
            'FLAVIO.BEZERRA - FIM - 15/01/2013

            '            gbldSinistro_ID = "0" & .TextMatrix(.Row, 1)        'AKIO.OKUNO - 30/09/2012
            gbllApolice_ID = "0" & .TextMatrix(.Row, 4)
            gbllProposta_ID = "0" & .TextMatrix(.Row, 5)
            gbllRamo_ID = "0" & .TextMatrix(.Row, 14)
            If strTipoProcesso <> strTipoProcesso_Avisar Then
                If strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then
                    gbldSinistro_ID = "0" & .TextMatrix(.Row, 1)
                End If
            End If
            gbldDt_Ocorrencia_Sinistro = IIf(.TextMatrix(.Row, 15) = "", Data_Sistema, .TextMatrix(.Row, 15))
            gbllProduto_ID = "0" & .TextMatrix(.Row, 13)

            datApolice_Dt_Emissao = IIf(.TextMatrix(.Row, 19) = "", "1900-01-01", .TextMatrix(.Row, 19))     'AKIO.OKUNO - 21/04/2013
        End If
    End With
    'AKIO.OKUNO - FIM - 29/09/2012
    'In�cio - Isabeli Silva - Confitec SP - 2017-10-09
    'MU-2017-036534 Cadastramento dos Corretores Exclusivos no SEGBR
    CorretorProposta = ValidaCorretorProposta(gbllProposta_ID)

    If isCorretor Then
        If CorretorProposta Then
            Call frmNovosEventos.Carrega_grid_Eventos(grdSelecao.TextMatrix(grdSelecao.Row, 1))
            If gblbExisteEvento Then
                frmNovosEventos.Show vbModal
            End If
        Else
            Call MensagemBatch("Ap�lice pertence a outra Jurisdi��o")
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
    End If
    'Fim - Isabeli Silva - Confitec SP - 2017-10-09

    'Ralves - 23/09/2012 - Adicionando Form de eventos
    If strTipoProcesso <> strTipoProcesso_Avisar And strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then
        '       bGTR_Implantado = Seleciona_GTR_Implantado(grdSelecao.TextMatrix(grdSelecao.Row, 1))   'AKIO.OKUNO - 24/09/2012
        '       bGTR_Implantado = Seleciona_GTR_Implantado(CDbl("0" & grdSelecao.TextMatrix(grdSelecao.Row, 1)))   'AKIO.OKUNO - 26/09/2012

        '        bGTR_Implantado = Seleciona_GTR_Implantado(CDbl("0" & grdSelecao.TextMatrix(grdSelecao.Row, 1)), lConexaoLocal)        'AKIO.OKUNO - 30/04/2013
        bGTR_Implantado = Obtem_GTR_Implantado("0" & grdSelecao.TextMatrix(grdSelecao.Row, 1))        'AKIO.OKUNO - 30/04/2013

        '       If bGTR_Implantado And bytTipoRamo <> bytTipoRamo_Vida Then    'RALVES (POR AKIO.OKUNO - 27/09/2012)
        'If bGTR_Implantado Then FLAVIO.BEZERRA - 05/01/2013

        If bGTR_Implantado And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then    'FLAVIO.BEZERRA - 05/01/2013
            With frmNovosEventos
                Call .Carrega_grid_Eventos(grdSelecao.TextMatrix(grdSelecao.Row, 1))
                If gblbExisteEvento Then
                    .Show vbModal
                End If
            End With
        End If

    End If

    SelecionaDados_Sinistro   'Fl�vio P�rsico - 01/10/2012

    'alan neves aqui

    'CarregaVariaveis_Sinistro 'Fl�vio P�rsico - 01/10/2012  -- Alan Neves testes - 15/04/2013

    Select Case strTipoProcesso
    Case strTipoProcesso_Avaliar_Com_Imagem
        pct_prazos.Visible = True

        If ValidaDados_SinistroSelecionado Then

            'Paulo Pelegrini - 10/12/2013
            VerificaRegulacaoSinistro

            txt_prazo_atual.Visible = (bytTipoRamo = bytTipoRamo_Vida) Or (bytTipoRamo = bytTipoRamo_Rural)
            Label1.Visible = (bytTipoRamo = bytTipoRamo_Vida) Or (bytTipoRamo = bytTipoRamo_Rural)

            CarregaDadosTela_PrazoRegulacao

            'Alan Neves - 18/04/2013
            DoEvents
            CarregaDadosTela_Detalhamento
            InicializaInterface_Detalhamento False
            'Alan Neves - 18/04/2013

            InicializaInterface_AgendaAtividadeLocal
            InicializaInterface_VisualizacaoImagem

            'Alan Neves - 18/04/2013
            '                CarregaDadosTela_Detalhamento
            '                InicializaInterface_Detalhamento False  'FLAVIO.BEZERRA - 07/03/2013
            'Alan Neves - 18/04/2013

            bProcessa = False                       'AKIO.OKUNO - 30/09/2012
        End If
    Case strTipoProcesso_Reabrir, strTipoProcesso_Reabrir_Cosseguro
        'AKIO.OKUNO - INICIO - 29/09/2012
        If MsgBox("Confirma reabertura do sinistro " & gbldSinistro_ID & "?", vbYesNo + vbDefaultButton2 + vbQuestion) = vbNo Then
            btnPesquisar.SetFocus
            bProcessa = False
            Exit Sub
        End If
        bProcessa = True                      'AKIO.OKUNO - 30/09/2012
        '            btnOK_Click                            'AKIO.OKUNO - 30/09/2012
        'AKIO.OKUNO - FIM - 29/09/2012

    Case Else
        bProcessa = True                      'AKIO.OKUNO - 30/09/2012
        '            btnOK_Click                            'AKIO.OKUNO - 30/09/2012
    End Select

    Dim sSQL As String
    Dim rsCobertura As Recordset
    Dim dblValor As Double


    'Somente deve criticar se n�o for AVISO
    If bProcessa Then   'AKIO.OKUNO - 09/11/2012
        If gbldSinistro_ID <> 0 Then       'AKIO.OKUNO - 04/10/2012
            '            sSQL = ""
            '            sSQL = "select tp_cobertura_id " & vbNewLine
            '            sSQL = sSQL & " from sinistro_cobertura_tb  WITH (NOLOCK) " & vbNewLine
            '            sSQL = sSQL & " Where sinistro_id = " & gbldSinistro_ID & vbNewLine
            '            sSQL = sSQL & " and dt_fim_vigencia is null " & vbNewLine
            '            Set rsCobertura = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         '                                          glAmbiente_id, _
                         '                                          App.Title, _
                         '                                          App.FileDescription, _
                         '                                          sSQL, _
                         '                                          lConexaoLocal, _
                         '                                          True)
            '
            '
            '            Do While Not rsCobertura.EOF
            '                bContratou = Contratou_Cobertura(rsCobertura.Fields!tp_Cobertura_id, datApolice_Dt_Emissao)
            '                rsCobertura.MoveNext
            '            Loop
            bContratou = Contratou_Cobertura(0, datApolice_Dt_Emissao)
        End If                                                  'AKIO.OKUNO - 04/10/2012

        'AKIO.OKUNO - INICIO - 30/09/2012
        '    If bProcessa Then      'AKIO.OKUNO - 09/11/2012
        btnOK_Click
    End If
    'AKIO.OKUNO - FIM - 30/09/2012

End Sub


'Paulo Pelegrini - 10/12/2013
Private Sub VerificaRegulacaoSinistro()

    Dim rs As ADODB.Recordset
    Dim SQL As String

'+----------------------------------------------------
'| 18313521 - Ajuste de performance
'| Petrauskas 26/08/2016
'|
    SQL = "SET NOCOUNT ON   exec SEGS13103_SPS " & grdSelecao.TextMatrix(grdSelecao.Row, 1) & ","
    
    If grdSelecao.TextMatrix(grdSelecao.Row, 13) > 1000 Then
      SQL = SQL & " 8000"
    Else
      SQL = SQL & " 2000"
    End If
    
'    If grdSelecao.TextMatrix(grdSelecao.Row, 12) > 1000 Then
'        '        SQL = SQL & "SELECT 1 "
'        '        SQL = SQL & "FROM SEGUROS_DB..EVENTO_SEGBR_SINISTRO_ATUAL_TB A  WITH (NOLOCK)  "
'        '        SQL = SQL & "JOIN INTERFACE_DB..SAIDA_GTR_ATUAL_TB B  WITH (NOLOCK)  "
'        '        SQL = SQL & "ON B.SINISTRO_BB = A.SINISTRO_BB "
'        '        SQL = SQL & "JOIN SEGUROS_DB..SINISTRO_TB C  WITH (NOLOCK)  "
'        '        SQL = SQL & "ON C.SINISTRO_ID = A.SINISTRO_ID "
'        '        SQL = SQL & "WHERE a.SINISTRO_ID = " & grdSelecao.TextMatrix(grdSelecao.Row, 1) & " "
'        '        SQL = SQL & "AND C.TP_RAMO_ID = 1 "
'        '        SQL = SQL & "AND A.EVENTO_BB_ID NOT IN (1199,2299,9999,7199,8199) "
'        '        SQL = SQL & "AND A.EVENTO_BB_ID < 8000 "
'        '        SQL = SQL & "AND (B.SITUACAO_MQ = 'E' OR (B.SITUACAO_MQ = 'N' AND (DATEDIFF(HOUR,B.DT_INCLUSAO,GETDATE()) > 24))) "
'
'        SQL = SQL & "SELECT 1 "
'        SQL = SQL & "FROM SEGUROS_DB..SINISTRO_BB_TB A  WITH (NOLOCK)  "
'        SQL = SQL & "JOIN INTERFACE_DB..SAIDA_GTR_ATUAL_TB B  WITH (NOLOCK)  "
'        SQL = SQL & "ON B.SINISTRO_BB = A.SINISTRO_BB  "
'        SQL = SQL & "JOIN SEGUROS_DB..SINISTRO_TB C  WITH (NOLOCK)  "
'        SQL = SQL & "ON C.SINISTRO_ID = A.SINISTRO_ID  "
'        SQL = SQL & "WHERE A.SINISTRO_ID = " & grdSelecao.TextMatrix(grdSelecao.Row, 1) & " "
'        SQL = SQL & "AND C.TP_RAMO_ID = 1 "
'        SQL = SQL & "AND B.EVENTO_BB NOT IN (1199,2299,9999,7199,8199)  "
'        SQL = SQL & "AND B.EVENTO_BB < 8000 "
'        SQL = SQL & "AND (B.SITUACAO_MQ = 'E' OR ((B.SITUACAO_MQ = 'P' OR B.SITUACAO_MQ = 'N') AND (DATEDIFF(HOUR,B.DT_INCLUSAO,GETDATE()) > 24))) "
'        SQL = SQL & "AND A.DT_FIM_VIGENCIA IS NULL"
'    Else
'
'        '        SQL = SQL & "SELECT 1 "
'        '        SQL = SQL & "FROM SEGUROS_DB..EVENTO_SEGBR_SINISTRO_ATUAL_TB A  WITH (NOLOCK)  "
'        '        SQL = SQL & "JOIN INTERFACE_DB..SAIDA_GTR_ATUAL_TB B  WITH (NOLOCK)  "
'        '        SQL = SQL & "ON B.SINISTRO_BB = A.SINISTRO_BB "
'        '        SQL = SQL & "JOIN SEGUROS_DB..SINISTRO_TB C  WITH (NOLOCK)  "
'        '        SQL = SQL & "ON C.SINISTRO_ID = A.SINISTRO_ID "
'        '        SQL = SQL & "WHERE A.SINISTRO_ID = " & grdSelecao.TextMatrix(grdSelecao.Row, 1) & " "
'        '        SQL = SQL & "AND C.TP_RAMO_ID = 1 "
'        '        SQL = SQL & "AND A.EVENTO_BB_ID NOT IN (1199,2299,9999,7199,8199) "
'        '        SQL = SQL & "AND A.EVENTO_BB_ID < 2000 "
'        '        SQL = SQL & "AND (B.SITUACAO_MQ = 'E' OR (B.SITUACAO_MQ = 'N' AND (DATEDIFF(HOUR,B.DT_INCLUSAO,GETDATE()) > 24))) "
'
'        SQL = SQL & "SELECT 1 "
'        SQL = SQL & "FROM SEGUROS_DB..SINISTRO_BB_TB A  WITH (NOLOCK)  "
'        SQL = SQL & "JOIN INTERFACE_DB..SAIDA_GTR_ATUAL_TB B  WITH (NOLOCK)  "
'        SQL = SQL & "ON B.SINISTRO_BB = A.SINISTRO_BB  "
'        SQL = SQL & "JOIN SEGUROS_DB..SINISTRO_TB C  WITH (NOLOCK)  "
'        SQL = SQL & "ON C.SINISTRO_ID = A.SINISTRO_ID  "
'        SQL = SQL & "WHERE A.SINISTRO_ID = " & grdSelecao.TextMatrix(grdSelecao.Row, 1) & " "
'        SQL = SQL & "AND C.TP_RAMO_ID = 1 "
'        SQL = SQL & "AND B.EVENTO_BB NOT IN (1199,2299,9999,7199,8199)  "
'        SQL = SQL & "AND B.EVENTO_BB < 2000 "
'        SQL = SQL & "AND (B.SITUACAO_MQ = 'E' OR ((B.SITUACAO_MQ = 'P' OR B.SITUACAO_MQ = 'N') AND (DATEDIFF(HOUR,B.DT_INCLUSAO,GETDATE()) > 24))) "
'        SQL = SQL & "AND A.DT_FIM_VIGENCIA IS NULL"
'    End If
'|
'| 18313521 - Ajuste de performance
'+----------------------------------------------------
    
    'SITUACAO_MQ  'P' = EM PROCESSAMENTO
    'SITUACAO_MQ  'N' = N�O PROCESSADO
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, True)
    If Not rs.EOF Then
        lblMensagem.Visible = True
    Else
        lblMensagem.Visible = False
    End If
    rs.Close

End Sub

Private Sub InicializaInterface_AgendaAtividadeLocal()
    
    'InicializaInterface_AgendaAtividade sCPF, lConexaoLocal
 
End Sub

Private Sub InicializaInterface_VisualizacaoImagem()
    Dim sChave                                  As String
    
    On Error GoTo TrataErro
    'FLAVIO.BEZERRA - 31/01/2013
    'mathayde
'    dRetorno = ExecutaPrograma("DIGP0014" _
'                                 , "DIGP0014" & ".EXE" _
'                                 , "" _
'                                 , vbNormalFocus _
'                                 , "DIGP0014")

    ID = Shell(App.PATH & "\DIGP0014.exe", vbNormalNoFocus)
    'FLAVIO.BEZERRA - 31/01/2013
    
    sChave = ""
    sChave = sChave & Right("00000000000" & CStr(gbldSinistro_ID), 11)
    sChave = sChave & Format(Val(gbllSeguradora_Cod_Susep), "00000")
    sChave = sChave & Format(Val(gbllSucursal_Seguradora_ID), "00000")
    sChave = sChave & Format(Val(gbllRamo_ID), "000")
    sChave = sChave & Format(Val(gbllApolice_ID), "000000000")

    InicializaInterface_DIGISINLocal sChave
    
    ' O formul�rio do DIGISIN tem ficar ativo.
    AppActivate "DIGP0014"

    Exit Sub

TrataErro:
    If Err.Number = 282 Then
        If MsgBox("O m�dulo de visualiza��o de imagens n�o est� ativo. Deseja carreg�-lo ?", vbYesNo) = vbYes Then
            ' Chama o DIGISIN
            ID = Shell(App.PATH & "\DIGP0014.exe", vbNormalNoFocus)
        End If
    Else
        mensagem_erro 6, "Falha geral na comunica��o com o m�dulo de visualiza��o"
    End If

End Sub
'Msalema - 17/05/2013 - 15644518 - Melhoria de performance do segbr
'Esta fun��o foi criada para inicializar o DIGP0014 conjuntamente com o SEGP1285
'em modo de avaliar com imagem "strTipoProcesso = "E""
Private Sub InicializaInterface_VisualizacaoImagem_I()
    Dim sChave                                  As String
    
    On Error GoTo TrataErro

    ID = Shell(App.PATH & "\DIGP0014.exe", vbNormalNoFocus)
    
    ' O formul�rio do DIGISIN tem ficar ativo.
    AppActivate "DIGP0014"

    Exit Sub

TrataErro:
    If Err.Number = 282 Then
        If MsgBox("O m�dulo de visualiza��o de imagens n�o est� ativo. Deseja carreg�-lo ?", vbYesNo) = vbYes Then
            ' Chama o DIGISIN
            ID = Shell(App.PATH & "\DIGP0014.exe", vbNormalNoFocus)
        End If
    Else
        mensagem_erro 6, "Falha geral na comunica��o com o m�dulo de visualiza��o"
    End If

End Sub
'Msalema - 17/05/2013 - 15644518 - Melhoria de performance do segbr


Private Sub InicializaModoProcessamentoObjeto(objObjeto As Object, bTipo As Byte)
    DoEvents
    
    With objObjeto
        Select Case bTipo
            Case 1              'Vai carregar
                If TypeOf objObjeto Is MSFlexGrid Then
                    .BackColorFixed = &HC0C0C0
                    .ForeColorFixed = &H808080
                Else
                    .BackColor = &HC0C0C0
                End If
            Case 2              'Carregado
                If TypeOf objObjeto Is MSFlexGrid Then
                    .BackColorFixed = &H8000000F
                    .ForeColorFixed = &H80000012
                Else
                    .BackColor = -2147483643
                End If
        End Select
    End With

End Sub

Private Sub txtDetalhamento_Visualizar_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))

End Sub

Private Sub txtExigencia_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        If Index < 3 Then
            txtExigencia(Index + 1).SetFocus
        End If
    Else
        If txtExigencia(Index).SelStart = 70 Then
            If Index < 3 Then
                txtExigencia(Index + 1).SetFocus
                txtExigencia(Index).SelStart = 1
            End If
        End If
    End If

End Sub
Private Sub Form_Load()
    On Error GoTo Erro
    
    Dim propostaSegp As String
        
'Teste - Retirar p/ liberar a aplica��o
    'cUserName = "F_18225206"
    'glAmbiente_id = 3
    'Data_Sistema = Date
    'glEmpresa_id = 30
    'gsCPF = "09970561774"
    
    programaSegp = (Mid(Command, 1, 4) = "SEGP")
    
    'Final -----------------------------------------
    'Monta_Parametros ("SEGP1285")       'MATHAYDE (POR AKIO.OKUNO)
    If Not Trata_Parametros(Command) And Not programaSegp Then
        FinalizarAplicacao
    ElseIf programaSegp Then
        glAmbiente_id = Mid(Command, 10, 1)
    End If
    'mathayde
    'BuscaStatusSistema
       
    'Isabeli Silva - Confitec SP - 2017-10-09
    'MU-2017-036534 Cadastramento dos Corretores Exclusivos no SEGBR
     isCorretor = ValidaCorretor(gsCPF)
    
    InicializaVariaveisLocal
    
    InicializaInterfaceLocal
    
    InicializaInterface_Funcionalidades
    
    'InicializaInterface_Versao Me 'AKIO.OKUNO - 06/05/2013
    
    'Msalema - 17/05/2013 - 15644518 - Melhoria de performance do segbr
    
'    cUserName = "48572845372"


    If strTipoProcesso = "E" Then
        InicializaInterface_VisualizacaoImagem_I
    End If
    'Msalema - 17/05/2013 - 15644518 - Melhoria de performance do segbr
    
    ' Jos� Moreira (Nova Consultoria) - 26/12/2012 - 14620257 - Melhorias no processo de subscri��o
    AbrirConsultaSinistro = False
    
    If (sSinistro <> "") Then
        ' pablo.dias (Nova Consultoria) - 18/10/2013 - 14620257 - Melhorias no processo de subscri��o
        ' N�o validar seguran�a quando chamado a partir de outro programa
        'Call Seguranca("SEGP1285.3", "Consultar Sinistro")
        AbrirConsultaSinistro = True
    End If

    
    ' Jos� Moreira (Nova Consultoria) - 25/12/2012 - 14620257 - Melhorias no processo de subscri��o
    If (AbrirConsultaSinistro) Then
          'GENJUNIOR - HI AGRO - Corre��o para Chamada de Outra Aplica��o
          'cmbCriterioPesquisa.ListIndex = 7
          cmbCriterioPesquisa.ListIndex = 8
          'Fim GENJUNIOR
          
          txtConteudo.Text = Trim(sSinistro)
          btnPesquisar_Click
          If grdSelecao.Rows > 1 Then
            grdSelecao.Row = 1
            grdSelecao_DblClick
          End If
          
        Else
          cmbCriterioPesquisa.ListIndex = -1

    End If
    
    Exit Sub
'    Resume
Erro:
    Call TrataErroGeral("Form_Load", "frmConsultaSinistro")
    Call FinalizarAplicacao
    
End Sub

Private Sub InicializaInterface_Funcionalidades()
    Dim dRetorno                                As Double
    Dim bConex                                  As Boolean      'AKIO.OKUNO 23/09/2012
    'AKIO.OKUNO - INICIO - 23/09/2012
    On Error GoTo Erro
    Select Case EstadoConexao(lConexaoLocal)
        Case adStateClosed
            lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
        Case adStateOpen
            bConex = FecharConexao(lConexaoLocal)
            lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
    End Select
'    If EstadoConexao(lConexaoLocal) = adStateClosed Then
'        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
'    End If
    'AKIO.OKUNO - FIM - 23/09/2012
    
    With cmbCriterioPesquisa 'cristovao.rodrigues 08/10/2012 mudancas itens da pesquisa
        Select Case strTipoProcesso
            Case strTipoProcesso_Consulta, _
                 strTipoProcesso_Consulta_Cosseguro, _
                 strTipoProcesso_Avaliar, _
                 strTipoProcesso_Avaliar_Cosseguro
                                 'CRISTOVAO.RODRIGUES - INICIO - (POR AKIO.OKUNO) - 10/10/2012
             If strTipoProcesso = strTipoProcesso_Consulta Or _
                   strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
                       .AddItem "Nome do Benefici�rio"
                       .AddItem "CPF do Benefici�rio"
                       .AddItem "CNPJ do Benefici�rio"
             End If
                                 'CRISTOVAO.RODRIGUES - FIM - (POR AKIO.OKUNO) - 10/10/2012
                .AddItem "N�mero Sinistro"
                .AddItem "Avisos Pendentes de Avalia��o"
                .AddItem "T�cnico Respons�vel"
                
'            If strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro And _  'cristovao.rodrigues 08/10/2012 comentado
'                strTipoProcesso <> strTipoProcesso_Avaliar Then
                .AddItem "Sinistros Pendentes de Aprova��o" 'cristovao.rodrigues 11/01/2013 retirado comentario
'            End If
                
                If strTipoProcesso = strTipoProcesso_Consulta Or _
                   strTipoProcesso = strTipoProcesso_Avaliar Then
                    .AddItem "Proposta BB"
'                    .AddItem "T�cnicos - Aprova��o"    'CRISTOVAO.RODRIGUES (POR AKIO.OKUNO) - 10/10/2012
                    .AddItem "N�mero Sinistro BB"
                
'                If strTipoProcesso = strTipoProcesso_Consulta Then 'cristovao.rodrigues 08/10/2012 comentado
'                    .AddItem "T�cnicos - Aprova��o"
'                End If
                    
                    'If bytTipoRamo <> bytTipoRamo_Rural Then 'cristovao.rodrigues 16/08/2012
                    If bytTipoRamo = bytTipoRamo_Vida Then 'mathayde
                        .AddItem "N�mero Sinistro Cardif" ' Cesar Santos CONFITEC - (SBRJ009952) 12/06/2020
                                                'CRISTOVAO.RODRIGUES - INICIO - (POR AKIO.OKUNO) - 10/10/2012
                    If strTipoProcesso = strTipoProcesso_Avaliar Or _
                        strTipoProcesso = strTipoProcesso_Avaliar_Cosseguro Then
                                                'CRISTOVAO.RODRIGUES - FIM - (POR AKIO.OKUNO) - 10/10/2012
                        .AddItem "N� da Certificado SC"
                        .AddItem "N�mero Sinistro SC"
                    End If
                End If
                                'CRISTOVAO.RODRIGUES - INICIO - (POR AKIO.OKUNO) - 10/10/2012
                'If strTipoProcesso = strTipoProcesso_Avaliar Then
                    If bytTipoRamo <> bytTipoRamo_Vida Then
                            .AddItem "CNPJ"
                    End If
                'End If
                                'CRISTOVAO.RODRIGUES - FIM - (POR AKIO.OKUNO) - 10/10/2012
                ElseIf strTipoProcesso = strTipoProcesso_Consulta_Cosseguro _
                  Or strTipoProcesso = strTipoProcesso_Avaliar_Cosseguro Then
                    
                    'cristovao.rodrigues 05/10/2012 incluidos parametros
                    .AddItem "N�mero Sinistro L�der"
                    .AddItem "Ap�lice L�der"
                    If bytTipoRamo <> bytTipoRamo_Vida Then
                        .AddItem "CNPJ"
                    End If
                                        'CRISTOVAO.RODRIGUES - INICIO - (POR AKIO.OKUNO) - 10/10/2012
                    If strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
                        .AddItem "N� da Certificado SC"
                        .AddItem "N�mero Sinistro SC"
                    End If
                                        'CRISTOVAO.RODRIGUES - FIM - (POR AKIO.OKUNO) - 10/10/2012
                End If
                                
    
            Case strTipoProcesso_Avisar ', strTipoProcesso_Avisar_Cosseguro 'cristovao.rodrigues 08/10/2012
                
                .AddItem "Proposta BB"
                Select Case bytTipoRamo
                    Case bytTipoRamo_Vida
                    Case bytTipoRamo_RE
                        .AddItem "CNPJ"
                    Case bytTipoRamo_Rural
                End Select
                
            'AKIO.OKUNO - INICIO
            Case strTipoProcesso_Avisar_Cosseguro
                .AddItem "Ap�lice L�der"        'CRISTOVAO.RODRIGUES (POR AKIO.OKUNO - 10/10/2012)
                Select Case bytTipoRamo
                    Case bytTipoRamo_Vida
                    Case bytTipoRamo_RE, bytTipoRamo_Rural
                        .AddItem "CNPJ"
                End Select
            'AKIO.OKUNO - FIM
            
           Case strTipoProcesso_Reabrir, _
                strTipoProcesso_Encerrar
                
                .AddItem "Proposta BB"
                .AddItem "N�mero Sinistro"
                .AddItem "N�mero Sinistro BB"
                .AddItem "T�cnico Respons�vel"
            
                If bytTipoRamo = bytTipoRamo_Vida Then ' Cesar Santos CONFITEC - (SBRJ009952) 12/06/2020
                    .AddItem "N�mero Sinistro Cardif"
                End If
			
'            Case strTipoProcesso_Reabrir_Cosseguro 'AKIO.OKUNO - 29/09/2012
            Case strTipoProcesso_Reabrir_Cosseguro, strTipoProcesso_Encerrar_Cosseguro
                .AddItem "Ap�lice L�der"        'CRISTOVAO.RODRIGUES (POR AKIO.OKUNO - 10/10/2012)
                .AddItem "N�mero Sinistro"
                .AddItem "N�mero Sinistro Lider"
                .AddItem "T�cnico Respons�vel"
                
            Case strTipoProcesso_Avaliar_Com_Imagem
                .AddItem "Proposta BB" 'cristovao.rodrigues 08/10/2012
                .AddItem "Ap�lice L�der"   'EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
                .AddItem "Avisos Pendentes de Avalia��o"
                .AddItem "T�cnico Respons�vel"
                .AddItem "N�mero Sinistro"
                .AddItem "N�mero Sinistro BB"
                .AddItem "N� da Certificado SC"
                .AddItem "N�mero Sinistro SC"
                .AddItem "N�mero Sinistro L�der" 'EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
                .AddItem "Sinistros Pendentes de Aprova��o"
                .AddItem "T�cnicos - Aprova��o"
                .AddItem "N�mero de Ordem"
'AKIO.OKUNO - INICIO - 13/12/2012
                Select Case bytTipoRamo
                    Case bytTipoRamo_Vida
                    Case bytTipoRamo_RE, bytTipoRamo_Rural
                        .AddItem "CNPJ"
                End Select
'AKIO.OKUNO - FIM - 13/12/2012

'AKIO.OKUNO - TRECHO RETIRADO POIS EST� SENDO CONFIGURADO EM InicializaInterfaceLocal_Form
'                'AKIO.OKUNO - INICIO - 02/10/2012
'                If Screen.Height > 15360 Then
'                    ' Altura
'                    FrmConsultaSinistrocon.Height = 10410 + 2595
'                    pbxBotoesVisualizacaoImagem.Top = 9120 + 2595
'                    BtnCancelar.Top = 9120 + 2595
'                Else
'                    FrmConsultaSinistrocon.Height = 10410
'                    pbxBotoesVisualizacaoImagem.Top = 9120
'                    BtnCancelar.Top = 9120
'                End If
'                'AKIO.OKUNO - FIM - 02/10/2012
                
                'AKIO.OKUNO - INICIO
                InicializaInterface_AgendaAtividade gsCPF, lConexaoLocal
                
                If App.PrevInstance Then
                    MsgBox "Programa j� carregado !!!", 16, "Mensagem ao Usu�rio"
                    DestroiInterface
                    btnCancelar_Click
                End If
 
'''habilitar
'''                lAplicativo = Shell("\\sisab005\Aplicacoes\Sisbr\Sisbr\DIGP0014.exe", vbNormalNoFocus)
'''                lAplicativo = Shell(App.PATH & "\DIGP0014.exe", vbNormalNoFocus) 'mathayde a pedido do rbernardi
''                'AKIO.OKUNO - FIM
                            
        End Select
        .ListIndex = 0
        
        .AddItem "N�mero de Protocolo" '' Laurent Silva - 10/11/2017 - Demanda HiAgro
        
    End With


    bValida_Tecnico = True
    'AKIO.OKUNO - INICIO
    'If EstadoConexao(lConexaoLocal) = adStateClosed Then
    '    lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
    'End If
        'AKIO.OKUNO - FIM
    'mathayde
    
    If (glAmbiente_id = 6 Or glAmbiente_id = 7) Then
         If Valida_Grupo_Intranet_1285("SINISTROACESSOGERAL", "Gerente") Then
             bValida_Tecnico = False
         End If
    Else
    
         If Valida_Grupo_Intranet_1285("SINISTROACESSOGERAL", "Gerente") Then
             bValida_Tecnico = False
         End If
    End If
    
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
'        id = Shell("\\sisab005\Aplicacoes\Sisbr\Sisbr\DIGP0014.exe", vbNormalNoFocus)
    End If
    
    Retorna_TecnicoAtual 'cristovao.rodrigues 11/01/2013
        
    Exit Sub
    
Erro:
    If Err.Number = 282 Then
        If MsgBox("O m�dulo de visualiza��o de imagens n�o est� ativo. Deseja carreg�-lo ?", vbYesNo) = vbYes Then
            ' Chama o DIGISIN
            ID = Shell(App.PATH & "\DIGP0014.exe", vbNormalNoFocus)
        End If
    Else
        'AKIO.OKUNO - INICIO - 23/09/2012
        If Err = 91 Then
            MsgBox "O m�dulo de visualiza��o de imagens n�o foi localizado." & Chr(13) & Chr(10) & "Entre em contato com o administrador do Sistema e informe-o desta ocorr�ncia e o caminho abaixo :" & Chr(13) & Chr(10) & App.PATH & "\DIGP0014.exe", 16, "Mensagem ao Usuario"
        Else
        'AKIO.OKUNO - FIM - 23/09/2012
            MsgBox "Falha geral na comunica��o com o m�dulo de visualiza��o !!!", 16, "Mensagem ao Usu�rio"
        End If 'AKIO.OKUNO - 23/09/2012
    End If



End Sub
Private Sub InicializaModoOperacaoLocal()
    On Error GoTo Erro
    
    '1-Inclusao / 2-Altera��o / 3-Consulta
    InicializaModoOperacao Me, bytModoOperacao
        
    Exit Sub
    
Erro:
    Call TrataErroGeral("InicializaModoOperacaoLocal", "frmConsultaSinistro")
    Call FinalizarAplicacao

End Sub

Private Sub InicializaVariaveisLocal()
    On Error GoTo Erro
    
    Dim sParametroLocal                         As String
    
    Call inicializa_variaveis_ambiente
    
    If Not Trata_Parametros(Command) And Not programaSegp Then
        FinalizarAplicacao
    Else

'VIDA - EMERSON VIANA MORENO                                         EMORENO              31141274876
'    cUserName = "31141274876"
'    glAmbiente_id = 3
'    bytTipoRamo = bytTipoRamo_Vida
'RE - MARCIO GUSHIKEM MORISHITA                                    MAMORISHITA          32169538828
'    cUserName = "32169538828"
'    glAmbiente_id = 7   'qualidade 4 - desenv
'    bytTipoRamo = bytTipoRamo_RE
'RURAL - VINICIUS GON�ALVES DE GARRIDO BA                             VBA                  37107403800
'    cUserName = "37107403800"
'    glAmbiente_id = 3
'    bytTipoRamo = bytTipoRamo_Rural
'
'    gsParamAplicacao = strTipoProcesso_Avisar  'strTipoProcesso_Consulta
'    sOperacao = gsParamAplicacao
    
    
'cristovao.rodrigues
'    cUserName = "30137998899"
    
    'Alan Neves - 18/04/2013
'    glAmbiente_id = 6

         Ambiente = ConvAmbiente(glAmbiente_id)
        
        sParametroLocal = Left(Command, InStrRev(Command, " "))
        sParametroLocal = Replace(sParametroLocal, "SEGP.", "")
        aParametros = Split(sParametroLocal, ".")
        bytTipoRamo = aParametros(0)
        gsParamAplicacao = aParametros(1)
        If UBound(aParametros) > 2 Then
        sSinistro = Trim(aParametros(3))
        gsCPF = aParametros(4)
        End If
        cUserName = gsCPF
        sOperacao = gsParamAplicacao
        
        ' Jos� Moreira (Nova Consultoria) - 26/12/2012 - 14620257 - Melhorias no processo de subscri��o
'        sSinistro = ""
'        If UBound(aParametros) = 2 Then
'            sSinistro = Trim(aParametros(2))
'        End If
        
        strTipoProcesso = UCase(Trim(gsParamAplicacao)) 'MATHAYDE (POR AKIO.OKUNO - 29/07/2012)
        
        If strTipoProcesso = strTipoProcesso_Avaliar_Cosseguro Or _
           strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Or _
           strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Or _
           strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
           strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro Then
            
            sOperacaoCosseguro = "C"
        End If
        
    End If
    
    'mathayde
    If glAmbiente_id = 2 Or glAmbiente_id = 3 Then
        iAmbienteValidaUsuario = 0
    Else
        iAmbienteValidaUsuario = 1
    End If
    
    'AKIO.OKUNO - INICIO - 17/10/2012 (refazendo 09/10/2012)
    sOperacaoCosseguro_ORIGINAL = sOperacaoCosseguro
    strTipoProcesso_ORIGINAL = strTipoProcesso
    'AKIO.OKUNO - FIM - 17/10/2012 (refazendo 09/10/2012)
    
    Exit Sub

Erro:
    Call TrataErroGeral("InicializaVariaveisLocal", "frmConsultaSinistroCon")
    Call FinalizarAplicacao
End Sub


Private Sub LimpaCamposTelaLocal(objObjeto As Object)
    On Error GoTo Erro
    
    If Not objObjeto Is Nothing Then
        LimpaCamposTela objObjeto
    End If
    
    Exit Sub

Erro:
    Call TrataErroGeral("LimpaCamposTelaLocal", "frmConsultaSinistroCon")
    Call FinalizarAplicacao

End Sub

Private Sub InicializaInterfaceLocal_Form()
    Dim sTipoProcesso_Descricao                 As String
    Dim sTipoProcesso_Descricao_Complemento     As String
    
    lblUsuario.Visible = False
    cmbTecnico.Visible = False
    lblSituacaoSinistro.Visible = False
    cmbSitSinistroAvaliador.Visible = False
    BtnOk.Visible = False
    txtDetalhamento_Anterior.Visible = False 'cristovao.rodrigues 03/01/2013
    
    With grdSelecao
        .ColWidth(13) = 0 'HiAgro Produto_ID'12) = 0
        .ColWidth(14) = 0 'HiAgro Ramo_ID'13) = 0
        .ColWidth(15) = 0 'HiAgro Dt_Ocorrencia_Sinistro'14) = 0
    End With
    
    sTipoProcesso_Descricao_Complemento = ""
    
'    Me.Height = 6705 '7200     'AKIO.OKUNO - 04/10/2012
    Me.Height = 5200 '7200
    
    Select Case strTipoProcesso
        Case strTipoProcesso_Consulta, strTipoProcesso_Consulta_Cosseguro
            sTipoProcesso_Descricao = "Consultar"
        
        Case strTipoProcesso_Avisar, strTipoProcesso_Avisar_Cosseguro
            sTipoProcesso_Descricao = "Avisar"
            
            With grdSelecao
                .ColWidth(0) = 0
                .ColWidth(1) = 0 'sinistro_id
                .ColWidth(2) = 0 'HiAgro incluido nro protocolo
                .ColWidth(9) = 0 'HiAgro Situa��o'8) = 0
                .ColWidth(10) = 0 'HiAgro Endosso'9) = 0
                .ColWidth(11) = 0 'HiAgro N� Sinistro L�der'10) = 0
                .ColWidth(12) = 0 'HiAgro �ltimo hist�rico '11) = 0
            End With
        
            Select Case bytTipoRamo
                Case bytTipoRamo_Vida
                Case bytTipoRamo_RE
                Case bytTipoRamo_Rural
            End Select
        
        Case strTipoProcesso_Avaliar, strTipoProcesso_Avaliar_Cosseguro
            sTipoProcesso_Descricao = "Avaliar"
        
            Select Case bytTipoRamo
                Case bytTipoRamo_Vida
                Case bytTipoRamo_RE, bytTipoRamo_Rural
            End Select
        
        Case strTipoProcesso_Reabrir, strTipoProcesso_Reabrir_Cosseguro
            sTipoProcesso_Descricao = "Reabrir"
        
'        Case strTipoProcesso_Encerrar      'AKIO.OKUNO - 29/09/2012
        Case strTipoProcesso_Encerrar, strTipoProcesso_Encerrar_Cosseguro
            sTipoProcesso_Descricao = "Encerrar"
            
        Case strTipoProcesso_Avaliar_Com_Imagem
            sTipoProcesso_Descricao = "Avalia��o de"
            sTipoProcesso_Descricao_Complemento = "com imagem "
'            Me.Height = 11145      'AKIO.OKUNO - 04/10/2012
            'Me.Height = 10410      'S�rgio Ricardo - 09/01/2014
            Me.Height = 10910
            BtnCancelar.Left = 7800 '7680         'AKIO.OKUNO - 04/10/2012
            
            'AKIO.OKUNO - INICIO - 02/10/2012
            If Screen.Height > 15360 Then
                ' Altura
'                FrmConsultaSinistrocon.Height = 10410 + 2595       'S�rgio Ricardo - 09/01/2014
'                pbxBotoesVisualizacaoImagem.Top = 9120 + 2595      'S�rgio Ricardo - 09/01/2014
'                BtnCancelar.Top = 9120 + 2595                      'S�rgio Ricardo - 09/01/2014
                FrmConsultaSinistrocon.Height = 10910 + 2595
                pbxBotoesVisualizacaoImagem.Top = 9618 + 2595
                BtnCancelar.Top = 9618 + 2595
            Else
'                FrmConsultaSinistrocon.Height = 10410              'S�rgio Ricardo - 09/01/2014
'                pbxBotoesVisualizacaoImagem.Top = 9120             'S�rgio Ricardo - 09/01/2014
'                BtnCancelar.Top = 9120                             'S�rgio Ricardo - 09/01/2014
                FrmConsultaSinistrocon.Height = 10910
                pbxBotoesVisualizacaoImagem.Top = 9618
                BtnCancelar.Top = 9618
            End If
            fmeDetalhamento_Visualizacao.Height = Me.Height - 5670
            txtDetalhamento_Visualizar.Height = fmeDetalhamento_Visualizacao.Height - 1400 - 150
            'AKIO.OKUNO - FIM - 02/10/2012
            
            InicializaInterface_Funcionalidades_ProcessoVisualizar
    End Select
    
    
    'fazer sempre que for DIFERENTE de Avaliar com Imagem
    If strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then   'AKIO.OKUNO - 02/10/2012
        BtnOk.Top = Me.Height - 1305
'        BtnCancelar.Top = BtnOk.Top
'        BtnCancelar.Left = 7320         'AKIO.OKUNO - 04/10/2012
    
'AKIO.OKUNO - INICIO - 09/10/2012 - CONFORME APONTAMENTO 67
        With Me
            .Height = 7200
            .Width = 11925
        End With
        
        Framecriterios.Width = Me.Width - 465
        
        btnPesquisar.Left = Framecriterios.Width - 1560
        
        With BtnCancelar
            .Width = 1335        'AKIO.OKUNO - 04/10/2012
            .Left = Me.Width - .Width - 465
            .Top = Me.Height - 1300
        End With
        
        With grdSelecao
            .Width = Me.Width - 465
            .Height = Me.Height - 3300
        End With
    
    End If  'AKIO.OKUNO - 02/10/2012
    lblSelecionados.Top = grdSelecao.Top + grdSelecao.Height + 100
    
    
    If sOperacaoCosseguro = "C" Then
        Me.Caption = App.EXEName & " - " & sTipoProcesso_Descricao & " Sinistro " & sTipoProcesso_Descricao_Complemento & "- Cosseguro - " & Ambiente
    Else
        Me.Caption = App.EXEName & " - " & sTipoProcesso_Descricao & " Sinistro " & sTipoProcesso_Descricao_Complemento & "- " & Ambiente
    End If
   
    Me.Caption = Me.Caption & " (Ramo: " & IIf(bytTipoRamo = bytTipoRamo_Vida, "Vida", IIf(bytTipoRamo = bytTipoRamo_RE, "RE", "Rural")) & ")"
   
    'Configurando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    'Alan Neves - 08/03/2013 - Inicio - Corre��o erro de maximizar e minimizar.
    'Call CentraFrm(Me)
    If Me.WindowState = 0 Then
       Me.Move (Screen.Width - Me.Width) \ 2, (Screen.Height - Me.Height) \ 2
    End If
    'Alan Neves - 08/03/2013 - Fim
End Sub

Private Sub InicializaInterface_Funcionalidades_ProcessoVisualizar()
    
    fmeDetalhamento_Visualizacao.Visible = True
    btnConsultaProposta.Visible = True
    'mathayde
    btnConsultaProposta.Enabled = False
    btnAnaliseTecnica.Visible = False   'FLAVIO.BEZERRA - 07/01/2013
    btnAnaliseTecnica.Enabled = False  'FLAVIO.BEZERRA - 07/01/2013

    'Sergio Ricardo Demanda 17931952 - Consulta Ap�lice e Proposta Ades�o
    btnConsultaApolice.Visible = False
    btnConsultaPropostaInicial.Visible = False
'    btnSinistro.Visible = True
'    btnAgenda.Visible = True
'    btnDetalhar.Visible = True
'    btnPagamento.Visible = True
'    btnAprovar.Visible = True
'    btnAssessoria.Visible = True
    
End Sub

Private Sub InicializaInterfaceLocal()
    On Error GoTo Erro
    'Demanda 18211992 - edilson silva - 14/07/2014 - Inicio
    chkppe.Visible = False
    'Demanda 18211992 - edilson silva - 14/07/2014 - Fim
    
    Screen.MousePointer = vbHourglass
        
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        ' Allways On Top
        lngontop = SetWindowPos(Me.hwnd, HWND_TOPMOST, 0, 0, 0, 0, (SWP_NOMOVE Or SWP_NOSIZE))  'AKIO.OKUNO - 09/11/2012
    End If
    
    If ValidaSegurancaLocal Then
    
        LimpaCamposTelaLocal Me                                         'SERA ALTERADO
        
        ObterDataSistema (gsSIGLASISTEMA)
        
        InicializaInterfaceLocal_Form
        
        CarregaDadosCombo_CriterioPesquisa
        
        CarregaDadosCombo_Situacao
        
        InicializaModoOperacaoLocal
    Else
        
        FinalizarAplicacao
    
    End If
    
    Screen.MousePointer = vbNormal
    Exit Sub
Resume
Erro:
    Call TrataErroGeral("InicializaInterfaceLocal", "frmConsultaSinistro")
    Call FinalizarAplicacao
Resume
End Sub

'cristovao.rodrigues 11/01/2013
Public Sub Retorna_TecnicoAtual()
Dim rs As ADODB.Recordset
Dim SQL As String

If glAmbiente_id = 2 Or glAmbiente_id = 3 Then
    sLoginRede = BuscaLogin(cUserName) 'Passando CPF para buscar
Else
    sLoginRede = BuscaLogin(cUserName, 1) 'Passando CPF para buscar
End If
  
Tecnico_atual = "0"
'If Not Alianca_Bahia Then
    SQL = "SELECT tecnico_id FROM tecnico_tb  WITH (NOLOCK)  "
    SQL = SQL & " WHERE case CHARINDEX('.', email) when 0 then email else LEFT(email,(CHARINDEX('.', email)-1)) end = '" & Trim(sLoginRede) & "'"
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, _
                                lConexaoLocal, True)
    If Not rs.EOF Then
        Tecnico_atual = rs(0)
    End If
    rs.Close
'End If

End Sub


Private Function ValidaSegurancaLocal() As Boolean
    
'     '------ VERIFICAR SE IREMOS UTILIZAR A VALIDA��O DE SEGURAN�A POR APLICA��O CONFORME PARAMETRO (strTipoProcesso)
''    Select Case strTipoProcesso
''        Case strTipoProcesso_Consulta
''            gsParamAplicacao = "C"
'''            ValidaSegurancaLocal = Seguranca("SEGP1285.0", "Consulta Sinistro")
'''            sOperacaoCosseguro = ""
''        Case strTipoProcesso_Avisar, strTipoProcesso_Avisar_Cosseguro
''            Select Case bytTipoRamo
''                Case bytTipoRamo_Vida
''                    gsParamAplicacao = "I"
'''                    ValidaSegurancaLocal = Seguranca("SEGP1285.11", "Avisar Sinistro")
'''                    sOperacaoCosseguro = ""
''                Case bytTipoRamo_RE
''                Case bytTipoRamo_Rural
''            End Select
''
''        Case strTipoProcesso_Avaliar, strTipoProcesso_Avaliar_Cosseguro
''
''        Case strTipoProcesso_Reabrir
''
''        Case strTipoProcesso_Encerrar
''
''        Case intSinistro_IncluirDetalhamento
''        Case intSinistro_IncluirEstimativa
''        Case intSinistro_ManutencaoBeneficiario
''        Case intSinistro_ManutencaoPrestador
''        Case intSinistro_ManutencaoSolicitante
''        Case intSinistro_ManutencaoAviso
''    End Select

    ValidaSegurancaLocal = True 'temporario
End Function

Private Sub CarregaDadosCombo_CriterioPesquisa()
    'Carga com as op��es "default" de pesquisa.
    With cmbCriterioPesquisa
        .Clear
        .AddItem "Ap�lice"
        .AddItem "Proposta"
        .AddItem "CPF"
        .AddItem "Nome"
    End With
End Sub


Private Sub btnPesquisar_Click()
    
    On Error GoTo TrataErro

    MousePointer = vbHourglass
    
    lblMensagem.Visible = False
  
    'AKIO.OKUNO - INICIO - 17/10/2012 (refazendo 09/10/2012)
    sOperacaoCosseguro = sOperacaoCosseguro_ORIGINAL
    strTipoProcesso = strTipoProcesso_ORIGINAL
    'AKIO.OKUNO - FIM - 17/10/2012 (refazendo 09/10/2012)
    
    
    'Demanda 18211992 - edilson silva - 14/07/2014 - Inicio
    chkppe.Visible = False
    'Demanda 18211992 - edilson silva - 14/07/2014 - Fim
    
    'Paulo Pelegrini - MU-2017-063260 - 16/07/2018 (INI)
    btnConsultaHistProposta.Visible = False
    'Paulo Pelegrini - MU-2017-063260 - 16/07/2018 (FIM)
    
    btnConsultaApolice.Visible = False
    btnConsultaPropostaInicial.Visible = False
    
    'mathayde
    btnConsultaProposta.Enabled = False
    btnAnaliseTecnica.Enabled = False 'FLAVIO.BEZERRA - 07/01/2013
    
    If ValidaDados_SelecaoConsulta Then
    
        InicializaModoProcessamentoObjeto grdSelecao, 1
        
        InicializaCargaDados_LimpezaTemporario
        
        SelecionaDados
        
        ' Alan Neves
        'InicializaCargaDados_Adicionais
    
'AKIO.OKUNO - INICIO - 08/05/2013
''        If (cmbCriterioPesquisa.Text <> "N�mero Sinistro") And (cmbCriterioPesquisa.Text <> "N�mero Sinistro") And (cmbCriterioPesquisa.Text <> "Proposta") And (cmbCriterioPesquisa.Text <> "CPF") And (cmbCriterioPesquisa.Text <> "Ap�lice") And (cmbCriterioPesquisa.Text <> "Proposta BB") And (cmbCriterioPesquisa.Text <> "N�mero Sinistro BB") And (cmbCriterioPesquisa.Text <> "Nome") Then   'AKIO.OKUNO - 17/04/2013
'        If (cmbCriterioPesquisa.Text <> "N�mero Sinistro") And (cmbCriterioPesquisa.Text <> "Proposta") And (cmbCriterioPesquisa.Text <> "CPF") And (cmbCriterioPesquisa.Text <> "Ap�lice") And (cmbCriterioPesquisa.Text <> "Proposta BB") And (cmbCriterioPesquisa.Text <> "N�mero Sinistro BB") And (cmbCriterioPesquisa.Text <> "Nome") Then                                                        'AKIO.OKUNO - 17/04/2013
'            InicializaCargaDados_Adicionais
'        End If
'AKIO.OKUNO - INICIO - 08/05/2013

        CarregaDadosGrid_Selecao
        
    End If
    
    MousePointer = vbNormal
    
    Exit Sub
    
TrataErro:
    Call TrataErroGeral("btnPesquisar_Click", Me.name)

End Sub

'Paulo Pelegrini - MU-2017-063260 - 16/07/2018 (INI)
Private Sub btnConsultaHistProposta_Click()
    frmHistoricoProposta.proposta_hist = Format(grdSelecao.TextMatrix(grdSelecao.Row, 5), "000000000")
    frmHistoricoProposta.Show
End Sub
'Paulo Pelegrini - MU-2017-063260 - 16/07/2018 (FIM)

Function ValidaDados_SelecaoConsulta() As Boolean
    Dim sTipoSelecao                            As String
    
    On Error GoTo TrataErro
    
  
'    grdSelecao.Rows = 1                        'FLAVIO.BEZERRA - 14/01/2013
'    grdSelecao.Row = 0                         'FLAVIO.BEZERRA - 14/01/2013
'
    sTipoSelecao = cmbCriterioPesquisa.Text
    txtConteudo.Text = Trim(txtConteudo.Text)
'
    ValidaDados_SelecaoConsulta = True
'
'    StatusBar1.SimpleText = ""                 'FLAVIO.BEZERRA - 14/01/2013
'    lblSelecionados = ""                       'FLAVIO.BEZERRA - 14/01/2013

    'FLAVIO.BEZERRA - IN�CIO - 09/01/2013
    If (btnDetalhar.Caption = "Cancelar") And (btnDetalhar.Visible) Then
        mensagem_erro 6, "Detalhamento em andamento"
        ValidaDados_SelecaoConsulta = False
        Exit Function
    End If

    If BtnOk.Enabled Then
        If MsgBox("O detalhamento foi alterado. Deseja abandonar as altera��es ?", vbYesNo) = vbNo Then
            ValidaDados_SelecaoConsulta = False
            Exit Function
        Else
            BtnOk.Enabled = False
            If strTipoProcesso <> strTipoProcesso_Avisar And _
               strTipoProcesso <> strTipoProcesso_Consulta And _
               strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
               strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
                    Call GravaDados_BloqueiaSinistro(False, grdSelecao.TextMatrix(grdSelecao.Row, 1)) 'FLAVIO.BEZERRA - 15/05/2013
            End If
        End If
    End If
    'FLAVIO.BEZERRA - FIM - 09/01/2013
    
    If cmbCriterioPesquisa.ListIndex = -1 Then
        ValidaDados_SelecaoConsulta = False
        MsgBox "O tipo de pesquisa deve ser selecionado.", vbExclamation
        cmbCriterioPesquisa.SetFocus
       
    ElseIf txtConteudo.Text = "" And (cmbCriterioPesquisa.Text <> "Avisos Pendentes de Avalia��o" And _
                                      cmbCriterioPesquisa.Text <> "T�cnico Respons�vel" And _
                                      cmbCriterioPesquisa.Text <> "Sinistros Pendentes de Aprova��o" And _
                                      cmbCriterioPesquisa.Text <> "T�cnicos - Aprova��o") Then
        ValidaDados_SelecaoConsulta = False
        MsgBox "O campo informando o valor para pesquisa deve ser preenchido.", vbExclamation
        txtConteudo.SetFocus
        Exit Function
    End If
    
    grdSelecao.Rows = 1                             'FLAVIO.BEZERRA - 14/01/2013
    grdSelecao.Row = 0                              'FLAVIO.BEZERRA - 14/01/2013
    StatusBar1.SimpleText = ""                      'FLAVIO.BEZERRA - 14/01/2013
    lblSelecionados = ""                            'FLAVIO.BEZERRA - 14/01/2013
    gblsDetalhamento_VisualizarAnt = ""             'FLAVIO.BEZERRA - 14/01/2013
    txtDetalhamento_Anterior = ""                   'FLAVIO.BEZERRA - 14/01/2013
    
    Select Case sTipoSelecao
    
        Case "N�mero Sinistro"
            If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 11 Then
                ValidaDados_SelecaoConsulta = False
                MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
                txtConteudo.SetFocus
            End If
        
        Case "N�mero Sinistro BB"
            If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 13 Then
                ValidaDados_SelecaoConsulta = False
                MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
                txtConteudo.SetFocus
            End If
            
        Case "Proposta"
            If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 9 Then
                ValidaDados_SelecaoConsulta = False
                MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
                txtConteudo.SetFocus
            End If
            
        Case "Proposta BB"
            If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 9 Then
                ValidaDados_SelecaoConsulta = False
                MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
                txtConteudo.SetFocus
            End If
            
        Case "CPF"
            If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 11 Or Not CPF_Ok(txtConteudo) Then
                ValidaDados_SelecaoConsulta = False
                MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
                txtConteudo.SetFocus
            End If
            
        Case "Nome"
            
        Case "Ap�lice"
            If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 9 Then
                ValidaDados_SelecaoConsulta = False
                MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
                txtConteudo.SetFocus
            End If
            
        Case "N� da Certificado SC"
            If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 11 Then
                ValidaDados_SelecaoConsulta = False
                MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
                txtConteudo.SetFocus
            End If
            
        Case "N�mero Sinistro SC"
            If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 11 Then
                ValidaDados_SelecaoConsulta = False
                MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
                txtConteudo.SetFocus
            End If
            
        Case "Avisos Pendentes de Avalia��o"
            If cmbTecnico.ListIndex = -1 Then
                ValidaDados_SelecaoConsulta = False
                MsgBox "Nenhum valor foi selecionado para pesquisa, favor verificar.", vbExclamation
                cmbTecnico.SetFocus
            End If
        
        Case "T�cnico Respons�vel"
            If cmbTecnico.ListIndex = -1 Then
                ValidaDados_SelecaoConsulta = False
                MsgBox "Nenhum valor foi selecionado para pesquisa, favor verificar.", vbExclamation
                cmbTecnico.SetFocus
            ElseIf cmbSitSinistroAvaliador.ListIndex = -1 Then
                ValidaDados_SelecaoConsulta = False
                MsgBox "Nenhum valor foi selecionado para pesquisa, favor verificar.", vbExclamation
                cmbSitSinistroAvaliador.SetFocus
            End If
        
        Case "Sinistros Pendentes de Aprova��o" 'cristovao.rodrigues 11/01/2013 incluido valida��o
            If cmbTecnico.ListIndex = -1 Then
               ValidaDados_SelecaoConsulta = False
                MsgBox "Nenhum valor foi selecionado para pesquisa, favor verificar.", vbExclamation
                cmbTecnico.SetFocus
            End If
        
        Case "T�cnicos - Aprova��o"
            If cmbTecnico.ListIndex = -1 Then
                ValidaDados_SelecaoConsulta = False
                MsgBox "Nenhum valor foi selecionado para pesquisa, favor verificar.", vbExclamation
                cmbTecnico.SetFocus
            End If
                'CRISTOVAO.RODRIGUES - INICIO - (POR AKIO.OKUNO) - 10/10/2012
        'cristovao.rodrigues 05/10/2012
        Case "Nome do Benefici�rio"
         
        Case "CPF do Benefici�rio"
        If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 11 Or Not CPF_Ok(txtConteudo) Then
              ValidaDados_SelecaoConsulta = False
              MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
              txtConteudo.SetFocus
        End If
            
        Case "CNPJ do Benefici�rio", "CNPJ"
        If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 14 Or Not CGC_OK(txtConteudo) Then
            ValidaDados_SelecaoConsulta = False
            MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
            txtConteudo.SetFocus
        End If
        
        Case "Ap�lice L�der"
        If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 30 Then ''EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
            ValidaDados_SelecaoConsulta = False
            MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
            txtConteudo.SetFocus
        End If
        
        Case "N�mero Sinistro L�der"
        If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 30 Then 'EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
            ValidaDados_SelecaoConsulta = False
            MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
            txtConteudo.SetFocus
        End If
        
        ' INCLUS�O DA CONSULTA NUMERO DE ORDEM - EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
           Case "N�mero de Ordem"
        If Not IsNumeric(txtConteudo.Text) Or txtConteudo.Text = "0" Or Len(txtConteudo.Text) > 30 Then 'EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
            ValidaDados_SelecaoConsulta = False
            MsgBox "Valor incorreto para pesquisa, favor verificar.", vbExclamation
            txtConteudo.SetFocus
        End If
        'EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
        
        
                        'CRISTOVAO.RODRIGUES - FIM - (POR AKIO.OKUNO) - 10/10/2012
            
'        Case "Sinistros com novos eventos"
'        Case "Ap�lice da L�der"
'        Case "CNPJ do Benefici�rio"
'        Case "CPF do Benefici�rio"
'        Case "Nome do Benefici�rio"
'        Case "N�mero de Sinistro da L�der"
'        Case "N�mero Sinistro L�der"
                 
    End Select

    'FLAVIO.BEZERRA - IN�CIO - 09/01/2013
    txtDetalhamento_Visualizar.Text = ""
    fmeDetalhamento_Visualizacao.Enabled = False
    fmeDetalhamento_Visualizacao.ForeColor = vbGrayText
    btnSinistro.Visible = False
    btnAgenda.Visible = False
    btnDetalhar.Visible = False
    btnPagamento.Visible = False
    btnAssessoria.Visible = False
    BtnOk.Visible = False
    
    cmd_prazo_seguradora.Visible = False '<- FLOW 18225198 - Petrauskas Jan/2016
    
    If cmbCriterioPesquisa.Text = "Sinistros Pendentes de Aprova��o" Then
        btnAprovar.Visible = True
        btnAprovar.Enabled = True
    Else
        btnAprovar.Visible = False
    End If
    'FLAVIO.BEZERRA - FIM - 09/01/2013
    
    Exit Function

TrataErro:
    Call TrataErroGeral("ValidaDados_SelecaoConsulta", Me.name)

End Function
Private Function ValidaDados_Tecnico(dSinistro_ID As Double) As Boolean
    Dim sSQL                                    As String
    Dim sLoginRede                              As String
    Dim rsAux As ADODB.Recordset  '<- FLOW 18225198 - Petrauskas Jan/2016
    
    On Error GoTo Erro

    MousePointer = vbHourglass
    
    ValidaDados_Tecnico = True
    sLoginRede = ""
    
'    If EstadoConexao(lConexaoLocal) = adStateClosed Then
'        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, _
'                                      glAmbiente_id, _
'                                      App.Title, _
'                                      App.FileDescription)
'    End If
    
    If bValida_Tecnico And dSinistro_ID <> 0 Then
        
'+----------------------------------------------------------------------
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'|
        sSQL = "SET NOCOUNT ON  Exec SEGS13112_SPS " & dSinistro_ID
'        sSQL = ""
'        sSQL = sSQL & "Select EMail " & vbNewLine
'        sSQL = sSQL & "     , Nome " & vbNewLine
'        sSQL = sSQL & "  From Sinistro_Tecnico_Tb                   Sinistro_Tecnico_Tb  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & " Inner Join Tecnico_Tb                       Tecnico_Tb  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & "    on Sinistro_Tecnico_Tb.Tecnico_ID        = Tecnico_Tb.Tecnico_ID " & vbNewLine
'        sSQL = sSQL & "  Join Sinistro_Tb                           Sinistro_Tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "    on Sinistro_Tb.Sinistro_ID               = Sinistro_Tecnico_Tb.Sinistro_ID " & vbNewLine
'        sSQL = sSQL & " Where Sinistro_Tb.Sinistro_ID               = " & dSinistro_ID & vbNewLine
'        sSQL = sSQL & "   and Sinistro_Tb.Sucursal_Seguradora_ID    = Sinistro_Tb.Sucursal_Seguradora_ID " & vbNewLine
'        sSQL = sSQL & "   and Sinistro_Tb.Seguradora_Cod_Susep      = Sinistro_Tb.Seguradora_Cod_Susep " & vbNewLine
'        sSQL = sSQL & "   and Sinistro_Tb.Ramo_ID                   = Sinistro_Tb.Ramo_ID " & vbNewLine
'        sSQL = sSQL & "   and Sinistro_Tecnico_Tb.Dt_fim_vigencia   IS NULL "
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'+----------------------------------------------------------------------
        
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, True)
        With rsRecordSet
            If .EOF Then
                mensagem_erro 6, "Ainda n�o foi feita a atribui��o de t�cnico respons�vel para este sinistro !"
                ValidaDados_Tecnico = False
            Else
                sLoginRede = BuscaLogin(cUserName)  'Passando CPF para buscar
                If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
                    sLoginRede = BuscaLogin(cUserName, 1)
                End If
'                If LCase(Trim("" & rsRecordSet("EMail"))) <> LCase(Trim(sLoginRede)) Then
'                    mensagem_erro 6, "O t�cnico respons�vel por este sinistro � : " & rsRecordSet("Nome")
'                    ValidaDados_Tecnico = False
'                End If
            End If
        End With
    
    End If
    
'+-------------------------------------------------
'| FLOW 18225198 - INICIO
'| Petrauskas Jan/2016
    usuario_is_tecnico_ativo = False
    If sLoginRede = "" Then
      If glAmbiente_id = 6 Or glAmbiente_id = 7 Then
        sLoginRede = BuscaLogin(cUserName, 1)
      Else
        sLoginRede = BuscaLogin(cUserName)
      End If
    End If
        
'    sSQL = "SELECT Ativo FROM Tecnico_tb WITH (NOLOCK) WHERE email = '" & sLoginRede & "'"  - INC000005116143 - Petrauskas
    sSQL = "SELECT Ativo FROM Tecnico_tb WITH (NOLOCK) WHERE email = '" & sLoginRede & "' AND Ativo = 's'"
        
    Set rsAux = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    sSQL, _
                                    lConexaoLocal, True)
        
    If Not rsAux.EOF Then
      'INC000005116143 - Petrauskas
      usuario_is_tecnico_ativo = True
      'If Not IsNull(rsAux!ativo) Then
      '  usuario_is_tecnico_ativo = (UCase(rsAux!ativo) = "S")
      'End If
    End If
    
    rsAux.Close: Set rsAux = Nothing
'| FLOW 18225198 - FIM
'+-------------------------------------------------
    
    MousePointer = vbNormal
    
    Set rsRecordSet = Nothing
    
    Exit Function
    Resume
Erro:
    Call TrataErroGeral("ValidaDados_Tecnico", "frmConsultaSinistroCon")
    Call FinalizarAplicacao

End Function
Private Sub InicializaInterface_Detalhamento(bHabilita As Boolean)

    With txtDetalhamento_Visualizar
        .Visible = True
        
        If bHabilita Then
'            .Width = 7535
'            .Left = 500
            .Font.name = "Courier"
            .SelStart = 0
            .SelLength = Len(.Text)
            .SelColor = vbButtonText
            .SelLength = 0
            .Text = vDetalhamento_Visualizar
            .RightMargin = .Width - 400
            
            optAnotacao.Visible = True
            'optRequisicao.Visible = True         'FLAVIO.BEZERRA - 21/01/2013
            chkRestrito.Visible = True
            BtnOk.Visible = False
            btnDetalhar.Caption = "Cancelar"
        Else
'            .Width = 8535
'            .Left = 15
            .Font.name = "Arial"
            .SelStart = 0
            .SelLength = Len(.Text)
            .SelColor = vbGrayText
            .SelLength = 0
            .Locked = True
            .RightMargin = 8500
            
            optAnotacao.Visible = False
            'optRequisicao.Visible = False              'FLAVIO.BEZERRA - 21/01/2013
            chkRestrito.Visible = False
            BtnOk.Visible = True
            btnDetalhar.Caption = "Detalhar"
            ' Acerta a rightmargin para criar o efeito de word wrap.
        End If
    End With
End Sub

Private Function ValidaDados_SinistroSelecionado() As Boolean
    Dim sSQL                                    As String
    Dim rsRecordSet                             As ADODB.Recordset
    Dim bProcessa                               As Boolean
    Dim bTecnico_Atribuido                      As Boolean
    Dim sDescricao_Situacao_Sinistro            As String
    Dim sSem_Comunicacao_BB                     As String       'AKIO.OKUNO - 17/04/2013
    
    
    On Error GoTo Erro
    
    MousePointer = vbHourglass
         
    With grdSelecao
'AKIO.OKUNO
'        gbllApolice_ID = "0" & .TextMatrix(.Row, 3)
'        gbllProposta_ID = "0" & .TextMatrix(.Row, 4)
'        gbllRamo_ID = "0" & .TextMatrix(.Row, 13)
'        If strTipoProcesso <> strTipoProcesso_Avisar Then
'            If strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then
'                gbldSinistro_ID = "0" & .TextMatrix(.Row, 1)
'            End If
'        End If
'        gbldDt_Ocorrencia_Sinistro = IIf(.TextMatrix(.Row, 14) = "", Data_Sistema, .TextMatrix(.Row, 14))
'        gbllProduto_ID = "0" & .TextMatrix(.Row, 12)
'AKIO
        sDescricao_Situacao_Sinistro = Trim(.TextMatrix(.Row, 9))
        sSem_Comunicacao_BB = Trim(.TextMatrix(.Row, 17))               'AKIO.OKUNO - 17/04/2013
        sSegmentoCliente = Trim(.TextMatrix(.Row, 18))                  'AKIO.OKUNO - 17/04/2013
    End With
    
    bProcessa = True
   
   
'AKIO.OKUNO - INICIO - 21/04/2013
'TRECHO TRANSPORTADO DO FINAL DESTA ROTINA PARA O INICIO, POIS TRATA-SE DE UMA VALIDA��O COM SAIDA EM CASO DE VERDADEIRO.
    If Not IsNull(gbldSinistro_ID) And Trim(gbldSinistro_ID) <> "" Then
        If strTipoProcesso <> strTipoProcesso_Consulta And _
            strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro And _
                strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
            'Demanda 17922837 - IN�CIO
            'If sSem_Comunicacao_BB = "T" Then
            If sSem_Comunicacao_BB = "T" And bytTipoRamo <> 1 Then
            'Demanda 17922837 - FIM
                MsgBox ("Este sinistro foi gerado sem comunica��o com o BB, sendo poss�vel ser visualizado no modo de Consulta ou de Visualizar.")
                MousePointer = vbNormal
                Exit Function
            End If
        End If
    End If
'AKIO.OKUNO - FIM - 21/04/2013

   
   
   'Valida��es gen�ricas
    bTecnico_Atribuido = True
    If strTipoProcesso <> strTipoProcesso_Avisar And strTipoProcesso <> strTipoProcesso_Consulta Then
        If Not ValidaDados_Tecnico(gbldSinistro_ID) Then
            bProcessa = False
            bTecnico_Atribuido = False
        End If
    End If
    
    If strTipoProcesso = strTipoProcesso_Encerrar Or _
       strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Then
'        If sDescricao_Situacao_Sinistro = "ENCERRADO" Then     'AKIO.OKUNO - 01/10/2012
        If sDescricao_Situacao_Sinistro = "ENCERRADO" Or sDescricao_Situacao_Sinistro = "ENCERRADO AG. FINANCEIRO" Then
            MsgBox "Este sinistro encontra-se " & sDescricao_Situacao_Sinistro & ". N�o pode ser encerrado novamente.", vbOKOnly + vbExclamation
            bProcessa = False
        End If
        
        'mathayde
        Dim Localizacao As String
        If bGTR_Implantado Then 'AKIO.OKUNO - ACRESCENTADO
            Localizacao = Seleciona_Localizacao_Processo(gbldSinistro_ID)
            
            'Ricardo Toledo (Confitec) : IM00015354 : 14/03/2017 : INI
            'Quando o sinistro estava "Reaberto Administrativamente", ao encerr�-lo ocorria uma mensagem conforme descrito no incidente
            'S� ser� permitido encerrar se estiver virado para a seguradora, raandre - flow214652
            'If Localizacao <> 2 Then
            If Localizacao = 1 Then
            'Ricardo Toledo (Confitec) : IM00015354 : 14/03/2017 : FIM
               MsgBox "Este sinistro n�o se encontra na Seguradora. N�o pode ser encerrado.", vbOKOnly + vbExclamation
               bProcessa = False
               Exit Function
            End If
        End If                  'AKIO.OKUNO - ACRESCENTADO

    End If
    
    
    'If bProcessa Then         'FLAVIO.BEZERRA - 07/03/2013
    If bProcessa Or strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        Select Case strTipoProcesso
            Case strTipoProcesso_Avaliar_Com_Imagem
                If bTecnico_Atribuido Then
                    btnPagamento.Visible = True
                    btnAprovar.Visible = True
                    btnAssessoria.Visible = True
                    btnDetalhar.Visible = True
                    btnSinistro.Caption = "Sinistro"
                    btnSinistro.Visible = True
                    btnAgenda.Visible = True
                    
                    cmd_prazo_seguradora.Visible = ((bytTipoRamo = bytTipoRamo_Vida) Or (bytTipoRamo = bytTipoRamo_Rural) Or (gbllProduto_ID = 1201) Or (gbllProduto_ID = 1226) Or (gbllProduto_ID = 1227)) '<- FLOW 18225198 - Petrauskas Jan/2016
                    
'                    If sSinistro_Situacao <> "2" Then      'AKIO.OKUNO - 05/10/2012
                    If gblsSinistro_Situacao_ID <> "2" Then
                        btnPagamento.Enabled = True
                    Else
                        btnPagamento.Enabled = False
                    End If
                    btnConsultaProposta.Enabled = True
                    'FLAVIO.BEZERRA - IN�CIO - 10/01/2013
                    If ValidaDados_AnaliseTecnica() Then
                        btnAnaliseTecnica.Visible = True
                        btnAnaliseTecnica.Enabled = True
                    End If
                    'FLAVIO.BEZERRA - IN�CIO - 10/01/2013
                Else
                    btnPagamento.Visible = False
                    btnAprovar.Visible = False
                    btnAssessoria.Visible = False
                    btnDetalhar.Visible = False
                    btnSinistro.Caption = "Sinistro"
                    btnSinistro.Visible = False
                    btnAgenda.Visible = False
                    
                    cmd_prazo_seguradora.Visible = False   '<- FLOW 18225198 - Petrauskas Jan/2016
                    
'                    If sSinistro_Situacao <> "2" Then      'AKIO.OKUNO - 05/10/2012
                    If gblsSinistro_Situacao_ID <> "2" Then
                        btnPagamento.Enabled = True
                    Else
                        btnPagamento.Enabled = False
                    End If
                End If
                'FLAVIO.BEZERRA - IN�CIO - 09/01/2013
                fmeDetalhamento_Visualizacao.Enabled = True
                fmeDetalhamento_Visualizacao.ForeColor = vbButtonText
                'FLAVIO.BEZERRA - FIM - 09/01/2013
                'InicializaInterface_Detalhamento False 'FLAVIO.BEZERRA - 07/03/2013
            
                'Verifica se existem imagens para habilitar o bot�o
                sSQL = ""
                sSQL = sSQL & "Set nocount on " & vbNewLine
                sSQL = sSQL & " exec imagem_db..SCIS0344_SPS 1, " & gbllProposta_ID & ", null, " & gbllProduto_ID & ", 1"
                Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            sSQL, _
                                            lConexaoLocal, _
                                            True)
                
                If rsRecordSet.Fields(0) = "S" Then
                    btnConsultaImagemProposta.Visible = True
                    btnAnaliseTecnica.Left = 2520   'FLAVIO.BEZERRA - 07/01/2013
                ElseIf rsRecordSet.Fields(0) = "N" Then
                    btnConsultaImagemProposta.Visible = False
                    btnAnaliseTecnica.Left = 5280   'FLAVIO.BEZERRA - 07/01/2013
                End If
                Set rsRecordSet = Nothing
        End Select
        
        If bProcessa Then          'AKIO.OKUNO - TRANSPORTADO PARA O INICIO DESTA ROTINA - 21/04/2013
        
        'cristovao.rodrigues 01/11/2012 inicio
        '233151 / v 516  FLOW14197479 eduamapa (Nova)
        'eduardo.amaral(Nova Consultoria)
        '-----------------------------------------------------------------
        '233454 / v 517  Estava ocasionando erro no aviso de sinistro.    rgtoledo
        'Ricardo Toledo (Confitec) : 06/09/2012 : INC000003700691 : Inicio
        'If Not IsNull(vSinistro) Then
        
'        If Not IsNull(gbldSinistro_ID) And Trim(gbldSinistro_ID) <> "" Then        'AKIO.OKUNO - TRANSPORTADO PARA O INICIO DESTA ROTINA - 21/04/2013
        'Ricardo Toledo (Confitec) : 06/09/2012 : INC000003700691 : fim
        '-----------------------------------------------------------------
'AKIO.OKUNO - INICIO - 17/04/2013
'            sSQL = "select count(sinistro_id) as sem_comunicacao_bb " & _
'                    "  from sinistro_tb " & _
'                    " where sinistro_id = " & gbldSinistro_ID & _
'                    "   and sem_comunicacao_bb = 'T'"
'
'            Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                            glAmbiente_id, _
'                                            App.Title, _
'                                            App.FileDescription, _
'                                            sSQL, _
'                                            lConexaoLocal, _
'                                            True)
'
'            If rsRecordSet(0) > 0 Then
'                If strTipoProcesso <> strTipoProcesso_Consulta And _
'                    strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro And _
'                        strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then 'critov�o.rodrigues 01/02/2013
'                    MsgBox ("Este sinistro foi gerado sem comunica��o com o BB, sendo poss�vel ser visualizado no modo de Consulta ou de Visualizar.")
'                    Exit Function
'                End If
'            End If
'
'            rsRecordSet.Close
'AKIO.OKUNO - INICIO - TRANSPORTADO PARA O INICIO DESTA ROTINA - 21/04/2013
'            If strTipoProcesso <> strTipoProcesso_Consulta And _
'                strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro And _
'                    strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'                If sSem_Comunicacao_BB = "T" Then
'                    MsgBox ("Este sinistro foi gerado sem comunica��o com o BB, sendo poss�vel ser visualizado no modo de Consulta ou de Visualizar.")
'                    Exit Function
'                End If
'            End If
'AKIO.OKUNO - TRANSPORTADO PARA O FIM DESTA ROTINA - 21/04/2013
'AKIO.OKUNO - FIM - 17/04/2013
 '       End If     'AKIO.OKUNO - TRANSPORTADO PARA O FIM DESTA ROTINA - 21/04/2013
        'fim eduardo.amaral(Nova Consultoria)
        'cristovao.rodrigues 01/11/2012 fim
        
            If gbllProduto_ID = 114 And strTipoProcesso <> "3" Then
               mensagem_erro 6, "O Sinistro selecionado pertence ao produto Seguro Cart�o de cr�dito e n�o pode ser regulado !" & vbCrLf & "Utilize a op��o de Consulta."
               Exit Function
            End If
            
            'Valida��es espec�ficas por Tipo de Processo
            Select Case strTipoProcesso
                Case strTipoProcesso_Consulta _
                   , strTipoProcesso_Avaliar _
                   , strTipoProcesso_Avaliar_Com_Imagem _
                   , strTipoProcesso_Avaliar_Cosseguro _
                   , strTipoProcesso_Reabrir _
                   , strTipoProcesso_Reabrir_Cosseguro _
                   , strTipoProcesso_Encerrar _
                   , strTipoProcesso_Encerrar_Cosseguro
                   
'+----------------------------------------------------------------------
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'|
                    sSQL = "SET NOCOUNT ON  Exec SEGS13113_SPS " & gbllProposta_ID & ", '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "'"

                    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                sSQL, _
                                                lConexaoLocal, _
                                                True)
                    If Not rsRecordSet.EOF Then
                      If Trim(rsRecordSet(0) & "") <> "" Then
                        MsgBox Trim(rsRecordSet(0) & ""), vbInformation, "Mensagem ao Usu�rio"
                      End If
                    End If
                    rsRecordSet.Close
                    Set rsRecordSet = Nothing

'                    'Valida��o de parcelas em atraso
'                    sSQL = ""
'                    sSQL = sSQL & "Select Count(Num_Cobranca)"
''                    sSQL = sSQL & "  From Agendamento_Cobranca_Tb  WITH (NOLOCK) "    'AKIO.OKUNO - 17/04/2013
'                    sSQL = sSQL & "  From Agendamento_Cobranca_Atual_Tb  WITH (NOLOCK) "     'AKIO.OKUNO - 17/04/2013
'                    sSQL = sSQL & " Where Proposta_ID = " & gbllProposta_ID
'                    sSQL = sSQL & "   and Dt_Agendamento <= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "'"
'                    sSQL = sSQL & "   and Dt_Recebimento is Null "
'                    sSQL = sSQL & "   and Canc_Endosso_ID is Null"
'                    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                                glAmbiente_id, _
'                                                App.Title, _
'                                                App.FileDescription, _
'                                                sSQL, _
'                                                lConexaoLocal, _
'                                                True)
'                    If rsRecordSet.Fields(0) > 0 Then
'                        MsgBox "Existe(m) parcela(s) em atraso para esta proposta, anterior(es) a data de ocorr�ncia do sinistro.", 64, "Mensagem ao Usu�rio"
'                    Else
'                        sSQL = ""
'                        sSQL = sSQL & "Select Count(1)"
'                        sSQL = sSQL & "  From Cobranca_Inadimplente_Tb Cobranca_Inadimplente_Tb  WITH (NOLOCK) "
'                        sSQL = sSQL & " Inner Join Agendamento_Cobranca_Atual_Tb Agendamento_Cobranca_Atual_Tb  WITH (NOLOCK) "
'                        sSQL = sSQL & "    on Cobranca_Inadimplente_Tb.Proposta_ID = Agendamento_Cobranca_Atual_Tb.Proposta_ID"
'                        sSQL = sSQL & "   and Cobranca_Inadimplente_Tb.Num_Cobranca = Agendamento_Cobranca_Atual_Tb.Num_Cobranca"
'                        sSQL = sSQL & " Where Cobranca_Inadimplente_Tb.Proposta_ID = " & gbllProposta_ID
'                        sSQL = sSQL & "   and Situacao <> 'a'"
'                        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                                    glAmbiente_id, _
'                                                    App.Title, _
'                                                    App.FileDescription, _
'                                                    sSQL, _
'                                                    lConexaoLocal, _
'                                                    True)
'
'                        If rsRecordSet.Fields(0) > 0 Then
'                            MsgBox "Existe(m) parcela(s) em atraso para esta proposta, anterior(es) a data de ocorr�ncia do sinistro.", 64, "Mensagem ao Usu�rio"
'                        End If
'                    End If
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'+----------------------------------------------------------------------
                    
                    If strTipoProcesso = strTipoProcesso_Reabrir Or _
                       strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro Then
                        'mathayde
                        'If sDescricao_Situacao_Sinistro <> "ENCERRADO" Then
'                        If Left(sDescricao_Situacao_Sinistro, 9) <> "ENCERRADO" Then   'AKIO.OKUNO - 04/10/2012
                        If Left(sDescricao_Situacao_Sinistro, 9) <> "ENCERRADO" Or gblsSinistro_Situacao_ID = 0 Or gblsSinistro_Situacao_ID = 5 Or gblsSinistro_Situacao_ID = 1 Then
                            sSQL = ""
                            sSQL = sSQL & "SELECT Pgto_Sinistro_Tb.acerto_id"
                            sSQL = sSQL & "  From Seguros_dB.dbo.Pgto_Sinistro_Tb               Pgto_Sinistro_Tb  WITH (NOLOCK)  "
                            sSQL = sSQL & "  Join Seguros_Db.Dbo.Ps_Acerto_Recebimento_Tb       Ps_Acerto_Recebimento_Tb  WITH (NOLOCK) "
                            sSQL = sSQL & "    on (Pgto_Sinistro_Tb.Acerto_ID                   = Ps_Acerto_Recebimento_Tb.Acerto_ID)"
                            sSQL = sSQL & " Where Sinistro_id                                   = " & gbldSinistro_ID
                            sSQL = sSQL & " Union"
                            sSQL = sSQL & " Select Pgto_Sinistro_Tb.Acerto_ID "
                            sSQL = sSQL & "   From Seguros_Db.Dbo.Pgto_Sinistro_Tb              Pgto_Sinistro_Tb  WITH (NOLOCK)  "
                            sSQL = sSQL & "  Inner Join Seguros_Db.Dbo.Ps_Acerto_Pagamento_Tb   Ps_Acerto_Pagamento_Tb  WITH (NOLOCK)  "
                            sSQL = sSQL & "     on Ps_Acerto_Pagamento_Tb.Acerto_ID             = Pgto_Sinistro_Tb.Acerto_ID "
                            sSQL = sSQL & "  Where Sinistro_ID                                  = " & gbldSinistro_ID
                            
                            Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                                                                , glAmbiente_id _
                                                                , App.Title _
                                                                , App.FileDescription _
                                                                , sSQL _
                                                                , lConexaoLocal _
                                                                , True)
                            
                            If rsRecordSet.EOF Then
                                MsgBox sDescricao_Situacao_Sinistro & " n�o pode ser reaberto!", vbCritical, "Sinistro"
                                btnPesquisar.SetFocus
                                bProcessa = False
                            Else
                                MsgBox "Este sinistro encontra-se " & sDescricao_Situacao_Sinistro & ". N�o pode ser reaberto.", vbOKOnly + vbExclamation
                                bProcessa = False
                            End If
                            Set rsRecordSet = Nothing
                        End If
                    End If
                
                Case strTipoProcesso_Avisar, strTipoProcesso_Avisar_Cosseguro
                    'Verifica se a proposta � Ades�o
                    sSQL = ""
                    sSQL = sSQL & "Select Count(Proposta_ID)" & vbNewLine
                    sSQL = sSQL & "  From Proposta_Adesao_Tb  WITH (NOLOCK)  " & vbNewLine
                    sSQL = sSQL & " Where Proposta_ID = " & gbllProposta_ID
            
                    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                sSQL, _
                                                lConexaoLocal, _
                                                True)
                    If rsRecordSet.Fields(0) = 0 Then
                       'Verifica se tem Ades�o nessa Ap�lice
                        sSQL = ""
                        sSQL = sSQL & "Set RowCount 1"
                        sSQL = sSQL & "Select 1 "
                        sSQL = sSQL & "  From Proposta_Adesao_Tb  WITH (NOLOCK) "
                        sSQL = sSQL & " Where Apolice_ID            = " & gbllApolice_ID
                        sSQL = sSQL & "   and Ramo_ID               = " & gbllRamo_ID
                        sSQL = sSQL & "Set RowCount 0"
                        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                    glAmbiente_id, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    sSQL, _
                                                    lConexaoLocal, _
                                                    True)
                        If Not rsRecordSet.EOF Then
                            'Verifica se o produto tem permiss�o para avisar na proposta b�sica
                            sSQL = ""
                            'mathayde
                            'sSQL = sSQL & "Select Permite_Aviso_Proposta_Basica"
                            sSQL = sSQL & "Select Permite_Aviso_Proposta_Basica = upper(Permite_Aviso_Proposta_Basica)"
                            sSQL = sSQL & "  From Item_Produto_Tb  WITH (NOLOCK) "
                            sSQL = sSQL & " Where Produto_ID = " & gbllProduto_ID
                            sSQL = sSQL & "   and Ramo_ID = " & gbllRamo_ID
                            Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                        glAmbiente_id, _
                                                        App.Title, _
                                                        App.FileDescription, _
                                                        sSQL, _
                                                        lConexaoLocal, _
                                                        True)
                            If Not rsRecordSet.EOF Then
                                If rsRecordSet.Fields(0) = "N" Then
                                    MsgBox "Voc� n�o pode avisar o sinistro na proposta b�sica !", vbCritical, "Sinistro"
                                    ValidaDados_SinistroSelecionado = False
                                    bProcessa = False
                                    MousePointer = vbNormal
                                    Exit Function
                                End If
                            End If
                        End If
                    End If
                    sSQL = ""
                    sSQL = sSQL & "Select Count(1) "
                    sSQL = sSQL & "  From Sinistro_Tb  WITH (NOLOCK) "
                    sSQL = sSQL & " Where Proposta_ID = " & gbllProposta_ID
                    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                sSQL, _
                                                lConexaoLocal, _
                                                True)
                    
                    If rsRecordSet.Fields(0) > 0 Then
                        If MsgBox("J� existe(m) " & rsRecordSet.Fields(0) & " sinistro(s) cadastrado(s) para o item selecionado. " & Chr(13) & "Confirma novo aviso?", 292, "Confirma��o de Aviso") = 7 Then
                            LimpaCamposTelaLocal Me
                            bProcessa = False
                        End If
                    End If
                
                Case strTipoProcesso_Encerrar
                
            End Select
            
            
'AKIO.OKUNO - INICIO - 17/04/2013
'            If bProcessa Then
'                sSQL = ""
'                sSQL = sSQL & "Select dbo.Retornar_Seg_Cliente_fn(" & CStr(gbllProposta_ID) & ")"
'
'                Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                                      glAmbiente_id, _
'                                                      App.Title, _
'                                                      App.FileDescription, _
'                                                      sSQL, _
'                                                      lConexaoLocal, _
'                                                      True)
'                With rsRecordSet
'                    If Not .EOF Then
'                        Select Case .Fields(0)
'                            Case 0
'                                sSegmentoCliente = "NORMAL"
'                            Case 1
'                                sSegmentoCliente = "PRIVATE"
'                            Case 2
'                                sSegmentoCliente = "ESTILO"
'                        End Select
'                    End If
'                End With
'
'            End If
'AKIO.OKUNO - FIM - 17/04/2013
        
        End If
        
    End If
    
    MousePointer = vbNormal

    Set rsRecordSet = Nothing
    
    'FLAVIO.BEZERRA - IN�CIO - 07/03/2013
    'ValidaDados_SinistroSelecionado = bProcessa
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        ValidaDados_SinistroSelecionado = True
    Else
        ValidaDados_SinistroSelecionado = bProcessa
    End If
    'FLAVIO.BEZERRA - FIM - 07/03/2013
    
    
    Exit Function

Erro:
    Call TrataErroGeral("ValidaDados_SinistroSelecionado", "frmConsultaSinistroCon")
    Call FinalizarAplicacao
'Resume
End Function

Function SelecionaDados()
 
    Dim sTipoSelecao As String
    Dim lRetorno As Boolean

    sTipoSelecao = cmbCriterioPesquisa.Text
    grdSelecao.Rows = 1
        
    Select Case sTipoSelecao
    
        Case "N�mero Sinistro" ' ru 9200500389 / vi 98201200017 / re 61201200001
            lRetorno = ConsultaSinistroPor_Sinistro(txtConteudo.Text, bytTipoRamo)
        
        Case "N�mero Sinistro BB" ' 20050400620 / 20061201521 / 20120100501
            lRetorno = ConsultaSinistroPor_SinistroBB(txtConteudo.Text, bytTipoRamo)
            
        Case "N�mero de Protocolo" ' Laurent Silva - 10/11/2017 - Demanda HiAgro
            ''lRetorno = ConsultaSinistroPor_Sinistro(txtConteudo.Text, bytTipoRamo)

            Select Case bytTipoRamo
                Case bytTipoRamo_Vida
                    ConsultaSinistroPor_Protocolo_RamoRural txtConteudo.Text, bytTipoRamo, "VI"
                Case bytTipoRamo_RE
                    ConsultaSinistroPor_Protocolo_RamoRural txtConteudo.Text, bytTipoRamo, "RE"
                Case bytTipoRamo_Rural
                    ConsultaSinistroPor_Protocolo_RamoRural txtConteudo.Text, bytTipoRamo_RE, "RU"
            End Select
            
        Case "Proposta" '8330160 / 26021407 / 22591641
            lRetorno = ConsultaSinistroPor_Proposta(txtConteudo.Text, bytTipoRamo, sOperacaoCosseguro = "C")
            
        Case "Proposta BB" '175100256 / 41135410 e 41264772 / 69895
            lRetorno = ConsultaSinistroPor_PropostaBB(txtConteudo.Text, bytTipoRamo)
            
        Case "CPF" '14462788672 / 00554362775 / 81342764900
            lRetorno = ConsultaSinistroPor_CPF(txtConteudo.Text, bytTipoRamo)
            
        Case "Nome" 'MAGNUS CORRADI MELLO / elio / HUMBERTO RODRIGUES
             lRetorno = ConsultaSinistroPor_Nome(txtConteudo.Text, bytTipoRamo)
            
        Case "Ap�lice" '36 / 13268 / 2372
            lRetorno = ConsultaSinistroPor_Apolice(txtConteudo.Text, bytTipoRamo)
            
        Case "N� da Certificado SC"
            lRetorno = ConsultaSinistroPor_CertificadoSC(txtConteudo.Text, bytTipoRamo) 'somente VIDA
            
        Case "N�mero Sinistro SC"
            lRetorno = ConsultaSinistroPor_SinistroSC(txtConteudo.Text, bytTipoRamo) 'somente VIDA
            
        Case "Avisos Pendentes de Avalia��o" 'eduardo beltrame / daniel gustavo / claudia cristina // willian dos santos ///
            lRetorno = ConsultaSinistroPor_AvisosPendentesdeAvaliacao(cmbTecnico.ItemData(cmbTecnico.ListIndex), bytTipoRamo)
            
        Case "T�cnico Respons�vel" 'GILBERTO DUR�ES DOS SANTOS // willian dos santos
            lRetorno = ConsultaSinistroPor_TecnicoResponsavel(cmbTecnico.ItemData(cmbTecnico.ListIndex), cmbSitSinistroAvaliador.ListIndex, bytTipoRamo)
                     
        Case "Sinistros Pendentes de Aprova��o"
            lRetorno = ConsultaSinistroPor_SituacaoPendenteAprovacao("SinistrosPendentesAprovacao", cmbTecnico.ItemData(cmbTecnico.ListIndex), bytTipoRamo) '--- cristovao.rodrigues 11/01/20123 estava com cUserName
     
        Case "T�cnicos - Aprova��o" 'MARCIO GUSHIKEM MORISHITA
            lRetorno = ConsultaSinistroPor_SituacaoPendenteAprovacao("TecnicosAprovacao", cmbTecnico.ItemData(cmbTecnico.ListIndex), bytTipoRamo)
            
'        Case "Sinistros com novos eventos"
        Case "Ap�lice L�der"    'CRISTOVAO.RODRIGUES (POR AKIO.OKUNO) - 10/10/2012
            ConsultaSinistroPor_ApoliceLider (txtConteudo.Text)
                'CRISTOVAO.RODRIGUES - INICIO - (POR AKIO.OKUNO) - 10/10/2012
'cristovao.rodrigues 05/10/2012 inclusao de pesquisas
    Case "N�mero Sinistro L�der"
        lRetorno = ConsultaSinistroPor_NumeroSinistroLider(txtConteudo.Text, bytTipoRamo)
        
    Case "CNPJ", "CNPJ do Benefici�rio"
        lRetorno = ConsultaSinistroPor_CNPJ(txtConteudo.Text, bytTipoRamo)
        
    Case "Nome do Benefici�rio"
        lRetorno = ConsultaSinistroPor_NomedoBeneficiario(txtConteudo.Text, bytTipoRamo)
        
    Case "CPF do Benefici�rio"
        lRetorno = ConsultaSinistroPor_CPFdoBeneficiario(txtConteudo.Text, bytTipoRamo)
                'CRISTOVAO.RODRIGUES - FIM - (POR AKIO.OKUNO) - 10/10/2012
        Case "N�mero de Ordem"   'iNCLUS�O DA CONSULTA NUMERO DE ORDEM - EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
            ConsultaSinistroPor_NumOrdemCosseguro (txtConteudo.Text)
    
    Case "N�mero de protocolo" '' Laurent Silva - 10/11/2017 - Demanda HiAgro
        
    ' Cesar Santos CONFITEC - (SBRJ009952) 04/06/2020
    Case "N�mero Sinistro Cardif"
          lRetorno = ConsultaSinistroPor_Sinistro_Cardif_RamoVIDA(txtConteudo.Text)
		  
    End Select
    
End Function

'cristovao.rodrigues 05/10/2012
Private Function ConsultaSinistroPor_CNPJ(sCnpj As String, bTP_Ramo As Byte)

    Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_CNPJ_Ramo sCnpj, bTP_Ramo, "VI"
        Case bytTipoRamo_RE
            ConsultaSinistroPor_CNPJ_Ramo sCnpj, bTP_Ramo, "RE"
        Case bytTipoRamo_Rural
            ConsultaSinistroPor_CNPJ_Ramo sCnpj, 2, "RU"
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_CNPJ", Me.name)

End Function

Private Function ConsultaSinistroPor_NumeroSinistroLider(nSinistroLider As String, bTP_Ramo As Byte)

Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_NumeroSinistroLider_Ramo nSinistroLider, bTP_Ramo, "VI"
        Case bytTipoRamo_RE
            ConsultaSinistroPor_NumeroSinistroLider_Ramo nSinistroLider, bTP_Ramo, "RE"
        Case bytTipoRamo_Rural
            ConsultaSinistroPor_NumeroSinistroLider_Ramo nSinistroLider, 2, "RU"
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_NumeroSinistroLider", Me.name)
    
End Function

Private Function ConsultaSinistroPor_NomedoBeneficiario(sNomeBeneficiario As String, bTP_Ramo As Byte)

'Select Case bTP_Ramo
'        Case bytTipoRamo_Vida
'            ConsultaSinistroPor_NomedoBeneficiario_RamoVIDA sNomeBeneficiario
'        Case bytTipoRamo_RE
'            ConsultaSinistroPor_NomedoBeneficiario_RamoRural sNomeBeneficiario, bTP_Ramo, "RE"
'        Case bytTipoRamo_Rural
'            ConsultaSinistroPor_NomedoBeneficiario_RamoRural sNomeBeneficiario, 2, "RU"
'    End Select
'
'    Exit Function
'
'TrataErro:
'    Call TrataErroGeral("ConsultaSinistroPor_NomedoBeneficiario", Me.name)

End Function

Private Function ConsultaSinistroPor_CPFdoBeneficiario(sCpfBeneficiario As String, bTP_Ramo As Byte)

'Select Case bTP_Ramo
'        Case bytTipoRamo_Vida
'            ConsultaSinistroPor_CPFdoBeneficiario_RamoVIDA sCpfBeneficiario
'        Case bytTipoRamo_RE
'            ConsultaSinistroPor_CPFdoBeneficiario_RamoRural sCpfBeneficiario, bTP_Ramo, "RE"
'        Case bytTipoRamo_Rural
'            ConsultaSinistroPor_CPFdoBeneficiario_RamoRural sCpfBeneficiario, 2, "RU"
'    End Select
'
'    Exit Function
'
'TrataErro:
'    Call TrataErroGeral("ConsultaSinistroPor_CPFdoBeneficiario", Me.name)

End Function


'cristovao.rodrigues 05/10/2012
Private Sub ConsultaSinistroPor_CNPJ_Ramo(sCnpj As String, iTPRamo As Byte, sVariacao As String)

    Dim sSQL    As String
    Dim rs      As ADODB.Recordset
    
    On Error GoTo Erro
    
    sSQL = ""
    
    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
    
    
    Else
        
        sSQL = sSQL & "Insert into #tempsel_tb (                                " & vbNewLine
        sSQL = sSQL & "       marca                                             " & vbNewLine
        sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
        sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
        sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
        sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
        sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
        sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
        sSQL = sSQL & "     , numendosso                                        " & vbNewLine
        sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
        sSQL = sSQL & "     , produto_id                                        " & vbNewLine
        sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
        sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
        sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
        sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
        sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
        sSQL = sSQL & "     , situacao                                          " & vbNewLine
        sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
        sSQL = sSQL & "  , situacao_evento                                      " & vbNewLine
        sSQL = sSQL & "  , sit_evento_desc                                      " & vbNewLine
        sSQL = sSQL & "  , Sucursal_Nome                                        " & vbNewLine
        sSQL = sSQL & "  , Endosso                                              " & vbNewLine
        sSQL = sSQL & "  , sinistro_id_lider                                    " & vbNewLine
        sSQL = sSQL & "  , processa_reintegracao_is                             " & vbNewLine
        sSQL = sSQL & "  , Dt_Ocorrencia_Sinistro                               " & vbNewLine 'cristovao.rodrigues 08/11/2012
        sSQL = sSQL & "     )                                                   " & vbNewLine
        
        If iTPRamo = 1 Then ' VIDA
            sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10467_SPS " & iTPRamo & ", '" & sCnpj & "'"
             
        Else ' RE / RURAL
            sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10466_SPS " & iTPRamo & ", '" & sCnpj & "','" & sVariacao & "'" & vbNewLine
        
        End If
        
    End If
    
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
                      
                      
    If iTPRamo = 1 And _
            strTipoProcesso <> strTipoProcesso_Avisar And strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then
    
            sSQL = " "
            sSQL = sSQL & " select count(1) contador " & vbCrLf
            sSQL = sSQL & "From seguros_db.dbo.pessoa_juridica_tb pessoa_juridica_tb  WITH (NOLOCK) " & vbCrLf
            sSQL = sSQL & "inner join seguros_db.dbo.cliente_tb cliente_tb  WITH (NOLOCK) " & vbCrLf
            sSQL = sSQL & "    on pessoa_juridica_tb.pj_cliente_id = cliente_tb.cliente_id" & vbCrLf
            sSQL = sSQL & "inner join seguros_db.dbo.proposta_tb proposta_tb  WITH (NOLOCK) " & vbCrLf
            sSQL = sSQL & "    on cliente_tb.cliente_id = proposta_tb.prop_cliente_id" & vbCrLf
            sSQL = sSQL & "inner join seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb  WITH (NOLOCK) " & vbCrLf
            sSQL = sSQL & "    on proposta_adesao_tb.proposta_id = proposta_tb.proposta_id" & vbCrLf
            sSQL = sSQL & "inner join seguros_db.dbo.produto_tb produto_tb  WITH (NOLOCK) " & vbCrLf
            sSQL = sSQL & "    on proposta_tb.produto_id = produto_tb.produto_id" & vbCrLf
            sSQL = sSQL & "left join seguros_db.dbo.ramo_tb ramo_tb  WITH (NOLOCK) " & vbCrLf
            sSQL = sSQL & "    on proposta_adesao_tb.ramo_id = ramo_tb.ramo_id" & vbCrLf
            sSQL = sSQL & "inner join seguros_db.dbo.sinistro_tb sinistro_tb  WITH (NOLOCK) " & vbCrLf
            sSQL = sSQL & "    on sinistro_tb.proposta_id = proposta_adesao_tb.proposta_id" & vbCrLf
            sSQL = sSQL & "    and sinistro_tb.cliente_id = cliente_tb.cliente_id" & vbCrLf
            sSQL = sSQL & "where pessoa_juridica_tb.cgc = '" & sCnpj & "'" & vbCrLf
            sSQL = sSQL & "    and isnull(ramo_tb.tp_ramo_id,1) = " & iTPRamo & vbCrLf
            sSQL = sSQL & "    and sinistro_tb.apolice_id is null" & vbCrLf
                    
            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , True)
                      
            If rs!Contador = 0 Then
                sSQL = ""
                sSQL = sSQL & "Insert into #tempsel_tb (                                " & vbNewLine
                sSQL = sSQL & "       marca                                             " & vbNewLine
                sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
                sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
                sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
                sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
                sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
                sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
                sSQL = sSQL & "     , numendosso                                        " & vbNewLine
                sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
                sSQL = sSQL & "     , produto_id                                        " & vbNewLine
                sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
                sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
                sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
                sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
                sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
                sSQL = sSQL & "     , situacao                                          " & vbNewLine
                sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
                sSQL = sSQL & "  , situacao_evento                                      " & vbNewLine
                sSQL = sSQL & "  , sit_evento_desc                                      " & vbNewLine
                sSQL = sSQL & "  , Sucursal_Nome                                        " & vbNewLine
                sSQL = sSQL & "  , Endosso                                              " & vbNewLine
                sSQL = sSQL & "  , sinistro_id_lider                                    " & vbNewLine
                sSQL = sSQL & "  , processa_reintegracao_is                             " & vbNewLine
                sSQL = sSQL & "     )     " & vbNewLine
                sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10468_SPS " & iTPRamo & ", '" & sCnpj & "'"
                
                Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
            End If
            
    End If
    
    Exit Sub
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_CNPJ_Ramo", Me.name)
    Call FinalizarAplicacao
    
End Sub

Private Sub ConsultaSinistroPor_NumeroSinistroLider_Ramo(sSinistroLIder As String, iTPRamo As Byte, sVariacao As String)

    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
    sSQL = ""
    
    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
    
    
    Else
        
        sSQL = sSQL & "Insert into #tempsel_tb (                                " & vbNewLine
        sSQL = sSQL & "       marca                                             " & vbNewLine
        sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
        sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
        sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
        sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
        sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
        sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
        sSQL = sSQL & "     , numendosso                                        " & vbNewLine
        sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
        sSQL = sSQL & "     , produto_id                                        " & vbNewLine
        sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
        sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
        sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
        sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
        sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
        sSQL = sSQL & "     , situacao                                          " & vbNewLine
        sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
        sSQL = sSQL & "  , situacao_evento                                      " & vbNewLine
        sSQL = sSQL & "  , sit_evento_desc                                      " & vbNewLine
        sSQL = sSQL & "  , Sucursal_Nome                                        " & vbNewLine
        sSQL = sSQL & "  , Endosso                                              " & vbNewLine
        sSQL = sSQL & "  , sinistro_id_lider                                    " & vbNewLine
        sSQL = sSQL & "  , processa_reintegracao_is                             " & vbNewLine
        sSQL = sSQL & "  , tp_emissao                                           " & vbNewLine
        sSQL = sSQL & "     )                                                   " & vbNewLine
        sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10469_SPS " & iTPRamo & ", '" & sSinistroLIder & "','" & sVariacao & "'" & vbNewLine
    End If
    
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
    
    Exit Sub
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_NumeroSinistroLider_Ramo", Me.name)
    Call FinalizarAplicacao
    
End Sub


Private Sub SelecionaDados_SinistroPorCPF(iTPRamo As Integer)
    
    Dim sinistroNum As String
    Dim parametroCPF As String
    Dim seguroNum As Integer
    Dim sSQL As String
    Dim rs As ADODB.Recordset
    
    On Error GoTo TrataErro
    
    sinistroNum = ""
    
    sSQL = ""
    sSQL = sSQL & " select top 1 sinistro_id from #tempsel_tb"
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                sSQL, _
                                lConexaoLocal, _
                                True)

    If Not rs.EOF Then sinistroNum = rs("sinistro_id")
        
    If sinistroNum <> "" Then
        
        sSQL = ""
        sSQL = sSQL & " select top 1 sinistro_vida_tb.cpf " & vbCrLf
        sSQL = sSQL & " from sinistro_tb  WITH (NOLOCK)  " & vbCrLf
        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)  " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.cliente_id = cliente_tb.cliente_id " & vbCrLf
        sSQL = sSQL & " inner join sinistro_vida_tb  WITH (NOLOCK)  " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id " & vbCrLf
        sSQL = sSQL & " Where sinistro_vida_tb.sinistro_id = " & sinistroNum & vbCrLf
            
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    sSQL, _
                                    lConexaoLocal, _
                                    True)
    
        parametroCPF = rs("cpf")

        sSQL = ""
        sSQL = sSQL & " select count(1) numero "
        sSQL = sSQL & " from sinistro_vida_tb   WITH (NOLOCK)  "
        sSQL = sSQL & " Where CPF = '" & parametroCPF & "'"
        
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    sSQL, _
                                    lConexaoLocal, _
                                    True)
                                    
        seguroNum = rs("numero")
        
        If seguroNum <= 25 Then
        
            If Len(parametroCPF) <= 11 Then
               
                 sSQL = ""
                 sSQL = sSQL & " DELETE FROM #tempsel_tb"
                 
                 Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription, _
                          sSQL, _
                          lConexaoLocal, _
                          False)
                
                sSQL = ""
                
'                sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf 'Alan Neves - 19/02/2013
                'Alan Neves - 19/02/2013
                sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
                sSQL = sSQL & "       marca                                             " & vbCrLf
                sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
                sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
                sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
                sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
                sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
                sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
                sSQL = sSQL & "     , numendosso                                        " & vbCrLf
                sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
                sSQL = sSQL & "     , produto_id                                        " & vbCrLf
                sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
                sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
                sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
                sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
                sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
                sSQL = sSQL & "     , situacao                                          " & vbCrLf
                sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
                sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
                sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
                sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
                sSQL = sSQL & "     , Endosso                                              " & vbCrLf
                sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
                sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
                sSQL = sSQL & "     , historico                                            " & vbCrLf
                sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
                sSQL = sSQL & "     )                                                   " & vbCrLf
                            
                sSQL = sSQL & " exec seguros_db.dbo.SEGS10356_SPS '" & parametroCPF & "'," & iTPRamo
                
                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription, _
                          sSQL, _
                          lConexaoLocal, _
                          False)
       
            End If
            
        End If
        
    End If
    
    rs.Close
    Set rs = Nothing

    Exit Sub

TrataErro:
Call TrataErroGeral("SelecionaDados_SinistroPorCPF", Me.name)

End Sub

'cristovao.rodrigues 15/08/2012
Private Function ConsultaSinistroPor_Proposta(lProposta_ID As Long, bTP_Ramo As Byte, Optional bCosseguro As Boolean)
'Private Function ConsultaSinistroPor_Proposta(dSinistro_ID As Long, bTP_Ramo As Byte, Optional bCosseguro As Boolean)  'AKIO.OKUNO

Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_Proposta_RamoVida lProposta_ID, bTP_Ramo, bCosseguro
        Case bytTipoRamo_RE
            ConsultaSinistroPor_Proposta_RamoRural lProposta_ID, bTP_Ramo, "RE"
        Case bytTipoRamo_Rural
            ConsultaSinistroPor_Proposta_RamoRural lProposta_ID, 2, "RU"
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_Proposta", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Function ConsultaSinistroPor_PropostaBB(lProposta_BB As Long, bTP_Ramo As Byte)
'Private Function ConsultaSinistroPor_PropostaBB(dSinistro_ID As Long, bTP_Ramo As Byte) 'AKIO.OKUNO

    Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_PropostaBB_RamoVida lProposta_BB, bTP_Ramo
        Case bytTipoRamo_RE
            ConsultaSinistroPor_PropostaBB_RamoRural lProposta_BB, bTP_Ramo, "RE"
        Case bytTipoRamo_Rural
            ConsultaSinistroPor_PropostaBB_RamoRural lProposta_BB, 2, "RU"
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_PropostaBB", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Sub ConsultaSinistroPor_Proposta_RamoRural(lProposta_ID As Long, iTPRamo As Byte, sVariacao As String)
'Private Sub ConsultaSinistroPor_Proposta_RamoRural(dSinistro_ID As Long, iTPRamo As Byte, sVariacao As String)
    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
    
    'Alan Neves - 15/02/2013
    
'    sSQL = ""
'    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'        sSQL = sSQL & "Insert into #TempSel_Tb                                                                          " & vbNewLine
'        sSQL = sSQL & "     ( Proposta_ID )                                                                             " & vbNewLine
'        sSQL = sSQL & "Select Proposta_Tb.Proposta_ID                                                                   " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK)                         " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Ramo_Tb                        Ramo_Tb  WITH (NOLOCK)                             " & vbNewLine
'        sSQL = sSQL & "    on Ramo_Tb.Ramo_ID                               = Proposta_Tb.Ramo_ID                       " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Adesao_Tb        Proposta_Adesao_Tb  WITH (NOLOCK)                  " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Adesao_Tb.Proposta_ID                = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Fechada_Tb       Proposta_Fechada_Tb  WITH (NOLOCK)                 " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Fechada_Tb.Proposta_ID               = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Apolice_Tb                Apolice_Tb  WITH (NOLOCK)                          " & vbNewLine
'        sSQL = sSQL & "    on Apolice_Tb.Proposta_ID                        = Proposta_Fechada_Tb.Proposta_ID           " & vbNewLine
'''        'MATHAYDE
'''        sSQL = sSQL & " Where Ramo_Tb.Tp_Ramo_ID                            = " & iTPRamo & vbNewLine
'''        sSQL = sSQL & "   and Proposta_Tb.Proposta_ID                       = " & lProposta_ID & vbNewLine
'''        sSQL = sSQL & "   and Proposta_Tb.Produto_ID                        " & IIf(sVariacao = "RE", "not", "") & " in (126, 155, 156)                      " & vbNewLine
'''        sSQL = sSQL & "   and Proposta_Tb.Situacao                          <> 't'" & vbNewLine
'''        sSQL = sSQL & "   and " & IIf(sVariacao = "RE", "not", "") & " (Proposta_Tb.PRoduto_ID = 400 and Proposta_Tb.Ramo_ID = 63)                           " & vbNewLine
'        sSQL = sSQL & "   Where Proposta_Tb.Situacao                          <> 't'" & vbNewLine
'        sSQL = sSQL & "     and Proposta_Tb.Proposta_ID                       = " & lProposta_ID & vbNewLine
''AKIO.OKUNO - INICIO - 26/09/2012
''        sSQL = sSQL & "     and  ((Proposta_Tb.produto_id in (126,155,156,1152,1204) and  ramo_tb.tp_ramo_id =  " & iTPRamo & ")" & vbNewLine
''        sSQL = sSQL & "      or (Proposta_Tb.produto_id =  400 and ramo_tb.ramo_id = 63)" & vbNewLine
''        sSQL = sSQL & "      or  Proposta_Tb.produto_id =  14 )" & vbNewLine
'        sSQL = sSQL & "     and  ((Proposta_Tb.produto_id " & IIf(sVariacao = "RE", "not", "") & " in (126,155,156,1152,1204) and  ramo_tb.tp_ramo_id =  " & iTPRamo & ")" & vbNewLine
'        sSQL = sSQL & "      or " & IIf(sVariacao = "RE", "not", "") & " (Proposta_Tb.produto_id =  400 and ramo_tb.ramo_id = 63)" & vbNewLine
'        sSQL = sSQL & "      " & IIf(sVariacao = "RE", "", "or  Proposta_Tb.produto_id =  14 ") & ")" & vbNewLine
''AKIO.OKUNO - FIM - 26/09/2012
'
'        Conexao_ExecutarSQL gsSIGLASISTEMA _
'                          , glAmbiente_id _
'                          , App.Title _
'                          , App.FileDescription _
'                          , sSQL _
'                          , lConexaoLocal _
'                          , False
'
'        sSQL = ""
'        Select Case bytTipoRamo
'            Case bytTipoRamo_RE
'                sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10421_SPS    "
'            Case bytTipoRamo_Rural
'                sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10422_SPS    "
'        End Select
'
'    Else
'        sSQL = sSQL & "Insert into #tempsel_tb (                                " & vbNewLine
'        sSQL = sSQL & "       marca                                             " & vbNewLine
'        sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
'        sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
'        sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
'        sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
'        sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
'        sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
'        sSQL = sSQL & "     , numendosso                                        " & vbNewLine
'        sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
'        sSQL = sSQL & "     , produto_id                                        " & vbNewLine
'        sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
'        sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
'        sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
'        sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
'        sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
'        sSQL = sSQL & "     , situacao                                          " & vbNewLine
'        sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
'        sSQL = sSQL & "  , situacao_evento                                      " & vbNewLine
'        sSQL = sSQL & "  , sit_evento_desc                                      " & vbNewLine
'        sSQL = sSQL & "  , Sucursal_Nome                                        " & vbNewLine
'        sSQL = sSQL & "  , Endosso                                              " & vbNewLine
'        sSQL = sSQL & "  , sinistro_id_lider                                    " & vbNewLine
'        sSQL = sSQL & "  , processa_reintegracao_is                             " & vbNewLine
'        sSQL = sSQL & "  , Dt_Ocorrencia_Sinistro                               " & vbNewLine 'cristovao.rodrigues 08/11/2012
'        sSQL = sSQL & "     )                                                   " & vbNewLine
'        sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10366_SPS " & iTPRamo & ", '" & lProposta_ID & "','" & sVariacao & "'" & vbNewLine
'    End If
    
    
'    sSQL = ""
'    sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbNewLine
'    sSQL = sSQL & "       marca                                             " & vbNewLine
'    sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
'    sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
'    sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
'    sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
'    sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
'    sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
'    sSQL = sSQL & "     , numendosso                                        " & vbNewLine
'    sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
'    sSQL = sSQL & "     , produto_id                                        " & vbNewLine
'    sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
'    sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
'    sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
'    sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
'    sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
'    sSQL = sSQL & "     , situacao                                          " & vbNewLine
'    sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
'    sSQL = sSQL & "     , situacao_evento                                      " & vbNewLine
'    sSQL = sSQL & "     , sit_evento_desc                                      " & vbNewLine
'    sSQL = sSQL & "     , Sucursal_Nome                                        " & vbNewLine
'    sSQL = sSQL & "     , Endosso                                              " & vbNewLine
'    sSQL = sSQL & "     , sinistro_id_lider                                    " & vbNewLine
'    sSQL = sSQL & "     , processa_reintegracao_is                             " & vbNewLine
'    sSQL = sSQL & "     , historico                                            " & vbNewLine
'    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbNewLine 'cristovao.rodrigues 08/11/2012
'    sSQL = sSQL & "     , Tp_Emissao                                           " & vbNewLine 'cristovao.rodrigues 08/11/2012
'    sSQL = sSQL & "     )                                                   " & vbNewLine

    sSQL = ""
    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10899_SPS '" & lProposta_ID & "','" & iTPRamo & "'" & ", '" & sVariacao & "','" & sOperacaoCosseguro & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "','" & bytTipoRamo & "'" & vbNewLine
    
    
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
    
    Exit Sub
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_Proposta_RamoRural", Me.name)
    Call FinalizarAplicacao
    
End Sub

'cristovao.rodrigues 15/08/2012
Private Sub ConsultaSinistroPor_PropostaBB_RamoRural(lProposta_BB As Long, iTPRamo As Byte, sVariacao As String)
'Private Sub ConsultaSinistroPor_PropostaBB_RamoRural(dSinistro_ID As Long, iTPRamo As Byte, sVariacao As String)   'AKIO.OKUNO


    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
    'Alan Neves - 21/02/2013
'    sSQL = ""
'    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'        sSQL = sSQL & "Insert into #TempSel_Tb                                                                          " & vbNewLine
'        sSQL = sSQL & "     ( Proposta_ID )                                                                             " & vbNewLine
'        sSQL = sSQL & "Select Proposta_Tb.Proposta_ID                                                                   " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK)                         " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Ramo_Tb                        Ramo_Tb  WITH (NOLOCK)                             " & vbNewLine
'        sSQL = sSQL & "    on Ramo_Tb.Ramo_ID                               = Proposta_Tb.Ramo_ID                       " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Adesao_Tb        Proposta_Adesao_Tb  WITH (NOLOCK)                  " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Adesao_Tb.Proposta_ID                = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Fechada_Tb       Proposta_Fechada_Tb  WITH (NOLOCK)                 " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Fechada_Tb.Proposta_ID               = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Apolice_Tb                Apolice_Tb  WITH (NOLOCK)                          " & vbNewLine
'        sSQL = sSQL & "    on Apolice_Tb.Proposta_ID                        = Proposta_Fechada_Tb.Proposta_ID           " & vbNewLine
'''        'MATHAYDE
'''        sSQL = sSQL & " Where Ramo_Tb.Tp_Ramo_ID                            = " & iTPRamo & vbNewLine
'''        sSQL = sSQL & "   and (Proposta_Fechada_Tb.Proposta_BB              = " & lProposta_BB & vbNewLine
'''        sSQL = sSQL & "        or Proposta_Adesao_Tb.Proposta_BB           = " & lProposta_BB & ")                     " & vbNewLine
'''        sSQL = sSQL & "   and Proposta_Tb.Produto_ID                        " & IIf(sVariacao = "RE", "not", "") & " in (126, 155, 156)                      " & vbNewLine
'''        sSQL = sSQL & "   and Proposta_Tb.Situacao                          <> 't'" & vbNewLine
'''        sSQL = sSQL & "   and " & IIf(sVariacao = "RE", "not", "") & " (Proposta_Tb.PRoduto_ID = 400 and Proposta_Tb.Ramo_ID = 63)                           " & vbNewLine
'        sSQL = sSQL & "   Where Proposta_Tb.Situacao                          <> 't'" & vbNewLine
'        sSQL = sSQL & "   and (Proposta_Fechada_Tb.Proposta_BB              = " & lProposta_BB & vbNewLine
'        sSQL = sSQL & "        or Proposta_Adesao_Tb.Proposta_BB           = " & lProposta_BB & ")"
''AKIO.OKUNO - INICIO - 26/09/2012
''        sSQL = sSQL & "     and  ((Proposta_Tb.produto_id in (126,155,156,1152,1204) and  ramo_tb.tp_ramo_id =  " & iTPRamo & ")" & vbNewLine
''        sSQL = sSQL & "      or (Proposta_Tb.produto_id =  400 and ramo_tb.ramo_id = 63)" & vbNewLine
''        sSQL = sSQL & "      or  Proposta_Tb.produto_id =  14 )" & vbNewLine
'        sSQL = sSQL & "     and  ((Proposta_Tb.produto_id " & IIf(sVariacao = "RE", "not", "") & " in (126,155,156,1152,1204) and  ramo_tb.tp_ramo_id =  " & iTPRamo & ")" & vbNewLine
'        sSQL = sSQL & "      or " & IIf(sVariacao = "RE", "not", "") & " (Proposta_Tb.produto_id =  400 and ramo_tb.ramo_id = 63)" & vbNewLine
'        sSQL = sSQL & "      " & IIf(sVariacao = "RE", "", "or  Proposta_Tb.produto_id =  14 ") & ")" & vbNewLine
''AKIO.OKUNO - FIM - 26/09/2012
'
'
'        Conexao_ExecutarSQL gsSIGLASISTEMA _
'                          , glAmbiente_id _
'                          , App.Title _
'                          , App.FileDescription _
'                          , sSQL _
'                          , lConexaoLocal _
'                          , False
'
'        sSQL = ""
'        Select Case bytTipoRamo
'            Case bytTipoRamo_RE
'                sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10421_SPS    "
'            Case bytTipoRamo_Rural
'                sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10422_SPS    "
'        End Select
'    Else
'        sSQL = sSQL & "Insert into #tempsel_tb (                                " & vbNewLine
'        sSQL = sSQL & "       marca                                             " & vbNewLine
'        sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
'        sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
'        sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
'        sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
'        sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
'        sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
'        sSQL = sSQL & "     , numendosso                                        " & vbNewLine
'        sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
'        sSQL = sSQL & "     , produto_id                                        " & vbNewLine
'        sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
'        sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
'        sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
'        sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
'        sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
'        sSQL = sSQL & "     , situacao                                          " & vbNewLine
'        sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
'        sSQL = sSQL & "  , situacao_evento                                      " & vbNewLine
'        sSQL = sSQL & "  , sit_evento_desc                                      " & vbNewLine
'        sSQL = sSQL & "  , Sucursal_Nome                                        " & vbNewLine
'        sSQL = sSQL & "  , Endosso                                              " & vbNewLine
'        sSQL = sSQL & "  , sinistro_id_lider                                    " & vbNewLine
'        sSQL = sSQL & "  , processa_reintegracao_is                             " & vbNewLine
'        sSQL = sSQL & "  , Dt_Ocorrencia_Sinistro                               " & vbNewLine 'cristovao.rodrigues 08/11/2012
'        sSQL = sSQL & "     )                                                   " & vbNewLine
'        sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10367_SPS " & iTPRamo & ", '" & lProposta_BB & "','" & sVariacao & "'" & vbNewLine
'    End If

    sSQL = ""
    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10915_SPS " & lProposta_BB & "," & iTPRamo & ", '" & sVariacao & "','" & sOperacaoCosseguro & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'," & bytTipoRamo & vbNewLine
        
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
    
    Exit Sub
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_PropostaBB_RamoRural", Me.name)
    Call FinalizarAplicacao
    
End Sub


'cristovao.rodrigues 15/08/2012
Private Function ConsultaSinistroPor_SinistroBB(dSinistro_ID As String, bTP_Ramo As Byte)

    Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_SinistroBB_RamoVIDA dSinistro_ID, bTP_Ramo
        Case bytTipoRamo_RE
            ConsultaSinistroPor_SinistroBB_RamoRural dSinistro_ID, bTP_Ramo, "RE"
        Case bytTipoRamo_Rural
            ConsultaSinistroPor_SinistroBB_RamoRural dSinistro_ID, 2, "RU"
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_SinistroBB", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Sub ConsultaSinistroPor_SinistroBB_RamoRural(iSinistroBB As String, iTPRamo As Byte, sVariacao As String)
    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
' Alan Neves - 21/02/2013
    sSQL = ""
'    sSQL = sSQL & "Insert into #tempsel_tb (                                " & vbNewLine
'    sSQL = sSQL & "       marca                                             " & vbNewLine
'    sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
'    sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
'    sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
'    sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
'    sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
'    sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
'    sSQL = sSQL & "     , numendosso                                        " & vbNewLine
'    sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
'    sSQL = sSQL & "     , produto_id                                        " & vbNewLine
'    sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
'    sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
'    sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
'    sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
'    sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
'    sSQL = sSQL & "     , situacao                                          " & vbNewLine
'    sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
'    sSQL = sSQL & "  , situacao_evento                                      " & vbNewLine
'    sSQL = sSQL & "  , sit_evento_desc                                      " & vbNewLine
'    sSQL = sSQL & "  , Sucursal_Nome                                        " & vbNewLine
'    sSQL = sSQL & "  , Endosso                                              " & vbNewLine
'    sSQL = sSQL & "  , sinistro_id_lider                                    " & vbNewLine
'    sSQL = sSQL & "  , processa_reintegracao_is                             " & vbNewLine
'    sSQL = sSQL & "  , Dt_Ocorrencia_Sinistro                               " & vbNewLine 'cristovao.rodrigues 08/11/2012
'    sSQL = sSQL & "     )                                                   " & vbNewLine
'    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10364_SPS " & iTPRamo & ", '" & iSinistroBB & "','" & sVariacao & "'" & vbNewLine
'    Conexao_ExecutarSQL gsSIGLASISTEMA _
'                      , glAmbiente_id _
'                      , App.Title _
'                      , App.FileDescription _
'                      , sSQL _
'                      , lConexaoLocal _
'                      , False
'    sSQL = ""
'    sSQL = sSQL & "if (Select count(1) From #tempsel_tb)= 0 " & vbNewLine
'    sSQL = sSQL & "Begin " & vbNewLine
'    sSQL = sSQL & "    Insert into #tempsel_tb (                " & vbNewLine
'    sSQL = sSQL & "           marca                             " & vbNewLine
'    sSQL = sSQL & "         , apolice_id                        " & vbNewLine
'    sSQL = sSQL & "         , sucursal_seguradora_id            " & vbNewLine
'    sSQL = sSQL & "         , seguradora_cod_susep              " & vbNewLine
'    sSQL = sSQL & "         , ramo_id                           " & vbNewLine
'    sSQL = sSQL & "         , nomeramo                          " & vbNewLine
'    sSQL = sSQL & "         , proposta_id                       " & vbNewLine
'    sSQL = sSQL & "         , numendosso                        " & vbNewLine
'    sSQL = sSQL & "         , sitproposta                       " & vbNewLine
'    sSQL = sSQL & "         , produto_id                        " & vbNewLine
'    sSQL = sSQL & "         , nomeproduto                       " & vbNewLine
'    sSQL = sSQL & "         , cliente_id                        " & vbNewLine
'    sSQL = sSQL & "         , nomecliente                       " & vbNewLine
'    sSQL = sSQL & "         , tipocomponente                    " & vbNewLine
'    sSQL = sSQL & "         , sinistro_id                       " & vbNewLine
'    sSQL = sSQL & "         , situacao                          " & vbNewLine
'    sSQL = sSQL & "         , situacao_desc                     " & vbNewLine
'    sSQL = sSQL & "         , situacao_evento                   " & vbNewLine
'    sSQL = sSQL & "         , sit_evento_desc                   " & vbNewLine
'    sSQL = sSQL & "         , sucursal_nome                     " & vbNewLine
'    sSQL = sSQL & "         , endosso                           " & vbNewLine
'    sSQL = sSQL & "         , sinistro_id_lider                 " & vbNewLine
'    sSQL = sSQL & "         , processa_reintegracao_is          " & vbNewLine
'    sSQL = sSQL & "         , Dt_Ocorrencia_Sinistro            " & vbNewLine 'cristovao.rodrigues 08/11/2012
'    sSQL = sSQL & "         )                                   " & vbNewLine
'    sSQL = sSQL & "          Exec seguros_db.dbo.SEGS10365_SPS " & iTPRamo & ", '" & iSinistroBB & "','" & sVariacao & "'" & vbNewLine
'    sSQL = sSQL & "End" & vbNewLine
    
    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10917_SPS '" & iSinistroBB & "','" & iTPRamo & "'" & ", '" & sVariacao & "','" & sOperacaoCosseguro & "'" & vbNewLine
    
    
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
    Exit Sub
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_SinistroBB_RamoRural", Me.name)
    Call FinalizarAplicacao
    
End Sub


Private Function ConsultaSinistroPor_SinistroBB_RamoVIDA(iSinistroBB As String, iTPRamo As Byte)

    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim sCPF As String
    
    On Error GoTo TrataErro
    
    sSQL = ""
    'Alan Neves - 21/02/2013
'    sSQL = sSQL & " select                                                          " & vbCrLf
'    sSQL = sSQL & "     sinistro_vida_tb.cpf,                                       " & vbCrLf
'    sSQL = sSQL & "     proposta_tb.proposta_id                                     " & vbCrLf
'    sSQL = sSQL & "     --sinistro_tb.cliente_id,                                   " & vbCrLf
'    sSQL = sSQL & "     --sinistro_tb.sinistro_id,                                  " & vbCrLf
'    sSQL = sSQL & "     --sinistro_tb.apolice_id                                    " & vbCrLf
'    sSQL = sSQL & " from sinistro_tb  WITH (NOLOCK)                                    " & vbCrLf
'    sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                               " & vbCrLf
'    sSQL = sSQL & "     on sinistro_tb.cliente_id = cliente_tb.cliente_id           " & vbCrLf
'    sSQL = sSQL & " inner join sinistro_vida_tb  WITH (NOLOCK)                         " & vbCrLf
'    sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id   " & vbCrLf
'    sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                  " & vbCrLf
'    sSQL = sSQL & "     on proposta_tb.proposta_id = sinistro_tb.proposta_id        " & vbCrLf
'    sSQL = sSQL & " inner join sinistro_bb_tb                                       " & vbCrLf
'    sSQL = sSQL & "     on sinistro_bb_tb.sinistro_id = sinistro_tb.sinistro_id     " & vbCrLf
'    sSQL = sSQL & " Where                                                           " & vbCrLf
'    sSQL = sSQL & "     sinistro_bb_tb.sinistro_bb  = " & iSinistroBB & vbCrLf
'    sSQL = sSQL & "     and sinistro_tb.situacao <> 7                               "
'
'    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                         glAmbiente_id, _
'                         App.Title, _
'                         App.FileDescription, _
'                         sSQL, _
'                         lConexaoLocal, _
'                         True)
'
'    If Not rsRecordSet.EOF Then
'
'        sCPF = IIf(IsNull(rsRecordSet("CPF")), "", rsRecordSet("cpf"))
'
'        sSQL = ""
'        sSQL = sSQL & " select                                                          " & vbCrLf
'        sSQL = sSQL & "     sinistro_vida_tb.cpf,                                       " & vbCrLf
'        sSQL = sSQL & "     proposta_tb.proposta_id                                    " & vbCrLf
'        sSQL = sSQL & "     --sinistro_tb.cliente_id ,                                    " & vbCrLf
'        sSQL = sSQL & "     --sinistro_tb.sinistro_id,                                    " & vbCrLf
'        sSQL = sSQL & "     --sinistro_tb.apolice_id                                      " & vbCrLf
'        sSQL = sSQL & " from sinistro_tb  WITH (NOLOCK)                                    " & vbCrLf
'        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                               " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.cliente_id = cliente_tb.cliente_id           " & vbCrLf
'        sSQL = sSQL & " inner join sinistro_vida_tb  WITH (NOLOCK)                         " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id   " & vbCrLf
'        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                  " & vbCrLf
'        sSQL = sSQL & "     on proposta_tb.proposta_id = sinistro_tb.proposta_id        " & vbCrLf
'        sSQL = sSQL & " inner join sinistro_bb_tb                                       " & vbCrLf
'        sSQL = sSQL & "     on sinistro_bb_tb.sinistro_id = sinistro_tb.sinistro_id     " & vbCrLf
'        sSQL = sSQL & " Where                                                           " & vbCrLf
'        sSQL = sSQL & "     sinistro_bb_tb.sinistro_bb  = " & iSinistroBB & vbCrLf
'        sSQL = sSQL & "     and sinistro_tb.situacao <> 7                               " & vbCrLf
'        sSQL = sSQL & "   and proposta_tb.produto_id in (115,123, 222 ,223 ,1140) "
'
'        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             True)
'
'
'        If rsRecordSet.RecordCount > 0 Or sCPF = "" Then
'            sSQL = ""
'
''            sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf  'Alan Neves - 19/02/2013
'            'Alan Neves - 19/02/2013
'            sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
'            sSQL = sSQL & "       marca                                             " & vbCrLf
'            sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
'            sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
'            sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
'            sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
'            sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
'            sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
'            sSQL = sSQL & "     , numendosso                                        " & vbCrLf
'            sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
'            sSQL = sSQL & "     , produto_id                                        " & vbCrLf
'            sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
'            sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
'            sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
'            sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
'            sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
'            sSQL = sSQL & "     , situacao                                          " & vbCrLf
'            sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
'            sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
'            sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
'            sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
'            sSQL = sSQL & "     , Endosso                                              " & vbCrLf
'            sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
'            sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
'            sSQL = sSQL & "     , historico                                            " & vbCrLf
'            sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
'            sSQL = sSQL & "     )                                                   " & vbCrLf
'
'            sSQL = sSQL & " exec seguros_db.dbo.SEGS10353_SPS " & iSinistroBB & "," & iTPRamo
'
'            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                         glAmbiente_id, _
'                         App.Title, _
'                         App.FileDescription, _
'                         sSQL, _
'                         lConexaoLocal, _
'                         False)
'
'        Else
'            Retorno = ConsultaSinistroPor_CPF_RamoVida(sCPF, iTPRamo)
'        End If
'
'    End If
'
'    rsRecordSet.Close
'    Set rsRecordSet = Nothing
        
    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10918_SPS '" & iSinistroBB & "','" & iTPRamo & "'" & ", '" & sOperacaoCosseguro & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'" & vbNewLine
    
            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         lConexaoLocal, _
                         False)
                         
    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_SinistroBB", Me.name)
    FinalizarAplicacao

End Function

Private Function ConsultaSinistroPor_AvisosPendentesdeAvaliacao(iTecnicoID As Long, bTP_Ramo As Byte)

Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_AvisosPendentesdeAvaliacao_RamoVida iTecnicoID, bTP_Ramo
        Case bytTipoRamo_RE
            ConsultaSinistroPor_AvisosPendentesdeAvaliacao_RamoRural iTecnicoID, bTP_Ramo, "RE"
        Case bytTipoRamo_Rural
            ConsultaSinistroPor_AvisosPendentesdeAvaliacao_RamoRural iTecnicoID, 2, "RU"
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_AvisosPendentesdeAvaliacao", Me.name)

End Function

'cristovao.rodrigues 13/06/2012
Private Function ConsultaSinistroPor_AvisosPendentesdeAvaliacao_RamoVida(iTecnicoID As Long, iTPRamo As Byte)
    Dim sSQL As String
    
    On Error GoTo TrataErro
    
    sSQL = ""
' Alan Neves - 19/02/2013
'   sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf
    
    'Alan Neves - 19/02/2013
    sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
    sSQL = sSQL & "       marca                                             " & vbCrLf
    sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
    sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
    sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
    sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
    sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
    sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
    sSQL = sSQL & "     , numendosso                                        " & vbCrLf
    sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
    sSQL = sSQL & "     , produto_id                                        " & vbCrLf
    sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
    sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
    sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
    sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
    sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
    sSQL = sSQL & "     , situacao                                          " & vbCrLf
    sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
    sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
    sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
    sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
    sSQL = sSQL & "     , Endosso                                              " & vbCrLf
    sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
    sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
    sSQL = sSQL & "     , historico                                            " & vbCrLf
    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
    sSQL = sSQL & "     )                                                   " & vbCrLf
    
    sSQL = sSQL & " exec seguros_db.dbo.SEGS10359_SPS " & iTecnicoID & "," & iTPRamo
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         lConexaoLocal, _
                         False)
    
       
    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_AvisosPendentesAvaliacao_RamoVida", Me.name)

End Function

'cristovao.rodrigues 16/08/2012
Private Function ConsultaSinistroPor_AvisosPendentesdeAvaliacao_RamoRural(iTecnicoID As Long, iTPRamo As Byte, sVariacao As String)
    Dim sSQL As String
    
    On Error GoTo TrataErro
    
    sSQL = ""
    ' Alan Neves - 19/02/2013
'    sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf

    'Alan Neves - 19/02/2013
    sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
    sSQL = sSQL & "       marca                                             " & vbCrLf
    sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
    sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
    sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
    sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
    sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
    sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
    sSQL = sSQL & "     , numendosso                                        " & vbCrLf
    sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
    sSQL = sSQL & "     , produto_id                                        " & vbCrLf
    sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
    sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
    sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
    sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
    sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
    sSQL = sSQL & "     , situacao                                          " & vbCrLf
    sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
    sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
    sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
    sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
    sSQL = sSQL & "     , Endosso                                              " & vbCrLf
    sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
    sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
    sSQL = sSQL & "     , historico                                            " & vbCrLf
    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
    sSQL = sSQL & "     )                                                   " & vbCrLf

    sSQL = sSQL & " exec seguros_db.dbo.SEGS10371_SPS " & iTecnicoID & "," & iTPRamo & ",'" & sVariacao & "'"
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         lConexaoLocal, _
                         False)
    
       
    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_AvisosPendentesAvaliacao_RamoRural", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Sub ConsultaSinistroPor_AvisosPendentesAvaliacao_RamoRural(iTecnicoID As Long, iTPRamo As Byte)
    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
    sSQL = ""
    sSQL = sSQL & "Insert into #tempsel_tb (                                " & vbNewLine
    sSQL = sSQL & "       marca                                             " & vbNewLine
    sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
    sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
    sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
    sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
    sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
    sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
    sSQL = sSQL & "     , numendosso                                        " & vbNewLine
    sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
    sSQL = sSQL & "     , produto_id                                        " & vbNewLine
    sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
    sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
    sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
    sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
    sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
    sSQL = sSQL & "     , situacao                                          " & vbNewLine
    sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
    sSQL = sSQL & "  , situacao_evento                                      " & vbNewLine
    sSQL = sSQL & "  , sit_evento_desc                                      " & vbNewLine
    sSQL = sSQL & "  , Sucursal_Nome                                        " & vbNewLine
    sSQL = sSQL & "  , Endosso                                              " & vbNewLine
    sSQL = sSQL & "  , sinistro_id_lider                                    " & vbNewLine
    sSQL = sSQL & "  , processa_reintegracao_is                             " & vbNewLine
    sSQL = sSQL & "     )                                                   " & vbNewLine
    
    '''' cristovao 15/08/2012
    '''' verificar usar ConsultaSinistroPor_AvisosPendentesdeAvaliacao_sps
    '''' fazer correcao sinistro_selecao_tipo_1_tramo_1_sps nao usa tecnico para pesquisa
    
'    sSQL = sSQL & "  Exec sinistro_selecao_tipo_1_tramo_1_sps iTPRamo, '" & iTecnicoID & "'" & vbNewLine
'    Conexao_ExecutarSQL gsSIGLASISTEMA _
'                      , glAmbiente_id _
'                      , App.Title _
'                      , App.FileDescription _
'                      , sSQL _
'                      , lConexaoLocal _
'                      , False
    
    Exit Sub
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_AvisosPendentesAvaliacao_RamoRural", Me.name)
    Call FinalizarAplicacao
    
End Sub


Private Function ConsultaSinistroPor_Sinistro(dSinistro_ID As Double, bTP_Ramo As Byte)

    Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_Sinistro_RamoVIDA dSinistro_ID
        Case bytTipoRamo_RE
            ConsultaSinistroPor_Sinistro_RamoRural dSinistro_ID, bTP_Ramo, "RE"
        Case bytTipoRamo_Rural
            ConsultaSinistroPor_Sinistro_RamoRural dSinistro_ID, 2, "RU"
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_Sinistro", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Sub ConsultaSinistroPor_Sinistro_RamoRural(dSinistro_ID As Double, iTPRamo As Byte, sVariacao As String)
    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
    sSQL = ""
    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10362_SPS " & iTPRamo & ", '" & dSinistro_ID & "','" & sVariacao & "'" & ", '" & sOperacaoCosseguro & "'" & vbNewLine
      
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
                      
    ' Alan Neves - 15/02/2013
'    sSQL = ""
'    sSQL = sSQL & "Insert into #tempsel_tb (                                " & vbNewLine
'    sSQL = sSQL & "       marca                                             " & vbNewLine
'    sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
'    sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
'    sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
'    sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
'    sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
'    sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
'    sSQL = sSQL & "     , numendosso                                        " & vbNewLine
'    sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
'    sSQL = sSQL & "     , produto_id                                        " & vbNewLine
'    sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
'    sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
'    sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
'    sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
'    sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
'    sSQL = sSQL & "     , situacao                                          " & vbNewLine
'    sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
'    sSQL = sSQL & "  , situacao_evento                                      " & vbNewLine
'    sSQL = sSQL & "  , sit_evento_desc                                      " & vbNewLine
'    sSQL = sSQL & "  , Sucursal_Nome                                        " & vbNewLine
'    sSQL = sSQL & "  , Endosso                                              " & vbNewLine
'    sSQL = sSQL & "  , sinistro_id_lider                                    " & vbNewLine
'    sSQL = sSQL & "  , processa_reintegracao_is                             " & vbNewLine
'    sSQL = sSQL & "  , Dt_Ocorrencia_Sinistro                               " & vbNewLine 'cristovao.rodrigues 08/11/2012
'    sSQL = sSQL & "     )                                                   " & vbNewLine
'    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10362_SPS " & iTPRamo & ", '" & dSinistro_ID & "','" & sVariacao & "'" & vbNewLine
'
'    Conexao_ExecutarSQL gsSIGLASISTEMA _
'                      , glAmbiente_id _
'                      , App.Title _
'                      , App.FileDescription _
'                      , sSQL _
'                      , lConexaoLocal _
'                      , False
'
'    sSQL = ""
'    sSQL = sSQL & "if (Select count(1) From #tempsel_tb)= 0 " & vbNewLine
'    sSQL = sSQL & "Begin " & vbNewLine
'    sSQL = sSQL & "    Insert into #tempsel_tb (                " & vbNewLine
'    sSQL = sSQL & "           marca                             " & vbNewLine
'    sSQL = sSQL & "         , apolice_id                        " & vbNewLine
'    sSQL = sSQL & "         , sucursal_seguradora_id            " & vbNewLine
'    sSQL = sSQL & "         , seguradora_cod_susep              " & vbNewLine
'    sSQL = sSQL & "         , ramo_id                           " & vbNewLine
'    sSQL = sSQL & "         , nomeramo                          " & vbNewLine
'    sSQL = sSQL & "         , proposta_id                       " & vbNewLine
'    sSQL = sSQL & "         , numendosso                        " & vbNewLine
'    sSQL = sSQL & "         , sitproposta                       " & vbNewLine
'    sSQL = sSQL & "         , produto_id                        " & vbNewLine
'    sSQL = sSQL & "         , nomeproduto                       " & vbNewLine
'    sSQL = sSQL & "         , cliente_id                        " & vbNewLine
'    sSQL = sSQL & "         , nomecliente                       " & vbNewLine
'    sSQL = sSQL & "         , tipocomponente                    " & vbNewLine
'    sSQL = sSQL & "         , sinistro_id                       " & vbNewLine
'    sSQL = sSQL & "         , situacao                          " & vbNewLine
'    sSQL = sSQL & "         , situacao_desc                     " & vbNewLine
'    sSQL = sSQL & "         , situacao_evento                   " & vbNewLine
'    sSQL = sSQL & "         , sit_evento_desc                   " & vbNewLine
'    sSQL = sSQL & "         , sucursal_nome                     " & vbNewLine
'    sSQL = sSQL & "         , endosso                           " & vbNewLine
'    sSQL = sSQL & "         , sinistro_id_lider                 " & vbNewLine
'    sSQL = sSQL & "         , processa_reintegracao_is          " & vbNewLine
'    sSQL = sSQL & "         , Dt_Ocorrencia_Sinistro                               " & vbNewLine 'cristovao.rodrigues 08/11/2012
'    sSQL = sSQL & "         )                                   " & vbNewLine
'    sSQL = sSQL & "          Exec seguros_db.dbo.SEGS10363_SPS " & iTPRamo & ", '" & dSinistro_ID & "','" & sVariacao & "'" & vbNewLine
'    sSQL = sSQL & "End" & vbNewLine
'
'    Conexao_ExecutarSQL gsSIGLASISTEMA _
'                      , glAmbiente_id _
'                      , App.Title _
'                      , App.FileDescription _
'                      , sSQL _
'                      , lConexaoLocal _
'                      , False
'



    Exit Sub
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_Sinistro_RamoRural", Me.name)
    Call FinalizarAplicacao
    
End Sub

Private Sub ConsultaSinistroPor_Sinistro_RamoVIDA(dSinistro_ID As Double)
    Dim rsRecordSet                             As ADODB.Recordset
    Dim sSQL                                    As String
    Dim sCPF                                    As String
    
    On Error GoTo Erro
    
'    sSQL = ""
'    sSQL = sSQL & " select sinistro_vida_tb.cpf                                     " & vbCrLf
'    sSQL = sSQL & "      , Sinistro_Tb.Situacao                                     " & vbCrLf
'    sSQL = sSQL & " from sinistro_tb  WITH (NOLOCK)                                    " & vbCrLf
'    sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                               " & vbCrLf
'    sSQL = sSQL & "     on sinistro_tb.cliente_id = cliente_tb.cliente_id           " & vbCrLf
'    sSQL = sSQL & " inner join sinistro_vida_tb  WITH (NOLOCK)                         " & vbCrLf
'    sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id   " & vbCrLf
'    sSQL = sSQL & " Where sinistro_vida_tb.sinistro_id = " & dSinistro_ID
'
'    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
'                                        , glAmbiente_id _
'                                        , App.Title _
'                                        , App.FileDescription _
'                                        , sSQL _
'                                        , lConexaoLocal _
'                                        , True)
'
'    If Not rsRecordSet.EOF Then
'
'        sCPF = IIf(IsNull(rsRecordSet("CPF")), "", rsRecordSet("cpf"))
'        sSinistro_Situacao = rsRecordSet.Fields!Situacao
'
'        sSQL = ""
'        sSQL = sSQL & " select sinistro_vida_tb.cpf                                     " & vbCrLf
'        sSQL = sSQL & " from sinistro_tb  WITH (NOLOCK)                                    " & vbCrLf
'        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                               " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.cliente_id = cliente_tb.cliente_id           " & vbCrLf
'        sSQL = sSQL & " inner join sinistro_vida_tb  WITH (NOLOCK)                         " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id   " & vbCrLf
'        sSQL = sSQL & "inner join proposta_tb  WITH (NOLOCK)                                   " & vbCrLf
'        sSQL = sSQL & "     on proposta_tb.proposta_id = sinistro_tb.proposta_id        " & vbCrLf
'        sSQL = sSQL & " Where sinistro_vida_tb.sinistro_id = " & dSinistro_ID & vbCrLf
'        sSQL = sSQL & "   and proposta_tb.produto_id in (115,123, 222 ,223 ,1140) "
'
'        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
'                                            , glAmbiente_id _
'                                            , App.Title _
'                                            , App.FileDescription _
'                                            , sSQL _
'                                            , lConexaoLocal _
'                                            , True)
'
'        If rsRecordSet.RecordCount > 1 Or sCPF = "" Then 'AKIO.OKUNO  24/09/2012
'
'            sSQL = ""
'            sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf
'            sSQL = sSQL & " exec seguros_db.dbo.SEGS10350_SPS '" & dSinistro_ID & "'," & 1
'
'            Conexao_ExecutarSQL gsSIGLASISTEMA _
'                              , glAmbiente_id _
'                              , App.Title _
'                              , App.FileDescription _
'                              , sSQL _
'                              , lConexaoLocal _
'                              , False
'
'        Else
'            ConsultaSinistroPor_CPF_RamoVida sCPF, 1
'        End If
'
'    End If
'
   
' Alan Neves - 08/02/2013
    
'    sSQL = ""
'    sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf
'    sSQL = sSQL & "SELECT DISTINCT                                                                                                          " & vbNewLine
'
'' Alan Neves - ja estava comentado este trecho
''    sSQL = sSQL & " sinistro_tb.lock_aviso, sinistro_tb.apolice_id, sinistro_tb.sucursal_seguradora_id,                                     " & vbNewLine
''    sSQL = sSQL & "    sinistro_tb.seguradora_cod_susep, ramo_tb.ramo_id, ramo_tb.nome, sinistro_tb.proposta_id,                            " & vbNewLine
''    sSQL = sSQL & "        proposta_tb.num_endosso, proposta_tb.situacao, produto_tb.produto_id, produto_tb.nome, cliente_tb.cliente_id,    " & vbNewLine
''    sSQL = sSQL & " cliente_tb.nome,TipoComponente = 'C', sinistro_tb.sinistro_id,sinistro_tb.situacao,Situacao_Desc = '', situacao_evento = '', sit_evento_desc = '',Sucursal_Nome = null, sinistro_tb.endosso_id,              " & vbNewLine
''    sSQL = sSQL & " sinistro_tb.sinistro_id_lider, sinistro_tb.processa_reintegracao_is                                                     " & vbNewLine
''    sSQL = sSQL & " '', Sinistro_Tb.Dt_Ocorrencia_Sinistro                                                                                  " & vbNewLine
'' Alan Neves - ja estava comentado este trecho
'
'
'    sSQL = sSQL & "    sinistro_tb.lock_aviso,                                          " & vbCrLf
'    sSQL = sSQL & "    sinistro_tb.apolice_id,                                          " & vbCrLf
'    sSQL = sSQL & "    sinistro_tb.sucursal_seguradora_id,                              " & vbCrLf
'    sSQL = sSQL & "    sinistro_tb.seguradora_cod_susep,                                " & vbCrLf
'    sSQL = sSQL & "    isnull(ramo_tb.ramo_id,0) ramo_id,                               " & vbCrLf
'    sSQL = sSQL & "    isnull(ramo_tb.nome,'') nome,                                    " & vbCrLf
'    sSQL = sSQL & "    proposta_tb.proposta_id,                                         " & vbCrLf
'    sSQL = sSQL & "    proposta_tb.num_endosso,                                         " & vbCrLf
'    sSQL = sSQL & "    proposta_tb.situacao,                                            " & vbCrLf
'    sSQL = sSQL & "    produto_tb.produto_id,                                           " & vbCrLf
'    sSQL = sSQL & "    produto_tb.nome,                                                 " & vbCrLf
'    sSQL = sSQL & "    cliente_tb.cliente_id,                                           " & vbCrLf
'    sSQL = sSQL & "    cliente_tb.nome,                                                 " & vbCrLf
'    sSQL = sSQL & "    case when proposta_complementar_tb.prop_cliente_id is null       " & vbCrLf
'    sSQL = sSQL & "    then 't'                                                         " & vbCrLf
'    sSQL = sSQL & "    Else 'c'  end tipocomponente,                                    " & vbCrLf
'    sSQL = sSQL & "    sinistro_tb.sinistro_id,                                         " & vbCrLf
'    sSQL = sSQL & "    sinistro_tb.situacao,                                            " & vbCrLf
'    sSQL = sSQL & "    situacao_desc   = '',                                            " & vbCrLf
'    sSQL = sSQL & "    situacao_evento = '',                                            " & vbCrLf
'    sSQL = sSQL & "    sit_evento_desc = '',                                            " & vbCrLf
'    sSQL = sSQL & "    sucursal_nome   = null,                                          " & vbCrLf
'    sSQL = sSQL & "    sinistro_tb.endosso_id ,                                         " & vbCrLf
'    sSQL = sSQL & "    sinistro_tb.sinistro_id_lider,                                   " & vbCrLf
'    sSQL = sSQL & "    sinistro_tb.processa_reintegracao_is,                            " & vbCrLf
'    sSQL = sSQL & "    historico = '',                                                  " & vbCrLf
'    sSQL = sSQL & "    sinistro_tb.dt_ocorrencia_sinistro                               " & vbCrLf
'    sSQL = sSQL & "FROM                                                                                                                     " & vbNewLine
'    sSQL = sSQL & " ((sinistro_tb  WITH (NOLOCK)                                                                                               " & vbNewLine
'    sSQL = sSQL & "        INNER JOIN (( (proposta_tb  WITH (NOLOCK)                                                                            " & vbNewLine
'    sSQL = sSQL & "        INNER JOIN proposta_complementar_tb  WITH (NOLOCK)     ON                                                           " & vbNewLine
'    sSQL = sSQL & "  proposta_tb.proposta_id    =  proposta_complementar_tb.proposta_id)                                                    " & vbNewLine
'    sSQL = sSQL & "        INNER JOIN produto_tb  WITH (NOLOCK)       ON                                                                       " & vbNewLine
'    sSQL = sSQL & "  proposta_tb.produto_id     =  produto_tb.produto_id)                                                                   " & vbNewLine
'    sSQL = sSQL & "        INNER JOIN cliente_tb  WITH (NOLOCK)       ON                                                                       " & vbNewLine
'    sSQL = sSQL & "  proposta_complementar_tb.prop_cliente_id  =  cliente_tb.cliente_id)                                                    " & vbNewLine
'    sSQL = sSQL & "         ON                                                                                                              " & vbNewLine
'    sSQL = sSQL & "  sinistro_tb.proposta_id    =  proposta_tb.proposta_id   AND                                                            " & vbNewLine
'    sSQL = sSQL & "  sinistro_tb.cliente_id     =  proposta_complementar_tb.prop_cliente_id)                                                " & vbNewLine
'    sSQL = sSQL & "        INNER JOIN ramo_tb  WITH (NOLOCK)       ON                                                                          " & vbNewLine
'    sSQL = sSQL & "  sinistro_tb.ramo_id     =  ramo_tb.ramo_id     AND                                                                     " & vbNewLine
'    sSQL = sSQL & "  ramo_tb.tp_ramo_id     = " & bytTipoRamo & "                                                                           " & vbNewLine
'    sSQL = sSQL & "     )                                                                                                                   " & vbNewLine
'    sSQL = sSQL & "        INNER JOIN apolice_tb  WITH (NOLOCK)       ON                                                                       " & vbNewLine
'    sSQL = sSQL & "  apolice_tb.apolice_id     =  sinistro_tb.apolice_id    AND                                                             " & vbNewLine
'    sSQL = sSQL & "  apolice_tb.ramo_id     =  sinistro_tb.ramo_id                                                                          " & vbNewLine
'    sSQL = sSQL & "WHERE                                                                                                                    " & vbNewLine
'    sSQL = sSQL & " sinistro_tb.sinistro_id     =  " & dSinistro_ID & "                                                                     " & vbNewLine

    'Alan Neves - 08/02/2013
'    sSQL = ""
'    sSQL = sSQL & " select Contador = Count(Sinistro_ID)                            " & vbCrLf
'    sSQL = sSQL & " from #tempsel_tb                                                " & vbCrLf
'    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
'                                        , glAmbiente_id _
'                                        , App.Title _
'                                        , App.FileDescription _
'                                        , sSQL _
'                                        , lConexaoLocal _
'                                        , True)
'
'
'    sSQL = ""
'    If rsRecordSet.Fields!Contador = 0 Then
'        'Inserir na #TempSel_Tb pela sinistro_selecao_tipo_2_cont_0_sps  -- Comentario j� existente
'        sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf
'        sSQL = sSQL & "SELECT DISTINCT                                                                                                          " & vbNewLine
'        sSQL = sSQL & " sinistro_tb.lock_aviso, sinistro_tb.apolice_id, sinistro_tb.sucursal_seguradora_id,                                     " & vbNewLine
'        sSQL = sSQL & " sinistro_tb.seguradora_cod_susep, ISNULL(ramo_tb.ramo_id,0) ramo_id, ISNULL(ramo_tb.nome,'') nome,                      " & vbNewLine
'        sSQL = sSQL & " proposta_tb.proposta_id, proposta_tb.num_endosso, proposta_tb.situacao, produto_tb.produto_id, produto_tb.nome,         " & vbNewLine
'        sSQL = sSQL & " cliente_tb.cliente_id, cliente_tb.nome,'', sinistro_tb.sinistro_id,sinistro_tb.situacao,'', '', '',null,                " & vbNewLine
'        sSQL = sSQL & " sinistro_tb.endosso_id, sinistro_tb.sinistro_id_lider, sinistro_tb.processa_reintegracao_is                             " & vbNewLine
'        sSQL = sSQL & "    ,historico = '',                                                  " & vbCrLf
'        sSQL = sSQL & "    sinistro_tb.dt_ocorrencia_sinistro                               " & vbCrLf
'        sSQL = sSQL & "         "
'        sSQL = sSQL & "FROM                                                                                                                     " & vbNewLine
'        sSQL = sSQL & " ((sinistro_tb  WITH (NOLOCK)                                                                                               " & vbNewLine
'        sSQL = sSQL & "        LEFT JOIN ((proposta_tb  WITH (NOLOCK)                                                                              " & vbNewLine
'        sSQL = sSQL & "        LEFT JOIN produto_tb  WITH (NOLOCK)     ON                                                                          " & vbNewLine
'        sSQL = sSQL & "  proposta_tb.produto_id   =  produto_tb.produto_id)                                                                     " & vbNewLine
'        sSQL = sSQL & "        LEFT JOIN cliente_tb  WITH (NOLOCK)     ON                                                                          " & vbNewLine
'        sSQL = sSQL & "  proposta_tb.prop_cliente_id  =  cliente_tb.cliente_id)                                                                 " & vbNewLine
'        sSQL = sSQL & "       ON                                                                                                                " & vbNewLine
'        sSQL = sSQL & "  sinistro_tb.proposta_id  =  proposta_tb.proposta_id                                                                    " & vbNewLine
'        sSQL = sSQL & "        INNER JOIN ramo_tb  WITH (NOLOCK)     ON                                                                            " & vbNewLine
'        sSQL = sSQL & "  sinistro_tb.ramo_id   =  ramo_tb.ramo_id and                                                                           " & vbNewLine   'AKIO.OKUNO - 27/09/2012
'        sSQL = sSQL & "  ramo_tb.tp_ramo_id     = " & bytTipoRamo & "))                                                                           " & vbNewLine
'        sSQL = sSQL & "WHERE                                                                                                                    " & vbNewLine
'        sSQL = sSQL & " sinistro_tb.sinistro_id     =  " & dSinistro_ID & "                                                                     " & vbNewLine
'    End If

    ' Comentario j� existente
    'Final da Proc Sinistro_Selecao_SPS
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Sinistro" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & "Select Top 1 Sinistro_ID                                                                                                 " & vbNewLine
'    sSQL = sSQL & "  Into #Sinistro                                                                                                         " & vbNewLine
'    sSQL = sSQL & "  From #TempSel_Tb                                                                                                       " & vbNewLine
'    Conexao_ExecutarSQL gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, lConexaoLocal, False
'
'    sSQL = ""
'    sSQL = sSQL & " select sinistro_vida_tb.cpf                                     " & vbCrLf
'    sSQL = sSQL & "      , Sinistro_Tb.Situacao                                     " & vbCrLf
'    sSQL = sSQL & " from sinistro_tb  WITH (NOLOCK)                                    " & vbCrLf
'    sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                               " & vbCrLf
'    sSQL = sSQL & "     on sinistro_tb.cliente_id = cliente_tb.cliente_id           " & vbCrLf
'    sSQL = sSQL & " inner join sinistro_vida_tb  WITH (NOLOCK)                         " & vbCrLf
'    sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id   " & vbCrLf
'    sSQL = sSQL & "  Join #Sinistro                                                 " & vbCrLf
'    sSQL = sSQL & "    on #Sinistro.Sinistro_ID = Sinistro_Vida_Tb.Sinistro_ID      " & vbCrLf
'
'    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
'                                        , glAmbiente_id _
'                                        , App.Title _
'                                        , App.FileDescription _
'                                        , sSQL _
'                                        , lConexaoLocal _
'                                        , True)
'
'    If Not rsRecordSet.EOF Then
'        sCPF = IIf(IsNull(rsRecordSet("CPF")), "", rsRecordSet("cpf"))
'        If LenB(Trim(sCPF)) <> 0 Then
'            sSQL = ""
'            sSQL = sSQL & "Select Contador = Count(Sinistro_ID)                                             " & vbNewLine
'            sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Vida_Tb               Sinistro_Vida_Tb  WITH (NOLOCK)    " & vbNewLine
'            sSQL = sSQL & " Where CPF = '" & sCPF & "'                                                      " & vbNewLine
'
'            Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
'                                                , glAmbiente_id _
'                                                , App.Title _
'                                                , App.FileDescription _
'                                                , sSQL _
'                                                , lConexaoLocal _
'                                                , True)
'
'            If rsRecordSet.Fields!Contador <= 25 Then
'                If Len(Trim(sCPF)) <= 11 Then
'                    sSQL = ""
'                    sSQL = sSQL & "  Delete From #TempSel_Tb                                                                                                       " & vbNewLine
'                    Conexao_ExecutarSQL gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, lConexaoLocal, False
'
'                    ConsultaSinistroPor_CPF_RamoVida sCPF, 1
'                End If
'            End If
'        Else
'            sSQL = ""
'            sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf
'            sSQL = sSQL & " exec seguros_db.dbo.SEGS10350_SPS '" & dSinistro_ID & "'," & 1
'
'            Conexao_ExecutarSQL gsSIGLASISTEMA _
'                              , glAmbiente_id _
'                              , App.Title _
'                              , App.FileDescription _
'                              , sSQL _
'                              , lConexaoLocal _
'                              , False
'        End If
'    End If
''    rsRecordSet.Close
''    Set rsRecordSet = Nothing


'' Alan Neves
''    Excluindo a temp
'    sSQL = ""
'    sSQL = sSQL & "Drop table #Sinistro" & vbNewLine
'
'    Conexao_ExecutarSQL gsSIGLASISTEMA _
'                      , glAmbiente_id _
'                      , App.Title _
'                      , App.FileDescription _
'                      , sSQL _
'                      , lConexaoLocal _
'                      , False
'
'    rsRecordSet.Close
'    Set rsRecordSet = Nothing

    sSQL = ""
    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10894_SPS '" & dSinistro_ID & "','" & sVariacao & "'" & ", '" & sOperacaoCosseguro & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'" & vbNewLine

    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
                      
    Exit Sub
    
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_Sinistro_RamoVIDA", Me.name)
    Call FinalizarAplicacao

End Sub

'' Laurent Silva - 10/11/2017
'' Demanda HiAgro

Private Sub ConsultaSinistroPor_Protocolo_RamoRural(dSinistro_ID As String, iTPRamo As Byte, sVariacao As String)
    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
    sSQL = ""
    ''sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10362_SPS " & iTPRamo & ", '" & dSinistro_ID & "','" & sVariacao & "'" & ", '" & sOperacaoCosseguro & "'" & vbNewLine
    sSQL = sSQL & "  Exec SEGS13612_SPS " & iTPRamo & ", '" & dSinistro_ID & "','" & sVariacao & "'" & ", '" & sOperacaoCosseguro & "'" & vbNewLine
      
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
                      

    Exit Sub
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_Sinistro_RamoRural", Me.name)
    Call FinalizarAplicacao
    
End Sub

'cristovao.rodrigues 15/08/2012
Private Function ConsultaSinistroPor_Apolice(lApolice_ID As Long, bTP_Ramo As Byte)

Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_Apolice_RamoVida lApolice_ID, bTP_Ramo
        Case bytTipoRamo_RE
            ConsultaSinistroPor_Apolice_RamoRural lApolice_ID, bTP_Ramo, "RE"
        Case bytTipoRamo_Rural
            ConsultaSinistroPor_Apolice_RamoRural lApolice_ID, 2, "RU"
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_Apolice", Me.name)

End Function

Private Function ConsultaSinistroPor_Apolice_RamoVida(lApolice_ID As Long, iTPRamo As Byte)

    Dim sSQL As String

    On Error GoTo TrataErro
    
    sSQL = ""
    
    'Alan Neves - 14/02/2013
'    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'        sSQL = sSQL & "Insert into #TempSel_Tb                                                                          " & vbNewLine
'        sSQL = sSQL & "     ( Proposta_ID )                                                                             " & vbNewLine
'        sSQL = sSQL & "Select Proposta_Tb.Proposta_ID                                                                   " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK)                         " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Ramo_Tb                        Ramo_Tb  WITH (NOLOCK)                             " & vbNewLine
'        sSQL = sSQL & "    on Ramo_Tb.Ramo_ID                               = Proposta_Tb.Ramo_ID                       " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Adesao_Tb        Proposta_Adesao_Tb  WITH (NOLOCK)                  " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Adesao_Tb.Proposta_ID                = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Fechada_Tb       Proposta_Fechada_Tb  WITH (NOLOCK)                 " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Fechada_Tb.Proposta_ID               = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Apolice_Tb                Apolice_Tb  WITH (NOLOCK)                          " & vbNewLine
'        sSQL = sSQL & "    on Apolice_Tb.Proposta_ID                        = Proposta_Fechada_Tb.Proposta_ID           " & vbNewLine
'        sSQL = sSQL & " Where Ramo_Tb.Tp_Ramo_ID                            = " & iTPRamo & vbNewLine
'        sSQL = sSQL & "   and Apolice_Tb.Apolice_ID                         = " & lApolice_ID & vbNewLine
'        sSQL = sSQL & "   and Proposta_Tb.Situacao                          <> 't'" & vbNewLine
'
'        Conexao_ExecutarSQL gsSIGLASISTEMA _
'                          , glAmbiente_id _
'                          , App.Title _
'                          , App.FileDescription _
'                          , sSQL _
'                          , lConexaoLocal _
'                          , False
'
'        sSQL = ""
'        sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10420_SPS    "
'    Else
'        sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf
'        sSQL = sSQL & " exec seguros_db.dbo.SEGS10358_SPS " & lApolice_ID & "," & iTPRamo
'    End If
    
'    sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbNewLine
'    sSQL = sSQL & "       marca                                             " & vbNewLine
'    sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
'    sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
'    sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
'    sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
'    sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
'    sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
'    sSQL = sSQL & "     , numendosso                                        " & vbNewLine
'    sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
'    sSQL = sSQL & "     , produto_id                                        " & vbNewLine
'    sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
'    sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
'    sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
'    sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
'    sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
'    sSQL = sSQL & "     , situacao                                          " & vbNewLine
'    sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
'    sSQL = sSQL & "     , situacao_evento                                      " & vbNewLine
'    sSQL = sSQL & "     , sit_evento_desc                                      " & vbNewLine
'    sSQL = sSQL & "     , Sucursal_Nome                                        " & vbNewLine
'    sSQL = sSQL & "     , Endosso                                              " & vbNewLine
'    sSQL = sSQL & "     , sinistro_id_lider                                    " & vbNewLine
'    sSQL = sSQL & "     , processa_reintegracao_is                             " & vbNewLine
'    sSQL = sSQL & "     , historico                                            " & vbNewLine
'    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbNewLine 'cristovao.rodrigues 08/11/2012
'    sSQL = sSQL & "     , Tp_Emissao                                           " & vbNewLine 'cristovao.rodrigues 08/11/2012
'    sSQL = sSQL & "     )                                                   " & vbNewLine

    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10896_SPS '" & lApolice_ID & "','" & iTPRamo & "'" & ", '" & sOperacaoCosseguro & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'" & vbNewLine

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         lConexaoLocal, _
                         False)

    
    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_Apolice_RamoVida", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Sub ConsultaSinistroPor_Apolice_RamoRural(lApolice_ID As Long, iTPRamo As Byte, sVariacao As String)
    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
    sSQL = ""
    
    'Alan Neves - 14/02/2013
'    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'        sSQL = sSQL & "Insert into #TempSel_Tb                                                                          " & vbNewLine
'        sSQL = sSQL & "     ( Proposta_ID )                                                                             " & vbNewLine
'        sSQL = sSQL & "Select Proposta_Tb.Proposta_ID                                                                   " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK)                         " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Ramo_Tb                        Ramo_Tb  WITH (NOLOCK)                             " & vbNewLine
'        sSQL = sSQL & "    on Ramo_Tb.Ramo_ID                               = Proposta_Tb.Ramo_ID                       " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Adesao_Tb        Proposta_Adesao_Tb  WITH (NOLOCK)                  " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Adesao_Tb.Proposta_ID                = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Fechada_Tb       Proposta_Fechada_Tb  WITH (NOLOCK)                 " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Fechada_Tb.Proposta_ID               = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Apolice_Tb                Apolice_Tb  WITH (NOLOCK)                          " & vbNewLine
'        sSQL = sSQL & "    on Apolice_Tb.Proposta_ID                        = Proposta_Fechada_Tb.Proposta_ID           " & vbNewLine
'
'''        'MATHAYDE
''        sSQL = sSQL & " Where Ramo_Tb.Tp_Ramo_ID                            = " & iTPRamo & vbNewLine
''        sSQL = sSQL & "   and Apolice_Tb.Apolice_ID                         = " & lApolice_ID & vbNewLine
''        sSQL = sSQL & "   and Proposta_Tb.Produto_ID                        " & IIf(sVariacao = "RE", "not", "") & " in (126, 155, 156)                      " & vbNewLine
''        sSQL = sSQL & "   and Proposta_Tb.Situacao                          <> 't'" & vbNewLine
''        sSQL = sSQL & "   and " & IIf(sVariacao = "RE", "not", "") & " (Proposta_Tb.PRoduto_ID = 400 and Proposta_Tb.Ramo_ID = 63)                           " & vbNewLine
'
'        sSQL = sSQL & "   Where Proposta_Tb.Situacao                          <> 't'" & vbNewLine
'        sSQL = sSQL & "     AND Ramo_Tb.Tp_Ramo_ID                            = " & iTPRamo & vbNewLine
'        sSQL = sSQL & "     and Apolice_Tb.Apolice_ID                         = " & lApolice_ID & vbNewLine
'
'        If sVariacao = "RU" Then                                                                                                                                                                                                                                                'MATHAYDE (POR AKIO.OKUNO - 27/09/2012)
'            sSQL = sSQL & "     and  ((Proposta_Tb.produto_id in (126,155,156,1152,1204) and  ramo_tb.tp_ramo_id =  " & iTPRamo & ")" & vbNewLine               'MATHAYDE (POR AKIO.OKUNO - 27/09/2012)
'            sSQL = sSQL & "      or (Proposta_Tb.produto_id =  400 and ramo_tb.ramo_id = 63)" & vbNewLine                                                                                               'MATHAYDE (POR AKIO.OKUNO - 27/09/2012)
'            sSQL = sSQL & "      or  Proposta_Tb.produto_id =  14 )" & vbNewLine                                                                                                                                                'MATHAYDE (POR AKIO.OKUNO - 27/09/2012)
'        Else                                                                                                                                                                                                                                                                                    'MATHAYDE (POR AKIO.OKUNO - 27/09/2012)
'            sSQL = sSQL & "     and  Proposta_Tb.produto_id NOT in (14, 126,155,156,1152,1204)" & vbNewLine                                                                                             'MATHAYDE (POR AKIO.OKUNO - 27/09/2012)
'            sSQL = sSQL & "     AND  NOT (Proposta_Tb.produto_id =  400 and ramo_tb.ramo_id = 63)" & vbNewLine                                                                                  'MATHAYDE (POR AKIO.OKUNO - 27/09/2012)
'        End If                                                                                                                                                                                                                                                                                  'MATHAYDE (POR AKIO.OKUNO - 27/09/2012)
'
'        Conexao_ExecutarSQL gsSIGLASISTEMA _
'                          , glAmbiente_id _
'                          , App.Title _
'                          , App.FileDescription _
'                          , sSQL _
'                          , lConexaoLocal _
'                          , False
'
'        sSQL = ""
'        Select Case bytTipoRamo
'            Case bytTipoRamo_RE
'                sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10421_SPS    "
'            Case bytTipoRamo_Rural
'                sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10422_SPS    "
'        End Select
'    Else
'        sSQL = sSQL & "Insert into #tempsel_tb (                                " & vbNewLine
'        sSQL = sSQL & "       marca                                             " & vbNewLine
'        sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
'        sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
'        sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
'        sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
'        sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
'        sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
'        sSQL = sSQL & "     , numendosso                                        " & vbNewLine
'        sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
'        sSQL = sSQL & "     , produto_id                                        " & vbNewLine
'        sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
'        sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
'        sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
'        sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
'        sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
'        sSQL = sSQL & "     , situacao                                          " & vbNewLine
'        sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
'        sSQL = sSQL & "  , situacao_evento                                      " & vbNewLine
'        sSQL = sSQL & "  , sit_evento_desc                                      " & vbNewLine
'        sSQL = sSQL & "  , Sucursal_Nome                                        " & vbNewLine
'        sSQL = sSQL & "  , Endosso                                              " & vbNewLine
'        sSQL = sSQL & "  , sinistro_id_lider                                    " & vbNewLine
'        sSQL = sSQL & "  , processa_reintegracao_is                             " & vbNewLine
'        sSQL = sSQL & "  , Dt_Ocorrencia_Sinistro                               " & vbNewLine 'cristovao.rodrigues 08/11/2012
'        sSQL = sSQL & "     )                                                   " & vbNewLine
'        sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10370_SPS " & iTPRamo & ", '" & lApolice_ID & "','" & sVariacao & "'" & vbNewLine
'    End If

    sSQL = ""
    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10895_SPS '" & lApolice_ID & "','" & iTPRamo & "'" & ", '" & sVariacao & "','" & sOperacaoCosseguro & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "','" & bytTipoRamo & "'" & vbNewLine

    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
    
    Exit Sub
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_Apolice_RamoRural", Me.name)
    Call FinalizarAplicacao
    
End Sub


Private Function ConsultaSinistroPor_Proposta_RamoVida(lProposta_ID As Long, iTPRamo As Byte, Optional bCosseguro As Boolean)

    Dim sSQL As String
    Dim rsRecordSet As ADODB.Recordset
    Dim SinistroConsulta As String
    Dim sCPF As String
    
'
'    sSQL = ""
'    sSQL = sSQL & "select distinct                                                  " & vbCrLf
'    sSQL = sSQL & " sinistro_vida_tb.cpf,                                           " & vbCrLf
'    sSQL = sSQL & " proposta_tb.proposta_id                                         " & vbCrLf
'    sSQL = sSQL & " --sinistro_tb.cliente_id ,                                      " & vbCrLf
'    sSQL = sSQL & " --sinistro_tb.sinistro_id,                                      " & vbCrLf
'    sSQL = sSQL & " --sinistro_tb.apolice_id                                        " & vbCrLf
'    sSQL = sSQL & "from sinistro_tb  WITH (NOLOCK)                                     " & vbCrLf
'    sSQL = sSQL & "inner join cliente_tb  WITH (NOLOCK)                                " & vbCrLf
'    sSQL = sSQL & "    on sinistro_tb.cliente_id = cliente_tb.cliente_id            " & vbCrLf
'    sSQL = sSQL & "inner join sinistro_vida_tb  WITH (NOLOCK)                          " & vbCrLf
'    sSQL = sSQL & "    on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id    " & vbCrLf
'    sSQL = sSQL & "inner join proposta_tb  WITH (NOLOCK)                                   " & vbCrLf
'    sSQL = sSQL & " on proposta_tb.proposta_id = sinistro_tb.proposta_id            " & vbCrLf
'    sSQL = sSQL & "Where "
'    sSQL = sSQL & "   proposta_tb.proposta_id = " & lProposta_ID & vbCrLf
'    sSQL = sSQL & "   and sinistro_tb.situacao <> 7 " & vbCrLf
'
'     Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                           glAmbiente_id, _
'                                           App.Title, _
'                                           App.FileDescription, _
'                                           sSQL, _
'                                           lConexaoLocal, _
'                                           True)
'    If Not rsRecordSet.EOF Then
        
        
        'Alan Neves - 15/02/2013
'        If Not (strTipoProcesso = strTipoProcesso_Avisar Or _
'                strTipoProcesso = strTipoProcesso_Avisar_Cosseguro) Then
'            sSQL = ""
'            sSQL = sSQL & "select distinct                                                  " & vbCrLf
'            sSQL = sSQL & " sinistro_vida_tb.cpf,                                           " & vbCrLf
'            sSQL = sSQL & " proposta_tb.proposta_id                                         " & vbCrLf
'            sSQL = sSQL & " --sinistro_tb.cliente_id ,                                      " & vbCrLf
'            sSQL = sSQL & " --sinistro_tb.sinistro_id,                                      " & vbCrLf
'            sSQL = sSQL & " --sinistro_tb.apolice_id                                        " & vbCrLf
'            sSQL = sSQL & "from sinistro_tb  WITH (NOLOCK)                                     " & vbCrLf
'            sSQL = sSQL & "inner join cliente_tb  WITH (NOLOCK)                                " & vbCrLf
'            sSQL = sSQL & "    on sinistro_tb.cliente_id = cliente_tb.cliente_id            " & vbCrLf
'            sSQL = sSQL & "inner join sinistro_vida_tb  WITH (NOLOCK)                          " & vbCrLf
'            sSQL = sSQL & "    on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id    " & vbCrLf
'            sSQL = sSQL & "inner join proposta_tb  WITH (NOLOCK)                                   " & vbCrLf
'            sSQL = sSQL & " on proposta_tb.proposta_id = sinistro_tb.proposta_id            " & vbCrLf
'            sSQL = sSQL & "Where "
'            sSQL = sSQL & "   proposta_tb.proposta_id = " & lProposta_ID & vbCrLf
'            sSQL = sSQL & "   and sinistro_tb.situacao <> 7 " & vbCrLf
'
'            Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                                  glAmbiente_id, _
'                                                  App.Title, _
'                                                  App.FileDescription, _
'                                                  sSQL, _
'                                                  lConexaoLocal, _
'                                                  True)
'            If Not rsRecordSet.EOF Then
'                sCPF = IIf(IsNull(rsRecordSet("CPF")), "", rsRecordSet("cpf"))
'
'                sSQL = ""
'                sSQL = sSQL & "select distinct                                                  " & vbCrLf
'                sSQL = sSQL & " sinistro_vida_tb.cpf,                                           " & vbCrLf
'                sSQL = sSQL & " proposta_tb.proposta_id                                         " & vbCrLf
'                sSQL = sSQL & " --sinistro_tb.cliente_id ,                                      " & vbCrLf
'                sSQL = sSQL & " --sinistro_tb.sinistro_id,                                      " & vbCrLf
'                sSQL = sSQL & " --sinistro_tb.apolice_id                                        " & vbCrLf
'                sSQL = sSQL & "from sinistro_tb  WITH (NOLOCK)                                     " & vbCrLf
'                sSQL = sSQL & "inner join cliente_tb  WITH (NOLOCK)                                " & vbCrLf
'                sSQL = sSQL & "    on sinistro_tb.cliente_id = cliente_tb.cliente_id            " & vbCrLf
'                sSQL = sSQL & "inner join sinistro_vida_tb  WITH (NOLOCK)                          " & vbCrLf
'                sSQL = sSQL & "    on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id    " & vbCrLf
'                sSQL = sSQL & "inner join proposta_tb  WITH (NOLOCK)                                   " & vbCrLf
'                sSQL = sSQL & " on proposta_tb.proposta_id = sinistro_tb.proposta_id            " & vbCrLf
'                sSQL = sSQL & "Where "
'                sSQL = sSQL & "   proposta_tb.proposta_id = " & lProposta_ID & vbCrLf
'                sSQL = sSQL & "   and sinistro_tb.situacao <> 7 " & vbCrLf
'                sSQL = sSQL & "   and proposta_tb.produto_id in (115,123, 222 ,223 ,1140) "
'
'                Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                                      glAmbiente_id, _
'                                                      App.Title, _
'                                                      App.FileDescription, _
'                                                      sSQL, _
'                                                      lConexaoLocal, _
'                                                      True)
'
'
'                If Not rsRecordSet.EOF Or sCPF = "" Then
'                    sSQL = ""
'                    sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf
'                    sSQL = sSQL & " exec seguros_db.dbo.SEGS10386_SPS '" & lProposta_ID & "'," & iTPRamo
'
'                    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                             glAmbiente_id, _
'                                             App.Title, _
'                                             App.FileDescription, _
'                                             sSQL, _
'                                             lConexaoLocal, _
'                                             False)
'                Else
'                    Retorno = ConsultaSinistroPor_CPF_RamoVida(sCPF, iTPRamo)
'                End If
'            End If
'
'        Else
'            sSQL = ""
'            sSQL = sSQL & "Insert into #TempSel_Tb                                                                          " & vbNewLine
'            sSQL = sSQL & "     ( Proposta_ID )                                                                             " & vbNewLine
'            sSQL = sSQL & "Select Proposta_Tb.Proposta_ID                                                                   " & vbNewLine
'            sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK)                         " & vbNewLine
'            sSQL = sSQL & "  Join Seguros_Db.Dbo.Ramo_Tb                        Ramo_Tb  WITH (NOLOCK)                             " & vbNewLine
'            sSQL = sSQL & "    on Ramo_Tb.Ramo_ID                               = Proposta_Tb.Ramo_ID                       " & vbNewLine
'            sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Adesao_Tb        Proposta_Adesao_Tb  WITH (NOLOCK)                  " & vbNewLine
'            sSQL = sSQL & "    on Proposta_Adesao_Tb.Proposta_ID                = Proposta_Tb.Proposta_ID                   " & vbNewLine
'            sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Fechada_Tb       Proposta_Fechada_Tb  WITH (NOLOCK)                 " & vbNewLine
'            sSQL = sSQL & "    on Proposta_Fechada_Tb.Proposta_ID               = Proposta_Tb.Proposta_ID                   " & vbNewLine
'            sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Apolice_Tb                Apolice_Tb  WITH (NOLOCK)                          " & vbNewLine
'            sSQL = sSQL & "    on Apolice_Tb.Proposta_ID                        = Proposta_Fechada_Tb.Proposta_ID           " & vbNewLine
'            sSQL = sSQL & " Where Ramo_Tb.Tp_Ramo_ID                            = " & iTPRamo & vbNewLine
'            sSQL = sSQL & "   and Proposta_Tb.Proposta_ID                       = " & lProposta_ID & vbNewLine
'            sSQL = sSQL & "   and Proposta_Tb.Situacao                          <> 't'" & vbNewLine
'
'            Conexao_ExecutarSQL gsSIGLASISTEMA _
'                              , glAmbiente_id _
'                              , App.Title _
'                              , App.FileDescription _
'                              , sSQL _
'                              , lConexaoLocal _
'                              , False
'
'            sSQL = ""
'            sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10420_SPS    "


'            sSQL = ""
'            sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf
'            sSQL = sSQL & " exec seguros_db.dbo.SEGS10354_SPS '" & lProposta_ID & "'," & iTPRamo

    sSQL = ""
    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10900_SPS '" & lProposta_ID & "','" & iTPRamo & "'" & ", '" & sVariacao & "','" & sOperacaoCosseguro & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'" & vbNewLine


    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
'        End If
        
'    End If
'
'    rsRecordSet.Close
'    Set rsRecordSet = Nothing
        
    
    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_Proposta", Me.name)
    FinalizarAplicacao

End Function

'cristovao.rodrigues 14/06/2012
Private Function ConsultaSinistroPor_PropostaBB_RamoVida(iPropostaBB As Long, iTPRamo As Byte)

    Dim sSQL                                    As String
    Dim rsRecordSet                             As ADODB.Recordset
    Dim SinistroConsulta                        As String
    Dim sCPF                                    As String
    
    On Error GoTo TrataErro
        
    'Alan Neves - 21/02/2013 - Performance
    sSQL = ""
'    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'        sSQL = ""
'        sSQL = sSQL & " select distinct                                                 " & vbCrLf
'        sSQL = sSQL & "     Cliente_Tb.cpf_cnpj as CPF,                                       " & vbCrLf
'        sSQL = sSQL & "     proposta_tb.proposta_id                                    " & vbCrLf
'        sSQL = sSQL & " from proposta_adesao_tb  WITH (NOLOCK)                             " & vbCrLf
'        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                  " & vbCrLf
'        sSQL = sSQL & "     on proposta_tb.proposta_id = proposta_adesao_tb.proposta_id " & vbCrLf
'        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                               " & vbCrLf
'        sSQL = sSQL & "     on cliente_tb.cliente_id = Proposta_Tb.Prop_Cliente_ID          " & vbCrLf
'        sSQL = sSQL & " Where                                                           " & vbCrLf
'        sSQL = sSQL & "     proposta_adesao_tb.proposta_bb  =  " & iPropostaBB & vbCrLf
'        sSQL = sSQL & "     and Proposta_Tb.Situacao <> 't'                               "
'    Else
'        sSQL = ""
'        sSQL = sSQL & " select distinct                                                 " & vbCrLf
'        sSQL = sSQL & "     sinistro_vida_tb.cpf,                                       " & vbCrLf
'        sSQL = sSQL & "     proposta_tb.proposta_id                                    " & vbCrLf
'        sSQL = sSQL & "     --sinistro_tb.cliente_id ,                                    " & vbCrLf
'        sSQL = sSQL & "     --sinistro_tb.sinistro_id,                                    " & vbCrLf
'        sSQL = sSQL & "     --sinistro_tb.apolice_id                                      " & vbCrLf
'        sSQL = sSQL & " from proposta_adesao_tb  WITH (NOLOCK)                             " & vbCrLf
'        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                  " & vbCrLf
'        sSQL = sSQL & "     on proposta_tb.proposta_id = proposta_adesao_tb.proposta_id " & vbCrLf
'        sSQL = sSQL & " inner join sinistro_tb  WITH (NOLOCK)                                  " & vbCrLf
'        sSQL = sSQL & "     on proposta_tb.proposta_id = sinistro_tb.proposta_id        " & vbCrLf
'        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                               " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.cliente_id = cliente_tb.cliente_id           " & vbCrLf
'        sSQL = sSQL & " inner join sinistro_vida_tb  WITH (NOLOCK)                         " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id   " & vbCrLf
'        sSQL = sSQL & " Where                                                           " & vbCrLf
'        sSQL = sSQL & "     proposta_adesao_tb.proposta_bb  =  " & iPropostaBB & vbCrLf
'        sSQL = sSQL & "     and sinistro_tb.situacao <> 7                               "
'    End If
'
'    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                          glAmbiente_id, _
'                                          App.Title, _
'                                          App.FileDescription, _
'                                          sSQL, _
'                                          lConexaoLocal, _
'                                          True)
'    If Not rsRecordSet.EOF Then
'
'        sCPF = IIf(IsNull(rsRecordSet("CPF")), "", rsRecordSet("cpf"))
'
'        If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'            sSQL = ""
'            sSQL = sSQL & "Select Cliente_Tb.CPF_CNPJ as CPF" & vbCrLf
'            sSQL = sSQL & "     , Proposta_Tb.Proposta_ID" & vbCrLf
'            sSQL = sSQL & " from proposta_adesao_tb  WITH (NOLOCK)                             " & vbCrLf
'            sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                  " & vbCrLf
'            sSQL = sSQL & "     on proposta_tb.proposta_id = proposta_adesao_tb.proposta_id " & vbCrLf
'            sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                               " & vbCrLf
'            sSQL = sSQL & "     on cliente_tb.cliente_id = Proposta_Tb.Prop_Cliente_ID          " & vbCrLf
'            sSQL = sSQL & " Where                                                           " & vbCrLf
'            sSQL = sSQL & "     proposta_adesao_tb.proposta_bb  =  " & iPropostaBB & vbCrLf
'            sSQL = sSQL & "     and Proposta_Tb.Situacao <> 't'                               " & vbNewLine
'            sSQL = sSQL & "   and proposta_tb.produto_id in (115,123, 222 ,223 ,1140) "
'
'        Else
'            sSQL = ""
'            sSQL = sSQL & " select distinct                                                 " & vbCrLf
'            sSQL = sSQL & "     sinistro_vida_tb.cpf,                                       " & vbCrLf
'            sSQL = sSQL & "     proposta_tb.proposta_id                                    " & vbCrLf
'            sSQL = sSQL & "     --sinistro_tb.cliente_id ,                                    " & vbCrLf
'            sSQL = sSQL & "     --sinistro_tb.sinistro_id,                                    " & vbCrLf
'            sSQL = sSQL & "     --sinistro_tb.apolice_id                                      " & vbCrLf
'            sSQL = sSQL & " from proposta_adesao_tb  WITH (NOLOCK)                             " & vbCrLf
'            sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                  " & vbCrLf
'            sSQL = sSQL & "     on proposta_tb.proposta_id = proposta_adesao_tb.proposta_id " & vbCrLf
'            sSQL = sSQL & " inner join sinistro_tb  WITH (NOLOCK)                                  " & vbCrLf
'            sSQL = sSQL & "     on proposta_tb.proposta_id = sinistro_tb.proposta_id        " & vbCrLf
'            sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                               " & vbCrLf
'            sSQL = sSQL & "     on sinistro_tb.cliente_id = cliente_tb.cliente_id           " & vbCrLf
'            sSQL = sSQL & " inner join sinistro_vida_tb  WITH (NOLOCK)                         " & vbCrLf
'            sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id   " & vbCrLf
'            sSQL = sSQL & " Where                                                           " & vbCrLf
'            sSQL = sSQL & "     proposta_adesao_tb.proposta_bb  =  " & iPropostaBB & vbCrLf
'            sSQL = sSQL & "     and sinistro_tb.situacao <> 7                               " & vbCrLf
'            sSQL = sSQL & "   and proposta_tb.produto_id in (115,123, 222 ,223 ,1140) "
'
'        End If
'
'        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                              glAmbiente_id, _
'                                              App.Title, _
'                                              App.FileDescription, _
'                                              sSQL, _
'                                              lConexaoLocal, _
'                                              True)
'
'
'        If rsRecordSet.RecordCount > 0 Or sCPF = "" Then
'
'
'            sSQL = ""
'            If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'                sSQL = ""
'                sSQL = sSQL & "Insert into #TempSel_Tb" & vbNewLine
'                sSQL = sSQL & "     ( Proposta_ID )" & vbNewLine
'                sSQL = sSQL & "Select Proposta_Tb.Proposta_ID" & vbCrLf
'                sSQL = sSQL & " from proposta_adesao_tb  WITH (NOLOCK)                             " & vbCrLf
'                sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                  " & vbCrLf
'                sSQL = sSQL & "     on proposta_tb.proposta_id = proposta_adesao_tb.proposta_id " & vbCrLf
'                sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                               " & vbCrLf
'                sSQL = sSQL & "     on cliente_tb.cliente_id = Proposta_Tb.Prop_Cliente_ID          " & vbCrLf
'                sSQL = sSQL & " Where                                                           " & vbCrLf
'                sSQL = sSQL & "     proposta_adesao_tb.proposta_bb  =  " & iPropostaBB & vbCrLf
'                sSQL = sSQL & "     and Proposta_Tb.Situacao <> 't'                               " & vbNewLine
'
'                Conexao_ExecutarSQL gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, lConexaoLocal, False
'
'                sSQL = ""
'                sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10420_SPS    "
'            Else
'                'Alan Neves - 19/02/2013
'                ' sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf
'
'                'Alan Neves - 19/02/2013
'                sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
'                sSQL = sSQL & "       marca                                             " & vbCrLf
'                sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
'                sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
'                sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
'                sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
'                sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
'                sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
'                sSQL = sSQL & "     , numendosso                                        " & vbCrLf
'                sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
'                sSQL = sSQL & "     , produto_id                                        " & vbCrLf
'                sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
'                sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
'                sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
'                sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
'                sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
'                sSQL = sSQL & "     , situacao                                          " & vbCrLf
'                sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
'                sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
'                sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
'                sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
'                sSQL = sSQL & "     , Endosso                                              " & vbCrLf
'                sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
'                sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
'                sSQL = sSQL & "     , historico                                            " & vbCrLf
'                sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
'                sSQL = sSQL & "     )                                                   " & vbCrLf
'
'                sSQL = sSQL & " exec seguros_db.dbo.SEGS10355_SPS '" & iPropostaBB & "'," & iTPRamo
'            End If
'
'            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                     glAmbiente_id, _
'                                     App.Title, _
'                                     App.FileDescription, _
'                                     sSQL, _
'                                     lConexaoLocal, _
'                                     False)
'
'        Else
'            If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'                'Inserir nova query para busca por proposta_bb
'                sSQL = ""
'                sSQL = sSQL & "Insert into #tempsel_tb( " & vbCrLf
'                sSQL = sSQL & "      marca                    " & vbCrLf
'                sSQL = sSQL & "     , apolice_id               " & vbCrLf
'                sSQL = sSQL & "     , sucursal_seguradora_id   " & vbCrLf
'                sSQL = sSQL & "     , seguradora_cod_susep     " & vbCrLf
'                sSQL = sSQL & "     , ramo_id                  " & vbCrLf
'                sSQL = sSQL & "     , nomeramo                 " & vbCrLf
'                sSQL = sSQL & "     , proposta_id              " & vbCrLf
'                sSQL = sSQL & "     , numendosso               " & vbCrLf
'                sSQL = sSQL & "     , sitproposta              " & vbCrLf
'                sSQL = sSQL & "     , produto_id               " & vbCrLf
'                sSQL = sSQL & "     , nomeproduto              " & vbCrLf
'                sSQL = sSQL & "     , cliente_id               " & vbCrLf
'                sSQL = sSQL & "     , nomecliente              " & vbCrLf
'                sSQL = sSQL & "     , tipocomponente           " & vbCrLf
'                sSQL = sSQL & "     , sinistro_id              " & vbCrLf
'                sSQL = sSQL & "     )   "
'                sSQL = sSQL & "SELECT DISTINCT                                                                             " & vbNewLine
'                sSQL = sSQL & "        'n'                                                                                  " & vbNewLine
'                sSQL = sSQL & "      , proposta_adesao_tb.apolice_id                                                        " & vbNewLine
'                sSQL = sSQL & "      , proposta_adesao_tb.sucursal_seguradora_id                                            " & vbNewLine
'                sSQL = sSQL & "      , proposta_adesao_tb.seguradora_cod_susep                                              " & vbNewLine
'                sSQL = sSQL & "      , ramo_tb.ramo_id                                                                      " & vbNewLine
'                sSQL = sSQL & "      , ramo_tb.nome                                                                         " & vbNewLine
'                sSQL = sSQL & "      , proposta_tb.proposta_id                                                              " & vbNewLine
'                sSQL = sSQL & "      , proposta_tb.num_endosso                                                              " & vbNewLine
'                sSQL = sSQL & "      , proposta_tb.situacao                                                                 " & vbNewLine
'                sSQL = sSQL & "      , produto_tb.produto_id                                                                " & vbNewLine
'                sSQL = sSQL & "      , produto_tb.nome                                                                      " & vbNewLine
'                sSQL = sSQL & "      , cliente_tb.cliente_id                                                                " & vbNewLine
'                sSQL = sSQL & "      , cliente_tb.nome                                                                      " & vbNewLine
'                sSQL = sSQL & "      , 'T'                                                                                  " & vbNewLine
'                sSQL = sSQL & "      , null                                                                                 " & vbNewLine
'                sSQL = sSQL & "  FROM  proposta_adesao_tb with ( nolock )                                                   " & vbNewLine
'                sSQL = sSQL & " INNER JOIN ( ( proposta_tb with ( nolock )                                                  " & vbNewLine
'                sSQL = sSQL & "                INNER JOIN produto_tb with ( nolock )                                        " & vbNewLine
'                sSQL = sSQL & "                   ON proposta_tb.produto_id = produto_tb.produto_id                         " & vbNewLine
'                sSQL = sSQL & "              )                                                                              " & vbNewLine
'                sSQL = sSQL & "              INNER JOIN cliente_tb with ( nolock )                                          " & vbNewLine
'                sSQL = sSQL & "                 ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id                      " & vbNewLine
'                sSQL = sSQL & "            )                                                                                " & vbNewLine
'                sSQL = sSQL & "    ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id                              " & vbNewLine
'                sSQL = sSQL & " INNER JOIN ramo_tb with ( nolock )                                                          " & vbNewLine
'                sSQL = sSQL & "    ON proposta_adesao_tb.ramo_id = ramo_tb.ramo_id                                          " & vbNewLine
'                sSQL = sSQL & "   AND ramo_tb.tp_ramo_id                                                                    = " & bytTipoRamo
'                sSQL = sSQL & " WHERE    proposta_adesao_tb.proposta_bb                                                     = " & iPropostaBB
'                sSQL = sSQL & " UNION ALL                                                                                   " & vbNewLine
'                sSQL = sSQL & "SELECT  DISTINCT                                                                             " & vbNewLine
'                sSQL = sSQL & "        'n'                                                                                  " & vbNewLine
'                sSQL = sSQL & "      , proposta_adesao_tb.apolice_id                                                        " & vbNewLine
'                sSQL = sSQL & "      , proposta_adesao_tb.sucursal_seguradora_id                                            " & vbNewLine
'                sSQL = sSQL & "      , proposta_adesao_tb.seguradora_cod_susep                                              " & vbNewLine
'                sSQL = sSQL & "      , ramo_tb.ramo_id                                                                      " & vbNewLine
'                sSQL = sSQL & "      , ramo_tb.nome                                                                         " & vbNewLine
'                sSQL = sSQL & "      , proposta_tb.proposta_id                                                              " & vbNewLine
'                sSQL = sSQL & "      , proposta_tb.num_endosso                                                              " & vbNewLine
'                sSQL = sSQL & "      , proposta_tb.situacao                                                                 " & vbNewLine
'                sSQL = sSQL & "      , produto_tb.produto_id                                                                " & vbNewLine
'                sSQL = sSQL & "      , produto_tb.nome                                                                      " & vbNewLine
'                sSQL = sSQL & "      , cliente_tb.cliente_id                                                                " & vbNewLine
'                sSQL = sSQL & "      , cliente_tb.nome                                                                      " & vbNewLine
'                sSQL = sSQL & "      , 'C'                                                                                  " & vbNewLine
'                sSQL = sSQL & "      , null                                                                                 " & vbNewLine
'                sSQL = sSQL & "  FROM proposta_adesao_tb with ( nolock )                                                    " & vbNewLine
'                sSQL = sSQL & " INNER JOIN ( ( ( proposta_tb with ( nolock )                                                " & vbNewLine
'                sSQL = sSQL & "                  INNER JOIN proposta_complementar_tb with ( nolock )                        " & vbNewLine
'                sSQL = sSQL & "                     ON proposta_tb.proposta_id = proposta_complementar_tb.proposta_id       " & vbNewLine
'                sSQL = sSQL & "                )                                                                            " & vbNewLine
'                sSQL = sSQL & "              INNER JOIN produto_tb with ( nolock )                                          " & vbNewLine
'                sSQL = sSQL & "                 ON proposta_tb.produto_id = produto_tb.produto_id                           " & vbNewLine
'                sSQL = sSQL & "             )                                                                               " & vbNewLine
'                sSQL = sSQL & "             INNER JOIN cliente_tb with ( nolock )                                           " & vbNewLine
'                sSQL = sSQL & "                ON proposta_complementar_tb.prop_cliente_id = cliente_tb.cliente_id          " & vbNewLine
'                sSQL = sSQL & "            )                                                                                " & vbNewLine
'                sSQL = sSQL & "    ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id                              " & vbNewLine
'                sSQL = sSQL & " INNER JOIN ramo_tb with ( nolock )                                                          " & vbNewLine
'                sSQL = sSQL & "    ON proposta_adesao_tb.ramo_id = ramo_tb.ramo_id                                          " & vbNewLine
'                sSQL = sSQL & "   AND ramo_tb.tp_ramo_id                                                                    = " & bytTipoRamo
'                sSQL = sSQL & " WHERE    proposta_adesao_tb.proposta_bb                                                     = " & iPropostaBB
'                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                         glAmbiente_id, _
'                                         App.Title, _
'                                         App.FileDescription, _
'                                         sSQL, _
'                                         lConexaoLocal, _
'                                         False)
'            Else
'                Retorno = ConsultaSinistroPor_CPF_RamoVida(sCPF, iTPRamo)
'            End If
'        End If
'
'    End If

        
    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10916_SPS " & iPropostaBB & "," & iTPRamo & ", '" & sVariacao & "','" & sOperacaoCosseguro & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'" & vbNewLine
    
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
                                
    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_PropostaBB", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Function ConsultaSinistroPor_CPF(sCPF As String, bTP_Ramo As Byte)
'Private Function ConsultaSinistroPor_CPF(iCPF As String, bTP_Ramo As Byte)

    Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_CPF_RamoVida sCPF, bTP_Ramo
        Case bytTipoRamo_RE
            ConsultaSinistroPor_CPF_RamoRural sCPF, bTP_Ramo, "RE"
        Case bytTipoRamo_Rural
            ConsultaSinistroPor_CPF_RamoRural sCPF, 2, "RU"
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_CPF", Me.name)

End Function

Private Function ConsultaSinistroPor_CPF_RamoVida(iCPF As String, iTPRamo As Byte)

    Dim sSQL As String
    
    On Error GoTo TrataErro
    
    sSQL = ""
' Alan Neves - apenas a linha abaixo
'If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then


'        sSQL = sSQL & "Insert into #TempSel_Tb                                                                                  " & vbNewLine
'        sSQL = sSQL & "     ( Proposta_ID )                                                                                     " & vbNewLine
'        sSQL = sSQL & "Select Proposta_Tb.Proposta_ID                                                                           " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Cliente_Tb                         Cliente_Tb  WITH (NOLOCK)                              " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Tb                            Proposta_Tb  WITH (NOLOCK)                         " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Tb.Prop_Cliente_ID                           = Cliente_Tb.CLiente_ID                     " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Ramo_Tb                                Ramo_Tb  WITH (NOLOCK)                             " & vbNewLine
'        sSQL = sSQL & "    on Ramo_Tb.Ramo_ID                                       = Proposta_Tb.Ramo_ID                       " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Complementar_Tb          Proposta_Complementar_Tb  WITH (NOLOCK)            " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Complementar_Tb.Proposta_ID                  = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "   and Proposta_Complementar_Tb.Prop_Cliente_ID              = Proposta_Tb.Prop_Cliente_ID               " & vbNewLine
'        sSQL = sSQL & " Where Cliente_Tb.CPF_CNPJ                                   = '" & iCPF & "'                            " & vbNewLine
'        sSQL = sSQL & "   and Ramo_Tb.Tp_Ramo_ID                                    = " & iTPRamo & vbNewLine
'        sSQL = sSQL & "   and Proposta_Tb.Situacao                                  <> 't'                                      " & vbNewLine
'        sSQL = sSQL & "   and proposta_tb.produto_id                                in (115,123, 222 ,223 ,1140)                " & vbNewLine
'        sSQL = ""
'        sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10420_SPS    "
        
' Alan Neves

'        sSQL = sSQL & "Insert into #tempsel_tb( " & vbCrLf
'        sSQL = sSQL & "       marca                    " & vbCrLf
'        sSQL = sSQL & "     , apolice_id               " & vbCrLf
'        sSQL = sSQL & "     , sucursal_seguradora_id   " & vbCrLf
'        sSQL = sSQL & "     , seguradora_cod_susep     " & vbCrLf
'        sSQL = sSQL & "     , ramo_id                  " & vbCrLf
'        sSQL = sSQL & "     , nomeramo                 " & vbCrLf
'        sSQL = sSQL & "     , proposta_id              " & vbCrLf
'        sSQL = sSQL & "     , numendosso               " & vbCrLf
'        sSQL = sSQL & "     , sitproposta              " & vbCrLf
'        sSQL = sSQL & "     , produto_id               " & vbCrLf
'        sSQL = sSQL & "     , nomeproduto              " & vbCrLf
'        sSQL = sSQL & "     , cliente_id               " & vbCrLf
'        sSQL = sSQL & "     , nomecliente              " & vbCrLf
'        sSQL = sSQL & "     , tipocomponente           " & vbCrLf
'        sSQL = sSQL & "     , sinistro_id              " & vbCrLf
'        sSQL = sSQL & "     ) " & vbCrLf
'        sSQL = sSQL & "SELECT  DISTINCT                                                                             " & vbNewLine
'        sSQL = sSQL & "       'n'                                                                                   " & vbNewLine
'        sSQL = sSQL & "     , proposta_adesao_tb.apolice_id                                                         " & vbNewLine
'        sSQL = sSQL & "     , proposta_adesao_tb.sucursal_seguradora_id                                             " & vbNewLine
'        sSQL = sSQL & "     , proposta_adesao_tb.seguradora_cod_susep                                               " & vbNewLine
'        sSQL = sSQL & "     , ramo_tb.ramo_id                                                                       " & vbNewLine
'        sSQL = sSQL & "     , ramo_tb.nome                                                                          " & vbNewLine
'        sSQL = sSQL & "     , proposta_tb.proposta_id                                                               " & vbNewLine
'        sSQL = sSQL & "     , proposta_tb.num_endosso                                                               " & vbNewLine
'        sSQL = sSQL & "     , proposta_tb.situacao                                                                  " & vbNewLine
'        sSQL = sSQL & "     , produto_tb.produto_id                                                                 " & vbNewLine
'        sSQL = sSQL & "     , produto_tb.nome                                                                       " & vbNewLine
'        sSQL = sSQL & "     , cliente_tb.cliente_id                                                                 " & vbNewLine
'        sSQL = sSQL & "     , cliente_tb.nome                                                                       " & vbNewLine
'        sSQL = sSQL & "     , 'T'                                                                                   " & vbNewLine
'        sSQL = sSQL & "     , null                                                                                  " & vbNewLine
'        sSQL = sSQL & "  FROM ( ( pessoa_fisica_tb  WITH (NOLOCK)                                                     " & vbNewLine
'        sSQL = sSQL & " INNER JOIN cliente_tb  WITH (NOLOCK)                                                          " & vbNewLine
'        sSQL = sSQL & "    ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id                                " & vbNewLine
'        sSQL = sSQL & "          )                                                                                  " & vbNewLine
'        sSQL = sSQL & " INNER JOIN ( proposta_adesao_tb  WITH (NOLOCK)                                                " & vbNewLine
'        sSQL = sSQL & "               INNER JOIN ( proposta_tb  WITH (NOLOCK)                                         " & vbNewLine
'        sSQL = sSQL & "                            INNER JOIN produto_tb  WITH (NOLOCK)                               " & vbNewLine
'        sSQL = sSQL & "                            ON proposta_tb.produto_id = produto_tb.produto_id                " & vbNewLine
'        sSQL = sSQL & "                       )                                                                     " & vbNewLine
'        sSQL = sSQL & "                  ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id                " & vbNewLine
'        sSQL = sSQL & "          )                                                                                  " & vbNewLine
'        sSQL = sSQL & " ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                                      " & vbNewLine
'        sSQL = sSQL & "       )                                                                                     " & vbNewLine
'        sSQL = sSQL & " INNER JOIN ramo_tb  WITH (NOLOCK)                                                             " & vbNewLine
'        sSQL = sSQL & "    ON proposta_adesao_tb.ramo_id = ramo_tb.ramo_id                                          " & vbNewLine
'        sSQL = sSQL & "   AND ramo_tb.tp_ramo_id                                                                    = " & iTPRamo
'        sSQL = sSQL & " WHERE pessoa_fisica_tb.cpf                                                                  = '" & iCPF & "'"
'        sSQL = sSQL & " UNION ALL                                                                                   " & vbNewLine
'        sSQL = sSQL & "SELECT DISTINCT                                                                              " & vbNewLine
'        sSQL = sSQL & "        'n'                                                                                  " & vbNewLine
'        sSQL = sSQL & "     , proposta_adesao_tb.apolice_id                                                         " & vbNewLine
'        sSQL = sSQL & "     , proposta_adesao_tb.sucursal_seguradora_id                                             " & vbNewLine
'        sSQL = sSQL & "     , proposta_adesao_tb.seguradora_cod_susep                                               " & vbNewLine
'        sSQL = sSQL & "     , ramo_tb.ramo_id                                                                       " & vbNewLine
'        sSQL = sSQL & "     , ramo_tb.nome                                                                          " & vbNewLine
'        sSQL = sSQL & "     , proposta_tb.proposta_id                                                               " & vbNewLine
'        sSQL = sSQL & "     , proposta_tb.num_endosso                                                               " & vbNewLine
'        sSQL = sSQL & "     , proposta_tb.situacao                                                                  " & vbNewLine
'        sSQL = sSQL & "     , produto_tb.produto_id                                                                 " & vbNewLine
'        sSQL = sSQL & "     , produto_tb.nome                                                                       " & vbNewLine
'        sSQL = sSQL & "     , cliente_tb.cliente_id                                                                 " & vbNewLine
'        sSQL = sSQL & "     , cliente_tb.nome                                                                       " & vbNewLine
'        sSQL = sSQL & "     , 'C'                                                                                   " & vbNewLine
'        sSQL = sSQL & "     , null                                                                                  " & vbNewLine
'        sSQL = sSQL & "  FROM ( ( pessoa_fisica_tb  WITH (NOLOCK)                                                     " & vbNewLine
'        sSQL = sSQL & "  INNER JOIN cliente_tb  WITH (NOLOCK)                                                         " & vbNewLine
'        sSQL = sSQL & "     ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id                               " & vbNewLine
'        sSQL = sSQL & "         )                                                                                   " & vbNewLine
'        sSQL = sSQL & "  INNER JOIN ( proposta_complementar_tb  WITH (NOLOCK)                                         " & vbNewLine
'        sSQL = sSQL & "               INNER JOIN ( proposta_adesao_tb  WITH (NOLOCK)                                  " & vbNewLine
'        sSQL = sSQL & "                            INNER JOIN ( proposta_tb  WITH (NOLOCK)                            " & vbNewLine
'        sSQL = sSQL & "                                         INNER JOIN produto_tb  WITH (NOLOCK)                  " & vbNewLine
'        sSQL = sSQL & "                                         ON proposta_tb.produto_id = produto_tb.produto_id   " & vbNewLine
'        sSQL = sSQL & "                                       )                                                     " & vbNewLine
'        sSQL = sSQL & "                            ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id      " & vbNewLine
'        sSQL = sSQL & "                                )                                                            " & vbNewLine
'        sSQL = sSQL & "                  ON proposta_complementar_tb.proposta_id = proposta_adesao_tb.proposta_id   " & vbNewLine
'        sSQL = sSQL & "          )                                                                                  " & vbNewLine
'        sSQL = sSQL & "  ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id                                     " & vbNewLine
'        sSQL = sSQL & "      )                                                                                      " & vbNewLine
'        sSQL = sSQL & "  INNER JOIN ramo_tb  WITH (NOLOCK)                                                            " & vbNewLine
'        sSQL = sSQL & "     ON proposta_adesao_tb.ramo_id = ramo_tb.ramo_id                                         " & vbNewLine
'        sSQL = sSQL & "   AND ramo_tb.tp_ramo_id                                                                    = " & iTPRamo
'        sSQL = sSQL & " WHERE pessoa_fisica_tb.cpf                                                                  = '" & iCPF & "'"

' Alan Neves
'        Conexao_ExecutarSQL gsSIGLASISTEMA _
'                          , glAmbiente_id _
'                          , App.Title _
'                          , App.FileDescription _
'                          , sSQL _
'                          , lConexaoLocal _
'                          , False
'
'    Else
'        sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf
'        sSQL = sSQL & " exec seguros_db.dbo.SEGS10356_SPS '" & iCPF & "'," & iTPRamo
'    End If
    
    
    sSQL = ""
    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10898_SPS '" & iCPF & "','" & iTPRamo & "'" & ", '" & sOperacaoCosseguro & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'" & vbNewLine
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)

    
    Exit Function


TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_CPF_RamoVida", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Sub ConsultaSinistroPor_CPF_RamoRural(sCPF As String, iTPRamo As Byte, sVariacao As String)
'Private Sub ConsultaSinistroPor_CPF_RamoRural(dSinistro_ID As String, iTPRamo As Byte, sVariacao As String)    'AKIO.OKUNO
    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
    sSQL = ""
    
    'Alan Neves - 14/02/2013
'    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'        sSQL = sSQL & "Insert into #TempSel_Tb                                                                          " & vbNewLine
'        sSQL = sSQL & "     ( Proposta_ID )                                                                             " & vbNewLine
'        sSQL = sSQL & "Select Proposta_Tb.Proposta_ID                                                                   " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK)                         " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Ramo_Tb                        Ramo_Tb  WITH (NOLOCK)                             " & vbNewLine
'        sSQL = sSQL & "    on Ramo_Tb.Ramo_ID                               = Proposta_Tb.Ramo_ID                       " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Adesao_Tb        Proposta_Adesao_Tb  WITH (NOLOCK)                  " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Adesao_Tb.Proposta_ID                = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Fechada_Tb       Proposta_Fechada_Tb  WITH (NOLOCK)                 " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Fechada_Tb.Proposta_ID               = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Apolice_Tb                Apolice_Tb  WITH (NOLOCK)                          " & vbNewLine
'        sSQL = sSQL & "    on Apolice_Tb.Proposta_ID                        = Proposta_Fechada_Tb.Proposta_ID           " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Cliente_Tb                     Cliente_Tb  WITH (NOLOCK)                          " & vbNewLine
'        sSQL = sSQL & "    on CLiente_Tb.Cliente_ID                         = Proposta_Tb.Prop_Cliente_ID               " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Pessoa_Fisica_Tb               Pessoa_Fisica_Tb  WITH (NOLOCK)                    " & vbNewLine
'        sSQL = sSQL & "    on Pessoa_Fisica_Tb.PF_Cliente_ID                = Cliente_Tb.CLiente_ID                     " & vbNewLine
'''        'MATHAYDE
'''        sSQL = sSQL & " Where Ramo_Tb.Tp_Ramo_ID                            = " & iTPRamo & vbNewLine
'''        sSQL = sSQL & "   and Pessoa_Fisica_Tb.CPF                          = '" & sCPF & "'                            " & vbNewLine
'''        sSQL = sSQL & "   and Proposta_Tb.Produto_ID                        " & IIf(sVariacao = "RE", "not", "") & " in (126, 155, 156)                      " & vbNewLine
'''        sSQL = sSQL & "   and Proposta_Tb.Situacao                          <> 't'" & vbNewLine
'''        sSQL = sSQL & "   and " & IIf(sVariacao = "RE", "not", "") & " (Proposta_Tb.PRoduto_ID = 400 and Proposta_Tb.Ramo_ID = 63)                           " & vbNewLine
'        sSQL = sSQL & "   Where Proposta_Tb.Situacao                          <> 't'" & vbNewLine
'        sSQL = sSQL & "     and Pessoa_Fisica_Tb.CPF                          = '" & sCPF & "'                            " & vbNewLine
''AKIO.OKUNO - INICIO - 26/09/2012
''        sSQL = sSQL & "     and  ((Proposta_Tb.produto_id in (126,155,156,1152,1204) and  ramo_tb.tp_ramo_id =  " & iTPRamo & ")" & vbNewLine
''        sSQL = sSQL & "      or (Proposta_Tb.produto_id =  400 and ramo_tb.ramo_id = 63)" & vbNewLine
''        sSQL = sSQL & "      or  Proposta_Tb.produto_id =  14 )" & vbNewLine
'        sSQL = sSQL & "     and  ((Proposta_Tb.produto_id " & IIf(sVariacao = "RE", "not", "") & " in (126,155,156,1152,1204) and  ramo_tb.tp_ramo_id =  " & iTPRamo & ")" & vbNewLine
'        sSQL = sSQL & "      or " & IIf(sVariacao = "RE", "not", "") & " (Proposta_Tb.produto_id =  400 and ramo_tb.ramo_id = 63)" & vbNewLine
'        sSQL = sSQL & "      " & IIf(sVariacao = "RE", "", "or  Proposta_Tb.produto_id =  14 ") & ")" & vbNewLine
''AKIO.OKUNO - FIM - 26/09/2012
'
'
'        Conexao_ExecutarSQL gsSIGLASISTEMA _
'                          , glAmbiente_id _
'                          , App.Title _
'                          , App.FileDescription _
'                          , sSQL _
'                          , lConexaoLocal _
'                          , False
'
'        sSQL = ""
'        Select Case bytTipoRamo
'            Case bytTipoRamo_RE
'                sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10421_SPS    "
'            Case bytTipoRamo_Rural
'                sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10422_SPS    "
'        End Select
'    Else
'        sSQL = sSQL & "Insert into #tempsel_tb (                                " & vbNewLine
'        sSQL = sSQL & "       marca                                             " & vbNewLine
'        sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
'        sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
'        sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
'        sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
'        sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
'        sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
'        sSQL = sSQL & "     , numendosso                                        " & vbNewLine
'        sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
'        sSQL = sSQL & "     , produto_id                                        " & vbNewLine
'        sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
'        sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
'        sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
'        sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
'        sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
'        sSQL = sSQL & "     , situacao                                          " & vbNewLine
'        sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
'        sSQL = sSQL & "  , situacao_evento                                      " & vbNewLine
'        sSQL = sSQL & "  , sit_evento_desc                                      " & vbNewLine
'        sSQL = sSQL & "  , Sucursal_Nome                                        " & vbNewLine
'        sSQL = sSQL & "  , Endosso                                              " & vbNewLine
'        sSQL = sSQL & "  , sinistro_id_lider                                    " & vbNewLine
'        sSQL = sSQL & "  , processa_reintegracao_is                             " & vbNewLine
'        sSQL = sSQL & "  , Dt_Ocorrencia_Sinistro                               " & vbNewLine 'cristovao.rodrigues 08/11/2012
'        sSQL = sSQL & "     )                                                   " & vbNewLine
'        sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10368_SPS " & iTPRamo & ", '" & sCPF & "','" & sVariacao & "'" & vbNewLine
'    End If

    sSQL = ""
'    sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbNewLine
'    sSQL = sSQL & "       marca                                             " & vbNewLine
'    sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
'    sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
'    sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
'    sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
'    sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
'    sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
'    sSQL = sSQL & "     , numendosso                                        " & vbNewLine
'    sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
'    sSQL = sSQL & "     , produto_id                                        " & vbNewLine
'    sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
'    sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
'    sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
'    sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
'    sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
'    sSQL = sSQL & "     , situacao                                          " & vbNewLine
'    sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
'    sSQL = sSQL & "     , situacao_evento                                      " & vbNewLine
'    sSQL = sSQL & "     , sit_evento_desc                                      " & vbNewLine
'    sSQL = sSQL & "     , Sucursal_Nome                                        " & vbNewLine
'    sSQL = sSQL & "     , Endosso                                              " & vbNewLine
'    sSQL = sSQL & "     , sinistro_id_lider                                    " & vbNewLine
'    sSQL = sSQL & "     , processa_reintegracao_is                             " & vbNewLine
'    sSQL = sSQL & "     , historico                                            " & vbNewLine
'    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbNewLine 'cristovao.rodrigues 08/11/2012
'    sSQL = sSQL & "     , Tp_Emissao                                           " & vbNewLine 'cristovao.rodrigues 08/11/2012
'    sSQL = sSQL & "     )                                                   " & vbNewLine

'    Alan Neves - 14/02/2013
    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10897_SPS '" & sCPF & "','" & iTPRamo & "'" & ", '" & sVariacao & "','" & sOperacaoCosseguro & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "','" & bytTipoRamo & "'" & vbNewLine
    
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
    
    Exit Sub

Erro:
    Call TrataErroGeral("ConsultaSinistroPor_CPF_RamoRural", Me.name)
    Call FinalizarAplicacao
    
End Sub

'cristovao.rodrigues 15/08/2012
Private Function ConsultaSinistroPor_Nome(sNome As String, bTP_Ramo As Byte)

Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_Nome_RamoVida sNome, bTP_Ramo
        Case bytTipoRamo_RE
            ConsultaSinistroPor_Nome_RamoRural sNome, bTP_Ramo, "RE"
        Case bytTipoRamo_Rural
            ConsultaSinistroPor_Nome_RamoRural sNome, 2, "RU"
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_Nome", Me.name)

End Function

Private Function ConsultaSinistroPor_Nome_RamoVida(sNome As String, iTPRamo As Byte)

    Dim sSQL As String
    
    On Error GoTo TrataErro
    
    sSQL = ""
    'Alan Neves - MP - 24/02/2013
'    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'        sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Cliente'),0) >0" & vbNewLine
'        sSQL = sSQL & " BEGIN" & vbNewLine
'        sSQL = sSQL & "     DROP TABLE #Cliente" & vbNewLine
'        sSQL = sSQL & " END" & vbNewLine
'        sSQL = sSQL & "Create Table #Cliente                                                                " & vbNewLine
'        sSQL = sSQL & "           ( Cliente_ID                              int                             " & vbNewLine
'        sSQL = sSQL & "           , Nome                                    VarChar(60)                     " & vbNewLine
'        sSQL = sSQL & "           )                                                                         " & vbNewLine
'        sSQL = sSQL & "Create Unique NonClustered Index UK001_#Cliente on #Cliente (Cliente_ID)             " & vbNewLine
'        sSQL = sSQL & "Insert into #Cliente                                                                 " & vbNewLine
'        sSQL = sSQL & "          ( Cliente_ID                                                               " & vbNewLine
'        sSQL = sSQL & "          , Nome                                                                     " & vbNewLine
'        sSQL = sSQL & "          )                                                                          " & vbNewLine
'        sSQL = sSQL & "Select Cliente_ID                                                                    " & vbNewLine
'        sSQL = sSQL & "     , Nome                                                                          " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Cliente_Tb                     Cliente_Tb  WITH (NOLOCK)              " & vbNewLine
'        sSQL = sSQL & " Where Nome                                          Like '" & sNome & "%'           " & vbNewLine
'
'        sSQL = sSQL & "Insert into #TempSel_Tb                                                                          " & vbNewLine
'        sSQL = sSQL & "     ( Proposta_ID )                                                                             " & vbNewLine
'        sSQL = sSQL & "Select Proposta_Tb.Proposta_ID                                                                   " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK)                         " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Ramo_Tb                        Ramo_Tb  WITH (NOLOCK)                             " & vbNewLine
'        sSQL = sSQL & "    on Ramo_Tb.Ramo_ID                               = Proposta_Tb.Ramo_ID                       " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Adesao_Tb        Proposta_Adesao_Tb  WITH (NOLOCK)                  " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Adesao_Tb.Proposta_ID                = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Fechada_Tb       Proposta_Fechada_Tb  WITH (NOLOCK)                 " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Fechada_Tb.Proposta_ID               = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Apolice_Tb                Apolice_Tb  WITH (NOLOCK)                          " & vbNewLine
'        sSQL = sSQL & "    on Apolice_Tb.Proposta_ID                        = Proposta_Fechada_Tb.Proposta_ID           " & vbNewLine
'        sSQL = sSQL & "  Join #Cliente                                                                                  " & vbNewLine
'        sSQL = sSQL & "    on #Cliente.Cliente_ID                           = Proposta_Tb.Prop_Cliente_ID               " & vbNewLine
'        sSQL = sSQL & " Where Ramo_Tb.Tp_Ramo_ID                            = " & iTPRamo & vbNewLine
'        sSQL = sSQL & "   and Proposta_Tb.Situacao                          <> 't'" & vbNewLine
'
'        Conexao_ExecutarSQL gsSIGLASISTEMA _
'                          , glAmbiente_id _
'                          , App.Title _
'                          , App.FileDescription _
'                          , sSQL _
'                          , lConexaoLocal _
'                          , False
'
'        sSQL = ""
'        sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10420_SPS    "
'    Else
'        'Alan Neves - 19/02/2013
'        ' sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf
'
'        'Alan Neves - 19/02/2013
'        sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
'        sSQL = sSQL & "       marca                                             " & vbCrLf
'        sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
'        sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
'        sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
'        sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
'        sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
'        sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
'        sSQL = sSQL & "     , numendosso                                        " & vbCrLf
'        sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
'        sSQL = sSQL & "     , produto_id                                        " & vbCrLf
'        sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
'        sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
'        sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
'        sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
'        sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
'        sSQL = sSQL & "     , situacao                                          " & vbCrLf
'        sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
'        sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
'        sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
'        sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
'        sSQL = sSQL & "     , Endosso                                              " & vbCrLf
'        sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
'        sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
'        sSQL = sSQL & "     , historico                                            " & vbCrLf
'        sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
'        sSQL = sSQL & "     )                                                   " & vbCrLf
'
'        sSQL = sSQL & " exec seguros_db.dbo.SEGS10357_SPS '" & sNome & "'," & iTPRamo
'    End If


    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10914_SPS '" & sNome & "','" & iTPRamo & "', '" & sOperacaoCosseguro & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'" & vbNewLine
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_Nome_RamoVida", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Sub ConsultaSinistroPor_Nome_RamoRural(sNome As String, iTPRamo As Byte, sVariacao As String)
    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
  
    'Alan Neves - MP - 24/02/2013
    sSQL = ""
'    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'        sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Cliente'),0) >0" & vbNewLine
'        sSQL = sSQL & " BEGIN" & vbNewLine
'        sSQL = sSQL & "     DROP TABLE #Cliente" & vbNewLine
'        sSQL = sSQL & " END" & vbNewLine
'        sSQL = sSQL & "Create Table #Cliente                                                                " & vbNewLine
'        sSQL = sSQL & "           ( Cliente_ID                              int                             " & vbNewLine
'        sSQL = sSQL & "           , Nome                                    VarChar(60)                     " & vbNewLine
'        sSQL = sSQL & "           )                                                                         " & vbNewLine
'        sSQL = sSQL & "Create Unique NonClustered Index UK001_#Cliente on #Cliente (Cliente_ID)             " & vbNewLine
'        sSQL = sSQL & "Insert into #Cliente                                                                 " & vbNewLine
'        sSQL = sSQL & "          ( Cliente_ID                                                               " & vbNewLine
'        sSQL = sSQL & "          , Nome                                                                     " & vbNewLine
'        sSQL = sSQL & "          )                                                                          " & vbNewLine
'        sSQL = sSQL & "Select Cliente_ID                                                                    " & vbNewLine
'        sSQL = sSQL & "     , Nome                                                                          " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Cliente_Tb                     Cliente_Tb  WITH (NOLOCK)              " & vbNewLine
'        sSQL = sSQL & " Where Nome                                          Like '" & sNome & "%'           " & vbNewLine
'
'        sSQL = sSQL & "Insert into #TempSel_Tb                                                                          " & vbNewLine
'        sSQL = sSQL & "     ( Proposta_ID )                                                                             " & vbNewLine
'        sSQL = sSQL & "Select Proposta_Tb.Proposta_ID                                                                   " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK)                         " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Ramo_Tb                        Ramo_Tb  WITH (NOLOCK)                             " & vbNewLine
'        sSQL = sSQL & "    on Ramo_Tb.Ramo_ID                               = Proposta_Tb.Ramo_ID                       " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Adesao_Tb        Proposta_Adesao_Tb  WITH (NOLOCK)                  " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Adesao_Tb.Proposta_ID                = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Fechada_Tb       Proposta_Fechada_Tb  WITH (NOLOCK)                 " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Fechada_Tb.Proposta_ID               = Proposta_Tb.Proposta_ID                   " & vbNewLine
'        sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Apolice_Tb                Apolice_Tb  WITH (NOLOCK)                          " & vbNewLine
'        sSQL = sSQL & "    on Apolice_Tb.Proposta_ID                        = Proposta_Fechada_Tb.Proposta_ID           " & vbNewLine
'        sSQL = sSQL & "  Join #Cliente                                                                                  " & vbNewLine
'        sSQL = sSQL & "    on #Cliente.Cliente_ID                           = Proposta_Tb.Prop_Cliente_ID               " & vbNewLine
'''        'MATHAYDE
'''        sSQL = sSQL & " Where Ramo_Tb.Tp_Ramo_ID                            = " & iTPRamo & vbNewLine
'''        sSQL = sSQL & "   and Proposta_Tb.Produto_ID                        " & IIf(sVariacao = "RE", "not", "") & " in (126, 155, 156)                      " & vbNewLine
'''        sSQL = sSQL & "   and Proposta_Tb.Situacao                          <> 't'" & vbNewLine
'''        sSQL = sSQL & "   and " & IIf(sVariacao = "RE", "not", "") & " (Proposta_Tb.PRoduto_ID = 400 and Proposta_Tb.Ramo_ID = 63)                           " & vbNewLine
'        sSQL = sSQL & "   Where Proposta_Tb.Situacao                          <> 't'" & vbNewLine
''AKIO.OKUNO - INICIO - 26/09/2012
''        sSQL = sSQL & "     and  ((Proposta_Tb.produto_id in (126,155,156,1152,1204) and  ramo_tb.tp_ramo_id =  " & iTPRamo & ")" & vbNewLine
''        sSQL = sSQL & "      or (Proposta_Tb.produto_id =  400 and ramo_tb.ramo_id = 63)" & vbNewLine
''        sSQL = sSQL & "      or  Proposta_Tb.produto_id =  14 )" & vbNewLine
'        sSQL = sSQL & "     and  ((Proposta_Tb.produto_id " & IIf(sVariacao = "RE", "not", "") & " in (126,155,156,1152,1204) and  ramo_tb.tp_ramo_id =  " & iTPRamo & ")" & vbNewLine
'        sSQL = sSQL & "      or " & IIf(sVariacao = "RE", "not", "") & " (Proposta_Tb.produto_id =  400 and ramo_tb.ramo_id = 63)" & vbNewLine
'        sSQL = sSQL & "      " & IIf(sVariacao = "RE", "", "or  Proposta_Tb.produto_id =  14 ") & ")" & vbNewLine
''AKIO.OKUNO - FIM - 26/09/2012
'
'
'        Conexao_ExecutarSQL gsSIGLASISTEMA _
'                          , glAmbiente_id _
'                          , App.Title _
'                          , App.FileDescription _
'                          , sSQL _
'                          , lConexaoLocal _
'                          , False
'
'        sSQL = ""
'        Select Case bytTipoRamo
'            Case bytTipoRamo_RE
'                sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10421_SPS    "
'            Case bytTipoRamo_Rural
'                sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10422_SPS    "
'        End Select
'    Else
'         sSQL = sSQL & "Insert into #tempsel_tb (                                " & vbNewLine
'         sSQL = sSQL & "       marca                                             " & vbNewLine
'         sSQL = sSQL & "     , apolice_id                                        " & vbNewLine
'         sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbNewLine
'         sSQL = sSQL & "     , seguradora_cod_susep                              " & vbNewLine
'         sSQL = sSQL & "     , ramo_id                                           " & vbNewLine
'         sSQL = sSQL & "     , nomeramo                                          " & vbNewLine
'         sSQL = sSQL & "     , proposta_id                                       " & vbNewLine
'         sSQL = sSQL & "     , numendosso                                        " & vbNewLine
'         sSQL = sSQL & "     , sitproposta                                       " & vbNewLine
'         sSQL = sSQL & "     , produto_id                                        " & vbNewLine
'         sSQL = sSQL & "     , nomeproduto                                       " & vbNewLine
'         sSQL = sSQL & "     , cliente_id                                        " & vbNewLine
'         sSQL = sSQL & "     , nomecliente                                       " & vbNewLine
'         sSQL = sSQL & "     , tipoComponente                                    " & vbNewLine
'         sSQL = sSQL & "     , sinistro_id                                       " & vbNewLine
'         sSQL = sSQL & "     , situacao                                          " & vbNewLine
'         sSQL = sSQL & "     , situacao_desc                                     " & vbNewLine
'         sSQL = sSQL & "  , situacao_evento                                      " & vbNewLine
'         sSQL = sSQL & "  , sit_evento_desc                                      " & vbNewLine
'         sSQL = sSQL & "  , Sucursal_Nome                                        " & vbNewLine
'         sSQL = sSQL & "  , Endosso                                              " & vbNewLine
'         sSQL = sSQL & "  , sinistro_id_lider                                    " & vbNewLine
'         sSQL = sSQL & "  , processa_reintegracao_is                             " & vbNewLine
'         sSQL = sSQL & "  , Dt_Ocorrencia_Sinistro                               " & vbNewLine 'cristovao.rodrigues 08/11/2012
'         sSQL = sSQL & "     )                                                   " & vbNewLine
'         sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10369_SPS " & iTPRamo & ", '" & sNome & "','" & sVariacao & "'" & vbNewLine
'
'    End If

    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10913_SPS '" & sNome & "','" & iTPRamo & "'" & ", '" & sVariacao & "','" & sOperacaoCosseguro & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "','" & bytTipoRamo & "'" & vbNewLine
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
    
    Exit Sub
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_Nome_RamoRural", Me.name)
    Call FinalizarAplicacao
    
End Sub

'cristovao.rodrigues 15/08/2012
Private Function ConsultaSinistroPor_CertificadoSC(iCertificadoSC As String, bTP_Ramo As Byte)

Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_CertificadoSC_RamoVida iCertificadoSC, bTP_Ramo
        Case bytTipoRamo_RE
            'ConsultaSinistroPor_CertificadoSC_RamoRE iCertificadoSC
        Case bytTipoRamo_Rural
            'ConsultaSinistroPor_CertificadoSC_RamoRural iCertificadoSC, 2 'bTP_Ramo ' SC somente VIDA
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_CertificadoSC", Me.name)

End Function


Private Function ConsultaSinistroPor_CertificadoSC_RamoVida(iCertificadoSC As String, iTPRamo As Byte)

    Dim sSQL As String
    Dim rsRecordSet As ADODB.Recordset
    Dim SinistroConsulta As String
    Dim Retorno As Boolean
    Dim sCPF As String
    
    On Error GoTo TrataErro
    
    sSQL = ""
    sSQL = sSQL & " select                                                      " & vbCrLf
    sSQL = sSQL & "     sinistro_tb.sinistro_id,                                    " & vbCrLf
    sSQL = sSQL & "     sinistro_tb.proposta_id,                                    " & vbCrLf
    sSQL = sSQL & "     sinistro_vida_tb.cpf                                        " & vbCrLf
    sSQL = sSQL & "     --sinistro_tb.cliente_id,                                     " & vbCrLf
    sSQL = sSQL & "     --sinistro_tb.apolice_id ,                                    " & vbCrLf
    sSQL = sSQL & " From sinistro_legado_tb WITH (NOLOCK)                              " & vbCrLf
    sSQL = sSQL & " inner join  sinistro_tb  WITH (NOLOCK)                             " & vbCrLf
    sSQL = sSQL & "     on alianca_cd_sinistro = sinistro_id                        " & vbCrLf
    sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                           " & vbCrLf
    sSQL = sSQL & "     on sinistro_tb.cliente_id = cliente_tb.cliente_id           " & vbCrLf
    sSQL = sSQL & " inner join sinistro_vida_tb  WITH (NOLOCK)                     " & vbCrLf
    sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id   " & vbCrLf
    sSQL = sSQL & " Where                                                       " & vbCrLf
    sSQL = sSQL & "     santa_catarina_cd_certificado = " & iCertificadoSC & vbCrLf
    sSQL = sSQL & "     and sinistro_tb.situacao <> 7 "
    
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
                         
    If Not rsRecordSet.EOF Then
    
        sCPF = IIf(IsNull(rsRecordSet("CPF")), "", rsRecordSet("cpf"))
        
        sSQL = ""
        sSQL = sSQL & " select                                                      " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id,                                    " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.proposta_id,                                    " & vbCrLf
        sSQL = sSQL & "     sinistro_vida_tb.cpf                                        " & vbCrLf
        sSQL = sSQL & "     --sinistro_tb.cliente_id,                                     " & vbCrLf
        sSQL = sSQL & "     --sinistro_tb.apolice_id ,                                    " & vbCrLf
        sSQL = sSQL & " From sinistro_legado_tb WITH (NOLOCK)                              " & vbCrLf
        sSQL = sSQL & " inner join  sinistro_tb  WITH (NOLOCK)                             " & vbCrLf
        sSQL = sSQL & "     on alianca_cd_sinistro = sinistro_id                        " & vbCrLf
        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                           " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.cliente_id = cliente_tb.cliente_id           " & vbCrLf
        sSQL = sSQL & " inner join sinistro_vida_tb  WITH (NOLOCK)                     " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id   " & vbCrLf
        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                  " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.proposta_id = sinistro_tb.proposta_id        " & vbCrLf
        sSQL = sSQL & " Where                                                       " & vbCrLf
        sSQL = sSQL & "     santa_catarina_cd_certificado = " & iCertificadoSC & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.situacao <> 7 " & vbCrLf
        sSQL = sSQL & "   and proposta_tb.produto_id in (115,123, 222 ,223 ,1140) "
        
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, _
                                              True)
        
    
        If rsRecordSet.RecordCount > 0 Or sCPF = "" Then
        
            sSQL = ""
            ' Alan Neves - 19/02/2013
            '            sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf

            'Alan Neves - 19/02/2013
            sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
            sSQL = sSQL & "       marca                                             " & vbCrLf
            sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
            sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
            sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
            sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
            sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
            sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
            sSQL = sSQL & "     , numendosso                                        " & vbCrLf
            sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
            sSQL = sSQL & "     , produto_id                                        " & vbCrLf
            sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
            sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
            sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
            sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
            sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
            sSQL = sSQL & "     , situacao                                          " & vbCrLf
            sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
            sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
            sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
            sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
            sSQL = sSQL & "     , Endosso                                              " & vbCrLf
            sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
            sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
            sSQL = sSQL & "     , historico                                            " & vbCrLf
            sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
            sSQL = sSQL & "     )                                                   " & vbCrLf
            sSQL = sSQL & " exec seguros_db.dbo.SEGS10350_SPS " & rsRecordSet("sinistro_id") & "," & iTPRamo
    
            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     sSQL, _
                                     lConexaoLocal, _
                                     False)
                         
        Else
            Retorno = ConsultaSinistroPor_CPF_RamoVida(sCPF, iTPRamo)
        End If
        
    End If
    
    rsRecordSet.Close
    Set rsRecordSet = Nothing
    
    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_CertificadoSC_RamoVida", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Function ConsultaSinistroPor_SinistroSC(iSinistroSC As String, bTP_Ramo As Byte)

Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_SinistroSC_RamoVida iSinistroSC, bTP_Ramo
        Case bytTipoRamo_RE
            'ConsultaSinistroPor_SinistroSC_RamoRE iSinistroSC, btp_ramo, "RE"
        Case bytTipoRamo_Rural
            'ConsultaSinistroPor_SinistroSC_RamoRural iSinistroSC, 2, "RU" 'bTP_Ramo ' SC somente VIDA
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_SinistroSC", Me.name)

End Function

Private Function ConsultaSinistroPor_SinistroSC_RamoVida(iSinistroSC As String, iTPRamo As Byte)

    Dim sSQL As String
    Dim rs As ADODB.Recordset
    Dim SinistroConsulta As String
    Dim sCPF As String
    
    On Error GoTo TrataErro
    
    sSQL = ""
    sSQL = sSQL & " select                                                          " & vbCrLf
    sSQL = sSQL & "     sinistro_tb.sinistro_id,                                    " & vbCrLf
    sSQL = sSQL & "     sinistro_tb.proposta_id,                                    " & vbCrLf
    sSQL = sSQL & "     sinistro_vida_tb.cpf                                       " & vbCrLf
    sSQL = sSQL & "     --sinistro_tb.cliente_id,                                     " & vbCrLf
    sSQL = sSQL & "     --sinistro_tb.apolice_id                                     " & vbCrLf
    sSQL = sSQL & " from sinistro_legado_tb WITH (NOLOCK)                                  " & vbCrLf
    sSQL = sSQL & " inner join  sinistro_tb  WITH (NOLOCK)                                 " & vbCrLf
    sSQL = sSQL & "     on alianca_cd_sinistro = sinistro_id                        " & vbCrLf
    sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                               " & vbCrLf
    sSQL = sSQL & "     on sinistro_tb.cliente_id = cliente_tb.cliente_id           " & vbCrLf
    sSQL = sSQL & " inner join sinistro_vida_tb  WITH (NOLOCK)                         " & vbCrLf
    sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id   " & vbCrLf
    sSQL = sSQL & " where                                                           " & vbCrLf
    sSQL = sSQL & "     santa_catarina_cd_sinistro = " & iSinistroSC & vbCrLf
    sSQL = sSQL & "     and situacao <> 7                                           "

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         lConexaoLocal, _
                         True)
                         
    If Not rs.EOF Then
    
        sCPF = IIf(IsNull(rs("CPF")), "", rs("cpf"))
        
        sSQL = ""
        sSQL = sSQL & " select                                                          " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id,                                    " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.proposta_id,                                    " & vbCrLf
        sSQL = sSQL & "     sinistro_vida_tb.cpf                                       " & vbCrLf
        sSQL = sSQL & "     --sinistro_tb.cliente_id,                                     " & vbCrLf
        sSQL = sSQL & "     --sinistro_tb.apolice_id                                     " & vbCrLf
        sSQL = sSQL & " from sinistro_legado_tb WITH (NOLOCK)                                  " & vbCrLf
        sSQL = sSQL & " inner join  sinistro_tb  WITH (NOLOCK)                                 " & vbCrLf
        sSQL = sSQL & "     on alianca_cd_sinistro = sinistro_id                        " & vbCrLf
        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                               " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.cliente_id = cliente_tb.cliente_id           " & vbCrLf
        sSQL = sSQL & " inner join sinistro_vida_tb  WITH (NOLOCK)                         " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_vida_tb.sinistro_id   " & vbCrLf
        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                  " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.proposta_id = sinistro_tb.proposta_id        " & vbCrLf
        sSQL = sSQL & " where                                                           " & vbCrLf
        sSQL = sSQL & "     santa_catarina_cd_sinistro = " & iSinistroSC & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.situacao <> 7                               " & vbCrLf
        sSQL = sSQL & "   and proposta_tb.produto_id in (115,123, 222 ,223 ,1140) "
        
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             True)
        
    
        If rs.RecordCount > 0 Or sCPF = "" Then
        
            sSQL = ""
            
            ' Alan Neves - 19/02/2013
            ' sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf

            'Alan Neves - 19/02/2013
            sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
            sSQL = sSQL & "       marca                                             " & vbCrLf
            sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
            sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
            sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
            sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
            sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
            sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
            sSQL = sSQL & "     , numendosso                                        " & vbCrLf
            sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
            sSQL = sSQL & "     , produto_id                                        " & vbCrLf
            sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
            sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
            sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
            sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
            sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
            sSQL = sSQL & "     , situacao                                          " & vbCrLf
            sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
            sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
            sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
            sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
            sSQL = sSQL & "     , Endosso                                              " & vbCrLf
            sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
            sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
            sSQL = sSQL & "     , historico                                            " & vbCrLf
            sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
            sSQL = sSQL & "     )                                                   " & vbCrLf
            
            sSQL = sSQL & " exec seguros_db.dbo.SEGS10350_SPS " & rs("sinistro_id") & "," & iTPRamo
    
            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         lConexaoLocal, _
                         False)
                         
        Else
            Retorno = ConsultaSinistroPor_CPF_RamoVida(sCPF, iTPRamo)
        End If
        
    End If
                             
    rs.Close
    Set rs = Nothing
    
    Exit Function

TrataErro:
Call TrataErroGeral("ConsultaSinistroPor_SinistroSC_RamoVida", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Function ConsultaSinistroPor_TecnicoResponsavel(iTecnicoID As Long, iSituacao As Integer, bTP_Ramo As Byte)

Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_TecnicoResponsavel_RamoVida iTecnicoID, iSituacao, bTP_Ramo
        Case bytTipoRamo_RE
            ConsultaSinistroPor_TecnicoResponsavel_RamoRural iTecnicoID, iSituacao, bTP_Ramo, "RE"
        Case bytTipoRamo_Rural
            ConsultaSinistroPor_TecnicoResponsavel_RamoRural iTecnicoID, iSituacao, 2, "RU"
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_TecnicoResponsavel", Me.name)

End Function


Private Function ConsultaSinistroPor_TecnicoResponsavel_RamoVida(iTecnicoID As Long, iSituacao As Integer, iTPRamo As Byte)

    Dim sSQL As String
    Dim rs As ADODB.Recordset
    
    On Error GoTo TrataErro
           
    sSQL = ""
    'Alan Neves - 19/02/2013
'    sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf

    'Alan Neves - 19/02/2013
    sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
    sSQL = sSQL & "       marca                                             " & vbCrLf
    sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
    sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
    sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
    sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
    sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
    sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
    sSQL = sSQL & "     , numendosso                                        " & vbCrLf
    sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
    sSQL = sSQL & "     , produto_id                                        " & vbCrLf
    sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
    sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
    sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
    sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
    sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
    sSQL = sSQL & "     , situacao                                          " & vbCrLf
    sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
    sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
    sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
    sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
    sSQL = sSQL & "     , Endosso                                              " & vbCrLf
    sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
    sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
    sSQL = sSQL & "     , historico                                            " & vbCrLf
    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
    sSQL = sSQL & "     )                                                   " & vbCrLf

    sSQL = sSQL & " exec seguros_db.dbo.SEGS10360_SPS " & iTecnicoID & "," & iSituacao & "," & iTPRamo
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         lConexaoLocal, _
                         False)
    
    
    Exit Function

TrataErro:
Call TrataErroGeral("ConsultaSinistroPor_TecnicoResponsavel_RamoVida", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Sub ConsultaSinistroPor_TecnicoResponsavel_RamoRural(iTecnicoID As Long, iSituacao As Integer, iTPRamo As Byte, sVariacao As String)
    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
    
    sSQL = ""
    'Alan Neves - 19/02/2013
'    sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf

    'Alan Neves - 19/02/2013
    sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
    sSQL = sSQL & "       marca                                             " & vbCrLf
    sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
    sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
    sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
    sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
    sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
    sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
    sSQL = sSQL & "     , numendosso                                        " & vbCrLf
    sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
    sSQL = sSQL & "     , produto_id                                        " & vbCrLf
    sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
    sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
    sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
    sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
    sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
    sSQL = sSQL & "     , situacao                                          " & vbCrLf
    sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
    sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
    sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
    sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
    sSQL = sSQL & "     , Endosso                                              " & vbCrLf
    sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
    sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
    sSQL = sSQL & "     , historico                                            " & vbCrLf
    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
    sSQL = sSQL & "     )                                                   " & vbCrLf

    sSQL = sSQL & " exec seguros_db.dbo.SEGS10372_SPS " & iTecnicoID & "," & iSituacao & "," & iTPRamo & ",'" & sVariacao & "'"
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         lConexaoLocal, _
                         False)
    
    Exit Sub
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_TecnicoResponsavel_RamoRural", Me.name)
    Call FinalizarAplicacao
    
End Sub

'cristovao.rodrigues 15/08/2012
Private Function ConsultaSinistroPor_SituacaoPendenteAprovacao(sTipoSelecao As String, sUsuario As String, bTP_Ramo As Byte)

Select Case bTP_Ramo
        Case bytTipoRamo_Vida
            ConsultaSinistroPor_SituacaoPendenteAprovacao_RamoVida sTipoSelecao, sUsuario, bTP_Ramo
        Case bytTipoRamo_RE
            ConsultaSinistroPor_SituacaoPendenteAprovacao_RamoRural sTipoSelecao, sUsuario, bTP_Ramo, "RE"
        Case bytTipoRamo_Rural
            ConsultaSinistroPor_SituacaoPendenteAprovacao_RamoRural sTipoSelecao, sUsuario, 2, "RU"
    End Select

    Exit Function

TrataErro:
    Call TrataErroGeral("ConsultaSinistroPor_SituacaoPendenteAprovacao", Me.name)

End Function

Private Function ConsultaSinistroPor_SituacaoPendenteAprovacao_RamoVida(sTipoSelecao As String, sUsuario As String, iTPRamo As Byte)

    Dim sSQL As String
    Dim rs As ADODB.Recordset
    
    On Error GoTo TrataErro
    
    If sTipoSelecao = "SinistrosPendentesAprovacao" Then
           
        'cristovao.rodrigues 11/01/2013 mudanca select passando verificar tecnico
        
        sSQL = "SELECT COUNT(DISTINCT b.sinistro_id) " & vbCrLf _
                & "FROM seguros_db.dbo.pgto_sinistro_tb a  WITH (NOLOCK)  " & vbCrLf _
                & "INNER JOIN seguros_db.dbo.sinistro_tb b  WITH (NOLOCK)  ON b.sinistro_id = a.sinistro_id  " & vbCrLf _
                & "INNER JOIN seguros_db.dbo.sinistro_tecnico_aprovador_tb c  WITH (NOLOCK)  ON b.sinistro_id = c.sinistro_id  " & vbCrLf _
                & "WHERE a.situacao_op = 'n'  " & vbCrLf _
                & "AND b.situacao <> 7  " & vbCrLf _
                & "AND c.tecnico_id = " & sUsuario & " " & vbCrLf _
                & "AND c.dt_fim_vigencia is null "
                
        ' flow 17813136 04/2013 petrauskas
        ' excluir casos de cosseguro da lista de casos pass�veis de distribui��o
        sSQL = sSQL & "AND NOT EXISTS (SELECT 1 " _
                    & "                FROM seguros_db.dbo.co_seguro_repassado_tb " _
                    & "                WHERE apolice_id = b.apolice_id " _
                    & "                  AND ramo_id=b.ramo_id) "

'        sSQL = sSQL & " select distinct tecnico_tb.tecnico_id " & vbCrLf
'        sSQL = sSQL & " from seguros_db.dbo.evento_segbr_sinistro_tb evento_segbr_sinistro_tb   WITH (NOLOCK)          " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.sinistro_tecnico_tb sinistro_tecnico_tb   WITH (NOLOCK)                    " & vbCrLf
'        sSQL = sSQL & "     on evento_segbr_sinistro_tb.sinistro_id     = sinistro_tecnico_tb.sinistro_id       " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.tecnico_tb tecnico_tb   WITH (NOLOCK)                                      " & vbCrLf
'        sSQL = sSQL & "     on tecnico_tb.tecnico_id = sinistro_tecnico_tb.tecnico_id                           " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.limite_aprovacao_pgto_tb limite_aprovacao_pgto_tb  WITH (NOLOCK)           " & vbCrLf
'        sSQL = sSQL & "     on limite_aprovacao_pgto_tb.tecnico_id      = tecnico_tb.tecnico_id                 " & vbCrLf
'        sSQL = sSQL & "     and evento_segbr_sinistro_tb.ramo_id        = limite_aprovacao_pgto_tb.ramo_id      " & vbCrLf
'        sSQL = sSQL & "     and evento_segbr_sinistro_tb.produto_id     = limite_aprovacao_pgto_tb.produto_id   " & vbCrLf
'        sSQL = sSQL & "     and evento_segbr_sinistro_tb.item_val_estimativa = limite_aprovacao_pgto_tb.item_val_estimativa " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.ramo_tb ramo_tb  WITH (NOLOCK)                                                         " & vbCrLf
'        sSQL = sSQL & "     on ramo_tb.ramo_id                          = sinistro_tecnico_tb.ramo_id                       " & vbCrLf
'        sSQL = sSQL & " join segab_db..usuario_tb usuario_tb  WITH (NOLOCK)                                                        " & vbCrLf
'        sSQL = sSQL & "     on tecnico_tb.nome                          = usuario_tb.nome                                   " & vbCrLf
'        sSQL = sSQL & " where 1 = 1                                                                                         " & vbCrLf
'        sSQL = sSQL & "     and sinistro_tecnico_tb.dt_fim_vigencia is null                                                 " & vbCrLf
'        sSQL = sSQL & "     and situacao_op = 'n'                                                                           " & vbCrLf
'        sSQL = sSQL & "     and evento_bb_id = 1151                                                                         " & vbCrLf
'        sSQL = sSQL & "     and evento_segbr_sinistro_tb.sinistro_id not in (select sinistro_id                             " & vbCrLf
'        sSQL = sSQL & "                                                from seguros_db.dbo.evento_segbr_sinistro_tb aux  WITH (NOLOCK)     " & vbCrLf
'        sSQL = sSQL & "                                               where aux.sinistro_id = evento_segbr_sinistro_tb.sinistro_id  " & vbCrLf
'        sSQL = sSQL & "                                                 and aux.num_recibo = evento_segbr_sinistro_tb.num_recibo    " & vbCrLf
'        sSQL = sSQL & "                                                 and aux.evento_bb_id in (1152, 1154))                       " & vbCrLf
'        sSQL = sSQL & "     and ativo = 's'                                         " & vbCrLf
'        sSQL = sSQL & "     and ramo_tb.tp_ramo_id = " & iTPRamo & vbCrLf
'        sSQL = sSQL & "     and tecnico_tb.ativo = 's'                              " & vbCrLf
'        sSQL = sSQL & "     and usuario_tb.cpf = '" & sUsuario & "'" & vbCrLf
        
    Else
        sSQL = ""
        sSQL = sSQL & " select total = count(*), sinistro_tecnico_tb.tecnico_id                                 " & vbCrLf
        sSQL = sSQL & " from seguros_db.dbo.evento_segbr_sinistro_tb evento_segbr_sinistro_tb   WITH (NOLOCK)          " & vbCrLf
        sSQL = sSQL & " join seguros_db.dbo.sinistro_tecnico_aprovador_tb sinistro_tecnico_tb   WITH (NOLOCK)          " & vbCrLf
        sSQL = sSQL & "     on evento_segbr_sinistro_tb.sinistro_id = sinistro_tecnico_tb.sinistro_id           " & vbCrLf
        sSQL = sSQL & " join seguros_db.dbo.tecnico_tb tecnico_tb   WITH (NOLOCK)                                      " & vbCrLf
        sSQL = sSQL & "     on tecnico_tb.tecnico_id = sinistro_tecnico_tb.tecnico_id                           " & vbCrLf
        sSQL = sSQL & " join seguros_db.dbo.limite_aprovacao_pgto_tb limite_aprovacao_pgto_tb  WITH (NOLOCK)           " & vbCrLf
        sSQL = sSQL & "     on limite_aprovacao_pgto_tb.tecnico_id = tecnico_tb.tecnico_id                      " & vbCrLf
        sSQL = sSQL & "     and evento_segbr_sinistro_tb.ramo_id = limite_aprovacao_pgto_tb.ramo_id             " & vbCrLf
        sSQL = sSQL & "     and evento_segbr_sinistro_tb.produto_id = limite_aprovacao_pgto_tb.produto_id       " & vbCrLf
        sSQL = sSQL & "     and evento_segbr_sinistro_tb.item_val_estimativa = limite_aprovacao_pgto_tb.item_val_estimativa " & vbCrLf
        sSQL = sSQL & " join seguros_db.dbo.ramo_tb ramo_tb  WITH (NOLOCK)                                             " & vbCrLf
        sSQL = sSQL & "     on ramo_tb.ramo_id = sinistro_tecnico_tb.ramo_id                                    " & vbCrLf
        sSQL = sSQL & " where 1=1                                                                               " & vbCrLf
        sSQL = sSQL & "     and sinistro_tecnico_tb.dt_fim_vigencia is null                                     " & vbCrLf
        sSQL = sSQL & "     and situacao_op = 'n'                                                               " & vbCrLf
        sSQL = sSQL & "     and evento_bb_id = 1151                                                             " & vbCrLf
        sSQL = sSQL & "     and evento_segbr_sinistro_tb.sinistro_id not in                                     " & vbCrLf
        sSQL = sSQL & "                 (select sinistro_id                                                     " & vbCrLf
        sSQL = sSQL & "                 from seguros_db.dbo.evento_segbr_sinistro_tb aux  WITH (NOLOCK)                " & vbCrLf
        sSQL = sSQL & "                 where aux.sinistro_id = evento_segbr_sinistro_tb.sinistro_id            " & vbCrLf
        sSQL = sSQL & "                     and aux.num_recibo = evento_segbr_sinistro_tb.num_recibo            " & vbCrLf
        sSQL = sSQL & "                     and aux.evento_bb_id in (1152, 1154))                               " & vbCrLf
        sSQL = sSQL & "     and ativo = 's'                                                                     " & vbCrLf
        sSQL = sSQL & "     and sinistro_tecnico_tb.tecnico_id = '" & sUsuario & "'" & vbCrLf
        sSQL = sSQL & "     and ramo_tb.tp_ramo_id = " & iTPRamo & vbCrLf
        sSQL = sSQL & " group by sinistro_tecnico_tb.tecnico_id "

    End If
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                sSQL, _
                                lConexaoLocal, _
                                True)
                                
    If Not rs.EOF Then
        If sTipoSelecao <> "SinistrosPendentesAprovacao" Then 'cristovao.rodrigues 11/01/2013 incluido if
       sUsuario = rs("tecnico_id")
        End If
    Else
       sUsuario = ""
    End If
       
    rs.Close
    Set rs = Nothing
 
            
    If sUsuario <> "" Then
    
        sSQL = ""
        ' Alan Neves - 19/02/2013
'        sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf
        
    'Alan Neves - 19/02/2013
    sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
    sSQL = sSQL & "       marca                                             " & vbCrLf
    sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
    sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
    sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
    sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
    sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
    sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
    sSQL = sSQL & "     , numendosso                                        " & vbCrLf
    sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
    sSQL = sSQL & "     , produto_id                                        " & vbCrLf
    sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
    sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
    sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
    sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
    sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
    sSQL = sSQL & "     , situacao                                          " & vbCrLf
    sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
    sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
    sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
    sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
    sSQL = sSQL & "     , Endosso                                              " & vbCrLf
    sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
    sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
    sSQL = sSQL & "     , historico                                            " & vbCrLf
    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
    sSQL = sSQL & "     )                                                   " & vbCrLf
        
        If sTipoSelecao <> "SinistrosPendentesAprovacao" Then 'cristovao.rodrigues 11/01/2013 incluido if
        sSQL = sSQL & " exec seguros_db.dbo.SEGS10361_SPS " & sUsuario & "," & iTPRamo
        Else
            sSQL = sSQL & " exec seguros_db.dbo.SEGS10780_SPS " & iTPRamo & "," & sUsuario 'cristovao.rodrigues 22/01/2013
        End If
        
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         lConexaoLocal, _
                         False)

       
    End If
   
    Exit Function

TrataErro:
Call TrataErroGeral("ConsultaSinistroPor_SituacaoPendenteAprovacao_RamoVida", Me.name)

End Function

'cristovao.rodrigues 15/08/2012
Private Function ConsultaSinistroPor_SituacaoPendenteAprovacao_RamoRural(sTipoSelecao As String, sUsuario As String, iTPRamo As Byte, sVariacao As String)
   
    Dim sSQL As String
    Dim rs As ADODB.Recordset
    
    On Error GoTo TrataErro
    
    If sTipoSelecao = "SinistrosPendentesAprovacao" Then
           
        sSQL = ""
        sSQL = sSQL & " select distinct tecnico_tb.tecnico_id " & vbCrLf
        sSQL = sSQL & " from seguros_db.dbo.evento_segbr_sinistro_tb evento_segbr_sinistro_tb   WITH (NOLOCK)          " & vbCrLf
        sSQL = sSQL & " join seguros_db.dbo.sinistro_tecnico_tb sinistro_tecnico_tb   WITH (NOLOCK)                    " & vbCrLf
        sSQL = sSQL & "     on evento_segbr_sinistro_tb.sinistro_id     = sinistro_tecnico_tb.sinistro_id       " & vbCrLf
        sSQL = sSQL & " join seguros_db.dbo.tecnico_tb tecnico_tb   WITH (NOLOCK)                                      " & vbCrLf
        sSQL = sSQL & "     on tecnico_tb.tecnico_id = sinistro_tecnico_tb.tecnico_id                           " & vbCrLf
        sSQL = sSQL & " join seguros_db.dbo.limite_aprovacao_pgto_tb limite_aprovacao_pgto_tb  WITH (NOLOCK)           " & vbCrLf
        sSQL = sSQL & "     on limite_aprovacao_pgto_tb.tecnico_id      = tecnico_tb.tecnico_id                 " & vbCrLf
        sSQL = sSQL & "     and evento_segbr_sinistro_tb.ramo_id        = limite_aprovacao_pgto_tb.ramo_id      " & vbCrLf
        sSQL = sSQL & "     and evento_segbr_sinistro_tb.produto_id     = limite_aprovacao_pgto_tb.produto_id   " & vbCrLf
        sSQL = sSQL & "     and evento_segbr_sinistro_tb.item_val_estimativa = limite_aprovacao_pgto_tb.item_val_estimativa " & vbCrLf
        sSQL = sSQL & " join seguros_db.dbo.ramo_tb ramo_tb  WITH (NOLOCK)                                                         " & vbCrLf
        sSQL = sSQL & "     on ramo_tb.ramo_id                          = sinistro_tecnico_tb.ramo_id                       " & vbCrLf
        sSQL = sSQL & " join segab_db..usuario_tb usuario_tb  WITH (NOLOCK)                                                        " & vbCrLf
        sSQL = sSQL & "     on tecnico_tb.nome                          = usuario_tb.nome                                   " & vbCrLf
        sSQL = sSQL & " where 1 = 1                                                                                         " & vbCrLf
        sSQL = sSQL & "     and sinistro_tecnico_tb.dt_fim_vigencia is null                                                 " & vbCrLf
        sSQL = sSQL & "     and situacao_op = 'n'                                                                           " & vbCrLf
        sSQL = sSQL & "     and evento_bb_id = 1151                                                                         " & vbCrLf
        sSQL = sSQL & "     and evento_segbr_sinistro_tb.sinistro_id not in (select sinistro_id                             " & vbCrLf
        sSQL = sSQL & "                                                from seguros_db.dbo.evento_segbr_sinistro_tb aux  WITH (NOLOCK)     " & vbCrLf
        sSQL = sSQL & "                                               where aux.sinistro_id = evento_segbr_sinistro_tb.sinistro_id  " & vbCrLf
        sSQL = sSQL & "                                                 and aux.num_recibo = evento_segbr_sinistro_tb.num_recibo    " & vbCrLf
        sSQL = sSQL & "                                                 and aux.evento_bb_id in (1152, 1154))                       " & vbCrLf
        sSQL = sSQL & "     and ativo = 's'                                         " & vbCrLf
        sSQL = sSQL & "     and ramo_tb.tp_ramo_id = " & iTPRamo & vbCrLf
        sSQL = sSQL & "     and tecnico_tb.ativo = 's'                              " & vbCrLf
'        sSQL = sSQL & "     and usuario_tb.cpf = '" & sUsuario & "'" & vbCrLf
        
        'petrauskas 04/2013
        'vari�vel sUsuario � preenchida com tecnico_id (vindo do combo)
        'sSQL = sSQL & "     and usuario_tb.cpf = '" & sUsuario & "'" & vbCrLf
        sSQL = sSQL & "     and tecnico_tb.tecnico_id = '" & sUsuario & "'" & vbCrLf
        
        ' flow 17813136 04/2013 petrauskas
        ' excluir casos de cosseguro da lista de casos pass�veis de distribui��o
        sSQL = sSQL & "AND NOT EXISTS (SELECT 1 " _
                    & "                FROM seguros_db.dbo.co_seguro_repassado_tb " _
                    & "                WHERE apolice_id = evento_segbr_sinistro_tb.apolice_id " _
                    & "                  AND ramo_id=evento_segbr_sinistro_tb.ramo_id) "
        
    Else
        sSQL = ""
        sSQL = sSQL & " select total = count(*), sinistro_tecnico_tb.tecnico_id                                 " & vbCrLf
        sSQL = sSQL & " from seguros_db.dbo.evento_segbr_sinistro_tb evento_segbr_sinistro_tb   WITH (NOLOCK)          " & vbCrLf
        sSQL = sSQL & " join seguros_db.dbo.sinistro_tecnico_aprovador_tb sinistro_tecnico_tb   WITH (NOLOCK)          " & vbCrLf
        sSQL = sSQL & "     on evento_segbr_sinistro_tb.sinistro_id = sinistro_tecnico_tb.sinistro_id           " & vbCrLf
        sSQL = sSQL & " join seguros_db.dbo.tecnico_tb tecnico_tb   WITH (NOLOCK)                                      " & vbCrLf
        sSQL = sSQL & "     on tecnico_tb.tecnico_id = sinistro_tecnico_tb.tecnico_id                           " & vbCrLf
        sSQL = sSQL & " join seguros_db.dbo.limite_aprovacao_pgto_tb limite_aprovacao_pgto_tb  WITH (NOLOCK)           " & vbCrLf
        sSQL = sSQL & "     on limite_aprovacao_pgto_tb.tecnico_id = tecnico_tb.tecnico_id                      " & vbCrLf
        sSQL = sSQL & "     and evento_segbr_sinistro_tb.ramo_id = limite_aprovacao_pgto_tb.ramo_id             " & vbCrLf
        sSQL = sSQL & "     and evento_segbr_sinistro_tb.produto_id = limite_aprovacao_pgto_tb.produto_id       " & vbCrLf
        sSQL = sSQL & "     and evento_segbr_sinistro_tb.item_val_estimativa = limite_aprovacao_pgto_tb.item_val_estimativa " & vbCrLf
        sSQL = sSQL & " join seguros_db.dbo.ramo_tb ramo_tb  WITH (NOLOCK)                                             " & vbCrLf
        sSQL = sSQL & "     on ramo_tb.ramo_id = sinistro_tecnico_tb.ramo_id                                    " & vbCrLf
        sSQL = sSQL & " where 1=1                                                                               " & vbCrLf
        sSQL = sSQL & "     and sinistro_tecnico_tb.dt_fim_vigencia is null                                     " & vbCrLf
        sSQL = sSQL & "     and situacao_op = 'n'                                                               " & vbCrLf
        sSQL = sSQL & "     and evento_bb_id = 1151                                                             " & vbCrLf
        sSQL = sSQL & "     and evento_segbr_sinistro_tb.sinistro_id not in                                     " & vbCrLf
        sSQL = sSQL & "                 (select sinistro_id                                                     " & vbCrLf
        sSQL = sSQL & "                 from seguros_db.dbo.evento_segbr_sinistro_tb aux  WITH (NOLOCK)                " & vbCrLf
        sSQL = sSQL & "                 where aux.sinistro_id = evento_segbr_sinistro_tb.sinistro_id            " & vbCrLf
        sSQL = sSQL & "                     and aux.num_recibo = evento_segbr_sinistro_tb.num_recibo            " & vbCrLf
        sSQL = sSQL & "                     and aux.evento_bb_id in (1152, 1154))                               " & vbCrLf
        sSQL = sSQL & "     and ativo = 's'                                                                     " & vbCrLf
        sSQL = sSQL & "     and sinistro_tecnico_tb.tecnico_id = '" & sUsuario & "'" & vbCrLf
        sSQL = sSQL & "     and ramo_tb.tp_ramo_id = " & iTPRamo & vbCrLf
        sSQL = sSQL & " group by sinistro_tecnico_tb.tecnico_id "

    End If
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                sSQL, _
                                lConexaoLocal, _
                                True)
                                
    If Not rs.EOF Then
       sUsuario = rs("tecnico_id")
    Else
       sUsuario = ""
    End If
       
    rs.Close
    Set rs = Nothing
 
            
    If sUsuario <> "" Then
    
        sSQL = ""
        'Alan Neves - 19/02/2013
'        sSQL = sSQL & " insert into #tempsel_tb " & vbCrLf

        'Alan Neves - 19/02/2013
        sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
        sSQL = sSQL & "       marca                                             " & vbCrLf
        sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
        sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
        sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
        sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
        sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
        sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
        sSQL = sSQL & "     , numendosso                                        " & vbCrLf
        sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
        sSQL = sSQL & "     , produto_id                                        " & vbCrLf
        sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
        sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
        sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
        sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
        sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
        sSQL = sSQL & "     , situacao                                          " & vbCrLf
        sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
        sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
        sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
        sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
        sSQL = sSQL & "     , Endosso                                              " & vbCrLf
        sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
        sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
        sSQL = sSQL & "     , historico                                            " & vbCrLf
        sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
        sSQL = sSQL & "     )                                                   " & vbCrLf
    
        sSQL = sSQL & " exec seguros_db.dbo.SEGS10373_SPS " & sUsuario & "," & iTPRamo & ",'" & sVariacao & "'"
        
        ' flow 17813136 04/2013 petrauskas
        ' excluir casos de cosseguro da lista de casos pass�veis de distribui��o
        ' passar parametro opcional, indicando que � sele��o por Sinistros Pendentes Aprovacao
        If sTipoSelecao = "SinistrosPendentesAprovacao" Then
         sSQL = sSQL & ",'s'"
        End If
        
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         lConexaoLocal, _
                         False)

       
    End If
   
    
'    sSql = ""
'    sSql = sSql & "Insert into #tempsel_tb (                                " & vbNewLine
'    sSql = sSql & "       marca                                             " & vbNewLine
'    sSql = sSql & "     , apolice_id                                        " & vbNewLine
'    sSql = sSql & "     , sucursal_seguradora_id                            " & vbNewLine
'    sSql = sSql & "     , seguradora_cod_susep                              " & vbNewLine
'    sSql = sSql & "     , ramo_id                                           " & vbNewLine
'    sSql = sSql & "     , nomeramo                                          " & vbNewLine
'    sSql = sSql & "     , proposta_id                                       " & vbNewLine
'    sSql = sSql & "     , numendosso                                        " & vbNewLine
'    sSql = sSql & "     , sitproposta                                       " & vbNewLine
'    sSql = sSql & "     , produto_id                                        " & vbNewLine
'    sSql = sSql & "     , nomeproduto                                       " & vbNewLine
'    sSql = sSql & "     , cliente_id                                        " & vbNewLine
'    sSql = sSql & "     , nomecliente                                       " & vbNewLine
'    sSql = sSql & "     , tipoComponente                                    " & vbNewLine
'    sSql = sSql & "     , sinistro_id                                       " & vbNewLine
'    sSql = sSql & "     , situacao                                          " & vbNewLine
'    sSql = sSql & "     , situacao_desc                                     " & vbNewLine
'    sSql = sSql & "  , situacao_evento                                      " & vbNewLine
'    sSql = sSql & "  , sit_evento_desc                                      " & vbNewLine
'    sSql = sSql & "  , Sucursal_Nome                                        " & vbNewLine
'    sSql = sSql & "  , Endosso                                              " & vbNewLine
'    sSql = sSql & "  , sinistro_id_lider                                    " & vbNewLine
'    sSql = sSql & "  , processa_reintegracao_is                             " & vbNewLine
'    sSql = sSql & "     )                                                   " & vbNewLine
'    sSql = sSql & "  Exec sinistro_selecao_xxxxxxxx iTPRamo, '" & sNome & "'" & vbNewLine
'    Conexao_ExecutarSQL gsSIGLASISTEMA _
'                      , glAmbiente_id _
'                      , App.Title _
'                      , App.FileDescription _
'                      , sSql _
'                      , lConexaoLocal _
'                      , False
'
   
   
    Exit Function

TrataErro:
Call TrataErroGeral("ConsultaSinistroPor_SituacaoPendenteAprovacao_RamoVida", Me.name)
   
   
End Function

Private Sub ConsultaSinistroPor_ApoliceLider(iApoliceLiderNum As String)

    Dim sSQL As String
    
    On Error GoTo TrataErro
    
    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
        sSQL = ""
        sSQL = sSQL & "    insert into #tempsel_tb(  " & vbNewLine
        sSQL = sSQL & "     marca                   ," & vbNewLine
        sSQL = sSQL & "     apolice_id              ," & vbNewLine
        sSQL = sSQL & "     sucursal_seguradora_id  ," & vbNewLine
        sSQL = sSQL & "     seguradora_cod_susep    ," & vbNewLine
        sSQL = sSQL & "     ramo_id                 ," & vbNewLine
        sSQL = sSQL & "     nomeramo                ," & vbNewLine
        sSQL = sSQL & "     proposta_id             ," & vbNewLine
        sSQL = sSQL & "     numendosso              ," & vbNewLine
        sSQL = sSQL & "     sitproposta             ," & vbNewLine
        sSQL = sSQL & "     produto_id              ," & vbNewLine
        sSQL = sSQL & "     nomeproduto             ," & vbNewLine
        sSQL = sSQL & "     cliente_id              ," & vbNewLine
        sSQL = sSQL & "     nomecliente             ," & vbNewLine
        sSQL = sSQL & "     tipocomponente          ," & vbNewLine
        sSQL = sSQL & "     sinistro_id             )" & vbNewLine
     
        sSQL = sSQL & "SELECT  DISTINCT                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "        'n'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , proposta_adesao_tb.apolice_id                                                                                                                                " & vbNewLine
        sSQL = sSQL & "      , proposta_adesao_tb.sucursal_seguradora_id                                                                                                                    " & vbNewLine
        sSQL = sSQL & "      , proposta_adesao_tb.seguradora_cod_susep                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.ramo_id                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.nome                                                                                                                                                 " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.proposta_id                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.num_endosso                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.situacao                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "      , produto_tb.produto_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , produto_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.cliente_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , 'T'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , null                                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "  FROM  proposta_adesao_tb with ( nolock )                                                                                                                           " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ( ( proposta_tb with ( nolock )                                                                                                                   " & vbNewLine
        sSQL = sSQL & "                       INNER JOIN produto_tb with ( nolock ) ON proposta_tb.produto_id = produto_tb.produto_id                                                       " & vbNewLine
        sSQL = sSQL & "                     )                                                                                                                                               " & vbNewLine
        sSQL = sSQL & "                     INNER JOIN cliente_tb with ( nolock ) ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id                                                    " & vbNewLine
        sSQL = sSQL & "                   ) ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id                                                                                     " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ramo_tb with ( nolock ) ON proposta_adesao_tb.ramo_id = ramo_tb.ramo_id                                                                           " & vbNewLine
        sSQL = sSQL & "                                              AND ramo_tb.tp_ramo_id = " & bytTipoRamo
        sSQL = sSQL & "        INNER JOIN apolice_terceiros_tb with ( nolock ) ON apolice_terceiros_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id                   " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.apolice_id = proposta_adesao_tb.apolice_id                                       " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.seguradora_cod_susep = proposta_adesao_tb.seguradora_cod_susep                   " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.ramo_id = proposta_adesao_tb.ramo_id                                             " & vbNewLine
        sSQL = sSQL & "        INNER JOIN co_seguro_aceito_tb with ( nolock ) ON co_seguro_aceito_tb.num_ordem_co_seguro_aceito = apolice_terceiros_tb.num_ordem_co_seguro_aceito           " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.seg_cod_susep_lider = apolice_terceiros_tb.seg_cod_susep_lider                     " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.sucursal_seg_lider = apolice_terceiros_tb.sucursal_seg_lider                       " & vbNewLine
        sSQL = sSQL & "WHERE    co_seguro_aceito_tb.num_apolice_lider = " & iApoliceLiderNum & vbNewLine
        sSQL = sSQL & "UNION ALL                                                                                                                                                            " & vbNewLine
        sSQL = sSQL & "                                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "SELECT  DISTINCT                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "        'n'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , proposta_adesao_tb.apolice_id                                                                                                                                " & vbNewLine
        sSQL = sSQL & "      , proposta_adesao_tb.sucursal_seguradora_id                                                                                                                    " & vbNewLine
        sSQL = sSQL & "      , proposta_adesao_tb.seguradora_cod_susep                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.ramo_id                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.nome                                                                                                                                                 " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.proposta_id                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.num_endosso                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.situacao                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "      , produto_tb.produto_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , produto_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.cliente_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , 'C'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , null                                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "FROM     proposta_adesao_tb with ( nolock )                                                                                                                          " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ( ( ( proposta_tb with ( nolock )                                                                                                                 " & vbNewLine
        sSQL = sSQL & "                         INNER JOIN proposta_complementar_tb with ( nolock ) ON proposta_tb.proposta_id = proposta_complementar_tb.proposta_id                       " & vbNewLine
        sSQL = sSQL & "                       )                                                                                                                                             " & vbNewLine
        sSQL = sSQL & "                       INNER JOIN produto_tb with ( nolock ) ON proposta_tb.produto_id = produto_tb.produto_id                                                       " & vbNewLine
        sSQL = sSQL & "                     )                                                                                                                                               " & vbNewLine
        sSQL = sSQL & "                     INNER JOIN cliente_tb with ( nolock ) ON proposta_complementar_tb.prop_cliente_id = cliente_tb.cliente_id                                       " & vbNewLine
        sSQL = sSQL & "                   ) ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id                                                                                     " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ramo_tb with ( nolock ) ON proposta_adesao_tb.ramo_id = ramo_tb.ramo_id                                                                           " & vbNewLine
        sSQL = sSQL & "                                              AND ramo_tb.tp_ramo_id = " & bytTipoRamo
        sSQL = sSQL & "        INNER JOIN apolice_terceiros_tb with ( nolock ) ON apolice_terceiros_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id                   " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.apolice_id = proposta_adesao_tb.apolice_id                                       " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.seguradora_cod_susep = proposta_adesao_tb.seguradora_cod_susep                   " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.ramo_id = proposta_adesao_tb.ramo_id                                             " & vbNewLine
        sSQL = sSQL & "        INNER JOIN co_seguro_aceito_tb with ( nolock ) ON co_seguro_aceito_tb.num_ordem_co_seguro_aceito = apolice_terceiros_tb.num_ordem_co_seguro_aceito           " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.seg_cod_susep_lider = apolice_terceiros_tb.seg_cod_susep_lider                     " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.sucursal_seg_lider = apolice_terceiros_tb.sucursal_seg_lider                       " & vbNewLine
        sSQL = sSQL & "WHERE    co_seguro_aceito_tb.num_apolice_lider = " & iApoliceLiderNum & vbNewLine
        sSQL = sSQL & "UNION ALL                                                                                                                                                            " & vbNewLine
        sSQL = sSQL & "                                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "SELECT  DISTINCT                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "        'n'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , apolice_tb.apolice_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , apolice_tb.sucursal_seguradora_id                                                                                                                            " & vbNewLine
        sSQL = sSQL & "      , apolice_tb.seguradora_cod_susep                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.ramo_id                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.nome                                                                                                                                                 " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.proposta_id                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.num_endosso                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.situacao                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "      , produto_tb.produto_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , produto_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.cliente_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , 'T'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , null                                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "FROM     proposta_fechada_tb with ( nolock )                                                                                                                         " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ( ( proposta_tb with ( nolock )                                                                                                                   " & vbNewLine
        sSQL = sSQL & "                       INNER JOIN produto_tb with ( nolock ) ON proposta_tb.produto_id = produto_tb.produto_id                                                       " & vbNewLine
        sSQL = sSQL & "                     )                                                                                                                                               " & vbNewLine
        sSQL = sSQL & "                     INNER JOIN cliente_tb with ( nolock ) ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id                                                    " & vbNewLine
        sSQL = sSQL & "                   ) ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                                                                                    " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ramo_tb with ( nolock ) ON proposta_tb.ramo_id = ramo_tb.ramo_id                                                                                  " & vbNewLine
        sSQL = sSQL & "                                              AND ramo_tb.tp_ramo_id = " & bytTipoRamo
        sSQL = sSQL & "        INNER JOIN apolice_tb with ( nolock ) ON proposta_tb.proposta_id = apolice_tb.proposta_id                                                                    " & vbNewLine
        sSQL = sSQL & "        INNER JOIN apolice_terceiros_tb with ( nolock ) ON apolice_terceiros_tb.sucursal_seguradora_id = apolice_tb.sucursal_seguradora_id                           " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.apolice_id = apolice_tb.apolice_id                                               " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.seguradora_cod_susep = apolice_tb.seguradora_cod_susep                           " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.ramo_id = apolice_tb.ramo_id                                                     " & vbNewLine
        sSQL = sSQL & "        INNER JOIN co_seguro_aceito_tb with ( nolock ) ON co_seguro_aceito_tb.num_ordem_co_seguro_aceito = apolice_terceiros_tb.num_ordem_co_seguro_aceito           " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.seg_cod_susep_lider = apolice_terceiros_tb.seg_cod_susep_lider                     " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.sucursal_seg_lider = apolice_terceiros_tb.sucursal_seg_lider                       " & vbNewLine
        sSQL = sSQL & "WHERE    co_seguro_aceito_tb.num_apolice_lider = " & iApoliceLiderNum & vbNewLine
        sSQL = sSQL & "UNION ALL                                                                                                                                                            " & vbNewLine
        sSQL = sSQL & "                                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "SELECT  DISTINCT                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "        'n'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , apolice_tb.apolice_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , apolice_tb.sucursal_seguradora_id                                                                                                                            " & vbNewLine
        sSQL = sSQL & "      , apolice_tb.seguradora_cod_susep                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.ramo_id                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.nome                                                                                                                                                 " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.proposta_id                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.num_endosso                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.situacao                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "      , produto_tb.produto_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , produto_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.cliente_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , 'C'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , null                                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "FROM     proposta_fechada_tb with ( nolock )                                                                                                                         " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ( ( ( proposta_tb with ( nolock )                                                                                                                 " & vbNewLine
        sSQL = sSQL & "                         INNER JOIN proposta_complementar_tb with ( nolock ) ON proposta_tb.proposta_id = proposta_complementar_tb.proposta_id                       " & vbNewLine
        sSQL = sSQL & "                       )                                                                                                                                             " & vbNewLine
        sSQL = sSQL & "                       INNER JOIN produto_tb with ( nolock ) ON proposta_tb.produto_id = produto_tb.produto_id                                                       " & vbNewLine
        sSQL = sSQL & "                     )                                                                                                                                               " & vbNewLine
        sSQL = sSQL & "                     INNER JOIN cliente_tb with ( nolock ) ON proposta_complementar_tb.prop_cliente_id = cliente_tb.cliente_id                                       " & vbNewLine
        sSQL = sSQL & "                   ) ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                                                                                    " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ramo_tb with ( nolock ) ON proposta_tb.ramo_id = ramo_tb.ramo_id                                                                                  " & vbNewLine
        sSQL = sSQL & "                                              AND ramo_tb.tp_ramo_id = " & bytTipoRamo
        sSQL = sSQL & "        INNER JOIN apolice_tb with ( nolock ) ON proposta_tb.proposta_id = apolice_tb.proposta_id                                                                    " & vbNewLine
        sSQL = sSQL & "        INNER JOIN apolice_terceiros_tb with ( nolock ) ON apolice_terceiros_tb.sucursal_seguradora_id = apolice_tb.sucursal_seguradora_id                           " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.apolice_id = apolice_tb.apolice_id                                               " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.seguradora_cod_susep = apolice_tb.seguradora_cod_susep                           " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.ramo_id = apolice_tb.ramo_id                                                     " & vbNewLine
        sSQL = sSQL & "        INNER JOIN co_seguro_aceito_tb with ( nolock ) ON co_seguro_aceito_tb.num_ordem_co_seguro_aceito = apolice_terceiros_tb.num_ordem_co_seguro_aceito           " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.seg_cod_susep_lider = apolice_terceiros_tb.seg_cod_susep_lider                     " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.sucursal_seg_lider = apolice_terceiros_tb.sucursal_seg_lider                       " & vbNewLine
        sSQL = sSQL & "WHERE    co_seguro_aceito_tb.num_apolice_lider = " & iApoliceLiderNum & vbNewLine
        sSQL = sSQL & "                                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "                                                                                                                                                                     " & vbNewLine
    Else
        ' Alan Neves - 19/02/2013
'        sSQL = " insert into #tempsel_tb                             " & vbCrLf

        'Alan Neves - 19/02/2013
        sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
        sSQL = sSQL & "       marca                                             " & vbCrLf
        sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
        sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
        sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
        sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
        sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
        sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
        sSQL = sSQL & "     , numendosso                                        " & vbCrLf
        sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
        sSQL = sSQL & "     , produto_id                                        " & vbCrLf
        sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
        sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
        sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
        sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
        sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
        sSQL = sSQL & "     , situacao                                          " & vbCrLf
        sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
        sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
        sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
        sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
        sSQL = sSQL & "     , Endosso                                              " & vbCrLf
        sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
        sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
        sSQL = sSQL & "     , historico                                            " & vbCrLf
        sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
        sSQL = sSQL & "     )                                                   " & vbCrLf



        sSQL = sSQL & " select distinct                                     " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.lock_aviso,                         " & vbCrLf
        sSQL = sSQL & "     proposta_adesao_tb.apolice_id,                  " & vbCrLf
        sSQL = sSQL & "     proposta_adesao_tb.sucursal_seguradora_id,      " & vbCrLf
        sSQL = sSQL & "     proposta_adesao_tb.seguradora_cod_susep,        " & vbCrLf
        sSQL = sSQL & "     ramo_tb.ramo_id,                                " & vbCrLf
        sSQL = sSQL & "     ramo_tb.nome,                                   " & vbCrLf
        sSQL = sSQL & "     proposta_tb.proposta_id,                        " & vbCrLf
        sSQL = sSQL & "     proposta_tb.num_endosso,                        " & vbCrLf
        sSQL = sSQL & "     proposta_tb.situacao,                           " & vbCrLf
        sSQL = sSQL & "     produto_tb.produto_id,                          " & vbCrLf
        sSQL = sSQL & "     produto_tb.nome,                                " & vbCrLf
        sSQL = sSQL & "     cliente_tb.cliente_id,                          " & vbCrLf
        sSQL = sSQL & "     cliente_tb.nome,                                " & vbCrLf
        sSQL = sSQL & "     tipocomponente  = 't',                          " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id,                        " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.situacao,                           " & vbCrLf
        sSQL = sSQL & "     situacao_desc   = '',                           " & vbCrLf
        sSQL = sSQL & "     situacao_evento = '',                           " & vbCrLf
        sSQL = sSQL & "     sit_evento_desc = '',                           " & vbCrLf
        sSQL = sSQL & "     sucursal_nome   = null,                         " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.endosso_id,                         " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id_lider,                  " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.processa_reintegracao_is,           " & vbCrLf
        'mathayde
        'sSQL = sSQL & "     historico = ''                                  " & vbCrLf
        sSQL = sSQL & "     historico = '',                                 " & vbCrLf
        sSQL = sSQL & "     dt_ocorrencia_sinistro                          " & vbCrLf
        sSQL = sSQL & " from proposta_adesao_tb  WITH (NOLOCK)                 " & vbCrLf
        sSQL = sSQL & " inner join apolice_terceiros_tb  WITH (NOLOCK)         " & vbCrLf
        sSQL = sSQL & "     on proposta_adesao_tb.apolice_id                =  apolice_terceiros_tb.apolice_id              " & vbCrLf
        sSQL = sSQL & "     and proposta_adesao_tb.sucursal_seguradora_id   =  apolice_terceiros_tb.sucursal_seguradora_id  " & vbCrLf
        sSQL = sSQL & "     and proposta_adesao_tb.seguradora_cod_susep     =  apolice_terceiros_tb.seguradora_cod_susep    " & vbCrLf
        sSQL = sSQL & "     and proposta_adesao_tb.ramo_id                  =  apolice_terceiros_tb.ramo_id                 " & vbCrLf
        sSQL = sSQL & " inner join ramo_tb  WITH (NOLOCK)                                                                      " & vbCrLf
        sSQL = sSQL & "     on proposta_adesao_tb.ramo_id                   =  ramo_tb.ramo_id                              " & vbCrLf
        sSQL = sSQL & "     and ramo_tb.tp_ramo_id                          =  1                                            " & vbCrLf
        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                                                  " & vbCrLf
        sSQL = sSQL & "  on proposta_adesao_tb.proposta_id                  =  proposta_tb.proposta_id                      " & vbCrLf
        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                                                                   " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.prop_cliente_id                  =  cliente_tb.cliente_id                        " & vbCrLf
        sSQL = sSQL & " inner join produto_tb  WITH (NOLOCK)                                                                   " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.produto_id                       = produto_tb.produto_id                         " & vbCrLf
        sSQL = sSQL & "  inner join sinistro_tb  WITH (NOLOCK)                                                                 " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.apolice_id                       =  proposta_adesao_tb.apolice_id                " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.sucursal_seguradora_id          =  proposta_adesao_tb.sucursal_seguradora_id    " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.seguradora_cod_susep            =  proposta_adesao_tb.seguradora_cod_susep      " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.ramo_id                         =  proposta_adesao_tb.ramo_id                   " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.proposta_id                     =  proposta_adesao_tb.proposta_id               " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.cliente_id                      =  cliente_tb.cliente_id                        " & vbCrLf
        sSQL = sSQL & " inner join co_seguro_aceito_tb  WITH (NOLOCK)                                                          " & vbCrLf
        sSQL = sSQL & "     on co_seguro_aceito_tb.seg_cod_susep_lider      =  apolice_terceiros_tb.seg_cod_susep_lider     " & vbCrLf
        sSQL = sSQL & "     and co_seguro_aceito_tb.num_ordem_co_seguro_aceito=  apolice_terceiros_tb.num_ordem_co_seguro_aceito    " & vbCrLf
        sSQL = sSQL & " where                                                                                                       " & vbCrLf
        sSQL = sSQL & "     co_seguro_aceito_tb.num_apolice_lider           =  " & iApoliceLiderNum & vbCrLf
        sSQL = sSQL & " union all                                           " & vbCrLf
        ' /* conjuge */
        sSQL = sSQL & " select distinct                                     " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.lock_aviso,                         " & vbCrLf
        sSQL = sSQL & "     proposta_adesao_tb.apolice_id,                  " & vbCrLf
        sSQL = sSQL & "     proposta_adesao_tb.sucursal_seguradora_id,      " & vbCrLf
        sSQL = sSQL & "     proposta_adesao_tb.seguradora_cod_susep,        " & vbCrLf
        sSQL = sSQL & "     ramo_tb.ramo_id,                                " & vbCrLf
        sSQL = sSQL & "     ramo_tb.nome,                                   " & vbCrLf
        sSQL = sSQL & "     proposta_tb.proposta_id,                        " & vbCrLf
        sSQL = sSQL & "     proposta_tb.num_endosso,                        " & vbCrLf
        sSQL = sSQL & "     proposta_tb.situacao,                           " & vbCrLf
        sSQL = sSQL & "     produto_tb.produto_id,                          " & vbCrLf
        sSQL = sSQL & "     produto_tb.nome,                                " & vbCrLf
        sSQL = sSQL & "     cliente_tb.cliente_id,                          " & vbCrLf
        sSQL = sSQL & "     cliente_tb.nome,                                " & vbCrLf
        sSQL = sSQL & "     tipocomponente  = 'c',                          " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id,                        " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.situacao,                           " & vbCrLf
        sSQL = sSQL & "     situacao_desc   = '',                           " & vbCrLf
        sSQL = sSQL & "     situacao_evento = '',                           " & vbCrLf
        sSQL = sSQL & "     sit_evento_desc = '',                           " & vbCrLf
        sSQL = sSQL & "     sucursal_nome   = null,                         " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.endosso_id,                         " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id_lider,                  " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.processa_reintegracao_is,           " & vbCrLf
        'mathayde
        'sSQL = sSQL & "     historico = ''                                  " & vbCrLf
        sSQL = sSQL & "     historico = '',                                 " & vbCrLf
        sSQL = sSQL & "     dt_ocorrencia_sinistro                          " & vbCrLf
        sSQL = sSQL & " from proposta_adesao_tb  WITH (NOLOCK)                 " & vbCrLf
        sSQL = sSQL & " inner join apolice_terceiros_tb  WITH (NOLOCK)         " & vbCrLf
        sSQL = sSQL & "     on proposta_adesao_tb.apolice_id                =  apolice_terceiros_tb.apolice_id              " & vbCrLf
        sSQL = sSQL & "     and proposta_adesao_tb.sucursal_seguradora_id   =  apolice_terceiros_tb.sucursal_seguradora_id  " & vbCrLf
        sSQL = sSQL & "     and proposta_adesao_tb.seguradora_cod_susep     =  apolice_terceiros_tb.seguradora_cod_susep    " & vbCrLf
        sSQL = sSQL & "     and proposta_adesao_tb.ramo_id                  =  apolice_terceiros_tb.ramo_id                 " & vbCrLf
        sSQL = sSQL & " inner join ramo_tb  WITH (NOLOCK)                                                                      " & vbCrLf
        sSQL = sSQL & "     on proposta_adesao_tb.ramo_id                   =  ramo_tb.ramo_id                              " & vbCrLf
        sSQL = sSQL & "     and ramo_tb.tp_ramo_id                          =  1                                            " & vbCrLf
        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                                                  " & vbCrLf
        sSQL = sSQL & "    on proposta_adesao_tb.proposta_id                =  proposta_tb.proposta_id                      " & vbCrLf
        sSQL = sSQL & " inner join proposta_complementar_tb  WITH (NOLOCK)                                                     " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.proposta_id                      =  proposta_complementar_tb.proposta_id         " & vbCrLf
        sSQL = sSQL & " inner join produto_tb  WITH (NOLOCK)                                                                   " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.produto_id                       =  produto_tb.produto_id                        " & vbCrLf
        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                                                                   " & vbCrLf
        sSQL = sSQL & "     on proposta_complementar_tb.prop_cliente_id     =  cliente_tb.cliente_id                        " & vbCrLf
        sSQL = sSQL & " inner join sinistro_tb  WITH (NOLOCK)                                                                  " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.apolice_id                       =  proposta_adesao_tb.apolice_id                " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.sucursal_seguradora_id          =  proposta_adesao_tb.sucursal_seguradora_id    " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.seguradora_cod_susep            =  proposta_adesao_tb.seguradora_cod_susep      " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.ramo_id                         =  proposta_adesao_tb.ramo_id                   " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.proposta_id                     =  proposta_adesao_tb.proposta_id               " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.cliente_id                      =  cliente_tb.cliente_id                        " & vbCrLf
        sSQL = sSQL & " inner join co_seguro_aceito_tb  WITH (NOLOCK)                                                          " & vbCrLf
        sSQL = sSQL & "     on co_seguro_aceito_tb.seg_cod_susep_lider      =  apolice_terceiros_tb.seg_cod_susep_lider     " & vbCrLf
        sSQL = sSQL & "     and co_seguro_aceito_tb.num_ordem_co_seguro_aceito=  apolice_terceiros_tb.num_ordem_co_seguro_aceito    " & vbCrLf
        sSQL = sSQL & " where                                                                                                       " & vbCrLf
        sSQL = sSQL & "     co_seguro_aceito_tb.num_apolice_lider           =  " & iApoliceLiderNum & vbCrLf
        sSQL = sSQL & " union all                                                   " & vbCrLf
        ' /* titular */
        sSQL = sSQL & " select distinct                             " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.lock_aviso,                 " & vbCrLf
        sSQL = sSQL & "     apolice.apolice_id,                     " & vbCrLf
        sSQL = sSQL & "     apolice.sucursal_seguradora_id,         " & vbCrLf
        sSQL = sSQL & "     apolice.seguradora_cod_susep,           " & vbCrLf
        sSQL = sSQL & "     ramo_tb.ramo_id,                        " & vbCrLf
        sSQL = sSQL & "     ramo_tb.nome,                           " & vbCrLf
        sSQL = sSQL & "     proposta_tb.proposta_id,                " & vbCrLf
        sSQL = sSQL & "     proposta_tb.num_endosso,                " & vbCrLf
        sSQL = sSQL & "     proposta_tb.situacao,                   " & vbCrLf
        sSQL = sSQL & "     produto_tb.produto_id,                  " & vbCrLf
        sSQL = sSQL & "     produto_tb.nome,                        " & vbCrLf
        sSQL = sSQL & "     cliente_tb.cliente_id,                  " & vbCrLf
        sSQL = sSQL & "     cliente_tb.nome,                        " & vbCrLf
        sSQL = sSQL & "     tipocomponente  = 't',                  " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id,                " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.situacao,                   " & vbCrLf
        sSQL = sSQL & "     situacao_desc   = '',                   " & vbCrLf
        sSQL = sSQL & "     situacao_evento = '',                   " & vbCrLf
        sSQL = sSQL & "     sit_evento_desc = '',                   " & vbCrLf
        sSQL = sSQL & "     sucursal_nome   = null,                 " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.endosso_id,                 " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id_lider,          " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.processa_reintegracao_is,   " & vbCrLf
        'mathayde
        'sSQL = sSQL & "     historico = ''                                  " & vbCrLf
        sSQL = sSQL & "     historico = '',                                 " & vbCrLf
        sSQL = sSQL & "     dt_ocorrencia_sinistro                          " & vbCrLf
        sSQL = sSQL & " from proposta_fechada_tb  WITH (NOLOCK)        " & vbCrLf
        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)          " & vbCrLf
        sSQL = sSQL & "     on proposta_fechada_tb.proposta_id          =  proposta_tb.proposta_id                                  " & vbCrLf
        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                                                                           " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.prop_cliente_id              =  cliente_tb.cliente_id                                    " & vbCrLf
        sSQL = sSQL & "  inner join ramo_tb  WITH (NOLOCK)                                                                             " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.ramo_id                      =  ramo_tb.ramo_id                                          " & vbCrLf
        sSQL = sSQL & "     and ramo_tb.tp_ramo_id                      =  1                                                        " & vbCrLf
        sSQL = sSQL & " inner join produto_tb  WITH (NOLOCK)                                                                           " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.produto_id                   = produto_tb.produto_id                                     " & vbCrLf
        sSQL = sSQL & " inner join apolice_tb apolice  WITH (NOLOCK)                                                                   " & vbCrLf
        sSQL = sSQL & "     on apolice.proposta_id                      =  proposta_tb.proposta_id                                  " & vbCrLf
        sSQL = sSQL & " inner join apolice_terceiros_tb  WITH (NOLOCK)                                                                 " & vbCrLf
        sSQL = sSQL & "     on apolice.apolice_id                       =  apolice_terceiros_tb.apolice_id                          " & vbCrLf
        sSQL = sSQL & "     and apolice.sucursal_seguradora_id          =  apolice_terceiros_tb.sucursal_seguradora_id              " & vbCrLf
        sSQL = sSQL & "     and apolice.seguradora_cod_susep            =  apolice_terceiros_tb.seguradora_cod_susep                " & vbCrLf
        sSQL = sSQL & "     and proposta_tb.ramo_id                     =  apolice_terceiros_tb.ramo_id                             " & vbCrLf
        sSQL = sSQL & " inner join co_seguro_aceito_tb  WITH (NOLOCK)                                                                  " & vbCrLf
        sSQL = sSQL & "     on co_seguro_aceito_tb.seg_cod_susep_lider  =  apolice_terceiros_tb.seg_cod_susep_lider                 " & vbCrLf
        sSQL = sSQL & "     and co_seguro_aceito_tb.num_ordem_co_seguro_aceito=  apolice_terceiros_tb.num_ordem_co_seguro_aceito    " & vbCrLf
        sSQL = sSQL & " inner join sinistro_tb  WITH (NOLOCK)                                                                          " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.apolice_id    =  apolice.apolice_id                                                      " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.sucursal_seguradora_id      =  apolice.sucursal_seguradora_id                           " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.sinistro_id_lider           =  co_seguro_aceito_tb.num_apolice_lider                    " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.seguradora_cod_susep        =  apolice.seguradora_cod_susep                             " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.ramo_id                     =  proposta_tb.ramo_id                                      " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.proposta_id                 =  proposta_fechada_tb.proposta_id                          " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.cliente_id                  =  cliente_tb.cliente_id                                    " & vbCrLf
        sSQL = sSQL & " where                                                                                                       " & vbCrLf
        sSQL = sSQL & "     co_seguro_aceito_tb.num_apolice_lider       =  " & iApoliceLiderNum & vbCrLf
        sSQL = sSQL & " union all                                   " & vbCrLf
        '/* conjuge */
        sSQL = sSQL & " select distinct                             " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.lock_aviso,                 " & vbCrLf
        sSQL = sSQL & "     apolice.apolice_id,                     " & vbCrLf
        sSQL = sSQL & "     apolice.sucursal_seguradora_id,         " & vbCrLf
        sSQL = sSQL & "     apolice.seguradora_cod_susep,           " & vbCrLf
        sSQL = sSQL & "     ramo_tb.ramo_id,                        " & vbCrLf
        sSQL = sSQL & "     ramo_tb.nome,                           " & vbCrLf
        sSQL = sSQL & "     proposta_tb.proposta_id,                " & vbCrLf
        sSQL = sSQL & "     proposta_tb.num_endosso,                " & vbCrLf
        sSQL = sSQL & "     proposta_tb.situacao,                   " & vbCrLf
        sSQL = sSQL & "     produto_tb.produto_id,                  " & vbCrLf
        sSQL = sSQL & "     produto_tb.nome,                        " & vbCrLf
        sSQL = sSQL & "     cliente_tb.cliente_id,                  " & vbCrLf
        sSQL = sSQL & "     cliente_tb.nome,                        " & vbCrLf
        sSQL = sSQL & "     tipocomponente  = 'c',                  " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id,                " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.situacao,                   " & vbCrLf
        sSQL = sSQL & "     situacao_desc   = '',                   " & vbCrLf
        sSQL = sSQL & "     situacao_evento = '',                   " & vbCrLf
        sSQL = sSQL & "     sit_evento_desc = '',                   " & vbCrLf
        sSQL = sSQL & "     sucursal_nome   = null,                 " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.endosso_id,                 " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id_lider,          " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.processa_reintegracao_is,   " & vbCrLf
        'mathayde
        'sSQL = sSQL & "     historico = ''                                  " & vbCrLf
        sSQL = sSQL & "     historico = '',                                 " & vbCrLf
        sSQL = sSQL & "     dt_ocorrencia_sinistro                          " & vbCrLf
        sSQL = sSQL & " from proposta_fechada_tb  WITH (NOLOCK)        " & vbCrLf
        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)          " & vbCrLf
        sSQL = sSQL & "     on proposta_fechada_tb.proposta_id              =  proposta_tb.proposta_id                              " & vbCrLf
        sSQL = sSQL & " inner join proposta_complementar_tb  WITH (NOLOCK)                                                             " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.proposta_id                      =  proposta_complementar_tb.proposta_id                 " & vbCrLf
        sSQL = sSQL & " inner join produto_tb  WITH (NOLOCK)                                                                           " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.produto_id                       = produto_tb.produto_id                                 " & vbCrLf
        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                                                                           " & vbCrLf
        sSQL = sSQL & "     on proposta_complementar_tb.prop_cliente_id     =  cliente_tb.cliente_id                                " & vbCrLf
        sSQL = sSQL & " inner join ramo_tb  WITH (NOLOCK)                                                                              " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.ramo_id                          =  ramo_tb.ramo_id                                      " & vbCrLf
        sSQL = sSQL & "     and ramo_tb.tp_ramo_id                          =  1                                                    " & vbCrLf
        sSQL = sSQL & " inner join apolice_tb apolice  WITH (NOLOCK)                                                                   " & vbCrLf
        sSQL = sSQL & "     on apolice.proposta_id                          =  proposta_tb.proposta_id                              " & vbCrLf
        sSQL = sSQL & " inner join apolice_terceiros_tb  WITH (NOLOCK)                                                                 " & vbCrLf
        sSQL = sSQL & "     on apolice.apolice_id                           =  apolice_terceiros_tb.apolice_id                      " & vbCrLf
        sSQL = sSQL & "     and apolice.sucursal_seguradora_id              =  apolice_terceiros_tb.sucursal_seguradora_id          " & vbCrLf
        sSQL = sSQL & "     and apolice.seguradora_cod_susep                =  apolice_terceiros_tb.seguradora_cod_susep            " & vbCrLf
        sSQL = sSQL & "     and proposta_tb.ramo_id                         =  apolice_terceiros_tb.ramo_id                         " & vbCrLf
        sSQL = sSQL & " inner join co_seguro_aceito_tb  WITH (NOLOCK)                                                                  " & vbCrLf
        sSQL = sSQL & "     on co_seguro_aceito_tb.seg_cod_susep_lider      =  apolice_terceiros_tb.seg_cod_susep_lider             " & vbCrLf
        sSQL = sSQL & "     and co_seguro_aceito_tb.num_ordem_co_seguro_aceito=  apolice_terceiros_tb.num_ordem_co_seguro_aceito    " & vbCrLf
        sSQL = sSQL & " inner join sinistro_tb  WITH (NOLOCK)                                                                          " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.apolice_id                       =  apolice.apolice_id                                   " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.sucursal_seguradora_id          =  apolice.sucursal_seguradora_id                       " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.sinistro_id_lider               =  co_seguro_aceito_tb.num_apolice_lider                " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.seguradora_cod_susep            =  apolice.seguradora_cod_susep                         " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.ramo_id                         =  proposta_tb.ramo_id                                  " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.proposta_id                     =  proposta_fechada_tb.proposta_id                      " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.cliente_id                      =  cliente_tb.cliente_id                                " & vbCrLf
        sSQL = sSQL & " where                                                                                                       " & vbCrLf
        sSQL = sSQL & "     co_seguro_aceito_tb.num_apolice_lider           =  " & iApoliceLiderNum & vbCrLf
    End If
'    Open "f:\fileTXT.txt" For Output As #100
'    Print #100, sSQL
'    Close #100

    sSQL = sSQL & "" & vbNewLine
    sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10420_SPS    "


    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         lConexaoLocal, _
                         False)
                         
   Exit Sub

TrataErro:
Call TrataErroGeral("ConsultaSinistroPor_ApoliceLider", Me.name)
        
End Sub

Private Sub ConsultaSinistroPor_NumOrdemCosseguro(iNumOrdemCosseguro As String)
'foi usado mesmo select da apolice lider.
' Modificado o parametro de consulta:
'          co_seguro_aceito_tb.num_apolice_lider           =  " & iApoliceLiderNum & vbCrLf
'    para  co_seguro_aceito_tb.num_ordem_co_seguro_aceito= " & iNumOrdemCosseguro & vbNewLine no lugar

    Dim sSQL As String
    
    On Error GoTo TrataErro
    
    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
        sSQL = ""
        sSQL = sSQL & "    insert into #tempsel_tb(  " & vbNewLine
        sSQL = sSQL & "     marca                   ," & vbNewLine
        sSQL = sSQL & "     apolice_id              ," & vbNewLine
        sSQL = sSQL & "     sucursal_seguradora_id  ," & vbNewLine
        sSQL = sSQL & "     seguradora_cod_susep    ," & vbNewLine
        sSQL = sSQL & "     ramo_id                 ," & vbNewLine
        sSQL = sSQL & "     nomeramo                ," & vbNewLine
        sSQL = sSQL & "     proposta_id             ," & vbNewLine
        sSQL = sSQL & "     numendosso              ," & vbNewLine
        sSQL = sSQL & "     sitproposta             ," & vbNewLine
        sSQL = sSQL & "     produto_id              ," & vbNewLine
        sSQL = sSQL & "     nomeproduto             ," & vbNewLine
        sSQL = sSQL & "     cliente_id              ," & vbNewLine
        sSQL = sSQL & "     nomecliente             ," & vbNewLine
        sSQL = sSQL & "     tipocomponente          ," & vbNewLine
        sSQL = sSQL & "     sinistro_id             )" & vbNewLine
     
        sSQL = sSQL & "SELECT  DISTINCT                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "        'n'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , proposta_adesao_tb.apolice_id                                                                                                                                " & vbNewLine
        sSQL = sSQL & "      , proposta_adesao_tb.sucursal_seguradora_id                                                                                                                    " & vbNewLine
        sSQL = sSQL & "      , proposta_adesao_tb.seguradora_cod_susep                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.ramo_id                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.nome                                                                                                                                                 " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.proposta_id                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.num_endosso                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.situacao                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "      , produto_tb.produto_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , produto_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.cliente_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , 'T'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , null                                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "  FROM  proposta_adesao_tb with ( nolock )                                                                                                                           " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ( ( proposta_tb with ( nolock )                                                                                                                   " & vbNewLine
        sSQL = sSQL & "                       INNER JOIN produto_tb with ( nolock ) ON proposta_tb.produto_id = produto_tb.produto_id                                                       " & vbNewLine
        sSQL = sSQL & "                     )                                                                                                                                               " & vbNewLine
        sSQL = sSQL & "                     INNER JOIN cliente_tb with ( nolock ) ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id                                                    " & vbNewLine
        sSQL = sSQL & "                   ) ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id                                                                                     " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ramo_tb with ( nolock ) ON proposta_adesao_tb.ramo_id = ramo_tb.ramo_id                                                                           " & vbNewLine
        sSQL = sSQL & "                                              AND ramo_tb.tp_ramo_id = " & bytTipoRamo
        sSQL = sSQL & "        INNER JOIN apolice_terceiros_tb with ( nolock ) ON apolice_terceiros_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id                   " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.apolice_id = proposta_adesao_tb.apolice_id                                       " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.seguradora_cod_susep = proposta_adesao_tb.seguradora_cod_susep                   " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.ramo_id = proposta_adesao_tb.ramo_id                                             " & vbNewLine
        sSQL = sSQL & "        INNER JOIN co_seguro_aceito_tb with ( nolock ) ON co_seguro_aceito_tb.num_ordem_co_seguro_aceito = apolice_terceiros_tb.num_ordem_co_seguro_aceito           " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.seg_cod_susep_lider = apolice_terceiros_tb.seg_cod_susep_lider                     " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.sucursal_seg_lider = apolice_terceiros_tb.sucursal_seg_lider                       " & vbNewLine
        sSQL = sSQL & "WHERE    co_seguro_aceito_tb.num_ordem_co_seguro_aceito= " & iNumOrdemCosseguro & vbNewLine
        sSQL = sSQL & "UNION ALL                                                                                                                                                            " & vbNewLine
        sSQL = sSQL & "                                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "SELECT  DISTINCT                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "        'n'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , proposta_adesao_tb.apolice_id                                                                                                                                " & vbNewLine
        sSQL = sSQL & "      , proposta_adesao_tb.sucursal_seguradora_id                                                                                                                    " & vbNewLine
        sSQL = sSQL & "      , proposta_adesao_tb.seguradora_cod_susep                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.ramo_id                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.nome                                                                                                                                                 " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.proposta_id                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.num_endosso                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.situacao                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "      , produto_tb.produto_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , produto_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.cliente_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , 'C'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , null                                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "FROM     proposta_adesao_tb with ( nolock )                                                                                                                          " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ( ( ( proposta_tb with ( nolock )                                                                                                                 " & vbNewLine
        sSQL = sSQL & "                         INNER JOIN proposta_complementar_tb with ( nolock ) ON proposta_tb.proposta_id = proposta_complementar_tb.proposta_id                       " & vbNewLine
        sSQL = sSQL & "                       )                                                                                                                                             " & vbNewLine
        sSQL = sSQL & "                       INNER JOIN produto_tb with ( nolock ) ON proposta_tb.produto_id = produto_tb.produto_id                                                       " & vbNewLine
        sSQL = sSQL & "                     )                                                                                                                                               " & vbNewLine
        sSQL = sSQL & "                     INNER JOIN cliente_tb with ( nolock ) ON proposta_complementar_tb.prop_cliente_id = cliente_tb.cliente_id                                       " & vbNewLine
        sSQL = sSQL & "                   ) ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id                                                                                     " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ramo_tb with ( nolock ) ON proposta_adesao_tb.ramo_id = ramo_tb.ramo_id                                                                           " & vbNewLine
        sSQL = sSQL & "                                              AND ramo_tb.tp_ramo_id = " & bytTipoRamo
        sSQL = sSQL & "        INNER JOIN apolice_terceiros_tb with ( nolock ) ON apolice_terceiros_tb.sucursal_seguradora_id = proposta_adesao_tb.sucursal_seguradora_id                   " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.apolice_id = proposta_adesao_tb.apolice_id                                       " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.seguradora_cod_susep = proposta_adesao_tb.seguradora_cod_susep                   " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.ramo_id = proposta_adesao_tb.ramo_id                                             " & vbNewLine
        sSQL = sSQL & "        INNER JOIN co_seguro_aceito_tb with ( nolock ) ON co_seguro_aceito_tb.num_ordem_co_seguro_aceito = apolice_terceiros_tb.num_ordem_co_seguro_aceito           " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.seg_cod_susep_lider = apolice_terceiros_tb.seg_cod_susep_lider                     " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.sucursal_seg_lider = apolice_terceiros_tb.sucursal_seg_lider                       " & vbNewLine
        sSQL = sSQL & "WHERE    co_seguro_aceito_tb.num_ordem_co_seguro_aceito = " & iNumOrdemCosseguro & vbNewLine
        sSQL = sSQL & "UNION ALL                                                                                                                                                            " & vbNewLine
        sSQL = sSQL & "                                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "SELECT  DISTINCT                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "        'n'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , apolice_tb.apolice_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , apolice_tb.sucursal_seguradora_id                                                                                                                            " & vbNewLine
        sSQL = sSQL & "      , apolice_tb.seguradora_cod_susep                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.ramo_id                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.nome                                                                                                                                                 " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.proposta_id                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.num_endosso                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.situacao                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "      , produto_tb.produto_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , produto_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.cliente_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , 'T'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , null                                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "FROM     proposta_fechada_tb with ( nolock )                                                                                                                         " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ( ( proposta_tb with ( nolock )                                                                                                                   " & vbNewLine
        sSQL = sSQL & "                       INNER JOIN produto_tb with ( nolock ) ON proposta_tb.produto_id = produto_tb.produto_id                                                       " & vbNewLine
        sSQL = sSQL & "                     )                                                                                                                                               " & vbNewLine
        sSQL = sSQL & "                     INNER JOIN cliente_tb with ( nolock ) ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id                                                    " & vbNewLine
        sSQL = sSQL & "                   ) ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                                                                                    " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ramo_tb with ( nolock ) ON proposta_tb.ramo_id = ramo_tb.ramo_id                                                                                  " & vbNewLine
        sSQL = sSQL & "                                              AND ramo_tb.tp_ramo_id = " & bytTipoRamo
        sSQL = sSQL & "        INNER JOIN apolice_tb with ( nolock ) ON proposta_tb.proposta_id = apolice_tb.proposta_id                                                                    " & vbNewLine
        sSQL = sSQL & "        INNER JOIN apolice_terceiros_tb with ( nolock ) ON apolice_terceiros_tb.sucursal_seguradora_id = apolice_tb.sucursal_seguradora_id                           " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.apolice_id = apolice_tb.apolice_id                                               " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.seguradora_cod_susep = apolice_tb.seguradora_cod_susep                           " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.ramo_id = apolice_tb.ramo_id                                                     " & vbNewLine
        sSQL = sSQL & "        INNER JOIN co_seguro_aceito_tb with ( nolock ) ON co_seguro_aceito_tb.num_ordem_co_seguro_aceito = apolice_terceiros_tb.num_ordem_co_seguro_aceito           " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.seg_cod_susep_lider = apolice_terceiros_tb.seg_cod_susep_lider                     " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.sucursal_seg_lider = apolice_terceiros_tb.sucursal_seg_lider                       " & vbNewLine
        sSQL = sSQL & "WHERE    co_seguro_aceito_tb.num_ordem_co_seguro_aceito= " & iNumOrdemCosseguro & vbNewLine
        sSQL = sSQL & "UNION ALL                                                                                                                                                            " & vbNewLine
        sSQL = sSQL & "                                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "SELECT  DISTINCT                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "        'n'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , apolice_tb.apolice_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , apolice_tb.sucursal_seguradora_id                                                                                                                            " & vbNewLine
        sSQL = sSQL & "      , apolice_tb.seguradora_cod_susep                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.ramo_id                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , ramo_tb.nome                                                                                                                                                 " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.proposta_id                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.num_endosso                                                                                                                                      " & vbNewLine
        sSQL = sSQL & "      , proposta_tb.situacao                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "      , produto_tb.produto_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , produto_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.cliente_id                                                                                                                                        " & vbNewLine
        sSQL = sSQL & "      , cliente_tb.nome                                                                                                                                              " & vbNewLine
        sSQL = sSQL & "      , 'C'                                                                                                                                                          " & vbNewLine
        sSQL = sSQL & "      , null                                                                                                                                                         " & vbNewLine
        sSQL = sSQL & "FROM     proposta_fechada_tb with ( nolock )                                                                                                                         " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ( ( ( proposta_tb with ( nolock )                                                                                                                 " & vbNewLine
        sSQL = sSQL & "                         INNER JOIN proposta_complementar_tb with ( nolock ) ON proposta_tb.proposta_id = proposta_complementar_tb.proposta_id                       " & vbNewLine
        sSQL = sSQL & "                       )                                                                                                                                             " & vbNewLine
        sSQL = sSQL & "                       INNER JOIN produto_tb with ( nolock ) ON proposta_tb.produto_id = produto_tb.produto_id                                                       " & vbNewLine
        sSQL = sSQL & "                     )                                                                                                                                               " & vbNewLine
        sSQL = sSQL & "                     INNER JOIN cliente_tb with ( nolock ) ON proposta_complementar_tb.prop_cliente_id = cliente_tb.cliente_id                                       " & vbNewLine
        sSQL = sSQL & "                   ) ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id                                                                                    " & vbNewLine
        sSQL = sSQL & "        INNER JOIN ramo_tb with ( nolock ) ON proposta_tb.ramo_id = ramo_tb.ramo_id                                                                                  " & vbNewLine
        sSQL = sSQL & "                                              AND ramo_tb.tp_ramo_id = " & bytTipoRamo
        sSQL = sSQL & "        INNER JOIN apolice_tb with ( nolock ) ON proposta_tb.proposta_id = apolice_tb.proposta_id                                                                    " & vbNewLine
        sSQL = sSQL & "        INNER JOIN apolice_terceiros_tb with ( nolock ) ON apolice_terceiros_tb.sucursal_seguradora_id = apolice_tb.sucursal_seguradora_id                           " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.apolice_id = apolice_tb.apolice_id                                               " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.seguradora_cod_susep = apolice_tb.seguradora_cod_susep                           " & vbNewLine
        sSQL = sSQL & "                                                           AND apolice_terceiros_tb.ramo_id = apolice_tb.ramo_id                                                     " & vbNewLine
        sSQL = sSQL & "        INNER JOIN co_seguro_aceito_tb with ( nolock ) ON co_seguro_aceito_tb.num_ordem_co_seguro_aceito = apolice_terceiros_tb.num_ordem_co_seguro_aceito           " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.seg_cod_susep_lider = apolice_terceiros_tb.seg_cod_susep_lider                     " & vbNewLine
        sSQL = sSQL & "                                                          AND co_seguro_aceito_tb.sucursal_seg_lider = apolice_terceiros_tb.sucursal_seg_lider                       " & vbNewLine
        sSQL = sSQL & "WHERE    co_seguro_aceito_tb.num_ordem_co_seguro_aceito = " & iNumOrdemCosseguro & vbNewLine
        sSQL = sSQL & "                                                                                                                                                                     " & vbNewLine
        sSQL = sSQL & "                                                                                                                                                                     " & vbNewLine
    Else

        sSQL = sSQL & "       Insert into #tempsel_tb (                         " & vbCrLf
        sSQL = sSQL & "       marca                                             " & vbCrLf
        sSQL = sSQL & "     , apolice_id                                        " & vbCrLf
        sSQL = sSQL & "     , sucursal_seguradora_id                            " & vbCrLf
        sSQL = sSQL & "     , seguradora_cod_susep                              " & vbCrLf
        sSQL = sSQL & "     , ramo_id                                           " & vbCrLf
        sSQL = sSQL & "     , nomeramo                                          " & vbCrLf
        sSQL = sSQL & "     , proposta_id                                       " & vbCrLf
        sSQL = sSQL & "     , numendosso                                        " & vbCrLf
        sSQL = sSQL & "     , sitproposta                                       " & vbCrLf
        sSQL = sSQL & "     , produto_id                                        " & vbCrLf
        sSQL = sSQL & "     , nomeproduto                                       " & vbCrLf
        sSQL = sSQL & "     , cliente_id                                        " & vbCrLf
        sSQL = sSQL & "     , nomecliente                                       " & vbCrLf
        sSQL = sSQL & "     , tipoComponente                                    " & vbCrLf
        sSQL = sSQL & "     , sinistro_id                                       " & vbCrLf
        sSQL = sSQL & "     , situacao                                          " & vbCrLf
        sSQL = sSQL & "     , situacao_desc                                     " & vbCrLf
        sSQL = sSQL & "     , situacao_evento                                      " & vbCrLf
        sSQL = sSQL & "     , sit_evento_desc                                      " & vbCrLf
        sSQL = sSQL & "     , Sucursal_Nome                                        " & vbCrLf
        sSQL = sSQL & "     , Endosso                                              " & vbCrLf
        sSQL = sSQL & "     , sinistro_id_lider                                    " & vbCrLf
        sSQL = sSQL & "     , processa_reintegracao_is                             " & vbCrLf
        sSQL = sSQL & "     , historico                                            " & vbCrLf
        sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                               " & vbCrLf 'cristovao.rodrigues 08/11/2012
        sSQL = sSQL & "     )                                                   " & vbCrLf



        sSQL = sSQL & " select distinct                                     " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.lock_aviso,                         " & vbCrLf
        sSQL = sSQL & "     proposta_adesao_tb.apolice_id,                  " & vbCrLf
        sSQL = sSQL & "     proposta_adesao_tb.sucursal_seguradora_id,      " & vbCrLf
        sSQL = sSQL & "     proposta_adesao_tb.seguradora_cod_susep,        " & vbCrLf
        sSQL = sSQL & "     ramo_tb.ramo_id,                                " & vbCrLf
        sSQL = sSQL & "     ramo_tb.nome,                                   " & vbCrLf
        sSQL = sSQL & "     proposta_tb.proposta_id,                        " & vbCrLf
        sSQL = sSQL & "     proposta_tb.num_endosso,                        " & vbCrLf
        sSQL = sSQL & "     proposta_tb.situacao,                           " & vbCrLf
        sSQL = sSQL & "     produto_tb.produto_id,                          " & vbCrLf
        sSQL = sSQL & "     produto_tb.nome,                                " & vbCrLf
        sSQL = sSQL & "     cliente_tb.cliente_id,                          " & vbCrLf
        sSQL = sSQL & "     cliente_tb.nome,                                " & vbCrLf
        sSQL = sSQL & "     tipocomponente  = 't',                          " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id,                        " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.situacao,                           " & vbCrLf
        sSQL = sSQL & "     situacao_desc   = '',                           " & vbCrLf
        sSQL = sSQL & "     situacao_evento = '',                           " & vbCrLf
        sSQL = sSQL & "     sit_evento_desc = '',                           " & vbCrLf
        sSQL = sSQL & "     sucursal_nome   = null,                         " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.endosso_id,                         " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id_lider,                  " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.processa_reintegracao_is,           " & vbCrLf
        'mathayde
        'sSQL = sSQL & "     historico = ''                                  " & vbCrLf
        sSQL = sSQL & "     historico = '',                                 " & vbCrLf
        sSQL = sSQL & "     dt_ocorrencia_sinistro                          " & vbCrLf
        sSQL = sSQL & " from proposta_adesao_tb  WITH (NOLOCK)                 " & vbCrLf
        sSQL = sSQL & " inner join apolice_terceiros_tb  WITH (NOLOCK)         " & vbCrLf
        sSQL = sSQL & "     on proposta_adesao_tb.apolice_id                =  apolice_terceiros_tb.apolice_id              " & vbCrLf
        sSQL = sSQL & "     and proposta_adesao_tb.sucursal_seguradora_id   =  apolice_terceiros_tb.sucursal_seguradora_id  " & vbCrLf
        sSQL = sSQL & "     and proposta_adesao_tb.seguradora_cod_susep     =  apolice_terceiros_tb.seguradora_cod_susep    " & vbCrLf
        sSQL = sSQL & "     and proposta_adesao_tb.ramo_id                  =  apolice_terceiros_tb.ramo_id                 " & vbCrLf
        sSQL = sSQL & " inner join ramo_tb  WITH (NOLOCK)                                                                      " & vbCrLf
        sSQL = sSQL & "     on proposta_adesao_tb.ramo_id                   =  ramo_tb.ramo_id                              " & vbCrLf
        sSQL = sSQL & "     and ramo_tb.tp_ramo_id                          =  1                                            " & vbCrLf
        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                                                  " & vbCrLf
        sSQL = sSQL & "  on proposta_adesao_tb.proposta_id                  =  proposta_tb.proposta_id                      " & vbCrLf
        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                                                                   " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.prop_cliente_id                  =  cliente_tb.cliente_id                        " & vbCrLf
        sSQL = sSQL & " inner join produto_tb  WITH (NOLOCK)                                                                   " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.produto_id                       = produto_tb.produto_id                         " & vbCrLf
        sSQL = sSQL & "  inner join sinistro_tb  WITH (NOLOCK)                                                                 " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.apolice_id                       =  proposta_adesao_tb.apolice_id                " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.sucursal_seguradora_id          =  proposta_adesao_tb.sucursal_seguradora_id    " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.seguradora_cod_susep            =  proposta_adesao_tb.seguradora_cod_susep      " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.ramo_id                         =  proposta_adesao_tb.ramo_id                   " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.proposta_id                     =  proposta_adesao_tb.proposta_id               " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.cliente_id                      =  cliente_tb.cliente_id                        " & vbCrLf
        sSQL = sSQL & " inner join co_seguro_aceito_tb  WITH (NOLOCK)                                                          " & vbCrLf
        sSQL = sSQL & "     on co_seguro_aceito_tb.seg_cod_susep_lider      =  apolice_terceiros_tb.seg_cod_susep_lider     " & vbCrLf
        sSQL = sSQL & "     and co_seguro_aceito_tb.num_ordem_co_seguro_aceito=  apolice_terceiros_tb.num_ordem_co_seguro_aceito    " & vbCrLf
        sSQL = sSQL & " where                                                                                                       " & vbCrLf
        sSQL = sSQL & "     co_seguro_aceito_tb.num_ordem_co_seguro_aceito          =  " & iNumOrdemCosseguro & vbCrLf
        sSQL = sSQL & " union all                                           " & vbCrLf
        ' /* conjuge */
        sSQL = sSQL & " select distinct                                     " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.lock_aviso,                         " & vbCrLf
        sSQL = sSQL & "     proposta_adesao_tb.apolice_id,                  " & vbCrLf
        sSQL = sSQL & "     proposta_adesao_tb.sucursal_seguradora_id,      " & vbCrLf
        sSQL = sSQL & "     proposta_adesao_tb.seguradora_cod_susep,        " & vbCrLf
        sSQL = sSQL & "     ramo_tb.ramo_id,                                " & vbCrLf
        sSQL = sSQL & "     ramo_tb.nome,                                   " & vbCrLf
        sSQL = sSQL & "     proposta_tb.proposta_id,                        " & vbCrLf
        sSQL = sSQL & "     proposta_tb.num_endosso,                        " & vbCrLf
        sSQL = sSQL & "     proposta_tb.situacao,                           " & vbCrLf
        sSQL = sSQL & "     produto_tb.produto_id,                          " & vbCrLf
        sSQL = sSQL & "     produto_tb.nome,                                " & vbCrLf
        sSQL = sSQL & "     cliente_tb.cliente_id,                          " & vbCrLf
        sSQL = sSQL & "     cliente_tb.nome,                                " & vbCrLf
        sSQL = sSQL & "     tipocomponente  = 'c',                          " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id,                        " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.situacao,                           " & vbCrLf
        sSQL = sSQL & "     situacao_desc   = '',                           " & vbCrLf
        sSQL = sSQL & "     situacao_evento = '',                           " & vbCrLf
        sSQL = sSQL & "     sit_evento_desc = '',                           " & vbCrLf
        sSQL = sSQL & "     sucursal_nome   = null,                         " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.endosso_id,                         " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id_lider,                  " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.processa_reintegracao_is,           " & vbCrLf
        'mathayde
        'sSQL = sSQL & "     historico = ''                                  " & vbCrLf
        sSQL = sSQL & "     historico = '',                                 " & vbCrLf
        sSQL = sSQL & "     dt_ocorrencia_sinistro                          " & vbCrLf
        sSQL = sSQL & " from proposta_adesao_tb  WITH (NOLOCK)                 " & vbCrLf
        sSQL = sSQL & " inner join apolice_terceiros_tb  WITH (NOLOCK)         " & vbCrLf
        sSQL = sSQL & "     on proposta_adesao_tb.apolice_id                =  apolice_terceiros_tb.apolice_id              " & vbCrLf
        sSQL = sSQL & "     and proposta_adesao_tb.sucursal_seguradora_id   =  apolice_terceiros_tb.sucursal_seguradora_id  " & vbCrLf
        sSQL = sSQL & "     and proposta_adesao_tb.seguradora_cod_susep     =  apolice_terceiros_tb.seguradora_cod_susep    " & vbCrLf
        sSQL = sSQL & "     and proposta_adesao_tb.ramo_id                  =  apolice_terceiros_tb.ramo_id                 " & vbCrLf
        sSQL = sSQL & " inner join ramo_tb  WITH (NOLOCK)                                                                      " & vbCrLf
        sSQL = sSQL & "     on proposta_adesao_tb.ramo_id                   =  ramo_tb.ramo_id                              " & vbCrLf
        sSQL = sSQL & "     and ramo_tb.tp_ramo_id                          =  1                                            " & vbCrLf
        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)                                                                  " & vbCrLf
        sSQL = sSQL & "    on proposta_adesao_tb.proposta_id                =  proposta_tb.proposta_id                      " & vbCrLf
        sSQL = sSQL & " inner join proposta_complementar_tb  WITH (NOLOCK)                                                     " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.proposta_id                      =  proposta_complementar_tb.proposta_id         " & vbCrLf
        sSQL = sSQL & " inner join produto_tb  WITH (NOLOCK)                                                                   " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.produto_id                       =  produto_tb.produto_id                        " & vbCrLf
        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                                                                   " & vbCrLf
        sSQL = sSQL & "     on proposta_complementar_tb.prop_cliente_id     =  cliente_tb.cliente_id                        " & vbCrLf
        sSQL = sSQL & " inner join sinistro_tb  WITH (NOLOCK)                                                                  " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.apolice_id                       =  proposta_adesao_tb.apolice_id                " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.sucursal_seguradora_id          =  proposta_adesao_tb.sucursal_seguradora_id    " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.seguradora_cod_susep            =  proposta_adesao_tb.seguradora_cod_susep      " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.ramo_id                         =  proposta_adesao_tb.ramo_id                   " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.proposta_id                     =  proposta_adesao_tb.proposta_id               " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.cliente_id                      =  cliente_tb.cliente_id                        " & vbCrLf
        sSQL = sSQL & " inner join co_seguro_aceito_tb  WITH (NOLOCK)                                                          " & vbCrLf
        sSQL = sSQL & "     on co_seguro_aceito_tb.seg_cod_susep_lider      =  apolice_terceiros_tb.seg_cod_susep_lider     " & vbCrLf
        sSQL = sSQL & "     and co_seguro_aceito_tb.num_ordem_co_seguro_aceito=  apolice_terceiros_tb.num_ordem_co_seguro_aceito    " & vbCrLf
        sSQL = sSQL & " where                                                                                                       " & vbCrLf
        sSQL = sSQL & "     co_seguro_aceito_tb.num_ordem_co_seguro_aceito          =  " & iNumOrdemCosseguro & vbCrLf
        sSQL = sSQL & " union all                                                   " & vbCrLf
        ' /* titular */
        sSQL = sSQL & " select distinct                             " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.lock_aviso,                 " & vbCrLf
        sSQL = sSQL & "     apolice.apolice_id,                     " & vbCrLf
        sSQL = sSQL & "     apolice.sucursal_seguradora_id,         " & vbCrLf
        sSQL = sSQL & "     apolice.seguradora_cod_susep,           " & vbCrLf
        sSQL = sSQL & "     ramo_tb.ramo_id,                        " & vbCrLf
        sSQL = sSQL & "     ramo_tb.nome,                           " & vbCrLf
        sSQL = sSQL & "     proposta_tb.proposta_id,                " & vbCrLf
        sSQL = sSQL & "     proposta_tb.num_endosso,                " & vbCrLf
        sSQL = sSQL & "     proposta_tb.situacao,                   " & vbCrLf
        sSQL = sSQL & "     produto_tb.produto_id,                  " & vbCrLf
        sSQL = sSQL & "     produto_tb.nome,                        " & vbCrLf
        sSQL = sSQL & "     cliente_tb.cliente_id,                  " & vbCrLf
        sSQL = sSQL & "     cliente_tb.nome,                        " & vbCrLf
        sSQL = sSQL & "     tipocomponente  = 't',                  " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id,                " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.situacao,                   " & vbCrLf
        sSQL = sSQL & "     situacao_desc   = '',                   " & vbCrLf
        sSQL = sSQL & "     situacao_evento = '',                   " & vbCrLf
        sSQL = sSQL & "     sit_evento_desc = '',                   " & vbCrLf
        sSQL = sSQL & "     sucursal_nome   = null,                 " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.endosso_id,                 " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id_lider,          " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.processa_reintegracao_is,   " & vbCrLf
        'mathayde
        'sSQL = sSQL & "     historico = ''                                  " & vbCrLf
        sSQL = sSQL & "     historico = '',                                 " & vbCrLf
        sSQL = sSQL & "     dt_ocorrencia_sinistro                          " & vbCrLf
        sSQL = sSQL & " from proposta_fechada_tb  WITH (NOLOCK)        " & vbCrLf
        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)          " & vbCrLf
        sSQL = sSQL & "     on proposta_fechada_tb.proposta_id          =  proposta_tb.proposta_id                                  " & vbCrLf
        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                                                                           " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.prop_cliente_id              =  cliente_tb.cliente_id                                    " & vbCrLf
        sSQL = sSQL & "  inner join ramo_tb  WITH (NOLOCK)                                                                             " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.ramo_id                      =  ramo_tb.ramo_id                                          " & vbCrLf
        sSQL = sSQL & "     and ramo_tb.tp_ramo_id                      =  1                                                        " & vbCrLf
        sSQL = sSQL & " inner join produto_tb  WITH (NOLOCK)                                                                           " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.produto_id                   = produto_tb.produto_id                                     " & vbCrLf
        sSQL = sSQL & " inner join apolice_tb apolice  WITH (NOLOCK)                                                                   " & vbCrLf
        sSQL = sSQL & "     on apolice.proposta_id                      =  proposta_tb.proposta_id                                  " & vbCrLf
        sSQL = sSQL & " inner join apolice_terceiros_tb  WITH (NOLOCK)                                                                 " & vbCrLf
        sSQL = sSQL & "     on apolice.apolice_id                       =  apolice_terceiros_tb.apolice_id                          " & vbCrLf
        sSQL = sSQL & "     and apolice.sucursal_seguradora_id          =  apolice_terceiros_tb.sucursal_seguradora_id              " & vbCrLf
        sSQL = sSQL & "     and apolice.seguradora_cod_susep            =  apolice_terceiros_tb.seguradora_cod_susep                " & vbCrLf
        sSQL = sSQL & "     and proposta_tb.ramo_id                     =  apolice_terceiros_tb.ramo_id                             " & vbCrLf
        sSQL = sSQL & " inner join co_seguro_aceito_tb  WITH (NOLOCK)                                                                  " & vbCrLf
        sSQL = sSQL & "     on co_seguro_aceito_tb.seg_cod_susep_lider  =  apolice_terceiros_tb.seg_cod_susep_lider                 " & vbCrLf
        sSQL = sSQL & "     and co_seguro_aceito_tb.num_ordem_co_seguro_aceito=  apolice_terceiros_tb.num_ordem_co_seguro_aceito    " & vbCrLf
        sSQL = sSQL & " inner join sinistro_tb  WITH (NOLOCK)                                                                          " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.apolice_id    =  apolice.apolice_id                                                      " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.sucursal_seguradora_id      =  apolice.sucursal_seguradora_id                           " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.sinistro_id_lider           =  co_seguro_aceito_tb.num_apolice_lider                    " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.seguradora_cod_susep        =  apolice.seguradora_cod_susep                             " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.ramo_id                     =  proposta_tb.ramo_id                                      " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.proposta_id                 =  proposta_fechada_tb.proposta_id                          " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.cliente_id                  =  cliente_tb.cliente_id                                    " & vbCrLf
        sSQL = sSQL & " where                                                                                                       " & vbCrLf
        sSQL = sSQL & "     co_seguro_aceito_tb.num_ordem_co_seguro_aceito     =  " & iNumOrdemCosseguro & vbCrLf
        sSQL = sSQL & " union all                                   " & vbCrLf
        '/* conjuge */
        sSQL = sSQL & " select distinct                             " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.lock_aviso,                 " & vbCrLf
        sSQL = sSQL & "     apolice.apolice_id,                     " & vbCrLf
        sSQL = sSQL & "     apolice.sucursal_seguradora_id,         " & vbCrLf
        sSQL = sSQL & "     apolice.seguradora_cod_susep,           " & vbCrLf
        sSQL = sSQL & "     ramo_tb.ramo_id,                        " & vbCrLf
        sSQL = sSQL & "     ramo_tb.nome,                           " & vbCrLf
        sSQL = sSQL & "     proposta_tb.proposta_id,                " & vbCrLf
        sSQL = sSQL & "     proposta_tb.num_endosso,                " & vbCrLf
        sSQL = sSQL & "     proposta_tb.situacao,                   " & vbCrLf
        sSQL = sSQL & "     produto_tb.produto_id,                  " & vbCrLf
        sSQL = sSQL & "     produto_tb.nome,                        " & vbCrLf
        sSQL = sSQL & "     cliente_tb.cliente_id,                  " & vbCrLf
        sSQL = sSQL & "     cliente_tb.nome,                        " & vbCrLf
        sSQL = sSQL & "     tipocomponente  = 'c',                  " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id,                " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.situacao,                   " & vbCrLf
        sSQL = sSQL & "     situacao_desc   = '',                   " & vbCrLf
        sSQL = sSQL & "     situacao_evento = '',                   " & vbCrLf
        sSQL = sSQL & "     sit_evento_desc = '',                   " & vbCrLf
        sSQL = sSQL & "     sucursal_nome   = null,                 " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.endosso_id,                 " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.sinistro_id_lider,          " & vbCrLf
        sSQL = sSQL & "     sinistro_tb.processa_reintegracao_is,   " & vbCrLf
        'mathayde
        'sSQL = sSQL & "     historico = ''                                  " & vbCrLf
        sSQL = sSQL & "     historico = '',                                 " & vbCrLf
        sSQL = sSQL & "     dt_ocorrencia_sinistro                          " & vbCrLf
        sSQL = sSQL & " from proposta_fechada_tb  WITH (NOLOCK)        " & vbCrLf
        sSQL = sSQL & " inner join proposta_tb  WITH (NOLOCK)          " & vbCrLf
        sSQL = sSQL & "     on proposta_fechada_tb.proposta_id              =  proposta_tb.proposta_id                              " & vbCrLf
        sSQL = sSQL & " inner join proposta_complementar_tb  WITH (NOLOCK)                                                             " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.proposta_id                      =  proposta_complementar_tb.proposta_id                 " & vbCrLf
        sSQL = sSQL & " inner join produto_tb  WITH (NOLOCK)                                                                           " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.produto_id                       = produto_tb.produto_id                                 " & vbCrLf
        sSQL = sSQL & " inner join cliente_tb  WITH (NOLOCK)                                                                           " & vbCrLf
        sSQL = sSQL & "     on proposta_complementar_tb.prop_cliente_id     =  cliente_tb.cliente_id                                " & vbCrLf
        sSQL = sSQL & " inner join ramo_tb  WITH (NOLOCK)                                                                              " & vbCrLf
        sSQL = sSQL & "     on proposta_tb.ramo_id                          =  ramo_tb.ramo_id                                      " & vbCrLf
        sSQL = sSQL & "     and ramo_tb.tp_ramo_id                          =  1                                                    " & vbCrLf
        sSQL = sSQL & " inner join apolice_tb apolice  WITH (NOLOCK)                                                                   " & vbCrLf
        sSQL = sSQL & "     on apolice.proposta_id                          =  proposta_tb.proposta_id                              " & vbCrLf
        sSQL = sSQL & " inner join apolice_terceiros_tb  WITH (NOLOCK)                                                                 " & vbCrLf
        sSQL = sSQL & "     on apolice.apolice_id                           =  apolice_terceiros_tb.apolice_id                      " & vbCrLf
        sSQL = sSQL & "     and apolice.sucursal_seguradora_id              =  apolice_terceiros_tb.sucursal_seguradora_id          " & vbCrLf
        sSQL = sSQL & "     and apolice.seguradora_cod_susep                =  apolice_terceiros_tb.seguradora_cod_susep            " & vbCrLf
        sSQL = sSQL & "     and proposta_tb.ramo_id                         =  apolice_terceiros_tb.ramo_id                         " & vbCrLf
        sSQL = sSQL & " inner join co_seguro_aceito_tb  WITH (NOLOCK)                                                                  " & vbCrLf
        sSQL = sSQL & "     on co_seguro_aceito_tb.seg_cod_susep_lider      =  apolice_terceiros_tb.seg_cod_susep_lider             " & vbCrLf
        sSQL = sSQL & "     and co_seguro_aceito_tb.num_ordem_co_seguro_aceito=  apolice_terceiros_tb.num_ordem_co_seguro_aceito    " & vbCrLf
        sSQL = sSQL & " inner join sinistro_tb  WITH (NOLOCK)                                                                          " & vbCrLf
        sSQL = sSQL & "     on sinistro_tb.apolice_id                       =  apolice.apolice_id                                   " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.sucursal_seguradora_id          =  apolice.sucursal_seguradora_id                       " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.sinistro_id_lider               =  co_seguro_aceito_tb.num_apolice_lider                " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.seguradora_cod_susep            =  apolice.seguradora_cod_susep                         " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.ramo_id                         =  proposta_tb.ramo_id                                  " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.proposta_id                     =  proposta_fechada_tb.proposta_id                      " & vbCrLf
        sSQL = sSQL & "     and sinistro_tb.cliente_id                      =  cliente_tb.cliente_id                                " & vbCrLf
        sSQL = sSQL & " where                                                                                                       " & vbCrLf
        sSQL = sSQL & "    co_seguro_aceito_tb.num_ordem_co_seguro_aceito          =  " & iNumOrdemCosseguro & vbCrLf
    End If

    sSQL = sSQL & "" & vbNewLine
    sSQL = sSQL & "Exec Seguros_Db.Dbo.SEGS10420_SPS    "


    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sSQL, _
                         lConexaoLocal, _
                         False)
                         
   Exit Sub

TrataErro:
Call TrataErroGeral("ConsultaSinistroPor_NumOrdemCosseguro", Me.name)
        
End Sub

' Cesar Santos CONFITEC - (SBRJ009952) 04/06/2020 Inicio
Private Function ConsultaSinistroPor_Sinistro_Cardif_RamoVIDA(Sinistro_Cadif_ID As String)
    Dim rsRecordSet  As ADODB.Recordset
    Dim sSQL         As String
    Dim dSinistro_ID As Double
    
    On Error GoTo Erro
    
     
    sSQL = ""
    sSQL = "exec seguros_db.dbo.SEGS14884_SPS "
    sSQL = sSQL & Sinistro_Cadif_ID & ", "
    sSQL = sSQL & 0 & ", "
    sSQL = sSQL & "NULL" & ", "
    sSQL = sSQL & "NULL"
    

Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, lConexaoLocal, True)
            
dSinistro_ID = 0

If Not rsRecordSet.EOF Then
    dSinistro_ID = CDbl(rsRecordSet(0))
End If
                      
ConsultaSinistroPor_Sinistro_RamoVIDA dSinistro_ID
                      
Exit Function
    
Erro:
    Call TrataErroGeral("ConsultaSinistroPor_Sinistro_RamoVIDA", Me.name)
    Call FinalizarAplicacao

End Function
' Cesar Santos CONFITEC - (SBRJ009952) 04/06/2020 Fim

Private Sub CarregaDadosGrid_Selecao()

    Dim sSQL As String
    Dim Linha As String
    Dim i As Integer
    Dim b_primeira_linha_usada As Boolean, irow As Integer
   
    Dim rs As New ADODB.Recordset
    Dim Rs1 As New ADODB.Recordset
    Dim rsRecordSet                             As New ADODB.Recordset
    Dim bProcessa                               As Boolean
    Dim bValidaRamoPesquisa                     As Integer
    Dim strSQLAux                               As String
    Dim rsCritPesquisa                          As New ADODB.Recordset
    Dim auxProdutoPesquisa                      As Integer
    Dim auxRamoPesquisa                         As Integer
    Dim auxTpAreaPesquisa                       As String
    Dim auxCountRow                             As Integer
    Dim intAlterRow                             As Integer
    Dim intAlterCol                             As Integer
        
    On Error GoTo TrataErro
    
    
    grdSelecao.Rows = 1
    
    bValidaRamoPesquisa = 0
    
    Select Case cmbCriterioPesquisa.Text
          Case "N�mero Sinistro"
            
            strSQLAux = ""
            strSQLAux = strSQLAux & " select b.produto_id, b.ramo_id, c.tp_area " & vbNewLine
            strSQLAux = strSQLAux & "   from seguros_db.dbo.sinistro_tb a with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "  inner join seguros_db.dbo.proposta_tb b with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "     on a.proposta_id = b.proposta_id " & vbNewLine
            strSQLAux = strSQLAux & "  inner join seguros_db.dbo.ramo_tb c with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "     on b.ramo_id = c.ramo_id " & vbNewLine
            strSQLAux = strSQLAux & "  where a.sinistro_id = " & txtConteudo.Text & vbNewLine
            
            Set rsCritPesquisa = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, strSQLAux, lConexaoLocal, True)
            
            If Not rsCritPesquisa.EOF Then
                auxProdutoPesquisa = rsCritPesquisa(0)
                auxRamoPesquisa = rsCritPesquisa(1)
                
                If rsCritPesquisa(2) = "RU" Or rsCritPesquisa(1) = 61 Or rsCritPesquisa(1) = 68 Then
                    bValidaRamoPesquisa = 1
                End If
            End If
            
            rsCritPesquisa.Close
            Set rsCritPesquisa = Nothing
            
          Case "N�mero Sinistro BB"

            strSQLAux = ""
            strSQLAux = strSQLAux & " select b.produto_id, b.ramo_id, d.tp_area " & vbNewLine
            strSQLAux = strSQLAux & "   from seguros_db.dbo.sinistro_tb a with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "  inner join seguros_db.dbo.proposta_tb b with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "     on a.proposta_id = b.proposta_id " & vbNewLine
            strSQLAux = strSQLAux & "  inner join seguros_db.dbo.sinistro_bb_tb c with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "     on a.sinistro_id = c.sinistro_id " & vbNewLine
            strSQLAux = strSQLAux & "  inner join seguros_db.dbo.ramo_tb d with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "     on b.ramo_id = d.ramo_id " & vbNewLine
            strSQLAux = strSQLAux & "  where c.sinistro_bb = " & txtConteudo.Text & vbNewLine
            
            Set rsCritPesquisa = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, strSQLAux, lConexaoLocal, True)
            
            If Not rsCritPesquisa.EOF Then
                auxProdutoPesquisa = rsCritPesquisa(0)
                auxRamoPesquisa = rsCritPesquisa(1)
                
                If rsCritPesquisa(2) = "RU" Or rsCritPesquisa(1) = 61 Or rsCritPesquisa(1) = 68 Then
                    bValidaRamoPesquisa = 1
                End If
            End If
            
            rsCritPesquisa.Close
            Set rsCritPesquisa = Nothing
            
          Case "Proposta"

            strSQLAux = ""
            strSQLAux = strSQLAux & " select a.produto_id, a.ramo_id, b.tp_area " & vbNewLine
            strSQLAux = strSQLAux & "   from seguros_db.dbo.proposta_tb a with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "  inner join seguros_db.dbo.ramo_tb b with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "     on a.ramo_id = b.ramo_id " & vbNewLine
            strSQLAux = strSQLAux & "  where a.proposta_id = " & txtConteudo.Text & vbNewLine
            
            Set rsCritPesquisa = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, strSQLAux, lConexaoLocal, True)
            
            If Not rsCritPesquisa.EOF Then
                auxProdutoPesquisa = rsCritPesquisa(0)
                auxRamoPesquisa = rsCritPesquisa(1)
                
                If rsCritPesquisa(2) = "RU" Or rsCritPesquisa(1) = 61 Or rsCritPesquisa(1) = 68 Then
                    bValidaRamoPesquisa = 1
                End If
            End If
            
            rsCritPesquisa.Close
            Set rsCritPesquisa = Nothing
            
          Case "Proposta BB"
            
            strSQLAux = ""
            strSQLAux = strSQLAux & " select a.produto_id, a.ramo_id, c.tp_area " & vbNewLine
            strSQLAux = strSQLAux & "   from seguros_db.dbo.proposta_tb a with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "  inner join seguros_db.dbo.proposta_fechada_tb b with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "     on a.proposta_id = b.proposta_id " & vbNewLine
            strSQLAux = strSQLAux & "  inner join seguros_db.dbo.ramo_tb c with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "     on a.ramo_id = c.ramo_id " & vbNewLine
            strSQLAux = strSQLAux & "  where b.proposta_bb = " & txtConteudo.Text & vbNewLine
            strSQLAux = strSQLAux & "  union " & vbNewLine
            strSQLAux = strSQLAux & " select a.produto_id, a.ramo_id, c.tp_area " & vbNewLine
            strSQLAux = strSQLAux & "   from seguros_db.dbo.proposta_tb a with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "  inner join seguros_db.dbo.proposta_adesao_tb b with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "     on a.proposta_id = b.proposta_id " & vbNewLine
            strSQLAux = strSQLAux & "  inner join seguros_db.dbo.ramo_tb c with (nolock) " & vbNewLine
            strSQLAux = strSQLAux & "     on a.ramo_id = c.ramo_id " & vbNewLine
            strSQLAux = strSQLAux & "  where b.proposta_bb = " & txtConteudo.Text & vbNewLine
            
            Set rsCritPesquisa = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, strSQLAux, lConexaoLocal, True)
            
            If Not rsCritPesquisa.EOF Then
                auxProdutoPesquisa = rsCritPesquisa(0)
                auxRamoPesquisa = rsCritPesquisa(1)
                
                If rsCritPesquisa(2) = "RU" Or rsCritPesquisa(1) = 61 Or rsCritPesquisa(1) = 68 Then
                    bValidaRamoPesquisa = 1
                End If
            End If
            
            rsCritPesquisa.Close
            Set rsCritPesquisa = Nothing
            
    End Select
    
'+----------------------------------------------------------------------
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'|
    
    sSQL = "SET NOCOUNT ON  Exec SEGS13117_SPS " & strTipoProcesso
    
    If bValidaRamoPesquisa <> 0 Then
        sSQL = sSQL & ", 'RU' "
    End If
    
'    sSQL = ""
'    sSQL = sSQL & "select #tempsel_tb.Marca                             " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.sinistro_id                       " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.NomeCliente                       " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.Apolice_ID                        " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.Proposta_id                       " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.Sucursal_Seguradora_ID            " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.seguradora_cod_susep              " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.Produto_ID                        " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.NomeProduto                       " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.ramo_id                           " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.NomeRamo                          " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.Situacao_Desc                     " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.Sit_Evento_Desc                   " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.Endosso                           " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.Sinistro_ID_Lider                 " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.Historico                         " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.Produto_ID                        " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.ramo_id                           " & vbNewLine
'    sSQL = sSQL & "     , #tempsel_tb.Dt_Ocorrencia_Sinistro            " & vbNewLine
'
''    'Alan Neves - 07/02/2013
'    sSQL = sSQL & "     , #tempsel_tb.tp_emissao           " & vbNewLine
'    sSQL = sSQL & "     , #TempSel_Tb.Sem_Comunicacao_BB    " & vbNewLine               'AKIO.OKUNO - 17/04/2013
'    sSQL = sSQL & "     , #TempSel_Tb.Seg_Cliente           " & vbNewLine               'AKIO.OKUNO - 17/04/2013
'    sSQL = sSQL & "     , #TempSel_Tb.Apolice_Dt_Emissao    " & vbNewLine               'AKIO.OKUNO - 21/04/2013
'    sSQL = sSQL & "     , #TempSel_Tb.SitProposta           " & vbNewLine               'Cleber Sardo - 23/04/2014 - INC000004276922 - Tratamento de proposta duplicada
'    sSQL = sSQL & " from #tempsel_tb                        " & vbCrLf
'
''AKIO.OKUNO / ALAN.NEVES - INICIO - 19/04/2013
''    'Alan Neves - comentado 07/02/2013
''    sSQL = sSQL & " LEFT join sinistro_tb s                              " & vbCrLf     'AKIO.OKUNO - 25/09/2012
''    sSQL = sSQL & "    on s.sinistro_id = #tempsel_tb.sinistro_id  " & vbCrLf           'AKIO.OKUNO - 25/09/2012
''AKIO.OKUNO / ALAN.NEVES - FIM - 19/04/2013
'
'    'Alan Neves - 18/04/2013 - Retirada do filtro - Inicio
'    'sSQL = sSQL & "   AND s.situacao <> 7" & vbCrLf                                     'AKIO.OKUNO - 25/09/2012
'    'Alan Neves - 18/04/2013 - Retirada do filtro - Fim
'
'    If strTipoProcesso = strTipoProcesso_Avisar Or _
'       strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'        sSQL = sSQL & "     where #tempsel_tb.sinistro_id is null                   " & vbCrLf
''    Else                                                                                                   'AKIO.OKUNO / ALAN.NEVES - 19/04/2013
''        sSQL = sSQL & "     where #tempsel_tb.sinistro_id is not null                   " & vbCrLf         'AKIO.OKUNO / ALAN.NEVES - 19/04/2013
'    End If
'
''AKIO.OKUNO / ALAN.NEVES - INICIO - 19/04/2013
''    sSQL = sSQL & " union                                               " & vbCrLf
''    sSQL = sSQL & "select #TempSel_Tb.Marca                             " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.sinistro_id                       " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.NomeCliente                       " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.Apolice_ID                        " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.Proposta_id                       " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.Sucursal_Seguradora_ID            " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.seguradora_cod_susep              " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.Produto_ID                        " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.NomeProduto                       " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.ramo_id                           " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.NomeRamo                          " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.Situacao_Desc                     " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.Sit_Evento_Desc                   " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.Endosso                           " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.Sinistro_ID_Lider                 " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.Historico                         " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.Produto_ID                        " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.ramo_id                           " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.Dt_Ocorrencia_Sinistro            " & vbNewLine
''
'''    'Alan Neves - 07/02/2013
''    sSQL = sSQL & "     , #tempsel_tb.tp_emissao           " & vbNewLine
''    sSQL = sSQL & "     , #TempSel_Tb.Sem_Comunicacao_BB    " & vbNewLine               'AKIO.OKUNO - 17/04/2013
''    sSQL = sSQL & "     , #TempSel_Tb.Seg_Cliente           " & vbNewLine               'AKIO.OKUNO - 17/04/2013
''
''    sSQL = sSQL & " from #tempsel_tb                        " & vbCrLf
''
''    'Alan Neves - comentado 07/01/2013 - proc j� filtrava situacao <> 7
''    sSQL = sSQL & " join sinistro_tb s                              " & vbCrLf
''    sSQL = sSQL & "     on s.sinistro_id = #tempsel_tb.sinistro_id  " & vbCrLf
''
''    sSQL = sSQL & " where #tempsel_tb.sinistro_id is not null       " & vbCrLf
'
'    'Alan Neves - 18/04/2013 - Retirada do filtro - Inicio
'    'sSQL = sSQL & "     and s.situacao <> 7                         " & vbCrLf          'AKIO.OKUNO - 25/09/2012 - ESTAVA COMENTADO
'    'Alan Neves - 18/04/2013 - Retirada do filtro - Fim
'
''AKIO.OKUNO / ALAN.NEVES - FIM - 19/04/2013
'
''AKIO.OKUNO - INICIO = 05/10/2012
''    sSql = sSql & " order by #tempsel_tb.Apolice_ID"
''    sSql = sSql & "        , #TempSel_Tb.Proposta_ID"
''    sSql = sSql & "        , #TempSel_Tb.Sucursal_Seguradora_ID"
''    sSql = sSql & "        , #TempSel_Tb.NomeProduto"
''    sSql = sSql & "        , #TempSel_Tb.NomeRamo"
''    sSql = sSql & "        , #TempSel_Tb.NomeCliente"
''NOVA ORDENA��O - SOLICITADO EM 04/10/2012 - DESVIO 20
'    sSQL = sSQL & "Order by #TempSel_Tb.Sinistro_ID"
'    sSQL = sSQL & "       , #TempSel_Tb.NomeProduto"
''AKIO.OKUNO - FIM - 05/10/2012

'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'+----------------------------------------------------------------------
    
    b_primeira_linha_usada = False
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    sSQL, _
                                    lConexaoLocal, _
                                    True)
    
    If (Not rs.EOF) Or (grdSelecao.Rows > 1) Then
        StatusBar1.SimpleText = "Clique no item desejado para exibir os dados."
        
        '07/2019 - permitir ordenar resultado - inclui primeira linha em branco
        If ordenar_resultado(auxRamoPesquisa, auxProdutoPesquisa) Then
            grdSelecao.Rows = 2
        End If
    Else
        StatusBar1.SimpleText = "N�o existe(m) item(s) para esta sele��o."
        If cmbCriterioPesquisa.ItemData(0) <> 0 Then
            cmbCriterioPesquisa.SetFocus
        End If
    End If
    
    i = 0

    With grdSelecao
        .ColWidth(16) = 0 'HiAgro Tp_Emissao'15) = 0   'AKIO.OKUNO 17/04/2013
        .ColWidth(17) = 0 'HiAgro Sem_Comunicacao_BB'16) = 0   'AKIO.OKUNO 17/04/2013
        .ColWidth(18) = 0 'HiAgro Seg_Cliente'17) = 0   'AKIO.OKUNO 17/04/2013
        .ColWidth(19) = 0 'HiAgro Apolice_Dt_Emissao'18) = 0   'AKIO.OKUNO 21/04/2013
        
        While Not rs.EOF
            If sOperacaoCosseguro = "C" Then
                'Valida��o de Cosseguro
                bProcessa = False
                
                'Alan Neves - 08/02/2013
'                sSQL = ""
'                sSQL = sSQL & "Select Tp_Emissao" & vbNewLine
'                sSQL = sSQL & "  From Apolice_Tb  WITH (NOLOCK) " & vbNewLine
'                sSQL = sSQL & " Where Apolice_ID = " & rs.Fields!Apolice_id & vbNewLine
'                sSQL = sSQL & "   and Sucursal_Seguradora_ID = " & rs.Fields!sucursal_seguradora_id & vbNewLine
'                sSQL = sSQL & "   and Seguradora_Cod_Susep = " & rs.Fields!seguradora_cod_susep & vbNewLine
'                sSQL = sSQL & "   and Ramo_ID = " & rs.Fields!ramo_id & vbNewLine
'
'                Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                                      glAmbiente_id, _
'                                                      App.Title, _
'                                                      App.FileDescription, _
'                                                      sSQL, _
'                                                      lConexaoLocal, _
'                                                      True)
                'Alan Neves - Fim 08/02/2013


                
'                If Not rsRecordSet.EOF Then                                                                    'FLAVIO.BEZERRA - 15/01/2013
'                    If rsRecordSet(0) = "D" And strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then        'FLAVIO.BEZERRA - 15/01/2013
'                        bProcessa = True                                                                       'FLAVIO.BEZERRA - 15/01/2013
'                    Else 'mathayde - verificar essa rotina, a mesma nao esta sendo utilizada pelo SEGP0246     'FLAVIO.BEZERRA - 15/01/2013
'                        bProcessa = False       'RALVES (ESTAVA TRUE, MAS NA VERS�O DO RALVES, ESTAVA FALSE)   'FLAVIO.BEZERRA - 15/01/2013
'                    End If                                                                                     'FLAVIO.BEZERRA - 15/01/2013
'                End If                                                                                         'FLAVIO.BEZERRA - 15/01/2013
                'FLAVIO.BEZERRA - IN�CIO - 15/01/2013
                
                
                'Alan Neves - 08/02/2013
'                If Not rsRecordSet.EOF Then
'                    If rsRecordSet(0) = "D" And strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
'                        bProcessa = False
'                    Else 'mathayde - verificar essa rotina, a mesma nao esta sendo utilizada pelo SEGP0246
'                        bProcessa = True
'                    End If
'                End If
                'Alan Neves - Fim 08/02/2013
                
               
                If rs(19) = "D" And strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
                    bProcessa = False
                Else
                    bProcessa = True
                End If
                
                
                'FLAVIO.BEZERRA - FIM - 15/01/2013
            Else
                bProcessa = True
'AKIO.OKUNO - INICIO = 09/10/2012
                
                'Alan Neves - 07/02/2013 - Comentado todo trecho pois a valida��o � feita na proc
                
                'Valida��o de Cosseguro
'                sSQL = ""
'                sSQL = sSQL & "Select Tp_Emissao" & vbNewLine
'                sSQL = sSQL & "  From Apolice_Tb  WITH (NOLOCK) " & vbNewLine
'                sSQL = sSQL & " Where Apolice_ID = " & rs.Fields!Apolice_id & vbNewLine
'                sSQL = sSQL & "   and Sucursal_Seguradora_ID = " & rs.Fields!sucursal_seguradora_id & vbNewLine
'                sSQL = sSQL & "   and Seguradora_Cod_Susep = " & rs.Fields!seguradora_cod_susep & vbNewLine
'                sSQL = sSQL & "   and Ramo_ID = " & rs.Fields!ramo_id & vbNewLine
'
'                Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                                      glAmbiente_id, _
'                                                      App.Title, _
'                                                      App.FileDescription, _
'                                                      sSQL, _
'                                                      lConexaoLocal, _
'                                                      True)
                
                'Alan Neves - 07/02/2013 - apenas a linha abaixo foi comentada
'                If Not rsRecordSet.EOF Then


                
' cristovao.rodrigues 11/01/2013 trecho comentado
' nao esta retornando para o grid os sinistros que NAO S�O COSSEGURO apos localizar um sinistro tipo COSSEGURO
'                    If rsRecordSet(0) = "A" Then
'                        sOperacaoCosseguro = "C"
'                        If strTipoProcesso = strTipoProcesso_Avisar Then strTipoProcesso = strTipoProcesso_Avisar_Cosseguro
'                        If strTipoProcesso = strTipoProcesso_Avaliar Then strTipoProcesso = strTipoProcesso_Avaliar_Cosseguro
'                        If strTipoProcesso = strTipoProcesso_Consulta Then strTipoProcesso = strTipoProcesso_Consulta_Cosseguro
'                        If strTipoProcesso = strTipoProcesso_Reabrir Then strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro
'                        If strTipoProcesso = strTipoProcesso_Encerrar Then strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro
'                    Else
'                        sOperacaoCosseguro = ""
'                    End If
'                'cristovao.rodrigues 11/01/2013 incluido if ...
'                If rsRecordSet(0) = "A" And _                                          'FLAVIO.BEZERRA - 15/01/2013
'                strTipoProcesso <> strTipoProcesso_Avaliar_Cosseguro And _             'FLAVIO.BEZERRA - 15/01/2013
'                strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _              'FLAVIO.BEZERRA - 15/01/2013
'                strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro And _            'FLAVIO.BEZERRA - 15/01/2013
'                strTipoProcesso <> strTipoProcesso_Encerrar_Cosseguro And _            'FLAVIO.BEZERRA - 15/01/2013
'                strTipoProcesso <> strTipoProcesso_Reabrir_Cosseguro Then              'FLAVIO.BEZERRA - 15/01/2013
'                    bProcessa = False                                                  'FLAVIO.BEZERRA - 15/01/2013
'                Else                                                                   'FLAVIO.BEZERRA - 15/01/2013
'                    bProcessa = True                                                   'FLAVIO.BEZERRA - 15/01/2013
'                End If                                                                 'FLAVIO.BEZERRA - 15/01/2013
                'FLAVIO.BEZERRA - INICIO - 15/01/2013
                    
                'Alan Neves - 07/02/2013 - apenas a linha abaixo foi comentada
'                    If rsRecordSet(0) = "A" And strTipoProcesso = strTipoProcesso_Avisar Then
                    If rs(19) = "A" And strTipoProcesso = strTipoProcesso_Avisar Then
                        bProcessa = False
                    Else
                        bProcessa = True
                    End If
                'FLAVIO.BEZERRA - FIM - 15/01/2013
                
                
                'Alan Neves - 07/02/2013 - apenas a linha abaixo foi comentada
                'End If
                
                
'AKIO.OKUNO - FIM - 09/10/2012
            End If
                'Cleber Sardo - 01/09/2014 - INC000004340012 - Tratamento de proposta duplicada, n�o deixar comunicar.
            
            If UCase(rs.Fields!SitProposta) = "X" Then
            
                  Select Case strTipoProcesso
                                          
                    Case strTipoProcesso_Avisar, strTipoProcesso_Avisar_Cosseguro
                        bProcessa = False
                                      
                    Case strTipoProcesso_Reabrir, strTipoProcesso_Reabrir_Cosseguro
                        bProcessa = False
                                                              
                  End Select
            End If
                'Cleber Sardo - 01/09/2014 - INC000004340012 - Tratamento de proposta duplicada, n�o deixar comunicar.
            
            If bProcessa Then
                '07/2019 - ordenar resultados
                '.Rows = .Rows + 1
                
                If ordenar_resultado(rs.Fields!ramo_id, rs.Fields!produto_Id) And Valor_buscado(rs) And (Not b_primeira_linha_usada) Then
                  irow = 1
                  b_primeira_linha_usada = True
                  auxCountRow = 1
                  
                  For i = 1 To .Cols - 1
                    .col = i
                    .CellBackColor = &H80FFFF
                  Next i
                  
                Else
                  .Rows = .Rows + 1
                  irow = .Rows - 1
                  
                  If ordenar_resultado(rs.Fields!ramo_id, rs.Fields!produto_Id) And Valor_buscado(rs) Then
                    auxCountRow = auxCountRow + 1
                    .Row = auxCountRow
                    irow = auxCountRow
                    For i = 1 To .Cols - 1
                      .col = i
                      .CellBackColor = &H80FFFF
                    Next i
                    
                    For intAlterRow = .Rows - 1 To auxCountRow Step -1
                        For intAlterCol = 0 To .Cols - 1
                            .TextMatrix(intAlterRow, intAlterCol) = .TextMatrix(intAlterRow - 1, intAlterCol)
                        Next
                    Next

                    For intCol = 0 To .Cols - 1
                        .TextMatrix(auxCountRow, intCol) = ""
                    Next
                  End If
                End If
            
                .TextMatrix(irow, 0) = IIf(rs.Fields!marca <> "s", "", "*")
                .TextMatrix(irow, 1) = IIf(IsNull(rs.Fields!sinistro_id), "", rs.Fields!sinistro_id)
                .TextMatrix(irow, 2) = " " & rs.Fields!num_Protocolo
                .TextMatrix(irow, 3) = " " & rs.Fields!NomeCliente
                .TextMatrix(irow, 4) = rs.Fields!apolice_id & ""
                .TextMatrix(irow, 5) = rs.Fields!Proposta_id & ""
                .TextMatrix(irow, 6) = rs.Fields!sucursal_seguradora_id & ""
                .TextMatrix(irow, 7) = " " & rs.Fields!produto_Id & " - " & rs.Fields!nomeproduto
                .TextMatrix(irow, 8) = " " & rs.Fields!ramo_id & " - " & rs.Fields!nomeramo
                .TextMatrix(irow, 9) = " " & rs.Fields!Situacao_Desc & " " & rs.Fields!Sit_Evento_Desc
                .TextMatrix(irow, 10) = IIf(IsNull(rs.Fields!Endosso), "0", rs.Fields!Endosso)
                .TextMatrix(irow, 11) = rs.Fields!sinistro_id_lider & ""
                .TextMatrix(irow, 12) = " " & rs.Fields!historico
                .TextMatrix(irow, 13) = rs.Fields!produto_Id & ""
                .TextMatrix(irow, 14) = rs.Fields!ramo_id & ""
                .TextMatrix(irow, 15) = IIf(IsNull(rs.Fields!Dt_Ocorrencia_Sinistro), "", rs.Fields!Dt_Ocorrencia_Sinistro)
                .TextMatrix(irow, 16) = UCase(rs.Fields!tp_emissao) & ""
                If Not IsNull(rs.Fields!Sem_Comunicacao_BB) Then
                    .TextMatrix(irow, 17) = UCase(rs.Fields!Sem_Comunicacao_BB) & ""
                Else
                    .TextMatrix(irow, 17) = ""
                End If
                .TextMatrix(irow, 18) = rs.Fields!Seg_Cliente & ""
                .TextMatrix(irow, 19) = rs.Fields!Apolice_Dt_Emissao & ""
              
                  irow = .Rows - 1
              ' If rs.Fields!SitProposta <> "x" Then       'Cleber Sardo - 23/04/2014 - INC000004276922 - Tratamento de proposta duplicada
'                .Rows = .Rows + 1
'                .TextMatrix(.Rows - 1, 0) = IIf(rs.Fields!marca <> "s", "", "*")
'                .TextMatrix(.Rows - 1, 1) = IIf(IsNull(rs.Fields!sinistro_id), "", rs.Fields!sinistro_id)
'                .TextMatrix(.Rows - 1, 2) = " " & rs.Fields!num_Protocolo
'                .TextMatrix(.Rows - 1, 3) = " " & rs.Fields!NomeCliente
'                .TextMatrix(.Rows - 1, 4) = rs.Fields!Apolice_id & ""
'                .TextMatrix(.Rows - 1, 5) = rs.Fields!Proposta_id & ""
'                .TextMatrix(.Rows - 1, 6) = rs.Fields!sucursal_seguradora_id & ""
'                .TextMatrix(.Rows - 1, 7) = " " & rs.Fields!produto_Id & " - " & rs.Fields!nomeproduto
'                .TextMatrix(.Rows - 1, 8) = " " & rs.Fields!ramo_id & " - " & rs.Fields!nomeramo
'                .TextMatrix(.Rows - 1, 9) = " " & rs.Fields!Situacao_Desc & " " & rs.Fields!Sit_Evento_Desc
'                .TextMatrix(.Rows - 1, 10) = IIf(IsNull(rs.Fields!Endosso), "0", rs.Fields!Endosso)
'                .TextMatrix(.Rows - 1, 11) = rs.Fields!sinistro_id_lider & ""
'                .TextMatrix(.Rows - 1, 12) = " " & rs.Fields!historico
'                .TextMatrix(.Rows - 1, 13) = rs.Fields!produto_Id & ""
'                .TextMatrix(.Rows - 1, 14) = rs.Fields!ramo_id & ""
'                .TextMatrix(.Rows - 1, 15) = IIf(IsNull(rs.Fields!Dt_Ocorrencia_Sinistro), "", rs.Fields!Dt_Ocorrencia_Sinistro)
'
'                'AKIO.OKUNO - INICIO - 17/04/2013
'                .TextMatrix(.Rows - 1, 16) = UCase(rs.Fields!tp_emissao) & ""
'                If Not IsNull(rs.Fields!Sem_Comunicacao_BB) Then
'                    .TextMatrix(.Rows - 1, 17) = UCase(rs.Fields!Sem_Comunicacao_BB) & ""
'                Else
'                    .TextMatrix(.Rows - 1, 17) = ""
'                End If
'                .TextMatrix(.Rows - 1, 18) = rs.Fields!Seg_Cliente & ""
'                .TextMatrix(.Rows - 1, 19) = rs.Fields!Apolice_Dt_Emissao & ""
'                'AKIO.OKUNO - FIM - 17/04/2013
             ' End If
            End If
            rs.MoveNext
        Wend
    
        '07/2019 - ordenar resultados. excluir 1a linha, se foi adicionada em branco e n�o usada
        If ordenar_resultado And (Not b_primeira_linha_usada) And grdSelecao.Rows > 1 Then
          Call grdSelecao.RemoveItem(1)
        End If
    
    
    End With
    
    InicializaModoProcessamentoObjeto grdSelecao, 2
    
    MousePointer = vbNormal
    
    lblSelecionados = Str(grdSelecao.Rows - 1) & " Iten(s) Encontrado(s)"
    
    'Hi Agro - Nro Protocolo
    If bytTipoRamo <> bytTipoRamo_Rural Then
        grdSelecao.ColWidth(2) = 0 'N�m. Protocolo
    End If
    
    rs.Close
    Set rs = Nothing
    
    
    Exit Sub
    
   
'Resume
TrataErro:
Call TrataErroGeral("CarregaDadosGrid_Selecao", Me.name)
Exit Sub

End Sub

Private Sub InicializaCargaDados_LimpezaTemporario()

    Dim sSQL As String
    
    On Error GoTo TrataErro
    
    'alterado para cima, e ao inves de apagar registros, dropar a tablea
    sSQL = ""
'    If EstadoConexao(lConexaoLocal) = adStateClosed Then
'        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, _
'                                      glAmbiente_id, _
'                                      App.Title, _
'                                      App.FileDescription)
'    Else
        'sSQL = " if isnull(OBJECT_ID('tempdb..#tempsel_tb'),0) >0" & vbNewLine
        sSQL = " if OBJECT_ID('tempdb..#tempsel_tb') IS NOT NULL " & vbNewLine
        sSQL = sSQL & " BEGIN" & vbNewLine
        sSQL = sSQL & "     DROP TABLE #tempsel_tb" & vbNewLine
        sSQL = sSQL & " END" & vbNewLine
'    End If
    
    sSQL = sSQL & " create table #tempsel_tb( " & vbCrLf
    sSQL = sSQL & "     marca                   char(1)         null, " & vbCrLf
    sSQL = sSQL & "     apolice_id              numeric(9,0)    null, " & vbCrLf
    sSQL = sSQL & "     sucursal_seguradora_id  numeric(5,0)    null, " & vbCrLf
    sSQL = sSQL & "     seguradora_cod_susep    numeric(5,0)    null, " & vbCrLf
    sSQL = sSQL & "     ramo_id                 int             null, " & vbCrLf
    sSQL = sSQL & "     nomeramo                varchar(60)     null, " & vbCrLf
    sSQL = sSQL & "     proposta_id             numeric(9,0)    null, " & vbCrLf
    sSQL = sSQL & "     numendosso              int             null, " & vbCrLf
    sSQL = sSQL & "     sitproposta             char(1)         null, " & vbCrLf
    sSQL = sSQL & "     produto_id              int             null, " & vbCrLf
    sSQL = sSQL & "     nomeproduto             varchar(60)     null, " & vbCrLf
    sSQL = sSQL & "     cliente_id              int             null, " & vbCrLf
    sSQL = sSQL & "     nomecliente             varchar(60)     null, " & vbCrLf
    sSQL = sSQL & "     tipocomponente          char(1)         null, " & vbCrLf
    sSQL = sSQL & "     sinistro_id             numeric(11,0)   null, " & vbCrLf
    sSQL = sSQL & "     situacao                char(1)         null, " & vbCrLf
    sSQL = sSQL & "     situacao_desc           varchar(30)     null, " & vbCrLf
    sSQL = sSQL & "     situacao_evento         char(1)         null, " & vbCrLf
    sSQL = sSQL & "     sit_evento_desc         varchar(20)     null, " & vbCrLf
    sSQL = sSQL & "     sucursal_nome           varchar(60)     null, " & vbCrLf
    sSQL = sSQL & "     endosso                 int             null, " & vbCrLf
    sSQL = sSQL & "     sinistro_id_lider       numeric(30,0)   null, " & vbCrLf 'EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
    sSQL = sSQL & "     processa_reintegracao_is char(1)        null, " & vbCrLf
    sSQL = sSQL & "     historico               varchar(100)    null, " & vbCrLf
    sSQL = sSQL & "     Dt_Ocorrencia_Sinistro  SmallDateTime   null," & vbCrLf
    
'    'Alan Neves - criacao de tp_emissao
    sSQL = sSQL & "     tp_emissao              char(1)         null" & vbCrLf
    sSQL = sSQL & "   , Sem_Comunicacao_BB      Char(1)         Null"           'AKIO.OKUNO - 17/04/2013
    sSQL = sSQL & "   , Seg_Cliente             Char(7)         Null"           'AKIO.OKUNO - 17/04/2013
    sSQL = sSQL & "   , Apolice_Dt_Emissao      SmallDateTime         Null"           'AKIO.OKUNO - 21/04/2013
    sSQL = sSQL & "   , num_protocolo           varchar(30) null " & vbCrLf
    
    sSQL = sSQL & "     )  "
    
    vNunCx = lConexaoLocal
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
Exit Sub

TrataErro:
    Call TrataErroGeral("CarregaDadosGrid_Selecao", Me.name)

End Sub
Private Sub InicializaCargaDados_Adicionais()

    Dim sSQL As String
    
    On Error GoTo TrataErro
    
    'VERIFICA A SITUACAO DO EVENTO
    sSQL = " select max(seq_evento) maxevento, #tempsel_tb.sinistro_id                  " & vbCrLf
    sSQL = sSQL & " into #maxeventos                                                    " & vbCrLf
    sSQL = sSQL & " from sinistro_historico_tb  WITH (NOLOCK)                                  " & vbCrLf
    sSQL = sSQL & " join #tempsel_tb                                                    " & vbCrLf
    sSQL = sSQL & "     on sinistro_historico_tb.sinistro_id = #tempsel_tb.sinistro_id  " & vbCrLf
    sSQL = sSQL & " group by #tempsel_tb.sinistro_id                                    " & vbCrLf
    sSQL = sSQL & " " & vbCrLf
       
    sSQL = sSQL & " update #tempsel_tb                                                      " & vbCrLf
    sSQL = sSQL & " set situacao_evento = sinistro_historico_tb.situacao                    " & vbCrLf
    sSQL = sSQL & "     ,historico = evento_segbr_tb.descricao                              " & vbCrLf
    sSQL = sSQL & "  --select  maxevento, #tempsel_tb.sinistro_id, evento_segbr_tb.descricao" & vbCrLf
    sSQL = sSQL & " from sinistro_historico_tb                                              " & vbCrLf
    sSQL = sSQL & " join evento_segbr_tb  WITH (NOLOCK)                                        " & vbCrLf
    sSQL = sSQL & "      on sinistro_historico_tb.evento_segbr_id = evento_segbr_tb.evento_segbr_id " & vbCrLf
    sSQL = sSQL & "  inner join  #maxeventos                                                " & vbCrLf
    sSQL = sSQL & "      on  #maxeventos.sinistro_id = sinistro_historico_tb.sinistro_id    " & vbCrLf
    sSQL = sSQL & "      and #maxeventos.maxevento   = sinistro_historico_tb.seq_evento     " & vbCrLf
    sSQL = sSQL & "  inner join #tempsel_tb                                                 " & vbCrLf
    sSQL = sSQL & "      on #tempsel_tb.sinistro_id  = #maxeventos.sinistro_id              " & vbCrLf
    sSQL = sSQL & " " & vbCrLf

    ' atualiza situacao
    sSQL = sSQL & " update #tempsel_tb                                              " & vbCrLf
    sSQL = sSQL & " set situacao_desc = case situacao                               " & vbCrLf
    sSQL = sSQL & "                     when '0' then 'ABERTO'                      " & vbCrLf
    sSQL = sSQL & "                     when '1' then 'REABERTO'                    " & vbCrLf
    sSQL = sSQL & "                     when '2' then 'ENCERRADO'                   " & vbCrLf
    sSQL = sSQL & "                     when '5' then 'REABERTO (ADMINISTRATIVO)'  " & vbCrLf
    sSQL = sSQL & "                     else ' ' end,                               " & vbCrLf
    sSQL = sSQL & " sit_evento_desc =   case situacao_evento                " & vbCrLf
    sSQL = sSQL & "                     when '' then ''                     " & vbCrLf
    sSQL = sSQL & "                     when '3' then 'AG. APROVA��O'       " & vbCrLf
    sSQL = sSQL & "                     when '4' then 'AG. FINANCEIRO'      " & vbCrLf
    sSQL = sSQL & "                     else ' ' end                        " & vbCrLf
    sSQL = sSQL & " " & vbCrLf
                                
                                
'AKIO.OKUNO - INICIO - 17/04/2013
'     ' ATUALIZA O NOME DA SUCURSAL
'    sSQL = sSQL & " update #tempsel_tb                                                                          " & vbCrLf
'    sSQL = sSQL & " set sucursal_nome = sucursal_seguradora_tb.nome                                             " & vbCrLf
'    sSQL = sSQL & " from #tempsel_tb, sucursal_seguradora_tb                                                    " & vbCrLf
'    sSQL = sSQL & " where #tempsel_tb.sucursal_seguradora_id = sucursal_seguradora_tb.sucursal_seguradora_id    " & vbCrLf
'    sSQL = sSQL & "     and #tempsel_tb.seguradora_cod_susep = sucursal_seguradora_tb.seguradora_cod_susep      " & vbCrLf
'    sSQL = sSQL & " " & vbCrLf
'AKIO.OKUNO - FIM - 17/04/2013
                           
    sSQL = sSQL & " drop table #MaxEventos  " & vbCrLf
        
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
    
    Exit Sub

TrataErro:
Call TrataErroGeral("InicializaCargaDados_Adicionais", Me.name)

End Sub

Private Sub btnCancelar_Click()
'ok
    Dim Retorno As Boolean

    On Error GoTo TrataErro
    
    'msalema - 23/04/2013
    If (btnDetalhar.Caption = "Cancelar" Or BtnOk.Enabled = True) And strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        If Not GravaDados_BloqueiaSinistro(False, gbldSinistro_ID) Then Exit Sub
    End If
    'msalema - 23/04/2013
    
    If EstadoConexao(lConexaoLocal) <> adStateClosed Then
'AKIO.OKUNO - INICIO - 30/04/2013
'        sSQL = ""
'        sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#tempsel_tb'),0) >0" & vbNewLine
'        sSQL = sSQL & " BEGIN" & vbNewLine
'        sSQL = sSQL & "     DROP TABLE #tempsel_tb" & vbNewLine
'        sSQL = sSQL & " END" & vbNewLine
'
'        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                 glAmbiente_id, _
'                                 App.Title, _
'                                 App.FileDescription, _
'                                 sSQL, _
'                                 lConexaoLocal, _
'                                 False)
        
        Close All
'AKIO.OKUNO - FIM - 30/04/2013

    
       Retorno = FecharConexao(lConexaoLocal)
    End If
    End
    Exit Sub
    
TrataErro:
Call TrataErroGeral("BtnCancelarClick", Me.name)
    
End Sub

Private Sub optAnotacao_Click()
    For i = 0 To 3
        txtExigencia(i).Visible = False
    Next i

    With txtDetalhamento_Visualizar
        .Visible = True
        If .Visible Then
            .SetFocus
        End If
    End With

End Sub

Private Sub optRequisicao_Click()

    For i = 0 To 3
        txtExigencia(i).Visible = True
    Next i
    txtDetalhamento_Visualizar.Visible = False
    If txtExigencia(0).Visible Then
        txtExigencia(0).SetFocus
    End If

End Sub

Private Sub txtConteudo_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then
        btnPesquisar_Click
    End If
End Sub

Private Sub CarregaDadosCombo_Tecnicos()

    Dim sSQL As String
    Dim rs As ADODB.Recordset
    
    cmbTecnico.Clear
        
'    If cmbCriterioPesquisa.Text = "T�cnicos - Aprova��o" Then
'
'        sSQL = sSQL & " select                                                                  " & vbCrLf
'        sSQL = sSQL & "     sinistro_tecnico_tb.tecnico_id,                                     " & vbCrLf
'        sSQL = sSQL & "     tecnico_tb.nome                                                     " & vbCrLf
'        sSQL = sSQL & " from seguros_db.dbo.pgto_sinistro_tb pgto_sinistro_tb  WITH (NOLOCK)           " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.sinistro_tb sinistro_tb  WITH (NOLOCK)                     " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.sinistro_id = pgto_sinistro_tb.sinistro_id           " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.sinistro_tecnico_tb sinistro_tecnico_tb   WITH (NOLOCK)    " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_tecnico_tb.sinistro_id        " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.tecnico_tb tecnico_tb   WITH (NOLOCK)                      " & vbCrLf
'        sSQL = sSQL & "     on tecnico_tb.tecnico_id = sinistro_tecnico_tb.tecnico_id           " & vbCrLf
'        sSQL = sSQL & " where 1 = 1                                                             " & vbCrLf
'        sSQL = sSQL & "     and item_val_estimativa = 1                                         " & vbCrLf
'        sSQL = sSQL & "     and situacao_op = 'n'                                               " & vbCrLf
'        sSQL = sSQL & "     and sinistro_tecnico_tb.dt_fim_vigencia is null                     " & vbCrLf
'        sSQL = sSQL & " union                                                                   " & vbCrLf
'        sSQL = sSQL & " select                                                                  " & vbCrLf
'        sSQL = sSQL & "     sinistro_tecnico_aprovador_tb.tecnico_id,                           " & vbCrLf
'        sSQL = sSQL & "     tecnico_tb.nome                                                     " & vbCrLf
'        sSQL = sSQL & " from seguros_db.dbo.pgto_sinistro_tb pgto_sinistro_tb  WITH (NOLOCK)           " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.sinistro_tb sinistro_tb  WITH (NOLOCK)                     " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.sinistro_id = pgto_sinistro_tb.sinistro_id           " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.sinistro_tecnico_aprovador_tb sinistro_tecnico_aprovador_tb  WITH (NOLOCK)     " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_tecnico_aprovador_tb.sinistro_id  " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.tecnico_tb tecnico_tb   WITH (NOLOCK)                          " & vbCrLf
'        sSQL = sSQL & "     on tecnico_tb.tecnico_id = sinistro_tecnico_aprovador_tb.tecnico_id     " & vbCrLf
'        sSQL = sSQL & " where 1 = 1                                                                 " & vbCrLf
'        sSQL = sSQL & "     and item_val_estimativa = 1                                             " & vbCrLf
'        sSQL = sSQL & "     and situacao_op = 'n'                                                   " & vbCrLf
'        sSQL = sSQL & "     and sinistro_tecnico_aprovador_tb.dt_fim_vigencia is null               " & vbCrLf
'        sSQL = sSQL & " order by tecnico_tb.nome                                                    " & vbCrLf
'
    'cristovao.rodrigues 11/01/2013' trecho modificado
     sSQL = ""
    If cmbCriterioPesquisa.Text = "T�cnicos - Aprova��o" And _
       cmbCriterioPesquisa.Tag <> "TEC_APROVA" Then
        
        sSQL = ""
        sSQL = sSQL & " SELECT sinistro_tecnico_tb.tecnico_id, tecnico_tb.nome" & vbNewLine
        sSQL = sSQL & "   from seguros_db.dbo.pgto_sinistro_tb pgto_sinistro_tb  WITH (NOLOCK) " & vbNewLine
        sSQL = sSQL & "   join seguros_db.dbo.sinistro_tb sinistro_tb  WITH (NOLOCK) " & vbNewLine
        sSQL = sSQL & "     on sinistro_tb.sinistro_id = pgto_sinistro_tb.sinistro_id" & vbNewLine
        sSQL = sSQL & "   JOIN seguros_db.dbo.sinistro_tecnico_tb sinistro_tecnico_tb   WITH (NOLOCK) " & vbNewLine
        sSQL = sSQL & "     ON sinistro_tb.sinistro_id = sinistro_tecnico_tb.sinistro_id" & vbNewLine
        sSQL = sSQL & "   JOIN seguros_db.dbo.tecnico_tb tecnico_tb   WITH (NOLOCK) " & vbNewLine
        sSQL = sSQL & "     ON tecnico_tb.tecnico_id = sinistro_tecnico_tb.tecnico_id" & vbNewLine
        sSQL = sSQL & "  Where 1 = 1" & vbNewLine
        sSQL = sSQL & "    AND item_val_estimativa = 1" & vbNewLine
        sSQL = sSQL & "    and situacao_op = 'n'" & vbNewLine
        sSQL = sSQL & "    AND sinistro_tecnico_tb.dt_fim_vigencia IS NULL" & vbNewLine
        sSQL = sSQL & "  Union" & vbNewLine
        sSQL = sSQL & " SELECT sinistro_tecnico_aprovador_tb.tecnico_id, tecnico_tb.nome" & vbNewLine
        sSQL = sSQL & "   from seguros_db.dbo.pgto_sinistro_tb pgto_sinistro_tb  WITH (NOLOCK) " & vbNewLine
        sSQL = sSQL & "   join seguros_db.dbo.sinistro_tb sinistro_tb  WITH (NOLOCK) " & vbNewLine
        sSQL = sSQL & "     on sinistro_tb.sinistro_id = pgto_sinistro_tb.sinistro_id" & vbNewLine
        sSQL = sSQL & "   JOIN seguros_db.dbo.sinistro_tecnico_aprovador_tb sinistro_tecnico_aprovador_tb  WITH (NOLOCK) " & vbNewLine
        sSQL = sSQL & "     ON sinistro_tb.sinistro_id = sinistro_tecnico_aprovador_tb.sinistro_id" & vbNewLine
        sSQL = sSQL & "   JOIN seguros_db.dbo.tecnico_tb tecnico_tb   WITH (NOLOCK) " & vbNewLine
        sSQL = sSQL & "     ON tecnico_tb.tecnico_id = sinistro_tecnico_aprovador_tb.tecnico_id" & vbNewLine
        sSQL = sSQL & "  Where 1 = 1" & vbNewLine
        sSQL = sSQL & "    AND item_val_estimativa = 1" & vbNewLine
        sSQL = sSQL & "    and situacao_op = 'n'" & vbNewLine
        sSQL = sSQL & "    AND sinistro_tecnico_aprovador_tb.dt_fim_vigencia IS NULL" & vbNewLine
        sSQL = sSQL & "  order by tecnico_tb.nome" & vbNewLine
        
        cmbCriterioPesquisa.Tag = "TEC_APROVA"
        
    ElseIf cmbCriterioPesquisa.Text = "Sinistros Pendentes de Aprova��o" And _
           chkTecnicosAlcadaMenor.Value = vbUnchecked And _
           cmbCriterioPesquisa.Tag <> "SIN_PENDENTE" Then
        sSQL = "SELECT DISTINCT sinistro_tecnico_aprovador_tb.tecnico_id, tecnico_tb.nome " & vbCrLf _
            & "FROM seguros_db.dbo.pgto_sinistro_tb pgto_sinistro_tb  WITH (NOLOCK)  " & vbCrLf _
            & "INNER JOIN seguros_db.dbo.sinistro_tb sinistro_tb  WITH (NOLOCK)  on sinistro_tb.sinistro_id = pgto_sinistro_tb.sinistro_id " & vbCrLf _
            & "INNER JOIN seguros_db.dbo.sinistro_tecnico_aprovador_tb sinistro_tecnico_aprovador_tb  WITH (NOLOCK)  ON sinistro_tb.sinistro_id = sinistro_tecnico_aprovador_tb.sinistro_id " & vbCrLf _
            & "INNER JOIN seguros_db.dbo.tecnico_tb tecnico_tb   WITH (NOLOCK)  ON tecnico_tb.tecnico_id = sinistro_tecnico_aprovador_tb.tecnico_id " & vbCrLf _
            & "WHERE item_val_estimativa = 1 " & vbCrLf _
            & "  AND situacao_op = 'n' " & vbCrLf _
            & "  AND sinistro_tecnico_aprovador_tb.dt_fim_vigencia IS NULL " & vbCrLf _
            & "  order by tecnico_tb.nome "
           
        cmbCriterioPesquisa.Tag = "SIN_PENDENTE"
    
    ElseIf cmbCriterioPesquisa.Text = "Sinistros Pendentes de Aprova��o" And _
           chkTecnicosAlcadaMenor.Value = vbChecked And _
           cmbCriterioPesquisa.Tag <> "SIN_PENDENTE_ALCADA" Then
    
         sSQL = "SELECT DISTINCT sinistro_tecnico_aprovador_tb.tecnico_id, tecnico_tb.nome " & vbCrLf _
             & "FROM seguros_db.dbo.pgto_sinistro_tb pgto_sinistro_tb  WITH (NOLOCK)  " & vbCrLf _
             & "INNER JOIN seguros_db.dbo.sinistro_tb sinistro_tb  WITH (NOLOCK)  on sinistro_tb.sinistro_id = pgto_sinistro_tb.sinistro_id " & vbCrLf _
             & "INNER JOIN seguros_db.dbo.sinistro_tecnico_aprovador_tb sinistro_tecnico_aprovador_tb  WITH (NOLOCK)  ON sinistro_tb.sinistro_id = sinistro_tecnico_aprovador_tb.sinistro_id " & vbCrLf _
             & "INNER JOIN seguros_db.dbo.tecnico_tb tecnico_tb   WITH (NOLOCK)  ON tecnico_tb.tecnico_id = sinistro_tecnico_aprovador_tb.tecnico_id " & vbCrLf _
             & "INNER JOIN (SELECT tecnico_id, max(val_limite_aprovacao) as val_limite " & vbCrLf _
             & "            FROM seguros_db.dbo.limite_aprovacao_pgto_tb  WITH (NOLOCK)  " & vbCrLf _
             & "            WHERE item_val_estimativa = 1 " & vbCrLf _
             & "            GROUP BY tecnico_id ) limites ON limites.tecnico_id = tecnico_tb.tecnico_id " & vbCrLf
        sSQL = sSQL & "WHERE item_val_estimativa = 1 " & vbCrLf _
                  & "AND situacao_op = 'n' " & vbCrLf _
                  & "AND sinistro_tecnico_aprovador_tb.dt_fim_vigencia IS NULL " & vbCrLf _
                  & "AND (limites.val_limite < (SELECT max(val_limite_aprovacao) as val_limite " & vbCrLf _
                  & "                           FROM seguros_db.dbo.limite_aprovacao_pgto_tb  WITH (NOLOCK)  " & vbCrLf _
                  & "                           WHERE tecnico_id = " & Tecnico_atual & " AND item_val_estimativa = 1) " & vbCrLf _
                  & "     OR sinistro_tecnico_aprovador_tb.tecnico_id = " & Tecnico_atual & " ) " & vbCrLf _
                  & "  order by tecnico_tb.nome "
                  
        cmbCriterioPesquisa.Tag = "SIN_PENDENTE_ALCADA"
        
    ElseIf cmbCriterioPesquisa.Tag <> "TODOS" Then
        
        sSQL = "SELECT tecnico_id, nome FROM tecnico_tb WHERE ativo = 's' ORDER BY nome"
        cmbCriterioPesquisa.Tag = "TODOS"
    End If
                                    
    If sSQL <> "" Then
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        sSQL, _
                        True)
    
    While Not rs.EOF
        cmbTecnico.AddItem rs("nome")
        cmbTecnico.ItemData(cmbTecnico.NewIndex) = rs("tecnico_id")
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
    End If
    
        
'    If cmbCriterioPesquisa.Text = "T�cnicos - Aprova��o" Then
'
'        sSQL = sSQL & " select                                                                  " & vbCrLf
'        sSQL = sSQL & "     sinistro_tecnico_tb.tecnico_id,                                     " & vbCrLf
'        sSQL = sSQL & "     tecnico_tb.nome                                                     " & vbCrLf
'        sSQL = sSQL & " from seguros_db.dbo.pgto_sinistro_tb pgto_sinistro_tb  WITH (NOLOCK)           " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.sinistro_tb sinistro_tb  WITH (NOLOCK)                     " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.sinistro_id = pgto_sinistro_tb.sinistro_id           " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.sinistro_tecnico_tb sinistro_tecnico_tb   WITH (NOLOCK)    " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_tecnico_tb.sinistro_id        " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.tecnico_tb tecnico_tb   WITH (NOLOCK)                      " & vbCrLf
'        sSQL = sSQL & "     on tecnico_tb.tecnico_id = sinistro_tecnico_tb.tecnico_id           " & vbCrLf
'        sSQL = sSQL & " where 1 = 1                                                             " & vbCrLf
'        sSQL = sSQL & "     and item_val_estimativa = 1                                         " & vbCrLf
'        sSQL = sSQL & "     and situacao_op = 'n'                                               " & vbCrLf
'        sSQL = sSQL & "     and sinistro_tecnico_tb.dt_fim_vigencia is null                     " & vbCrLf
'        sSQL = sSQL & " union                                                                   " & vbCrLf
'        sSQL = sSQL & " select                                                                  " & vbCrLf
'        sSQL = sSQL & "     sinistro_tecnico_aprovador_tb.tecnico_id,                           " & vbCrLf
'        sSQL = sSQL & "     tecnico_tb.nome                                                     " & vbCrLf
'        sSQL = sSQL & " from seguros_db.dbo.pgto_sinistro_tb pgto_sinistro_tb  WITH (NOLOCK)           " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.sinistro_tb sinistro_tb  WITH (NOLOCK)                     " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.sinistro_id = pgto_sinistro_tb.sinistro_id           " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.sinistro_tecnico_aprovador_tb sinistro_tecnico_aprovador_tb  WITH (NOLOCK)     " & vbCrLf
'        sSQL = sSQL & "     on sinistro_tb.sinistro_id = sinistro_tecnico_aprovador_tb.sinistro_id  " & vbCrLf
'        sSQL = sSQL & " join seguros_db.dbo.tecnico_tb tecnico_tb   WITH (NOLOCK)                          " & vbCrLf
'        sSQL = sSQL & "     on tecnico_tb.tecnico_id = sinistro_tecnico_aprovador_tb.tecnico_id     " & vbCrLf
'        sSQL = sSQL & " where 1 = 1                                                                 " & vbCrLf
'        sSQL = sSQL & "     and item_val_estimativa = 1                                             " & vbCrLf
'        sSQL = sSQL & "     and situacao_op = 'n'                                                   " & vbCrLf
'        sSQL = sSQL & "     and sinistro_tecnico_aprovador_tb.dt_fim_vigencia is null               " & vbCrLf
'        sSQL = sSQL & " order by tecnico_tb.nome                                                    " & vbCrLf
'
'    Else
'
'        sSQL = ""
'        sSQL = sSQL & " select tecnico_id, nome from tecnico_tb where ativo = 's' order by nome"
'
'    End If
'
'    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
'                        glAmbiente_id, _
'                        App.Title, _
'                        App.FileDescription, _
'                        sSQL, _
'                        True)
'
'    While Not rs.EOF
'        cmbTecnico.AddItem rs("nome")
'        cmbTecnico.ItemData(cmbTecnico.NewIndex) = rs("tecnico_id")
'        rs.MoveNext
'    Wend
'
'    rs.Close
'    Set rs = Nothing

End Sub

Private Sub CarregaDadosCombo_Situacao()

    cmbSitSinistroAvaliador.AddItem ("Aberto")
    cmbSitSinistroAvaliador.AddItem ("Reaberto")
    cmbSitSinistroAvaliador.AddItem ("Encerrado")

End Sub

'Private Sub SelecionaDados_Sinistro()
'    Dim sSQL                                    As String
'
'    On Error GoTo Erro
'
'    MousePointer = vbHourglass
'
'    sSQL = ""
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Principal'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Create Table #Sinistro_Principal "
'    sSQL = sSQL & "     ( Sinistro_ID                                   Numeric(11)         " & vbNewLine   ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Sucursal_Seguradora_ID                        Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Seguradora_Cod_Susep                          Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Apolice_ID                                    Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Ramo_ID                                       Int                 " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Ramo_Nome                                     VarChar(60)         " & vbNewLine  ' -- Ramo_Tb
'    sSQL = sSQL & "     , Seguradora_Nome_Lider                         VarChar(60)         " & vbNewLine  ' -- Seguradora_Tb (Nome Lider)
'    sSQL = sSQL & "     , Apolice_Dt_Inicio_Vigencia                    SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
'    sSQL = sSQL & "     , Apolice_Dt_Fim_Vigencia                       SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
'    sSQL = sSQL & "     , Apolice_Dt_Emissao                            SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
'    sSQL = sSQL & "     , Proposta_ID                                   Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Produto_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb
'    sSQL = sSQL & "     , Produto_Nome                                  VarChar(60)         " & vbNewLine  ' -- Produto_Tb
'    sSQL = sSQL & "     , Certificado_Dt_Inicio_Vigencia                SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
'    sSQL = sSQL & "     , Certificado_Dt_Fim_Vigencia                   SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
'    sSQL = sSQL & "     , Certficado_Dt_Emissao                         SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
'    sSQL = sSQL & "     , Quantidade_Endossos                           Int                 " & vbNewLine  ' -- Count Endosso_Tb ???
'    sSQL = sSQL & "     , Proposta_Situacao                             VarChar(1)          " & vbNewLine  ' -- Proposta_Tb
'    sSQL = sSQL & "     , Proposta_Situacao_Descricao                   VarChar(60)         " & vbNewLine  ' -- Dado_Basico_Db..Situacao_Proposta_tb.Descricao
'    sSQL = sSQL & "     , Tp_Cobertura_Nome                             VarChar(60)         " & vbNewLine  ' -- Tp_Cobertura_Tb
'    sSQL = sSQL & "     , Corretor_Nome                                 VarChar(60)         " & vbNewLine  ' -- Corretor_Tb
'    sSQL = sSQL & "     , Cliente_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb (Prop_Cliente_ID)
'    sSQL = sSQL & "     , Cliente_Nome                                  VarChar(60)         " & vbNewLine  ' -- Cliente_Tb
'    sSQL = sSQL & "     , Tp_Componente_ID                              TinyInt             " & vbNewLine  ' -- Escolha_Tp_Cob_Vida_Tp
'    sSQL = sSQL & "     , Tp_Componente_Nome                            VarChar(60)         " & vbNewLine  ' -- Tp_Componente_Tb
'    sSQL = sSQL & "     , Evento_Sinistro_ID                            Int                 " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Evento_Sinistro_Nome                          VarChar(60)         " & vbNewLine  ' -- Evento_Sinistro_Tb
'    sSQL = sSQL & "     , SubEvento_Sinistro_ID                         Int                 " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , SubEvento_Sinistro_Nome                       VarChar(60)         " & vbNewLine  ' -- SubEvento_Sinistro_Tb      'AKIO.OKUNO - 22/11/2012
'    sSQL = sSQL & "     , SubEvento_Sinistro_Nome                       VarChar(120)         " & vbNewLine  ' -- SubEvento_Sinistro_Tb
'    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                        SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Dt_Entrada_Seguradora                         SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Dt_Aviso_Sinistro                             SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Dt_Inclusao                                   SmallDateTIme       " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Agencia_ID                                    Numeric(4)          " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Sinistro_Vida_Nome                            VarChar(60)         " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
'    sSQL = sSQL & "     , Sinistro_Vida_CPF                             VarChar(11)         " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
'    sSQL = sSQL & "     , Sinistro_Vida_Dt_Nasc                         DateTime            " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
'    sSQL = sSQL & "     , Sinistro_Vida_Sexo                            Char(1)             " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
'    sSQL = sSQL & "     , Sinistrado_Cliente_ID                         Int                 " & vbNewLine  ' -- Pessoa_Fisica_Tb (Pf_Cliente_ID) (Sinistrado)
'    sSQL = sSQL & "     , Solicitante_ID                                Int                 " & vbNewLine  ' -- Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Nome                     VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Endereco                 VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Bairro                   VarChar(30)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio_ID             Numeric(3)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio                VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Municipio_Nome                    VarChar(60)         " & vbNewLine  ' -- Municipio_Tb (Nome) (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD1                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax                  VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax             VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1                   Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2                   Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3                   Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4                   Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5                   Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Estado                   Char(2)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_CEP                      VarChar(8)          " & vbNewLine  ' -- SOlicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Email                    VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Sinistro_Causa_Observacao_Item                Int                 " & vbNewLine  ' -- Sinistro_Causa_Observacao_Tb
'    sSQL = sSQL & "     , Sinistro_Causa_Observacao                     VarChar(300)        " & vbNewLine  ' -- Sinistro_Causa_Observacao_Tb
'    sSQL = sSQL & "     , Sinistro_Situacao_ID                          Int                 " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Sinistro_Situacao                             VarChar(60)         " & vbNewLine  ' -- Dado_Basico_Db..Sinistro_Situacao_Tb
'    sSQL = sSQL & "     , Max_Endosso_ID                                Int                 " & vbNewLine  ' -- Endosso_Tb
'    sSQL = sSQL & "     ) " & vbNewLine
'    sSQL = sSQL & "     " & vbNewLine
'    sSQL = sSQL & "Create Index PK_Sinistro_ID                          on #Sinistro_Principal (Sinistro_ID)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_Proposta_ID                          on #Sinistro_Principal (Proposta_ID)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_Evento_Sinistro_ID                   on #Sinistro_Principal (Evento_Sinistro_ID)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_SubEvento_Sinistro_ID                on #Sinistro_Principal (SubEvento_Sinistro_ID)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_A                                    on #Sinistro_Principal (Sucursal_Seguradora_ID, Seguradora_Cod_Susep, Apolice_ID, Ramo_ID)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_Seguradora_Cod_Susep                 on #Sinistro_Principal (Seguradora_Cod_Susep)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_Cliente_ID                           on #Sinistro_Principal (Cliente_ID)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_Solicitante_ID                       on #Sinistro_Principal (Solicitante_ID)" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
'
'
'
'
'
'    '-->> Dados do Frame: Seguro                                                                                                                                                                                                             & vbNewLine
'    '-- Dados do Sinistro                                                                                                                                                                                                                    & vbNewLine
'    sSQL = ""
'    sSQL = sSQL & "Insert into #Sinistro_Principal " & vbNewLine
'    sSQL = sSQL & "          ( Sinistro_ID                      " & vbNewLine
'    sSQL = sSQL & "          , Sucursal_Seguradora_ID           " & vbNewLine
'    sSQL = sSQL & "          , Seguradora_Cod_Susep             " & vbNewLine
'    sSQL = sSQL & "          , Apolice_ID                       " & vbNewLine
'    sSQL = sSQL & "          , Ramo_ID                          " & vbNewLine
'    sSQL = sSQL & "          , Proposta_ID" & vbNewLine
'    sSQL = sSQL & "          , Evento_Sinistro_ID" & vbNewLine
'    sSQL = sSQL & "          , SubEvento_Sinistro_ID    " & vbNewLine
'    sSQL = sSQL & "          , Dt_Ocorrencia_Sinistro" & vbNewLine
'    sSQL = sSQL & "          , Dt_Entrada_Seguradora" & vbNewLine
'    sSQL = sSQL & "          , Dt_Aviso_Sinistro    " & vbNewLine
'    sSQL = sSQL & "          , Dt_Inclusao          " & vbNewLine
'    sSQL = sSQL & "          , Agencia_ID" & vbNewLine
'    sSQL = sSQL & "          , Solicitante_ID                       " & vbNewLine
'    sSQL = sSQL & "          , Sinistro_Situacao_ID" & vbNewLine
'    sSQL = sSQL & "          )" & vbNewLine
'    sSQL = sSQL & "Select Sinistro_ID                       " & vbNewLine
'    sSQL = sSQL & "     , Sucursal_Seguradora_ID            " & vbNewLine
'    sSQL = sSQL & "     , Seguradora_Cod_Susep              " & vbNewLine
'    sSQL = sSQL & "     , Apolice_ID                        " & vbNewLine
'    sSQL = sSQL & "     , Ramo_ID                           " & vbNewLine
'    sSQL = sSQL & "     , Proposta_ID           " & vbNewLine
'    sSQL = sSQL & "     , Evento_Sinistro_ID            " & vbNewLine
'    sSQL = sSQL & "     , SubEvento_Sinistro_ID " & vbNewLine
'    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro" & vbNewLine
'    sSQL = sSQL & "     , Dt_Entrada_Seguradora" & vbNewLine
'    sSQL = sSQL & "     , Dt_Aviso_Sinistro " & vbNewLine
'    sSQL = sSQL & "     , Dt_Inclusao           " & vbNewLine
'    sSQL = sSQL & "     , Agencia_ID" & vbNewLine
'    sSQL = sSQL & "     , Solicitante_ID                        " & vbNewLine
'    sSQL = sSQL & "     , Situacao" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Tb                    Sinistro_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & " Where Sinistro_ID = " & gbldSinistro_ID & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    '--Situacao Sinistro                                                                                                                                                                                                                     & vbNewLine
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Sinistro_Situacao                             = Sinistro_Situacao_Tb.Situacao" & vbNewLine
'    sSQL = sSQL & "  From Dado_Basico_Db..Sinistro_Situacao_Tb          Sinistro_Situacao_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_Situacao_ID      = Sinistro_Situacao_Tb.Situacao_ID" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    '-- Ramo_Tb                                                                                                                                                                                                                              & vbNewLine
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Ramo_Nome                                     = Ramo_Tb.Nome" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Ramo_Tb                        Ramo_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & " on #Sinistro_Principal.Ramo_ID                      = Ramo_Tb.Ramo_ID" & vbNewLine
'    '-- Lider                                                                                                                                                                                                                                & vbNewLine
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Seguradora_Nome_Lider                         = Seguradora_Tb.Nome" & vbNewLine
'    'sSQL = sSQL & "  From Seguros_Db.Dbo.Seguradora_Tb                  Seguradora_Tb (NoLock, Index = PK_seguradora)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Seguradora_Tb                  Seguradora_Tb  WITH (NOLOCK) " & vbNewLine
'    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Seguradora_Cod_Susep)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Seguradora_Cod_Susep      = Seguradora_Tb.Seguradora_Cod_Susep " & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    '-- Dados da Proposta                                                                                                                                                                                                                    & vbNewLine
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Produto_ID                                    = Proposta_Tb.Produto_ID" & vbNewLine
'    sSQL = sSQL & "     , Proposta_Situacao                             = Proposta_Tb.Situacao" & vbNewLine
'    sSQL = sSQL & "     , Cliente_ID                                    = Proposta_Tb.Prop_Cliente_ID" & vbNewLine
'    sSQL = sSQL & "     , Quantidade_Endossos                           = Proposta_Tb.Num_Endosso" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK) " & vbNewLine
'    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Proposta_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'    sSQL = sSQL & "    on Proposta_Tb.Proposta_ID                       = #Sinistro_Principal.Proposta_ID" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    '-- Dados do Produto
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   set Produto_Nome                                  = Produto_Tb.Nome   " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Produto_Tb                     Produto_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Produto_ID                = Produto_Tb.Produto_ID " & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    '-- Situacao Proposta                                                                                                                                                                                                                    & vbNewLine
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Proposta_Situacao_Descricao                   = Situacao_Proposta_Tb.Descricao_Situacao" & vbNewLine
'    sSQL = sSQL & "  From Dado_Basico_Db..Situacao_Proposta_Tb          Situacao_Proposta_Tb  WITH (NOLOCK)  " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_Situacao         = upper(Situacao_Proposta_Tb.Situacao)" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    '-- Dados da Apolice                                                                                                                                                                                                                     & vbNewLine
'    sSQL = sSQL & "Declare @Max_Endosso_ID                              int" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & "Select @Max_Endosso_ID                               = Max(Endosso_Tb.Endosso_ID)" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Endosso_Tb Endosso_Tb           WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Endosso_Tb.Proposta_ID" & vbNewLine
'    sSQL = sSQL & " Where Endosso_Tb.Tp_Endosso_ID                      = 250" & vbNewLine
'    sSQL = sSQL & " " & vbNewLine
'    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Max_Endosso_ID = @Max_Endosso_ID" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Apolice_Dt_Inicio_Vigencia                    = Proposta_Adesao_Tb.Dt_Inicio_Vigencia" & vbNewLine
'    sSQL = sSQL & "  From #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Adesao_Tb             Proposta_Adesao_Tb  WITH (NOLOCK) "
'    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Proposta_Adesao_Tb.Proposta_ID" & vbNewLine
'    sSQL = sSQL & " Where #Sinistro_Principal.Produto_ID                = 721" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Apolice_Dt_Fim_Vigencia                       = Endosso_Tb.Dt_Fim_Vigencia_End" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Endosso_Tb                     Endosso_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Endosso_Tb.Proposta_ID" & vbNewLine
'    sSQL = sSQL & " Where Endosso_Tb.Endosso_ID                         = @Max_Endosso_ID" & vbNewLine
'    sSQL = sSQL & "   and Endosso_Tb.Tp_Endosso_ID                      = 250" & vbNewLine
'    sSQL = sSQL & "   and #Sinistro_Principal.Produto_ID                = 721" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Proposta_Adesao'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Proposta_Adesao" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Select Proposta_Adesao_Tb.Proposta_ID" & vbNewLine
'    sSQL = sSQL & "     , Proposta_Adesao_Tb.Dt_Inicio_Vigencia" & vbNewLine
'    sSQL = sSQL & "     , Proposta_Adesao_Tb.Dt_Fim_Vigencia" & vbNewLine
'    sSQL = sSQL & "  Into #Proposta_Adesao" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Adesao_Tb             Proposta_Adesao_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Proposta_Adesao_Tb.Proposta_ID" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Apolice_Dt_Inicio_Vigencia                    = Dt_Inicio_Vigencia" & vbNewLine
'    sSQL = sSQL & "  , Apolice_Dt_Fim_Vigencia                          = Dt_Fim_Vigencia" & vbNewLine
'    sSQL = sSQL & "  From #Proposta_Adesao" & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = #Proposta_Adesao.Proposta_ID " & vbNewLine
'    sSQL = sSQL & "   and (#Sinistro_Principal.Apolice_Dt_Inicio_Vigencia is null" & vbNewLine
'    sSQL = sSQL & "    and #Sinistro_Principal.Apolice_Dt_Fim_Vigencia  is null" & vbNewLine
'    sSQL = sSQL & "    and #Sinistro_Principal.Produto_ID               = 721 )" & vbNewLine
'    sSQL = sSQL & "    or (#Sinistro_Principal.Produto_ID               <> 721)" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Apolice_Dt_Inicio_Vigencia                    = Apolice_Tb.Dt_Inicio_Vigencia" & vbNewLine
'    sSQL = sSQL & "     , Apolice_Dt_Fim_Vigencia                       = Apolice_Tb.Dt_Fim_Vigencia" & vbNewLine
'    sSQL = sSQL & "     , Apolice_Dt_Emissao                                = Apolice_Tb.Dt_Emissao" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Apolice_Tb Apolice_Tb           WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Apolice_ID                = Apolice_Tb.Apolice_ID" & vbNewLine
'    sSQL = sSQL & "   and #Sinistro_Principal.Sucursal_Seguradora_ID    = Apolice_Tb.Sucursal_Seguradora_ID" & vbNewLine
'    sSQL = sSQL & "   and #Sinistro_Principal.Seguradora_Cod_Susep      = Apolice_Tb.Seguradora_Cod_Susep" & vbNewLine
'    sSQL = sSQL & "   and #Sinistro_Principal.Ramo_ID                   = Apolice_Tb.Ramo_ID" & vbNewLine
'    sSQL = sSQL & "  left Join #Proposta_Adesao" & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = #Proposta_Adesao.Proposta_ID " & vbNewLine
'    sSQL = sSQL & " Where #Sinistro_Principal.Produto_ID                <> 721" & vbNewLine
'    sSQL = sSQL & "   and #Proposta_Adesao.Proposta_ID                  is null" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
'    '-- Dados do Certificado                                                                                                                                                                                                                 & vbNewLine
'    sSQL = ""
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Certificado_Dt_Inicio_Vigencia                = Certificado_Tb.Dt_Inicio_Vigencia" & vbNewLine
'    sSQL = sSQL & "  , Certificado_Dt_Fim_Vigencia                      = Certificado_Tb.Dt_Fim_Vigencia" & vbNewLine
'    sSQL = sSQL & "  , Certficado_Dt_Emissao                            = Certificado_Tb.Dt_Emissao" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Certificado_Tb                 Certificado_Tb  WITH (NOLOCK) " & vbNewLine
'    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Proposta_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'    sSQL = sSQL & "    on Certificado_Tb.Proposta_ID                    = #Sinistro_Principal.Proposta_ID   " & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Corretor'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Corretor" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Select Top 1 Corretor_Tb.Nome" & vbNewLine
'    sSQL = sSQL & "  , Corretor_Tb.Situacao" & vbNewLine
'    sSQL = sSQL & "  Into #Corretor" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Corretor_Tb                    Corretor_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Corretagem_Tb                  Corretagem_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "    on Corretagem_Tb.Corretor_ID                     = Corretor_Tb.Corretor_ID" & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "    on Proposta_Tb.Proposta_ID                       = Corretagem_Tb.Proposta_ID" & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Produto_ID                = Proposta_Tb.Produto_ID" & vbNewLine
'    sSQL = sSQL & " Where Corretor_Tb.Situacao = 'a'    " & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Corretor_Nome                                 = #Corretor.Nome" & vbNewLine
'    sSQL = sSQL & "  From #Corretor" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    '-- Cliente_Tb                                                                                                                                                                                                                           & vbNewLine
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set #Sinistro_Principal.Cliente_Nome              = Cliente_Tb.Nome" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Cliente_Tb                     Cliente_Tb  WITH (NOLOCK) " & vbNewLine
'    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Cliente_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Cliente_ID                = Cliente_Tb.Cliente_ID " & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    '-- Tp_Cobertura_Tb                                                                                                                                                                                                                      & vbNewLine
'    '-- Cobertura Principal                                                                                                                                                                                                                  & vbNewLine
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Tp_Componente_Nome                            = Case When Proposta_Complementar_Tb.Prop_Cliente_ID is null" & vbNewLine
'    sSQL = sSQL & "                                                         Then 'TITULAR'" & vbNewLine
'    sSQL = sSQL & "                                                         Else 'CONJUGE'" & vbNewLine
'    sSQL = sSQL & "                                                       End" & vbNewLine
'    sSQL = sSQL & "  From #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Complementar_Tb  Proposta_Complementar_Tb   WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "    on Proposta_Complementar_Tb.Prop_Cliente_ID      = #Sinistro_Principal.CLiente_ID" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Tp_Cobertura_Nome                             = Tp_Cobertura_Tb.Nome" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Tp_Cob_Comp_Tb                 Tp_Cob_Comp_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Cobertura_Tb                Tp_Cobertura_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "    on Tp_Cobertura_Tb.Tp_Cobertura_ID               = Tp_Cob_Comp_Tb.Tp_Cobertura_ID" & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Escolha_Tp_Cob_Vida_Tb         Escolha_Tp_Cob_Vida_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "    on Escolha_Tp_Cob_Vida_Tb.Tp_Cob_Comp_ID         = Tp_Cob_Comp_Tb.Tp_Cob_Comp_ID" & vbNewLine
'    sSQL = sSQL & "   and Escolha_Tp_Cob_Vida_Tb.Tp_Componente_ID       = Tp_Cob_Comp_Tb.Tp_Componente_ID " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Componente_Tb               Tp_Componente_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "    on Tp_Componente_Tb.Tp_Componente_ID             = Tp_Cob_Comp_Tb.Tp_Componente_ID " & vbNewLine
'    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Proposta_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Escolha_Tp_Cob_Vida_Tb.Proposta_ID" & vbNewLine
'    sSQL = sSQL & " Where Escolha_Tp_Cob_Vida_Tb.Class_Tp_Cobertura     = 'B'" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'
'    'AKIO.OKUNO - INICIO - 05/10/2012
'    If bytTipoRamo = bytTipoRamo_Vida Then
'        sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'        sSQL = sSQL & "   Set Tp_Cobertura_Nome                             = Tp_Cobertura_Tb.Nome" & vbNewLine
'        sSQL = sSQL & "     , Tp_Componente_ID                              = Escolha_Tp_Cob_Vida_Tb.Tp_Componente_ID " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Tp_Cob_Comp_Tb                 Tp_Cob_Comp_Tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Cobertura_Tb                Tp_Cobertura_Tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "    on Tp_Cobertura_Tb.Tp_Cobertura_ID               = Tp_Cob_Comp_Tb.Tp_Cobertura_ID" & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Escolha_Tp_Cob_Vida_Tb         Escolha_Tp_Cob_Vida_Tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "    on Escolha_Tp_Cob_Vida_Tb.Tp_Cob_Comp_ID         = Tp_Cob_Comp_Tb.Tp_Cob_Comp_ID" & vbNewLine
'        sSQL = sSQL & "   and Escolha_Tp_Cob_Vida_Tb.Tp_Componente_ID       = Tp_Cob_Comp_Tb.Tp_Componente_ID " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Componente_Tb               Tp_Componente_Tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "    on Tp_Componente_Tb.Tp_Componente_ID             = Tp_Cob_Comp_Tb.Tp_Componente_ID " & vbNewLine
'        'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Proposta_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'        sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Escolha_Tp_Cob_Vida_Tb.Proposta_ID" & vbNewLine
'        sSQL = sSQL & " Where Escolha_Tp_Cob_Vida_Tb.Class_Tp_Cobertura     = 'B'" & vbNewLine
'        sSQL = sSQL & "" & vbNewLine
'    Else
'        sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Cobertura_Nome'),0) >0" & vbNewLine
'        sSQL = sSQL & " BEGIN" & vbNewLine
'        sSQL = sSQL & "     DROP TABLE #Cobertura_Nome" & vbNewLine
'        sSQL = sSQL & " END" & vbNewLine
'        sSQL = sSQL & "Select Tp_Cobertura_Nome = Tp_Cobertura_Tb.Nome                                                                                  " & vbNewLine
'        sSQL = sSQL & "  Into #Cobertura_Nome"
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Tp_Cob_Item_Prod_Tb                    Tp_Cob_Item_Prod_Tb   WITH (NOLOCK)               " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Cobertura_Tb                        Tp_Cobertura_Tb  WITH (NOLOCK)                         " & vbNewLine
'        sSQL = sSQL & "    on Tp_Cobertura_Tb.Tp_Cobertura_ID                       = Tp_Cob_Item_Prod_Tb.Tp_Cobertura_ID           " & vbNewLine
'        sSQL = sSQL & "  Join #Sinistro_Principal                                                                                   " & vbNewLine
'        sSQL = sSQL & "    on #Sinistro_principal.Ramo_ID                           = Tp_Cob_Item_Prod_Tb.Ramo_ID                   " & vbNewLine
'        sSQL = sSQL & "   and #Sinistro_principal.Produto_ID                        = Tp_Cob_Item_Prod_Tb.Produto_ID                " & vbNewLine
'        sSQL = sSQL & " Where Tp_Cob_Item_Prod_Tb.Class_Tp_Cobertura                = 'B'                                           " & vbNewLine
'        sSQL = sSQL & " Order by Tp_Cobertura_Tb.Nome" & vbNewLine
'        sSQL = sSQL & " Update #Sinistro_Principal                                                                                   " & vbNewLine
'        sSQL = sSQL & "   set Tp_Cobertura_Nome = #Cobertura_Nome.Tp_Cobertura_Nome                                                                                  " & vbNewLine
'        sSQL = sSQL & "  From #Cobertura_Nome                                                                                       " & vbNewLine
'        sSQL = sSQL & " Drop table #Cobertura_Nome                                                                                  " & vbNewLine
'    End If
'    'AKIO.OKUNO - FIM - 05/10/2012
'
'    '-->> Dados do Frame: Aviso                                                                                                                                                                                                              & vbNewLine
'    '-- Evento_Sinistro_Tb                                                                                                                                                                                                                   & vbNewLine
'    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Evento_Sinistro_Nome                          = Evento_Sinistro_Tb.Nome" & vbNewLine
'    'sSQL = sSQL & "  From #Sinistro_Principal                           (Index = FK_Evento_Sinistro_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'    sSQL = sSQL & "  From #Sinistro_Principal                           " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Evento_Sinistro_Tb             Evento_Sinistro_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "    on Evento_Sinistro_Tb.Evento_Sinistro_ID         = #Sinistro_Principal.Evento_Sinistro_ID " & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    '-- SubEvento_Sinistro_Tb                                                                                                                                                                                                                & vbNewLine
'    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set SubEvento_Sinistro_Nome                       = SubEvento_Sinistro_Tb.Nome" & vbNewLine
'    'sSQL = sSQL & "  From #Sinistro_Principal                           (Index = FK_SubEvento_Sinistro_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'    sSQL = sSQL & "  From #Sinistro_Principal                           " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.SubEvento_Sinistro_Tb          SubEvento_Sinistro_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "    on SubEvento_Sinistro_Tb.SubEvento_Sinistro_ID   = #Sinistro_Principal.SubEvento_Sinistro_ID " & vbNewLine
'    sSQL = sSQL & "     " & vbNewLine
'    'Dados da Grid: Aviso" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosGerais_Aviso'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #DadosGerais_Aviso" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Select Sinistro_BB_Tb.Sinistro_Bb" & vbNewLine
'    sSQL = sSQL & "  , Sinistro_BB_Tb.Dt_Fim_Vigencia" & vbNewLine
'    sSQL = sSQL & "  , Agencia_ID                                       = #Sinistro_Principal.Agencia_ID" & vbNewLine
'    sSQL = sSQL & "  , Situacao                                         = Case When Sinistro_BB_Tb.Dt_Fim_Vigencia is Null" & vbNewLine
'    sSQL = sSQL & "                                                       Then 'Ativo'" & vbNewLine
'    sSQL = sSQL & "                                                       Else 'Inativo'" & vbNewLine
'    sSQL = sSQL & "                                                       End" & vbNewLine
'    sSQL = sSQL & "  Into #DadosGerais_Aviso" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_BB_Tb                 Sinistro_BB_Tb  WITH (NOLOCK) " & vbNewLine
'    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = PK_Sinistro_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_BB_Tb.Sinistro_ID" & vbNewLine
'    sSQL = sSQL & " Order By Sinistro_BB" & vbNewLine
'    sSQL = sSQL & "     " & vbNewLine
'    sSQL = sSQL & "     " & vbNewLine
'
'    'DADOS DO SINISTRADO" & vbNewLine
'    sSQL = sSQL & "Update #Sinistro_Principal " & vbNewLine
'    sSQL = sSQL & "   Set Sinistro_Vida_Nome                            = IsNull(Sinistro_Vida_Tb.Nome, '')" & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Vida_CPF                             = IsNull(Sinistro_Vida_Tb.Cpf, '')" & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Vida_Dt_Nasc                         = IsNull(Sinistro_Vida_Tb.Dt_Nascimento, '')" & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Vida_Sexo                            = IsNull(Sinistro_Vida_Tb.Sexo, '') " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Vida_Tb               Sinistro_Vida_Tb  WITH (NOLOCK) " & vbNewLine
'    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = PK_Sinistro_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Vida_Tb.Sinistro_ID" & vbNewLine
'    sSQL = sSQL & "     " & vbNewLine
'    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & "   Set Sinistrado_Cliente_ID                         = Pessoa_Fisica_Tb.Pf_Cliente_ID" & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Pessoa_Fisica_Tb               Pessoa_Fisica_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_Vida_CPF         = Pessoa_Fisica_Tb.CPF" & vbNewLine
'    sSQL = sSQL & "   and #Sinistro_Principal.Sinistro_Vida_Dt_Nasc     = Pessoa_Fisica_Tb.Dt_Nascimento" & vbNewLine
'    sSQL = sSQL & "   and #Sinistro_Principal.Sinistro_Vida_Sexo        = Pessoa_Fisica_Tb.Sexo" & vbNewLine
'
'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
'    Exit Sub
'
'Erro:
'    TrataErroGeral "SelecionaDados_Sinistro", Me.name
'    FinalizarAplicacao
'
'End Sub
'ALAN.NEVES - SelecionaDados_Sinistro - 17/04/2013
Private Sub SelecionaDados_Sinistro()
    Dim sSQL                                    As String
    Dim rsRecordSet                             As ADODB.Recordset
    
    On Error GoTo Erro
    
    MousePointer = vbHourglass
        
    sSQL = ""
    sSQL = sSQL & " set nocount on " & vbNewLine  'Alan Neves - 15/04/2013 - Teste carregavariavies_Sinistro
    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Principal'),0) >0" & vbNewLine
    sSQL = sSQL & " if OBJECT_ID('tempdb..#Sinistro_Principal') IS NOT NULL " & vbNewLine
    sSQL = sSQL & " BEGIN" & vbNewLine
    sSQL = sSQL & "     DROP TABLE #Sinistro_Principal" & vbNewLine
    sSQL = sSQL & " END" & vbNewLine
    sSQL = sSQL & "Create Table #Sinistro_Principal "
    sSQL = sSQL & "     ( Sinistro_ID                                   Numeric(11)         " & vbNewLine   ' -- Sinistro_Tb
    sSQL = sSQL & "     , Sucursal_Seguradora_ID                        Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Seguradora_Cod_Susep                          Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Apolice_ID                                    Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Ramo_ID                                       Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Ramo_Nome                                     VarChar(60)         " & vbNewLine  ' -- Ramo_Tb
    sSQL = sSQL & "     , Seguradora_Nome_Lider                         VarChar(60)         " & vbNewLine  ' -- Seguradora_Tb (Nome Lider)
    sSQL = sSQL & "     , Apolice_Dt_Inicio_Vigencia                    SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
    sSQL = sSQL & "     , Apolice_Dt_Fim_Vigencia                       SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
    sSQL = sSQL & "     , Apolice_Dt_Emissao                            SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
    sSQL = sSQL & "     , Proposta_ID                                   Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Produto_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb
    sSQL = sSQL & "     , Produto_Nome                                  VarChar(60)         " & vbNewLine  ' -- Produto_Tb
    sSQL = sSQL & "     , Certificado_Dt_Inicio_Vigencia                SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
    sSQL = sSQL & "     , Certificado_Dt_Fim_Vigencia                   SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
    sSQL = sSQL & "     , Certificado_Dt_Emissao                         SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
    sSQL = sSQL & "     , Certificado_ID                                Numeric(9)          " & vbNewLine  ' -- Certificado_Tb
    sSQL = sSQL & "     , Certificado_Sub_Grupo_ID                      Int                 " & vbNewLine  ' -- seguro_vida_sub_grupo_tb
    sSQL = sSQL & "     , Quantidade_Endossos                           Int                 " & vbNewLine  ' -- Count Endosso_Tb ???
    sSQL = sSQL & "     , Proposta_Situacao                             VarChar(1)          " & vbNewLine  ' -- Proposta_Tb
    sSQL = sSQL & "     , Proposta_Situacao_Descricao                   VarChar(60)         " & vbNewLine  ' -- Dado_Basico_Db..Situacao_Proposta_tb.Descricao
    sSQL = sSQL & "     , Tp_Cobertura_Nome                             VarChar(60)         " & vbNewLine  ' -- Tp_Cobertura_Tb
    sSQL = sSQL & "     , Corretor_Nome                                 VarChar(60)         " & vbNewLine  ' -- Corretor_Tb
    sSQL = sSQL & "     , Cliente_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb (Prop_Cliente_ID)
    sSQL = sSQL & "     , Cliente_Nome                                  VarChar(60)         " & vbNewLine  ' -- Cliente_Tb
    sSQL = sSQL & "     , Tp_Componente_ID                              TinyInt             " & vbNewLine  ' -- Escolha_Tp_Cob_Vida_Tp
    sSQL = sSQL & "     , Tp_Componente_Nome                            VarChar(60)         " & vbNewLine  ' -- Tp_Componente_Tb
    sSQL = sSQL & "     , Evento_Sinistro_ID                            Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Evento_Sinistro_Nome                          VarChar(60)         " & vbNewLine  ' -- Evento_Sinistro_Tb
    sSQL = sSQL & "     , SubEvento_Sinistro_ID                         Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , SubEvento_Sinistro_Nome                       VarChar(120)         " & vbNewLine  ' -- SubEvento_Sinistro_Tb
    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                        SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Dt_Entrada_Seguradora                         SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Dt_Aviso_Sinistro                             SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Dt_Inclusao                                   SmallDateTIme       " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Agencia_ID                                    Numeric(4)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Sinistro_Vida_Nome                            VarChar(60)         " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
    sSQL = sSQL & "     , Sinistro_Vida_CPF                             VarChar(11)         " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
    sSQL = sSQL & "     , Sinistro_Vida_Dt_Nasc                         DateTime            " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
    sSQL = sSQL & "     , Sinistro_Vida_Sexo                            Char(1)             " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
    sSQL = sSQL & "     , Sinistrado_Cliente_ID                         Int                 " & vbNewLine  ' -- Pessoa_Fisica_Tb (Pf_Cliente_ID) (Sinistrado)
    sSQL = sSQL & "     , Solicitante_ID                                Int                 " & vbNewLine  ' -- Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Nome                     VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Endereco                 VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Bairro                   VarChar(30)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio_ID             Numeric(3)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio                VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Municipio_Nome                    VarChar(60)         " & vbNewLine  ' -- Municipio_Tb (Nome) (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD1                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax                  VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax             VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Estado                   Char(2)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_EMail                    VarChar(1000)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante) - jose soares - confitec - 04/06/2020 - aumentando caracteres do campo email
    sSQL = sSQL & "     , Solicitante_Sinistro_CEP                      VarChar(8)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Sinistro_Causa_Observacao_Item                Int                 " & vbNewLine  ' -- Sinistro_Causa_Observacao_Tb
    sSQL = sSQL & "     , Sinistro_Causa_Observacao                     VarChar(300)        " & vbNewLine  ' -- Sinistro_Causa_Observacao_Tb
    sSQL = sSQL & "     , Sinistro_Situacao_ID                          Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Sinistro_Situacao                             VarChar(60)         " & vbNewLine  ' -- Dado_Basico_Db..Sinistro_Situacao_Tb
    sSQL = sSQL & "     , Max_Endosso_ID                                Int                 " & vbNewLine  ' -- Endosso_Tb
    
    'Para consulta
    sSQL = sSQL & "     , Apolice_Terceiros_Num_Ordem_Co_Seguro_Aceito          Numeric(30,0)           " & vbNewLine 'EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
    sSQL = sSQL & "     , Apolice_Terceiros_Seg_Cod_Susep_Lider                 Numeric(5,0)            " & vbNewLine
    sSQL = sSQL & "     , Apolice_Terceiros_Ramo_ID                             Int                     " & vbNewLine
    sSQL = sSQL & "     , Apolice_Terceiros_Sucursal_Seg_Lider                  Numeric(5,0)            " & vbNewLine
    
    'Campos do frame Cosseguro
    sSQL = sSQL & "     , Cosseguro_Aceito_sinistro_id_lider                    Numeric(30)             " & vbNewLine 'EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
    sSQL = sSQL & "     , Cosseguro_Aceito_Num_Apolice_Lider                    Numeric(30,0)           " & vbNewLine 'EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
    sSQL = sSQL & "     , Cosseguro_Aceito_Ramo_Lider                           Int                     " & vbNewLine
    sSQL = sSQL & "     , Cosseguro_Aceito_Sucursal_Seg_Lider                   Numeric(5,0)            " & vbNewLine
    sSQL = sSQL & "     , Cosseguro_Aceito_Seg_Cod_Susep_Lider                  Numeric(5,0)            " & vbNewLine
    sSQL = sSQL & "     , Apolice_Terceiros_Apolice_ID                          Numeric(9,0)            " & vbNewLine
    sSQL = sSQL & "     , Seguradora_Nome                                       VarChar(60)             " & vbNewLine
    sSQL = sSQL & "     , Cosseguro_Ramo_Nome                                   VarChar(60)             " & vbNewLine
    sSQL = sSQL & "     , Sucursal_Seguradora_Nome                              VarChar(60)             " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Endereco                                     VarChar(60)             " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Bairro                                       VarChar(30)             " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Estado                                       VarChar(20)             " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Municipio                                    VarChar(30)             " & vbNewLine
    sSQL = sSQL & "     , Passivel_Ressarcimento                                Char(1)                 " & vbNewLine       'AKIO.OKUNO - 02/10/2012
    
    '(INI) Demanda 18225206 - O campo grau_parentesco nunca foi preenchido na solicitante_sinistro_tb.
    sSQL = sSQL & "     , Solicitante_Sinistro_Grau_Parentesco_ID               Int                     " & vbNewLine ' -- Solicitante_Sinistro_Tb (Solicitante)
    '(FIM) Demanda 18225206.

    'Sergio Demanda Historico Clientes Vida
    sSQL = sSQL & "     , cpf_cnpj                                              VARCHAR(14)              " & vbNewLine ' -- cliente_tb (cpf_cnpj)
    
    'Andr� Martins - Facelift 14/11/2019
    sSQL = sSQL & "     , plano_id                                              varchar(5)              " & vbNewLine
    sSQL = sSQL & "     , plano_nome                                            varchar(30)             " & vbNewLine
    
    
    
    sSQL = sSQL & "     ) " & vbNewLine
    sSQL = sSQL & "     " & vbNewLine
    sSQL = sSQL & "Create Index PK_Sinistro_ID                          on #Sinistro_Principal (Sinistro_ID)" & vbNewLine
    sSQL = sSQL & "Create Index FK_Proposta_ID                          on #Sinistro_Principal (Proposta_ID)" & vbNewLine
    sSQL = sSQL & "Create Index FK_Evento_Sinistro_ID                   on #Sinistro_Principal (Evento_Sinistro_ID)" & vbNewLine
    sSQL = sSQL & "Create Index FK_SubEvento_Sinistro_ID                on #Sinistro_Principal (SubEvento_Sinistro_ID)" & vbNewLine
    sSQL = sSQL & "Create Index FK_A                                    on #Sinistro_Principal (Sucursal_Seguradora_ID, Seguradora_Cod_Susep, Apolice_ID, Ramo_ID)" & vbNewLine
    sSQL = sSQL & "Create Index FK_Seguradora_Cod_Susep                 on #Sinistro_Principal (Seguradora_Cod_Susep)" & vbNewLine
    sSQL = sSQL & "Create Index FK_Cliente_ID                           on #Sinistro_Principal (Cliente_ID)" & vbNewLine
    sSQL = sSQL & "Create Index FK_Solicitante_ID                       on #Sinistro_Principal (Solicitante_ID)" & vbNewLine
    sSQL = sSQL & "" & vbNewLine
    
'AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
'TEMPS RETIRADAS POIS N�O ESTAVAM SENDO UTILIZADAS MAIS
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_BB'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Sinistro_BB" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Create Table #Sinistro_BB " & vbNewLine
'    sSQL = sSQL & "     ( Sinistro_BB                        Numeric(13)         " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_ID                        Numeric(11)         " & vbNewLine
'    sSQL = sSQL & "     , Dt_Fim_Vigencia                    smalldatetime      " & vbNewLine  ' Alan Neves - 18/04/2013
'    sSQL = sSQL & "     , Agencia_ID                         Numeric(4)    )     " & vbNewLine ' Alan Neves - 18/04/2013
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Entrada_GTR'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Entrada_GTR" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & "Select Entrada_GTR_Tb.* " & vbNewLine
'    sSQL = sSQL & " Into #Entrada_GTR   " & vbNewLine
'    sSQL = sSQL & " From Interface_Db..Entrada_GTR_Tb Entrada_GTR_Tb  WITH (NOLOCK)    " & vbNewLine
'    sSQL = sSQL & " where 1 = 0  " & vbNewLine
'    ' Para identificar se visualiza ou n�o bot�o de Analise t�cnica
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Seguro_Fraude_Historico'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Seguro_Fraude_Historico" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & "Create Table #Seguro_Fraude_Historico " & vbNewLine
'    sSQL = sSQL & "     ( Fase_ID                        int         " & vbNewLine
'    sSQL = sSQL & "       , Nivel_ID                     int )        " & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Evento_SegBR_Sinistro_Atual_Temp'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Evento_SegBR_Sinistro_Atual_Temp" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "create table #Evento_SegBR_Sinistro_Atual_Temp ( " & vbNewLine
'    sSQL = sSQL & "   Evento_id         int " & vbNewLine
'    sSQL = sSQL & " , Evento_SegBr_ID   int  " & vbNewLine
'    sSQL = sSQL & " , Evento_BB_Id      int  " & vbNewLine
'    sSQL = sSQL & " , Num_Recibo        tinyint " & vbNewLine  ' Alan Neves - 18/04/2013
'    sSQL = sSQL & " , Sinistro_ID       Numeric(11) " & vbNewLine  ' Alan Neves - 18/04/2013
'    sSQL = sSQL & " , Sinistro_BB       Numeric(13) )" & vbNewLine  ' Alan Neves - 18/04/2013
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Evento_SegBR_Sinistro_Atual'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Evento_SegBR_Sinistro_Atual" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Select Evento_ID " & vbNewLine
'    sSQL = sSQL & " , Evento_SegBr_ID  " & vbNewLine
'    sSQL = sSQL & " Into #Evento_SegBR_Sinistro_Atual " & vbNewLine
'    sSQL = sSQL & " From #Evento_SegBR_Sinistro_Atual_Temp " & vbNewLine
'    sSQL = sSQL & " Where 1 = 0" & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Evento_ID'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Evento_ID" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Create table #Evento_ID ( " & vbNewLine
'    sSQL = sSQL & " Evento_ID int ) " & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Localizacao'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Localizacao" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Create Table #Localizacao " & vbNewLine
'    sSQL = sSQL & "     ( prox_localizacao_processo          char(1)           " & vbNewLine
'    sSQL = sSQL & "     , Evento_BB_ID                       Int               " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_BB                        Numeric(13) )     " & vbNewLine
'
'    If bytTipoRamo = bytTipoRamo_RE Then
'        sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Principal_Endereco_Risco'),0) >0" & vbNewLine
'        sSQL = sSQL & " BEGIN" & vbNewLine
'        sSQL = sSQL & "     DROP TABLE #Sinistro_Principal_Endereco_Risco" & vbNewLine
'        sSQL = sSQL & " END" & vbNewLine
'    End If
'AKIO.OKUNO - #RETIRADA - FIM - 09/05/2013
    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosGerais_Aviso'),0) >0" & vbNewLine
    sSQL = sSQL & " if OBJECT_ID('tempdb..#DadosGerais_Aviso') IS NOT NULL " & vbNewLine
    sSQL = sSQL & " BEGIN" & vbNewLine
    sSQL = sSQL & "     DROP TABLE #DadosGerais_Aviso" & vbNewLine
    sSQL = sSQL & " END" & vbNewLine
    sSQL = sSQL & "Create Table #DadosGerais_Aviso " & vbNewLine
    sSQL = sSQL & "     ( Sinistro_Bb                           Numeric(13)         " & vbNewLine
    sSQL = sSQL & "     , Dt_Fim_Vigencia                       smalldatetime       " & vbNewLine
    sSQL = sSQL & "     , Agencia_ID                            Numeric(4)          " & vbNewLine
    sSQL = sSQL & "     , Situacao                              varchar(20)  )      " & vbNewLine  ' -- Sinistro_Tb

'Alan Neves - 18/04/2013  - Teste performance - Proc Stamato
'    sSQL = sSQL & "  Exec seguros_db..SEGS10922_SPS '" & grdSelecao.TextMatrix(grdSelecao.Row, 4) & "','" & IIf(gbldSinistro_ID = 0, "", gbldSinistro_ID) & "'" & ", '" & lAviso_Solicitante_ID & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'," & bytTipoRamo & vbNewLine
    sSQL = sSQL & "  Exec seguros_db.DBO.SEGS10922_SPS '" & grdSelecao.TextMatrix(grdSelecao.Row, 5) & "','" & IIf(gbldSinistro_ID = 0, "", gbldSinistro_ID) & "'" & ", '" & lAviso_Solicitante_ID & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'," & bytTipoRamo & vbNewLine
         
    
'+----------------------------------------------------------------------
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'|
    sSQL = sSQL & vbCrLf & " SET NOCOUNT ON  EXEC SEGS13110_SPS"
'    sSQL = sSQL & "Select seguradora_cod_susep, sucursal_seguradora_id, produto_id, Sinistro_Situacao_ID, Tp_Componente_ID, Agencia_ID " & vbCrLf
'    sSQL = sSQL & "  From #Sinistro_Principal"
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'+----------------------------------------------------------------------
   
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                           glAmbiente_id, _
                                           App.Title, _
                                           App.FileDescription, _
                                           sSQL, _
                                           lConexaoLocal, True)
    
    If strTipoProcesso <> strTipoProcesso_Avisar And strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then
   
        With rsRecordSet
            If Not .EOF Then
                'Ajustando vari�veis globais
                gbllSeguradora_Cod_Susep = .Fields!seguradora_cod_susep
                gbllSucursal_Seguradora_ID = .Fields!sucursal_seguradora_id
                gbllProduto_ID = .Fields!produto_Id
                gblsSinistro_Situacao_ID = .Fields!Sinistro_Situacao_ID
                gblsTp_Componente = .Fields!Tp_Componente_ID & ""    'FLAVIO.BEZERRA 04/01/2013
                If bytTipoRamo = bytTipoRamo_Vida Then
                    If gblsTp_Componente_ID = "C" Then
                        gblsTp_Componente_ID = "C"
                    Else
                        gblsTp_Componente_ID = "T"
                    End If
                End If
                gbllAgencia_ID = "0" & .Fields!Agencia_ID
            End If
        End With
        
    End If
    
    MousePointer = vbNormal

    Set rsRecordSet = Nothing
    
    Exit Sub
    
Erro:
    TrataErroGeral "SelecionaDados_Sinistro", Me.name
    FinalizarAplicacao

End Sub

Private Sub CarregaVariaveis_Sinistro()
    Dim rsRecordSet                             As ADODB.Recordset
    Dim sSQL                                    As String

    On Error GoTo Erro

    MousePointer = vbHourglass

    sSQL = ""
    ' Alan Neves - 15/04/2013 - Inicio
'    sSQL = sSQL & "Select * " & vbCrLf
    sSQL = sSQL & "Select seguradora_cod_susep, sucursal_seguradora_id, produto_id, Sinistro_Situacao_ID, Tp_Componente_ID, Agencia_ID " & vbCrLf
    sSQL = sSQL & "  From #Sinistro_Principal"
    ' Alan Neves - 15/04/2013 - Fim

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, True)
    With rsRecordSet
        If Not .EOF Then

            'Dados do Cabe�alho do Form:

'            dApolice_Dt_Fim_Vigencia = IIf(IsNull(.Fields!Apolice_Dt_Fim_Vigencia), "1900-01-01", .Fields!Apolice_Dt_Fim_Vigencia)
'            dApolice_Dt_Inicio_Vigencia = IIf(IsNull(.Fields!Apolice_Dt_Inicio_Vigencia), "1900-01-01", .Fields!Apolice_Dt_Inicio_Vigencia)
            
            'Ajustando vari�veis globais
            gbllSeguradora_Cod_Susep = .Fields!seguradora_cod_susep
            gbllSucursal_Seguradora_ID = .Fields!sucursal_seguradora_id
            gbllProduto_ID = .Fields!produto_Id
            gblsSinistro_Situacao_ID = .Fields!Sinistro_Situacao_ID
            'AKIO.OKUNO - INICIO - 05/10/2012
            'gblsTp_Componente_ID = .Fields!Tp_Componente_ID & "" 'FLAVIO.BEZERRA 04/01/2013
            gblsTp_Componente = .Fields!Tp_Componente_ID & ""    'FLAVIO.BEZERRA 04/01/2013
            If bytTipoRamo = bytTipoRamo_Vida Then
                If gblsTp_Componente_ID = "C" Then
                    gblsTp_Componente_ID = "C"
                Else
                    gblsTp_Componente_ID = "T"
                End If
            End If
            'AKIO.OKUNO - FIM - 05/10/2012
            gbllAgencia_ID = "0" & .Fields!Agencia_ID
                        
        End If
    End With

    MousePointer = vbNormal

    Set rsRecordSet = Nothing
    Exit Sub


Erro:
    Call TrataErroGeral("CarregaVariaveis_Sinistro", "frmConsultaSinistro")
    Call FinalizarAplicacao

End Sub



Private Sub InicializaInterface_DIGISINLocal(sChave As String)
    
    InicializaInterface_DIGISIN App.EXEName, sChave, Me

End Sub

'AKIO.OKUNO - RETIRADO - Contratou_Cobertura - 20/04/2013
'Public Function Contratou_Cobertura(tp_Cobertura_id As String) As Boolean
'
'    Dim sSQL As String
'    Dim rs As Recordset
'    Dim rsCobertura As Recordset
'    Dim bTranspInternacional As Boolean
'
'    Contratou_Cobertura = True
'
'    sSQL = ""
'    sSQL = sSQL & "Select Cod_Objeto_Segurado                                               " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Re_Tb             Sinistro_Re_Tb  WITH (NOLOCK)  " & vbNewLine
'    sSQL = sSQL & " Where Sinistro_ID                               = " & gbldSinistro_ID & vbNewLine
'    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                  glAmbiente_id, _
'                                  App.Title, _
'                                  App.FileDescription, _
'                                  sSQL, _
'                                  lConexaoLocal, _
'                                  True)
'
'    If Not rs.EOF Then
'        gblsCod_Objeto_Segurado = rs.Fields!COd_Objeto_Segurado
'    End If
'
'    sSQL = "SELECT dt_emissao "
'    sSQL = sSQL & "FROM apolice_tb  WITH (NOLOCK)  "
'    sSQL = sSQL & "WHERE apolice_id = " & gbllApolice_ID
'    sSQL = sSQL & "  AND ramo_id = " & gbllRamo_ID
'    sSQL = sSQL & "  AND sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID
'    sSQL = sSQL & "  AND seguradora_cod_susep = " & gbllSeguradora_Cod_Susep
'
'    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                 glAmbiente_id, _
'                                 App.Title, _
'                                 App.FileDescription, _
'                                 sSQL, _
'                                 lConexaoLocal, _
'                                 True)
'
'    If Not rs.EOF Then
'        If Val(Format(rs.Fields!dt_emissao, "yyyymmdd")) < 20000701 Then
'            Exit Function
'        End If
'    End If
'    Set rs = Nothing
'
'    If bytTipoRamo = "1" Then
'        sSQL = "SELECT tp_cobertura_id "
'        sSQL = sSQL & "FROM escolha_plano_tb  WITH (NOLOCK) , tp_cob_comp_tb  WITH (NOLOCK) , tp_cob_comp_plano_tb  WITH (NOLOCK) , "
'        sSQL = sSQL & "     tp_plano_tp_comp_tb  WITH (NOLOCK) , plano_tb  WITH (NOLOCK)  "
'        sSQL = sSQL & "WHERE escolha_plano_tb.Proposta_id = " & gbllProposta_ID
'        sSQL = sSQL & "  AND convert(smalldatetime, convert(varchar(10), escolha_plano_tb.dt_escolha, 111)) <= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' "
'        sSQL = sSQL & "  AND (convert(smalldatetime, convert(varchar(10), escolha_plano_tb.dt_fim_vigencia, 111)) >= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' OR "
'        sSQL = sSQL & "       escolha_plano_tb.dt_fim_vigencia IS NULL) "
'        sSQL = sSQL & "  AND escolha_plano_tb.plano_id = plano_tb.plano_id "
'        sSQL = sSQL & "  AND escolha_plano_tb.dt_inicio_vigencia = plano_tb.dt_inicio_vigencia "
'        sSQL = sSQL & "  AND escolha_plano_tb.produto_id = plano_tb.produto_id "
'        sSQL = sSQL & "  AND plano_tb.tp_plano_id = tp_plano_tp_comp_tb.tp_plano_id "

'        sSQL = sSQL & "  AND tp_cob_comp_plano_tb.tp_plano_id = plano_tb.tp_plano_id "
'        sSQL = sSQL & "  AND tp_cob_comp_tb.tp_cob_comp_id = tp_cob_comp_plano_tb.tp_cob_comp_id "

'        sSQL = sSQL & "  AND tp_cob_comp_tb.tp_componente_id = tp_plano_tp_comp_tb.tp_componente_id "
'        sSQL = sSQL & "  AND tp_cob_comp_tb.tp_cobertura_id = " & tp_Cobertura_id
'        If gblsTp_Componente_ID = "T" Then
'            sSQL = sSQL & "   AND   tp_cob_comp_tb.tp_componente_id = 1"  'Titular
'        Else
'            sSQL = sSQL & "   AND   tp_cob_comp_tb.tp_componente_id = 3"  'Conjuge
'        End If
'        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                      glAmbiente_id, _
'                                      App.Title, _
'                                      App.FileDescription, _
'                                      sSQL, _
'                                      lConexaoLocal, _
'                                      True)
'        If rs.EOF Then
'            Set rs = Nothing
'            sSQL = "SELECT tp_cobertura_id"
'            sSQL = sSQL & " FROM escolha_tp_cob_vida_tb  WITH (NOLOCK) , tp_cob_comp_tb  WITH (NOLOCK)  "
'            If vProduto = 14 Then 'OUROVIDA PRODUTOR RURAL
'                'Buscar cobertua pela proposta_b�sica
'                Dim vsql As String
'                Dim RS_AUX As Recordset
'                vsql = " select  proposta_id "
'                vsql = vsql & " From apolice_tb  WITH (NOLOCK)  "
'                vsql = vsql & " where   apolice_id = " & gbllApolice_ID
'                vsql = vsql & " and ramo_id = " & gbllRamo_ID
'                Set RS_AUX = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                                  glAmbiente_id, _
'                                                  App.Title, _
'                                                  App.FileDescription, _
'                                                  sSQL, _
'                                                  lConexaoLocal, _
'                                                  True)
'
'                sSQL = sSQL & " WHERE escolha_tp_cob_vida_tb.Proposta_id = " & RS_AUX.Fields!Proposta_id
'                Set RS_AUX = Nothing
'            Else
'                sSQL = sSQL & " WHERE escolha_tp_cob_vida_tb.Proposta_id = " & gbllProposta_ID
'            End If
'            'Fim
'            sSQL = sSQL & " AND (convert(smalldatetime, convert(varchar(10), escolha_tp_cob_vida_tb.dt_fim_vigencia_esc, 111)) >= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' "
'            sSQL = sSQL & " OR  escolha_tp_cob_vida_tb.dt_fim_vigencia_esc IS NULL)"
'            sSQL = sSQL & " AND escolha_tp_cob_vida_tb.tp_cob_comp_id = tp_cob_comp_tb.tp_cob_comp_id"
'            sSQL = sSQL & " AND tp_cob_comp_tb.tp_cobertura_id = " & tp_Cobertura_id
'
''            'Verifica se o tipo de componente � diferente de 1-TITULAR ou 3-CONJUGE
''            If gblsTp_Componente_ID <> "" Then                                                                                                         'CRISTOVAO.RODRIGUES (POR AKIO.OKUNO) - 10/10/2012
''                sSQL = sSQL & " AND tp_cob_comp_tb.tp_componente_id = " & gblsTp_Componente_ID         'CRISTOVAO.RODRIGUES (POR AKIO.OKUNO) - 10/10/2012
''            Else                                                                                                                                                                       'CRISTOVAO.RODRIGUES (POR AKIO.OKUNO) - 10/10/2012
''                If gblsTp_Componente_ID = "T" Then
''                    sSQL = sSQL & " AND tp_cob_comp_tb.tp_componente_id = 1"  'Titular
''                Else
''                    sSQL = sSQL & " AND tp_cob_comp_tb.tp_componente_id = 3"  'Conjuge
''                End If
''            End If                                                                                      'CRISTOVAO.RODRIGUES (POR AKIO.OKUNO) - 10/10/2012
'
'           'Verifica se o tipo de componente � diferente de 1-TITULAR ou 3-CONJUGE
'            If gblsTp_Componente <> "" Then
'                sSQL = sSQL & " AND tp_cob_comp_tb.tp_componente_id = " & gblsTp_Componente         'FLAVIO.BEZERRA 04/01/2013
'            Else
'                If gblsTp_Componente_ID = "T" Then
'                    sSQL = sSQL & " AND tp_cob_comp_tb.tp_componente_id = 1"  'Titular
'                Else
'                    sSQL = sSQL & " AND tp_cob_comp_tb.tp_componente_id = 3"  'Conjuge
'                End If
'            End If
'
'        Else
'            Set rs = Nothing
'            Exit Function
'        End If
'        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                      glAmbiente_id, _
'                                      App.Title, _
'                                      App.FileDescription, _
'                                      sSQL, _
'                                      lConexaoLocal, _
'                                      True)
'        If rs.EOF Then
'            Set rs = Nothing
'            sSQL = "SELECT tp_cobertura_id"
'            sSQL = sSQL & " FROM escolha_tp_cob_vida_aceito_tb  WITH (NOLOCK) , tp_cob_comp_tb  WITH (NOLOCK)  "
'            sSQL = sSQL & " WHERE escolha_tp_cob_vida_aceito_tb.Proposta_id = " & gbllProposta_ID
'            sSQL = sSQL & " AND (convert(smalldatetime, convert(varchar(10), escolha_tp_cob_vida_aceito_tb.dt_inicio_vigencia_esc, 111)) <= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' "
'            sSQL = sSQL & " AND convert(smalldatetime, convert(varchar(10), escolha_tp_cob_vida_aceito_tb.dt_fim_vigencia_esc, 111)) >= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' "
'            sSQL = sSQL & " OR  escolha_tp_cob_vida_aceito_tb.dt_fim_vigencia_esc IS NULL)"
'            sSQL = sSQL & " AND escolha_tp_cob_vida_aceito_tb.tp_cob_comp_id = tp_cob_comp_tb.tp_cob_comp_id"
'            sSQL = sSQL & " AND tp_cob_comp_tb.tp_cobertura_id = " & tp_Cobertura_id
'            If gblsTp_Componente_ID = "T" Then
'                sSQL = sSQL & " AND tp_cob_comp_tb.tp_componente_id = 1"  'Titular
'            Else
'                sSQL = sSQL & " AND tp_cob_comp_tb.tp_componente_id = 3"  'Conjuge
'            End If
'        Else
'            Set rs = Nothing
'            Exit Function
'        End If
'        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                      glAmbiente_id, _
'                                      App.Title, _
'                                      App.FileDescription, _
'                                      sSQL, _
'                                      lConexaoLocal, _
'                                      True)
'
'        If rs.EOF Then
'            Set rs = Nothing
'            sSQL = "SELECT * "
'            sSQL = sSQL & " FROM escolha_sub_grp_tp_cob_comp_tb a  WITH (NOLOCK) , tp_cob_comp_tb b  WITH (NOLOCK)  "
'            sSQL = sSQL & " WHERE a.tp_cob_comp_id = b.tp_cob_comp_id "
'            sSQL = sSQL & "   AND b.tp_cobertura_id = " & tp_Cobertura_id
'            sSQL = sSQL & "   AND a.Apolice_id = " & gbllApolice_ID
'            sSQL = sSQL & "   AND a.ramo_id = " & gbllRamo_ID
'            sSQL = sSQL & "   AND a.seguradora_cod_susep = " & gbllSeguradora_Cod_Susep
'            sSQL = sSQL & "   AND a.sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID
'
'            If gblsTp_Componente_ID = "T" Then
'                sSQL = sSQL & "   AND   b.tp_componente_id = 1"  'Titular
'            Else
'                sSQL = sSQL & "   AND   b.tp_componente_id = 3"  'Conjuge
'            End If
'        Else
'            Set rs = Nothing
'            Exit Function
'        End If
'    Else
'        sSQL = "SELECT a.val_is , b.nome "
'        sSQL = sSQL & "FROM escolha_tp_cob_emp_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK)  "
'        sSQL = sSQL & "WHERE a.proposta_id = " & gbllProposta_ID
'        sSQL = sSQL & "  AND a.cod_objeto_segurado = " & gblsCod_Objeto_Segurado
'        sSQL = sSQL & "  AND a.produto_id = " & gbllProduto_ID
'        sSQL = sSQL & "  AND a.ramo_id = " & gbllRamo_ID
'        sSQL = sSQL & "  AND b.tp_cobertura_id = " & tp_Cobertura_id
'        'If bytModoOperacao <> "2" Then 'cristovao.rodrigues 12/11/2012 linha substituida
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "  AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' "
'            sSQL = sSQL & "  AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' or a.dt_fim_vigencia_esc IS NULL ) "
'        End If
'        sSQL = sSQL & "  AND a.tp_cobertura_id = b.tp_cobertura_id "
'        sSQL = sSQL & "UNION "
'        sSQL = sSQL & "SELECT a.val_is , b.nome "
'        sSQL = sSQL & "FROM escolha_tp_cob_res_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK)  "
'        sSQL = sSQL & "WHERE a.proposta_id = " & gbllProposta_ID
'        sSQL = sSQL & "  AND a.cod_objeto_segurado = " & gblsCod_Objeto_Segurado
'        sSQL = sSQL & "  AND a.produto_id = " & gbllProduto_ID
'        sSQL = sSQL & "  AND a.ramo_id = " & gbllRamo_ID
'        sSQL = sSQL & "  AND b.tp_cobertura_id = " & tp_Cobertura_id
'        'If bytModoOperacao <> "2" Then 'cristovao.rodrigues 12/11/2012 linha substituida
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "  AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' "
'            sSQL = sSQL & "  AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' or a.dt_fim_vigencia_esc IS NULL ) "
'        End If
'        sSQL = sSQL & "  AND a.tp_cobertura_id = b.tp_cobertura_id "
'        sSQL = sSQL & "UNION "
'        sSQL = sSQL & "SELECT a.val_is , b.nome "
'        sSQL = sSQL & "FROM escolha_tp_cob_cond_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK)  "
'        sSQL = sSQL & "WHERE a.proposta_id = " & gbllProposta_ID
'        sSQL = sSQL & "  AND a.cod_objeto_segurado = " & gblsCod_Objeto_Segurado
'        sSQL = sSQL & "  AND a.produto_id = " & gbllProduto_ID
'        sSQL = sSQL & "  AND a.ramo_id = " & gbllRamo_ID
'        sSQL = sSQL & "  AND b.tp_cobertura_id = " & tp_Cobertura_id
'        'If bytModoOperacao <> "2" Then 'cristovao.rodrigues 12/11/2012 linha substituida
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "  AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' "
'            sSQL = sSQL & "  AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' or a.dt_fim_vigencia_esc IS NULL ) "
'        End If
'        sSQL = sSQL & "  AND a.tp_cobertura_id = b.tp_cobertura_id "
'        sSQL = sSQL & "UNION "
'        sSQL = sSQL & "SELECT a.val_is , b.nome "
'        sSQL = sSQL & "FROM escolha_tp_cob_maq_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK)  "
'        sSQL = sSQL & "WHERE a.proposta_id = " & gbllProposta_ID
'        sSQL = sSQL & "  AND a.cod_objeto_segurado = " & gblsCod_Objeto_Segurado
'        sSQL = sSQL & "  AND a.produto_id = " & gbllProduto_ID
'        sSQL = sSQL & "  AND a.ramo_id = " & gbllRamo_ID
'        sSQL = sSQL & "  AND b.tp_cobertura_id = " & tp_Cobertura_id
'       'If bytModoOperacao <> "2" Then 'cristovao.rodrigues 12/11/2012 linha substituida
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "  AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' "
'            sSQL = sSQL & "  AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' or a.dt_fim_vigencia_esc IS NULL ) "
'        End If
'        sSQL = sSQL & "  AND a.tp_cobertura_id = b.tp_cobertura_id "
'        sSQL = sSQL & "UNION "
'        sSQL = sSQL & "SELECT a.val_is , b.nome "
'        sSQL = sSQL & "FROM escolha_tp_cob_aceito_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK)  "
'        sSQL = sSQL & "WHERE a.proposta_id = " & gbllProposta_ID
'        sSQL = sSQL & "  AND a.cod_objeto_segurado = " & gblsCod_Objeto_Segurado
'        sSQL = sSQL & "  AND a.produto_id = " & gbllProduto_ID
'        sSQL = sSQL & "  AND a.ramo_id = " & gbllRamo_ID
'        sSQL = sSQL & "  AND b.tp_cobertura_id = " & tp_Cobertura_id
'        'If bytModoOperacao <> "2" Then 'cristovao.rodrigues 12/11/2012 linha substituida
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "  AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' "
'            sSQL = sSQL & "  AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' or a.dt_fim_vigencia_esc IS NULL ) "
'        End If
'        sSQL = sSQL & "  AND a.tp_cobertura_id = b.tp_cobertura_id "
'        sSQL = sSQL & "UNION "
'        sSQL = sSQL & "SELECT a.val_is , b.nome "
'        sSQL = sSQL & "FROM escolha_tp_cob_avulso_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK) "
'        sSQL = sSQL & "WHERE a.proposta_id = " & gbllProposta_ID
'        sSQL = sSQL & "  AND a.cod_objeto_segurado = " & gblsCod_Objeto_Segurado
'        sSQL = sSQL & "  AND a.produto_id = " & gbllProduto_ID
'        sSQL = sSQL & "  AND a.ramo_id = " & gbllRamo_ID
'        sSQL = sSQL & "  AND b.tp_cobertura_id = " & tp_Cobertura_id
'        'If bytModoOperacao <> "2" Then 'cristovao.rodrigues 12/11/2012 linha substituida
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "  AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' "
'            sSQL = sSQL & "  AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' or a.dt_fim_vigencia_esc IS NULL ) "
'        End If
'        sSQL = sSQL & "  AND a.tp_cobertura_id = b.tp_cobertura_id "
'        sSQL = sSQL & "UNION "
'        sSQL = sSQL & "SELECT a.val_is , b.nome "
'        sSQL = sSQL & "FROM escolha_tp_cob_generico_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK)  "
'        sSQL = sSQL & "WHERE a.proposta_id = " & gbllProposta_ID
'        sSQL = sSQL & "  AND a.cod_objeto_segurado = " & gblsCod_Objeto_Segurado
'        sSQL = sSQL & "  AND a.produto_id = " & gbllProduto_ID
'        sSQL = sSQL & "  AND a.ramo_id = " & gbllRamo_ID
'        sSQL = sSQL & "  AND b.tp_cobertura_id = " & tp_Cobertura_id
'        'If bytModoOperacao <> "2" Then 'cristovao.rodrigues 12/11/2012 linha substituida
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "  AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' "
'            sSQL = sSQL & "  AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "' or a.dt_fim_vigencia_esc IS NULL ) "
'        End If
'        sSQL = sSQL & "  AND a.tp_cobertura_id = b.tp_cobertura_id "
'    End If
'
'    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                  glAmbiente_id, _
'                                  App.Title, _
'                                  App.FileDescription, _
'                                  sSQL, _
'                                  lConexaoLocal, _
'                                  True)
'
'    If rs.EOF Then
'
'        If gbllRamo_ID <> "1" Then
'            bTranspInternacional = Verifica_Transporte(False)
'        End If
'
'        If bTranspInternacional = True Then
'            Set rs = Nothing
'            Exit Function
'        End If
'        Set rs = Nothing
'        sSQL = "SELECT nome "
'        sSQL = sSQL & "FROM tp_cobertura_tb  WITH (NOLOCK)  "
'        sSQL = sSQL & "WHERE tp_cobertura_id = " & tp_Cobertura_id
'
'        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                      glAmbiente_id, _
'                                      App.Title, _
'                                      App.FileDescription, _
'                                      sSQL, _
'                                      lConexaoLocal, _
'                                      True)
'        If Not rs.EOF Then
'            MsgBox "Cobertura " & rs("nome") & " n�o contratada !"
'        End If
'        Contratou_Cobertura = False
'    End If
'
'    Set rs = Nothing
'
'End Function

    
Private Function Contratou_Cobertura(strTp_Cobertura_ID As String, Optional datApolice_Dt_Emissao As Date) As Boolean

    Dim sSQL As String
    Dim rs As Recordset
    Dim rsCobertura As Recordset
    Dim bTranspInternacional As Boolean

    Contratou_Cobertura = True
    
    If Val(Format(datApolice_Dt_Emissao, "yyyymmdd")) < 20000701 Then
        Exit Function
    End If

'+----------------------------------------------------------------------
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'|
    
      sSQL = "SET NOCOUNT ON  EXEC SEGS13111_SPS " _
           & gbldSinistro_ID & ", " & gbllProposta_ID & ", " & gbllProduto_ID & ",'" & strTipoProcesso & "', '" _
           & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "', '" & gblsTp_Componente_ID & "', " & gbllApolice_ID & ", " _
           & gbllSucursal_Seguradora_ID & ", " & gbllSeguradora_Cod_Susep & ", " & gbllRamo_ID & ", " & bytTipoRamo
    
'    sSQL = ""
'    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Contratou_Cobertura'),0) >0" & vbNewLine
'    sSQL = sSQL & " if OBJECT_ID('tempdb..#Contratou_Cobertura') IS NOT NULL " & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Contratou_Cobertura" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & " " & vbNewLine
'    'FLAVIO.BEZERRA - INICIO - 26/04/2013
'    sSQL = sSQL & " Declare @Proposta_ID                        as Numeric(9)                       " & vbNewLine
'    sSQL = sSQL & " Declare @Cod_Objeto_Segurado                as Int                              " & vbNewLine
'    sSQL = sSQL & " Declare @TranspInternacional                as bit                              " & vbNewLine
'    sSQL = sSQL & " Declare @Qtd_Contratada                     as int                              " & vbNewLine
'    sSQL = sSQL & " Declare @Sinistro_ID                        as Numeric(11)                      " & vbNewLine
'    sSQL = sSQL & " Declare @bytTipoRamo                        as smallint                         " & vbNewLine
'    sSQL = sSQL & " Declare @Data                               as SmallDateTime                    " & vbNewLine
'    sSQL = sSQL & " Declare @TpComponenteID                     as TinyInt                          " & vbNewLine
'    sSQL = sSQL & " Declare @gblsTp_Componente_ID               as VarChar(1)                       " & vbNewLine
'    sSQL = sSQL & " Declare @Apolice_ID                         as Numeric(9)                       " & vbNewLine
'    sSQL = sSQL & " Declare @Sucursal_Seguradora_ID             as Numeric(5)                       " & vbNewLine
'    sSQL = sSQL & " Declare @Seguradora_Cod_Susep               as Numeric(5)                       " & vbNewLine
'    sSQL = sSQL & " Declare @Ramo_ID                            as Int                              " & vbNewLine
'    sSQL = sSQL & " Declare @Produto_ID                         as Int                              " & vbNewLine
''    sSQL = sSQL & "Select Tp_Cobertura_ID                                                                                                                                                       " & vbNewLine
''    sSQL = sSQL & "  Into #Contratou_Cobertura                                                                                                                                                  " & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Cobertura_tb      Sinistro_Cobertura_tb  WITH (NOLOCK)                                                                                          " & vbNewLine
''    sSQL = sSQL & " Where Sinistro_ID                               = " & gbldSinistro_ID & "                                                                                                   " & vbNewLine
''    sSQL = sSQL & "   and Dt_Fim_Vigencia                           is Null "
'    sSQL = sSQL & vbNewLine
'    sSQL = sSQL & "Select @Sinistro_ID                         = " & gbldSinistro_ID & vbNewLine
'    sSQL = sSQL & " " & vbNewLine
'    sSQL = sSQL & "Select Sinistro_Cobertura_tb.Tp_Cobertura_ID                                                 " & vbNewLine
'    sSQL = sSQL & "     , Tp_Cobertura_Tb.Nome                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Contratou                                 = 'n'                                       " & vbNewLine
'    sSQL = sSQL & "     , Qtd_Cobertura                             = 0                                         " & vbNewLine
'    sSQL = sSQL & "     , Qtd_Contratada                            = 0                                         " & vbNewLine
'    sSQL = sSQL & "     , Cod_Objeto_Segurado                       = Null                                      " & vbNewLine
'    sSQL = sSQL & "  Into #Contratou_Cobertura                                                                  " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Cobertura_tb      Sinistro_Cobertura_tb  WITH (NOLOCK)          " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Cobertura_Tb            Tp_Cobertura_Tb  WITH (NOLOCK)                " & vbNewLine
'    sSQL = sSQL & "    on Tp_Cobertura_Tb.Tp_Cobertura_ID           = Sinistro_Cobertura_tb.Tp_Cobertura_ID     " & vbNewLine
'    sSQL = sSQL & " Where Sinistro_ID                               = @Sinistro_ID                              " & vbNewLine
'    sSQL = sSQL & "   and Dt_Fim_Vigencia                           is Null                                     " & vbNewLine
'
'    'ATUALIZANDO QUANTIDADE DE COBERTURAS
'    sSQL = sSQL & "Update #Contratou_Cobertura                                                                  " & vbNewLine
'    sSQL = sSQL & "   Set Qtd_Cobertura = @@RowCount                                                            " & vbNewLine
'    sSQL = sSQL & "  From #Contratou_Cobertura                                                                  " & vbNewLine
'    'FLAVIO.BEZERRA - FIM - 26/04/2013
'
'    If bytTipoRamo = 1 Then
'        sSQL = sSQL & " " & vbNewLine
'        sSQL = sSQL & "Set NoCount on                                                                                                                                                       " & vbNewLine
'        'FLAVIO.BEZERRA - INICIO - 26/04/2013
'        'sSQL = sSQL & "Declare @Contador                            as int  'FLAVIO.BEZERRA - 25/04/2013                                                                                                                " & vbNewLine
'        'sSQL = sSQL & "Declare @Proposta_ID                         as Numeric(9)                                                                                                           " & vbNewLine
'        'sSQL = sSQL & "Declare @Cod_Objeto_Segurado                 as Int                                                                                                                  " & vbNewLine
'        '-- VARIAVEIS-----------------------------------------------------
'        sSQL = sSQL & "Select @Proposta_ID                         = " & gbllProposta_ID & vbNewLine
'        sSQL = sSQL & "Select @Data                                = '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "'" & vbNewLine
'        sSQL = sSQL & "Select @gblsTp_Componente_ID                = '" & gblsTp_Componente_ID & "'" & vbNewLine
'        sSQL = sSQL & "Select @Apolice_ID                          = " & gbllApolice_ID & vbNewLine
'        sSQL = sSQL & "Select @Sucursal_Seguradora_ID              = " & gbllSucursal_Seguradora_ID & vbNewLine
'        sSQL = sSQL & "Select @Seguradora_Cod_Susep                = " & gbllSeguradora_Cod_Susep & vbNewLine
'        sSQL = sSQL & "Select @Ramo_ID                             = " & gbllRamo_ID & vbNewLine
'        sSQL = sSQL & " " & vbNewLine
'        sSQL = sSQL & "If @gblsTp_Componente_ID = 'T'" & vbNewLine
'        sSQL = sSQL & "Begin" & vbNewLine
'        sSQL = sSQL & "     Select @TpComponenteID                      = 1" & vbNewLine
'        sSQL = sSQL & "End" & vbNewLine
'        sSQL = sSQL & "Else" & vbNewLine
'        sSQL = sSQL & "Begin" & vbNewLine
'        sSQL = sSQL & "     Select @TpComponenteID                      = 3" & vbNewLine
'        sSQL = sSQL & "End" & vbNewLine
'        '------------------------------------------------------------------
'        sSQL = sSQL & "                                                                                                                                                                    " & vbNewLine
'        'sSQL = sSQL & "Select @Contador                             = Count(1) 'FLAVIO.BEZERRA - 25/04/2013                                                                                                                 " & vbNewLine
'        sSQL = sSQL & "Update #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "   Set Contratou                             = 's'                                                                                                                   " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Escolha_Plano_Tb       Escolha_Plano_Tb  WITH (NOLOCK)                                                                                           " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Plano_Tb               Plano_Tb  WITH (NOLOCK)                                                                                                   " & vbNewLine
'        sSQL = sSQL & "    on Plano_Tb.Plano_ID                     = Escolha_Plano_Tb.Plano_ID                                                                                             " & vbNewLine
'        sSQL = sSQL & "   and Plano_Tb.Dt_Inicio_Vigencia           = Escolha_Plano_Tb.Dt_Inicio_Vigencia                                                                                   " & vbNewLine
'        sSQL = sSQL & "   and Plano_Tb.Produto_ID                   = Escolha_Plano_Tb.Produto_ID                                                                                           " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Plano_Tp_Comp_Tb    Tp_Plano_Tp_Comp_Tb  WITH (NOLOCK)                                                                                        " & vbNewLine
'        sSQL = sSQL & "    on Tp_Plano_Tp_Comp_Tb.Tp_Plano_ID       = Plano_Tb.Tp_Plano_ID                                                                                                  " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Cob_Comp_Plano_Tb   Tp_Cob_Comp_Plano_Tb  WITH (NOLOCK)                                                                                       " & vbNewLine
'        sSQL = sSQL & "    on Tp_Cob_Comp_Plano_tb.Tp_Plano_ID      = Plano_tb.Plano_ID                                                                                                     " & vbNewLine
'        'sSQL = sSQL & "   and Tp_Cob_Comp_Plano_tb.Tp_Cob_Comp_ID   = Tp_Cob_Comp_Tb.Tp_Cob_Comp_ID "
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Cob_Comp_Tb         Tp_Cob_Comp_Tb  WITH (NOLOCK)                                                                                             " & vbNewLine
'        sSQL = sSQL & "    on Tp_Cob_Comp_Tb.Tp_Cob_Comp_ID         = Tp_Cob_Comp_Plano_Tb.Tp_Cob_Comp_ID                                                                                   " & vbNewLine
'        sSQL = sSQL & "   and Tp_Cob_Comp_Tb.Tp_Componente_ID       = Tp_Plano_Tp_Comp_Tb.Tp_Componente_ID                                                                                  " & vbNewLine
''        If gblsTp_Componente_ID = "T" Then
''            sSQL = sSQL & "   AND Tp_Cob_Comp_Tb.tp_componente_id       = 1                                                                                                                 " & vbNewLine  'Titular
''        Else
''            sSQL = sSQL & "   AND Tp_Cob_Comp_Tb.tp_componente_id       = 3                                                                                                                 " & vbNewLine  'Conjuge
''        End If
'        sSQL = sSQL & "   AND Tp_Cob_Comp_Tb.tp_componente_id       = @TpComponenteID"
'        sSQL = sSQL & "  Join #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "    on #Contratou_Cobertura.Tp_Cobertura_ID      = Tp_Cob_Comp_Tb.Tp_Cobertura_ID                                                                                    " & vbNewLine
'        sSQL = sSQL & " Where Escolha_Plano_Tb.Proposta_ID = @Proposta_ID " & vbNewLine
'        sSQL = sSQL & "   and convert(smalldatetime, convert(varchar(10), escolha_plano_tb.dt_escolha, 111)) <= @Data " & vbNewLine
'        sSQL = sSQL & "   and (convert(smalldatetime, convert(varchar(10), escolha_plano_tb.dt_fim_vigencia, 111)) >= @Data OR " & vbNewLine
'        sSQL = sSQL & "        Escolha_Plano_tb.Dt_Fim_Vigencia Is Null)                                                                                                                    " & vbNewLine
'        sSQL = sSQL & ""
'        sSQL = sSQL & "Select @Qtd_Contratada = @@RowCount "
'        'sSQL = sSQL & "if @Contador = 0 " & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "if @Qtd_Contratada = 0 " & vbNewLine
'        sSQL = sSQL & "begin" & vbNewLine
'        If vProduto = 14 Then
'            sSQL = sSQL & "    Select @Proposta_ID                          = Proposta_ID" & vbNewLine
'            sSQL = sSQL & "      From Seguros_Db.Dbo.Apolice_Tb             Apolice_Tb  WITH (NOLOCK) " & vbNewLine
'            sSQL = sSQL & "     Where Apolice_ID                            = @Apolice_id" & vbNewLine
'            sSQL = sSQL & "       and Ramo_ID                               = @Ramo_id " & vbNewLine
'        Else
'            sSQL = sSQL & "    Select @Proposta_ID                          = @Proposta_ID " & vbNewLine
'        End If
'        'sSQL = sSQL & "    Select @Contador                                         = Count(1)" & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "    Update #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "       Set Contratou                             = 's'                                                                                                                   " & vbNewLine
'        sSQL = sSQL & "      From Seguros_Db.Dbo.Escolha_Tp_Cob_Vida_Tb             Escolha_Tp_Cob_Vida_Tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "      Join Seguros_Db.Dbo.Tp_Cob_Comp_Tb                     Tp_Cob_Comp_Tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "        on Tp_Cob_Comp_Tb.Tp_Cob_Comp_ID                     = Escolha_Tp_Cob_Vida_Tb.Tp_Cob_Comp_ID" & vbNewLine
'        sSQL = sSQL & "      Join #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "        on #Contratou_Cobertura.Tp_Cobertura_ID              = Tp_Cob_Comp_Tb.Tp_Cobertura_ID                                                                                    " & vbNewLine
'        sSQL = sSQL & "     Where Escolha_Tp_Cob_Vida_Tb.Proposta_ID                = @Proposta_ID" & vbNewLine
'        sSQL = sSQL & "       and (convert(smalldatetime, convert(varchar(10), escolha_tp_cob_vida_tb.dt_fim_vigencia_esc, 111)) >= @Data" & vbNewLine
'        sSQL = sSQL & "        Or escolha_tp_cob_vida_tb.dt_fim_vigencia_esc IS NULL)" & vbNewLine
'        sSQL = sSQL & "       and Escolha_tp_cob_vida_tb.tp_cob_comp_id = tp_cob_comp_tb.tp_cob_comp_id" & vbNewLine
'        If gblsTp_Componente <> "" Then
'            sSQL = sSQL & "   AND tp_cob_comp_tb.tp_componente_id = " & gblsTp_Componente & vbNewLine
'        Else
''            If gblsTp_Componente_ID = "T" Then
''                sSQL = sSQL & "       AND tp_cob_comp_tb.tp_componente_id = 1" & vbNewLine  'Titular
''            Else
''                sSQL = sSQL & "       AND tp_cob_comp_tb.tp_componente_id = 3" & vbNewLine  'Conjuge
''            End If
'            sSQL = sSQL & "   AND tp_cob_comp_tb.tp_componente_id = @TpComponenteID " & vbNewLine
'        End If
'        sSQL = sSQL & "    Select @Qtd_Contratada = @@RowCount" & vbNewLine
'        'sSQL = sSQL & "    if @Contador = 0 " & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "    if @Qtd_Contratada = 0 " & vbNewLine
'        sSQL = sSQL & "    Begin" & vbNewLine
'        'sSQL = sSQL & "    Select @Contador                                     = Count(1)" & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "       Update #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "          Set Contratou                             = 's'                                                                                                                   " & vbNewLine
'        sSQL = sSQL & "         From Seguros_Db.Dbo.Escolha_Tp_Cob_Vida_Aceito_Tb   Escolha_Tp_Cob_Vida_Aceito_Tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "         Join Seguros_Db.Dbo.Tp_Cob_Comp_Tb                  Tp_Cob_Comp_Tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "           on Tp_Cob_Comp_Tb.Tp_Cob_Comp_ID                  = Escolha_Tp_Cob_Vida_Aceito_Tb.Tp_Cob_Comp_ID" & vbNewLine
'        sSQL = sSQL & "         Join #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "           on #Contratou_Cobertura.Tp_Cobertura_ID      = Tp_Cob_Comp_Tb.Tp_Cobertura_ID                                                                                    " & vbNewLine
'        sSQL = sSQL & "        WHERE escolha_tp_cob_vida_aceito_tb.Proposta_id      = @Proposta_ID " & vbNewLine
'        sSQL = sSQL & "          AND (convert(smalldatetime, convert(varchar(10), escolha_tp_cob_vida_aceito_tb.dt_inicio_vigencia_esc, 111)) <= @Data " & vbNewLine
'        sSQL = sSQL & "          AND convert(smalldatetime, convert(varchar(10), escolha_tp_cob_vida_aceito_tb.dt_fim_vigencia_esc, 111)) >= @Data " & vbNewLine
'        sSQL = sSQL & "           OR escolha_tp_cob_vida_aceito_tb.dt_fim_vigencia_esc IS NULL)" & vbNewLine
''        If gblsTp_Componente_ID = "T" Then
''            sSQL = sSQL & "          AND tp_cob_comp_tb.tp_componente_id = 1" & vbNewLine  'Titular
''        Else
''            sSQL = sSQL & "          AND tp_cob_comp_tb.tp_componente_id = 3"  'Conjuge
''        End If
'        sSQL = sSQL & "          AND tp_cob_comp_tb.tp_componente_id = @TpComponenteID "  'Conjuge
'        sSQL = sSQL & "       Select @Qtd_Contratada = @@RowCount" & vbNewLine
'        'sSQL = sSQL & "        If @Contador = 0 " & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "       If @Qtd_Contratada = 0 " & vbNewLine
'        sSQL = sSQL & "       Begin" & vbNewLine
'        'sSQL = sSQL & "            Select @Contador                                              = Count(1)" & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "            Update #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "               Set Contratou                             = 's'                                                                                                                   " & vbNewLine
'        sSQL = sSQL & "              From Seguros_Db.Dbo.Escolha_Sub_Grp_Tp_Cob_Comp_Tb         Escolha_Sub_Grp_Tp_Cob_Comp_Tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "              Join Seguros_Db.Dbo.Tp_Cob_Comp_Tb                         Tp_Cob_Comp_Tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "                on Escolha_Sub_Grp_Tp_Cob_Comp_Tb.Tp_Cob_Comp_ID         = Tp_Cob_Comp_Tb.Tp_Cob_Comp_ID" & vbNewLine
'        sSQL = sSQL & "              Join #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "                on #Contratou_Cobertura.Tp_Cobertura_ID                  = Tp_Cob_Comp_Tb.Tp_Cobertura_ID                                                                                    " & vbNewLine
'        'FLAVIO.BEZERRA - INICIO - 25/04/2013
''        sSQL = sSQL & "             Where Escolha_Sub_Grp_Tp_Cob_Comp_Tb.Apolice_ID             = Tp_Cob_Comp_Tb.Apolice_ID" & vbNewLine
''        sSQL = sSQL & "               and Escolha_Sub_Grp_Tp_Cob_Comp_Tb.Ramo_ID                = Tp_Cob_Comp_Tb.Ramo_ID" & vbNewLine
''        sSQL = sSQL & "               and Escolha_Sub_Grp_Tp_Cob_Comp_Tb.Seguradora_Cod_Susep   = Tp_Cob_Comp_Tb.Seguradora_Cod_Susep" & vbNewLine
''        sSQL = sSQL & "               and Escolha_Sub_Grp_Tp_Cob_Comp_Tb.Sucursal_Seguradora_ID = Tp_Cob_Comp_Tb.Sucursal_Seguradora_ID" & vbNewLine
'        sSQL = sSQL & "             Where Escolha_Sub_Grp_Tp_Cob_Comp_Tb.Apolice_ID             = @Apolice_ID " & vbNewLine
'        sSQL = sSQL & "               and Escolha_Sub_Grp_Tp_Cob_Comp_Tb.Ramo_ID                = @Ramo_ID " & vbNewLine
'        sSQL = sSQL & "               and Escolha_Sub_Grp_Tp_Cob_Comp_Tb.Seguradora_Cod_Susep   = @Seguradora_Cod_Susep " & vbNewLine
'        sSQL = sSQL & "               and Escolha_Sub_Grp_Tp_Cob_Comp_Tb.Sucursal_Seguradora_ID = @Sucursal_Seguradora_ID " & vbNewLine
'        'FLAVIO.BEZERRA - FIM - 25/04/2013
'
''        If gblsTp_Componente_ID = "T" Then
''            sSQL = sSQL & "               and Tp_Cob_Comp_Tb.tp_componente_id = 1" & vbNewLine  'Titular
''        Else
''            sSQL = sSQL & "               and Tp_Cob_Comp_Tb.tp_componente_id = 3" & vbNewLine  'Conjuge
''        End If
'        sSQL = sSQL & "               and Tp_Cob_Comp_Tb.tp_componente_id = @TpComponenteID " & vbNewLine
'        sSQL = sSQL & "            Select @Qtd_Contratada = @@RowCount" & vbNewLine
'        sSQL = sSQL & "        End" & vbNewLine
'        sSQL = sSQL & "    End     " & vbNewLine
'        sSQL = sSQL & "End" & vbNewLine
'        'FLAVIO.BEZERRA - FIM - 26/04/2013
'    Else            'se tipo de ramo = 2
'
'        sSQL = sSQL & "Set NoCount on " & vbNewLine
'        'FLAVIO.BEZERRA - INICIO - 26/04/2013
'        'sSQL = sSQL & "Declare @Cod_Objeto_Segurado                     as Int  " & vbNewLine
'        sSQL = sSQL & "Select @Proposta_ID                         = " & gbllProposta_ID & vbNewLine
'        sSQL = sSQL & "Select @Produto_ID                          = " & gbllProduto_ID & vbNewLine
'        sSQL = sSQL & "Select @Ramo_ID                             = " & gbllRamo_ID & vbNewLine
'        sSQL = sSQL & "Select @Data                                = '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "'" & vbNewLine
'        sSQL = sSQL & "                                                                         " & vbNewLine
'        sSQL = sSQL & "Select @Cod_Objeto_Segurado                      = Cod_Objeto_Segurado   " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Re_Tb             Sinistro_Re_Tb  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & " Where Sinistro_ID                               = @Sinistro_ID " & vbNewLine
'        sSQL = sSQL & vbNewLine
'        'sSQL = sSQL & "SELECT a.val_is , b.nome  , @Cod_Objeto_Segurado" & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "Update #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "   Set Contratou                             = 's'                                                                                                                   " & vbNewLine
'        sSQL = sSQL & "  FROM escolha_tp_cob_generico_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & "  Join #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "    on #Contratou_Cobertura.Tp_Cobertura_ID                  = B.Tp_Cobertura_ID                                                                                    " & vbNewLine
'        sSQL = sSQL & " WHERE a.proposta_id = @Proposta_ID " & vbNewLine
'        sSQL = sSQL & "   AND a.cod_objeto_segurado = @Cod_Objeto_Segurado " & vbNewLine
'        sSQL = sSQL & "   AND a.produto_id = @Produto_ID" & vbNewLine
'        sSQL = sSQL & "   AND a.ramo_id = @Ramo_ID " & vbNewLine
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "  AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= @Data " & vbNewLine
'            sSQL = sSQL & "  AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= @Data or a.dt_fim_vigencia_esc IS NULL ) " & vbNewLine
'        End If
'        sSQL = sSQL & "   AND a.tp_cobertura_id = b.tp_cobertura_id " & vbNewLine
'        sSQL = sSQL & "   and a.dt_fim_vigencia_esc is null " & vbNewLine
'        sSQL = sSQL & "Select @Qtd_Contratada = @@RowCount" & vbNewLine
'        sSQL = sSQL & vbNewLine
'        sSQL = sSQL & "If @Qtd_Contratada = 0" & vbNewLine
'        sSQL = sSQL & "begin" & vbNewLine
'        'sSQL = sSQL & "   SELECT a.val_is , b.nome  , @Cod_Objeto_Segurado" & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "   Update #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "      Set Contratou                             = 's'                                                                                                                   " & vbNewLine
'        sSQL = sSQL & "     FROM escolha_tp_cob_res_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & "     Join #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "       on #Contratou_Cobertura.Tp_Cobertura_ID                  = B.Tp_Cobertura_ID                                                                                    " & vbNewLine
'        sSQL = sSQL & "    WHERE a.proposta_id = @Proposta_ID " & vbNewLine
'        sSQL = sSQL & "      AND a.cod_objeto_segurado = @Cod_Objeto_Segurado " & vbNewLine
'        sSQL = sSQL & "      AND a.produto_id = @Produto_ID " & vbNewLine
'        sSQL = sSQL & "      AND a.ramo_id = @Ramo_ID " & vbNewLine
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "      AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= @Data " & vbNewLine
'            sSQL = sSQL & "      AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= @Data or a.dt_fim_vigencia_esc IS NULL ) " & vbNewLine
'        End If
'        sSQL = sSQL & "      AND a.tp_cobertura_id = b.tp_cobertura_id " & vbNewLine
'        sSQL = sSQL & "      and a.dt_fim_vigencia_esc is null " & vbNewLine
'        sSQL = sSQL & "   Select @Qtd_Contratada = @@RowCount" & vbNewLine
'        sSQL = sSQL & vbNewLine
'        sSQL = sSQL & "    If @Qtd_Contratada = 0" & vbNewLine
'        sSQL = sSQL & "    begin" & vbNewLine
'        'sSQL = sSQL & "        SELECT a.val_is , b.nome  , @Cod_Objeto_Segurado" & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "        Update #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "           Set Contratou                             = 's'                                                                                                                   " & vbNewLine
'        sSQL = sSQL & "          FROM escolha_tp_cob_cond_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & "          Join #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "            on #Contratou_Cobertura.Tp_Cobertura_ID                  = B.Tp_Cobertura_ID                                                                                    " & vbNewLine
'        sSQL = sSQL & "         WHERE a.proposta_id = @Proposta_ID " & vbNewLine
'        sSQL = sSQL & "           AND a.cod_objeto_segurado = @Cod_Objeto_Segurado " & vbNewLine
'        sSQL = sSQL & "           AND a.produto_id = @Produto_ID " & vbNewLine
'        sSQL = sSQL & "           AND a.ramo_id = @Ramo_ID " & vbNewLine
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "           AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= @Data " & vbNewLine
'            sSQL = sSQL & "           AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= @Data or a.dt_fim_vigencia_esc IS NULL ) " & vbNewLine
'        End If
'        sSQL = sSQL & "           AND a.tp_cobertura_id = b.tp_cobertura_id " & vbNewLine
'        sSQL = sSQL & "           and a.dt_fim_vigencia_esc is null " & vbNewLine
'        sSQL = sSQL & "        Select @Qtd_Contratada = @@RowCount" & vbNewLine
'        sSQL = sSQL & vbNewLine
'        sSQL = sSQL & "        If @Qtd_Contratada = 0" & vbNewLine
'        sSQL = sSQL & "        begin" & vbNewLine
'        'sSQL = sSQL & "            SELECT a.val_is , b.nome  , @Cod_Objeto_Segurado" & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "            Update #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "               Set Contratou                             = 's'                                                                                                                   " & vbNewLine
'        sSQL = sSQL & "              FROM escolha_tp_cob_emp_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & "              Join #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "                on #Contratou_Cobertura.Tp_Cobertura_ID                  = B.Tp_Cobertura_ID                                                                                    " & vbNewLine
'        sSQL = sSQL & "             WHERE a.proposta_id = @Proposta_ID " & vbNewLine
'        sSQL = sSQL & "               AND a.cod_objeto_segurado = @Cod_Objeto_Segurado " & vbNewLine
'        sSQL = sSQL & "               AND a.produto_id = @Produto_ID " & vbNewLine
'        sSQL = sSQL & "               AND a.ramo_id = @Ramo_ID " & vbNewLine
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "               AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= @Data " & vbNewLine
'            sSQL = sSQL & "               AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= @Data or a.dt_fim_vigencia_esc IS NULL ) " & vbNewLine
'        End If
'        sSQL = sSQL & "               AND a.tp_cobertura_id = b.tp_cobertura_id " & vbNewLine
'        sSQL = sSQL & "               and a.dt_fim_vigencia_esc is null " & vbNewLine
'        sSQL = sSQL & "            Select @Qtd_Contratada = @@RowCount" & vbNewLine
'        sSQL = sSQL & vbNewLine
'        sSQL = sSQL & "            If @Qtd_Contratada = 0 " & vbNewLine
'        sSQL = sSQL & "            begin" & vbNewLine
'        'sSQL = sSQL & "                SELECT a.val_is , b.nome  , @Cod_Objeto_Segurado" & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "                Update #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "                   Set Contratou                             = 's'                                                                                                                   " & vbNewLine
'        sSQL = sSQL & "                  FROM escolha_tp_cob_maq_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & "                  Join #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "                    on #Contratou_Cobertura.Tp_Cobertura_ID                  = B.Tp_Cobertura_ID                                                                                    " & vbNewLine
'        sSQL = sSQL & "                 WHERE a.proposta_id = @Proposta_ID " & vbNewLine
'        sSQL = sSQL & "                   AND a.cod_objeto_segurado  = @Cod_Objeto_Segurado " & vbNewLine
'        sSQL = sSQL & "                   AND a.produto_id = @Produto_ID " & vbNewLine
'        sSQL = sSQL & "                   AND a.ramo_id = @Ramo_ID " & vbNewLine
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "                   AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= @Data " & vbNewLine
'            sSQL = sSQL & "                   AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= @Data or a.dt_fim_vigencia_esc IS NULL ) " & vbNewLine
'        End If
'        sSQL = sSQL & "                   AND a.tp_cobertura_id = b.tp_cobertura_id " & vbNewLine
'        sSQL = sSQL & "                   and a.dt_fim_vigencia_esc is null " & vbNewLine
'        sSQL = sSQL & "                Select @Qtd_Contratada = @@RowCount" & vbNewLine
'        sSQL = sSQL & vbNewLine
'        sSQL = sSQL & "                If @Qtd_Contratada = 0 " & vbNewLine
'        sSQL = sSQL & "                begin" & vbNewLine
'        'sSQL = sSQL & "                    SELECT a.val_is , b.nome  , @Cod_Objeto_Segurado" & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "                    Update #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "                       Set Contratou                             = 's'                                                                                                                   " & vbNewLine
'        sSQL = sSQL & "                      FROM escolha_tp_cob_aceito_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & "                      Join #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "                        on #Contratou_Cobertura.Tp_Cobertura_ID                  = B.Tp_Cobertura_ID                                                                                    " & vbNewLine
'        sSQL = sSQL & "                     WHERE a.proposta_id = @Proposta_ID " & vbNewLine
'        sSQL = sSQL & "                       AND a.cod_objeto_segurado = @Cod_Objeto_Segurado " & vbNewLine
'        sSQL = sSQL & "                       AND a.produto_id = @Produto_ID " & vbNewLine
'        sSQL = sSQL & "                       AND a.ramo_id = @Ramo_ID " & vbNewLine
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "                       AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= @Data " & vbNewLine
'            sSQL = sSQL & "                       AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= @Data or a.dt_fim_vigencia_esc IS NULL ) " & vbNewLine
'        End If
'        sSQL = sSQL & "                       AND a.tp_cobertura_id = b.tp_cobertura_id " & vbNewLine
'        sSQL = sSQL & "                       and a.dt_fim_vigencia_esc is null " & vbNewLine
'        sSQL = sSQL & "                    Select @Qtd_Contratada = @@RowCount" & vbNewLine
'        sSQL = sSQL & vbNewLine
'        sSQL = sSQL & "                    If @Qtd_Contratada = 0 " & vbNewLine
'        sSQL = sSQL & "                    Begin " & vbNewLine
'        'sSQL = sSQL & "                        SELECT a.val_is , b.nome  , @Cod_Objeto_Segurado" & vbNewLine 'FLAVIO.BEZERRA - 25/04/2013
'        sSQL = sSQL & "                        Update #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "                           Set Contratou                             = 's'                                                                                                                   " & vbNewLine
'        sSQL = sSQL & "                          FROM escolha_tp_cob_avulso_tb a  WITH (NOLOCK) , tp_cobertura_tb b  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "                          Join #Contratou_Cobertura                                                                                                                                          " & vbNewLine
'        sSQL = sSQL & "                            on #Contratou_Cobertura.Tp_Cobertura_ID                  = B.Tp_Cobertura_ID                                                                                    " & vbNewLine
'        sSQL = sSQL & "                         WHERE a.proposta_id = @Proposta_ID " & vbNewLine
'        sSQL = sSQL & "                           AND a.cod_objeto_segurado = @Cod_Objeto_Segurado " & vbNewLine
'        sSQL = sSQL & "                           AND a.produto_id = @Produto_ID " & vbNewLine
'        sSQL = sSQL & "                           AND a.ramo_id = @Ramo_ID " & vbNewLine
'        If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
'            sSQL = sSQL & "                          AND convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= @Data " & vbNewLine
'            sSQL = sSQL & "                          AND ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= @Data or a.dt_fim_vigencia_esc IS NULL ) " & vbNewLine
'        End If
'        sSQL = sSQL & "                           AND a.tp_cobertura_id = b.tp_cobertura_id " & vbNewLine
'        sSQL = sSQL & "                           and a.dt_fim_vigencia_esc is null " & vbNewLine
'        sSQL = sSQL & "                        Select @Qtd_Contratada = @@RowCount" & vbNewLine
'        sSQL = sSQL & "                    End" & vbNewLine
'        sSQL = sSQL & "                End" & vbNewLine
'        sSQL = sSQL & "            End" & vbNewLine
'        sSQL = sSQL & "        End" & vbNewLine
'        sSQL = sSQL & "    End" & vbNewLine
'        sSQL = sSQL & " End" & vbNewLine
'        '--VAI VERIFICAR TRANSPORTE, SOMENTE SE TIPO DE RAMO <> 1
'        '--VERIFICANDO TRANSPORTE
'        sSQL = sSQL & "" & vbNewLine
'        sSQL = sSQL & "Select @TranspInternacional = 0" & vbNewLine
'        sSQL = sSQL & "If @Ramo_ID = 22 " & vbNewLine
'        sSQL = sSQL & "begin" & vbNewLine
'        sSQL = sSQL & "     If(SELECT proposta_bb" & vbNewLine
'        sSQL = sSQL & "        FROM proposta_transporte_tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "        WHERE proposta_id = @Proposta_ID) > 0 " & vbNewLine
'        sSQL = sSQL & "     begin" & vbNewLine
'        sSQL = sSQL & "         Select @TranspInternacional = 1" & vbNewLine
'        sSQL = sSQL & "     End" & vbNewLine
'        sSQL = sSQL & "     Else" & vbNewLine
'        sSQL = sSQL & "     begin" & vbNewLine
'        sSQL = sSQL & "         If(SELECT Top 1 fatura_id" & vbNewLine
'        sSQL = sSQL & "              FROM fatura_tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "             WHERE apolice_id = @Apolice_ID" & vbNewLine
'        sSQL = sSQL & "               AND sucursal_seguradora_id = @Sucursal_Seguradora_ID" & vbNewLine
'        sSQL = sSQL & "               AND seguradora_cod_susep = @Seguradora_Cod_Susep" & vbNewLine
'        sSQL = sSQL & "               AND ramo_id = @Ramo_ID) > 0 " & vbNewLine
'        sSQL = sSQL & "         begin" & vbNewLine
'        sSQL = sSQL & "             Select @TranspInternacional = 1" & vbNewLine
'        sSQL = sSQL & "         End" & vbNewLine
'        sSQL = sSQL & "         Else" & vbNewLine
'        sSQL = sSQL & "         begin" & vbNewLine
'        sSQL = sSQL & "             if(SELECT TOP 1 num_cobranca" & vbNewLine
'        sSQL = sSQL & "                  FROM agendamento_cobranca_tb  WITH (NOLOCK) " & vbNewLine
'        sSQL = sSQL & "                 WHERE apolice_id = @Apolice_ID" & vbNewLine
'        sSQL = sSQL & "                   AND proposta_id = @Proposta_ID) = 0 " & vbNewLine
'        sSQL = sSQL & "             begin" & vbNewLine
'        sSQL = sSQL & "                 Select @TranspInternacional = 1" & vbNewLine
'        sSQL = sSQL & "             End" & vbNewLine
'        sSQL = sSQL & "         End" & vbNewLine
'        sSQL = sSQL & "     End" & vbNewLine
'        sSQL = sSQL & "End" & vbNewLine
'        sSQL = sSQL & "Else" & vbNewLine
'        sSQL = sSQL & "begin" & vbNewLine
'        sSQL = sSQL & "     Select @TranspInternacional = 0" & vbNewLine
'        sSQL = sSQL & "End" & vbNewLine
''        sSQL = sSQL & "Select @Cod_Objeto_Segurado" & vbNewLine
'    End If
'
'        '--REGISTRANDO REGISTRO DE CONTROLE
'        sSQL = sSQL & "" & vbNewLine
'        sSQL = sSQL & "Insert into #Contratou_Cobertura" & vbNewLine
'        sSQL = sSQL & "Select top 1 Tp_Cobertura_ID                     = 0" & vbNewLine
'        sSQL = sSQL & ", Nome                                      = ''" & vbNewLine
'        sSQL = sSQL & ", Contratou                                 = 'n'" & vbNewLine
'        sSQL = sSQL & ", Qtd_Cobertura" & vbNewLine
'        sSQL = sSQL & ", Qtd_Contratada" & vbNewLine
'        sSQL = sSQL & ", Cod_Objeto_Segurado                       = @Cod_Objeto_Segurado" & vbNewLine
'        sSQL = sSQL & "From #Contratou_Cobertura" & vbNewLine
'
'        '--ATUALIZANDO QUANTIDADE DE COBERTURAS CONTRATADAS
'        sSQL = sSQL & " " & vbNewLine
'        sSQL = sSQL & "Update #Contratou_Cobertura" & vbNewLine
'        sSQL = sSQL & "Set Qtd_Contratada = @Qtd_Contratada" & vbNewLine
'        sSQL = sSQL & "From #Contratou_Cobertura" & vbNewLine
'
'        '-- trecho abaixo ser� processado sempre, vida ou re/rural
'        sSQL = sSQL & " " & vbNewLine
'        sSQL = sSQL & "If @TranspInternacional = 1" & vbNewLine
'        sSQL = sSQL & "Begin -- (SE TRANSPORTE = true)" & vbNewLine
'        sSQL = sSQL & "Delete" & vbNewLine
'        sSQL = sSQL & "From #Contratou_Cobertura" & vbNewLine
'        sSQL = sSQL & "Where Tp_Cobertura_ID <> 0" & vbNewLine
'        sSQL = sSQL & "End" & vbNewLine
'
'        '--RETORNANDO OS DADOS PARA CRITICA DE COBERTURAS N�O CONTRATADAS
'        sSQL = sSQL & " " & vbNewLine
'        sSQL = sSQL & "Select *" & vbNewLine
'        sSQL = sSQL & "From #Contratou_Cobertura" & vbNewLine
'        sSQL = sSQL & "Where Contratou = 'n'" & vbNewLine
'        sSQL = sSQL & "Order by Tp_Cobertura_ID" & vbNewLine
        
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'+----------------------------------------------------------------------
        
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSQL, _
                                  lConexaoLocal, _
                                  True)
    
    'If rs.EOF Then
    If Not rs.EOF Then
    
        'FLAVIO.BEZERRA - INICIO - 26/04/2013
        If bytTipoRamo <> 1 Then
            gblsCod_Objeto_Segurado = rs.Fields!Cod_Objeto_Segurado
        End If
        'FLAVIO.BEZERRA - FIM - 26/04/2013
        
        If rs.Fields!Qtd_Cobertura <> rs.Fields!Qtd_Contratada Then
             rs.MoveNext
'            If gbllRamo_ID <> "1" Then
'               bTranspInternacional = Verifica_Transporte(False)
'            End If
'
'            If bTranspInternacional = True Then
'                Set rs = Nothing
'                Exit Function
'            End If
'            Set rs = Nothing
            
    '        sSQL = "SELECT nome "
    '        sSQL = sSQL & "FROM tp_cobertura_tb  WITH (NOLOCK)  "
    '        sSQL = sSQL & "WHERE tp_cobertura_id = " & strTp_Cobertura_ID
            
    '        sSQL = sSQL & "SELECT Tp_Cobertura_Tb.nome "
    '        sSQL = sSQL & "  FROM Seguros_Db.Dbo.tp_cobertura_tb            tp_cobertura_tb  WITH (NOLOCK)  "
    '        sSQL = sSQL & "  Join Seguros_Db.Dbo.Sinistro_Cobertura_Tb      Sinistro_Cobertura_Tb  WITH (NOLOCK) "
    '        sSQL = sSQL & "    on Sinistro_Cobertura_tb.Tp_cobertura_Tb     = Tp_cobertura_Tb.Tp_cobertura_Tb"
    '        sSQL = sSQL & " Where Sinistro_Cobertura_Tb.Sinistro_ID         = " & gbldSinistro_ID
    '        sSQL = sSQL & "   and Sinistro_Cobertura_Tb.Dt_Fim_Vigencia     is Null"
    
'            sSQL = "SELECT Tp_Cobertura_Tb.nome                                                          " & vbNewLine
'            sSQL = sSQL & "  FROM Seguros_Db.Dbo.tp_cobertura_tb            tp_cobertura_tb  WITH (NOLOCK)        " & vbNewLine
'            sSQL = sSQL & "  Join #Contratou_Cobertura                                       WITH (NOLOCK)        " & vbNewLine
'            sSQL = sSQL & "    on #Contratou_Cobertura.Tp_cobertura_id      = Tp_cobertura_Tb.Tp_cobertura_id   " & vbNewLine
'            sSQL = sSQL & " Where #Contratou_Cobertura.contratou            = 'n'                               " & vbNewLine
'
'            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                          glAmbiente_id, _
'                                          App.Title, _
'                                          App.FileDescription, _
'                                          sSQL, _
'                                          lConexaoLocal, _
'                                          True)
    '        If Not rs.EOF Then
    '            MsgBox "Cobertura " & rs("nome") & " n�o contratada !"
    '        End If
            Do While Not rs.EOF
                MsgBox "Cobertura " & rs.Fields!Nome & " n�o contratada !"
                rs.MoveNext
            Loop
            Contratou_Cobertura = False
        End If
'    Else
'        'AKIO.OKUNO - INICIO - 21/04/2013
'        If bytTipoRamo = 2 Then
'            gblsCod_Objeto_Segurado = rs.Fields!Cod_Objeto_Segurado
'        End If
'        'AKIO.OKUNO - FIM - 21/04/2013
    End If
    'FLAVIO.BEZERRA - FIM - 26/04/2013
    
    Set rs = Nothing

End Function


Public Function Verifica_Transporte(ByRef Avulso As Boolean) As Boolean
    
    Dim sSQL As String
    Dim rs As Recordset

    Verifica_Transporte = False
    
    If gbllRamo_ID <> 22 Then
        Verifica_Transporte = False
        Exit Function
    End If
    
    sSQL = "SELECT proposta_bb FROM proposta_transporte_tb  WITH (NOLOCK)  " & _
           "WHERE proposta_id = " & gbllProposta_ID
           
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSQL, _
                                  lConexaoLocal, _
                                  True) '� transporte vindo do banco.
    If Not rs.EOF Then
        Verifica_Transporte = True
        Avulso = True
        Set rs = Nothing
        Exit Function
    End If
    
    Set rs = Nothing
'AKIO.OKUNO - INICIO - 17/04/2013
'    sSQL = "SELECT fatura_id FROM fatura_tb  WITH (NOLOCK)  " & _
           " WHERE apolice_id = " & gbllApolice_ID & _
           " AND sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID & _
           " AND seguradora_cod_susep = " & gbllSeguradora_Cod_Susep & _
           " AND ramo_id = " & gbllRamo_ID
    sSQL = "SELECT Top 1 fatura_id FROM fatura_tb  WITH (NOLOCK)  " & _
           " WHERE apolice_id = " & gbllApolice_ID & _
           " AND sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID & _
           " AND seguradora_cod_susep = " & gbllSeguradora_Cod_Susep & _
           " AND ramo_id = " & gbllRamo_ID
'AKIO.OKUNO - FIM - 17/04/2013
           
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSQL, _
                                  lConexaoLocal, _
                                  True)
    If Not rs.EOF Then
        Verifica_Transporte = True
        Set rs = Nothing
    Else
        Set rs = Nothing
'AKIO.OKUNO - INICIO - 17/04/2013
'        sSQL = "SELECT num_cobranca FROM agendamento_cobranca_tb  WITH (NOLOCK)  " & _
               " WHERE apolice_id = " & gbllApolice_ID & _
               " AND proposta_id = " & gbllProposta_ID
        sSQL = "SELECT TOP 1 num_cobranca FROM agendamento_cobranca_tb  WITH (NOLOCK)  " & _
               " WHERE apolice_id = " & gbllApolice_ID & _
               " AND proposta_id = " & gbllProposta_ID
'AKIO.OKUNO - FIM - 17/04/2013
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                      glAmbiente_id, _
                                      App.Title, _
                                      App.FileDescription, _
                                      sSQL, _
                                      lConexaoLocal, _
                                      True)
        If rs.EOF Then
            Verifica_Transporte = True
        End If
        Set rs = Nothing
    End If
    
End Function

Private Function ValidaCosseguro(strApolice As String, strSucursal As String, intRamo As Integer) As Boolean
    
    Dim vsql As String
    Dim rs As Recordset

    ValidaCosseguro = False

    vsql = ""
    vsql = vsql & "SELECT tp_emissao FROM Apolice_tb  WITH (NOLOCK)  " & vbNewLine
    vsql = vsql & " WHERE apolice_id = " & strApolice & vbNewLine
    vsql = vsql & " AND sucursal_seguradora_id = " & strSucursal & vbNewLine
    vsql = vsql & " AND ramo_id = " & intRamo & vbNewLine
            
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vsql, _
                                 lConexaoLocal, _
                                 True)
    
    If Not rs.EOF Then
        If rs(0) = "A" Then
            ValidaCosseguro = True
            rs.Close
        Else
            rs.Close
            Exit Function
        End If
    Else
        rs.Close
        Exit Function
    End If
        
End Function
Function ValidaCorretor(gsCPF) As Boolean
'Isabeli Silva - Confitec SP - 2017-10-09
'MU-2017-036534 Cadastramento dos Corretores Exclusivos no SEGBR

    Dim sSQL As String
    Dim rsCorretor As Recordset
    Dim propostaCorretor As String
   
    If Trim(gsCPF) = "" Then
    
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 4 Then
            sSQL = ""
            sSQL = sSQL & " select isnull(cpf, '0') as cpf " & vbNewLine
            sSQL = sSQL & "   from segab_db.dbo.usuario_tb with (nolock) " & vbNewLine
            sSQL = sSQL & "  where login_rede = '" & cUserName & "' " & vbNewLine
            sSQL = sSQL & "    and situacao = 'a' " & vbNewLine
        Else
            sSQL = ""
            sSQL = sSQL & " select isnull(cpf, '0') as cpf " & vbNewLine
            sSQL = sSQL & "   from abss.segab_db.dbo.usuario_tb with (nolock) " & vbNewLine
            sSQL = sSQL & "  where login_rede = '" & cUserName & "' " & vbNewLine
            sSQL = sSQL & "    and situacao = 'a' " & vbNewLine
        End If
        
        Set rsCorretor = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, True)
        
        If Not rsCorretor.EOF Then
            gsCPF = rsCorretor(0)
        End If
        
        rsCorretor.Close
        Set rsCorretor = Nothing
    End If
    
    sSQL = ""
    sSQL = sSQL & " EXEC seguros_db.dbo.SEGS13508_SPS "
    sSQL = sSQL & "'" & gsCPF & "'"
    
    Set rsCorretor = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, True)
                
    If Not rsCorretor.EOF Then
    
        propostaCorretor = rsCorretor("CorretorCorretora")
        
        If propostaCorretor = "s" Then
            ValidaCorretor = True
            rsCorretor.Close
        Else
            ValidaCorretor = False
            rsCorretor.Close
        End If
    End If

    Set rsCorretor = Nothing

    Exit Function

End Function
Function ValidaCorretorProposta(ByVal iPropostaId As Long) As Boolean
'Isabeli Silva - Confitec SP - 2017-10-09
'MU-2017-036534 Cadastramento dos Corretores Exclusivos no SEGBR
    
    
    Dim sSQL As String
    Dim rsCorretorProposta As Recordset
    Dim propostaCorretora As String
    
    If Trim(gsCPF) = "" Then
    
        If glAmbiente_id = 2 Or glAmbiente_id = 3 Or glAmbiente_id = 4 Then
            sSQL = ""
            sSQL = sSQL & " select isnull(cpf, '0') as cpf " & vbNewLine
            sSQL = sSQL & "   from segab_db.dbo.usuario_tb with (nolock) " & vbNewLine
            sSQL = sSQL & "  where login_rede = '" & cUserName & "' " & vbNewLine
            sSQL = sSQL & "    and situacao = 'a' " & vbNewLine
        Else
            sSQL = ""
            sSQL = sSQL & " select isnull(cpf, '0') as cpf " & vbNewLine
            sSQL = sSQL & "   from abss.segab_db.dbo.usuario_tb with (nolock) " & vbNewLine
            sSQL = sSQL & "  where login_rede = '" & cUserName & "' " & vbNewLine
            sSQL = sSQL & "    and situacao = 'a' " & vbNewLine
        End If
        
        Set rsCorretorProposta = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, True)
        
        If Not rsCorretorProposta.EOF Then
            gsCPF = rsCorretorProposta(0)
        End If
        
        rsCorretorProposta.Close
        Set rsCorretorProposta = Nothing
    End If
    
    sSQL = ""
    sSQL = sSQL & " EXEC seguros_db.dbo.SEGS13508_SPS "
    sSQL = sSQL & "'" & gsCPF & "'"
    sSQL = sSQL & ", " & iPropostaId
    
    Set rsCorretorProposta = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, True)
    
    If Not rsCorretorProposta.EOF Then
        propostaCorretora = rsCorretorProposta("CorretorCorretora")
        
        If propostaCorretora = "s" Then
            ValidaCorretorProposta = True
            rsCorretorProposta.Close
        Else
            ValidaCorretorProposta = False
            rsCorretorProposta.Close
        End If
    End If
    
    Set rsCorretorProposta = Nothing
    

End Function


