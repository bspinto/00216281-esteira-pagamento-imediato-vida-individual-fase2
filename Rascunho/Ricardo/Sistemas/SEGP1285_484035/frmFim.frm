VERSION 5.00
Begin VB.Form frmFim 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEG50246- Gera��o de N�mero Ap�lice RE - "
   ClientHeight    =   2775
   ClientLeft      =   7680
   ClientTop       =   4305
   ClientWidth     =   4425
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2775
   ScaleWidth      =   4425
   Begin VB.CommandButton btnOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   1560
      TabIndex        =   1
      Top             =   2280
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      Height          =   1935
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4215
      Begin VB.Label lblSinistro 
         Alignment       =   2  'Center
         Caption         =   "19990000000"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   615
         Left            =   600
         TabIndex        =   4
         Top             =   1200
         Width           =   3015
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "SINISTRO N�MERO"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   3
         Top             =   840
         Width           =   3375
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "PROCESSO REALIZADO COM SUCESSO!"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   2
         Top             =   360
         Width           =   3735
      End
   End
End
Attribute VB_Name = "frmFim"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub btnOK_Click()
    Call SetWindowPos(frmFim.hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE) 'FLAVIO.BEZERRA - 05/02/2013
    Unload Me
End Sub

Private Sub Form_Load()
    
    CentraFrm frmFim

    If vOperVisual Then
        frmFim.Left = frmFim.Left + 5000
    End If
    
    lblSinistro = gbldSinistro_ID
    
    Call SetWindowPos(frmFim.hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE) 'FLAVIO.BEZERRA - 05/02/2013
    Me.Caption = App.EXEName & " - Gera��o de N�mero Ap�lice RE - " & Ambiente & " - " & sSegmentoCliente
    
End Sub
