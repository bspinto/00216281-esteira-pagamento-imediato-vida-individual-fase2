VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "Msflxgrd.ocx"
Begin VB.Form frmNovosEventos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "SEGB0246 - Novos Eventos"
   ClientHeight    =   5100
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8850
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5100
   ScaleWidth      =   8850
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraEventos 
      Caption         =   " Novos Eventos "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4425
      Left            =   90
      TabIndex        =   1
      Top             =   90
      Width           =   8610
      Begin MSFlexGridLib.MSFlexGrid fgridEventos 
         Height          =   4065
         Left            =   135
         TabIndex        =   2
         Top             =   225
         Width           =   8340
         _ExtentX        =   14711
         _ExtentY        =   7170
         _Version        =   393216
         Rows            =   1
         Cols            =   4
         HighLight       =   2
         ScrollBars      =   2
         SelectionMode   =   1
         FormatString    =   $"frmNovosEventos.frx":0000
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   7290
      TabIndex        =   0
      Top             =   4635
      Width           =   1452
   End
End
Attribute VB_Name = "frmNovosEventos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private lConexaoLocal                           As Integer

Private Sub cmdSair_Click()
        Unload Me
End Sub

Private Sub Form_Load()

        Me.Caption = "SEGP1285 - Novos Eventos"
        Call CentraFrm(Me)

        Me.Caption = Me.Caption & " - " & sSegmentoCliente
        
End Sub

Public Sub Carrega_grid_Eventos(cSinist As Currency)
        
    Dim SQL As String
    Dim rsRecordSet As ADODB.Recordset

    If EstadoConexao(lConexaoLocal) = adStateClosed Then
        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
    End If
    
    gblbExisteEvento = True     'AKIO.OKUNO - 24/09/2012
    
          SQL = "SELECT e.evento_BB_id, e.descricao, s.dt_evento"
    SQL = SQL & "  FROM sinistro_historico_tb s  WITH (NOLOCK)  "
    SQL = SQL & " INNER JOIN evento_SEGBR_tb e  WITH (NOLOCK)  "
    SQL = SQL & "    ON (e.evento_SEGBR_id = s.evento_SEGBR_id)"
    SQL = SQL & " WHERE s.Sinistro_id        = " & cSinist
    SQL = SQL & "   AND s.evento_visualizado = 'n'"
    SQL = SQL & "   AND e.evento_BB_id IS NOT NULL"
    SQL = SQL & " ORDER BY s.dt_inclusao, s.dt_evento, s.seq_evento"
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          SQL, _
                                          lConexaoLocal, _
                                          True)
                                    
    'MATHAYDE
    If rsRecordSet.EOF Then
        gblbExisteEvento = False    'AKIO.OKUNO - 24/09/2012
        Unload Me
    End If
    
    While Not rsRecordSet.EOF
        fgridEventos.AddItem "" & Chr(9) & rsRecordSet!evento_BB_id & Chr(9) & rsRecordSet!Descricao & Chr(9) & Format(rsRecordSet!dt_Evento, "DD/MM/YYYY")
        rsRecordSet.MoveNext
    Wend
    rsRecordSet.Close
        
End Sub
