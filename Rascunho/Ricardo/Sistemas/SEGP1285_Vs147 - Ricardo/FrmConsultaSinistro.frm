VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "COMCT232.OCX"
Begin VB.Form FrmConsultaSinistro 
   Caption         =   "SEGP0001 - Consulta Sinistro - VIDA"
   ClientHeight    =   12555
   ClientLeft      =   3465
   ClientTop       =   600
   ClientWidth     =   15195
   LinkMode        =   1  'Source
   LinkTopic       =   "FrmConsultaSinistro"
   ScaleHeight     =   12555
   ScaleWidth      =   15195
   Begin VB.CommandButton btnHistoricoCliente 
      Caption         =   "Hist�rico Cliente"
      Height          =   495
      Left            =   9990
      TabIndex        =   241
      Top             =   11610
      Width           =   1452
   End
   Begin VB.VScrollBar VScroll1 
      Height          =   12195
      Left            =   14880
      TabIndex        =   224
      Top             =   120
      Width           =   255
   End
   Begin VB.CommandButton btnSolicitarSaldo 
      Caption         =   "&Solicitar Saldo"
      Height          =   495
      Left            =   270
      TabIndex        =   199
      TabStop         =   0   'False
      Top             =   11385
      Width           =   1452
   End
   Begin VB.CommandButton BtnAplicar 
      Height          =   495
      Left            =   11520
      TabIndex        =   175
      Top             =   11610
      Visible         =   0   'False
      Width           =   1452
   End
   Begin VB.TextBox txtRetornoAuxiliar 
      Height          =   315
      Left            =   270
      TabIndex        =   174
      Top             =   11430
      Width           =   1215
   End
   Begin VB.CommandButton btnCancelar 
      Caption         =   "Sair da Avali��o"
      Height          =   495
      Left            =   13050
      TabIndex        =   52
      Top             =   11610
      Width           =   1452
   End
   Begin TabDlg.SSTab tabPrincipal 
      Height          =   11655
      Left            =   120
      TabIndex        =   37
      Top             =   675
      Width           =   14745
      _ExtentX        =   26009
      _ExtentY        =   20558
      _Version        =   393216
      Tabs            =   8
      Tab             =   1
      TabsPerRow      =   8
      TabHeight       =   520
      TabCaption(0)   =   "Dados gerais"
      TabPicture(0)   =   "FrmConsultaSinistro.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "fmeDadosGerais_Seguro"
      Tab(0).Control(1)=   "fmeDadosdoAviso"
      Tab(0).Control(2)=   "fmeDadosGerais_Cosseguro"
      Tab(0).Control(3)=   "fmeDadosGerais_Solicitante"
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Detalhamento"
      TabPicture(1)   =   "FrmConsultaSinistro.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "fmeDetalhamento_VisualizarImpressao"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Command1"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Hist�rico"
      TabPicture(2)   =   "FrmConsultaSinistro.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fmeHistorico_EvolucaoHistorico"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Estimativa/Pagto"
      TabPicture(3)   =   "FrmConsultaSinistro.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fmeEstimativasPagamentos_Valores_Voucher"
      Tab(3).Control(1)=   "fmeEstimativasPagamentos_Valores_Beneficiario"
      Tab(3).Control(2)=   "fmeEstimativasPagamentos_Valores"
      Tab(3).Control(3)=   "fmeEstimativasPagamentos_Dados"
      Tab(3).ControlCount=   4
      TabCaption(4)   =   "Dados espec�ficos"
      TabPicture(4)   =   "FrmConsultaSinistro.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "FrmDadosEspecificos_ParecerMedico"
      Tab(4).Control(1)=   "fmeDadosEspecificos_BensSinistrados"
      Tab(4).Control(2)=   "fmeDadosEspecificos_Reguladores"
      Tab(4).Control(3)=   "fmeDadosEspecificos_Prestadores"
      Tab(4).Control(4)=   "fmeDadosEspecificos_AcaoJudicial"
      Tab(4).ControlCount=   5
      TabCaption(5)   =   "Causas e Eventos"
      TabPicture(5)   =   "FrmConsultaSinistro.frx":008C
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "btnCausasEEventos_Operacao"
      Tab(5).Control(1)=   "txtObservacao1"
      Tab(5).Control(2)=   "txtObservacao2"
      Tab(5).Control(3)=   "fmeCausasEEventos_CausaSinistro"
      Tab(5).Control(4)=   "lblCausasEEventos_CausaSinistro(0)"
      Tab(5).Control(5)=   "lblCausasEEventos_CausaSinistro(1)"
      Tab(5).ControlCount=   6
      TabCaption(6)   =   "PLD"
      TabPicture(6)   =   "FrmConsultaSinistro.frx":00A8
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "fmePLD_Parcelas"
      Tab(6).ControlCount=   1
      TabCaption(7)   =   "Agravamentos"
      TabPicture(7)   =   "FrmConsultaSinistro.frx":00C4
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "btnAgravamentos_Avaliar"
      Tab(7).Control(1)=   "Frame1"
      Tab(7).Control(2)=   "GrdAgravo"
      Tab(7).ControlCount=   3
      Begin VB.CommandButton btnAgravamentos_Avaliar 
         Caption         =   "&Avaliar"
         Height          =   375
         Left            =   -62280
         TabIndex        =   239
         Top             =   6120
         Visible         =   0   'False
         Width           =   1452
      End
      Begin VB.Frame Frame1 
         Height          =   735
         Left            =   -74760
         TabIndex        =   230
         Top             =   480
         Width           =   13935
         Begin VB.Label Lbl_qtd_indeferidos 
            Caption         =   "0123"
            BeginProperty Font 
               Name            =   "Arial Black"
               Size            =   9.75
               Charset         =   0
               Weight          =   900
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   11280
            TabIndex        =   238
            Top             =   270
            Width           =   615
         End
         Begin VB.Label Lbl_qtd_deferidos 
            Caption         =   "0123"
            BeginProperty Font 
               Name            =   "Arial Black"
               Size            =   9.75
               Charset         =   0
               Weight          =   900
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   8040
            TabIndex        =   237
            Top             =   270
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Total de indeferidos:"
            BeginProperty Font 
               Name            =   "Arial Narrow"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   3
            Left            =   9120
            TabIndex        =   236
            Top             =   240
            Width           =   1935
         End
         Begin VB.Label Label1 
            Caption         =   "Total de deferidos:"
            BeginProperty Font 
               Name            =   "Arial Narrow"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   1
            Left            =   6000
            TabIndex        =   235
            Top             =   240
            Width           =   1815
         End
         Begin VB.Label Label1 
            Caption         =   "Sinistro:"
            BeginProperty Font 
               Name            =   "Arial Narrow"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   2
            Left            =   3240
            TabIndex        =   234
            Top             =   240
            Width           =   855
         End
         Begin VB.Label Lbl_Sinistro 
            Caption         =   "0123456789"
            BeginProperty Font 
               Name            =   "Arial Black"
               Size            =   9.75
               Charset         =   0
               Weight          =   900
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   4080
            TabIndex        =   233
            Top             =   270
            Width           =   1695
         End
         Begin VB.Label Lbl_Proposta 
            Caption         =   "0123456789"
            BeginProperty Font 
               Name            =   "Arial Black"
               Size            =   9.75
               Charset         =   0
               Weight          =   900
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1320
            TabIndex        =   232
            Top             =   270
            Width           =   1695
         End
         Begin VB.Label Label1 
            Caption         =   "Proposta:"
            BeginProperty Font 
               Name            =   "Arial Narrow"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   240
            TabIndex        =   231
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.CommandButton Command1 
         Caption         =   "An�li&se T�cnica"
         Height          =   495
         Left            =   1755
         TabIndex        =   223
         TabStop         =   0   'False
         Top             =   10710
         Visible         =   0   'False
         Width           =   1452
      End
      Begin VB.Frame FrmDadosEspecificos_ParecerMedico 
         Caption         =   "Parecer M�dico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1890
         Left            =   -74775
         TabIndex        =   214
         Top             =   6570
         Width           =   14295
         Begin VB.CommandButton btnDadosEspecificos_Parecer_Consultar 
            Caption         =   "&Consultar"
            Height          =   375
            Left            =   12645
            TabIndex        =   218
            Top             =   1350
            Width           =   1452
         End
         Begin VB.CommandButton btnDadosEspecificos_Parecer_Excluir 
            Caption         =   "&Excluir"
            Height          =   375
            Left            =   9405
            TabIndex        =   217
            Top             =   1350
            Width           =   1452
         End
         Begin VB.CommandButton btnDadosEspecificos_Parecer_Incluir 
            Caption         =   "&Novo"
            Height          =   375
            Left            =   11025
            TabIndex        =   216
            Top             =   1350
            Width           =   1452
         End
         Begin MSFlexGridLib.MSFlexGrid grdDadosEspecificos_ParecerMedico 
            Height          =   1005
            Left            =   120
            TabIndex        =   215
            Top             =   270
            Width           =   14055
            _ExtentX        =   24791
            _ExtentY        =   1773
            _Version        =   393216
            Rows            =   3
            Cols            =   4
            FixedCols       =   0
            BackColorSel    =   -2147483646
            FormatString    =   $"FrmConsultaSinistro.frx":00E0
         End
      End
      Begin VB.Frame fmeDadosGerais_Solicitante 
         Caption         =   "Solicitante"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Left            =   -74760
         TabIndex        =   81
         Top             =   6840
         Width           =   14295
         Begin VB.Frame fmeVidaTelFax 
            BorderStyle     =   0  'None
            Height          =   735
            Left            =   8790
            TabIndex        =   203
            Top             =   120
            Width           =   4725
            Begin VB.TextBox txtVidaSolicitante_Sinistro_Parentesco 
               Height          =   315
               Left            =   60
               TabIndex        =   213
               Top             =   360
               Width           =   1545
            End
            Begin VB.TextBox txtVidaSolicitante_Sinistro_Telefone1 
               Height          =   315
               Left            =   2190
               TabIndex        =   207
               Top             =   360
               Width           =   945
            End
            Begin VB.TextBox txtVidaSolicitante_Sinistro_DDD1 
               Height          =   315
               Left            =   1650
               TabIndex        =   206
               Top             =   360
               Width           =   495
            End
            Begin VB.TextBox txtVidaSolicitante_Sinistro_DDD_Fax 
               Height          =   315
               Left            =   3180
               TabIndex        =   205
               Top             =   360
               Width           =   495
            End
            Begin VB.TextBox txtVidaSolicitante_Sinistro_Telefone_Fax 
               Height          =   315
               Left            =   3720
               TabIndex        =   204
               Top             =   360
               Width           =   945
            End
            Begin VB.Label lblDadosGerais_Solicitante 
               Caption         =   "Parentesco:"
               Height          =   255
               Index           =   14
               Left            =   60
               TabIndex        =   212
               Top             =   120
               Width           =   945
            End
            Begin VB.Label lblDadosGerais_Solicitante 
               Caption         =   "Telefone:"
               Height          =   255
               Index           =   13
               Left            =   2190
               TabIndex        =   211
               Top             =   120
               Width           =   945
            End
            Begin VB.Label lblDadosGerais_Solicitante 
               Caption         =   "DDD:"
               Height          =   255
               Index           =   12
               Left            =   1650
               TabIndex        =   210
               Top             =   120
               Width           =   495
            End
            Begin VB.Label lblDadosGerais_Solicitante 
               Caption         =   "Fax:"
               Height          =   255
               Index           =   11
               Left            =   3720
               TabIndex        =   209
               Top             =   120
               Width           =   945
            End
            Begin VB.Label lblDadosGerais_Solicitante 
               Caption         =   "DDD:"
               Height          =   255
               Index           =   10
               Left            =   3180
               TabIndex        =   208
               Top             =   120
               Width           =   495
            End
         End
         Begin VB.CommandButton btnDadosGerais_Solicitante_Cadastrar 
            Caption         =   "No&vo"
            Height          =   375
            Left            =   12600
            TabIndex        =   189
            Top             =   960
            Visible         =   0   'False
            Width           =   1452
         End
         Begin VB.TextBox txtSolicitante_Sinistro_CEP 
            Height          =   315
            Left            =   5640
            MaxLength       =   30
            TabIndex        =   179
            Top             =   1080
            Width           =   975
         End
         Begin VB.TextBox txtSolicitante_Sinistro_EMail 
            Height          =   315
            Left            =   5640
            MaxLength       =   30
            TabIndex        =   21
            Top             =   480
            Width           =   3135
         End
         Begin VB.TextBox txtSolicitante_Sinistro_Telefone_Fax 
            Height          =   315
            Left            =   12000
            TabIndex        =   25
            Top             =   480
            Width           =   1335
         End
         Begin VB.TextBox txtSolicitante_Sinistro_DDD_Fax 
            Height          =   315
            Left            =   11280
            TabIndex        =   24
            Top             =   480
            Width           =   495
         End
         Begin VB.TextBox txtSolicitante_Sinistro_Telefone1 
            Height          =   315
            Left            =   9720
            TabIndex        =   23
            Top             =   480
            Width           =   1335
         End
         Begin VB.TextBox txtSolicitante_Sinistro_DDD1 
            Height          =   315
            Left            =   9000
            TabIndex        =   22
            Top             =   480
            Width           =   495
         End
         Begin VB.TextBox txtSolicitante_Municipio_Nome 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   10800
            TabIndex        =   30
            Text            =   "13123123123"
            Top             =   1080
            Width           =   1605
         End
         Begin VB.CommandButton btnSolicitante_Sinistro_Telefone_Consulta 
            Height          =   495
            Left            =   13560
            Picture         =   "FrmConsultaSinistro.frx":0172
            Style           =   1  'Graphical
            TabIndex        =   26
            Top             =   240
            Width           =   495
         End
         Begin VB.TextBox txtSolicitante_Sinistro_Estado 
            Height          =   315
            Left            =   10080
            MaxLength       =   2
            TabIndex        =   29
            Top             =   1080
            Width           =   495
         End
         Begin VB.TextBox txtSolicitante_Sinistro_Nome 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   240
            MaxLength       =   60
            TabIndex        =   20
            Text            =   "13123123123"
            Top             =   480
            Width           =   5175
         End
         Begin VB.TextBox txtSolicitante_Sinistro_Endereco 
            Height          =   315
            Left            =   240
            MaxLength       =   60
            TabIndex        =   27
            Text            =   "1412421421"
            Top             =   1080
            Width           =   5175
         End
         Begin VB.TextBox txtSolicitante_Sinistro_Bairro 
            Height          =   315
            Left            =   6840
            MaxLength       =   30
            TabIndex        =   28
            Text            =   "21421412412"
            Top             =   1080
            Width           =   3015
         End
         Begin VB.Label lbla 
            Caption         =   "CEP:"
            Height          =   255
            Index           =   5
            Left            =   5640
            TabIndex        =   180
            Top             =   840
            Width           =   975
         End
         Begin VB.Label lbla 
            Caption         =   "E-mail:"
            Height          =   255
            Index           =   4
            Left            =   5640
            TabIndex        =   178
            Top             =   240
            Width           =   975
         End
         Begin VB.Label lblDadosGerais_Solicitante 
            Caption         =   "DDD:"
            Height          =   255
            Index           =   3
            Left            =   11280
            TabIndex        =   90
            Top             =   240
            Width           =   615
         End
         Begin VB.Label lblDadosGerais_Solicitante 
            Alignment       =   1  'Right Justify
            Caption         =   "N�mero do fax:"
            Height          =   255
            Index           =   4
            Left            =   11880
            TabIndex        =   89
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label lblDadosGerais_Solicitante 
            Caption         =   "DDD:"
            Height          =   255
            Index           =   1
            Left            =   9000
            TabIndex        =   88
            Top             =   240
            Width           =   615
         End
         Begin VB.Label lblDadosGerais_Solicitante 
            Alignment       =   1  'Right Justify
            Caption         =   "N�mero do telefone:"
            Height          =   255
            Index           =   2
            Left            =   9600
            TabIndex        =   87
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label lblDadosGerais_Solicitante 
            Caption         =   "Estado:"
            Height          =   255
            Index           =   7
            Left            =   10080
            TabIndex        =   86
            Top             =   840
            Width           =   615
         End
         Begin VB.Label lblDadosGerais_Solicitante 
            Caption         =   "Nome:"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   85
            Top             =   240
            Width           =   615
         End
         Begin VB.Label lblDadosGerais_Solicitante 
            Caption         =   "Endere�o:"
            Height          =   255
            Index           =   5
            Left            =   240
            TabIndex        =   84
            Top             =   840
            Width           =   855
         End
         Begin VB.Label lblDadosGerais_Solicitante 
            Caption         =   "Bairro:"
            Height          =   255
            Index           =   6
            Left            =   6840
            TabIndex        =   83
            Top             =   840
            Width           =   975
         End
         Begin VB.Label lblDadosGerais_Solicitante 
            Caption         =   "Munic�pio:"
            Height          =   255
            Index           =   8
            Left            =   10800
            TabIndex        =   82
            Top             =   840
            Width           =   1095
         End
      End
      Begin VB.Frame fmeDadosGerais_Cosseguro 
         Caption         =   "Cosseguro"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Left            =   -74760
         TabIndex        =   119
         Top             =   6840
         Visible         =   0   'False
         Width           =   14295
         Begin VB.TextBox txtCosseguro_Ramo_Nome 
            Height          =   315
            Left            =   1200
            MaxLength       =   30
            TabIndex        =   35
            Text            =   "21421412412"
            Top             =   1080
            Width           =   4935
         End
         Begin VB.TextBox txtSucursal_Seguradora_Nome 
            Height          =   315
            Left            =   9360
            TabIndex        =   33
            Top             =   480
            Width           =   4695
         End
         Begin VB.TextBox txtCosseguro_Aceito_Sucursal_Seg_Lider 
            Height          =   315
            Left            =   8160
            TabIndex        =   32
            Top             =   480
            Width           =   1095
         End
         Begin VB.TextBox txtCosseguro_Aceito_Num_Apolice_Lider 
            Height          =   315
            Left            =   6360
            TabIndex        =   36
            Top             =   1080
            Width           =   1575
         End
         Begin VB.TextBox txtSeguradora_Nome 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   120
            MaxLength       =   60
            TabIndex        =   31
            Text            =   "13123123123"
            Top             =   480
            Width           =   7815
         End
         Begin VB.TextBox txtCosseguro_Aceito_Ramo_Lider 
            Height          =   315
            Left            =   120
            MaxLength       =   60
            TabIndex        =   34
            Text            =   "1412421421"
            Top             =   1080
            Width           =   1095
         End
         Begin VB.Label lblDadosGerais_Cosseguro 
            Caption         =   "Sucursal:"
            Height          =   255
            Index           =   1
            Left            =   8160
            TabIndex        =   123
            Top             =   240
            Width           =   975
         End
         Begin VB.Label lblDadosGerais_Cosseguro 
            Caption         =   "Ap�lice da l�der:"
            Height          =   255
            Index           =   3
            Left            =   6360
            TabIndex        =   122
            Top             =   840
            Width           =   1935
         End
         Begin VB.Label lblDadosGerais_Cosseguro 
            Caption         =   "Seguradora:"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   121
            Top             =   240
            Width           =   975
         End
         Begin VB.Label lblDadosGerais_Solicitante 
            Caption         =   "Ramo:"
            Height          =   255
            Index           =   9
            Left            =   120
            TabIndex        =   120
            Top             =   840
            Width           =   855
         End
      End
      Begin VB.Frame fmePLD_Parcelas 
         Caption         =   "Consulta Parcelas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   8055
         Left            =   -74760
         TabIndex        =   200
         Top             =   480
         Width           =   14265
         Begin MSFlexGridLib.MSFlexGrid grdPLD_Parcelas 
            Height          =   7575
            Left            =   240
            TabIndex        =   201
            Top             =   360
            Width           =   13815
            _ExtentX        =   24368
            _ExtentY        =   13361
            _Version        =   393216
            Rows            =   1
            Cols            =   9
            FixedCols       =   0
            WordWrap        =   -1  'True
            FormatString    =   $"FrmConsultaSinistro.frx":09C4
         End
      End
      Begin VB.CommandButton btnCausasEEventos_Operacao 
         Caption         =   "&Novo"
         Height          =   375
         Left            =   -61920
         TabIndex        =   187
         Top             =   8160
         Visible         =   0   'False
         Width           =   1452
      End
      Begin VB.TextBox txtObservacao1 
         Height          =   975
         Left            =   -74760
         MaxLength       =   300
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   184
         Top             =   3945
         Width           =   14205
      End
      Begin VB.TextBox txtObservacao2 
         Height          =   975
         Left            =   -74745
         MaxLength       =   300
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   183
         Top             =   5595
         Width           =   14205
      End
      Begin VB.Frame fmeCausasEEventos_CausaSinistro 
         Caption         =   "Causas sinistro"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2895
         Left            =   -74760
         TabIndex        =   181
         Top             =   480
         Width           =   14295
         Begin MSFlexGridLib.MSFlexGrid grdCausasEEventos_CausaSinistro 
            Height          =   2535
            Left            =   120
            TabIndex        =   182
            Top             =   240
            Width           =   14055
            _ExtentX        =   24791
            _ExtentY        =   4471
            _Version        =   393216
            Rows            =   3
            Cols            =   4
            FixedCols       =   0
            FormatString    =   $"FrmConsultaSinistro.frx":0A91
         End
      End
      Begin VB.Frame fmeDadosdoAviso 
         Height          =   3495
         Left            =   -74760
         TabIndex        =   133
         Top             =   3240
         Width           =   14295
         Begin VB.Frame fmeDadosGerais_Aviso 
            Caption         =   "Aviso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2175
            Left            =   120
            TabIndex        =   155
            Top             =   120
            Width           =   14055
            Begin VB.CheckBox chkClassificacaoSinistro 
               Enabled         =   0   'False
               Height          =   195
               Left            =   12105
               TabIndex        =   248
               Top             =   225
               Visible         =   0   'False
               Width           =   285
            End
            Begin VB.Frame gbxCIAg 
               Caption         =   "CIAg"
               Height          =   1335
               Left            =   12120
               TabIndex        =   243
               Top             =   720
               Visible         =   0   'False
               Width           =   1815
               Begin VB.Label lblCiagTipo 
                  Alignment       =   2  'Center
                  Caption         =   "DEFERIDO"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   245
                  Top             =   960
                  Width           =   1575
               End
               Begin VB.Label lblCiagValor 
                  Alignment       =   2  'Center
                  Caption         =   "0%"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   29.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   735
                  Left            =   120
                  TabIndex        =   244
                  Top             =   240
                  Width           =   1575
               End
            End
            Begin VB.CommandButton cmdSensoriamento 
               Caption         =   "&Sensoriamento"
               Height          =   375
               Left            =   10560
               TabIndex        =   242
               Top             =   840
               Width           =   1452
            End
            Begin VB.CommandButton cmdVistoria 
               Caption         =   "&Vistoria"
               Height          =   375
               Left            =   10560
               TabIndex        =   225
               Top             =   1200
               Width           =   1452
            End
            Begin VB.TextBox TxtNumSinistroLider 
               Height          =   315
               Left            =   10440
               TabIndex        =   161
               Top             =   480
               Width           =   1575
            End
            Begin VB.TextBox txtEndosso 
               Height          =   330
               Left            =   8415
               MaxLength       =   7
               TabIndex        =   159
               Text            =   "0"
               Top             =   480
               Width           =   705
            End
            Begin VB.CheckBox chkReintegracaoIS 
               Caption         =   "Reintegra��o de IS"
               Height          =   255
               Left            =   12120
               TabIndex        =   162
               Top             =   480
               Width           =   1770
            End
            Begin VB.TextBox txtAgencia_ID 
               BackColor       =   &H00FFFFFF&
               Height          =   315
               Left            =   9240
               TabIndex        =   160
               Top             =   480
               Width           =   1095
            End
            Begin VB.CommandButton btnDadosGerais_Aviso_Cadastrar 
               Caption         =   "&Novo"
               Height          =   375
               Left            =   10560
               TabIndex        =   166
               Top             =   1560
               Visible         =   0   'False
               Width           =   1452
            End
            Begin VB.TextBox txtDt_Aviso_Sinistro 
               Height          =   315
               Left            =   240
               TabIndex        =   165
               Top             =   1680
               Width           =   1695
            End
            Begin VB.TextBox txtDt_Entrada_Seguradora 
               Height          =   315
               Left            =   2040
               TabIndex        =   164
               Top             =   1080
               Width           =   1695
            End
            Begin VB.TextBox txtDt_Ocorrencia_Sinistro 
               Height          =   315
               Left            =   240
               TabIndex        =   163
               Top             =   1080
               Width           =   1695
            End
            Begin VB.TextBox txtSubEvento_Sinistro_Nome 
               BackColor       =   &H00FFFFFF&
               Height          =   315
               Left            =   4455
               TabIndex        =   158
               Text            =   "13123123123"
               Top             =   480
               Width           =   3855
            End
            Begin VB.TextBox txtEvento_Sinistro_Nome 
               BackColor       =   &H00FFFFFF&
               Height          =   315
               Left            =   240
               TabIndex        =   157
               Text            =   "13123123123"
               Top             =   480
               Width           =   4095
            End
            Begin VB.TextBox txtDt_Inclusao 
               Height          =   315
               Left            =   2040
               TabIndex        =   156
               Text            =   "123123"
               Top             =   1680
               Width           =   1695
            End
            Begin MSFlexGridLib.MSFlexGrid grdDadosGerais_Aviso 
               Height          =   1200
               Left            =   4440
               TabIndex        =   167
               Top             =   840
               Width           =   7560
               _ExtentX        =   13335
               _ExtentY        =   2117
               _Version        =   393216
               Rows            =   4
               Cols            =   4
               FixedCols       =   0
               ScrollBars      =   0
               FormatString    =   $"FrmConsultaSinistro.frx":0BAC
            End
            Begin VB.Label lblClassificacaoSinistro 
               AutoSize        =   -1  'True
               Height          =   195
               Left            =   12375
               TabIndex        =   249
               Top             =   225
               Visible         =   0   'False
               Width           =   1455
            End
            Begin VB.Label lblDadosGerais_Aviso 
               Caption         =   "N�mero Sinistro L�der:"
               Height          =   255
               Index           =   7
               Left            =   10440
               TabIndex        =   188
               Top             =   240
               Width           =   1695
            End
            Begin VB.Label lbl 
               Caption         =   "Endosso:"
               Height          =   195
               Index           =   90
               Left            =   8400
               TabIndex        =   177
               Top             =   240
               Width           =   780
            End
            Begin VB.Label lblDadosGerais_Aviso 
               Caption         =   "Ag�ncia Aviso:"
               Height          =   255
               Index           =   6
               Left            =   9240
               TabIndex        =   176
               Top             =   240
               Width           =   1215
            End
            Begin VB.Label lblDadosGerais_Aviso 
               Caption         =   "Evento do sinistro"
               Height          =   255
               Index           =   0
               Left            =   240
               TabIndex        =   173
               Top             =   240
               Width           =   1335
            End
            Begin VB.Label lblDadosGerais_Aviso 
               Caption         =   "Subevento do sinistro"
               Height          =   255
               Index           =   1
               Left            =   4455
               TabIndex        =   172
               Top             =   240
               Width           =   1695
            End
            Begin VB.Label lblDadosGerais_Aviso 
               Caption         =   "Data da ocorr�ncia"
               Height          =   255
               Index           =   2
               Left            =   240
               TabIndex        =   171
               Top             =   840
               Width           =   1815
            End
            Begin VB.Label lblDadosGerais_Aviso 
               Caption         =   "Data do aviso"
               Height          =   255
               Index           =   4
               Left            =   240
               TabIndex        =   170
               Top             =   1440
               Width           =   1215
            End
            Begin VB.Label lblDadosGerais_Aviso 
               Caption         =   "Data da entrada da seguradora"
               Height          =   255
               Index           =   3
               Left            =   2040
               TabIndex        =   169
               Top             =   840
               Width           =   2415
            End
            Begin VB.Label lblDadosGerais_Aviso 
               Caption         =   "Data da inclus�o"
               Height          =   255
               Index           =   5
               Left            =   2040
               TabIndex        =   168
               Top             =   1440
               Width           =   1575
            End
         End
         Begin VB.Frame fmeDadosGerais_EnderecoOcorrenciaSinistro 
            Caption         =   "Endere�o de ocorr�ncia do sinistro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   975
            Left            =   120
            TabIndex        =   146
            Top             =   2400
            Visible         =   0   'False
            Width           =   14085
            Begin VB.TextBox txtEndereco 
               Height          =   315
               Left            =   120
               MaxLength       =   60
               TabIndex        =   150
               Top             =   480
               Width           =   5535
            End
            Begin VB.TextBox txtBairro 
               Height          =   315
               Left            =   5880
               MaxLength       =   30
               TabIndex        =   149
               Top             =   480
               Width           =   3135
            End
            Begin VB.TextBox txtEstado 
               Height          =   315
               Left            =   9240
               MaxLength       =   30
               TabIndex        =   148
               Top             =   480
               Width           =   495
            End
            Begin VB.TextBox txtMunicipio 
               Height          =   315
               Left            =   9960
               MaxLength       =   30
               TabIndex        =   147
               Top             =   480
               Width           =   3855
            End
            Begin VB.Label lbla 
               Caption         =   "Estado:"
               Height          =   255
               Index           =   2
               Left            =   9240
               TabIndex        =   154
               Top             =   240
               Width           =   615
            End
            Begin VB.Label lbla 
               Caption         =   "Endere�o:"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   153
               Top             =   240
               Width           =   855
            End
            Begin VB.Label lbla 
               Caption         =   "Bairro:"
               Height          =   255
               Index           =   1
               Left            =   5880
               TabIndex        =   152
               Top             =   240
               Width           =   975
            End
            Begin VB.Label lbla 
               Caption         =   "Munic�pio:"
               Height          =   255
               Index           =   3
               Left            =   9960
               TabIndex        =   151
               Top             =   240
               Width           =   1095
            End
         End
         Begin VB.Frame fmeDadosGerais_Sinistrado 
            Caption         =   "Sinistrado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   975
            Left            =   120
            TabIndex        =   134
            Top             =   2400
            Width           =   14085
            Begin VB.TextBox txtSinistro_Vida_CPF 
               BackColor       =   &H00FFFFFF&
               Height          =   315
               Left            =   240
               TabIndex        =   140
               Text            =   "13123123123"
               Top             =   480
               Width           =   1455
            End
            Begin VB.CommandButton btnDadosGerais_Sinistrado_Cadastrar 
               Caption         =   "N&ovo"
               Height          =   375
               Left            =   12480
               TabIndex        =   139
               Top             =   480
               Visible         =   0   'False
               Width           =   1455
            End
            Begin VB.TextBox txtSinistro_Vida_Dt_Nasc 
               Height          =   315
               Left            =   9840
               TabIndex        =   138
               Top             =   480
               Width           =   1455
            End
            Begin VB.TextBox txtSinistro_Vida_Sexo 
               Height          =   315
               Left            =   11520
               TabIndex        =   137
               Top             =   480
               Width           =   2295
            End
            Begin VB.TextBox txtSinistrado_Cliente_ID 
               Height          =   315
               Left            =   8400
               MaxLength       =   60
               TabIndex        =   136
               Top             =   480
               Width           =   1185
            End
            Begin VB.TextBox txtSinistro_Vida_Nome 
               Height          =   315
               Left            =   1920
               MaxLength       =   60
               TabIndex        =   135
               Top             =   480
               Width           =   6255
            End
            Begin VB.Label lblDadosGerais_Sinistrado 
               Caption         =   "C�digo do Cliente :"
               Height          =   255
               Index           =   2
               Left            =   8400
               TabIndex        =   145
               Top             =   240
               Width           =   1425
            End
            Begin VB.Label lblDadosGerais_Sinistrado 
               Caption         =   "Data de Nascimento :"
               Height          =   255
               Index           =   3
               Left            =   9840
               TabIndex        =   144
               Top             =   255
               Width           =   1455
            End
            Begin VB.Label lblDadosGerais_Sinistrado 
               Caption         =   "Nome :"
               Height          =   255
               Index           =   1
               Left            =   1920
               TabIndex        =   143
               Top             =   270
               Width           =   2295
            End
            Begin VB.Label lblDadosGerais_Sinistrado 
               Caption         =   "Sexo :"
               Height          =   255
               Index           =   4
               Left            =   11520
               TabIndex        =   142
               Top             =   240
               Width           =   1095
            End
            Begin VB.Label lblDadosGerais_Sinistrado 
               AutoSize        =   -1  'True
               Caption         =   "CPF :"
               Height          =   195
               Index           =   0
               Left            =   240
               TabIndex        =   141
               Top             =   255
               Width           =   390
            End
         End
      End
      Begin VB.Frame fmeDadosEspecificos_BensSinistrados 
         Caption         =   "Bens Sinistrados"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2115
         Left            =   -74775
         TabIndex        =   130
         Top             =   8505
         Width           =   14295
         Begin MSFlexGridLib.MSFlexGrid grdDadosEspecificos_BensSinistrados 
            Height          =   1395
            Left            =   180
            TabIndex        =   132
            Top             =   270
            Width           =   14055
            _ExtentX        =   24791
            _ExtentY        =   2461
            _Version        =   393216
            Rows            =   3
            Cols            =   5
            FixedCols       =   0
            FormatString    =   $"FrmConsultaSinistro.frx":0C53
         End
         Begin VB.CommandButton btnDadosEspecificos_BensSinistrados_Operacao 
            Caption         =   "N&ovo"
            Height          =   375
            Left            =   12510
            TabIndex        =   131
            Top             =   1665
            Visible         =   0   'False
            Width           =   1452
         End
      End
      Begin VB.Frame fmeEstimativasPagamentos_Valores_Voucher 
         Caption         =   "Vouchers"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2175
         Left            =   -74760
         TabIndex        =   110
         Top             =   6360
         Width           =   14295
         Begin MSFlexGridLib.MSFlexGrid grdEstimativasPagamentos_Voucher 
            Height          =   1695
            Left            =   240
            TabIndex        =   190
            Top             =   360
            Width           =   13815
            _ExtentX        =   24368
            _ExtentY        =   2990
            _Version        =   393216
            Rows            =   5
            Cols            =   34
            FixedCols       =   0
            FormatString    =   $"FrmConsultaSinistro.frx":0D95
         End
      End
      Begin VB.Frame fmeEstimativasPagamentos_Valores_Beneficiario 
         Caption         =   "Benefici�rios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2175
         Left            =   -74760
         TabIndex        =   106
         Top             =   4080
         Width           =   14295
         Begin VB.CommandButton cmdPMBC 
            Caption         =   "PMBC"
            Enabled         =   0   'False
            Height          =   375
            Left            =   7560
            TabIndex        =   219
            Top             =   1560
            Visible         =   0   'False
            Width           =   1452
         End
         Begin VB.CommandButton btnEstimativasPagamentos_Valores_Beneficiario_Alterar 
            Caption         =   "&Alterar"
            Height          =   375
            Left            =   12600
            TabIndex        =   114
            Top             =   1560
            Visible         =   0   'False
            Width           =   1452
         End
         Begin VB.CommandButton btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider 
            Caption         =   "C&adastrar da Lider"
            Height          =   375
            Left            =   9240
            TabIndex        =   108
            Top             =   1560
            Visible         =   0   'False
            Width           =   1452
         End
         Begin VB.CommandButton btnEstimativasPagamentos_Valores_Beneficiario_CadastrarProposta 
            Caption         =   "C&adastrar da Proposta"
            Height          =   375
            Left            =   10920
            TabIndex        =   107
            Top             =   1560
            Width           =   1452
         End
         Begin MSFlexGridLib.MSFlexGrid grdEstimativasPagamentos_Beneficiario 
            Height          =   1695
            Left            =   240
            TabIndex        =   109
            Top             =   240
            Width           =   13815
            _ExtentX        =   24368
            _ExtentY        =   2990
            _Version        =   393216
            Rows            =   5
            Cols            =   18
            FixedCols       =   0
            ScrollTrack     =   -1  'True
            FormatString    =   $"FrmConsultaSinistro.frx":101B
         End
      End
      Begin VB.Frame fmeEstimativasPagamentos_Valores 
         Caption         =   "Valores"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2610
         Left            =   -74760
         TabIndex        =   103
         Top             =   1440
         Width           =   14265
         Begin VB.CommandButton btnEncerrar_Sinistro 
            Caption         =   "&Encerrar"
            Height          =   375
            Left            =   3780
            TabIndex        =   202
            Top             =   1925
            Width           =   1452
         End
         Begin VB.CommandButton btnDespSalvados 
            Caption         =   "..."
            Height          =   195
            Index           =   2
            Left            =   13920
            TabIndex        =   197
            Top             =   1990
            Width           =   255
         End
         Begin VB.CommandButton btnSalvados 
            Caption         =   "..."
            Height          =   195
            Index           =   3
            Left            =   13920
            TabIndex        =   196
            Top             =   1750
            Width           =   255
         End
         Begin VB.CommandButton btnDespressarcimento 
            Caption         =   "..."
            Height          =   195
            Index           =   4
            Left            =   13920
            TabIndex        =   195
            Top             =   1510
            Width           =   255
         End
         Begin VB.CommandButton btnressarcimento 
            Caption         =   "..."
            Height          =   195
            Index           =   5
            Left            =   13920
            TabIndex        =   194
            Top             =   1270
            Width           =   255
         End
         Begin VB.CommandButton btnDespesas 
            Caption         =   "..."
            Height          =   195
            Index           =   6
            Left            =   13920
            TabIndex        =   193
            Top             =   1030
            Width           =   255
         End
         Begin VB.CommandButton btnHonorarios 
            Caption         =   "..."
            Height          =   195
            Index           =   0
            Left            =   13920
            TabIndex        =   192
            Top             =   790
            Width           =   255
         End
         Begin VB.CommandButton btnIndenizacao 
            Caption         =   "..."
            Height          =   195
            Left            =   13920
            TabIndex        =   191
            Top             =   550
            Width           =   255
         End
         Begin VB.CommandButton btnEstimativasPagamentos_Valores_Cobertura 
            Caption         =   "&Novo"
            Height          =   375
            Left            =   5280
            TabIndex        =   45
            Top             =   1920
            Visible         =   0   'False
            Width           =   1452
         End
         Begin MSFlexGridLib.MSFlexGrid grdEstimativasPagamentos_Estimativas 
            Height          =   2265
            Left            =   6840
            TabIndex        =   46
            Top             =   240
            Width           =   7095
            _ExtentX        =   12515
            _ExtentY        =   3995
            _Version        =   393216
            Rows            =   9
            Cols            =   7
            ScrollBars      =   0
            GridLineWidth   =   2
            FormatString    =   "^Item                          |^Valor          |^Pago/Recebido |^Saldo          |^Resseguro                |^Sit.| $ "
         End
         Begin MSFlexGridLib.MSFlexGrid grdEstimativasPagamentos_Coberturas 
            Height          =   2265
            Left            =   240
            TabIndex        =   44
            Top             =   240
            Width           =   6495
            _ExtentX        =   11456
            _ExtentY        =   3995
            _Version        =   393216
            Rows            =   7
            Cols            =   3
            FixedCols       =   0
            AllowBigSelection=   0   'False
            FormatString    =   "Cobertura                                                                 | Valor estimado   | Corre��o monet."
         End
      End
      Begin VB.Frame fmeEstimativasPagamentos_Dados 
         Caption         =   "Dados"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1020
         Left            =   -74760
         TabIndex        =   92
         Top             =   360
         Width           =   14280
         Begin VB.TextBox txtValPrejuizoInformado 
            Height          =   315
            Left            =   11520
            MaxLength       =   18
            TabIndex        =   226
            Top             =   0
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.TextBox txtNum_Carta_Seguro_Aceito 
            Height          =   315
            Left            =   12960
            TabIndex        =   129
            Top             =   510
            Width           =   1095
         End
         Begin VB.TextBox txtDt_Inclusao_Estimativa 
            Height          =   315
            Left            =   7080
            TabIndex        =   128
            Top             =   510
            Width           =   1215
         End
         Begin VB.TextBox txtDt_Fim_Estimativa 
            Height          =   315
            Left            =   5640
            TabIndex        =   127
            Top             =   510
            Width           =   1215
         End
         Begin VB.TextBox txtDt_Inicio_Estimativa 
            Height          =   315
            Left            =   4200
            TabIndex        =   126
            Top             =   510
            Width           =   1215
         End
         Begin VB.TextBox txtPerc_Re_Seguro_Er 
            Enabled         =   0   'False
            Height          =   315
            Left            =   10560
            TabIndex        =   125
            Top             =   510
            Width           =   735
         End
         Begin VB.TextBox txtPerc_Re_Seguro_Quota 
            Enabled         =   0   'False
            Height          =   315
            Left            =   9840
            TabIndex        =   124
            Top             =   510
            Width           =   735
         End
         Begin ComCtl2.UpDown btnSeq_Estimativa 
            Height          =   300
            Left            =   3210
            TabIndex        =   41
            Top             =   525
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   529
            _Version        =   327681
            BuddyControl    =   "txtSeq_Estimativa"
            BuddyDispid     =   196727
            OrigLeft        =   3480
            OrigTop         =   480
            OrigRight       =   3735
            OrigBottom      =   855
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtCountEstimativa 
            Alignment       =   2  'Center
            Height          =   315
            Left            =   3435
            Locked          =   -1  'True
            TabIndex        =   104
            Text            =   "6"
            Top             =   510
            Width           =   560
         End
         Begin VB.TextBox txtUsuario 
            Height          =   315
            Left            =   8520
            TabIndex        =   42
            Text            =   "Usu�rio"
            Top             =   510
            Width           =   1095
         End
         Begin VB.TextBox txtNossa_Parte 
            Height          =   315
            Left            =   1560
            TabIndex        =   39
            Top             =   510
            Width           =   855
         End
         Begin VB.TextBox txtMoeda_Sigla 
            Height          =   315
            Left            =   240
            TabIndex        =   38
            Top             =   510
            Width           =   1095
         End
         Begin VB.TextBox txtSeq_Estimativa 
            Alignment       =   2  'Center
            Height          =   315
            Left            =   2670
            TabIndex        =   40
            Text            =   "6"
            Top             =   510
            Width           =   560
         End
         Begin VB.CheckBox chkPgto_Parcial_Co_Seguro 
            Caption         =   "Pgto. sobre nossa parte"
            Enabled         =   0   'False
            Height          =   495
            Left            =   11520
            TabIndex        =   43
            Top             =   380
            Width           =   1215
         End
         Begin VB.Label lblValPrejuizoInformado 
            Caption         =   "Valor Preju�zo Informado:"
            Height          =   255
            Left            =   9240
            TabIndex        =   228
            Top             =   0
            Visible         =   0   'False
            Width           =   2175
         End
         Begin VB.Label lblEstimativasPagamentos_Dados 
            Caption         =   "Usu�rio:"
            Height          =   255
            Index           =   6
            Left            =   8520
            TabIndex        =   102
            Top             =   300
            Width           =   855
         End
         Begin VB.Label lblEstimativasPagamentos_Dados 
            Caption         =   "% Nossa parte:"
            Height          =   255
            Index           =   1
            Left            =   1560
            TabIndex        =   101
            Top             =   300
            Width           =   1095
         End
         Begin VB.Label lblEstimativasPagamentos_Dados 
            Caption         =   "Moeda seguro:"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   100
            Top             =   300
            Width           =   1215
         End
         Begin VB.Label lblEstimativasPagamentos_Dados 
            Caption         =   "Data de inclus�o:"
            Height          =   255
            Index           =   5
            Left            =   7080
            TabIndex        =   99
            Top             =   300
            Width           =   1335
         End
         Begin VB.Label lblEstimativasPagamentos_Dados 
            Caption         =   "Seq. da estimativa:"
            Height          =   255
            Index           =   2
            Left            =   2670
            TabIndex        =   98
            Top             =   300
            Width           =   1455
         End
         Begin VB.Label lblEstimativasPagamentos_Dados 
            Caption         =   "Data fim:"
            Height          =   255
            Index           =   4
            Left            =   5640
            TabIndex        =   97
            Top             =   300
            Width           =   1335
         End
         Begin VB.Label lblEstimativasPagamentos_Dados 
            AutoSize        =   -1  'True
            Caption         =   "N�m. da carta:"
            Height          =   195
            Index           =   9
            Left            =   12960
            TabIndex        =   96
            Top             =   300
            Width           =   1050
         End
         Begin VB.Label lblEstimativasPagamentos_Dados 
            AutoSize        =   -1  'True
            Caption         =   "% ER:"
            Height          =   195
            Index           =   8
            Left            =   10560
            TabIndex        =   95
            Top             =   300
            Width           =   435
         End
         Begin VB.Label lblEstimativasPagamentos_Dados 
            Caption         =   "% Quota:"
            Height          =   255
            Index           =   7
            Left            =   9840
            TabIndex        =   94
            Top             =   300
            Width           =   735
         End
         Begin VB.Label lblEstimativasPagamentos_Dados 
            Caption         =   "Data de in�cio:"
            Height          =   255
            Index           =   3
            Left            =   4200
            TabIndex        =   93
            Top             =   300
            Width           =   1095
         End
      End
      Begin VB.Frame fmeDadosEspecificos_Reguladores 
         Caption         =   "Reguladores"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1935
         Left            =   -74760
         TabIndex        =   74
         Top             =   4560
         Width           =   14295
         Begin VB.CommandButton btnDadosEspecificos_Reguladores_Operacao 
            Caption         =   "&Vistoria"
            Height          =   375
            Index           =   1
            Left            =   10905
            TabIndex        =   117
            Top             =   1320
            Visible         =   0   'False
            Width           =   1452
         End
         Begin VB.CommandButton btnDadosEspecificos_Reguladores_Operacao 
            Caption         =   "N&ovo"
            Height          =   375
            Index           =   0
            Left            =   12585
            TabIndex        =   116
            Top             =   1320
            Visible         =   0   'False
            Width           =   1452
         End
         Begin MSFlexGridLib.MSFlexGrid grdDadosEspecificos_Regulador 
            Height          =   1575
            Left            =   120
            TabIndex        =   51
            Top             =   225
            Width           =   14055
            _ExtentX        =   24791
            _ExtentY        =   2778
            _Version        =   393216
            Rows            =   3
            Cols            =   13
            FixedCols       =   0
            FormatString    =   $"FrmConsultaSinistro.frx":11F6
         End
      End
      Begin VB.Frame fmeDadosEspecificos_Prestadores 
         Caption         =   "Prestadores"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1935
         Left            =   -74760
         TabIndex        =   73
         Top             =   2520
         Width           =   14295
         Begin VB.CommandButton btnDadosEspecificos_Prestadores_Operacao 
            Caption         =   "N&ovo"
            Height          =   375
            Left            =   12585
            TabIndex        =   50
            Top             =   1320
            Visible         =   0   'False
            Width           =   1452
         End
         Begin MSFlexGridLib.MSFlexGrid grdDadosEspecificos_Prestador 
            Height          =   1575
            Left            =   120
            TabIndex        =   49
            Top             =   240
            Width           =   14055
            _ExtentX        =   24791
            _ExtentY        =   2778
            _Version        =   393216
            Rows            =   3
            Cols            =   10
            FixedCols       =   0
            FormatString    =   $"FrmConsultaSinistro.frx":1398
         End
      End
      Begin VB.Frame fmeDadosEspecificos_AcaoJudicial 
         Caption         =   "A��o judicial"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1935
         Left            =   -74760
         TabIndex        =   72
         Top             =   480
         Width           =   14295
         Begin VB.CommandButton btnDadosEspecificos_AcaoJudicial_Operacao 
            Caption         =   "&Novo"
            Height          =   375
            Left            =   12585
            TabIndex        =   48
            Top             =   1320
            Visible         =   0   'False
            Width           =   1452
         End
         Begin MSFlexGridLib.MSFlexGrid grdDadosEspecificos_AcaoJudicial 
            Height          =   1575
            Left            =   120
            TabIndex        =   47
            Top             =   240
            Width           =   14055
            _ExtentX        =   24791
            _ExtentY        =   2778
            _Version        =   393216
            Rows            =   3
            Cols            =   5
            FixedCols       =   0
            FormatString    =   $"FrmConsultaSinistro.frx":14FD
         End
      End
      Begin VB.Frame fmeHistorico_EvolucaoHistorico 
         Caption         =   "Evolu��o do hist�rico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   8055
         Left            =   -74760
         TabIndex        =   66
         Top             =   480
         Width           =   14265
         Begin MSFlexGridLib.MSFlexGrid grdHistorico_EvolucaoHistorico 
            Height          =   7575
            Left            =   240
            TabIndex        =   67
            Top             =   360
            Width           =   13815
            _ExtentX        =   24368
            _ExtentY        =   13361
            _Version        =   393216
            Rows            =   43
            Cols            =   5
            FixedCols       =   0
            WordWrap        =   -1  'True
            AllowUserResizing=   1
            FormatString    =   $"FrmConsultaSinistro.frx":15FB
         End
      End
      Begin VB.Frame fmeDetalhamento_VisualizarImpressao 
         Caption         =   " Visualizar impress�o "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   9180
         Left            =   240
         TabIndex        =   65
         Top             =   480
         Width           =   14295
         Begin VB.CommandButton gestaoCartaEncerramento 
            Caption         =   "Gest�o de Carta de Encerramento"
            Height          =   465
            Left            =   7470
            TabIndex        =   250
            Top             =   8490
            Visible         =   0   'False
            Width           =   1452
         End
         Begin VB.CommandButton btnDetalhamento_EnviarSite 
            Caption         =   "&Enviar para o site"
            Height          =   465
            Left            =   10980
            TabIndex        =   221
            Top             =   8460
            Width           =   1452
         End
         Begin MSFlexGridLib.MSFlexGrid grdDetalhamento 
            Height          =   7815
            Left            =   135
            TabIndex        =   220
            ToolTipText     =   "Clique 2 vezes no texto para editar ou visualizar"
            Top             =   360
            Width           =   13950
            _ExtentX        =   24606
            _ExtentY        =   13785
            _Version        =   393216
            Cols            =   6
            FixedCols       =   0
            WordWrap        =   -1  'True
            HighLight       =   0
            SelectionMode   =   1
            AllowUserResizing=   3
            FormatString    =   $"FrmConsultaSinistro.frx":1705
         End
         Begin VB.CommandButton cmdExigencia 
            Caption         =   "Exig�ncias"
            Height          =   465
            Left            =   9360
            TabIndex        =   198
            Top             =   8460
            Width           =   1452
         End
         Begin VB.CommandButton btnDetalhamento_VisualizarImpressao_Cadastrar 
            Caption         =   "&Novo"
            Height          =   465
            Left            =   12600
            TabIndex        =   105
            Top             =   8460
            Visible         =   0   'False
            Width           =   1452
         End
         Begin RichTextLib.RichTextBox txtDetalhamento_Visualizar 
            Height          =   7905
            Left            =   120
            TabIndex        =   118
            Top             =   360
            Width           =   14010
            _ExtentX        =   24712
            _ExtentY        =   13944
            _Version        =   393217
            Enabled         =   -1  'True
            ReadOnly        =   -1  'True
            ScrollBars      =   2
            DisableNoScroll =   -1  'True
            TextRTF         =   $"FrmConsultaSinistro.frx":186C
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblTextoDetalhamento 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Label1"
            Enabled         =   0   'False
            Height          =   195
            Left            =   7515
            TabIndex        =   222
            Top             =   8595
            Visible         =   0   'False
            Width           =   1335
            WordWrap        =   -1  'True
         End
      End
      Begin VB.Frame fmeDadosGerais_Seguro 
         Caption         =   "Seguro"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2775
         Left            =   -74760
         TabIndex        =   53
         Top             =   360
         Width           =   14295
         Begin VB.TextBox txtPlano 
            Height          =   315
            Left            =   5280
            TabIndex        =   246
            Top             =   1080
            Width           =   3015
         End
         Begin VB.TextBox txtProduto_ID 
            Height          =   315
            Left            =   2040
            TabIndex        =   8
            Text            =   "1312312"
            Top             =   1080
            Width           =   615
         End
         Begin VB.TextBox txtRamo_ID 
            Height          =   315
            Left            =   2040
            TabIndex        =   1
            Text            =   "1312312"
            Top             =   480
            Width           =   615
         End
         Begin VB.TextBox txtCertificado_Dt_Emissao 
            Height          =   315
            Left            =   12360
            TabIndex        =   12
            Top             =   1080
            Width           =   1695
         End
         Begin VB.TextBox txtCertificado_Dt_Fim_Vigencia 
            Height          =   315
            Left            =   10440
            TabIndex        =   11
            Top             =   1080
            Width           =   1695
         End
         Begin VB.TextBox txtApolice_Dt_Emissao 
            Height          =   315
            Left            =   12360
            TabIndex        =   6
            Top             =   480
            Width           =   1695
         End
         Begin VB.TextBox txtApolice_Dt_Fim_Vigencia 
            Height          =   315
            Left            =   10440
            TabIndex        =   5
            Top             =   480
            Width           =   1695
         End
         Begin VB.TextBox txtCertificado_Dt_Inicio_Vigencia 
            Height          =   315
            Left            =   8520
            TabIndex        =   10
            Top             =   1080
            Width           =   1695
         End
         Begin VB.TextBox txtApolice_Dt_Inicio_Vigencia 
            Height          =   315
            Left            =   8520
            TabIndex        =   4
            Top             =   480
            Width           =   1695
         End
         Begin VB.TextBox txtRamo_Nome 
            Height          =   315
            Left            =   2760
            TabIndex        =   2
            Text            =   "123213"
            Top             =   480
            Width           =   2295
         End
         Begin VB.TextBox txtTp_Componente_Nome 
            Height          =   315
            Left            =   8520
            TabIndex        =   19
            Text            =   "123123123123"
            Top             =   2280
            Width           =   5475
         End
         Begin VB.TextBox txtCliente_Nome 
            Height          =   315
            Left            =   2040
            TabIndex        =   18
            Text            =   "1231231231"
            Top             =   2280
            Width           =   6255
         End
         Begin VB.TextBox txtCliente_ID 
            Height          =   315
            Left            =   240
            TabIndex        =   17
            Top             =   2280
            Width           =   1575
         End
         Begin VB.TextBox txtApolice_ID 
            Height          =   315
            Left            =   240
            TabIndex        =   0
            Text            =   "1312312"
            Top             =   480
            Width           =   1575
         End
         Begin VB.TextBox txtSeguradora_Nome_Lider 
            Height          =   315
            Left            =   5280
            TabIndex        =   3
            Text            =   "123213"
            Top             =   480
            Width           =   3015
         End
         Begin VB.TextBox txtQuantidade_Endossos 
            Height          =   315
            Left            =   240
            TabIndex        =   13
            Top             =   1680
            Width           =   1575
         End
         Begin VB.TextBox txtTp_Cobertura_Nome 
            Height          =   315
            Left            =   3840
            TabIndex        =   15
            Text            =   "12312"
            Top             =   1680
            Width           =   4455
         End
         Begin VB.TextBox txtProposta_ID 
            Height          =   315
            Left            =   240
            TabIndex        =   7
            Top             =   1080
            Width           =   1575
         End
         Begin VB.TextBox txtProduto_Nome 
            Height          =   315
            Left            =   2760
            TabIndex        =   9
            Text            =   "123123"
            Top             =   1080
            Width           =   2295
         End
         Begin VB.TextBox txtProposta_Situacao 
            Height          =   315
            Left            =   2040
            TabIndex        =   14
            Top             =   1680
            Width           =   1575
         End
         Begin VB.TextBox txtCorretor_Nome 
            Height          =   315
            Left            =   8520
            TabIndex        =   16
            Text            =   "123"
            Top             =   1680
            Width           =   5475
         End
         Begin VB.Label lblPlano 
            Caption         =   "Nome do Plano"
            Height          =   255
            Left            =   5280
            TabIndex        =   247
            Top             =   840
            Width           =   2655
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "Emiss�o do certificado"
            Height          =   255
            Index           =   10
            Left            =   12360
            TabIndex        =   80
            Top             =   840
            Width           =   1650
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "Fim vig.do certificado"
            Height          =   255
            Index           =   9
            Left            =   10440
            TabIndex        =   79
            Top             =   840
            Width           =   2055
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "In�cio vig.do certificado"
            Height          =   255
            Index           =   8
            Left            =   8520
            TabIndex        =   78
            Top             =   840
            Width           =   2055
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "Tipo"
            Height          =   255
            Index           =   17
            Left            =   8520
            TabIndex        =   77
            Top             =   2040
            Width           =   1815
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "Nome do segurado"
            Height          =   255
            Index           =   16
            Left            =   2040
            TabIndex        =   76
            Top             =   2040
            Width           =   3015
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "C�digo do segurado"
            Height          =   255
            Index           =   15
            Left            =   240
            TabIndex        =   75
            Top             =   2040
            Width           =   1455
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "Ramo"
            Height          =   255
            Index           =   1
            Left            =   2040
            TabIndex        =   71
            Top             =   240
            Width           =   735
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "N�mero da ap�lice"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   64
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "L�der"
            Height          =   255
            Index           =   2
            Left            =   5280
            TabIndex        =   63
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "In�cio vig. da apolice"
            Height          =   255
            Index           =   3
            Left            =   8520
            TabIndex        =   62
            Top             =   240
            Width           =   1815
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "Fim vig. da apolice"
            Height          =   255
            Index           =   4
            Left            =   10440
            TabIndex        =   61
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "Emiss�o da ap�lice"
            Height          =   255
            Index           =   5
            Left            =   12360
            TabIndex        =   60
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "N�mero da  proposta"
            Height          =   255
            Index           =   6
            Left            =   240
            TabIndex        =   59
            Top             =   840
            Width           =   1575
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "Produto"
            Height          =   255
            Index           =   7
            Left            =   2040
            TabIndex        =   58
            Top             =   840
            Width           =   1215
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "Cobertura principal"
            Height          =   255
            Index           =   13
            Left            =   3840
            TabIndex        =   57
            Top             =   1440
            Width           =   1575
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "Quantidade endossos"
            Height          =   255
            Index           =   11
            Left            =   240
            TabIndex        =   56
            Top             =   1440
            Width           =   1695
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "Situa��o da proposta"
            Height          =   255
            Index           =   12
            Left            =   2040
            TabIndex        =   55
            Top             =   1440
            Width           =   1695
         End
         Begin VB.Label lblDadosGerais_Seguro 
            Caption         =   "Corretor"
            Height          =   255
            Index           =   14
            Left            =   8520
            TabIndex        =   54
            Top             =   1440
            Width           =   2295
         End
      End
      Begin MSFlexGridLib.MSFlexGrid GrdAgravo 
         Height          =   4695
         Left            =   -74760
         TabIndex        =   240
         Top             =   1320
         Width           =   13935
         _ExtentX        =   24580
         _ExtentY        =   8281
         _Version        =   393216
      End
      Begin VB.Label lblCausasEEventos_CausaSinistro 
         Caption         =   "Observa��o 1:"
         Height          =   255
         Index           =   0
         Left            =   -74745
         TabIndex        =   186
         Top             =   3600
         Width           =   1815
      End
      Begin VB.Label lblCausasEEventos_CausaSinistro 
         Caption         =   "Observa��o 2:"
         Height          =   255
         Index           =   1
         Left            =   -74745
         TabIndex        =   185
         Top             =   5250
         Width           =   1815
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   150
      Left            =   0
      TabIndex        =   111
      Top             =   12405
      Width           =   15195
      _ExtentX        =   26802
      _ExtentY        =   265
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton btnAnaliseTecnica 
      Caption         =   "An�li&se T�cnica"
      Height          =   495
      Left            =   1755
      TabIndex        =   115
      TabStop         =   0   'False
      Top             =   11340
      Visible         =   0   'False
      Width           =   1452
   End
   Begin VB.Label lblProtocolo 
      AutoSize        =   -1  'True
      Caption         =   "123456789012345"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   11.25
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Index           =   0
      Left            =   3060
      TabIndex        =   227
      Tag             =   "D"
      Top             =   300
      Width           =   2250
   End
   Begin VB.Label lblProtocolo 
      AutoSize        =   -1  'True
      Caption         =   "Protocolo"
      Height          =   195
      Index           =   1
      Left            =   3060
      TabIndex        =   229
      Top             =   60
      Width           =   675
   End
   Begin VB.Label lblDescricaoLocalizacaoProcesso 
      Alignment       =   1  'Right Justify
      Caption         =   "Localiza��o do Processo:"
      Height          =   255
      Index           =   0
      Left            =   6480
      TabIndex        =   113
      Top             =   60
      Width           =   1935
   End
   Begin VB.Label lblDescricaoLocalizacaoProcesso 
      Caption         =   "Ag�ncia"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   11.25
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   1
      Left            =   6540
      TabIndex        =   112
      Tag             =   "D"
      Top             =   240
      Width           =   3210
   End
   Begin VB.Label lblSinistro_Situacao 
      Caption         =   "Situa��o:"
      Height          =   255
      Index           =   1
      Left            =   12525
      TabIndex        =   91
      Top             =   60
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblSinistro_Situacao 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "REABERTO (ADMINISTRATIVO)"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   11.25
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   10845
      TabIndex        =   70
      Tag             =   "D"
      Top             =   225
      Width           =   3930
   End
   Begin VB.Label lblSinistro_ID 
      AutoSize        =   -1  'True
      Caption         =   "93201200956"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   11.25
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Index           =   0
      Left            =   210
      TabIndex        =   69
      Tag             =   "D"
      Top             =   300
      Width           =   1650
   End
   Begin VB.Label lblSinistro_ID 
      Caption         =   "Sinistro AB:"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   68
      Top             =   60
      Width           =   855
   End
End
Attribute VB_Name = "FrmConsultaSinistro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Conexao
Private lConexaoLocal                                       As Integer

'Variaveis
Private bCarregou_Form                                      As Boolean
Private bCarregou_AbaDadosGerais                            As Boolean
Private bCarregou_AbaDetalhamento                           As Boolean
Private bCarregou_AbaHistorico                              As Boolean
Private bCarregou_AbaEstimativasPagamentos                  As Boolean
Private bCarregou_AbaDadosEspecificos                       As Boolean
Private bCarregou_AbaCausasEEventos                         As Boolean
Private bCarregou_AbaAgravamento                            As Boolean ' Wilder - Agravamento Pecu�rio - 06/09/2018
Private bCarregou_AbaPLD                                    As Boolean  'AKIO.OKUNO - 22/01/2013
Private bAtivou_Form                                        As Boolean  'Alan Neves - 16/04/2013


Private bAlterou_Seq_Estimativa                             As Boolean

'Vari�veis - Controle de Manuten��o de frames
Private bAlterou_fmeDadosGerais_Aviso                       As Boolean
Private bAlterou_fmeDadosGerais_Sinistrado                  As Boolean
Private bAlterou_fmeDadosGerais_EnderecoOcorrenciaSinistro  As Boolean
Private bAlterou_fmeDadosGerais_Solicitante                 As Boolean
Private bAlterou_fmeDetalhamento_VisualizarImpressao        As Boolean
Private bAlterou_fmeEstimativasPagamentos_Valores           As Boolean
Private bAlterou_fmeEstimativasPagamentos_Valores_Beneficiario As Boolean
Private bAlterou_fmeDadosEspecificos_AcaoJudicial           As Boolean
Private bAlterou_fmeDadosEspecificos_Prestadores            As Boolean
Private bAlterou_fmeDadosEspecificos_Reguladores            As Boolean
Private bAlterou_fmeDadosEspecificos_BensSinistrados        As Boolean

Private bytModoOperacao                                     As Byte         '1-Inclusao / 2-Alteracao / 3-Consulta
Private sSinistro_Situacao_ID                               As String

Private lSolicitante_ID                                     As Long
Private lSinistrado_ID                                      As Long

Private lAviso_Solicitante_ID                               As Long

'Sinistro de transporte internacional
Public bTranspInternacional                                 As Boolean

''cristovao.rodrigues 20/08/2012
Private bExecutou_grdDblClick                               As Boolean
Private iParametroParaChamadaAuxiliar                       As Byte
Private bTipoProcesso_Avisar_Liberado                       As Boolean    ' cristovao.rodrigues 23/08/2012
Private sAuxiliarExecutado                                  As String

Private sPassivel_Ressarcimento                             As String             'AKIO.OKUNO - 02/10/2012
Private bGerou_Aviso                                        As Boolean             'AKIO.OKUNO - 05/10/2012
Private bGerou_Estimativa                                   As Boolean             'AKIO.OKUNO - 05/10/2012
Private bRetornaSemGerarAviso                               As Boolean             'AKIO.OKUNO - 05/10/2012
Private sSite                                               As String '18/06/2019 - Schoralick (ntendencia) - string do site para ser acessado.
Private sCpf_Cnpj_Sem_Formatacao                            As String '21/06/2019 - Sergio (ntendencia) - string para passar parametro.
Private Const conSwNormal = 1

Private bVerifica_Protocolacao                              As Boolean  'AKIO.OKUNO - MP - 18/02/2013

'Victor - 02/10/2013 - 17905311 - IN�CIO
Public qtdeRuralEstudo As Integer
'Victor - 02/10/2013 - 17905311 - FIM

Public CiagMotivo                                            As String 'Bruno Pardim - CIAG 11/11/2019

'Collections
Private cDados_Estimativa As New Collection
Private bRetornoEncerramento As Boolean

Private Const strChecked = "�"                   'gustavo.cruz 21/12/2017
Private Const strUnChecked = "q"                 'gustavo.cruz 21/12/2017

Public PosAnterior                                          As Integer 'FBEZERRA/ FABREU - Inclus�o barra de rolagem SEGP1285 - 21/02/2018
''cristovao.rodrigues 31/03/2016 16429073 PMBC Amparo Familiar
'Private PMBC_Benef_id As String
'Private valorResgatePMBC As String
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, _
ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, _
ByVal nShowCmd As Long) As Long



Private Function usa_pgto_saldo_online(ByVal produto_Id As Integer) As Boolean
'+------------------------------
'| Hi@gro 06/2018 petrauskas
    Dim sSQL As String
    Dim RS As ADODB.Recordset
    
    sSQL = "select count(1) as qtde " _
         & "from seguros_db.dbo.parametros_saldo_devedor_online_tb with (nolock) " _
         & "where produto_id = " & gbllProduto_ID

    Set RS = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 lConexaoLocal, _
                                 True)
    usa_pgto_saldo_online = False
    If Not RS.EOF Then
        If RS(0).Value > 0 Then
            usa_pgto_saldo_online = True
        End If
    End If
    
    RS.Close: Set RS = Nothing
    

End Function

Private Function status_pgto_bb(ByVal sinistro_id As String) As Integer
'+------------------------------
'| Hi@gro 06/2018 petrauskas
    
    Dim sSQL As String, iResp As Integer
    Dim rsRecordSet As ADODB.Recordset
    
    sSQL = "exec seguros_db.dbo.SEGS13798_SPS @sinistro_id = " & sinistro_id
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    iResp = 0
    If Not rsRecordSet.EOF Then
        If Not IsNull(rsRecordSet!Situacao) Then
            iResp = rsRecordSet!Situacao
        End If
        rsRecordSet.Close
    End If
    Set rsRecordSet = Nothing

    status_pgto_bb = iResp

End Function

Private Sub InicializaModoOperacaoLocal()
    On Error GoTo Erro

    '1-Inclusao / 2-Altera��o / 3-Consulta
    InicializaModoOperacao Me, bytModoOperacao

    txtPerc_Re_Seguro_Quota.Enabled = False
    txtPerc_Re_Seguro_Er.Enabled = False

    Exit Sub

Erro:
    Call TrataErroGeral("InicializaModoOperacaoLocal", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub LimpaColecaoLocal(Optional cNomeColecao As Collection)
    Dim iItemColecao As Integer

    On Error GoTo Erro

    LimpaColecao cDados_Estimativa

    Exit Sub

Erro:
    Call TrataErroGeral("LimpaColecaoLocal", Me.Caption)
    Call FinalizarAplicacao

End Sub



Private Sub btnAnaliseTecnica_Click()
'REVER
'    Call Shell(App.PATH & "\SPFP0002.exe " & Parametro)
End Sub



Private Sub btnCancelar_Click()
    Dim sSQL As String
    Dim bProcessa As Boolean             'AKIO.OKUNO - 05/10/2012
    
    ' CONFITEC - INCIDENTE (IM00490782)- inicio
    Dim Controle_Erro As Integer
    
    On Error GoTo Erro

    ' Jos� Moreira (Nova Consultoria) - 26/12/2012 - 14620257 - Melhorias no processo de subscri��o
    ' Verifico a vari�vel se � para "fechar todo o programa"
    If (FrmConsultaSinistrocon.AbrirConsultaSinistro) Then
      Call FinalizarAplicacao
    End If
    

    'AKIO.OKUNO/MATHAYDE - INICIO - 01/10/2012
    Sleep (2000) 'Evitar erro no fechamento da aplica��o - 30.05.2018 - Redmine 2570
    
     Controle_Erro = 0
     
Trata_Fechamento:
   
    If PegaJanela(gblAuxiliarExecutando) And Controle_Erro = 0 Then
        On Error GoTo Trata_Fechamento
        Controle_Erro = 1
        
     ' CONFITEC - INCIDENTE (IM00490782)- FIM
        AppActivate gblAuxiliarExecutando
        Exit Sub
    End If
    'AKIO.OKUNO/MATHAYDE - FIM - 01/10/2012

    'AKIO.OKUNO - INICIO - 05/10/2012
    bProcessa = False
    If strTipoProcesso = strTipoProcesso_Avisar Or _
       strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
        If bGerou_Aviso Then
            If bGerou_Estimativa Then
                bProcessa = True
            End If
        End If
    Else
        bProcessa = True
    End If
    'AKIO.OKUNO - FIM - 05/10/2012

    If bProcessa Then   'AKIO.OKUNO - 05/10/2012
        If strTipoProcesso <> strTipoProcesso_Avisar And _
           strTipoProcesso <> strTipoProcesso_Consulta And _
           strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro And _
           strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then

            sSQL = ""
            sSQL = sSQL & "Exec Sinistro_Desmarca_SPU "
            sSQL = sSQL & "     @sinistro_id = " & gbldSinistro_ID
            sSQL = sSQL & "   , @apolice_id = " & gbllApolice_ID
            sSQL = sSQL & "   , @sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID
            sSQL = sSQL & "   , @seguradora_cod_susep = " & gbllSeguradora_Cod_Susep
            sSQL = sSQL & "   , @ramo_id = " & gbllRamo_ID
            
            'RCA 485 - ignora se status_sistema estiver bloqueado
            sSQL = sSQL & "   , @ignorabloqueio = 1"

            FrmConsultaSinistrocon.bUsuario_Utilizador = False
            
            Conexao_ExecutarSQL gsSIGLASISTEMA _
                              , glAmbiente_id _
                              , App.Title _
                              , App.FileDescription _
                              , sSQL _
                              , lConexaoLocal _
                              , False

        End If

        DestroiInterface

        'mathayde
        '    FrmConsultaSinistrocon.Enabled = True
        FrmConsultaSinistrocon.Visible = True          'AKIO.OKUNO - 24/09/2012 - Conforme solicitado no report n.79
        'AKIO.OKUNO - INICIO - 18/10/2012
        If strTipoProcesso_ORIGINAL = strTipoProcesso_Avaliar_Com_Imagem Then
            If btnEstimativasPagamentos_Valores_Cobertura.Caption = "&Encerrar" And btnEstimativasPagamentos_Valores_Cobertura.Enabled = True Then
                FrmConsultaSinistrocon.btnPagamento.Enabled = True
            End If
            strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem
        End If
        'AKIO.OKUNO - FIM - 18/10/2012

        Unload Me

        'AKIO.OKUNO - INICIO - 05/10/2012
    Else
        If Not bGerou_Aviso Then
            If btnDadosGerais_Solicitante_Cadastrar.Enabled Then
                MsgBox "� necess�rio que seja inclu�do um solicitante !", 64, "Mensagem ao Usu�rio"
            Else    'AKIO.OKUNO - 10/10/2012
                MsgBox "� necess�rio que o aviso seja inclu�do !", 64, "Mensagem ao Usu�rio"""
            End If
            If MsgBox("Deseja retornar para a tela de pesquisa?", 292, "Confirma��o do Usu�rio") = 6 Then
                DestroiInterface

                FrmConsultaSinistrocon.Visible = True

                gbldSinistro_ID = 0         'AKIO.OKUNO - 31/10/2012

                bRetornaSemGerarAviso = True

                Unload Me
            Else
                If btnDadosGerais_Solicitante_Cadastrar.Enabled Then    'AKIO.OKUNO - 10/10/2012
                    bRetornaSemGerarAviso = False
                    btnDadosGerais_Solicitante_Cadastrar.SetFocus
                    'AKIO.OKUNO - INICIO - 10/10/2012
                Else
                    tabPrincipal.Tab = 0
                    If btnDadosGerais_Aviso_Cadastrar.Enabled Then
                        btnDadosGerais_Aviso_Cadastrar.SetFocus
                    End If

                End If
                'AKIO.OKUNO - FIM - 10/10/2012
            End If
            'AKIO.OKUNO - INICIO - 10/10/2012
            '            Else
            '                MsgBox "� necess�rio que o aviso seja inclu�do !", 64, "Mensagem ao Usu�rio"
            '                tabPrincipal.Tab = 0
            '                If btnDadosGerais_Aviso_Cadastrar.Enabled Then
            '                    btnDadosGerais_Aviso_Cadastrar.SetFocus
            '                End If
            '
            '            End If
            'AKIO.OKUNO - FIM - 10/10/2012
        Else
            If Not bGerou_Estimativa Then
                'AKIO.OKUNO - INICIO - 10/10/2012
                MsgBox "� necess�rio que a estimativa seja inclu�da !", 64, "Mensagem ao Usu�rio"
                If MsgBox("Deseja retornar para a tela de pesquisa?", 292, "Confirma��o do Usu�rio") = 6 Then
                    DestroiInterface

                    FrmConsultaSinistrocon.Visible = True
                    
                    gblsSinistro_Situacao_ID = 0      'AKIO.OKUNO - 28/02/2013 POR FLAVIO.BEZERRA
                    
                    bRetornaSemGerarAviso = True

                    Unload Me
                Else
                    tabPrincipal.Tab = 3
                    If btnEstimativasPagamentos_Valores_Cobertura.Enabled Then
                        btnEstimativasPagamentos_Valores_Cobertura.SetFocus
                    End If
                End If
                '                tabPrincipal.Tab = 3
                '                If btnEstimativasPagamentos_Valores_Cobertura.Enabled Then
                '                    btnEstimativasPagamentos_Valores_Cobertura.SetFocus
                '                End If
                'AKIO.OKUNO - FIM - 10/10/2012
            End If
        End If
    End If
    'AKIO.OKUNO - FIM - 05/10/2012
'    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then   'AKIO.OKUNO - 30/04/2013
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem _
       And bAlterou_fmeDetalhamento_VisualizarImpressao Then    'AKIO.OKUNO - 30/04/2013
        Screen.MousePointer = vbHourglass                       'AKIO.OKUNO - 30/04/2013
        SelecionaDados_Detalhamento lConexaoLocal                                             'FLAVIO.BEZERRA - 30/01/2013
        FrmConsultaSinistrocon.txtDetalhamento_Visualizar = CarregaDados_DetalhamentoSinistro 'FLAVIO.BEZERRA - 30/01/2013
        Screen.MousePointer = vbNormal                          'AKIO.OKUNO - 30/04/2013
        bAlterou_fmeDetalhamento_VisualizarImpressao = False 'AKIO.OKUNO - 30/04/2013
    End If
    Exit Sub
    Resume
Erro:
    Call TrataErroGeral("BtnCancelar_Click", Me.Caption)
    Call FinalizarAplicacao
Resume
End Sub

Private Sub btnDadosEspecificos_AcaoJudicial_Operacao_Click()

    CarregaAuxiliarAcaoJudicial (1)

    ''    Dim sChaveComposta                          As String
    ''    Dim bProcessa                               As Boolean
    ''    bProcessa = False
    ''    bAlterou_fmeDadosEspecificos_AcaoJudicial = True
    ''
    ''    iParametroParaChamadaAuxiliar = VerificaSelecao()
    '''    If strTipoProcesso = strTipoProcesso_Consulta Or _
     '''       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
    '''        iParametroParaChamadaAuxiliar = 3
    '''    Else
    '''        'SE FOR CHAMADO PELO BOT�O
    '''        iParametroParaChamadaAuxiliar = 1
    '''        If bExecutou_grdDblClick Then
    '''            iParametroParaChamadaAuxiliar = 2
    '''        End If
    '''    End If
    ''
    ''
    ''    With grdDadosEspecificos_AcaoJudicial
    ''        If .Rows > 1 Then
    ''            sChaveComposta = .TextMatrix(.Row, 0)
    ''            bProcessa = True
    ''        Else
    ''            If iParametroParaChamadaAuxiliar = 1 Then
    ''                bProcessa = True
    ''            Else
    ''                bProcessa = False
    ''            End If
    ''        End If
    ''    End With
    ''
    ''    If bProcessa Then
    ''        'If btnDadosEspecificos_AcaoJudicial_Operacao.Tag >= 2 Then
    ''        If iParametroParaChamadaAuxiliar >= 2 Then
    ''            If LenB(Trim(sChaveComposta)) <> 0 Then
    ''                bProcessa = True
    ''            Else
    ''                MsgBox "N�o existe a��o judicial selecionada para consulta !", 16, "Mensagem ao Usu�rio"
    ''                bProcessa = False
    ''            End If
    ''        Else
    ''            bProcessa = True
    ''        End If
    ''
    ''        'aguardando desenvolvimento aplicacao
    '''        If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
    ''        If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
    ''            'ExecutaAplicacao "SEGPxxxx", btnDadosEspecificos_AcaoJudicial_Operacao.Tag, lblSinistro_ID(0), sChaveComposta
    ''            ExecutaAplicacao "SEGP1299", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
    ''            'cristovao.rodrigues 24/08/2012
    ''            If iParametroParaChamadaAuxiliar <> 3 Then
    ''                bCarregou_AbaDadosEspecificos = False
    ''                sAuxiliarExecutado = "AcaoJudicial"
    ''                'VerificaRetornoAplicacao
    ''            End If
    ''        End If
    ''    End If
    ''
    ''    Exit Sub
    ''
    ''Erro:
    ''    Call TrataErroGeral("btnDadosEspecificos_AcaoJudicial_Operacao_Click", Me.Caption)
    ''    bAlterou_fmeDadosEspecificos_AcaoJudicial = False

End Sub

'mathayde
Sub CarregaAuxiliarAcaoJudicial(iTpOperacao As Integer)

    Dim sChaveComposta As String
    Dim bProcessa As Boolean
    bProcessa = False
    bAlterou_fmeDadosEspecificos_AcaoJudicial = True

    'FLAVIO.BEZERRA - INICIO - 19/02/2013
    'AKIO.OKUNO - INICIO - 03/10/2012
        If strTipoProcesso = strTipoProcesso_Consulta Or _
         strTipoProcesso = strTipoProcesso_Encerrar Or _
         strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
         strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
'    If strTipoProcesso = strTipoProcesso_Consulta Or _
'       strTipoProcesso = strTipoProcesso_Encerrar Or _
'       strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
'       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Or _
'       gblsSinistro_Situacao_ID = "2" Or _
'       lblSinistro_Situacao(0) = "ENCERRADO" Then
        'AKIO.OKUNO - FIM - 03/10/2012        iParametroParaChamadaAuxiliar = 3
     'FLAVIO.BEZERRA - FIM - 19/02/2013
        iParametroParaChamadaAuxiliar = 3
    Else
        iParametroParaChamadaAuxiliar = iTpOperacao
    End If


    With grdDadosEspecificos_AcaoJudicial
        If .Rows > 1 Then
            sChaveComposta = .TextMatrix(.Row, 0)
            bProcessa = True
        Else
            If iParametroParaChamadaAuxiliar = 1 Then
                bProcessa = True
            Else
                bProcessa = False
            End If
        End If
    End With

    If bProcessa Then
        'If btnDadosEspecificos_AcaoJudicial_Operacao.Tag >= 2 Then
        If iParametroParaChamadaAuxiliar >= 2 Then
            If LenB(Trim(sChaveComposta)) <> 0 Then
                bProcessa = True
            Else
                MsgBox "N�o existe a��o judicial selecionada para consulta !", 16, "Mensagem ao Usu�rio"
                bProcessa = False
            End If
        Else
            bProcessa = True
        End If

        'aguardando desenvolvimento aplicacao
        '        If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
        If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
            'ExecutaAplicacao "SEGPxxxx", btnDadosEspecificos_AcaoJudicial_Operacao.Tag, lblSinistro_ID(0), sChaveComposta
            ExecutaAplicacao "SEGP1299", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
            'cristovao.rodrigues 24/08/2012
            If iParametroParaChamadaAuxiliar <> 3 Then
                '                bCarregou_AbaDadosEspecificos = False      'AKIO.OKUNO - 02/10/2012
                sAuxiliarExecutado = "AcaoJudicial"
                'VerificaRetornoAplicacao
            End If
        End If
    End If

    Exit Sub

Erro:
    Call TrataErroGeral("btnDadosEspecificos_AcaoJudicial_Operacao_Click", Me.Caption)
    bAlterou_fmeDadosEspecificos_AcaoJudicial = False

End Sub


Private Sub btnDadosEspecificos_Parecer_Consultar_Click()
CarregarAuxiliarParecer (3)
End Sub

Private Sub btnDadosEspecificos_Parecer_Excluir_Click()
CarregarAuxiliarParecer (2)
End Sub

Private Sub btnDadosEspecificos_Parecer_Incluir_Click()
CarregarAuxiliarParecer (1)
End Sub

Private Sub btnDadosEspecificos_Prestadores_Operacao_Click()

    CarregarAuxiliarPrestadores (1)

    ''    Dim sChaveComposta                          As String
    ''    Dim bProcessa                               As Boolean
    ''    bProcessa = False
    ''    bAlterou_fmeDadosEspecificos_Prestadores = True
    ''    iParametroParaChamadaAuxiliar = VerificaSelecao()
    ''
    '''    If strTipoProcesso = strTipoProcesso_Consulta Or _
     '''       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
    '''        iParametroParaChamadaAuxiliar = 3
    '''    Else
    '''        'SE FOR CHAMADO PELO BOT�O
    '''        iParametroParaChamadaAuxiliar = 1
    '''        If bExecutou_grdDblClick Then
    '''            iParametroParaChamadaAuxiliar = 2
    '''        End If
    '''    End If
    ''    With grdDadosEspecificos_Prestador
    ''        If .Rows > 1 Then
    ''            sChaveComposta = .TextMatrix(.Row, 9)
    ''            bProcessa = True
    ''        Else
    ''            If iParametroParaChamadaAuxiliar = 1 Then
    ''                bProcessa = True
    ''            Else
    ''                bProcessa = False
    ''            End If
    ''        End If
    ''    End With
    ''
    ''    If bProcessa Then
    ''        'If btnDadosEspecificos_Prestadores_Operacao.Tag >= 2 Then
    ''        If iParametroParaChamadaAuxiliar >= 2 Then
    ''            If LenB(Trim(sChaveComposta)) <> 0 Then
    ''                bProcessa = True
    ''            Else
    ''                MsgBox "N�o existe prestador selecionado para consulta !", 16, "Mensagem ao Usu�rio"
    ''                bProcessa = False
    ''            End If
    ''        Else
    ''            bProcessa = True
    ''        End If
    ''
    '''        If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
    ''        If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
    ''            'ExecutaAplicacao "SEGP1289", btnDadosEspecificos_Prestadores_Operacao.Tag, lblSinistro_ID(0), sChaveComposta
    ''            ExecutaAplicacao "SEGP1289", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
    ''            'cristovao.rodrigues 24/08/2012
    ''            If iParametroParaChamadaAuxiliar <> 3 Then
    ''                bCarregou_AbaDadosEspecificos = False
    ''                sAuxiliarExecutado = "Prestador"
    ''                'VerificaRetornoAplicacao
    ''            End If
    ''        End If
    ''    End If
    ''
    ''    Exit Sub
    ''
    ''Erro:
    ''    Call TrataErroGeral("btnDadosEspecificos_Prestadores_Operacao_Click", Me.Caption)
    ''    bAlterou_fmeDadosEspecificos_Prestadores = False

End Sub

'MATHAYDE
Sub CarregarAuxiliarPrestadores(iTpOperacao As Integer)

    Dim sChaveComposta As String
    Dim bProcessa As Boolean
    bProcessa = False
    bAlterou_fmeDadosEspecificos_Prestadores = True

    'AKIO.OKUNO - INICIO - 03/10/2012
    '    If strTipoProcesso = strTipoProcesso_Consulta Or _
         strTipoProcesso = strTipoProcesso_Encerrar Or _
         strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
         strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
    If strTipoProcesso = strTipoProcesso_Consulta Or _
       strTipoProcesso = strTipoProcesso_Encerrar Or _
       strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Or _
       gblsSinistro_Situacao_ID = "2" Or _
       lblSinistro_Situacao(0) = "ENCERRADO" Then
        'AKIO.OKUNO - FIM - 03/10/2012
        iParametroParaChamadaAuxiliar = 3
    Else
        iParametroParaChamadaAuxiliar = iTpOperacao
    End If


    With grdDadosEspecificos_Prestador
        If .Rows > 1 Then
            sChaveComposta = .TextMatrix(.Row, 9)
            bProcessa = True
        Else
            If iParametroParaChamadaAuxiliar = 1 Then
                bProcessa = True
            Else
                bProcessa = False
            End If
        End If
    End With

    If bProcessa Then
        'If btnDadosEspecificos_Prestadores_Operacao.Tag >= 2 Then
        If iParametroParaChamadaAuxiliar >= 2 Then
            If LenB(Trim(sChaveComposta)) <> 0 Then
                bProcessa = True
            Else
                MsgBox "N�o existe prestador selecionado para consulta !", 16, "Mensagem ao Usu�rio"
                bProcessa = False
            End If
        Else
            bProcessa = True
        End If

        '        If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
        If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
            'ExecutaAplicacao "SEGP1289", btnDadosEspecificos_Prestadores_Operacao.Tag, lblSinistro_ID(0), sChaveComposta
            ExecutaAplicacao "SEGP1289", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
            'cristovao.rodrigues 24/08/2012
            If iParametroParaChamadaAuxiliar <> 3 Then
                '                bCarregou_AbaDadosEspecificos = False          'AKIO.OKUNO - 02/10/2012
                sAuxiliarExecutado = "Prestador"
                'VerificaRetornoAplicacao
            End If
        End If
    End If

    Exit Sub

Erro:
    Call TrataErroGeral("btnDadosEspecificos_Prestadores_Operacao_Click", Me.Caption)
    bAlterou_fmeDadosEspecificos_Prestadores = False

End Sub


Sub CarregarAuxiliarParecer(iTpOperacao As Integer)
 ' CONFITEC - INCIDENTE (IM00748555)- inicio
 Dim parecer_id As Double
 ' CONFITEC - INCIDENTE (IM00748555)- fim
    If (iTpOperacao = 1) Then
    
            iParametroParaChamadaAuxiliar = iTpOperacao
            
            If PegaJanela(gblAuxiliarExecutando) = 0 Or gblAuxiliarExecutando = 0 Then
            
                ExecutaAplicacao "SEGP1420", iParametroParaChamadaAuxiliar, lblSinistro_ID(0)
            
            End If
            
        ElseIf (iTpOperacao = 2) Then
            
            iParametroParaChamadaAuxiliar = iTpOperacao
            
            If PegaJanela(gblAuxiliarExecutando) = 0 Or gblAuxiliarExecutando = 0 Then
            
                    parecer_id = grdDadosEspecificos_ParecerMedico.TextMatrix(grdDadosEspecificos_ParecerMedico.Row, 4)
                    
                    ExecutaAplicacao "SEGP1420", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), CStr(parecer_id)
            
            End If
    
    Else
    
            iParametroParaChamadaAuxiliar = iTpOperacao
            
            If PegaJanela(gblAuxiliarExecutando) = 0 Or gblAuxiliarExecutando = 0 Then
            
                If grdDadosEspecificos_ParecerMedico.Rows > 1 Then
                
                            parecer_id = grdDadosEspecificos_ParecerMedico.TextMatrix(grdDadosEspecificos_ParecerMedico.Row, 4)
                            
                            ExecutaAplicacao "SEGP1420", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), CStr(parecer_id)
                    
                    End If
            
            End If
    
    End If
    
      If iParametroParaChamadaAuxiliar <> 3 Then
                
            sAuxiliarExecutado = "Parecer_Medico"
            
      End If


End Sub


Private Sub btnDadosEspecificos_Reguladores_Operacao_Click(Index As Integer)
    CarregarAuxiliarReguladores (1)    ''mathayde - opercacao 1 - inclusao
'
'    Dim sChaveComposta                          As String
'    Dim bProcessa                               As Boolean
'    bProcessa = False
'
'    If Index = 0 Then
'        bAlterou_fmeDadosEspecificos_Reguladores = True
'
'        iParametroParaChamadaAuxiliar = VerificaSelecao()
'
''        If strTipoProcesso = strTipoProcesso_Consulta Or _
          ''           strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
''            iParametroParaChamadaAuxiliar = 3
''        Else
''            'SE FOR CHAMADO PELO BOT�O
''            iParametroParaChamadaAuxiliar = 1
''            If bExecutou_grdDblClick Then
''                iParametroParaChamadaAuxiliar = 2
''            End If
''        End If
'
'        With grdDadosEspecificos_Regulador
'            If .Rows > 1 Then
'                sChaveComposta = .TextMatrix(.Row, 12)
'                bProcessa = True
'            Else
'                If iParametroParaChamadaAuxiliar = 1 Then
'                bProcessa = True
'            Else
'                bProcessa = False
'            End If
'            End If
'        End With
'        If bProcessa Then
'            'If btnDadosEspecificos_Prestadores_Operacao.Tag >= 2 Then
'            If iParametroParaChamadaAuxiliar >= 2 Then
'                If LenB(Trim(sChaveComposta)) <> 0 Then
'                    bProcessa = True
'                Else
'                    MsgBox "N�o existe regulador selecionada para consulta !", 16, "Mensagem ao Usu�rio"
'                    bProcessa = False
'                End If
'            Else
'                bProcessa = True
'            End If
'
''            If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
'            If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
'                ExecutaAplicacao "SEGP1290", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
'                'ExecutaAplicacao "SEGP1290", btnDadosEspecificos_Prestadores_Operacao.Tag, lblSinistro_ID(0), sChaveComposta
'                'cristovao.rodrigues 24/08/2012
'                If iParametroParaChamadaAuxiliar <> 3 Then
'                    bCarregou_AbaDadosEspecificos = False
'                    sAuxiliarExecutado = "Regulador"
'                    'VerificaRetornoAplicacao
'                End If
'            End If
'        End If
'    End If
'
    Exit Sub

Erro:
    Call TrataErroGeral("btnDadosEspecificos_Prestadores_Operacao_Click", Me.Caption)
    bAlterou_fmeDadosEspecificos_Reguladores = False

End Sub

Private Sub btnDadosEspecificos_BensSinistrados_Operacao_Click()

    CarregaAuxiliarBensSinistrados (1)

    ''    Dim sChaveComposta                          As String
    ''    Dim bProcessa                               As Boolean
    ''    bProcessa = False
    ''    bAlterou_fmeDadosEspecificos_BensSinistrados = True
    ''    iParametroParaChamadaAuxiliar = VerificaSelecao()
    ''
    '''    If strTipoProcesso = strTipoProcesso_Consulta Or _
     '''       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
    '''        iParametroParaChamadaAuxiliar = 3
    '''    Else
    '''        'SE FOR CHAMADO PELO BOT�O
    '''        iParametroParaChamadaAuxiliar = 1
    '''        If bExecutou_grdDblClick Then
    '''            iParametroParaChamadaAuxiliar = 2
    '''        End If
    '''    End If
    ''
    ''    With grdDadosEspecificos_BensSinistrados
    ''        If .Rows > 1 Then
    ''            sChaveComposta = .TextMatrix(.Row, 4)
    ''            bProcessa = True
    ''        Else
    ''            If iParametroParaChamadaAuxiliar = 1 Then
    ''                bProcessa = True
    ''            Else
    ''                bProcessa = False
    ''            End If
    ''        End If
    ''    End With
    ''
    ''    If bProcessa Then
    ''        'If btnDadosEspecificos_BensSinistrados_Operacao.Tag >= 2 Then
    ''        If iParametroParaChamadaAuxiliar >= 2 Then
    ''            If LenB(Trim(sChaveComposta)) <> 0 Then
    ''                bProcessa = True
    ''            Else
    ''                MsgBox "N�o existe cobertura selecionada para consulta !", 16, "Mensagem ao Usu�rio"
    ''                bProcessa = False
    ''            End If
    ''        Else
    ''            bProcessa = True
    ''        End If
    ''
    '''        If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
    ''        If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
    ''            'ExecutaAplicacao "SEGP1292", btnDadosEspecificos_BensSinistrados_Operacao.Tag, lblSinistro_ID(0), sChaveComposta
    ''            ExecutaAplicacao "SEGP1292", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
    ''            'cristovao.rodrigues 24/08/2012
    ''            If iParametroParaChamadaAuxiliar <> 3 Then
    ''                bCarregou_AbaDadosEspecificos = False
    ''                sAuxiliarExecutado = "BensSinistrados"
    ''                'VerificaRetornoAplicacao
    ''            End If
    ''        End If
    ''    End If
    ''
    ''    Exit Sub
    ''
    ''Erro:
    ''    Call TrataErroGeral("btnDadosEspecificos_BensSinistrados_Operacao_Click", Me.Caption)
    ''    bAlterou_fmeDadosEspecificos_BensSinistrados = False
    ''
End Sub

'mathayde
Sub CarregaAuxiliarBensSinistrados(iTpOperacao As Integer)


    Dim sChaveComposta As String
    Dim bProcessa As Boolean
    bProcessa = False
    bAlterou_fmeDadosEspecificos_BensSinistrados = True

    'mathayde
    sAuxiliarExecutado = "BensSinistrados"

    'AKIO.OKUNO - INICIO - 03/10/2012
    '    If strTipoProcesso = strTipoProcesso_Consulta Or _
         strTipoProcesso = strTipoProcesso_Encerrar Or _
         strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
         strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
    If strTipoProcesso = strTipoProcesso_Consulta Or _
       strTipoProcesso = strTipoProcesso_Encerrar Or _
       strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Or _
       gblsSinistro_Situacao_ID = "2" Or _
       lblSinistro_Situacao(0) = "ENCERRADO" Then
        'AKIO.OKUNO - FIM - 03/10/2012        iParametroParaChamadaAuxiliar = 3
    Else
        iParametroParaChamadaAuxiliar = iTpOperacao
    End If





    With grdDadosEspecificos_BensSinistrados
        If .Rows > 1 Then
            sChaveComposta = .TextMatrix(.Row, 4)
            bProcessa = True
        Else
            If iParametroParaChamadaAuxiliar = 1 Then
                bProcessa = True
            Else
                bProcessa = False
            End If
        End If
    End With

    If bProcessa Then
        'If btnDadosEspecificos_BensSinistrados_Operacao.Tag >= 2 Then
        If iParametroParaChamadaAuxiliar >= 2 Then
            If LenB(Trim(sChaveComposta)) <> 0 Then
                bProcessa = True
            Else
                MsgBox "N�o existe cobertura selecionada para consulta !", 16, "Mensagem ao Usu�rio"
                bProcessa = False
            End If
        Else
            bProcessa = True
        End If

        '        If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
        If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
            'ExecutaAplicacao "SEGP1292", btnDadosEspecificos_BensSinistrados_Operacao.Tag, lblSinistro_ID(0), sChaveComposta
            ExecutaAplicacao "SEGP1292", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
            'cristovao.rodrigues 24/08/2012
            If iParametroParaChamadaAuxiliar <> 3 Then
                '                bCarregou_AbaDadosEspecificos = False          'AKIO.OKUNO - 02/10/2012
                sAuxiliarExecutado = "BensSinistrados"
                'VerificaRetornoAplicacao
            End If
        End If
    End If

    Exit Sub

Erro:
    Call TrataErroGeral("btnDadosEspecificos_BensSinistrados_Operacao_Click", Me.Caption)
    bAlterou_fmeDadosEspecificos_BensSinistrados = False

End Sub


Private Sub btnDadosGerais_Aviso_Cadastrar_Click()

    Dim sChaveComposta As String
    Dim bProcessa As Boolean
    bProcessa = False
    bAlterou_fmeDadosGerais_Aviso = True

    'FLAVIO.BEZERRA - IN�CIO - 28/01/2013
'    'If sOperacaoCosseguro = "C" And strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then
'    'Else
'    sChaveComposta = CStr("0" & lSolicitante_ID)
'    'End If

    If sOperacaoCosseguro = "C" And strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then
        sChaveComposta = CStr("0" & lSolicitante_ID & ".C")
    Else
    sChaveComposta = CStr("0" & lSolicitante_ID)
    End If
    'FLAVIO.BEZERRA - FIM - 28/01/2013
    'RALVES INICIO
    If strTipoProcesso = strTipoProcesso_Consulta Or _
       strTipoProcesso = strTipoProcesso_Encerrar Or _
       strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
        iParametroParaChamadaAuxiliar = 3
    Else
        If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
            iParametroParaChamadaAuxiliar = 1
        Else
            iParametroParaChamadaAuxiliar = 2
        End If
    End If
    'RALVES FIM
    'iParametroParaChamadaAuxiliar = VerificaSelecao()
    'iParametroParaChamadaAuxiliar = strTipoProcesso    'RALVES

    If iParametroParaChamadaAuxiliar >= 2 Then
        If LenB(Trim(sChaveComposta)) <> 0 Then
            bProcessa = True
        Else
            MsgBox "N�o existe aviso selecionado para consulta !", 16, "Mensagem ao Usu�rio"
            bProcessa = False
        End If
    Else
        bProcessa = True
    End If

    'aguardando desenvolvimento aplicacao
    If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then

        If strTipoProcesso = strTipoProcesso_Avisar Then        'Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then 'RALVES
            '            ExecutaAplicacao "SEGP1296", iParametroParaChamadaAuxiliar, Trim(Str(gbllProposta_ID)) & "." & Trim(Str(lSolicitante_ID)), Str(lSolicitante_ID)
            ExecutaAplicacao "SEGP1296", iParametroParaChamadaAuxiliar, Trim(Str(gbllProposta_ID)) & "." & Trim(Str(lSolicitante_ID))
        ElseIf strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then                                                                                                                                                                                          'RALVES
            ExecutaAplicacao "SEGP1296", iParametroParaChamadaAuxiliar, Trim(Str(gbllProposta_ID)) & "." & Trim(Str(lSolicitante_ID)) & "." & sOperacaoCosseguro        'RALVES
        Else
            ExecutaAplicacao "SEGP1296", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
        End If

        If iParametroParaChamadaAuxiliar <> 3 Then
            bCarregou_AbaDadosGerais = False
            sAuxiliarExecutado = "Aviso"

        End If

    End If
    Exit Sub

Erro:
    Call TrataErroGeral("btnDadosGerais_Aviso_Cadastrar_Click", Me.Caption)
    bAlterou_fmeDadosGerais_Aviso = False

End Sub


Private Sub btnDadosGerais_Solicitante_Cadastrar_Click()
'mathayde
    If btnDadosGerais_Solicitante_Cadastrar.Caption <> "No&vo" Then
        CarregarDadosSolicitante (2)
    Else
        CarregarDadosSolicitante (1)
    End If


    ''    Dim sChaveComposta                          As String
    ''    Dim bProcessa                               As Boolean
    ''    bProcessa = False
    ''    bAlterou_fmeDadosGerais_Solicitante = True
    ''
    ''    'iParametroParaChamadaAuxiliar = VerificaSelecao()
    ''
    ''    'If btnDadosGerais_Solicitante_Cadastrar.Caption = "&Alterar" Then
    ''        iParametroParaChamadaAuxiliar = strTipoProcesso
    ''    'End If
    ''
    '''    If strTipoProcesso = strTipoProcesso_Consulta Or _
     '''       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
    '''        iParametroParaChamadaAuxiliar = 3
    '''    Else
    '''        'SE FOR CHAMADO PELO BOT�O
    '''        iParametroParaChamadaAuxiliar = 1
    '''        If bExecutou_grdDblClick Then
    '''            iParametroParaChamadaAuxiliar = 2
    '''        End If
    '''    End If
    ''
    ''    sChaveComposta = CStr("0" & lSolicitante_ID)
    ''
    ''    If iParametroParaChamadaAuxiliar >= 2 Then
    ''        If LenB(Trim(sChaveComposta)) <> 0 Then
    ''            bProcessa = True
    ''        Else
    ''            MsgBox "N�o existe solicitante selecionado para consulta !", 16, "Mensagem ao Usu�rio"
    ''            bProcessa = False
    ''        End If
    ''    Else
    ''        bProcessa = True
    ''    End If
    ''
    '''    If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
    ''    If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
    ''
    ''        ExecutaAplicacao "SEGP1291", iParametroParaChamadaAuxiliar, IIf(lblSinistro_ID(0) = "", "0", lblSinistro_ID(0)), sChaveComposta
    ''        'cristovao.rodrigues 24/08/2012
    ''        If iParametroParaChamadaAuxiliar <> 3 Then
    ''            bCarregou_AbaDadosGerais = False
    ''            sAuxiliarExecutado = "Solicitante"
    ''            'VerificaRetornoAplicacao
    ''        End If
    ''
    ''    End If
    ''    Exit Sub
    ''
    ''Erro:
    ''    Call TrataErroGeral("btnDadosGerais_Solicitante_Cadastrar_Click", Me.Caption)
    ''    bAlterou_fmeDadosGerais_Solicitante = False
End Sub


Private Sub btnDespesas_Click(Index As Integer)
    Carrega_Detalhe_Resseguro (3)
End Sub

Private Sub btnDespressarcimento_Click(Index As Integer)
    Carrega_Detalhe_Resseguro (5)
End Sub

Private Sub btnDespSalvados_Click(Index As Integer)
    Carrega_Detalhe_Resseguro (7)
End Sub
'gustavo.cruz 21/12/2017
Private Sub btnDetalhamento_EnviarSite_Click()
Dim sSQL As String
Dim restritoWeb As String
Dim detalhamentoId As String

On Error GoTo Erro

     If MsgBox("Enviar detalhamentos do sinistro?", vbYesNo + vbQuestion) = vbYes Then
     
            For Linha = 1 To grdDetalhamento.Rows - 1
            
                If grdDetalhamento.TextMatrix(Linha, 5) = 0 Then ' n�o permite detalhamento historico
                
                    If grdDetalhamento.TextMatrix(Linha, 0) = strChecked Then
                        restritoWeb = "S"
                        Else
                        restritoWeb = "N"
                    End If
                    
                    detalhamentoId = grdDetalhamento.TextMatrix(Linha, 2)
                                                                  
                    sSQL = ""
                    sSQL = sSQL & "EXEC SEGS13639_SPU " & gbldSinistro_ID
                    sSQL = sSQL & ", " & detalhamentoId
                    sSQL = sSQL & ", '" & restritoWeb & "'"
                    sSQL = sSQL & ", '" & cUserName & "'"
                            
                    Conexao_ExecutarSQL gsSIGLASISTEMA _
                                          , glAmbiente_id _
                                          , App.Title _
                                          , App.FileDescription _
                                          , sSQL _
                                          , lConexaoLocal _
                                          , False
                                                          
                End If
            
            Next Linha
            
            MsgBox "Detalhamentos enviados para o site.", vbOKOnly + vbInformation
            Exit Sub
            
    Else
        Exit Sub
    
    End If
    
    
Erro:
    Call TrataErroGeral("btnDetalhamento_EnviarSite_Click", Me.Caption)
    Call FinalizarAplicacao
End Sub
Private Sub btnDetalhamento_VisualizarImpressao_Cadastrar_Click()
    Dim sChaveParaCommandDemaisCampos            As String
'    bAlterou_fmeDetalhamento_VisualizarImpressao = True    'AKIO.OKUNO - 30/04/2013
    bAlterou_fmeDetalhamento_VisualizarImpressao = False    'AKIO.OKUNO - 30/04/2013
    
    'FLAVIO.BEZERRA - IN�CIO - 14/02/2013
    sChaveParaCommandDemaisCampos = ""
    sChaveParaCommandDemaisCampos = Format(Data_Sistema, "dd/mm/yyyy") & "."                                    'Data_Sistema (string, formato: dd/mm/yyyy)
    sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & IIf(bGTR_Implantado = True, 1, 0) & "."     'GTR_Implantado (0 = false, 1 = true)
    sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllProduto_ID & "."                        'Produto_ID (produto_ID da Proposta/Sinistro) (Integer)
'    sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & Seleciona_Localizacao_Processo_Local & "."  'Localizacao_Processo (string)                 'AKIO.OKUNO - MP - 18/02/2013
'    sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & IIf(Verifica_Protocolacao = True, 1, 0)     'Verifica_Protocolacao (0 = false, 1 = true)   'AKIO.OKUNO - MP - 18/02/2013
    sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & lblDescricaoLocalizacaoProcesso(1).Tag & "." 'Localizacao_Processo (string)                'AKIO.OKUNO - MP - 18/02/2013

'AKIO.OKUNO - INICIO - 30/04/2013
'    sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & IIf(bVerifica_Protocolacao, 1, 0)           'Verifica_Protocolacao (0 = false, 1 = true)    'AKIO.OKUNO - MP - 18/02/2013
    sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & IIf(bVerifica_Protocolacao, 1, 0) & "."      'Verifica_Protocolacao (0 = false, 1 = true)
    sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllRamo_ID & "."                            'SEGP1286.SinRamo_Id
    sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllApolice_ID & "."                         'SEGP1286.SinApolice_Id
    sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllSucursal_Seguradora_ID & "."             'SEGP1286.SinSucursal_Id
    sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllSeguradora_Cod_Susep & "."               'SEGP1286.SinSeguradora_Id
    sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gblsSinistro_Situacao_ID                     'SEGP1286.SinSituacao_Id
'AKIO.OKUNO - FIM - 30/04/2013

    'mathayde
    'iParametroParaChamadaAuxiliar = VerificaSelecao()
    'iParametroParaChamadaAuxiliar = "1"
    iParametroParaChamadaAuxiliar = "2"    'F.BEZERRA 19/10/2012

    '    If PegaJanela(gblAuxiliarExecutando) = 0 Or gblAuxiliarExecutando = 0 Then
    '        ExecutaAplicacao "SEGP1286", iParametroParaChamadaAuxiliar, lblSinistro_ID(0)
    'F.BEZERRA 19/10/2012
    If PegaJanela(gblAuxiliarExecutando) = 0 Or gblAuxiliarExecutando = 0 Then
        'ExecutaAplicacao "SEGP1286", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), "1" 'FLAVIO.BEZERRA - 14/02/2013
        'ExecutaAplicacao "tela", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), "1"
        'cristovao.rodrigues 24/08/2012
        ExecutaAplicacao "SEGP1286", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveParaCommandDemaisCampos
    'FLAVIO.BEZERRA - FIM - 14/02/2013
        If iParametroParaChamadaAuxiliar <> 3 Then
            bCarregou_AbaDetalhamento = False
            sAuxiliarExecutado = "Detalhamento"
            'VerificaRetornoAplicacao
        End If
    End If
    Exit Sub
    
Erro:
    Call TrataErroGeral("btnDetalhamento_VisualizarImpressao_Cadastrar_Click", Me.Caption)
    bAlterou_fmeDetalhamento_VisualizarImpressao = False
End Sub


Private Sub btnEncerrar_Sinistro_Click()
    On Error GoTo Erro
    
    Dim sChaveComposta As String
    Dim sNomeAplicacao As String

    bAlterou_fmeEstimativasPagamentos_Valores = True
    iParametroParaChamadaAuxiliar = 2 'faz envio do tipo de processo
    sChaveComposta = Val(txtCountEstimativa.Tag)
    
    If PegaJanela(gblAuxiliarExecutando) = 0 Or gblAuxiliarExecutando = 0 Then
        
        sNomeAplicacao = "SEGP1294"
        sAuxiliarExecutado = "EnceSinistro"
        iParametroParaChamadaAuxiliar = 1
    
        sChaveComposta = sChaveComposta & Format(Data_Sistema, "dd/mm/yyyy") & "."                      '- 6 - Data_Sistema (string, formato: dd/mm/yyyy)
        sChaveComposta = sChaveComposta & IIf(sOperacaoCosseguro <> "C", "-", sOperacaoCosseguro) & "." '- 7 - OperacaoCosseguro (C = Cosseguro, - = N�o cosseguro)

        sChaveComposta = sChaveComposta & gbllProduto_ID & "."                                          '- 8 - Produto_ID (produto_ID da Proposta/Sinistro) (Integer)
        sChaveComposta = sChaveComposta & IIf(bGTR_Implantado, 1, 0) & "."                              '- 9 - GTR_Implantado
        
        With grdDadosGerais_Aviso                                                                       '- 10 - SINISTRO_BB
            If .Rows > 1 Then
                sChaveComposta = sChaveComposta & .TextMatrix(1, 0)
            End If
            sChaveComposta = sChaveComposta & "."
        End With
        
        ExecutaAplicacao sNomeAplicacao, iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
       
    End If
    
    Exit Sub
    

Erro:
    Call TrataErroGeral("btnEstimativasPagamentos_Valores_Cobertura_Click", Me.Caption)
    bAlterou_fmeEstimativasPagamentos_Valores = False
    

End Sub

Private Sub btnEstimativasPagamentos_Valores_Beneficiario_Alterar_Click()
'mathayde 03
    CarregarAuxiliarBeneficiario (1)
    '    Dim sChaveComposta                          As String
    '    Dim bProcessa                               As Boolean
    '    bProcessa = False
    '    bAlterou_fmeEstimativasPagamentos_Valores_Beneficiario = True
    '
    '    iParametroParaChamadaAuxiliar = VerificaSelecao()
    ''AKIO.OKUNO - INICIO - 27/09/2012
    '    If strTipoProcesso = strTipoProcesso_Reabrir Or strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro Then
    '        iParametroParaChamadaAuxiliar = 1
    '    End If
    ''AKIO.OKUNO - FIM - 27/09/2012
    '
    ''    If strTipoProcesso = strTipoProcesso_Consulta Or _
     ''       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
    ''        iParametroParaChamadaAuxiliar = 3
    ''    Else
    ''        'SE FOR CHAMADO PELO BOT�O
    ''        iParametroParaChamadaAuxiliar = 1
    ''        If bExecutou_grdDblClick Then
    ''            iParametroParaChamadaAuxiliar = 2
    ''        End If
    ''    End If
    '
    '    With grdEstimativasPagamentos_Beneficiario
    '        If .Rows > 1 Then
    '            sChaveComposta = .TextMatrix(.Row, 17) 'mathayde
    '            bProcessa = True
    '        Else
    '            If iParametroParaChamadaAuxiliar = 1 Then
    '                bProcessa = True
    '            Else
    '                bProcessa = False
    '            End If
    '        End If
    '    End With
    '
    '    If bProcessa Then
    '        If iParametroParaChamadaAuxiliar >= 2 Then
    '            If LenB(Trim(sChaveComposta)) <> 0 Then
    '                bProcessa = True
    '            Else
    '                MsgBox "N�o existe benefici�rio selecionado para consulta !", 16, "Mensagem ao Usu�rio"
    '                bProcessa = False
    '            End If
    '        Else
    '            bProcessa = True
    '        End If
    '
    ''        If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
    '        If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
    '            ExecutaAplicacao "SEGP1288", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
    '            'cristovao.rodrigues 24/08/2012
    '            If iParametroParaChamadaAuxiliar <> 3 Then
    '                bCarregou_AbaEstimativasPagamentos = False
    '                sAuxiliarExecutado = "Beneficiario"
    '                'VerificaRetornoAplicacao
    '            End If
    '        End If
    '    End If
    '

    Exit Sub

Erro:
    Call TrataErroGeral("btnEstimativasPagamentos_Valores_Beneficiario_Alterar_Click", Me.Caption)
    bAlterou_fmeEstimativasPagamentos_Valores_Beneficiario = False

End Sub

Private Sub btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider_Click()
    btnEstimativasPagamentos_Valores_Beneficiario_Alterar_Click
End Sub

Private Sub btnEstimativasPagamentos_Valores_Beneficiario_CadastrarProposta_Click()
    MsgBox "Em Desenvolvimento ", 16, "Mensagem "

End Sub

Private Sub btnEstimativasPagamentos_Valores_Cobertura_Click()
    Dim sChaveComposta As String
    Dim bProcessa As Boolean
    Dim sNomeAplicacao As String

    bProcessa = False
    bAlterou_fmeEstimativasPagamentos_Valores = True

    'mathayde
    'iParametroParaChamadaAuxiliar = VerificaSelecao()
    If strTipoProcesso = strTipoProcesso_Consulta _
       Or strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
        iParametroParaChamadaAuxiliar = 3
    Else
        iParametroParaChamadaAuxiliar = 2 'cristovao.rodrigues 29/10/2012 mudei de 1 para 2, assim faz envio do tipo de processo
    End If

    If Val(txtCountEstimativa.Tag) <> 0 Then
        sChaveComposta = txtCountEstimativa.Tag
    Else
        sChaveComposta = "0" 'cristovao.rodrigues 30/10/2012 mudei de "" para "0"
        'btnEstimativasPagamentos_Valores_Cobertura.Tag = 1
        iParametroParaChamadaAuxiliar = 2 'cristovao.rodrigues 30/10/2012 mudei de 1 para 2, assim faz envio do tipo de processo
    End If

    If iParametroParaChamadaAuxiliar >= 2 Then
        If LenB(Trim(sChaveComposta)) <> 0 Then
            bProcessa = True
        Else
            MsgBox "N�o existe estimativa selecionada para consulta !", 16, "Mensagem ao Usu�rio"
            bProcessa = False
        End If
    Else
        bProcessa = True
    End If

    'mathayde
    'If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
    If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
        If btnEstimativasPagamentos_Valores_Cobertura.Caption = "&Encerrar" Then
            sNomeAplicacao = "SEGP1294"
            sAuxiliarExecutado = "EnceSinistro"
            iParametroParaChamadaAuxiliar = 1 'cristovao.rodrigues 30/10/2012
            'SUBSTITUIDO PELA ROTINA DE APPACTIVATE
            '            Me.Enabled = False      'AKIO.OKUNO - 27/09/2012
        Else
            sNomeAplicacao = "SEGP1287"
            sAuxiliarExecutado = "Estimativa"
            sChaveComposta = sChaveComposta & "." & strTipoProcesso & "."    'AKIO.OKUNO - 10/10/2012
        End If
        
        'FLAVIO.BEZERRA - IN�CIO - 15/02/2013
        sChaveComposta = sChaveComposta & Format(Data_Sistema, "dd/mm/yyyy") & "."                      '- 6 - Data_Sistema (string, formato: dd/mm/yyyy)
        sChaveComposta = sChaveComposta & IIf(sOperacaoCosseguro <> "C", "-", sOperacaoCosseguro) & "." '- 7 - OperacaoCosseguro (C = Cosseguro, - = N�o cosseguro)
'        sChaveComposta = sChaveComposta & gbllProduto_ID                                                '- 8 - Produto_ID (produto_ID da Proposta/Sinistro) (Integer)  'AKIO.OKUNO - 06/05/2013
        'FLAVIO.BEZERRA - FIM - 15/02/2013
'AKIO.OKUNO - INICIO - 07/06/2013
        sChaveComposta = sChaveComposta & gbllProduto_ID & "."                                          '- 8 - Produto_ID (produto_ID da Proposta/Sinistro) (Integer)
        sChaveComposta = sChaveComposta & IIf(bGTR_Implantado, 1, 0) & "."                              '- 9 - GTR_Implantado
        With grdDadosGerais_Aviso                                                                       '- 10 - SINISTRO_BB
            If .Rows > 1 Then
                sChaveComposta = sChaveComposta & .TextMatrix(1, 0)
            End If
            sChaveComposta = sChaveComposta & "."
        End With
'AKIO.OKUNO - FIM - 07/06/2013
        
        'MU-2018-052406- Ajuste autom�tico das Reservas � SEGBR (Victor Fonseca)
        'Inclus�o do par�metro cosseguro na chamada do SEGP1287
        sChaveComposta = sChaveComposta & sOperacaoCosseguro & "."
        
        If sNomeAplicacao = "SEGP1287" Then
            sChaveComposta = sChaveComposta & "." & gsCPF & "."    'NTendencia - Vagner - 15/10/2019
        End If
		
        ' Cesar Santos CONFITEC - (SBRJ009952) 18/08/2020 inicio
        If sNomeAplicacao = "SEGP1287" And gbllProduto_ID = 1243 Then
            btnEstimativasPagamentos_Valores_Cobertura.Enabled = False
            Exit Sub
        End If
        ' Cesar Santos CONFITEC - (SBRJ009952) 18/08/2020 fim
		
        ExecutaAplicacao sNomeAplicacao, iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta

        'cristovao.rodrigues 24/08/2012
        If iParametroParaChamadaAuxiliar <> 3 Then
            bCarregou_AbaEstimativasPagamentos = False
            bCarregou_AbaHistorico = False 'cristovao.rodrigues 26/10/2012
            '            sAuxiliarExecutado = "Estimativa"
            'VerificaRetornoAplicacao
        End If
    End If
    Exit Sub

Erro:
    Call TrataErroGeral("btnEstimativasPagamentos_Valores_Cobertura_Click", Me.Caption)
    bAlterou_fmeEstimativasPagamentos_Valores = False

End Sub

Private Sub Reabrir_Click()
    Dim sChaveComposta As String
    Dim bProcessa As Boolean
    bProcessa = False

    '    iParametroParaChamadaAuxiliar = 1 'strTipoProcesso 'AKIO.OKUNO - 28/09/2012
    iParametroParaChamadaAuxiliar = 4
    bProcessa = True

    '    If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
    If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
        sAuxiliarExecutado = "ReabSinistro"
'        ExecutaAplicacao "SEGP1295", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta 'AKIO.OKUNO - 28/05/2013
        ExecutaAplicacao "SEGP1295", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta & sOperacaoCosseguro & "." & Data_Sistema & "."
        bCarregou_AbaHistorico = False 'cristovao.rodrigues 26/10/2012
    End If

    Exit Sub

Erro:
    Call TrataErroGeral("Reabrir_Click", Me.Caption)

End Sub

Private Sub btnHistoricoCliente_Click()
   Call AbreSiteHistorico
End Sub

Private Sub btnHonorarios_Click(Index As Integer)
    Carrega_Detalhe_Resseguro (2)
End Sub

Private Sub btnIndenizacao_Click()
    Carrega_Detalhe_Resseguro (1)
End Sub

Private Sub btnressarcimento_Click(Index As Integer)
    Carrega_Detalhe_Resseguro (4)
End Sub

Private Sub btnSalvados_Click(Index As Integer)
    Carrega_Detalhe_Resseguro (6)
End Sub

Private Sub btnSeq_Estimativa_DownClick()
    On Error GoTo Erro

    With btnSeq_Estimativa
        If .Value >= 1 Then
            bAlterou_Seq_Estimativa = True
            Seleciona_Estimativa .Value
        End If
    End With
    Exit Sub

Erro:
    Call TrataErroGeral("btnSeq_Estimativa_DownClick", Me.Caption)
    Call FinalizarAplicacao

End Sub


Private Sub btnSeq_Estimativa_UpClick()
    On Error GoTo Erro

    With btnSeq_Estimativa
        If .Value <= Val(txtCountEstimativa.Tag) Then
            bAlterou_Seq_Estimativa = True
            Seleciona_Estimativa .Value
        End If
    End With
    Exit Sub

Erro:
    Call TrataErroGeral("btnSeq_Estimativa_UpClick", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub Seleciona_Estimativa(iSeq_Estimativa As Integer)
    On Error GoTo Erro

    CarregaDadosTela_AbaEstimativasPagamentos iSeq_Estimativa
    Exit Sub

Erro:
    Call TrataErroGeral("Seleciona_Estimativa", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub btnSolicitante_Sinistro_Telefone_Consulta_Click()

    FrmAlteraTelefoneSolicitante.Show 1

End Sub

Private Sub InicializaVariaveisLocal()
    bAtivou_Form = True 'Alan Neves - 16/04/2013
    bCarregou_Form = True
    bCarregou_AbaDadosGerais = False
    bCarregou_AbaDetalhamento = False
    bCarregou_AbaHistorico = False
    bCarregou_AbaEstimativas = False
    bCarregou_AbaDadosEspecificos = False
    bTipoProcesso_Avisar_Liberado = False 'FLAVIO.BEZERRA - 21/02/2013
    
    'AKIO.OKUNO - MP - INICIO - 09/05/2013
    If strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
        bAlterou_fmeDetalhamento_VisualizarImpressao = True
    End If
    'AKIO.OKUNO - MP - FIM - 09/05/2013

    'Inicializa em Modo Consulta
    bytModoOperacao = 3
    'MATHAYDE
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        iParametroParaChamadaAuxiliar = 2
    ElseIf strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Then        'RALVES (POR AKIO.OKUNO)
        iParametroParaChamadaAuxiliar = 5                                   'RALVES (POR AKIO.OKUNO)
    Else
        iParametroParaChamadaAuxiliar = strTipoProcesso
    End If


    TipoProcesso_Avisar_Liberado = False    'cristovao.rodrigues 23/08/2012
    sSinistro_Situacao_ID = 2

    bGTR_Implantado = False

    bGerou_Aviso = False    'AKIO.OKUNO - 05/10/2012
    bGerou_Estimativa = False   'AKIO.OKUNO - 05/10/2012
    bRetornaSemGerarAviso = False   'AKIO.OKUNO - 05/10/2012
    bRetornoEncerramento = False

End Sub

Private Sub DestroiInterface()
    On Error GoTo Erro

    Set rsRecordSet = Nothing

    'X = FecharConexao(lConexaoLocal) 'cristovao.rodrigues 23/08/2012

    bAtivou_Form = False 'Alan Neves - 16/04/2013
    bCarregou_Form = False
    bCarregou_AbaDadosGerais = False
    bCarregou_AbaDetalhamento = False
    bCarregou_AbaHistorico = False
    bCarregou_AbaEstimativas = False
    bCarregou_AbaDadosEspecificos = False
    bCarregou_AbaEstimativasPagamentos = False
    bCarregou_AbaCausasEEventos = False
    bCarregou_AbaPLD = False            'AKIO.OKUNO - 23/01/2013
        bCarregou_AbaAgravamento = False    'wilder - Agravamento Pecu�rio
    bAlterou_Seq_Estimativa = False

    'FBEZERRA/ FABREU - Inclus�o barra de rolagem SEGP1285 - 21/02/2018
    PosAnterior = 0
    '-------------------------------------------------------------------

    Set cDados_Estimativa = Nothing

    Exit Sub

Erro:
    Call TrataErroGeral("DestroiInterface", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub InicializaInterfaceLocal()
    On Error GoTo Erro
    Dim sSQL As String
    Dim RS As Recordset

    'Alexandre - INICIO 30/07/2014 -
    'Modificado - 28/08/2014
    lblSinistro_ID(0) = gbldSinistro_ID
    CarregaDadosTela_Localizacao
    If Seleciona_Localizacao_Processo(gbldSinistro_ID) = 2 And bGTR_Implantado Then 'Verifica se � seguradora para habilitar bot�o
    'Visualizacao do controle
    Dim booSaldo_Devedor_Visivel As Boolean
    booSaldo_Devedor_Visivel = (strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem) Or (strTipoProcesso = strTipoProcesso_Avaliar)
    btnSolicitarSaldo.Visible = booSaldo_Devedor_Visivel
    Else
    btnSolicitarSaldo.Visible = False
    
    End If
    'Alexandre - FIM 30/07/2014
    
    Screen.MousePointer = vbHourglass

    'Configurando interface''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    'Alan Neves - 08/03/2013 - Inicio - Corre��o erro de maximizar e minimizar.
    'Call CentraFrm(Me)
    If Me.WindowState = 0 Then
       Me.Move (Screen.Width - Me.Width) \ 2, (Screen.Height - Me.Height) \ 2
    End If
    'Alan Neves - 08/03/2013 - Fim
    

    'Limpando Campos
    LimpaCamposTelaLocal Me                                        'SERA ALTERADO
    'AKIO.OKUNO - INICIO 23/09/2012
    tabPrincipal.TabVisible(5) = (bytTipoRamo = bytTipoRamo_Vida And _
                                  (strTipoProcesso <> strTipoProcesso_Avisar And strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro) _
                                )  'Causas e Eventos
    'AKIO.OKUNO - FIM 23/09/2012
    If gbldSinistro_ID = 0 Then
        If strTipoProcesso = strTipoProcesso_Avisar Or _
           strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
            With txtProposta_ID
                .Text = FrmConsultaSinistrocon.grdSelecao.TextMatrix(FrmConsultaSinistrocon.grdSelecao.Row, 4)
                .Refresh
            End With
        Else
            With lblSinistro_ID(0)
                .Caption = FrmConsultaSinistrocon.grdSelecao.TextMatrix(FrmConsultaSinistrocon.grdSelecao.Row, 1)
                .Refresh
            End With
        End If
    Else
        With lblSinistro_ID(0)
            .Caption = gbldSinistro_ID
            .Refresh
        End With
    End If
    InicializaInterfaceLocal_Grid_AbaEstimativasPagamentos

    CarregaDadosTela_AbaDadosGerais

    'AKIOOKUNO
    '    CarregaDadosTela_Localizacao

    InicializaModoOperacaoLocal

    InicializaInterface_Funcionalidades

    Select Case strTipoProcesso
    Case strTipoProcesso_Reabrir, Is <> strTipoProcesso_Avaliar
        If strTipoProcesso = strTipoProcesso_Reabrir _
           Or strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro Then
            'REVER
            '               Shell reabrir.exe
        End If
        With tabPrincipal
            If .Tab <> 0 Then
                .Tab = 0
            End If
        End With
    Case Else
    End Select

    'AKIO.OKUNO - INICIO - 27/09/2012
    '    If strTipoProcesso = strTipoProcesso_Encerrar Or strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Then
'    If strTipoProcesso = strTipoProcesso_Encerrar Or strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or strTipoProcesso = strTipoProcesso_Avaliar_Cosseguro Then  'AKIO.OKUNO - 22/10/2012
    If strTipoProcesso = strTipoProcesso_Encerrar Or strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or strTipoProcesso = strTipoProcesso_Avaliar_Cosseguro Or strTipoProcesso = strTipoProcesso_Reabrir Or strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro Then
        tabPrincipal.Tab = 3
        tabPrincipal_Click 3
    Else
        tabPrincipal.Tab = 0
    End If

'AKIO.OKUNO - MP - INICIO - 09/05/2013
'    'F.BEZERRA 22/10/2012
'    If strTipoProcesso <> strTipoProcesso_Avisar Or strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then
'        sSQL = ""
'        sSQL = sSQL & " SELECT A.EVENTO_SINISTRO_ID " & vbNewLine
'        sSQL = sSQL & " FROM EXIBICAO_EXIGENCIA_RAMO_TB A " & vbNewLine
'        sSQL = sSQL & "  WITH (NOLOCK)  INNER JOIN SINISTRO_TB B " & vbNewLine
'        sSQL = sSQL & "  WITH (NOLOCK)  ON A.EVENTO_SINISTRO_ID = B.EVENTO_SINISTRO_ID " & vbNewLine
'        sSQL = sSQL & " OR A.RAMO_ID = B.RAMO_ID WHERE B.SINISTRO_ID = " & gbldSinistro_ID & vbNewLine
'
'        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                     glAmbiente_id, _
'                                     App.Title, _
'                                     App.FileDescription, _
'                                     sSQL, _
'                                     lConexaoLocal)
'        cmdExigencia.Visible = Not rs.EOF
'
'        'cristovao.rodrigues 04/01/2013 verificar se botao novo nao esta visivel e se for consulta
'        If (strTipoProcesso = strTipoProcesso_Consulta Or strTipoProcesso = strTipoProcesso_Consulta_Cosseguro) _
'                And btnDetalhamento_VisualizarImpressao_Cadastrar.Visible = False Then
'            InicializaInterface_Dimensao_Objeto txtDetalhamento_Visualizar, "d", 0, cmdExigencia.Height, 0, 0
'        End If
'
'        Set rs = Nothing
'    End If
    
    cmdExigencia.Visible = False
    btnSolicitarSaldo.Enabled = False

    If strTipoProcesso <> strTipoProcesso_Avisar Or strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Or gbldSinistro_ID <> 0 Then
        
'+----------------------------------------------------------------------
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'|
        sSQL = "SET NOCOUNT ON  Exec SEGS13137_SPS " & gbldSinistro_ID & ",'" & strTipoProcesso & "', " & gbllProduto_ID
        
'        sSQL = ""
'        sSQL = sSQL & "declare @liberacao Char(1)                                                                                               " & vbNewLine
'        sSQL = sSQL & "declare @Exigencia Int                                                                                                   " & vbNewLine
'        sSQL = sSQL & "Set @liberacao     = 0                                                                                                   " & vbNewLine
'        sSQL = sSQL & "Set @Exigencia     = 0                                                                                                   " & vbNewLine
'        If strTipoProcesso <> strTipoProcesso_Avisar Or strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then
'            sSQL = sSQL & " SELECT @Exigencia = Count(1) " & vbNewLine
'            sSQL = sSQL & " FROM EXIBICAO_EXIGENCIA_RAMO_TB A " & vbNewLine
'            sSQL = sSQL & "  WITH (NOLOCK)  INNER JOIN SINISTRO_TB B " & vbNewLine
'            sSQL = sSQL & "  WITH (NOLOCK)  ON A.EVENTO_SINISTRO_ID = B.EVENTO_SINISTRO_ID " & vbNewLine
'            sSQL = sSQL & " OR A.RAMO_ID = B.RAMO_ID WHERE B.SINISTRO_ID = " & gbldSinistro_ID & vbNewLine
'        End If
'        If gbldSinistro_ID <> 0 Then
'            sSQL = sSQL & "select @liberacao = 'N'                                                                                                  " & vbNewLine
'            sSQL = sSQL & "if exists (select sinistro_solicita_saldo                                                                                " & vbNewLine
'            sSQL = sSQL & "             from produto_tb  WITH (NOLOCK)                                                                                 " & vbNewLine
'            sSQL = sSQL & "            where produto_id = " & gbllProduto_ID & "                                                                    " & vbNewLine
'            sSQL = sSQL & "              and sinistro_solicita_saldo = 's')                                                                         " & vbNewLine
'            sSQL = sSQL & "begin                                                                                                                    " & vbNewLine
'            sSQL = sSQL & "      if not exists (select 1                                                                                                " & vbNewLine  'FLAVIO.BEZERRA - 01/03/2013
'            sSQL = sSQL & "                   from sinistro_solicita_saldo_tb  WITH (NOLOCK)                                                           " & vbNewLine
'            sSQL = sSQL & "                  where sinistro_solicita_saldo_tb.sinistro_id = " & gbldSinistro_ID & "                                 " & vbNewLine
'            sSQL = sSQL & "                    and sinistro_solicita_saldo_tb.situacao > 0 )                                                        " & vbNewLine
'            sSQL = sSQL & "      begin                                                                                                              " & vbNewLine
'            sSQL = sSQL & "           select @liberacao = 'S'                                                                                       " & vbNewLine
'            sSQL = sSQL & "             from evento_segbr_sinistro_atual_tb  WITH (NOLOCK)                                                             " & vbNewLine
'            sSQL = sSQL & "            where evento_segbr_sinistro_atual_tb.sinistro_id = " & gbldSinistro_ID & "                                   " & vbNewLine
'            sSQL = sSQL & "              and evento_segbr_sinistro_atual_tb.evento_bb_id = 1110                                                     " & vbNewLine
'            sSQL = sSQL & "           and IsNull(Evento_SEGBR_Sinistro_Atual_Tb.Num_Recibo, 0 ) = 0                                                 " & vbNewLine
'            sSQL = sSQL & "      end                                                                                                                " & vbNewLine
'            sSQL = sSQL & "end                                                                                                                      " & vbNewLine
'        End If
'        sSQL = sSQL & "select @Exigencia                                                                                                        " & vbNewLine
'        sSQL = sSQL & "     , @liberacao                                                                                                        " & vbNewLine
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'+----------------------------------------------------------------------
        
        Set RS = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     sSQL, _
                                     lConexaoLocal)
        
        cmdExigencia.Visible = RS.Fields(0) <> 0

        If (strTipoProcesso = strTipoProcesso_Consulta Or strTipoProcesso = strTipoProcesso_Consulta_Cosseguro) _
                And btnDetalhamento_VisualizarImpressao_Cadastrar.Visible = False Then
            InicializaInterface_Dimensao_Objeto txtDetalhamento_Visualizar, "d", 0, cmdExigencia.Height, 0, 0
        End If
    
        If RS.Fields(1) = "S" And CInt("0" & gblsSinistro_Situacao_ID) < 2 Then
            btnSolicitarSaldo.Enabled = True
        End If
        
        Set RS = Nothing
    End If
'    'AKIO.OKUNO - INICIO - 09/11/2012
'    ValidaDados_PermiteSolicitacaoSaldo
'    'AKIO.OKUNO - FIM - 09/11/2012
'AKIO.OKUNO - MP - FIM - 09/05/2013

    Screen.MousePointer = vbNormal
    'Alexandre.Valim - INICIO - 28/07/2014
    btnSolicitarSaldo.Caption = IIf(Solicitou_Saldo_Devedor(lblSinistro_ID(0).Caption), "Cancelar solicita��o de saldo", "Solicitar saldo")
    'Alexandre.Valim - FIM - 28/07/2014
   
    '(INI) Demanda 18225206.
    If bytTipoRamo = bytTipoRamo_Vida Then
        fmeVidaTelFax.Visible = True
    Else
        fmeVidaTelFax.Visible = False
    End If
    '(FIM) Demanda 18225206.

    Exit Sub

Erro:
    Call TrataErroGeral("InicializaInterfaceLocal", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub ValidaDados_PermiteSolicitacaoSaldo()
    Dim rsRecordSet         As ADODB.Recordset
    
    On Error GoTo Erro
    'Habilita o bot�o Solicita Saldo Devedor somente para os produtos parametrizados
    
    btnSolicitarSaldo.Enabled = False

    If gbldSinistro_ID <> 0 Then
        
        sSQL = "" & vbNewLine
'AKIO.OKUNO - MP - INICIO - 25/02/2013
'        sSQL = sSQL & "SELECT produto_tb.sinistro_solicita_saldo  " & vbNewLine
'        sSQL = sSQL & "  FROM evento_segbr_sinistro_atual_tb  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & "  JOIN produto_tb  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & "    ON produto_tb.produto_id = evento_segbr_sinistro_atual_tb.produto_id " & vbNewLine
'        sSQL = sSQL & "  JOIN evento_segbr_tb  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & "    ON evento_segbr_tb.evento_bb_id = evento_segbr_sinistro_atual_tb.evento_bb_id " & vbNewLine
'        sSQL = sSQL & " WHERE evento_segbr_sinistro_atual_tb.sinistro_id = " & gbldSinistro_ID & vbNewLine
'        sSQL = sSQL & "   AND evento_segbr_sinistro_atual_tb.evento_bb_id = 1110 " & vbNewLine
'        sSQL = sSQL & "   AND produto_tb.sinistro_solicita_saldo = 's' " & vbNewLine
'        sSQL = sSQL & "   AND ISNULL(evento_segbr_sinistro_atual_tb.num_recibo, 0) = 0 " & vbNewLine
'        sSQL = sSQL & "   AND NOT EXISTS (SELECT 1 FROM sinistro_solicita_saldo_tb  WITH (NOLOCK)  " & vbNewLine
'        sSQL = sSQL & "                    WHERE sinistro_solicita_saldo_tb.sinistro_id = evento_segbr_sinistro_atual_tb.sinistro_id " & vbNewLine
'        sSQL = sSQL & "                      AND sinistro_solicita_saldo_tb.situacao > 0 ) " & vbNewLine
        sSQL = sSQL & "declare @liberacao Char(1)                                                                                               " & vbNewLine
        sSQL = sSQL & "select @liberacao = 'N'                                                                                                  " & vbNewLine
        sSQL = sSQL & "if exists (select sinistro_solicita_saldo                                                                                " & vbNewLine
        sSQL = sSQL & "             from produto_tb  WITH (NOLOCK)                                                                                 " & vbNewLine
        sSQL = sSQL & "            where produto_id = " & gbllProduto_ID & "                                                                    " & vbNewLine
        sSQL = sSQL & "              and sinistro_solicita_saldo = 's')                                                                         " & vbNewLine
        sSQL = sSQL & "begin                                                                                                                    " & vbNewLine
        'sSQL = sSQL & "      if exists (select 1                                                                                                " & vbNewLine
        sSQL = sSQL & "      if not exists (select 1                                                                                                " & vbNewLine  'FLAVIO.BEZERRA - 01/03/2013
        sSQL = sSQL & "                   from sinistro_solicita_saldo_tb  WITH (NOLOCK)                                                           " & vbNewLine
        sSQL = sSQL & "                  where sinistro_solicita_saldo_tb.sinistro_id = " & gbldSinistro_ID & "                                 " & vbNewLine
        sSQL = sSQL & "                    and sinistro_solicita_saldo_tb.situacao > 0 )                                                        " & vbNewLine
        sSQL = sSQL & "      begin                                                                                                              " & vbNewLine
        sSQL = sSQL & "           select @liberacao = 'S'                                                                                       " & vbNewLine
        sSQL = sSQL & "             from evento_segbr_sinistro_atual_tb  WITH (NOLOCK)                                                             " & vbNewLine
        sSQL = sSQL & "            where evento_segbr_sinistro_atual_tb.sinistro_id = " & gbldSinistro_ID & "                                   " & vbNewLine
        sSQL = sSQL & "              and evento_segbr_sinistro_atual_tb.evento_bb_id = 1110                                                     " & vbNewLine
        sSQL = sSQL & "           and IsNull(Evento_SEGBR_Sinistro_Atual_Tb.Num_Recibo, 0 ) = 0                                                 " & vbNewLine
        sSQL = sSQL & "      end                                                                                                                " & vbNewLine
        sSQL = sSQL & "end                                                                                                                      " & vbNewLine
        sSQL = sSQL & "select @liberacao                                                                                                        " & vbNewLine
'AKIO.OKUNO - MP - FIM - 25/02/2013
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                                            , glAmbiente_id _
                                            , App.Title _
                                            , App.FileDescription _
                                            , sSQL _
                                            , lConexaoLocal _
                                            , True)
                
        
        ' Demanda:12967997 - Anderson Teixeira - 12/02/2012
        ' Nao deixar um sinistro encerrado pedir saldo
        With rsRecordSet
            If Not .EOF Then
'                If .Fields!Sinistro_Solicita_Saldo = "S" And CInt(gblsSinistro_Situacao_ID) < 2 Then   AKIO.OKUNO - MP - 25/02/2013
'                If .Fields(0) = "S" And CInt(gblsSinistro_Situacao_ID) < 2 Then    'AKIO.OKUNO - 21/03/2013
                If .Fields(0) = "S" And CInt("0" & gblsSinistro_Situacao_ID) < 2 Then
                    btnSolicitarSaldo.Enabled = True
                End If
            End If
        End With
    
        Set rsRecordSet = Nothing
        
    End If

    Exit Sub

Erro:
    Call TrataErroGeral("InicializaInterfaceLocal", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosTela_Localizacao()
    On Error GoTo Erro

    Screen.MousePointer = vbHourglass


    Select Case strTipoProcesso
    Case strTipoProcesso_Consulta, strTipoProcesso_Avaliar, strTipoProcesso_Reabrir, strTipoProcesso_Encerrar, strTipoProcesso_Avaliar_Com_Imagem
        '            bGTR_Implantado = Seleciona_GTR_Implantado(lblSinistro_ID(0))  'AKIO.OKUNO - 26/09/2012
        'bGTR_Implantado = Seleciona_GTR_Implantado(lblSinistro_ID(0), lConexaoLocal)   'AKIO.OKUNO - 23/04/2013
        bGTR_Implantado = Obtem_GTR_Implantado(lblSinistro_ID(0))
    Case strTipoProcesso_Avisar, strTipoProcesso_Avisar_Cosseguro
        Select Case bytTipoRamo
        Case bytTipoRamo_Vida
            bGTR_Implantado = False
        Case bytTipoRamo_RE
        Case bytTipoRamo_Rural
        End Select
    Case intSinistro_IncluirDetalhamento
    Case intSinistro_IncluirEstimativa
    Case intSinistro_ManutencaoBeneficiario
    Case intSinistro_ManutencaoPrestador
    Case intSinistro_ManutencaoSolicitante
    Case intSinistro_ManutencaoAviso
    End Select

    lblDescricaoLocalizacaoProcesso(0).Visible = bGTR_Implantado
    lblDescricaoLocalizacaoProcesso(1).Visible = lblDescricaoLocalizacaoProcesso(0).Visible

    If bGTR_Implantado Then
        If strTipoProcesso = strTipoProcesso_Avisar _
           Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
            lblDescricaoLocalizacaoProcesso(1) = "AG�NCIA"
            'REVER
            '       Else
            '          If Existem_Novos_Eventos(Sin) Then
            '             With frmNovosEventos
            '                 Call .Carrega_grid_Eventos(Sin)
            '                 .Show vbModal
            '             End With
            '          End If
        End If
    Else
    End If

    If bGTR_Implantado Then    'F.BEZERRA 16/10/2012
        lblDescricaoLocalizacaoProcesso(1).Tag = Seleciona_Localizacao_Processo_Local
    Else
        lblDescricaoLocalizacaoProcesso(1).Tag = 2    'Na Seguradora
    End If
    Select Case lblDescricaoLocalizacaoProcesso(1).Tag
    Case 0: lblDescricaoLocalizacaoProcesso(1) = "ERRO NA LOCALIZA��O"
    Case 1: lblDescricaoLocalizacaoProcesso(1) = "AG�NCIA"
    Case 2: lblDescricaoLocalizacaoProcesso(1) = "SEGURADORA"
    Case 3: lblDescricaoLocalizacaoProcesso(1) = "ENCERRADO"
    Case 4: lblDescricaoLocalizacaoProcesso(1) = "INDEFINIDO"
    End Select
    
    '' Laurent Silva - 06/11/2017
    '' Demanda Hiagro
    
    CarregaProtocolo
    
    '' Fim 06/11/2017
    

    Screen.MousePointer = vbNormal
    Exit Sub

Erro:
    Call TrataErroGeral("InicializaInterfaceLocal", Me.Caption)
    Call FinalizarAplicacao

End Sub


'' Laurent Silva - 06/11/2017
'' Demanda Hiagro
Private Sub CarregaProtocolo()

Dim rsRecordSet As ADODB.Recordset
Dim SQL As String
Dim protocolo As String

protocolo = ""

''SQL = "select isnull(NR_PTC_AVISO,'') as NR_PTC_AVISO from seguros_db..evento_SEGBR_sinistro_atual_tb with (nolock) "
SQL = "select sinistro_id, isnull(NR_PTC_AVISO,'') as NR_PTC_AVISO from seguros_db.dbo.sinistro_tb with (nolock) "
SQL = SQL & " where sinistro_id = " & gbldSinistro_ID
    
   
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                                            , glAmbiente_id _
                                            , App.Title _
                                            , App.FileDescription _
                                            , SQL _
                                            , lConexaoLocal _
                                            , True)
                
        
        With rsRecordSet
            If Not .EOF Then
                protocolo = .Fields(1)
            End If
        End With
    
        Set rsRecordSet = Nothing


    'petrauskas 07/08/2018
    lblProtocolo(0).Caption = protocolo
    lblProtocolo(0).Visible = (protocolo <> "")
    lblProtocolo(1).Visible = lblProtocolo(0).Visible
'    If protocolo <> "" Then
'        lblDescricaoLocalizacaoProcesso(0) = "Protocolo: "
'        lblDescricaoLocalizacaoProcesso(1) = protocolo
'        lblDescricaoLocalizacaoProcesso(0).Visible = True
'        lblDescricaoLocalizacaoProcesso(1).Visible = True
'    End If
    

End Sub

Private Sub InicializaInterface_Funcionalidades_AbaDetalhamento()
    Select Case strTipoProcesso
    Case strTipoProcesso_Consulta
        'AKIO.OKUNO - INICIO 23/09/2012
    Case strTipoProcesso_Avisar _
       , strTipoProcesso_Avisar_Cosseguro _
       , strTipoProcesso_Avaliar _
       , strTipoProcesso_Avaliar_Cosseguro _
       , strTipoProcesso_Avaliar_Com_Imagem
        'AKIO.OKUNO - INICIO 23/09/2012
        Select Case bytTipoRamo
        Case bytTipoRamo_Vida
            With btnDetalhamento_VisualizarImpressao_Cadastrar
                If Not .Visible Then
                    .Visible = True
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    InicializaInterface_Dimensao_Objeto txtDetalhamento_Visualizar, "d", 0, .Height, 0, 0
                End If
            End With
        Case bytTipoRamo_RE
            With btnDetalhamento_VisualizarImpressao_Cadastrar
                If Not .Visible Then
                    .Visible = True
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    InicializaInterface_Dimensao_Objeto txtDetalhamento_Visualizar, "d", 0, .Height, 0, 0
                End If
            End With
        Case bytTipoRamo_Rural
            With btnDetalhamento_VisualizarImpressao_Cadastrar
                If Not .Visible Then
                    .Visible = True
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    InicializaInterface_Dimensao_Objeto txtDetalhamento_Visualizar, "d", 0, .Height, 0, 0
                End If
            End With
        End Select
        ''        mathayde
        ''        Case strTipoProcesso_Reabrir, strTipoProcesso_Reabrir_Cosseguro
    Case strTipoProcesso_Reabrir, strTipoProcesso_Reabrir_Cosseguro, strTipoProcesso_Encerrar, strTipoProcesso_Encerrar_Cosseguro
        With btnDetalhamento_VisualizarImpressao_Cadastrar
            If Not .Visible Then
                .Visible = True
                .Tag = 1
                iParametroParaChamadaAuxiliar = 1
                InicializaInterface_Dimensao_Objeto txtDetalhamento_Visualizar, "d", 0, .Height, 0, 0
            End If
        End With
    End Select
End Sub

Private Sub InicializaInterface_Funcionalidades_AbaEstimativaPagamento()
    Dim bProcessa As Boolean             'AKIO.OKUNO - 24/09/2012

    Select Case strTipoProcesso
    Case strTipoProcesso_Consulta, strTipoProcesso_Consulta_Cosseguro
        'Aba: Estimativa/Pagamentos
        btnEstimativasPagamentos_Valores_Beneficiario_CadastrarProposta.Visible = False
        btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider.Visible = False

        'Frame: Beneficiarios
        With btnEstimativasPagamentos_Valores_Beneficiario_Alterar
            If Not .Visible Then
                .Visible = True
                InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Beneficiario, "D", 0, .Height, 0, 0
                .Caption = "&Consulta"
                .Tag = 3
            End If
        End With
        'Frame: Estimativas / Pagamentos
        With btnEstimativasPagamentos_Valores_Cobertura
            If Not .Visible Then
                .Visible = True
                .Caption = "&Consulta"
                .Tag = 3
                iParametroParaChamadaAuxiliar = 3
                InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Coberturas, "D", 0, .Height, 0, 0
            End If
            '.Enabled = LiberaUsoCompenente 'cristovao.rodrigues 22/08/2012
        End With

    Case strTipoProcesso_Avisar, strTipoProcesso_Avisar_Cosseguro
        bTipoProcesso_Avisar_Liberado = LenB(Trim(lblSinistro_ID(0))) <> 0  'INSERIDO - AKIO.OKUNO - 02092012

        Select Case bytTipoRamo
        Case bytTipoRamo_Vida

            btnEstimativasPagamentos_Valores_Beneficiario_CadastrarProposta.Visible = False
            'RETIRADO - AKIO.OKUNO - 02092012
            '                    With btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider
            '                        If Not .Visible Then
            '                            .Visible = True
            '                            .Tag = 1
            '                            iParametroParaChamadaAuxiliar = 1
            '                            InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Beneficiario, "D", 0, .Height, 0, 0
            '                            .Enabled = LiberaUsoCompenente 'cristovao.rodrigues 22/08/2012
            '                        End If
            '                    End With
            If sOperacaoCosseguro = "C" Then
                lblEstimativasPagamentos_Dados(9).Visible = True    'Num. da Carta
                With txtNum_Carta_Seguro_Aceito
                    .Visible = True
                    .Enabled = True
                End With
            Else
            End If
            btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider.Caption = "N&ovo"
            'Frame: Estimativas / Pagamentos
            With btnEstimativasPagamentos_Valores_Cobertura
                If Not .Visible Then
                    .Visible = True
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Coberturas, "D", 0, .Height, 0, 0
                End If
                'RETIRADO - AKIO.OKUNO - 02092012 (RETIRADO DE DENTRO DO IF NOT .VISIBLE)
                .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
            End With

            btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider.Visible = False
            'Frame: Beneficiarios
            With btnEstimativasPagamentos_Valores_Beneficiario_Alterar
                If Not .Visible Then
                    .Visible = True
                    InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Beneficiario, "D", 0, .Height, 0, 0
                    .Caption = "&Novo"
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                End If
                'RETIRADO - AKIO.OKUNO - 02092012 (RETIRADO DE DENTRO DO IF NOT .VISIBLE)
                .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
            End With


        Case bytTipoRamo_RE
            btnEstimativasPagamentos_Valores_Beneficiario_CadastrarProposta.Visible = False
            With btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider
                If Not .Visible Then
                    .Visible = True
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Beneficiario, "D", 0, .Height, 0, 0
                End If
                '.Left = 13320  'AKIO.OKUNO - 27/09/2012
                .Left = 12600  'AKIO.OKUNO - 10/10/2012
                .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
            End With
            If sOperacaoCosseguro = "C" Then
                lblEstimativasPagamentos_Dados(9).Visible = True    'Num. da Carta
                With txtNum_Carta_Seguro_Aceito
                    .Visible = True
                    .Enabled = True
                End With
            Else
            End If
            btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider.Caption = "N&ovo"
            'Frame: Estimativas / Pagamentos
            With btnEstimativasPagamentos_Valores_Cobertura
                If Not .Visible Then
                    .Visible = True
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Coberturas, "D", 0, .Height, 0, 0
                End If
                .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
            End With

            'Frame: Beneficiarios
            If Not btnEstimativasPagamentos_Valores_Beneficiario_CadastrarProposta.Visible And _
               Not btnEstimativasPagamentos_Valores_Beneficiario_Alterar.Visible Then
                '                        btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider.Left = btnEstimativasPagamentos_Valores_Beneficiario_Alterar.Left 'AKIO.OKUNO - 27/09/2012
                '                        btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider.Left = 13320  'AKIO.OKUNO - 27/09/2012
                btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider.Left = 12600  'AKIO.OKUNO - 27/09/2012
            End If

        Case bytTipoRamo_Rural
            btnEstimativasPagamentos_Valores_Beneficiario_CadastrarProposta.Visible = False
            With btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider
                If Not .Visible Then
                    .Visible = True
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Beneficiario, "D", 0, .Height, 0, 0
                End If
                '.Left = 13320  'AKIO.OKUNO - 27/09/2012
                .Left = 12600  'AKIO.OKUNO - 10/10/2012
                .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
            End With
            If sOperacaoCosseguro = "C" Then
                lblEstimativasPagamentos_Dados(9).Visible = True    'Num. da Carta
                With txtNum_Carta_Seguro_Aceito
                    .Visible = True
                    .Enabled = True
                End With
            Else
            End If
            btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider.Caption = "N&ovo"
            'Frame: Estimativas / Pagamentos
            With btnEstimativasPagamentos_Valores_Cobertura
                If Not .Visible Then
                    .Visible = True
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Coberturas, "D", 0, .Height, 0, 0
                End If
                .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
            End With

        End Select

    Case strTipoProcesso_Avaliar, strTipoProcesso_Avaliar_Cosseguro, strTipoProcesso_Reabrir, strTipoProcesso_Reabrir_Cosseguro, strTipoProcesso_Avaliar_Com_Imagem
        'AKIO.OKUNO - INICIO - 24/09/2012
        If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
            bProcessa = gblsSinistro_Situacao_ID <> 2
        Else
            bProcessa = True
        End If
        'AKIO.OKUNO - FIM - 24/09/2012

        'Inicializa com visible false
        btnEstimativasPagamentos_Valores_Beneficiario_CadastrarProposta.Visible = False
        btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider.Visible = False
        'valida ...
        Select Case bytTipoRamo
        Case bytTipoRamo_Vida
            If sOperacaoCosseguro = "C" Then
                lblEstimativasPagamentos_Dados(9).Visible = True    'Num. da Carta
                With txtNum_Carta_Seguro_Aceito
                    .Visible = True
                    .Enabled = True
                End With
            Else
            End If

            If bProcessa Then   'AKIO.OKUNO - 24/09/2012
                'Frame: Estimativas / Pagamentos
                With btnEstimativasPagamentos_Valores_Cobertura
                    If Not .Visible Then
                        .Visible = True
                        .Tag = 1
                        iParametroParaChamadaAuxiliar = 1
                        InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Coberturas, "D", 0, .Height, 0, 0
                        .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                    End If
                End With

                'Frame: Beneficiarios
                With btnEstimativasPagamentos_Valores_Beneficiario_Alterar
                    If Not .Visible Then
                        .Visible = True
                        InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Beneficiario, "D", 0, .Height, 0, 0
                        .Caption = "&Novo"
                        .Tag = 1
                        iParametroParaChamadaAuxiliar = 1
                        .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                    End If
                End With
            Else
                grdEstimativasPagamentos_Beneficiario.Enabled = False 'FLAVIO.BEZERRA - 30/01/2013
            End If  'AKIO.OKUNO - 24/09/2012
            If strTipoProcesso = strTipoProcesso_Reabrir _
               Or strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro Then
                btnEstimativasPagamentos_Valores_Beneficiario_CadastrarLider.Enabled = LiberaUsoCompenente
            End If

        Case bytTipoRamo_RE
            If sOperacaoCosseguro = "C" Then
                lblEstimativasPagamentos_Dados(9).Visible = True    'Num. da Carta
                With txtNum_Carta_Seguro_Aceito
                    .Visible = True
                    .Enabled = True
                End With
            Else
            End If

            'Frame: Estimativas / Pagamentos
            With btnEstimativasPagamentos_Valores_Cobertura
                If Not .Visible Then
                    .Visible = True
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Coberturas, "D", 0, .Height, 0, 0
                    .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                End If
            End With

            'Frame: Beneficiarios
            With btnEstimativasPagamentos_Valores_Beneficiario_Alterar
                If Not .Visible Then
                    .Visible = True
                    InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Beneficiario, "D", 0, .Height, 0, 0
                    .Caption = "&Novo"
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                End If
            End With
        Case bytTipoRamo_Rural
            If sOperacaoCosseguro = "C" Then
                lblEstimativasPagamentos_Dados(9).Visible = True    'Num. da Carta
                With txtNum_Carta_Seguro_Aceito
                    .Visible = True
                    .Enabled = True
                End With
            Else
            End If

            'Frame: Estimativas / Pagamentos
            With btnEstimativasPagamentos_Valores_Cobertura
                If Not .Visible Then
                    .Visible = True
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Coberturas, "D", 0, .Height, 0, 0
                    .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                End If
            End With

            'Frame: Beneficiarios
            With btnEstimativasPagamentos_Valores_Beneficiario_Alterar
                If Not .Visible Then
                    .Visible = True
                    InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Beneficiario, "D", 0, .Height, 0, 0
                    .Caption = "&Novo"
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                End If
            End With
        End Select
    Case strTipoProcesso_Encerrar, strTipoProcesso_Encerrar_Cosseguro       'RALVES (POR AKIO.OKUNO)
        btnEstimativasPagamentos_Valores_Beneficiario_CadastrarProposta.Visible = False
        'Frame: Estimativas / Pagamentos
        With btnEstimativasPagamentos_Valores_Cobertura
            If Not .Visible Then
                .Visible = True
                .Caption = "&Encerrar"
                .Tag = 1
                InicializaInterface_Dimensao_Objeto grdEstimativasPagamentos_Coberturas, "D", 0, .Height, 0, 0
                'AKIO.OKUNO - INICIO - 01/10/2012
                If lblSinistro_Situacao(0) = "ENCERRADO" Or lblSinistro_Situacao(0) = "ENCERRADO AG. FINANCEIRO" Then
                    .Enabled = False
                Else
                    .Enabled = True
                End If
                'AKIO.OKUNO - FIM - 01/10/2012
            End If
        End With

    End Select

End Sub
'AKIO.OKUNO InicializaInterface_Funcionalidades_Gerais_PorRamo - 23/09/2012
Private Sub InicializaInterface_Funcionalidades_Gerais_PorRamo()
    Select Case bytTipoRamo
    Case bytTipoRamo_Vida
        fmeDadosGerais_EnderecoOcorrenciaSinistro.Visible = False
        fmeDadosGerais_Sinistrado.Visible = True
        fmeDadosEspecificos_BensSinistrados.Visible = False     'AKIO.OKUNO - 30/09/2012

    Case bytTipoRamo_Rural
        fmeDadosGerais_EnderecoOcorrenciaSinistro.Visible = True    'cristovao.rodrigues 15/08/2012
        fmeDadosGerais_Sinistrado.Visible = False
        fmeDadosEspecificos_BensSinistrados.Visible = True      'AKIO.OKUNO - 30/09/2012

    Case bytTipoRamo_RE    'Case bytTipoRamo_Vida ' mathayde 26
        fmeDadosGerais_EnderecoOcorrenciaSinistro.Visible = True    'cristovao.rodrigues 15/08/2012
        fmeDadosGerais_Sinistrado.Visible = False
        fmeDadosEspecificos_BensSinistrados.Visible = True      'AKIO.OKUNO - 30/09/2012
    End Select

End Sub

Private Sub InicializaInterface_Funcionalidades_AbaDadosGerais()
    Dim bProcessa As Boolean                 'AKIO.OKUNO - 24/09/2012

    Select Case strTipoProcesso
    Case strTipoProcesso_Consulta
        Select Case bytTipoRamo
        Case bytTipoRamo_Vida
            'fmeDadosGerais_EnderecoOcorrenciaSinistro.Visible = False  'AKIO.OKUNO - 23/09/2012
            'fmeDadosGerais_Sinistrado.Visible = True                                   'AKIO.OKUNO - 23/09/2012

            'Frame: Aviso
            With btnDadosGerais_Aviso_Cadastrar
                .Visible = True
                .Tag = 3
                .Caption = "&Consultar"
                InicializaInterface_Dimensao_Objeto grdDadosGerais_Aviso, "D", .Width, 0, 0, 0
                iParametroParaChamadaAuxiliar = .Tag
            End With

            '                    'Frame: Sinistrado
            '                    With btnDadosGerais_Sinistrado_Cadastrar
            '                        .Visible = True
            '                        .Tag = 3
            '                        .Caption = "C&onsultar"
            '                        InicializaInterface_Dimensao_Objeto fmeDadosGerais_Sinistrado, "A", 0, .Height, 0, 0
            '                    End With

            'Frame: Solicitante
            With btnDadosGerais_Solicitante_Cadastrar
                .Visible = True
                .Tag = 3
                .Caption = "Co&nsultar"
                'InicializaInterface_Dimensao_Objeto txtSolicitante_Municipio_Nome, "D", .Width, 0, 0, 0
                iParametroParaChamadaAuxiliar = .Tag
            End With
            '                    With fmeDadosGerais_Solicitante
            '                        .Top = .Top + fmeDadosGerais_Sinistrado.Height ' btnDadosGerais_Sinistrado_Cadastrar.Height 'cristovao.rodrigues
            '                    End With

        Case bytTipoRamo_RE
            With fmeDadosGerais_EnderecoOcorrenciaSinistro
                .Visible = True
                .Enabled = False
            End With
            fmeDadosGerais_Sinistrado.Visible = False

            'Frame: Aviso
            With btnDadosGerais_Aviso_Cadastrar
                .Visible = True
                .Tag = 3
                .Caption = "&Consultar"
                InicializaInterface_Dimensao_Objeto grdDadosGerais_Aviso, "D", .Width, 0, 0, 0
                iParametroParaChamadaAuxiliar = .Tag
            End With

            'Frame: Solicitante
            With btnDadosGerais_Solicitante_Cadastrar
                .Visible = True
                .Tag = 3
                .Caption = "Co&nsultar"
                'InicializaInterface_Dimensao_Objeto txtSolicitante_Municipio_Nome, "D", .Width, 0, 0, 0
                iParametroParaChamadaAuxiliar = .Tag
            End With
        Case bytTipoRamo_Rural
            'fmeDadosGerais_EnderecoOcorrenciaSinistro.Visible = True 'cristovao.rodrigues 15/08/2012   'AKIO.OKUNO - 23/09/2012
            'fmeDadosGerais_Sinistrado.Visible = False                                                                                                  'AKIO.OKUNO - 23/09/2012

            'Frame: Aviso
            With btnDadosGerais_Aviso_Cadastrar
                .Visible = True
                .Tag = 3
                .Caption = "&Consultar"
                InicializaInterface_Dimensao_Objeto grdDadosGerais_Aviso, "D", .Width, 0, 0, 0
                iParametroParaChamadaAuxiliar = .Tag
            End With

            'Frame: Solicitante
            With btnDadosGerais_Solicitante_Cadastrar
                .Visible = True
                .Tag = 3
                .Caption = "Co&nsultar"
                'InicializaInterface_Dimensao_Objeto txtSolicitante_Municipio_Nome, "D", .Width, 0, 0, 0
                iParametroParaChamadaAuxiliar = .Tag
            End With

        End Select
    Case strTipoProcesso_Avisar, strTipoProcesso_Avisar_Cosseguro
        Select Case bytTipoRamo
        Case bytTipoRamo_Vida
            btnSolicitante_Sinistro_Telefone_Consulta.Enabled = LenB(Trim(lblSinistro_ID(0))) <> 0
            'Aba: Dados Gerais
            btnEstimativasPagamentos_Valores_Beneficiario_CadastrarProposta.Visible = False    'RALVES
            If sOperacaoCosseguro = "C" Then    'RALVES
                With btnDadosGerais_Aviso_Cadastrar
                    If Not .Visible Then
                        .Visible = True
                        .Tag = 1
                        InicializaInterface_Dimensao_Objeto grdDadosGerais_Aviso, "D", .Width, 0, 0, 0
                        iParametroParaChamadaAuxiliar = .Tag
                        .Enabled = True    'RALVES
                    End If
                End With
                grdDadosGerais_Aviso.Enabled = False
                fmeDadosGerais_Solicitante.Enabled = False
            Else
                '                        btnEstimativasPagamentos_Valores_Beneficiario_CadastrarProposta.Visible = False    'RALVES
                '                    End If                                                                                 'RALVES

                'Frame: Aviso
                '                    LimpaCamposTelaLocal_AbaDadosGerais_FrameAviso

                With btnDadosGerais_Aviso_Cadastrar
                    If Not .Visible Then
                        .Visible = True
                        .Tag = 1
                        InicializaInterface_Dimensao_Objeto grdDadosGerais_Aviso, "d", .Width, 0, 0, 0
                        iParametroParaChamadaAuxiliar = .Tag
                        .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                    End If
                End With

                'Frame: Solicitante
                '                    LimpaCamposTelaLocal_AbaDadosGerais_FrameSolicitante

                With btnDadosGerais_Solicitante_Cadastrar
                    If Not .Visible Then
                        .Visible = True
                        .Tag = 1
                        'mathayde
                        'erro
                        'InicializaInterface_Dimensao_Objeto txtSolicitante_Municipio_Nome, "D", .Width, 0, 0, 0
                        txtSolicitante_Municipio_Nome.Width = 1600
                        iParametroParaChamadaAuxiliar = .Tag
                        .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 23/08/2012
                    End If
                End With

                '                    'Frame: Sinistrado
                '                    With btnDadosGerais_Sinistrado_Cadastrar
                '                        .Visible = True
                '                        .Tag = 1
                '                        InicializaInterface_Dimensao_Objeto fmeDadosGerais_Sinistrado, "A", 0, .Height, 0, 0
                '                    End With

                'Frame: Solicitante
                '                    With fmeDadosGerais_Solicitante
                '                        .Top = .Top + btnDadosGerais_Sinistrado_Cadastrar.Height
                '                    End With
                btnDadosGerais_Solicitante_Cadastrar.Enabled = True
            End If  'RALVES
            'SUBSTITUIDO - AKIO.OKUNO - 02092012
            '                    btnSolicitante_Sinistro_Telefone_Consulta.Enabled = False
            btnSolicitante_Sinistro_Telefone_Consulta.Enabled = LenB(Trim(lblSinistro_ID(0))) <> 0

        Case bytTipoRamo_RE
            'Aba: Dados Gerais
            If sOperacaoCosseguro = "C" Then    'RALVES
                With btnDadosGerais_Aviso_Cadastrar
                    If Not .Visible Then
                        .Visible = True
                        .Tag = 1
                        InicializaInterface_Dimensao_Objeto grdDadosGerais_Aviso, "D", .Width, 0, 0, 0
                        iParametroParaChamadaAuxiliar = .Tag
                        .Enabled = True    'RALVES
                    End If
                End With
                grdDadosGerais_Aviso.Enabled = False
                fmeDadosGerais_Solicitante.Enabled = False
            Else

                With fmeDadosGerais_EnderecoOcorrenciaSinistro
                    .Visible = True
                    .Enabled = False
                End With
                fmeDadosGerais_Sinistrado.Visible = False

                With btnDadosGerais_Aviso_Cadastrar
                    If Not .Visible Then
                        .Visible = True
                        .Tag = 1
                        InicializaInterface_Dimensao_Objeto grdDadosGerais_Aviso, "d", .Width, 0, 0, 0
                        iParametroParaChamadaAuxiliar = .Tag
                        .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                    End If
                End With

                With btnDadosGerais_Solicitante_Cadastrar
                    If Not .Visible Then
                        .Visible = True
                        .Tag = 1
                        'InicializaInterface_Dimensao_Objeto txtSolicitante_Municipio_Nome, "D", .Width, 0, 0, 0
                        iParametroParaChamadaAuxiliar = .Tag
                        .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 23/08/2012
                    End If
                End With

                btnDadosGerais_Solicitante_Cadastrar.Enabled = True
                btnSolicitante_Sinistro_Telefone_Consulta.Enabled = LenB(Trim(lblSinistro_ID(0))) <> 0
            End If
        Case bytTipoRamo_Rural
            'fmeDadosGerais_EnderecoOcorrenciaSinistro.Visible = True 'cristovao.rodrigues 15/08/2012   'AKIO.OKUNO - 23/09/2012
            'fmeDadosGerais_Sinistrado.Visible = False                                                                                                  'AKIO.OKUNO - 23/09/2012

            'Frame: Aviso
            'MATHAYDE
            If Not ((strTipoProcesso = strTipoProcesso_Avisar Or bytModoOperacao = strTipoProcesso_Avisar_Cosseguro) And sAuxiliarExecutado = "Aviso") Then
                LimpaCamposTelaLocal_AbaDadosGerais_FrameAviso
            End If

            With btnDadosGerais_Aviso_Cadastrar
                .Visible = True
                .Tag = 1
                InicializaInterface_Dimensao_Objeto grdDadosGerais_Aviso, "d", .Width, 0, 0, 0
                iParametroParaChamadaAuxiliar = .Tag
                .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
            End With

            'Frame: Solicitante
            'MATHAYDE
            If Not ((strTipoProcesso = strTipoProcesso_Avisar Or bytModoOperacao = strTipoProcesso_Avisar_Cosseguro) And sAuxiliarExecutado = "Aviso") Then
                LimpaCamposTelaLocal_AbaDadosGerais_FrameSolicitante
            End If


            With btnDadosGerais_Solicitante_Cadastrar
                .Visible = True
                .Tag = 1
                'InicializaInterface_Dimensao_Objeto txtSolicitante_Municipio_Nome, "D", .Width, 0, 0, 0
                iParametroParaChamadaAuxiliar = .Tag
                .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 23/08/2012
            End With

            btnDadosGerais_Solicitante_Cadastrar.Enabled = True
            btnSolicitante_Sinistro_Telefone_Consulta.Enabled = False
        End Select

    Case strTipoProcesso_Avaliar, strTipoProcesso_Avaliar_Com_Imagem
        'AKIO.OKUNO - INICIO - 24/09/2012
        If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
            bProcessa = gblsSinistro_Situacao_ID <> 2
            '----------------------------------------------------------------------
            'Ricardo Toledo (Confitec) : IM00414242 : 12/07/2018 : INI
            'Ocultar bot�o de vistoria quando o tipo de ramo for diferente de Rural
            ''gbllProduto_ID = 1220 Or gbllProduto_ID = 1221 Or gbllProduto_ID = 1222 Or gbllProduto_ID = 1223 Or gbllProduto_ID = 1224
            If bytTipoRamo = 3 Or getProdutoRamoInspecao Then
                InicializaInterface_Dimensao_Objeto grdDadosGerais_Aviso, "D", cmdVistoria.Width, 0, 0, 0
               cmdVistoria.Visible = True
            Else
                cmdVistoria.Visible = False
            End If
            'Ricardo Toledo (Confitec) : IM00414242 : 12/07/2018 : FIM
            '----------------------------------------------------------------------
        Else
            bProcessa = True
        End If
        'AKIO.OKUNO - FIM - 24/09/2012

        If bProcessa Then   'AKIO.OKUNO - 24/09/2012
            With btnDadosGerais_Aviso_Cadastrar
                If .Visible = False Then    'AKIO.OKUNO - 23/09/2012
                    .Visible = True
                    .Caption = "&Alterar"
                    .Tag = 2
                    '----------------------------------------------------------------------
                    'Ricardo Toledo (Confitec) : IM00414242 : 12/07/2018 : INI
                    'S� executa se o ramo for diferente de "Rural (corre��o bot�o vistoria)
                    If bytTipoRamo <> 3 Then
                        InicializaInterface_Dimensao_Objeto grdDadosGerais_Aviso, "D", .Width, 0, 0, 0
                    End If
                    'Ricardo Toledo (Confitec) : IM00414242 : 12/07/2018 : FIM
                    '----------------------------------------------------------------------
                    iParametroParaChamadaAuxiliar = .Tag
                    .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                End If  'AKIO.OKUNO - 23/09/2012
            End With
            With btnDadosGerais_Solicitante_Cadastrar
                'AKIO.OKUNO - INICIO - 23/09/2012
                .Visible = True
                .Caption = "&Alterar"
                .Tag = 2
                'mathayde
                'exibi erro ao carregar novamente apos a alteracao do aviso
                'existe a necessidade de configurar a tela novamente, ja que nao temos caso de alteracao de layout no AVISAR?
                'InicializaInterface_Dimensao_Objeto txtSolicitante_Municipio_Nome, "D", .Width, 0, 0, 0
                '???? corrigir abaixo
                txtSolicitante_Municipio_Nome.Width = 1600
                bParametroParaChamadaAuxiliar = .Tag
                .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                'AKIO.OKUNO - FIM - 23/09/2012


            End With
        Else
            btnSolicitante_Sinistro_Telefone_Consulta.Enabled = False 'FLAVIO.BEZERRA 30/01/2013
        End If  'AKIO.OKUNO - FIM - 24/09/2012
    Case strTipoProcesso_Reabrir
        With btnDadosGerais_Aviso_Cadastrar
            .Visible = True
            .Caption = "&Alterar"
            .Tag = 2
            InicializaInterface_Dimensao_Objeto grdDadosGerais_Aviso, "d", .Width, 0, 0, 0
            iParametroParaChamadaAuxiliar = .Tag
            .Enabled = LiberaUsoCompenente
        End With

        With btnDadosGerais_Solicitante_Cadastrar
            .Visible = False
            .Caption = "&Alterar"
            .Tag = 2
            'mathayde
            '???? apresenta erro no reabrir sinistro 93201111039
            'InicializaInterface_Dimensao_Objeto txtSolicitante_Municipio_Nome, "D", .Width, 0, 0, 0
            iParametroParaChamadaAuxiliar = .Tag
        End With

        Select Case bytTipoRamo
        Case bytTipoRamo_Vida
        Case bytTipoRamo_RE
            fmeDadosGerais_EnderecoOcorrenciaSinistro.Visible = True    'cristovao.rodrigues 15/08/2012
            fmeDadosGerais_Sinistrado.Visible = False



        Case bytTipoRamo_Rural
            fmeDadosGerais_EnderecoOcorrenciaSinistro.Visible = True    'cristovao.rodrigues 15/08/2012
            fmeDadosGerais_Sinistrado.Visible = False
        End Select

    Case strTipoProcesso_Consulta_Cosseguro, strTipoProcesso_Avaliar_Cosseguro, strTipoProcesso_Reabrir_Cosseguro
        fmeDadosGerais_Sinistrado.Enabled = False
        grdDadosGerais_Aviso.Visible = False
        fmeDadosGerais_Solicitante.Visible = False
        fmeDadosGerais_Cosseguro.Visible = True
    End Select

End Sub

Private Function getProdutoRamoInspecao() As Boolean
    On Error GoTo Trata_Erro
    
    getProdutoRamoInspecao = False
    
    If Len(gbllProduto_ID) = 0 Or Len(gbllRamo_ID) = 0 Then Exit Function
      
    
    Dim rsRecordSet As Recordset
    Dim sSQL As String
      
    sSQL = "SELECT COUNT(1) " & vbNewLine
    sSQL = sSQL & "FROM seguros_db.dbo.produto_ramo_inspecao_tb WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "WHERE produto_id = " & gbllProduto_ID & " " & vbNewLine
    sSQL = sSQL & "AND ramo_id = " & gbllRamo_ID
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                                        , glAmbiente_id _
                                        , App.Title _
                                        , App.FileDescription _
                                        , sSQL _
                                        , lConexaoLocal _
                                        , True)

    If rsRecordSet(0) <> 0 Then
          getProdutoRamoInspecao = True
    End If
    
    Set rsRecordSet = Nothing
                              
    Exit Function
    
Trata_Erro:
     MousePointer = 0
     mensagem_erro 6, vMsg
     Unload Me
     
End Function


Private Sub LimpaCamposTelaLocal_AbaDadosGerais_FrameAviso()


    txtEvento_Sinistro_Nome = ""
    txtSubEvento_Sinistro_Nome = ""
    txtDt_Ocorrencia_Sinistro = ""
    txtDt_Entrada_Seguradora = ""
    txtDt_Aviso_Sinistro = ""
    txtDt_Inclusao = Format(Data_Sistema, "dd/mm/yyyy")

    LimpaGrid grdDadosGerais_Aviso

End Sub


Private Sub LimpaCamposTelaLocal_AbaDadosGerais_FrameSolicitante()

    lSolicitante_ID = 0
    txtSolicitante_Sinistro_Nome = ""
    txtSolicitante_Sinistro_DDD1 = ""
    txtSolicitante_Sinistro_Telefone1 = ""
    txtSolicitante_Sinistro_DDD_Fax = ""
    txtSolicitante_Sinistro_Telefone_Fax = ""
    txtSolicitante_Sinistro_Endereco = ""
    txtSolicitante_Sinistro_Bairro = ""
    txtSolicitante_Sinistro_Estado = ""
    txtSolicitante_Municipio_Nome = ""

    txtSolicitante_Sinistro_CEP = ""
    txtSolicitante_Sinistro_EMail = ""

    '(INI) Demanda 18225206.
    txtVidaSolicitante_Sinistro_Parentesco = ""
    txtVidaSolicitante_Sinistro_DDD1 = ""
    txtVidaSolicitante_Sinistro_Telefone1 = ""
    txtVidaSolicitante_Sinistro_DDD_Fax = ""
    txtVidaSolicitante_Sinistro_Telefone_Fax = ""
    '(FIM) Demanda 18225206.
End Sub

Private Sub InicializaInterface_Funcionalidades_Form()
    Dim bIndice As Byte
    Dim sTipoProcesso_Descricao As String

    'Ocultando campo com Prestador_ID
    grdDadosEspecificos_Prestador.ColWidth(9) = 0

    'Ocultando campo Regulador_ID
    grdDadosEspecificos_Regulador.ColWidth(12) = 0

    txtRetornoAuxiliar.Visible = False    'cristovao.rodrigues 24/08/2012

    Select Case strTipoProcesso
    Case strTipoProcesso_Consulta, strTipoProcesso_Consulta_Cosseguro
        sTipoProcesso_Descricao = "Consultar"

        BtnAplicar.Visible = False
        BtnCancelar.Caption = "&Sair da consulta"

        For bIndice = 0 To 1
            lblSinistro_ID(bIndice).Visible = True
        Next

        For bIndice = 0 To 1
            lblSinistro_Situacao(bIndice).Visible = True
        Next

    Case strTipoProcesso_Avisar, strTipoProcesso_Avisar_Cosseguro
        sTipoProcesso_Descricao = "Avisar"

        BtnAplicar.Visible = False
        BtnCancelar.Caption = "&Sair do aviso"

        For bIndice = 0 To 1
            lblSinistro_ID(bIndice).Visible = LenB(Trim(lblSinistro_ID(0))) <> 0
        Next

        For bIndice = 0 To 1
            lblSinistro_Situacao(bIndice).Visible = LenB(Trim(lblSinistro_ID(0))) <> 0
        Next

        For bIndice = 0 To 1
            lblDescricaoLocalizacaoProcesso(bIndice).Visible = LenB(Trim(lblSinistro_ID(0))) <> 0
        Next

        'cristovao.rodrigues 23/08/2012
        tabPrincipal.TabEnabled(1) = LenB(Trim(lblSinistro_ID(0))) <> 0
        tabPrincipal.TabEnabled(2) = LenB(Trim(lblSinistro_ID(0))) <> 0
        tabPrincipal.TabEnabled(3) = LenB(Trim(lblSinistro_ID(0))) <> 0
        tabPrincipal.TabEnabled(4) = LenB(Trim(lblSinistro_ID(0))) <> 0
        tabPrincipal.TabEnabled(5) = LenB(Trim(lblSinistro_ID(0))) <> 0

    Case strTipoProcesso_Avaliar, strTipoProcesso_Avaliar_Cosseguro
        sTipoProcesso_Descricao = "Avaliar"
        BtnCancelar.Caption = "&Sair da avalia��o"
        ValidaDados_AnaliseTecnica

    Case strTipoProcesso_Reabrir, strTipoProcesso_Reabrir_Cosseguro
        sTipoProcesso_Descricao = "Reabrir"
        With BtnAplicar
            .Caption = "&Aplicar reabertura "
            .Visible = True
        End With
        BtnCancelar.Caption = "&Sair da reabertura"

        BtnAplicar.Enabled = IIf(sSinistro_Situacao_ID = 2, True, False)    'cristovao.rodrigues 22/08/2012


    Case strTipoProcesso_Encerrar
        sTipoProcesso_Descricao = "Encerrar "
        tabPrincipal.Tab = 1    'MATHAYDE (POR AKIO.OKUNO - 27/09/2012)
        BtnAplicar.Visible = False
        BtnCancelar.Caption = "&Sair do encerramento"
        'AKIO.OKUNO - INICIO - 23/09/2012
    Case strTipoProcesso_Avaliar_Com_Imagem
        sTipoProcesso_Descricao = "Avaliar "
        BtnCancelar.Caption = "&Sair da avalia��o"
        'AKIO.OKUNO - FIM - 23/09/2012
    End Select

    If sOperacaoCosseguro = "C" Then
        Me.Caption = App.EXEName & " - " & sTipoProcesso_Descricao & " Sinistro Cosseguro - " & Ambiente & " - " & sSegmentoCliente
    Else
        Me.Caption = App.EXEName & " - " & sTipoProcesso_Descricao & " Sinistro - " & Ambiente & " - " & sSegmentoCliente
    End If

    Me.Caption = Me.Caption & " (Ramo: " & IIf(bytTipoRamo = bytTipoRamo_Vida, "Vida", IIf(bytTipoRamo = bytTipoRamo_RE, "RE", "Rural")) & ")"


End Sub

Private Sub InicializaInterface_Funcionalidades_AbaHistorico()

    Select Case strTipoProcesso
    Case strTipoProcesso_Consulta

    Case strTipoProcesso_Avisar, strTipoProcesso_Avisar_Cosseguro
        Select Case bytTipoRamo
        Case bytTipoRamo_Vida
            '                    tabPrincipal.TabVisible(2) = False  'Historico
        Case bytTipoRamo_RE
        Case bytTipoRamo_Rural
        End Select
    End Select

End Sub

Private Sub InicializaInterface_Funcionalidades()

    txtCountEstimativa.Enabled = False

    InicializaInterface_Funcionalidades_Form

    InicializaInterface_Funcionalidades_Gerais_PorRamo  'AKIO.OKUNO - 23/09/2012

    InicializaInterface_Funcionalidades_AbaDadosGerais

    InicializaInterface_Funcionalidades_AbaDetalhamento

    InicializaInterface_Funcionalidades_AbaHistorico

    InicializaInterface_Funcionalidades_AbaEstimativaPagamento

    InicializaInterface_Funcionalidades_AbaDadosEspecificos
    
    InicializaInterface_Funcionalidades_AbaCausasEEventos 'cristovao.rodrigues 22/10/2012
    
    InicializaInterface_Funcionalidades_AbaPLD          'AKIO.OKUNO - 23/01/2013
        
        InicializaInterface_Funcionalidades_AbaAgravamento  'Wilder - agravamento pecu�rio - 06/09/2018

    InicializaInterface_Funcionalidade_Encerramento

End Sub



Private Sub InicializaInterface_Funcionalidades_AbaDadosEspecificos()
    Dim bIndice As Byte
    Dim bProcessa As Boolean             'AKIO.OKUNO - 24/09/2012

    'AKIO.OKUNO - INICIO - 24/09/2012
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        bProcessa = gblsSinistro_Situacao_ID <> 2
    Else
        bProcessa = True
    End If
    'AKIO.OKUNO - FIM - 24/09/2012

    Select Case strTipoProcesso
    Case strTipoProcesso_Avisar _
       , strTipoProcesso_Avisar_Cosseguro _
       , strTipoProcesso_Consulta _
       , strTipoProcesso_Avaliar _
       , strTipoProcesso_Avaliar_Com_Imagem    'AKIO.OKUNO - 23/09/2012

        Select Case bytTipoRamo
        Case bytTipoRamo_Vida
            'Frame: A��o Judicial
            btnDadosEspecificos_AcaoJudicial_Operacao.Visible = True
            With btnDadosEspecificos_AcaoJudicial_Operacao
                If Not .Visible Then
                    .Visible = True
                    If strTipoProcesso = strTipoProcesso_Consulta Then
                        .Caption = "&Consulta"
                        .Tag = 3
                    Else
                        .Caption = "&Novo"
                        .Tag = 1
                    End If
                    InicializaInterface_Dimensao_Objeto grdDadosEspecificos_AcaoJudicial, "d", 0, .Height, 0, 0
                End If
                'RETIRADO DE DENTRO DO IF NOT .VISIBLE
                'mathayde
                If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Or (sSinistro_Situacao_ID = 2 Or sSinistro_Situacao_ID = 6) Then
                    bTipoProcesso_Avisar_Liberado = LenB(Trim(lblSinistro_ID(0))) <> 0
                    .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012      'REABILITADO - AKIO.OKUNO - 02092012
                End If

                'MATHAYDE 26 - REVISAR
                If (strTipoProcesso = strTipoProcesso_Avaliar Or strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem) And (sSinistro_Situacao_ID = 2 Or sSinistro_Situacao_ID = 6) Then
                    .Enabled = True
                End If

            End With

            If bProcessa Then   'AKIO.OKUNO - 24/09/2012
                'Frame: Prestadores
                'mathayde
                'If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Cosseguro Then
                With btnDadosEspecificos_Prestadores_Operacao
                    If Not .Visible Then
                        .Visible = True
                        If strTipoProcesso = strTipoProcesso_Consulta Then
                            .Caption = "&Consulta"
                            .Tag = 3
                        Else
                            .Caption = "&Novo"
                            .Tag = 1
                        End If
                        InicializaInterface_Dimensao_Objeto grdDadosEspecificos_Prestador, "d", 0, .Height, 0, 0
                    End If
                    'RETIRADO DE DENTRO DO IF NOT .VISIBLE
                    'mathayde
                    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Or (sSinistro_Situacao_ID = 2 Or sSinistro_Situacao_ID = 6) Then
                        bTipoProcesso_Avisar_Liberado = LenB(Trim(lblSinistro_ID(0))) <> 0
                        .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                    End If
                End With
                'End If
                'cristovao.rodrigues so ativa botao de consulta regulador se strTipoProcesso <> strTipoProcesso_Consulta ???
                'Frame: Reguladores
                '                 If strTipoProcesso <> strTipoProcesso_Consulta And strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
                'mathayde
                'If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Cosseguro Then
                If Not btnDadosEspecificos_Reguladores_Operacao(0).Visible Then
                    InicializaInterface_Dimensao_Objeto grdDadosEspecificos_Regulador, "d", 0, btnDadosEspecificos_Reguladores_Operacao(0).Height, 0, 0
                End If
                For bIndice = 0 To 1
                    If bIndice = 0 Then
                        With btnDadosEspecificos_Reguladores_Operacao(bIndice)
                            If Not .Visible Then
                                .Visible = True
                                If strTipoProcesso = strTipoProcesso_Consulta Then
                                    .Caption = "&Consulta"
                                    .Tag = 3
                                    '.Enabled = LiberaUsoCompenente
                                    iParametroParaChamadaAuxiliar = 3
                                Else
                                    'AKIO.OKUNO - INICIO - 01/10/2012
                                    If strTipoProcesso <> strTipoProcesso_Avaliar And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem And strTipoProcesso <> strTipoProcesso_Avaliar_Cosseguro Then
                                        bTipoProcesso_Avisar_Liberado = LenB(Trim(lblSinistro_ID(0))) <> 0
                                    End If
                                    'AKIO.OKUNO - FIM - 01/10/2012
                                    .Caption = "&Novo"
                                    .Tag = 1
                                End If
                            End If
                            'RETIRADO DE DENTRO DO IF NOT .VISIBLE
                            If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Or (sSinistro_Situacao_ID = 2 Or sSinistro_Situacao_ID = 6) Then
                                bTipoProcesso_Avisar_Liberado = LenB(Trim(lblSinistro_ID(0))) <> 0
                                .Enabled = LiberaUsoCompenente    '
                                iParametroParaChamadaAuxiliar = 1
                            End If
                        End With
                    End If
                    'AKIO.OKUNO - INSERIDO PARA VERS�O PROVISORIA
                    btnDadosEspecificos_Reguladores_Operacao(1).Enabled = False
                Next
                'RETIRADO - AKIO.OKUNO 02092012
                '                        InicializaInterface_Dimensao_Objeto grdDadosEspecificos_Regulador, "d", 0, btnDadosEspecificos_Reguladores_Operacao(0).Height, 0, 0
                '                    End If
            Else
                grdDadosEspecificos_Prestador.Enabled = False 'FLAVIO.BEZERRA - 30/01/2013
                grdDadosEspecificos_Regulador.Enabled = False 'FLAVIO.BEZERRA - 30/01/2013
            End If  'AKIO.OKUNO - 24/09/2012
            'Frame: Reguladores
            With fmeDadosEspecificos_Reguladores
                .Visible = (strTipoProcesso = strTipoProcesso_Consulta Or _
                            strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Or _
                            strTipoProcesso = strTipoProcesso_Avaliar Or _
                            strTipoProcesso = strTipoProcesso_Avaliar_Cosseguro Or _
                            strTipoProcesso = strTipoProcesso_Avisar Or _
                            strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Or _
                            strTipoProcesso = strTipoProcesso_Reabrir Or _
                            strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro Or _
                            strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem)       'AKIO.OKUNO - 23/09/2012
            End With

            '                     'cristovao.rodrigues 23/08/2012
            '                     With btnDadosEspecificos_Reguladores_Operacao
            '                        .Visible = True
            '                        If strTipoProcesso = strTipoProcesso_Consulta Then
            '                            .Caption = "&Consulta"
            '                            .Tag = 3
            '                        Else
            '                            .Caption = "&Novo"
            '                            .Tag = 1
            '                            .Enabled = LiberaUsoCompenente
            '                        End If
            '                        InicializaInterface_Dimensao_Objeto grdDadosEspecificos_Regulador, "d", 0, .Height, 0, 0
            '                    End With


            'RETIRADO - AKIO.OKUNO - 02092012 (RAMO VIDA SEM BENS SINISTRADOS)
            fmeDadosEspecificos_BensSinistrados.Visible = False             'INSERIDO
            '                    'Frame: Bens Sinistrados ' cristovao.rodrigues 15/08/2012
            '                    With btnDadosEspecificos_BensSinistrados_Operacao
            '                        If Not .Visible Then
            '                            .Visible = True
            '                            If strTipoProcesso = strTipoProcesso_Consulta Then
            '                                .Caption = "&Consulta"
            '                                .Tag = 3
            '                            Else
            '                                .Caption = "&Novo"
            '                                .Tag = 1
            '                                .Enabled = LiberaUsoCompenente 'cristovao.rodrigues 22/08/2012
            '                            End If
            '                            InicializaInterface_Dimensao_Objeto grdDadosEspecificos_BensSinistrados, "d", 0, .Height, 0, 0
            '                        End If
            '                    End With
            'FIM-RETIRADO - AKIO.OKUNO - 02092012

        Case bytTipoRamo_RE
            'Frame: A��o Judicial
            btnDadosEspecificos_AcaoJudicial_Operacao.Visible = True
            With btnDadosEspecificos_AcaoJudicial_Operacao
                If Not .Visible Then
                    .Visible = True
                    If strTipoProcesso = strTipoProcesso_Consulta Then
                        .Caption = "&Consulta"
                        .Tag = 3
                    Else
                        .Caption = "&Novo"
                        .Tag = 1
                    End If
                    InicializaInterface_Dimensao_Objeto grdDadosEspecificos_AcaoJudicial, "d", 0, .Height, 0, 0
                End If
                If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Or (sSinistro_Situacao_ID = 2 Or sSinistro_Situacao_ID = 6) Then
                    bTipoProcesso_Avisar_Liberado = LenB(Trim(lblSinistro_ID(0))) <> 0
                    .Enabled = LiberaUsoCompenente
                End If
            End With

            If bProcessa Then   'AKIO.OKUNO - 24/09/2012
                'Frame: Prestadores
                With btnDadosEspecificos_Prestadores_Operacao
                    If Not .Visible Then
                        .Visible = True
                        If strTipoProcesso = strTipoProcesso_Consulta Then
                            .Caption = "&Consulta"
                            .Tag = 3
                        Else
                            .Caption = "&Novo"
                            .Tag = 1
                        End If
                        InicializaInterface_Dimensao_Objeto grdDadosEspecificos_Prestador, "d", 0, .Height, 0, 0
                    End If
                    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Or (sSinistro_Situacao_ID = 2 Or sSinistro_Situacao_ID = 6) Then
                        bTipoProcesso_Avisar_Liberado = LenB(Trim(lblSinistro_ID(0))) <> 0
                        .Enabled = LiberaUsoCompenente
                    End If
                End With

                'Frame: Reguladores
                'If strTipoProcesso <> strTipoProcesso_Consulta And strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
                If Not btnDadosEspecificos_Reguladores_Operacao(0).Visible Then
                    InicializaInterface_Dimensao_Objeto grdDadosEspecificos_Regulador, "d", 0, btnDadosEspecificos_Reguladores_Operacao(0).Height, 0, 0
                End If
                For bIndice = 0 To 1
                    If bIndice = 0 Then
                        With btnDadosEspecificos_Reguladores_Operacao(bIndice)
                            If Not .Visible Then
                                .Visible = True
                                If strTipoProcesso = strTipoProcesso_Consulta Then
                                    .Caption = "&Consulta"
                                    .Tag = 3
                                Else
                                    .Caption = "&Novo"
                                    .Tag = 1
                                End If
                            End If
                            If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Or (sSinistro_Situacao_ID = 2 Or sSinistro_Situacao_ID = 6) Then
                                bTipoProcesso_Avisar_Liberado = LenB(Trim(lblSinistro_ID(0))) <> 0
                                .Enabled = LiberaUsoCompenente
                            End If
                        End With
                    End If
                    'AKIO.OKUNO - INSERIDO PARA VERS�O PROVISORIA
                    btnDadosEspecificos_Reguladores_Operacao(1).Enabled = False
                Next

                'End If

                'Frame: Reguladores
                With fmeDadosEspecificos_Reguladores
                    .Visible = (strTipoProcesso = strTipoProcesso_Consulta Or _
                                strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Or _
                                strTipoProcesso = strTipoProcesso_Avaliar Or _
                                strTipoProcesso = strTipoProcesso_Avaliar_Cosseguro Or _
                                strTipoProcesso = strTipoProcesso_Avisar Or _
                                strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Or _
                                strTipoProcesso = strTipoProcesso_Reabrir Or _
                                strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro Or _
                                strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem)  'MATHAYDE - 26
                End With

                'Frame: Bens Sinistrados ' cristovao.rodrigues 15/08/2012
                With btnDadosEspecificos_BensSinistrados_Operacao
                    If Not .Visible Then
                        .Visible = True
                        If strTipoProcesso = strTipoProcesso_Consulta Then
                            .Caption = "&Consulta"
                            .Tag = 3
                        Else
                            .Caption = "&Novo"
                            .Tag = 1
                        End If
                        InicializaInterface_Dimensao_Objeto grdDadosEspecificos_BensSinistrados, "d", 0, .Height, 0, 0
                    End If
                    If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Or (sSinistro_Situacao_ID = 2 Or sSinistro_Situacao_ID = 6) Then
                        bTipoProcesso_Avisar_Liberado = LenB(Trim(lblSinistro_ID(0))) <> 0
                        .Enabled = LiberaUsoCompenente
                    End If
                End With

            End If  'AKIO.OKUNO - 24/09/2012

        Case bytTipoRamo_Rural
            'Frame: A��o Judicial
            btnDadosEspecificos_AcaoJudicial_Operacao.Visible = True
            With btnDadosEspecificos_AcaoJudicial_Operacao
                If Not .Visible Then
                    .Visible = True
                    If strTipoProcesso = strTipoProcesso_Consulta Then
                        .Caption = "&Consulta"
                        .Tag = 3
                    Else
                        .Caption = "&Novo"
                        .Tag = 1
                        '.Enabled = LiberaUsoCompenente 'cristovao.rodrigues 22/08/2012
                    End If
                    InicializaInterface_Dimensao_Objeto grdDadosEspecificos_AcaoJudicial, "d", 0, .Height, 0, 0
                End If
            End With

            If bProcessa Then   'AKIO.OKUNO - 24/09/2012
                'Frame: Prestadores
                With btnDadosEspecificos_Prestadores_Operacao
                    If Not .Visible Then
                        .Visible = True
                        If strTipoProcesso = strTipoProcesso_Consulta Then
                            .Caption = "&Consulta"
                            .Tag = 3
                        Else
                            .Caption = "&Novo"
                            .Tag = 1
                            .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                        End If
                        InicializaInterface_Dimensao_Objeto grdDadosEspecificos_Prestador, "d", 0, .Height, 0, 0
                    End If
                End With

                'Frame: Reguladores
                'If strTipoProcesso <> strTipoProcesso_Consulta And strTipoProcesso <> strTipoProcesso_Consulta_Cosseguro Then
                For bIndice = 0 To 1
                    If bIndice = 0 Then
                        With btnDadosEspecificos_Reguladores_Operacao(bIndice)
                            If Not .Visible Then
                                .Visible = True
                                If strTipoProcesso = strTipoProcesso_Consulta Then
                                    .Caption = "&Consulta"
                                    .Tag = 3
                                Else
                                    .Caption = "&Novo"
                                    .Tag = 1
                                    .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                                End If
                            End If
                        End With
                    End If
                    'AKIO.OKUNO - INSERIDO PARA VERS�O PROVISORIA
                    btnDadosEspecificos_Reguladores_Operacao(1).Enabled = False
                Next
                InicializaInterface_Dimensao_Objeto grdDadosEspecificos_Regulador, "d", 0, btnDadosEspecificos_Reguladores_Operacao(0).Height, 0, 0
                'End If

                'Frame: Reguladores
'AKIO.OKUNO - INICIO - 01/11/2012
'                With fmeDadosEspecificos_Reguladores
'                    .Visible = (strTipoProcesso = strTipoProcesso_Consulta Or _
'                                strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Or _
'                                strTipoProcesso = strTipoProcesso_Avaliar Or _
'                                strTipoProcesso = strTipoProcesso_Avaliar_Cosseguro Or _
'                                strTipoProcesso = strTipoProcesso_Reabrir Or _
'                                strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro)
'                End With
                With fmeDadosEspecificos_Reguladores
                    .Visible = (strTipoProcesso = strTipoProcesso_Consulta Or _
                                strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Or _
                                strTipoProcesso = strTipoProcesso_Avaliar Or _
                                strTipoProcesso = strTipoProcesso_Avaliar_Cosseguro Or _
                                strTipoProcesso = strTipoProcesso_Reabrir Or _
                                strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro Or _
                                strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem)
                End With
'AKIO.OKUNO - FIM - 01/11/2012
                'Frame: Bens Sinistrados ' cristovao.rodrigues 15/08/2012
                With btnDadosEspecificos_BensSinistrados_Operacao
                    If Not .Visible Then
                        .Visible = True
                        If strTipoProcesso = strTipoProcesso_Consulta Then
                            .Caption = "&Consulta"
                            .Tag = 3
                        Else
                            .Caption = "&Novo"
                            .Tag = 1
                            .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                        End If
                        InicializaInterface_Dimensao_Objeto grdDadosEspecificos_BensSinistrados, "d", 0, .Height, 0, 0
                    End If
                End With
            End If  'AKIO.OKUNO - 24/09/2012
        End Select
    Case strTipoProcesso_Avaliar_Cosseguro
        'Frame: A��o Judicial
        btnDadosEspecificos_AcaoJudicial_Operacao.Visible = True
        With btnDadosEspecificos_AcaoJudicial_Operacao
            If Not .Visible Then
                .Visible = True
                .Tag = 1
                '.Enabled = LiberaUsoCompenente 'cristovao.rodrigues 22/08/2012
                InicializaInterface_Dimensao_Objeto grdDadosEspecificos_AcaoJudicial, "d", 0, .Height, 0, 0
            End If
        End With

        fmeDadosEspecificos_Prestadores.Visible = False
        fmeDadosEspecificos_Reguladores.Visible = False
        'MATHAYDE
        fmeDadosEspecificos_BensSinistrados.Top = fmeDadosEspecificos_Prestadores.Top
        'AKIO.OKUNO - INICIO - 28/09/2012
        With btnDadosEspecificos_BensSinistrados_Operacao
            If Not .Visible Then
                .Visible = True
                .Caption = "&Novo"
                .Tag = 1
                .Enabled = LiberaUsoCompenente    'cristovao.rodrigues 22/08/2012
                InicializaInterface_Dimensao_Objeto grdDadosEspecificos_BensSinistrados, "d", 0, .Height, 0, 0
            End If
        End With
        'AKIO.OKUNO - FIM - 28/09/2012

    Case strTipoProcesso_Reabrir, strTipoProcesso_Reabrir_Cosseguro
        'btnDadosEspecificos_AcaoJudicial_Operacao.Visible = True
        With btnDadosEspecificos_AcaoJudicial_Operacao
            If Not .Visible Then
                .Visible = True
                .Tag = 1
                '.Enabled = LiberaUsoCompenente 'cristovao.rodrigues 22/08/2012
                InicializaInterface_Dimensao_Objeto grdDadosEspecificos_AcaoJudicial, "d", 0, .Height, 0, 0
            End If
        End With

        'retirarAKIO.OKUNO - INICIO - 02/10/2012
    Case strTipoProcesso_Encerrar, strTipoProcesso_Encerrar_Cosseguro

    End Select

    'AKIO.OKUNO - INICIO - 02/10/2012
    If strTipoProcesso = strTipoProcesso_Consulta Or strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
        btnDadosEspecificos_AcaoJudicial_Operacao.Enabled = True
        btnDadosEspecificos_Prestadores_Operacao.Enabled = True
        btnDadosEspecificos_Reguladores_Operacao(0).Enabled = True
        btnDadosEspecificos_Reguladores_Operacao(1).Enabled = True
    End If
    'AKIO.OKUNO - FIM- 02/10/2012
End Sub

'Alexandre.Valim - INICIO - 28/07/2014
Private Sub btnSolicitarSaldo_Click()
    
   If MsgBox("Confirma Solicita��o ?", vbQuestion + vbYesNo + vbDefaultButton2) = vbNo Then
        'operacao cancelada
        Exit Sub
   End If
   On Error GoTo Trata_Erro
   
   If btnSolicitarSaldo.Caption = "Cancelar solicita��o de saldo" Then
           Call Cancela_Solicitacao_Saldo_Devedor(CStr(gbldSinistro_ID))
            
   Else
          Call Solicitacao_Saldo_Devedor
   End If

   btnSolicitarSaldo.Caption = IIf(Solicitou_Saldo_Devedor(lblSinistro_ID(0).Caption), "Cancelar solicita��o de saldo", "Solicitar saldo")
    
   Exit Sub
    
Trata_Erro:
     MousePointer = 0
     mensagem_erro 6, vMsg
     Unload Me


End Sub
'Alexandre.Valim - FIM - 28/07/2014

Private Sub Solicitacao_Saldo_Devedor()
    Dim iIndice             As Integer
    Dim rsRecordSet         As ADODB.Recordset
    Dim sSQL                As String
    Dim bConex              As Boolean
'
'    Dim i As Integer
'    Dim Estima As Estimativa
'    Dim Sin As Sinist
'    Dim vValor_IS As Double
'    Dim Mensagem, vMsg  As String

    '=================================================================================================================================='
    '17919477 - BB Cr�dito Protegido Empresa PJ - Cl�udio Cirne - 09/06/2014 - in�cio
    If Trim(txtProduto_ID.Text) = 1225 Or Trim(txtProduto_ID.Text) = 1231 Then 'BB SEGURO CR�DITO PROTEGIDO PARA EMPRESAS
                
       If Not bCarregou_AbaEstimativasPagamentos Then
          'FRAME BENEFICIARIOS
          InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Beneficiario, 1
          CarregaDadosGrid_AbaEstimativasPagamentos_Beneficiario
            
          'FRAME VOUCHER
          InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Voucher, 1
          CarregaDadosGrid_AbaEstimativasPagamentos_Voucher
            
          CarregaDadosTela_AbaEstimativasPagamentos 0
       End If
       
       grdEstimativasPagamentos_Coberturas.Row = 1
       grdEstimativasPagamentos_Coberturas.col = 3 '<-- Tipo de Cobertura
       
       If grdEstimativasPagamentos_Coberturas.Text = "1" Then '<-- Cobertura Gen�rica de Sinistro
          MsgBox "N�o � poss�vel solicitar saldo para Cobertura Gen�rica do produto" & vbCrLf & _
                 Trim(txtProduto_ID.Text) & " - " & Trim(txtProduto_Nome), vbOKOnly + vbCritical, "Solicitar Saldo - Aten��o"
          Exit Sub
       End If
       
    End If
    '17919477 - BB Cr�dito Protegido Empresa PJ - Cl�udio Cirne - 09/06/2014 - fim
    '=================================================================================================================================='
    
    If bytTipoRamo = bytTipoRamo_Vida Then
        iIndice = 3
    Else
        iIndice = 7
    End If
        
    If gbldSinistro_ID = 0 Then
        MsgBox "N�o h� sinistro para solcitar saldo!", vbExclamation + vbOKOnly
    Else ' Autorizar Pagamento
        
        vFRMAVISO_ESTA_VISIVEL = True
        
        ' Se algum pagamento foi rejeitado, tem que cancelar.
        If ValidaDados_ExisteEstimativaRejeitada(lConexaoLocal) Then
            MsgBox "Existe pagamento rejeitado!", 16, "Mensagem ao Usu�rio"
            Exit Sub
        End If
        
        If bytTipoRamo <> bytTipoRamo_Vida Then
            If gbllProduto_ID <> 680 And _
               gbllProduto_ID <> 1168 And _
               gbllProduto_ID <> 1201 And _
               gbllProduto_ID <> 1226 And _
               gbllProduto_ID <> 1227 Then
                If ValidaDados_AnaliseResseguro(lConexaoLocal, txtEndosso) Then
                    MsgBox "Verificar an�lise de resseguro!", 16, "Mensagem ao Usu�rio"
                    Exit Sub
                End If
            
                If ValidaDados_PendenciaAnaliseResseguro(True, lConexaoLocal) = False Then
                    MsgBox "Consultar pend�ncia de resseguro!", 16, "Mensagem ao Usu�rio"
                    Exit Sub
                End If
            End If
        End If
            
        'Mostra um aviso se estiver inadimplente
        sSQL = ""
        sSQL = sSQL & "Select Count(Num_Cobranca)" & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Agendamento_Cobranca_Tb        Agendamento_Cobranca_Tb  WITH (NOLOCK)  " & vbNewLine         'AKIO.OKUNO - 17/04/2013
        sSQL = sSQL & "  From Seguros_Db.Dbo.Agendamento_Cobranca_Atual_Tb        Agendamento_Cobranca_Tb  WITH (NOLOCK)  " & vbNewLine    'AKIO.OKUNO - 17/04/2013
        sSQL = sSQL & " Where Proposta_ID                                   = " & gbllProposta_ID & vbNewLine
        If bytTipoRamo = bytTipoRamo_Vida Then
            sSQL = sSQL & "   and Dt_Agendamento                                <= '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "'" & vbNewLine
        Else
            sSQL = sSQL & "   and Dt_Agendamento                                <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
        End If
        sSQL = sSQL & "  and Dt_Recebimento                                 is Null " & vbNewLine
        sSQL = sSQL & "  and Canc_Endosso_ID                                is Null " & vbNewLine
                
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                                            , glAmbiente_id _
                                            , App.Title _
                                            , App.FileDescription _
                                            , sSQL _
                                            , lConexaoLocal _
                                            , True)
                
        If rsRecordSet.Fields(0) > 0 Then
            MsgBox "Existe(m) parcela(s) em atraso para esta proposta, anterior(es) a data de hoje."
        Else
            Set rsRecordSet = Nothing
            sSQL = ""
            sSQL = sSQL & "Select Count(*) " & vbNewLine
            sSQL = sSQL & "  From Seguros_Db.Dbo.Cobranca_Inadimplente_Tb   Cobranca_Inadimplente_Tb  WITH (NOLOCK) " & vbNewLine
            sSQL = sSQL & " Where Proposta_ID                               = " & gbllProposta_ID & vbNewLine
            sSQL = sSQL & "   and Dt_Cobranca                               < '" & Format(gbldDt_Ocorrencia_Sinistro, "yyyymmdd") & "'" & vbNewLine
            
            Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                                                , glAmbiente_id _
                                                , App.Title _
                                                , App.FileDescription _
                                                , sSQL _
                                                , lConexaoLocal _
                                                , True)
            If rsRecordSet.Fields(0) > 0 Then
                MsgBox "Existe(m) parcela(s) em atraso para esta proposta, anterior(es) a data de ocorr�ncia do sinistro."
            End If
        End If
        Set rsRecordSet = Nothing
        
        bConex = AbrirTransacao(lConexaoLocal)

        If Not AtualizaDados_SolicitacaoSaldo(CStr(gbldSinistro_ID) _
                                            , CStr(gbllApolice_ID) _
                                            , CStr(gbllSucursal_Seguradora_ID) _
                                            , CStr(gbllSeguradora_Cod_Susep) _
                                            , CStr(gbllRamo_ID) _
                                            , CStr(gbllProduto_ID)) Then
           MsgBox "Erro na inclus�o da solicita��o do saldo!", vbOKOnly + vbCritical
           bConex = RetornarTransacao(lConexaoLocal)
           Exit Sub
        End If

        bConex = ConfirmarTransacao(lConexaoLocal)
        
        btnSolicitarSaldo.Enabled = False
        'FLAVIO.BEZERRA - IN�CIO - 30/01/2013
        If tabPrincipal.Tab = 1 Then
            bCarregou_AbaDetalhamento = False
            CarregaDadosTela_AbaDetalhamento
        Else
            bCarregou_AbaDetalhamento = False
        End If
        'FLAVIO.BEZERRA - FIM - 30/01/2013
    End If
    
    Exit Sub
    
Trata_Erro:
     MousePointer = 0
     mensagem_erro 6, vMsg
     Unload Me

End Sub
'Alexandre.Valim - INICIO - 28/07/2014
Private Function Solicitou_Saldo_Devedor(ByVal sSinistro_ID As String) As Boolean
    On Error GoTo Trata_Erro
    If Len(sSinistro_ID) = 0 Then Exit Function
    Dim rsRecordSet As Recordset
    Dim sSQL As String
    
    '    Dom�nio do campo situa��o.
    '   0 - Pendente de solicita��o de saldo       -> Selecionado
    '   1 - Solicita��o de saldo enviada           -> Selecionado
    '   2 - Pendente de solicita��o de pagamento   -> Selecionado
    '   3 - Pagamento realizado
    '   5 - Sem saldo

    sSQL = "SELECT COUNT(1) " & _
                  "FROM seguros_db.dbo.sinistro_solicita_saldo_tb  WITH (NOLOCK)  " & _
                  "WHERE Sinistro_id = " & sSinistro_ID & " " & _
                  "AND situacao IN (0,1,2)"
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                                        , glAmbiente_id _
                                        , App.Title _
                                        , App.FileDescription _
                                        , sSQL _
                                        , lConexaoLocal _
                                        , True)
    
    Solicitou_Saldo_Devedor = rsRecordSet(0) <> 0

    If (Solicitou_Saldo_Devedor) Then
            btnSolicitarSaldo.Enabled = True
            btnSolicitarSaldo.Visible = True
            With btnSolicitarSaldo
             .Width = 2000
            End With
           
    End If
                              
    Exit Function
    
Trata_Erro:
     MousePointer = 0
     mensagem_erro 6, vMsg
     Unload Me

End Function
'Alexandre.Valim - FIM - 28/07/2014
Private Sub cmdExigencia_Click()
    Dim glAmbienteTemp As Integer
    Dim AInstancia     As String
    glAmbienteTemp = glAmbiente_id_seg2
    glAmbiente_id_seg2 = glAmbiente_id
    'HENRIQUE H. HENRIQUES CONFITEC(SP) INC000004336100 INI
    'ExecutaPrograma "SEGP9083.exe", "SEGP9083.exe", "/s_id " & gbldSinistro_ID , vbNormalFocus, "SEGP9083.exe"
    ExecutaPrograma "SEGP9083.exe", "SEGP9083.exe", "/s_id " & gbldSinistro_ID & " /user " & cUserName, vbNormalFocus, "SEGP9083.exe"
    'HENRIQUE H. HENRIQUES CONFITEC(SP) INC000004336100 FIM
    'AInstancia = Shell(gsPastaLocalSegbr & "SEGP9083.exe" & " " & "/s_id " & gbldSinistro_ID & " " & Monta_Parametros("SEGP9083.exe"), OEstilo)
    
    glAmbiente_id_seg2 = glAmbienteTemp
End Sub

'cristovao.rodrigues 13/03/2015
'incluida chamada para SEGP1380 PMBC
'Private Sub cmdPMBC_Click()
'
'Dim sChaveComposta As String
'Dim sChaveParaCommandDemaisCampos As String
'Dim sNomeAplicacao As String
'Dim sBeneficiarioID As Integer
'
'    sChaveComposta = 1
'
'    If bytTipoRamo = bytTipoRamo_Vida Then
'
'        If gbldSinistro_ID = 0 Then
'           MsgBox "Sinistro inexistente!", 16, "Mensagem ao Usu�rio"
'        Else
'            If grdEstimativasPagamentos_Beneficiario.Rows > 1 Then
'
'                    'C_ANEVES - 22/11/2013 - Demanda 16429073 - Amparo Familiar - Cobertura 885 - Inicio
'                    'Valida��o de sele��o beneficiario/herdeiro para exibir saldo PMBC
'                    sSQL = "" & vbNewLine
'                    sSQL = sSQL & " declare @contador           int" & vbNewLine
'                    sSQL = sSQL & " declare @beneficiario_id    tinyint" & vbNewLine
'                    sSQL = sSQL & " declare @sinistro_id        numeric(11)" & vbNewLine
'                    sSQL = sSQL & " set     @sinistro_id = " & gbldSinistro_ID & vbNewLine
'                    sSQL = sSQL & " set     @beneficiario_id     = " & FrmConsultaSinistro.grdEstimativasPagamentos_Beneficiario.TextMatrix(grdEstimativasPagamentos_Beneficiario.Row, 17) & vbNewLine
'                    sSQL = sSQL & " select  @contador = count(1)" & vbNewLine
'                    sSQL = sSQL & "   from  seguros_db.dbo.saldo_resgate_pgto_sinistro_atual_tb saldo_resgate_pgto_sinistro_atual_tb with (nolock)" & vbNewLine
'                    sSQL = sSQL & "  where  sinistro_id     = @sinistro_id" & vbNewLine
'                    sSQL = sSQL & "    and  beneficiario_id = @beneficiario_id" & vbNewLine
'                    sSQL = sSQL & " " & vbNewLine
'                    sSQL = sSQL & " if @contador = 0" & vbNewLine
'                    sSQL = sSQL & " begin" & vbNewLine
'                    sSQL = sSQL & "     select  @contador = count(1), @beneficiario_id = sinistro_benef_herdeiro_tb.beneficiario_id" & vbNewLine
'                    sSQL = sSQL & "       from  seguros_db.dbo.sinistro_benef_herdeiro_tb sinistro_benef_herdeiro_tb with (nolock)" & vbNewLine
'                    sSQL = sSQL & "      where  sinistro_id                 = @sinistro_id" & vbNewLine
'                    sSQL = sSQL & "        and  beneficiario_herdeiro_id    = @beneficiario_id" & vbNewLine
'                    sSQL = sSQL & "      group  by sinistro_benef_herdeiro_tb.beneficiario_id" & vbNewLine
'                    sSQL = sSQL & " End" & vbNewLine
'                    sSQL = sSQL & " select @beneficiario_id, @contador" & vbNewLine
'                    'C_ANEVES - 22/11/2013 - Demanda 16429073 - Amparo Familiar - Cobertura 885 - Fim
'
'                    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                             glAmbiente_id, _
'                                             App.Title, _
'                                             App.FileDescription, _
'                                             sSQL, _
'                                             lConexaoLocal)
'
'                    If rs.Fields(1) > 0 Then
'
'                        'cristovao.rodrigues 31/03/2016 16429073 PMBC Amparo Familiar
'                        PMBC_Benef_id = rs.Fields(0)
'
'                        sBeneficiarioID = rs.Fields(0)
'                        sChaveParaCommandDemaisCampos = "2." & gbldSinistro_ID & "." & sBeneficiarioID & "." & txtCountEstimativa.Tag & "." & gblsSinistro_Situacao_ID
'                        iParametroParaChamadaAuxiliar = 2
'                        sNomeAplicacao = "SEGP1380"
'
'                        If PegaJanela(gblAuxiliarExecutando) = 0 Or gblAuxiliarExecutando = 0 Then 'cristovao.rodrigues 26/11/2013
'                            ExecutaAplicacao sNomeAplicacao, 0, sChaveParaCommandDemaisCampos, sChaveComposta
'                            sAuxiliarExecutado = "PMBC"
'                        End If
'
'                    Else
'                        MsgBox "Benefici�rio n�o possui saldo PMBC!", 16, "Mensagem ao Usu�rio"
'                    End If
'            Else
'               MsgBox "Favor efetuar cadastro de benefici�rio!", 16, "Mensagem ao Usu�rio"
'            End If
'         End If
'    Else
'        MsgBox "PMBC permitida apenas para produtos do tipo ramo Vida !", 16, "Mensagem ao Usu�rio"
'    End If
'Exit Sub
'
'
''''''''Dim sChaveComposta As String
''''''''Dim bProcessa As Boolean
''''''''
'''''''''Renato Silva - PMBC
''''''''On Error GoTo Erro
''''''''
'''''''''2.29201500003.2.3.0.-.2 ,19,80,113,170,224,187,217,132,128,29,92,13,108,125,122,230,235,241,218,207,200,81,80,69,102
'''''''''1.2.29201500003.3.1.-.2 ,19,68,113,190,224,175,217,144,128,9,92,26,32   1
''''''''
'''''''''---------
'''''''''
'''''''''N�mero do sinistro
'''''''''Id do benefici�rio
'''''''''Sequ�ncia da estimativa
'''''''''Situa��o do sinistro
''''''''
''''''''        'If grdEstimativasPagamentos_Estimativas.Col = 6 And rdEstimativasPagamentos_Estimativas.ColWidth(6) <> 0 Then
''''''''   If grdEstimativasPagamentos_Beneficiario.Rows <= 1 Then
''''''''          MsgBox "N�o h� benefici�rios!", 64, "Mensagem ao Usu�rio"
''''''''          Exit Sub
''''''''    Else
'''''''''        With grdEstimativasPagamentos_Beneficiario
'''''''''            If LenB(Trim(.TextMatrix(.Row, 3))) <> 0 Then
'''''''''                If CDbl(.TextMatrix(.Row, 3)) = 0 Then
'''''''''                    MsgBox "Este item, " & .TextMatrix(.Row, 0) & ", n�o possui saldo a pagar !", 64, "Mensagem ao Usu�rio"
'''''''''                    Exit Sub
'''''''''                End If
'''''''''            End If
'''''''''        End With
''''''''        'AKIO.OKUNO - INICIO - 30/09/2012
''''''''
''''''''        bProcessa = False
''''''''        bAlterou_fmeEstimativasPagamentos_Valores = True
''''''''
''''''''        iParametroParaChamadaAuxiliar = 2    ' VerificaSelecao()
''''''''
''''''''        If Val(txtCountEstimativa.Tag) <> 0 Then
''''''''            sChaveComposta = txtCountEstimativa.Tag & "." & grdEstimativasPagamentos_Estimativas.Row
''''''''        Else
''''''''            sChaveComposta = ""
''''''''
''''''''            iParametroParaChamadaAuxiliar = 1
''''''''        End If
''''''''
''''''''        'AKIO.OKUNO - INICIO - 22/10/2012
''''''''        If LenB(Trim(sOperacaoCosseguro)) <> 0 Then
''''''''            sChaveComposta = sChaveComposta & "." & sOperacaoCosseguro
''''''''        Else                                            'AKIO.OKUNO - 13/11/2012
''''''''            sChaveComposta = sChaveComposta & ".-"      'AKIO.OKUNO - 13/11/2012
''''''''        End If
''''''''        'AKIO.OKUNO - FIM - 22/10/2012
''''''''        sChaveComposta = sChaveComposta & "." & strTipoProcesso     'AKIO.OKUNO - 13/11/2012
''''''''
''''''''        If iParametroParaChamadaAuxiliar >= 2 Then
''''''''            If LenB(Trim(sChaveComposta)) <> 0 Then
''''''''                bProcessa = True
''''''''            Else
''''''''                MsgBox "N�o existe estimativa selecionada para consulta !", 16, "Mensagem ao Usu�rio"
''''''''                bProcessa = False
''''''''            End If
''''''''        Else
''''''''            bProcessa = True
''''''''        End If
''''''''
''''''''        '        If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
''''''''        If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
''''''''            ExecutaAplicacao "SEGP1293", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
''''''''
''''''''            If iParametroParaChamadaAuxiliar <> 3 Then
''''''''                bCarregou_AbaEstimativasPagamentos = False
'''''''''                sAuxiliarExecutado = "Estimativa"  'AKIO.OKUNO - 01/11/2012
''''''''                sAuxiliarExecutado = "PgtoSinistro"
''''''''
''''''''            End If
''''''''        End If
''''''''
''''''''    End If
''''''''    Exit Sub
'
'Erro:
'    Call TrataErroGeral("cmdPMBC_Click", Me.Caption)
'
'End Sub

Private Sub cmdSensoriamento_Click()

    Dim dRet As Double
    dRet = ExecutaAplicacao("SEGP1492", 0, gbldSinistro_ID & "", "") 'F.BEZERRA 19/12/2012

End Sub

'Ricardo Toledo (Confitec) : 10/05/2018 : 00416443 - Integra��o SEGBR & iRisk : INI
Private Sub cmdVistoria_Click()

On Error GoTo Trata_Erro

'Verifica se existe o arquivo de configura��o de ambiente para execu��o do SEGP1457
If Dir$(gsPastaLocalSegbr & "SEGP1457.exe.config") = vbNullString Then
    MsgBox "N�o foi encontrado o arquivo necess�rio para a configura��o de conexao com o sitema iRisk.", vbCritical + vbOKOnly, "SEGP1285"
    Exit Sub
End If

'Desabilita o form para evitar diversos cliques durante a execu��o
Me.MousePointer = vbHourglass
Me.Enabled = False

'Se n�o estiver executando
If PegaJanela(gblAuxiliarExecutando) = 0 Or (gblAuxiliarExecutando <> 0 And FindWindow(vbNullString, "SEGP1461") = 0) Then
    '--------------------------------------------------------------------------------------------------------------------------
    'Par�metros CommandLine (SEGP1461): 1 - Ambiente de execu��o / 2 - TipoRamo / 3 - Usuario / 4 - Sinistro / 5 - Caminho SEGP
    '--------------------------------------------------------------------------------------------------------------------------
    gblAuxiliarExecutando = Shell(gsPastaLocalSegbr & "SEGP1461.exe" & " " & glAmbiente_id & " " & bytTipoRamo & " " & cUserName & " " & gbldSinistro_ID & " " & gsPastaLocalSegbr & "SEGP1457.exe", vbNormalFocus)
    
    'Delay para aguardar a aplica��o aparecer na tela
    Sleep (1000)
Else
    'Se j� estiver executando
    AppActivate gblAuxiliarExecutando
End If

'Habilita o form
Me.Enabled = True
Me.MousePointer = vbDefault

Exit Sub

Trata_Erro:
    'Habilita o form
    Me.Enabled = True
    Me.MousePointer = vbDefault
    'Informa o erro
    MsgBox Err.Description, vbCritical + vbOKOnly, "ERRO - cmdVistoria_Click"
End Sub
'Ricardo Toledo (Confitec) : 10/05/2018 : 00416443 - Integra��o SEGBR & iRisk : FIM

Private Sub Form_Load()
    On Error GoTo Erro

'FBEZERRA/ FABREU - Inclus�o barra de rolagem SEGP1285 - 21/02/2018
'----------------------------------------------
    Dim iAlturaFormulario As Integer
    Dim iExibeAltura As Integer
'----------------------------------------------
    IniciarConexao
    'RETIRAR - VEM PELO PRIMEIRO FORM

    If Not bCarregou_Form Then
        InicializaVariaveisLocal

        InicializaInterfaceLocal

        FrmConsultaSinistrocon.Hide         'AKIO.OKUNO - 24/09/2012 - Conforme solicitado no report n.79

'FBEZERRA/ FABREU - Inclus�o barra de rolagem SEGP1285 - 21/02/2018
'----------------------------------------------
        iAlturaFormulario = 13125
        'Me.Height = 10800
                Me.Height = 13125
        iExibeAltura = Me.Height

        If iAlturaFormulario = iExibeAltura Then
            VScroll1.Visible = False
        Else
           With VScroll1
            .Height = Me.ScaleHeight
            .Min = 0
            .Max = iAlturaFormulario - iExibeAltura
            .SmallChange = Screen.TwipsPerPixelY * 10
            .LargeChange = .SmallChange
           End With
        End If
'----------------------------------------------
        
        'mathayde
        'tabPrincipal.Tab = 0               'AKIO.OKUNO - 27/09/2012 - ALTERADO PARA DENTRO DA INICIALIZAINTERFACELOCAL
    End If

    sSite = retornaSite()
    btnHistoricoCliente.Visible = (sSite <> "")
    RetornoCloud
    
    Call BuscaSinistroCardif ' Cesar Santos CONFITEC - (SBRJ009952) 18/08/2020 inicio
	
    'Zoro.gomes - Habilitar o bot�o Gest�o de Cartas de Encerramento
    'SEGP1285 - Avisar Sinistro
    'SEGP1285.E   - Avalia��o com Visualiza��o de documentos - Tp.ramo 1
    'SEGP1285.2.E - Avalia��o com Visualiza��o de documentos - Tp.ramo 2
    'SEGP1285.3.E - Avalia��o com Visualiza��o de documentos - Tp.ramo 3
    'SEGP1285.5   - Encerrar sinistro
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Or _
       strTipoProcesso = strTipoProcesso_Encerrar Or _
       strTipoProcesso = strTipoProcesso_Avisar Then
       gestaoCartaEncerramento.Visible = True
    End If
    
    Exit Sub
Erro:
    Call TrataErroGeral("Form_Load", Me.Caption)
    Call FinalizarAplicacao

End Sub


Function retornaSite() As String
    On Error GoTo Erro

    Dim sSQL As String
    Dim RS As ADODB.Recordset
    Dim sCont As Integer
    Dim sRet As String
    Dim sUrl As String
       
    sRet = ""
    sUrl = ""
    sSQL = "EXEC seguros_db.dbo.SEGS14304_SPS " & txtProduto_ID
    
    Set RS = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    sSQL, _
                                    lConexaoLocal, _
                                    True)
    
    If Not RS.EOF Then
        sCont = RS(0)
        sUrl = RS(1)
        RS.Close
    End If
    Set RS = Nothing
    
    If sCont > 0 Then
       sRet = sUrl & "Home/CreateSecureLink?user=" & BuscaLogin(cUserName) & "&CPFCNPJ=" & sCpf_Cnpj_Sem_Formatacao
     End If
    
    retornaSite = sRet
    
    Exit Function
Erro:
    Call TrataErroGeral("AbreHistoricoCliente", Me.Caption)
    Call FinalizarAplicacao
End Function
Private Sub AbreSiteHistorico()

  ShellExecute hwnd, "open", sSite, vbNullString, vbNullString, conSwNormal
  
End Sub

Private Sub LimpaCamposTelaLocal(Optional objObjeto As Object)
    On Error GoTo Erro

    If Not objObjeto Is Nothing Then
        LimpaCamposTela objObjeto
    End If
    
    'FLAVIO.BEZERRA - INICIO - 01/02/2013
    bCarregou_AbaCausasEEventos = False
    bCarregou_AbaDadosEspecificos = False
    bCarregou_AbaDadosGerais = False
    
'AKIO.OKUNO - INICIO - 30/04/2013
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        bCarregou_AbaDetalhamento = True
        txtDetalhamento_Visualizar = FrmConsultaSinistrocon.txtDetalhamento_Visualizar
        txtDetalhamento_Visualizar.Font.Size = 10
    Else
'AKIO.OKUNO - FIM - 30/04/2013
        bCarregou_AbaDetalhamento = False
    End If  'AKIO.OKUNO - 30/04/2013
    
    bCarregou_AbaEstimativasPagamentos = False
    bCarregou_AbaHistorico = False
    bCarregou_AbaPLD = False
        bCarregou_AbaAgravamento = False    'wilder - Agravamento Pecu�rio
    'FLAVIO.BEZERRA - FIM - 01/02/2013
    
    Exit Sub

Erro:
    Call TrataErroGeral("LimpaCamposTelaLocal", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub IniciarConexao()
    On Error GoTo Erro

'AKIO.OKUNO - INICIO - 30/04/2013
'    If EstadoConexao(vNunCx) = adStateClosed Then
'
'        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)'
'
'    End If
'AKIO.OKUNO - FIM - 30/04/2013

    lConexaoLocal = vNunCx
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosCombo", Me.Caption)
    Call FinalizarAplicacao


End Sub


Private Sub HabilitaBotaoSensoriamento()
 Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String

            sSQL = ""
            sSQL = sSQL & " exec seguros_db.dbo.SEGS14237_SPS " & gbldSinistro_ID

        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, True)

         With rsRecordSet
                cmdSensoriamento.Visible = Not .EOF
        End With
    End Sub

Private Sub CarregaDadosTela_AbaDadosGerais()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim iInd As Integer 'Demanda 18225206.

    On Error GoTo Erro

    If Not bCarregou_AbaDadosGerais Then

        MousePointer = vbHourglass

        'Alan Neves - 15/04/2013 - Comentado a chamada da sub SelecionaDados 0, pois parametro 0 � utilizado tamb�m no Carregamento do Formul�rio,
        'logo, como os dados j� vem preenchidos do form de pesquisa - FrmConsultaSinistrocon, a atualiza��o n�o seria realizada
        'Trata-se de uma solu��o paleativa, pois estaremos alterando o processo em procs
        'Inicio
        
        'SelecionaDados 0
        
        If Not bAtivou_Form Then
            
            If LenB(Trim(lblSinistro_ID(0))) <> 0 Then
'               InicializaCargaDados_LimpezaTemporario      'AKIO.OKUNO - #RETIRADA - 09/05/2013
               SelecionaDados_AbaDadosGerais
            Else
               SelecionaDados_AbaDadosGerais
            End If
            
            bAtivou_Form = True
        End If
        'Alan Neves - 15/04/2013 - Fim

        InicializaModoProcessamentoObjeto grdDadosGerais_Aviso, 1

        sSQL = ""
        sSQL = sSQL & "Select * " & vbCrLf
        sSQL = sSQL & "  From #Sinistro_Principal"

        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, True)
        With rsRecordSet
            If Not .EOF Then

                'Dados do Cabe�alho do Form:
                If Not IsNull(.Fields!sinistro_id) Then
                    lblSinistro_ID(0).Caption = .Fields!sinistro_id
                End If
                lblSinistro_Situacao(1).Visible = False
                If Not IsNull(.Fields!Sinistro_Situacao_ID) Then
                    lblSinistro_Situacao(0) = Seleciona_Sinistro_Situacao(.Fields!Sinistro_Situacao_ID)
                Else
                    lblSinistro_Situacao(0) = ""
                End If
                If Not IsNull(.Fields!Sinistro_Situacao_ID) Then
                    sSinistro_Situacao_ID = .Fields!Sinistro_Situacao_ID
                End If
                lblSinistro_Situacao(1).Left = lblSinistro_Situacao(0).Left '- lblSinistro_Situacao(1).Width - 100
                lblSinistro_Situacao(1).Visible = True
                gbldSinistro_ID = "0" & .Fields!sinistro_id

                'Dados do Frame: Seguro
                txtApolice_ID = IIf(IsNull(.Fields!Apolice_id), 0, .Fields!Apolice_id) 'Luis Deilson - 16/06/2014 -17905311
                gbllApolice_ID = txtApolice_ID

                txtRamo_Nome = UCase(.Fields!Ramo_Nome)
                gbllRamo_ID = .Fields!ramo_id
                txtRamo_ID = .Fields!ramo_id & ""

                gbllSeguradora_Cod_Susep = .Fields!seguradora_cod_susep
                gbllSucursal_Seguradora_ID = .Fields!sucursal_seguradora_id

                txtSeguradora_Nome_Lider = UCase(.Fields!Seguradora_Nome_Lider) & ""
                If Not IsNull(.Fields!Apolice_Dt_Inicio_Vigencia) Then
                    txtApolice_Dt_Inicio_Vigencia = Format(.Fields!Apolice_Dt_Inicio_Vigencia, "dd/mm/yyyy")
                Else
                    txtApolice_Dt_Inicio_Vigencia = ""
                End If
                If Not IsNull(.Fields!Apolice_Dt_Fim_Vigencia) Then
                    txtApolice_Dt_Fim_Vigencia = Format(.Fields!Apolice_Dt_Fim_Vigencia, "dd/mm/yyyy")
                Else
                    txtApolice_Dt_Fim_Vigencia = ""
                End If
                If Not IsNull(.Fields!Apolice_Dt_Emissao) Then
                    txtApolice_Dt_Emissao = Format(.Fields!Apolice_Dt_Emissao, "dd/mm/yyyy")
                Else
                    txtApolice_Dt_Emissao = ""
                End If
                txtProposta_ID = .Fields!Proposta_id

                txtProduto_Nome = UCase(.Fields!Produto_Nome) & ""
                txtProduto_ID = .Fields!produto_Id & ""
                
                'Andr� Martins - FaceLift
                If .Fields!produto_Id = 1237 And (.Fields!plano_id = 4 Or .Fields!plano_id = 5 Or .Fields!plano_id = 6) Then
                    lblPlano.Visible = True
                    txtPlano.Visible = True
                    txtPlano.Text = UCase(.Fields!plano_nome) & ""
                    txtProduto_Nome.Width = 2335
                Else
                    lblPlano.Visible = False
                    txtPlano.Visible = False
                    txtPlano.Text = ""
                    txtProduto_Nome.Width = 5535
                End If

                If Not IsNull(.Fields!cpf_cnpj) Then
                    sCpf_Cnpj_Sem_Formatacao = .Fields!cpf_cnpj
                Else
                    sCpf_Cnpj_Sem_Formatacao = ""
                End If

                If Not IsNull(.Fields!Certificado_Dt_Inicio_Vigencia) Then
                    txtCertificado_Dt_Inicio_Vigencia = Format(.Fields!Certificado_Dt_Inicio_Vigencia, "dd/mm/yyyy")
                Else
                    txtCertificado_Dt_Inicio_Vigencia = ""
                    'mathayde
                    txtCertificado_Dt_Inicio_Vigencia.Visible = False
                    lblDadosGerais_Seguro(8).Visible = False

                End If
                If Not IsNull(.Fields!Certificado_Dt_Fim_Vigencia) Then
                    txtCertificado_Dt_Fim_Vigencia = Format(.Fields!Certificado_Dt_Fim_Vigencia, "dd/mm/yyyy")
                Else
                    txtCertificado_Dt_Fim_Vigencia = ""
                    'mathayde
                    txtCertificado_Dt_Fim_Vigencia.Visible = False
                    lblDadosGerais_Seguro(9).Visible = False
                End If
                'AKIO.OKUNO - INICIO - 25/09/2012
                '                If Not IsNull(.Fields!Certficado_Dt_Emissao) Then
                '                txtCertificado_Dt_Emissao = Format(.Fields!Certficado_Dt_Emissao, "dd/mm/yyyy")
                If Not IsNull(.Fields!Certificado_Dt_Emissao) Then
                    txtCertificado_Dt_Emissao = Format(.Fields!Certificado_Dt_Emissao, "dd/mm/yyyy")
                    'AKIO.OKUNO - FIM - 25/09/2012
                Else
                    txtCertificado_Dt_Emissao = ""
                    'mathayde
                    txtCertificado_Dt_Emissao.Visible = False
                    lblDadosGerais_Seguro(10).Visible = False

                End If

                txtQuantidade_Endossos = Val("0" & .Fields!Quantidade_Endossos)
                txtProposta_Situacao = UCase(.Fields!Proposta_Situacao_Descricao) & ""
                txtTp_Cobertura_Nome = .Fields!Tp_Cobertura_Nome & ""
                txtCorretor_Nome = .Fields!Corretor_Nome & ""
                If Not IsNull(.Fields!Cliente_id) Then
                    txtCliente_ID = .Fields!Cliente_id
                Else
                    txtCliente_ID = ""
                End If
                txtCliente_Nome = .Fields!Cliente_Nome & ""
                txtTp_Componente_Nome = .Fields!Tp_Componente_Nome & ""


                'AKIO.OKUNO - INICIO - 05/10/2012
                'gblsTp_Componente_ID = .Fields!Tp_Componente_ID & "" 'FLAVIO.BEZERRA 04/01/2013
                gblsTp_Componente = .Fields!Tp_Componente_ID & "" 'FLAVIO.BEZERRA 04/01/2013
                If bytTipoRamo = bytTipoRamo_Vida Then
                    If gblsTp_Componente_ID = "C" Then
                        gblsTp_Componente_ID = "C"
                    Else
                        gblsTp_Componente_ID = "T"
                    End If
                End If
                'AKIO.OKUNO - FIM - 05/10/2012



                'Dados do Frame: Aviso

                CarregaDadosTela_AbaDadosGerais_Aviso rsRecordSet


                If sOperacaoCosseguro = "C" Then
                    'Dados do Frame: Cosseguro
                    txtSeguradora_Nome = .Fields!Seguradora_Nome & ""
                    txtCosseguro_Aceito_Sucursal_Seg_Lider = .Fields!Cosseguro_Aceito_Sucursal_Seg_Lider & ""
                    txtSucursal_Seguradora_Nome = .Fields!Sucursal_Seguradora_Nome & ""
                    txtCosseguro_Aceito_Ramo_Lider = .Fields!Cosseguro_Aceito_Ramo_Lider & ""
                    txtCosseguro_Ramo_Nome = .Fields!Cosseguro_Ramo_Nome & ""
                    'mathayde
                    txtCosseguro_Aceito_Num_Apolice_Lider = .Fields!Cosseguro_Aceito_Num_Apolice_Lider & ""
                    fmeDadosGerais_Solicitante.Visible = False
                    fmeDadosGerais_Cosseguro.Visible = True
                    fmeDadosGerais_Cosseguro.Top = fmeDadosGerais_Solicitante.Top
                    fmeDadosGerais_Cosseguro.Left = fmeDadosGerais_Solicitante.Left

                Else
                    'Dados do Frame: Solicitante
                    lSolicitante_ID = "0" & .Fields!solicitante_id
                    txtSolicitante_Sinistro_Nome = .Fields!Solicitante_Sinistro_Nome & ""
                    If Not IsNull(.Fields!Solicitante_Sinistro_DDD1) Then txtSolicitante_Sinistro_DDD1 = "(" & .Fields!Solicitante_Sinistro_DDD1 & ")"
                    If Not IsNull(.Fields!Solicitante_Sinistro_Telefone1) Then txtSolicitante_Sinistro_Telefone1 = .Fields!Solicitante_Sinistro_Telefone1
                    If Not IsNull(.Fields!Solicitante_Sinistro_DDD_Fax) Then txtSolicitante_Sinistro_DDD_Fax = "(" & .Fields!Solicitante_Sinistro_DDD_Fax & ")"
                    If Not IsNull(.Fields!Solicitante_Sinistro_Telefone_Fax) Then txtSolicitante_Sinistro_Telefone_Fax = .Fields!Solicitante_Sinistro_Telefone_Fax
                    txtSolicitante_Sinistro_Endereco = .Fields!Solicitante_Sinistro_Endereco & ""
                    txtSolicitante_Sinistro_Bairro = .Fields!Solicitante_Sinistro_Bairro & ""
                    txtSolicitante_Sinistro_Estado = .Fields!Solicitante_Sinistro_Estado & ""
                    txtSolicitante_Municipio_Nome = IIf(IsNull(.Fields!Solicitante_Sinistro_Municipio), " ", .Fields!Solicitante_Sinistro_Municipio & "")
                    txtSolicitante_Sinistro_CEP = IIf(IsNull(.Fields!Solicitante_Sinistro_CEP), " ", MasCEP(.Fields!Solicitante_Sinistro_CEP) & "")
                    txtSolicitante_Sinistro_EMail = IIf(IsNull(.Fields!Solicitante_Sinistro_EMail), " ", .Fields!Solicitante_Sinistro_EMail & "")
                    '(INI) Demanda 18225206.
                    If Not IsNull(.Fields!Solicitante_Sinistro_DDD1) Then txtVidaSolicitante_Sinistro_DDD1 = "(" & .Fields!Solicitante_Sinistro_DDD1 & ")"
                    If Not IsNull(.Fields!Solicitante_Sinistro_Telefone1) Then txtVidaSolicitante_Sinistro_Telefone1 = .Fields!Solicitante_Sinistro_Telefone1
                    If Not IsNull(.Fields!Solicitante_Sinistro_DDD_Fax) Then txtVidaSolicitante_Sinistro_DDD_Fax = "(" & .Fields!Solicitante_Sinistro_DDD_Fax & ")"
                    If Not IsNull(.Fields!Solicitante_Sinistro_Telefone_Fax) Then txtVidaSolicitante_Sinistro_Telefone_Fax = .Fields!Solicitante_Sinistro_Telefone_Fax
                    
                    'PAULO PELEGRINI - MU-2017-045136 - 16/07/2018 (INI)
                    'If Not IsNull(.Fields!Solicitante_Sinistro_Grau_Parentesco_ID) Then txtVidaSolicitante_Sinistro_Parentesco = CarregaParentesco(.Fields!Solicitante_Sinistro_Grau_Parentesco_ID)
                    '(FIM) Demanda 18225206.
                    If bytTipoRamo = bytTipoRamo_Vida Then
                        txtVidaSolicitante_Sinistro_Parentesco = CarregaParentesco()
                    End If
                    'PAULO PELEGRINI - MU-2017-045136 - 16/07/2018 (fim)
                    
                End If

                If Not IsNull(.Fields!solicitante_id) Then
                    btnSolicitante_Sinistro_Telefone_Consulta.Enabled = .Fields!solicitante_id <> 0
                    'AKIO.OKUNO - 23/09/2012
                    '                    btnDadosGerais_Aviso_Cadastrar.Enabled = .Fields!solicitante_id <> 0
                End If

                'Dados do Frame: Sinistrado

                If sOperacaoCosseguro <> "C" Then   'AKIO.OKUNO - 09/10/2012
                    txtSinistro_Vida_Nome = .Fields!Sinistro_Vida_Nome & ""
                    If Not IsNull(.Fields!Sinistro_Vida_CPF) Then
                        txtSinistro_Vida_CPF = Format(.Fields!Sinistro_Vida_CPF, "&&&.&&&.&&&-&&")
                    End If

                    If Not IsNull(.Fields!Sinistro_Vida_Dt_Nasc) Then
                        txtSinistro_Vida_Dt_Nasc = Format(.Fields!Sinistro_Vida_Dt_Nasc, "dd/mm/yyyy")
                    End If

                    If Not IsNull(.Fields!Sinistro_Vida_Dt_Nasc) Then
                        txtSinistro_Vida_Dt_Nasc = Format(.Fields!Sinistro_Vida_Dt_Nasc, "dd/mm/yyyy")
                    End If

                    txtSinistro_Vida_Sexo = IIf(IsNull(.Fields!Sinistro_Vida_Sexo), "", IIf(.Fields!Sinistro_Vida_Sexo = "M", "MASCULINO", "FEMININO"))
                    txtSinistrado_Cliente_ID = .Fields!Sinistrado_Cliente_ID & ""
                End If  'AKIO.OKUNO - 09/10/2012

                'AKIOOKUNO
                CarregaDadosTela_Localizacao

                'F.BEZERRA 16/10/2012
                '                If bGTR_Implantado Then
                '                    lblDescricaoLocalizacaoProcesso(1).Tag = Seleciona_Localizacao_Processo_Local
                '                Else
                '                    lblDescricaoLocalizacaoProcesso(1).Tag = 2    'Na Seguradora
                '                End If
                '                Select Case lblDescricaoLocalizacaoProcesso(1).Tag
                '                Case 0: lblDescricaoLocalizacaoProcesso(1) = "ERRO NA LOCALIZA��O"
                '                Case 1: lblDescricaoLocalizacaoProcesso(1) = "AG�NCIA"
                '                Case 2: lblDescricaoLocalizacaoProcesso(1) = "SEGURADORA"
                '                Case 3: lblDescricaoLocalizacaoProcesso(1) = "ENCERRADO"
                '                Case 4: lblDescricaoLocalizacaoProcesso(1) = "INDEFINIDO"
                '                End Select

                Select Case bytTipoRamo
                    Case bytTipoRamo_Vida
                        btnEstimativasPagamentos_Valores_Beneficiario_CadastrarProposta.Visible = False
    
                    Case bytTipoRamo_RE
                        txtEndereco = .Fields!Sinistro_Endereco & ""
                        txtBairro = .Fields!Sinistro_Bairro & ""
                        txtEstado = .Fields!Sinistro_Estado & ""
                        txtMunicipio = .Fields!Sinistro_Municipio & ""
    
                    Case bytTipoRamo_Rural
                        txtEndereco = .Fields!Sinistro_Endereco & ""
                        txtBairro = .Fields!Sinistro_Bairro & ""
                        txtEstado = .Fields!Sinistro_Estado & ""
                        txtMunicipio = .Fields!Sinistro_Municipio & ""
                End Select

                sPassivel_Ressarcimento = Seleciona_PassivelRessarcimento(.Fields!Passivel_Ressarcimento & "")  'AKIO.OKUNO - #RETIRADA - 02/05/2013

                sPassivel_Ressarcimento = Seleciona_PassivelRessarcimento(.Fields!Passivel_Ressarcimento & "")  'AKIO.OKUNO - 02/10/2012

                bVerifica_Protocolacao = Verifica_Protocolacao  'AKIO.OKUNO - MP - 18/02/2013
                
                CarregaDadosGrid_AbaDadosGerais_Aviso

                InicializaToolTipTextLocal
            Else                                                'AKIO.OKUNO - #RETIRADA - 02/05/2013
                sPassivel_Ressarcimento = "N�o informado"       'AKIO.OKUNO - #RETIRADA - 02/05/2013
            End If
        End With

        bCarregou_AbaDadosGerais = True

        MousePointer = vbNormal

        Set rsRecordSet = Nothing
        
        HabilitaBotaoSensoriamento

    End If

    Exit Sub

Erro:
    'Resume
    Call TrataErroGeral("CarregaDadosTela_AbaDadosGerais", Me.Caption)
    Call FinalizarAplicacao

End Sub

'AKIO.OKUNO - Function Seleciona_PassivelRessarcimento - 02/10/2012
Private Function Seleciona_PassivelRessarcimento(sPassivel As String) As String
    On Error GoTo Erro

    Select Case sPassivel
        Case ""
            Seleciona_PassivelRessarcimento = "N�o informado"
        Case "N"
            Seleciona_PassivelRessarcimento = "N�o pass�vel de ressarcimento"
        Case "S"
            Seleciona_PassivelRessarcimento = "Pass�vel de ressarcimento"
        Case Else
            Seleciona_PassivelRessarcimento = "N�o informado"
    End Select

    Exit Function

Erro:
    Call TrataErroGeral("Seleciona_PassivelRessarcimento", Me.Caption)
    Call FinalizarAplicacao


End Function


Private Sub CarregaDadosTela_AbaDadosGerais_Aviso(rsRecordSet As ADODB.Recordset)
    Dim sSQL As String
    Dim rsRecordSet2 As ADODB.Recordset

    On Error GoTo TrataErro
    
    'Victor - 02/10/2013 - 17905311 - IN�CIO
    Dim Rst_Rural_Estudo As Recordset

    vSql = "Select count(*) AS qtde_rural " & vbNewLine
    vSql = vSql & " From proposta_tb " & vbNewLine
    vSql = vSql & " Where proposta_id = " & gbllProposta_ID & vbNewLine
    vSql = vSql & " And produto_id in (1152,1204,1240) " & vbNewLine '06/06/2018 - Schoralick (ntendencia) - 00421597-seguro-faturamento-pecuario  [(sprint 6 - HI00051)]
    vSql = vSql & " And situacao in ('a','e') " & vbNewLine
    
    Set Rst_Rural_Estudo = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        vSql, _
                                        lConexaoLocal, True)

    qtdeRuralEstudo = Rst_Rural_Estudo("qtde_rural")
    'Victor - 02/10/2013 - 17905311 - FIM

    With rsRecordSet
        txtEvento_Sinistro_Nome = .Fields!Evento_Sinistro_Nome & ""
        txtSubEvento_Sinistro_Nome = .Fields!SubEvento_Sinistro_Nome & ""

        If Not IsNull(.Fields!Dt_Ocorrencia_Sinistro) Then
            txtDt_Ocorrencia_Sinistro = Format(.Fields!Dt_Ocorrencia_Sinistro, "dd/mm/yyyy")
        Else
            txtDt_Ocorrencia_Sinistro = ""
        End If
        If Not IsNull(.Fields!Dt_Entrada_Seguradora) Then
            txtDt_Entrada_Seguradora = Format(.Fields!Dt_Entrada_Seguradora, "dd/mm/yyyy")
        Else
            txtDt_Entrada_Seguradora = ""
        End If
        If Not IsNull(.Fields!dt_aviso_sinistro) Then
            txtDt_Aviso_Sinistro = Format(.Fields!dt_aviso_sinistro, "dd/mm/yyyy")
        Else
            txtDt_Aviso_Sinistro = ""
        End If
        If Not IsNull(.Fields!Dt_Inclusao) Then
            txtDt_Inclusao = Format(.Fields!Dt_Inclusao, "dd/mm/yyyy")
        Else
            txtDt_Inclusao = ""
        End If

        txtAgencia_ID = .Fields!Agencia_ID & ""

        'mathayde
        If sOperacaoCosseguro = "C" And Not IsNull(.Fields!Cosseguro_Aceito_sinistro_id_lider) Then
            TxtNumSinistroLider = .Fields!Cosseguro_Aceito_sinistro_id_lider
            TxtNumSinistroLider.Visible = True
            lblDadosGerais_Aviso(7).Visible = True
        Else
            TxtNumSinistroLider.Visible = False
            lblDadosGerais_Aviso(7).Visible = False
        End If

        'if strTipPr
        chkReintegracaoIS.Value = IIf(Produto_Permite_Reintegracao_IS(txtProduto_ID, txtRamo_ID), 1, 0)

    End With

    sSQL = ""
    sSQL = sSQL & "SELECT s.endosso_id"
    sSQL = sSQL & "     , sre.num_averbacao"
    sSQL = sSQL & " FROM sinistro_tb                  s  WITH (NOLOCK)  "
    sSQL = sSQL & " LEFT JOIN sinistro_re_tb          sre  WITH (NOLOCK)  "
    sSQL = sSQL & "   ON (sre.sinistro_id             = s.sinistro_id)"
    sSQL = sSQL & " WHERE s.sinistro_id               = " & gbldSinistro_ID

    Set rsRecordSet2 = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                     , glAmbiente_id _
                     , App.Title _
                     , App.FileDescription _
                     , sSQL _
                     , lConexaoLocal _
                     , True)
    With rsRecordSet2
        If Not .EOF Then
            '            If Not IsNull(.Fields(1)) Then And vTranspInternacional = True Then
            '                mskAverbacao.Text = IIf(Len(.Fields(1)) < 7, CDbl(.Fields(1)) & String(7 - Len(.Fields(1)), "_"), CDbl(.Fields(1)))
            '            End If
            If Not IsNull(.Fields(0)) Then
                txtEndosso.Text = .Fields(0)
            Else
                txtEndosso.Text = "0"
                '                vEndosso_Ant = 0
            End If
        End If
    End With

    Set rsRecordSet2 = Nothing

    '21/02/2020 (ntedencia) c00216281-esteira-pagamento-imediato-vida-individual (inicio)
    'Carregamento de MSG de classificacao de sinistro conforme parametrizado na tp_sinistro_parametro_tb
    'Pagamento Imediato para produtos residenciais
    'Deferimento para produtos de vida
                                     
    ExibeClassificacaoSinistro (gbldSinistro_ID)
    
    '21/02/2020 (ntedencia) c00216281-esteira-pagamento-imediato-vida-individual (fim)

    Exit Sub

TrataErro:
    Call TrataErroGeral("CarregaDadosTela_AbaDadosGerais_Aviso", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Function Seleciona_Sinistro_Situacao(iSinistro_Situacao_ID As Integer) As String
    On Error GoTo Erro

    Select Case iSinistro_Situacao_ID
    Case 0
        Seleciona_Sinistro_Situacao = "ABERTO"
    Case 1
        Seleciona_Sinistro_Situacao = "REABERTO"
    Case 2
        Seleciona_Sinistro_Situacao = "ENCERRADO"
    Case 5
        Seleciona_Sinistro_Situacao = "REABERTO (ADMINISTRATIVO)"
        Case 8
        Seleciona_Sinistro_Situacao = "AGUARDANDO EXECU��O"
    Case Else
        Seleciona_Sinistro_Situacao = "DIFERENTE 0, 1, 2, 5 "
    End Select
    Exit Function

Erro:
    Call TrataErroGeral("Seleciona_Sinistro_Situacao", Me.Caption)
    Call FinalizarAplicacao

End Function

Private Sub EncerrarConexao()

    Set rsRecordSet = Nothing

    Set conConexao = Nothing

End Sub

Private Sub SelecionaDados(bAba As Byte)
    Dim bProcessa As Boolean

    bProcessa = True

    If LenB(Trim(lblSinistro_ID(0))) <> 0 Then
        Select Case bAba
            Case 0
                'InicializaCargaDados_LimpezaTemporario 'AKIO.OKUNO - #RETIRADA - 09/05/2013
                'SelecionaDados_AbaDadosGerais - Alan Neves - 12/04/2013 Comentada a chamada - Carregamento feito no form de consulta
                ' SelecionaDados_Form  ' - Alan Neves - 15/02/2013 processo est� sendo executado em SelecionaDados_AbaDadosGerais
            Case 1
                SelecionaDados_AbaDetalhamentoLocal
            Case 2
'                SelecionaDados_AbaHistorico        'AKIO.OKUNO - #RETIRADA - 09/05/2013
            Case 3
'                SelecionaDados_AbaEstimativa       'AKIO.OKUNO - #RETIRADA - 09/05/2013
'                CarregaDadosColecao 3              'AKIO.OKUNO - #RETIRADA - 09/05/2013
            Case 4
                'SelecionaDados_AbaDadosEspecificos 'AKIO.OKUNO - #RETIRADA - 02/05/2013
            Case 5
'                SelecionaDados_AbaCausasEEventos   'AKIO.OKUNO - #RETIRADA - 07/02/2013
            Case 6                                  'AKIO.OKUNO - 22/01/2013
                'SelecionaDados_AbaPLD               'AKIO.OKUNO - 22/01/2013   / AKIO.OKUNO - #RETIRADA - 07/02/2013
        End Select
    Else    'cristovao.rodrigues 22/08/2012
        SelecionaDados_AbaDadosGerais

    End If


End Sub

'AKIO.OKUNO - #RETIRADA - SelecionaDados_Formm - 09/05/2013
'CONFORME OBSERVA��O ABAIXO, PROCEDURE FOI DESATIVADA.
'Alan Neves - 15/02/2013 processo est� sendo executado em SelecionaDados_AbaDadosGerais
'
'Private Sub SelecionaDados_Form()
'    Dim sSQL As String
'
'    On Error GoTo TrataErro
'
'    MousePointer = vbHourglass
'    sSQL = ""
'
'' Alan Neves - 15/02/2013  -- As tabelas s�o criadas e � chamado proc para realizar tarefas
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Principal'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Sinistro_Principal" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Create Table #Sinistro_Principal "
'    sSQL = sSQL & "     ( Sinistro_ID                                   Numeric(11)         " & vbNewLine   ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Sucursal_Seguradora_ID                        Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Seguradora_Cod_Susep                          Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Apolice_ID                                    Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Ramo_ID                                       Int                 " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Ramo_Nome                                     VarChar(60)         " & vbNewLine  ' -- Ramo_Tb
'    sSQL = sSQL & "     , Seguradora_Nome_Lider                         VarChar(60)         " & vbNewLine  ' -- Seguradora_Tb (Nome Lider)
'    sSQL = sSQL & "     , Apolice_Dt_Inicio_Vigencia                    SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
'    sSQL = sSQL & "     , Apolice_Dt_Fim_Vigencia                       SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
'    sSQL = sSQL & "     , Apolice_Dt_Emissao                            SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
'    sSQL = sSQL & "     , Proposta_ID                                   Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Produto_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb
'    sSQL = sSQL & "     , Produto_Nome                                  VarChar(60)         " & vbNewLine  ' -- Produto_Tb
'    sSQL = sSQL & "     , Certificado_Dt_Inicio_Vigencia                SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
'    sSQL = sSQL & "     , Certificado_Dt_Fim_Vigencia                   SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
'    sSQL = sSQL & "     , Certificado_Dt_Emissao                         SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
'    sSQL = sSQL & "     , Certificado_ID                                Numeric(9)          " & vbNewLine  ' -- Certificado_Tb
'    sSQL = sSQL & "     , Certificado_Sub_Grupo_ID                      Int                 " & vbNewLine  ' -- seguro_vida_sub_grupo_tb
'    sSQL = sSQL & "     , Quantidade_Endossos                           Int                 " & vbNewLine  ' -- Count Endosso_Tb ???
'    sSQL = sSQL & "     , Proposta_Situacao                             VarChar(1)          " & vbNewLine  ' -- Proposta_Tb
'    sSQL = sSQL & "     , Proposta_Situacao_Descricao                   VarChar(60)         " & vbNewLine  ' -- Dado_Basico_Db..Situacao_Proposta_tb.Descricao
'    sSQL = sSQL & "     , Tp_Cobertura_Nome                             VarChar(60)         " & vbNewLine  ' -- Tp_Cobertura_Tb
'    sSQL = sSQL & "     , Corretor_Nome                                 VarChar(60)         " & vbNewLine  ' -- Corretor_Tb
'    sSQL = sSQL & "     , Cliente_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb (Prop_Cliente_ID)
'    sSQL = sSQL & "     , Cliente_Nome                                  VarChar(60)         " & vbNewLine  ' -- Cliente_Tb
'    sSQL = sSQL & "     , Tp_Componente_ID                              TinyInt             " & vbNewLine  ' -- Escolha_Tp_Cob_Vida_Tp
'    sSQL = sSQL & "     , Tp_Componente_Nome                            VarChar(60)         " & vbNewLine  ' -- Tp_Componente_Tb
'    sSQL = sSQL & "     , Evento_Sinistro_ID                            Int                 " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Evento_Sinistro_Nome                          VarChar(60)         " & vbNewLine  ' -- Evento_Sinistro_Tb
'    sSQL = sSQL & "     , SubEvento_Sinistro_ID                         Int                 " & vbNewLine  ' -- Sinistro_Tb
'    'cristovao.rodrigues 04/01/2013 mudanca para VarChar(120)
'    'sSQL = sSQL & "     , SubEvento_Sinistro_Nome                       VarChar(60)         " & vbNewLine  ' -- SubEvento_Sinistro_Tb
'    sSQL = sSQL & "     , SubEvento_Sinistro_Nome                       VarChar(120)         " & vbNewLine  ' -- SubEvento_Sinistro_Tb
'    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                        SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Dt_Entrada_Seguradora                         SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Dt_Aviso_Sinistro                             SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Dt_Inclusao                                   SmallDateTIme       " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Agencia_ID                                    Numeric(4)          " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Sinistro_Vida_Nome                            VarChar(60)         " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
'    sSQL = sSQL & "     , Sinistro_Vida_CPF                             VarChar(11)         " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
'    sSQL = sSQL & "     , Sinistro_Vida_Dt_Nasc                         DateTime            " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
'    sSQL = sSQL & "     , Sinistro_Vida_Sexo                            Char(1)             " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
'    sSQL = sSQL & "     , Sinistrado_Cliente_ID                         Int                 " & vbNewLine  ' -- Pessoa_Fisica_Tb (Pf_Cliente_ID) (Sinistrado)
'    sSQL = sSQL & "     , Solicitante_ID                                Int                 " & vbNewLine  ' -- Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Nome                     VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Endereco                 VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Bairro                   VarChar(30)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio_ID             Numeric(3)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio                VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Municipio_Nome                    VarChar(60)         " & vbNewLine  ' -- Municipio_Tb (Nome) (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD1                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax                  VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax             VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Estado                   Char(2)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_EMail                    VarChar(1000)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_CEP                      VarChar(8)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'
'    sSQL = sSQL & "     , Sinistro_Causa_Observacao_Item                Int                 " & vbNewLine  ' -- Sinistro_Causa_Observacao_Tb
'    sSQL = sSQL & "     , Sinistro_Causa_Observacao                     VarChar(300)        " & vbNewLine  ' -- Sinistro_Causa_Observacao_Tb
'    sSQL = sSQL & "     , Sinistro_Situacao_ID                          Int                 " & vbNewLine  ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Sinistro_Situacao                             VarChar(60)         " & vbNewLine  ' -- Dado_Basico_Db..Sinistro_Situacao_Tb
'    sSQL = sSQL & "     , Max_Endosso_ID                                Int                 " & vbNewLine  ' -- Endosso_Tb
'    'Para consulta
'    sSQL = sSQL & "     , Apolice_Terceiros_Num_Ordem_Co_Seguro_Aceito          Numeric(11,0)           " & vbNewLine
'    sSQL = sSQL & "     , Apolice_Terceiros_Seg_Cod_Susep_Lider                 Numeric(5,0)            " & vbNewLine
'    sSQL = sSQL & "     , Apolice_Terceiros_Ramo_ID                             Int                     " & vbNewLine
'    sSQL = sSQL & "     , Apolice_Terceiros_Sucursal_Seg_Lider                  Numeric(5,0)            " & vbNewLine
'    'Campos do frame Cosseguro
'    'mathayde
'    sSQL = sSQL & "     , Cosseguro_Aceito_sinistro_id_lider                    Numeric(13)           " & vbNewLine
'    sSQL = sSQL & "     , Cosseguro_Aceito_Num_Apolice_Lider                    Numeric(12,0)           " & vbNewLine
'    sSQL = sSQL & "     , Cosseguro_Aceito_Ramo_Lider                           Int                     " & vbNewLine
'    sSQL = sSQL & "     , Cosseguro_Aceito_Sucursal_Seg_Lider                   Numeric(5,0)            " & vbNewLine
'    sSQL = sSQL & "     , Cosseguro_Aceito_Seg_Cod_Susep_Lider                  Numeric(5,0)            " & vbNewLine
'    sSQL = sSQL & "     , Apolice_Terceiros_Apolice_ID                          Numeric(9,0)            " & vbNewLine
'    sSQL = sSQL & "     , Seguradora_Nome                                       VarChar(60)             " & vbNewLine
'    sSQL = sSQL & "     , Cosseguro_Ramo_Nome                                   VarChar(60)             " & vbNewLine
'    sSQL = sSQL & "     , Sucursal_Seguradora_Nome                              VarChar(60)             " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Endereco                                     VarChar(60)             " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Bairro                                       VarChar(30)             " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Estado                                       VarChar(20)             " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Municipio                                    VarChar(30)             " & vbNewLine
'    sSQL = sSQL & "     , Passivel_Ressarcimento                                Char(1)                 " & vbNewLine       'AKIO.OKUNO - 02/10/2012
'
'    sSQL = sSQL & "     ) " & vbNewLine
'    sSQL = sSQL & "     " & vbNewLine
'    sSQL = sSQL & "Create Index PK_Sinistro_ID                          on #Sinistro_Principal (Sinistro_ID)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_Proposta_ID                          on #Sinistro_Principal (Proposta_ID)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_Evento_Sinistro_ID                   on #Sinistro_Principal (Evento_Sinistro_ID)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_SubEvento_Sinistro_ID                on #Sinistro_Principal (SubEvento_Sinistro_ID)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_A                                    on #Sinistro_Principal (Sucursal_Seguradora_ID, Seguradora_Cod_Susep, Apolice_ID, Ramo_ID)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_Seguradora_Cod_Susep                 on #Sinistro_Principal (Seguradora_Cod_Susep)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_Cliente_ID                           on #Sinistro_Principal (Cliente_ID)" & vbNewLine
'    sSQL = sSQL & "Create Index FK_Solicitante_ID                       on #Sinistro_Principal (Solicitante_ID)" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_BB'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Sinistro_BB" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & " Select Sinistro_Bb_Tb.Sinistro_BB" & vbNewLine
'    sSQL = sSQL & "      , Sinistro_Bb_Tb.Sinistro_ID" & vbNewLine
'    sSQL = sSQL & "      , Sinistro_Bb_Tb.Dt_Fim_Vigencia " & vbNewLine
'    sSQL = sSQL & "   Into #Sinistro_BB" & vbNewLine
'    sSQL = sSQL & "   From Sinistro_Bb_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "   JOin #Sinistro_Principal                                          " & vbNewLine
'    sSQL = sSQL & "     on #Sinistro_Principal.Sinistro_Id = Sinistro_BB_Tb.Sinistro_ID " & vbNewLine
'    sSQL = sSQL & "  Where Dt_Fim_Vigencia is Null                                      " & vbNewLine
'    '    sSql = sSql & "  Where Sinistro_ID =  " & lblSinistro_ID(0) & vbNewLine
'    '    sSql = sSql & "    and Dt_Fim_Vigencia is null" & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Entrada_GTR'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Entrada_GTR" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & "Select Entrada_GTR_Tb.*" & vbNewLine
'    sSQL = sSQL & "  Into #Entrada_GTR" & vbNewLine
'    sSQL = sSQL & "  From Interface_Db..Entrada_GTR_Tb Entrada_GTR_Tb  WITH (NOLOCK) " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_BB " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_BB.Sinistro_BB = Entrada_GTR_Tb.Sinistro_BB" & vbNewLine
'    sSQL = sSQL & "   and Evento_BB         = 2000" & vbNewLine
'
'    'Para identificar se visualiza ou n�o bot�o de Analise t�cnica
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Seguro_Fraude_Historico'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Seguro_Fraude_Historico" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
'    sSQL = sSQL & "Select Top 1 Fase_ID                                             " & vbNewLine
'    sSQL = sSQL & "     , Nivel_ID                                                  " & vbNewLine
'    sSQL = sSQL & "  Into #Seguro_Fraude_Historico                                  " & vbNewLine
'    sSQL = sSQL & "  From Seguro_Fraude_Db..Historico_Tb Historico_Tb  WITH (NOLOCK)       " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                       " & vbNewLine
'    sSQL = sSQL & "    ON #Sinistro_Principal.Sinistro_ID = Historico_Tb.Sinistro_ID" & vbNewLine
'    '    sSql = sSql & " Where Sinistro_ID = " & IIf(IsNull(lblSinistro_ID(0)) Or lblSinistro_ID(0) = "", "0", lblSinistro_ID(0)) & vbNewLine
'    sSQL = sSQL & " Order By Historico_Tb.Dt_Inclusao Desc" & vbNewLine
'
'
'    sSQL = sSQL & "Select Evento_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Evento_SegBr_ID                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Evento_BB_Id                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Num_Recibo                                                                                    " & vbNewLine
'
'    sSQL = sSQL & "  Into #Evento_SegBR_Sinistro_Atual_Temp                                                             " & vbNewLine
'    sSQL = sSQL & "  From evento_SEGBR_sinistro_atual_tb                                                                " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_BB                                                                                  " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_BB.Sinistro_ID  = evento_SEGBR_sinistro_atual_tb.Sinistro_ID                        " & vbNewLine
'    sSQL = sSQL & "                                                                                                     " & vbNewLine
'    sSQL = sSQL & "Insert into #Evento_SegBR_Sinistro_Atual_Temp                                                        " & vbNewLine
'    sSQL = sSQL & "Select Evento_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Evento_SegBr_ID                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Evento_BB_Id                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Num_Recibo                                                                                    " & vbNewLine
'    sSQL = sSQL & "  From evento_SEGBR_sinistro_hist_2004_tb                                                            " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_BB                                                                                  " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_BB.Sinistro_ID  = evento_SEGBR_sinistro_hist_2004_tb.Sinistro_ID                    " & vbNewLine
'    sSQL = sSQL & "                                                                                                     " & vbNewLine
'    sSQL = sSQL & "Insert into #Evento_SegBR_Sinistro_Atual_Temp                                                        " & vbNewLine
'    sSQL = sSQL & "Select Evento_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Evento_SegBr_ID                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Evento_BB_Id                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Num_Recibo                                                                                    " & vbNewLine
'    sSQL = sSQL & "  From evento_SEGBR_sinistro_hist_2003_tb                                                            " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_BB                                                                                  " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_BB.Sinistro_ID  = evento_SEGBR_sinistro_hist_2003_tb.Sinistro_ID                    " & vbNewLine
'    sSQL = sSQL & "                                                                                                     " & vbNewLine
'    sSQL = sSQL & "Select Evento_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Evento_SegBr_ID                                                                               " & vbNewLine
'    sSQL = sSQL & "  Into #Evento_SegBR_Sinistro_Atual                                                                  " & vbNewLine
'    sSQL = sSQL & "  From #Evento_SegBR_Sinistro_Atual_Temp                                                             " & vbNewLine
'    sSQL = sSQL & " Where #Evento_SegBR_Sinistro_Atual_Temp.Evento_BB_Id is Not Null                                    " & vbNewLine
'    sSQL = sSQL & "  and (#Evento_SegBR_Sinistro_Atual_Temp.Num_Recibo is Null                                          " & vbNewLine
'    sSQL = sSQL & "      or #Evento_SegBR_Sinistro_Atual_Temp.Num_Recibo = 0                                            " & vbNewLine
'    sSQL = sSQL & "      or #Evento_SegBR_Sinistro_Atual_Temp.Evento_BB_ID = 2009)                                      " & vbNewLine
'    sSQL = sSQL & "  and #Evento_SegBR_Sinistro_Atual_Temp.Evento_BB_ID Not In (1150, 1151, 1152, 1153, 1154, 1181)     " & vbNewLine
'    sSQL = sSQL & "                                                                                                     " & vbNewLine
'    sSQL = sSQL & "Select Evento_ID = Max(isnull(#Evento_SegBR_Sinistro_Atual.Evento_ID,0))                             " & vbNewLine
'    sSQL = sSQL & "  Into #Evento_ID                                                                                    " & vbNewLine
'    sSQL = sSQL & "  From Evento_SegBr_Tb                                                                               " & vbNewLine
'    sSQL = sSQL & "  Join #Evento_SegBR_Sinistro_Atual                                                                  " & vbNewLine
'    sSQL = sSQL & "    on #Evento_SegBR_Sinistro_Atual.Evento_SegBr_ID = Evento_SegBr_Tb.Evento_SegBr_ID                " & vbNewLine
'    sSQL = sSQL & " Where Evento_SegBr_Tb.Prox_Localizacao_Processo is Not Null                                         " & vbNewLine
'    sSQL = sSQL & "                                                                                                     " & vbNewLine
'
'
'    'AKIOOKUNO
'    'sSQL = sSQL & "Select * Into ##AKIO_Evento_ID From #Evento_ID" & vbNewLine
'    'sSQL = sSQL & "Select * Into ##AKIO_Sinistro_BB from #Sinistro_BB" & vbNewLine
'    sSQL = sSQL & "Select Prox_Localizacao_Processo                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Evento_Segbr_Tb.Evento_BB_ID                                                                  " & vbNewLine
'    sSQL = sSQL & "     , #Sinistro_BB.Sinistro_BB                                                                      " & vbNewLine
'    sSQL = sSQL & "  Into #Localizacao                                                                                  " & vbNewLine
'    sSQL = sSQL & "  From Sinistro_Tb Sinistro_Tb  WITH (NOLOCK)                                                               " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_BB #Sinistro_BB  WITH (NOLOCK)                                                             " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_BB.Sinistro_ID = Sinistro_Tb.Sinistro_ID                                            " & vbNewLine
'    sSQL = sSQL & "  Left Join #Evento_ID                                                                               " & vbNewLine
'    sSQL = sSQL & " on #Evento_ID.Evento_ID <> 0                                                                         " & vbNewLine
'    sSQL = sSQL & "  Join Evento_SegBr_Sinistro_Tb Evento_SegBr_Sinistro_Tb  WITH (NOLOCK)                                     " & vbNewLine
'    sSQL = sSQL & "    on Evento_SegBr_Sinistro_Tb.Sinistro_ID = #Sinistro_BB.Sinistro_ID                               " & vbNewLine
'    sSQL = sSQL & "   and Evento_SegBr_Sinistro_Tb.Sinistro_BB = #Sinistro_BB.Sinistro_BB                               " & vbNewLine
'    sSQL = sSQL & "   and Evento_SegBr_Sinistro_Tb.Evento_ID = #Evento_ID.Evento_ID                                     " & vbNewLine
'    sSQL = sSQL & "  Join Evento_SegBr_Tb Evento_SegBr_Tb  WITH (NOLOCK)                                                       " & vbNewLine
'    sSQL = sSQL & "    on Evento_SegBr_Tb.Evento_SegBr_Id = Evento_SegBr_Sinistro_Tb.Evento_SegBr_ID                    "
'    sSQL = sSQL & " Where #Sinistro_BB.Dt_Fim_Vigencia is Null                                                          " & vbNewLine
'
'    sSQL = sSQL & "   and Evento_SegBr_Sinistro_Tb.Evento_BB_ID not in(1150,1151,1152,1153,1154,1181)   " & vbNewLine
'    sSQL = sSQL & "   and Evento_SegBr_Tb.prox_localizacao_processo is not null                         " & vbNewLine
'
'
'    'AKIOOKUNO
'    'sSQL = sSQL & "Select * Into ##AKIO_Localizacao From #Localizacao"
'
'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
'    sSQL = sSQL & "Create Table #Seguro_Fraude_Historico " & vbNewLine
'    sSQL = sSQL & "     ( Fase_ID                        int         " & vbNewLine
'    sSQL = sSQL & "       , Nivel_ID                     int )        " & vbNewLine
'
'    Exit Sub
'
'TrataErro:
'    Call TrataErroGeral("SelecionaDados_form", Me.Caption)
'    Call FinalizarAplicacao
'
'
'End Sub
''
''Private Sub SelecionaDados_AbaDadosGerais()
''    Dim sSQL As String
''
''    On Error GoTo Erro
''
''    MousePointer = vbHourglass
''
''    sSQL = ""
''
''' Alan Neves - 15/02/2013  -- As tabelas s�o criadas e � chamado proc para realizar tarefas
''
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Principal'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''    sSQL = sSQL & "Create Table #Sinistro_Principal "
''    sSQL = sSQL & "     ( Sinistro_ID                                   Numeric(11)         " & vbNewLine   ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Sucursal_Seguradora_ID                        Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Seguradora_Cod_Susep                          Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Apolice_ID                                    Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Ramo_ID                                       Int                 " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Ramo_Nome                                     VarChar(60)         " & vbNewLine  ' -- Ramo_Tb
''    sSQL = sSQL & "     , Seguradora_Nome_Lider                         VarChar(60)         " & vbNewLine  ' -- Seguradora_Tb (Nome Lider)
''    sSQL = sSQL & "     , Apolice_Dt_Inicio_Vigencia                    SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
''    sSQL = sSQL & "     , Apolice_Dt_Fim_Vigencia                       SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
''    sSQL = sSQL & "     , Apolice_Dt_Emissao                            SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
''    sSQL = sSQL & "     , Proposta_ID                                   Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Produto_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb
''    sSQL = sSQL & "     , Produto_Nome                                  VarChar(60)         " & vbNewLine  ' -- Produto_Tb
''    sSQL = sSQL & "     , Certificado_Dt_Inicio_Vigencia                SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
''    sSQL = sSQL & "     , Certificado_Dt_Fim_Vigencia                   SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
''    sSQL = sSQL & "     , Certificado_Dt_Emissao                         SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
''    sSQL = sSQL & "     , Certificado_ID                                Numeric(9)          " & vbNewLine  ' -- Certificado_Tb
''    sSQL = sSQL & "     , Certificado_Sub_Grupo_ID                      Int                 " & vbNewLine  ' -- seguro_vida_sub_grupo_tb
''    sSQL = sSQL & "     , Quantidade_Endossos                           Int                 " & vbNewLine  ' -- Count Endosso_Tb ???
''    sSQL = sSQL & "     , Proposta_Situacao                             VarChar(1)          " & vbNewLine  ' -- Proposta_Tb
''    sSQL = sSQL & "     , Proposta_Situacao_Descricao                   VarChar(60)         " & vbNewLine  ' -- Dado_Basico_Db..Situacao_Proposta_tb.Descricao
''    sSQL = sSQL & "     , Tp_Cobertura_Nome                             VarChar(60)         " & vbNewLine  ' -- Tp_Cobertura_Tb
''    sSQL = sSQL & "     , Corretor_Nome                                 VarChar(60)         " & vbNewLine  ' -- Corretor_Tb
''    sSQL = sSQL & "     , Cliente_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb (Prop_Cliente_ID)
''    sSQL = sSQL & "     , Cliente_Nome                                  VarChar(60)         " & vbNewLine  ' -- Cliente_Tb
''    sSQL = sSQL & "     , Tp_Componente_ID                              TinyInt             " & vbNewLine  ' -- Escolha_Tp_Cob_Vida_Tp
''    sSQL = sSQL & "     , Tp_Componente_Nome                            VarChar(60)         " & vbNewLine  ' -- Tp_Componente_Tb
''    sSQL = sSQL & "     , Evento_Sinistro_ID                            Int                 " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Evento_Sinistro_Nome                          VarChar(60)         " & vbNewLine  ' -- Evento_Sinistro_Tb
''    sSQL = sSQL & "     , SubEvento_Sinistro_ID                         Int                 " & vbNewLine  ' -- Sinistro_Tb
''    'cristovao.rodrigues 04/01/2013 mudanca para VarChar(120)
''    'sSQL = sSQL & "     , SubEvento_Sinistro_Nome                       VarChar(60)         " & vbNewLine  ' -- SubEvento_Sinistro_Tb
''    sSQL = sSQL & "     , SubEvento_Sinistro_Nome                       VarChar(120)         " & vbNewLine  ' -- SubEvento_Sinistro_Tb
''    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                        SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Dt_Entrada_Seguradora                         SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Dt_Aviso_Sinistro                             SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Dt_Inclusao                                   SmallDateTIme       " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Agencia_ID                                    Numeric(4)          " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Sinistro_Vida_Nome                            VarChar(60)         " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
''    sSQL = sSQL & "     , Sinistro_Vida_CPF                             VarChar(11)         " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
''    sSQL = sSQL & "     , Sinistro_Vida_Dt_Nasc                         DateTime            " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
''    sSQL = sSQL & "     , Sinistro_Vida_Sexo                            Char(1)             " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
''    sSQL = sSQL & "     , Sinistrado_Cliente_ID                         Int                 " & vbNewLine  ' -- Pessoa_Fisica_Tb (Pf_Cliente_ID) (Sinistrado)
''    sSQL = sSQL & "     , Solicitante_ID                                Int                 " & vbNewLine  ' -- Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Nome                     VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Endereco                 VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Bairro                   VarChar(30)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio_ID             Numeric(3)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio                VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Municipio_Nome                    VarChar(60)         " & vbNewLine  ' -- Municipio_Tb (Nome) (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD1                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax                  VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax             VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_Estado                   Char(2)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_EMail                    VarChar(60)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''    sSQL = sSQL & "     , Solicitante_Sinistro_CEP                      VarChar(8)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
''
''    sSQL = sSQL & "     , Sinistro_Causa_Observacao_Item                Int                 " & vbNewLine  ' -- Sinistro_Causa_Observacao_Tb
''    sSQL = sSQL & "     , Sinistro_Causa_Observacao                     VarChar(300)        " & vbNewLine  ' -- Sinistro_Causa_Observacao_Tb
''    sSQL = sSQL & "     , Sinistro_Situacao_ID                          Int                 " & vbNewLine  ' -- Sinistro_Tb
''    sSQL = sSQL & "     , Sinistro_Situacao                             VarChar(60)         " & vbNewLine  ' -- Dado_Basico_Db..Sinistro_Situacao_Tb
''    sSQL = sSQL & "     , Max_Endosso_ID                                Int                 " & vbNewLine  ' -- Endosso_Tb
''    'Para consulta
''    sSQL = sSQL & "     , Apolice_Terceiros_Num_Ordem_Co_Seguro_Aceito          Numeric(11,0)           " & vbNewLine
''    sSQL = sSQL & "     , Apolice_Terceiros_Seg_Cod_Susep_Lider                 Numeric(5,0)            " & vbNewLine
''    sSQL = sSQL & "     , Apolice_Terceiros_Ramo_ID                             Int                     " & vbNewLine
''    sSQL = sSQL & "     , Apolice_Terceiros_Sucursal_Seg_Lider                  Numeric(5,0)            " & vbNewLine
''    'Campos do frame Cosseguro
''    'mathayde
''    sSQL = sSQL & "     , Cosseguro_Aceito_sinistro_id_lider                    Numeric(13)           " & vbNewLine
''    sSQL = sSQL & "     , Cosseguro_Aceito_Num_Apolice_Lider                    Numeric(12,0)           " & vbNewLine
''    sSQL = sSQL & "     , Cosseguro_Aceito_Ramo_Lider                           Int                     " & vbNewLine
''    sSQL = sSQL & "     , Cosseguro_Aceito_Sucursal_Seg_Lider                   Numeric(5,0)            " & vbNewLine
''    sSQL = sSQL & "     , Cosseguro_Aceito_Seg_Cod_Susep_Lider                  Numeric(5,0)            " & vbNewLine
''    sSQL = sSQL & "     , Apolice_Terceiros_Apolice_ID                          Numeric(9,0)            " & vbNewLine
''    sSQL = sSQL & "     , Seguradora_Nome                                       VarChar(60)             " & vbNewLine
''    sSQL = sSQL & "     , Cosseguro_Ramo_Nome                                   VarChar(60)             " & vbNewLine
''    sSQL = sSQL & "     , Sucursal_Seguradora_Nome                              VarChar(60)             " & vbNewLine
''    sSQL = sSQL & "     , Sinistro_Endereco                                     VarChar(60)             " & vbNewLine
''    sSQL = sSQL & "     , Sinistro_Bairro                                       VarChar(30)             " & vbNewLine
''    sSQL = sSQL & "     , Sinistro_Estado                                       VarChar(20)             " & vbNewLine
''    sSQL = sSQL & "     , Sinistro_Municipio                                    VarChar(30)             " & vbNewLine
''    sSQL = sSQL & "     , Passivel_Ressarcimento                                Char(1)                 " & vbNewLine       'AKIO.OKUNO - 02/10/2012
''
''    sSQL = sSQL & "     ) " & vbNewLine
''    sSQL = sSQL & "     " & vbNewLine
''    sSQL = sSQL & "Create Index PK_Sinistro_ID                          on #Sinistro_Principal (Sinistro_ID)" & vbNewLine
''    sSQL = sSQL & "Create Index FK_Proposta_ID                          on #Sinistro_Principal (Proposta_ID)" & vbNewLine
''    sSQL = sSQL & "Create Index FK_Evento_Sinistro_ID                   on #Sinistro_Principal (Evento_Sinistro_ID)" & vbNewLine
''    sSQL = sSQL & "Create Index FK_SubEvento_Sinistro_ID                on #Sinistro_Principal (SubEvento_Sinistro_ID)" & vbNewLine
''    sSQL = sSQL & "Create Index FK_A                                    on #Sinistro_Principal (Sucursal_Seguradora_ID, Seguradora_Cod_Susep, Apolice_ID, Ramo_ID)" & vbNewLine
''    sSQL = sSQL & "Create Index FK_Seguradora_Cod_Susep                 on #Sinistro_Principal (Seguradora_Cod_Susep)" & vbNewLine
''    sSQL = sSQL & "Create Index FK_Cliente_ID                           on #Sinistro_Principal (Cliente_ID)" & vbNewLine
''    sSQL = sSQL & "Create Index FK_Solicitante_ID                       on #Sinistro_Principal (Solicitante_ID)" & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
''
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_BB'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #Sinistro_BB" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''    sSQL = sSQL & "Create Table #Sinistro_BB " & vbNewLine
''    sSQL = sSQL & "     ( Sinistro_BB                        Numeric(13)         " & vbNewLine
''    sSQL = sSQL & "     , Sinistro_ID                        Numeric(11)         " & vbNewLine
''    sSQL = sSQL & "     , Dt_Fim_Vigencia                    smalldatetime )     " & vbNewLine
''
''
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Entrada_GTR'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #Entrada_GTR" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''
''    sSQL = sSQL & "Select Entrada_GTR_Tb.* " & vbNewLine
''    sSQL = sSQL & " Into #Entrada_GTR   " & vbNewLine
''    sSQL = sSQL & " From Interface_Db..Entrada_GTR_Tb Entrada_GTR_Tb  WITH (NOLOCK)    " & vbNewLine
''    sSQL = sSQL & " where 1 = 0  " & vbNewLine
''
''    ' Para identificar se visualiza ou n�o bot�o de Analise t�cnica
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Seguro_Fraude_Historico'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #Seguro_Fraude_Historico" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''
''    sSQL = sSQL & "Create Table #Seguro_Fraude_Historico " & vbNewLine
''    sSQL = sSQL & "     ( Fase_ID                        int         " & vbNewLine
''    sSQL = sSQL & "       , Nivel_ID                     int )        " & vbNewLine
''
''
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Evento_SegBR_Sinistro_Atual_Temp'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #Evento_SegBR_Sinistro_Atual_Temp" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''    sSQL = sSQL & "create table #Evento_SegBR_Sinistro_Atual_Temp ( " & vbNewLine
''    sSQL = sSQL & "   Evento_id         int " & vbNewLine
''    sSQL = sSQL & " , Evento_SegBr_ID   int  " & vbNewLine
''    sSQL = sSQL & " , Evento_BB_Id      int  " & vbNewLine
''    sSQL = sSQL & " , Num_Recibo        tinyint )" & vbNewLine
''
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Evento_SegBR_Sinistro_Atual'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #Evento_SegBR_Sinistro_Atual" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''    sSQL = sSQL & "Select Evento_ID " & vbNewLine
''    sSQL = sSQL & " , Evento_SegBr_ID  " & vbNewLine
''    sSQL = sSQL & " Into #Evento_SegBR_Sinistro_Atual " & vbNewLine
''    sSQL = sSQL & " From #Evento_SegBR_Sinistro_Atual_Temp " & vbNewLine
''    sSQL = sSQL & " Where 1 = 0" & vbNewLine
''
''
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Evento_ID'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #Evento_ID" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''    sSQL = sSQL & "Create table #Evento_ID ( " & vbNewLine
''    sSQL = sSQL & " Evento_ID int ) " & vbNewLine
''
''
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Localizacao'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #Localizacao" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''    sSQL = sSQL & "Create Table #Localizacao " & vbNewLine
''    sSQL = sSQL & "     ( prox_localizacao_processo          char(1)           " & vbNewLine
''    sSQL = sSQL & "     , Evento_BB_ID                       Int               " & vbNewLine
''    sSQL = sSQL & "     , Sinistro_BB                        Numeric(13) )     " & vbNewLine
''
''
''' Alan Neves - 15/02/2013
''
'''    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'''                             glAmbiente_id, _
'''                             App.Title, _
'''                             App.FileDescription, _
'''                             sSQL, _
'''                             lConexaoLocal, _
'''                             False)
'''
'''
'''
'''    '-->> Dados do Frame: Seguro                                                                                                                                                                                                             & vbNewLine
'''    '-- Dados do Sinistro                                                                                                                                                                                                                    & vbNewLine
'''    sSQL = ""
'''    sSQL = sSQL & "Insert into #Sinistro_Principal " & vbNewLine
'''    sSQL = sSQL & "          ( Sucursal_Seguradora_ID           " & vbNewLine
'''    sSQL = sSQL & "          , Seguradora_Cod_Susep             " & vbNewLine
'''    sSQL = sSQL & "          , Apolice_ID                       " & vbNewLine
'''    sSQL = sSQL & "          , Ramo_ID                          " & vbNewLine
'''    sSQL = sSQL & "          , Proposta_ID                      " & vbNewLine
'''    sSQL = sSQL & "          , Cliente_ID                       " & vbNewLine   'AKIO.OKUNO - 24/09/2012
'''    sSQL = sSQL & "          , Passivel_Ressarcimento           " & vbNewLine   'AKIO.OKUNO - 02/10/2012
'''    sSQL = sSQL & "          )" & vbNewLine
'''
'''    If (strTipoProcesso = strTipoProcesso_Avisar Or _
'''        strTipoProcesso = strTipoProcesso_Avisar_Cosseguro) And LenB(Trim(lblSinistro_ID(0).Caption)) = 0 Then
'''        sSQL = sSQL & "Select Proposta_Adesao_Tb.Sucursal_Seguradora_ID                                             " & vbNewLine
'''        sSQL = sSQL & "     , Proposta_Adesao_Tb.Seguradora_Cod_Susep                                               " & vbNewLine
'''        sSQL = sSQL & "     , Proposta_Adesao_Tb.Apolice_ID                                                         " & vbNewLine
'''        sSQL = sSQL & "     , Proposta_Adesao_Tb.Ramo_ID                                                            " & vbNewLine
'''        sSQL = sSQL & "     , Proposta_Adesao_Tb.Proposta_ID                                                        " & vbNewLine
'''        sSQL = sSQL & "     , Proposta_Tb.Prop_Cliente_ID                                                                " & vbNewLine
'''        sSQL = sSQL & "     , Passivel_Ressarcimento                            = ''                                " & vbNewLine           'AKIO.OKUNO - 02/10/2012
'''        sSQL = sSQL & "  From Seguros_db.Dbo.Proposta_Adesao_Tb                 Proposta_Adesao_Tb  WITH (NOLOCK)              " & vbNewLine
'''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Tb                        Proposta_Tb  WITH (NOLOCK)                     " & vbNewLine
'''        sSQL = sSQL & "    on Proposta_tb.Proposta_ID                           = Proposta_Adesao_Tb.Proposta_ID        " & vbNewLine
'''        sSQL = sSQL & " Where Proposta_Adesao_Tb.Proposta_ID                    = " & txtProposta_ID & vbNewLine
'''        sSQL = sSQL & "Union                                                                                        " & vbNewLine
'''        sSQL = sSQL & "Select Apolice_Tb.Sucursal_Seguradora_ID                                                     " & vbNewLine
'''        sSQL = sSQL & "     , Apolice_Tb.Seguradora_Cod_Susep                                                       " & vbNewLine
'''        sSQL = sSQL & "     , Apolice_Tb.Apolice_ID                                                                 " & vbNewLine
'''        sSQL = sSQL & "     , Apolice_Tb.Ramo_ID                                                                    " & vbNewLine
'''        sSQL = sSQL & "     , Proposta_Fechada_Tb.Proposta_ID                                                       " & vbNewLine
'''        sSQL = sSQL & "     , Proposta_Tb.Prop_Cliente_ID                                                                " & vbNewLine
'''        sSQL = sSQL & "     , Passivel_Ressarcimento                            = ''                                " & vbNewLine           'AKIO.OKUNO - 02/10/2012
'''        sSQL = sSQL & "  From Seguros_db.Dbo.Proposta_Fechada_Tb                Proposta_Fechada_Tb WITH (NOLOCK)          " & vbNewLine
'''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Apolice_Tb                         Apolice_Tb  WITH (NOLOCK)                      " & vbNewLine
'''        sSQL = sSQL & "    on Apolice_Tb.Proposta_ID                            = Proposta_Fechada_Tb.Proposta_ID   " & vbNewLine
'''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Tb                        Proposta_Tb  WITH (NOLOCK)                     " & vbNewLine
'''        sSQL = sSQL & "    on Proposta_tb.Proposta_ID                           = Proposta_Fechada_Tb.Proposta_ID        " & vbNewLine
'''        sSQL = sSQL & " Where Proposta_Fechada_Tb.Proposta_ID = " & txtProposta_ID & vbNewLine
'''    Else
'''        sSQL = sSQL & "Select Sucursal_Seguradora_ID                                                    " & vbNewLine
'''        sSQL = sSQL & "     , Seguradora_Cod_Susep                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Apolice_ID                                                                " & vbNewLine
'''        sSQL = sSQL & "     , Ramo_ID                                                                   " & vbNewLine
'''        sSQL = sSQL & "     , Proposta_ID                                                               " & vbNewLine
'''        sSQL = sSQL & "     , Cliente_ID                                                                " & vbNewLine
'''        sSQL = sSQL & "     , Passivel_Ressarcimento                                                    " & vbNewLine   'AKIO.OKUNO - 02/10/2012
'''        sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Tb                    Sinistro_Tb WITH (NOLOCK)  " & vbNewLine
'''        sSQL = sSQL & " Where Sinistro_ID = " & lblSinistro_ID(0).Caption & vbNewLine
'''    End If
'''    If LenB(Trim(lblSinistro_ID(0).Caption)) <> 0 Then
'''        sSQL = sSQL & "" & vbNewLine
'''        sSQL = sSQL & "Update #Sinistro_Principal                                                       " & vbNewLine
'''        sSQL = sSQL & "   Set Sinistro_ID             = Sinistro_Tb.Sinistro_ID                         " & vbNewLine
'''        sSQL = sSQL & "     , Evento_Sinistro_ID      = Sinistro_Tb.Evento_Sinistro_ID                  " & vbNewLine
'''        sSQL = sSQL & "     , SubEvento_Sinistro_ID   = Sinistro_Tb.SubEvento_Sinistro_ID               " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro  = Sinistro_Tb.Dt_Ocorrencia_Sinistro              " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Entrada_Seguradora   = Sinistro_Tb.Dt_Entrada_Seguradora               " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Aviso_Sinistro       = Sinistro_Tb.Dt_Aviso_Sinistro                   " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Inclusao             = Sinistro_Tb.Dt_Inclusao                         " & vbNewLine
'''        sSQL = sSQL & "     , Agencia_ID              = Sinistro_Tb.Agencia_ID                          " & vbNewLine
'''        sSQL = sSQL & "     , Solicitante_ID          = Sinistro_Tb.Solicitante_ID                      " & vbNewLine
'''        sSQL = sSQL & "     , Sinistro_Endereco       = Sinistro_Tb.Endereco                            " & vbNewLine
'''        sSQL = sSQL & "     , Sinistro_Bairro         = Sinistro_Tb.Bairro                              " & vbNewLine
'''        sSQL = sSQL & "     , Sinistro_Municipio      = Sinistro_Tb.Municipio                           " & vbNewLine
'''        sSQL = sSQL & "     , Sinistro_Estado         = Sinistro_Tb.Estado                              " & vbNewLine
'''        sSQL = sSQL & "     , Sinistro_Situacao_ID    = Sinistro_Tb.Situacao                            " & vbNewLine
'''        'mathayde
'''        sSQL = sSQL & "     , Cosseguro_Aceito_sinistro_id_lider = isnull(Sinistro_Tb.sinistro_id_lider,0)" & vbNewLine
'''        sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Tb                    Sinistro_Tb  WITH (NOLOCK)         " & vbNewLine
'''        sSQL = sSQL & " Where Sinistro_Tb.Sinistro_ID = " & lblSinistro_ID(0).Caption & vbNewLine
'''    End If
'''
'''    '--Situacao Sinistro                                                                                                                                                                                                                     & vbNewLine
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Sinistro_Situacao                             = Sinistro_Situacao_Tb.Situacao" & vbNewLine
'''    sSQL = sSQL & "  From Dado_Basico_Db..Sinistro_Situacao_Tb          Sinistro_Situacao_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_Situacao_ID      = Sinistro_Situacao_Tb.Situacao_ID" & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''    '-- Ramo_Tb                                                                                                                                                                                                                              & vbNewLine
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Ramo_Nome                                     = Ramo_Tb.Nome" & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Ramo_Tb                        Ramo_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & " on #Sinistro_Principal.Ramo_ID                      = Ramo_Tb.Ramo_ID" & vbNewLine
'''    '-- Lider                                                                                                                                                                                                                                & vbNewLine
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Seguradora_Nome_Lider                         = Seguradora_Tb.Nome" & vbNewLine
'''    'sSQL = sSQL & "  From Seguros_Db.Dbo.Seguradora_Tb                  Seguradora_Tb (NoLock, Index = PK_seguradora)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Seguradora_Tb                  Seguradora_Tb  WITH (NOLOCK) " & vbNewLine
'''    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Seguradora_Cod_Susep)" & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Seguradora_Cod_Susep      = Seguradora_Tb.Seguradora_Cod_Susep " & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''    '-- Solicitante                                                                                                                                                                                                                          & vbNewLine
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Solicitante_Sinistro_Nome                     = Solicitante_Sinistro_Tb.Nome" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Endereco                 = Solicitante_Sinistro_Tb.Endereco" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Bairro                   = Solicitante_Sinistro_Tb.Bairro" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio_ID             = Solicitante_Sinistro_Tb.Municipio_ID" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio                = Solicitante_Sinistro_Tb.Municipio " & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD1                     = Solicitante_Sinistro_Tb.DDD" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1                = Solicitante_Sinistro_Tb.Telefone" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                     = Solicitante_Sinistro_Tb.DDD1" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2                = Solicitante_Sinistro_Tb.Telefone1" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                     = Solicitante_Sinistro_Tb.DDD2" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3                = Solicitante_Sinistro_Tb.Telefone2" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                     = Solicitante_Sinistro_Tb.DDD3" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4                = Solicitante_Sinistro_Tb.Telefone3" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                     = Solicitante_Sinistro_Tb.DDD4" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5                = Solicitante_Sinistro_Tb.Telefone4" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1             = Solicitante_Sinistro_Tb.Tp_Telefone" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2             = Solicitante_Sinistro_Tb.Tp_Telefone1" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3             = Solicitante_Sinistro_Tb.Tp_Telefone2" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4             = Solicitante_Sinistro_Tb.Tp_Telefone3" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5             = Solicitante_Sinistro_Tb.Tp_Telefone4" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1                   = Solicitante_Sinistro_Tb.Ramal" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2                   = Solicitante_Sinistro_Tb.Ramal1" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3                   = Solicitante_Sinistro_Tb.Ramal2" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4                   = Solicitante_Sinistro_Tb.Ramal3" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5                   = Solicitante_Sinistro_Tb.Ramal4" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Estado                   = Case When Solicitante_Sinistro_Tb.Estado = 'XX' or Solicitante_Sinistro_Tb.Estado = '--' " & vbNewLine
'''    sSQL = sSQL & "                                                            Then ''" & vbNewLine
'''    sSQL = sSQL & "                                                            Else Solicitante_Sinistro_Tb.Estado" & vbNewLine
'''    sSQL = sSQL & "                                                       End" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax                  = Solicitante_Sinistro_Tb.DDD_Fax" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax             = Solicitante_Sinistro_Tb.Telefone_Fax" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_CEP                      = Solicitante_Sinistro_Tb.CEP" & vbNewLine
'''    sSQL = sSQL & "     , Solicitante_Sinistro_Email                    = Solicitante_Sinistro_Tb.Email" & vbNewLine
'''    'sSQL = sSQL & "  From Seguros_Db.Dbo.Solicitante_Sinistro_Tb        Solicitante_Sinistro_Tb (NoLock, Index = PK_solicitante_sinistro)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Solicitante_Sinistro_Tb        Solicitante_Sinistro_Tb  WITH (NOLOCK) " & vbNewLine
'''    If (strTipoProcesso = strTipoProcesso_Avisar Or _
'''        strTipoProcesso = strTipoProcesso_Avisar_Cosseguro) And _
'''        LenB(Trim(lblSinistro_ID(0))) <> 0 Then
'''        sSQL = sSQL & " Where Solicitante_Sinistro_Tb.Solicitante_ID = " & lAviso_Solicitante_ID & vbNewLine
'''    Else
'''        sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Solicitante_ID            = Solicitante_Sinistro_Tb.Solicitante_ID" & vbNewLine
'''    End If
'''
'''
'''    sSQL = sSQL & "" & vbNewLine
'''    '-- Dados do municipio
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   set Solicitante_Municipio_Nome                    = Municipio_Tb.Nome" & vbNewLine
'''    sSQL = sSQL & "  From #Sinistro_Principal                           " & vbNewLine
'''    'sSQL = sSQL & "  Join Seguros_Db.Dbo.Municipio_Tb                   Municipio_Tb (NoLock, Index = PK_municipio)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'''    sSQL = sSQL & "  Join Seguros_Db.Dbo.Municipio_Tb                   Municipio_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "    on Municipio_Tb.Municipio_ID                     = #Sinistro_Principal.Solicitante_Sinistro_Municipio_ID" & vbNewLine
'''    '-- Dados da Proposta                                                                                                                                                                                                                    & vbNewLine
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Produto_ID                                    = Proposta_Tb.Produto_ID" & vbNewLine
'''    sSQL = sSQL & "     , Proposta_Situacao                             = Proposta_Tb.Situacao" & vbNewLine
'''    '    sSQL = sSQL & "     , Cliente_ID                                    = Proposta_Tb.Prop_Cliente_ID" & vbNewLine
'''    sSQL = sSQL & "     , Quantidade_Endossos                           = Proposta_Tb.Num_Endosso" & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'''    sSQL = sSQL & "    on Proposta_Tb.Proposta_ID                       = #Sinistro_Principal.Proposta_ID" & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''
'''    'AKIO.OKUNO - INICIO = 23/09/2012
'''    sSQL = sSQL & "Update #Sinistro_Principal                                                                                                                       " & vbNewLine
'''    'sSQL = sSQL & "   Set Cliente_ID                                            = isNull(Proposta_Complementar_Tb.Prop_Cliente_ID, Proposta_Tb.Prop_Cliente_ID)     " & vbNewLine 'F.BEZERRA - 17/10/2012
'''    sSQL = sSQL & "   Set Cliente_ID                                            = isNull(Proposta_Complementar_Tb.Prop_Cliente_ID, isnull(Proposta_Tb.Prop_Cliente_ID,#Sinistro_Principal.Cliente_ID))" & vbNewLine
'''    sSQL = sSQL & "  From #Sinistro_Principal                                                                                                                       " & vbNewLine
'''    sSQL = sSQL & "  Left Join Seguros_db.Dbo.Proposta_Tb                       Proposta_Tb  WITH (NOLOCK)                                                                 " & vbNewLine
'''    sSQL = sSQL & "    on Proposta_Tb.Proposta_ID                               = #Sinistro_Principal.Proposta_ID                                                   " & vbNewLine
'''    sSQL = sSQL & "   and Proposta_Tb.Prop_Cliente_ID                           = #Sinistro_Principal.Cliente_ID                                                    " & vbNewLine
'''    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Complementar_Tb          Proposta_Complementar_Tb  WITH (NOLOCK)                                                    " & vbNewLine
'''    sSQL = sSQL & "    on Proposta_Complementar_Tb.Proposta_ID                  = #Sinistro_Principal.Proposta_ID                                                   " & vbNewLine
'''    sSQL = sSQL & "   and Proposta_Complementar_Tb.Prop_Cliente_ID              = #Sinistro_Principal.Cliente_ID                                                    " & vbNewLine
'''    'AKIO.OKUNO - FIM - 23/09/2012
'''
'''    '-- Dados do Produto
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   set Produto_Nome                                  = Produto_Tb.Nome   " & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Produto_Tb                     Produto_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Produto_ID                = Produto_Tb.Produto_ID " & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''    '-- Situacao Proposta                                                                                                                                                                                                                    & vbNewLine
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Proposta_Situacao_Descricao                   = Situacao_Proposta_Tb.Descricao_Situacao" & vbNewLine
'''    sSQL = sSQL & "  From Dado_Basico_Db..Situacao_Proposta_Tb          Situacao_Proposta_Tb  WITH (NOLOCK)  " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_Situacao         = upper(Situacao_Proposta_Tb.Situacao)" & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''
'''    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'''                             glAmbiente_id, _
'''                             App.Title, _
'''                             App.FileDescription, _
'''                             sSQL, _
'''                             lConexaoLocal, _
'''                             False)
'''
'''
'''    '-- Dados da Apolice                                                                                                                                                                                                                     & vbNewLine
'''    sSQL = ""
'''    sSQL = sSQL & "Declare @Max_Endosso_ID                              int" & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''    sSQL = sSQL & "Select @Max_Endosso_ID                               = Max(Endosso_Tb.Endosso_ID)" & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Endosso_Tb Endosso_Tb           WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Endosso_Tb.Proposta_ID" & vbNewLine
'''    sSQL = sSQL & " Where Endosso_Tb.Tp_Endosso_ID                      = 250" & vbNewLine
'''    sSQL = sSQL & " " & vbNewLine
'''
'''    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Max_Endosso_ID = @Max_Endosso_ID" & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''
'''    Select Case bytTipoRamo
'''    Case bytTipoRamo_Vida
'''        sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'''        sSQL = sSQL & "   Set Apolice_Dt_Inicio_Vigencia                    = Proposta_Adesao_Tb.Dt_Inicio_Vigencia" & vbNewLine
'''        sSQL = sSQL & "  From #Sinistro_Principal" & vbNewLine
'''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Adesao_Tb             Proposta_Adesao_Tb  WITH (NOLOCK) "
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Proposta_Adesao_Tb.Proposta_ID" & vbNewLine
'''        sSQL = sSQL & " Where #Sinistro_Principal.Produto_ID                = 721" & vbNewLine
'''        sSQL = sSQL & "" & vbNewLine
'''
'''    Case bytTipoRamo_RE, bytTipoRamo_Rural
'''        sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'''        sSQL = sSQL & "   Set Apolice_Dt_Inicio_Vigencia                    = Apolice_Tb.Dt_Inicio_Vigencia" & vbNewLine
'''        sSQL = sSQL & "  From #Sinistro_Principal" & vbNewLine
'''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Apolice_Tb                     Apolice_Tb  WITH (NOLOCK) "
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Apolice_Tb.Proposta_ID" & vbNewLine
'''        sSQL = sSQL & " Where #Sinistro_Principal.Produto_ID                <> 721" & vbNewLine
'''        sSQL = sSQL & "" & vbNewLine
'''
'''    End Select
'''
'''    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Apolice_Dt_Fim_Vigencia                       = Endosso_Tb.Dt_Fim_Vigencia_End" & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Endosso_Tb                     Endosso_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Endosso_Tb.Proposta_ID" & vbNewLine
'''    sSQL = sSQL & " Where Endosso_Tb.Endosso_ID                         = @Max_Endosso_ID" & vbNewLine
'''    sSQL = sSQL & "   and Endosso_Tb.Tp_Endosso_ID                      = 250" & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Produto_ID                = 721" & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''
'''    sSQL = sSQL & "Select Proposta_Adesao_Tb.Proposta_ID" & vbNewLine
'''    sSQL = sSQL & "     , Proposta_Adesao_Tb.Dt_Inicio_Vigencia" & vbNewLine
'''    sSQL = sSQL & "     , Proposta_Adesao_Tb.Dt_Fim_Vigencia" & vbNewLine
'''    sSQL = sSQL & "  Into #Proposta_Adesao" & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Adesao_Tb             Proposta_Adesao_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Proposta_Adesao_Tb.Proposta_ID" & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Apolice_Dt_Inicio_Vigencia                    = Dt_Inicio_Vigencia" & vbNewLine
'''    sSQL = sSQL & "  , Apolice_Dt_Fim_Vigencia                          = Dt_Fim_Vigencia" & vbNewLine
'''    sSQL = sSQL & "  From #Proposta_Adesao" & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = #Proposta_Adesao.Proposta_ID " & vbNewLine
'''    sSQL = sSQL & "   and (#Sinistro_Principal.Apolice_Dt_Inicio_Vigencia is null" & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Apolice_Dt_Fim_Vigencia  is null" & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Produto_ID               = 721 )" & vbNewLine
'''    sSQL = sSQL & "    or (#Sinistro_Principal.Produto_ID               <> 721)" & vbNewLine
'''    sSQL = sSQL & " where Apolice_Dt_Inicio_Vigencia is null" & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''
''''AKIO.OKUNO - INICIO - 11/12/2012
'''Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'''                         glAmbiente_id, _
'''                         App.Title, _
'''                         App.FileDescription, _
'''                         sSQL, _
'''                         lConexaoLocal, _
'''                         False)
'''sSQL = ""
''''AKIO.OKUNO - FIM - 11/12/2012
'''    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Apolice_Dt_Inicio_Vigencia                    = Apolice_Tb.Dt_Inicio_Vigencia" & vbNewLine
'''    sSQL = sSQL & "     , Apolice_Dt_Fim_Vigencia                       = Apolice_Tb.Dt_Fim_Vigencia" & vbNewLine
'''    sSQL = sSQL & "     , Apolice_Dt_Emissao                                = Apolice_Tb.Dt_Emissao" & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Apolice_Tb Apolice_Tb           WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Apolice_ID                = Apolice_Tb.Apolice_ID" & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Sucursal_Seguradora_ID    = Apolice_Tb.Sucursal_Seguradora_ID" & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Seguradora_Cod_Susep      = Apolice_Tb.Seguradora_Cod_Susep" & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Ramo_ID                   = Apolice_Tb.Ramo_ID" & vbNewLine
'''    sSQL = sSQL & "  left Join #Proposta_Adesao" & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = #Proposta_Adesao.Proposta_ID " & vbNewLine
'''    sSQL = sSQL & " Where #Sinistro_Principal.Produto_ID                <> 721" & vbNewLine
'''    sSQL = sSQL & "   and #Proposta_Adesao.Proposta_ID                  is null" & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
''''AKIO.OKUNO - INICIO - 11/12/2012
'''Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'''                         glAmbiente_id, _
'''                         App.Title, _
'''                         App.FileDescription, _
'''                         sSQL, _
'''                         lConexaoLocal, _
'''                         False)
'''sSQL = ""
''''AKIO.OKUNO - FIM - 11/12/2012
'''
'''
'''
'''    'DADOS DO SINISTRADO" & vbNewLine
'''    sSQL = sSQL & "Update #Sinistro_Principal " & vbNewLine
'''    sSQL = sSQL & "   Set Sinistro_Vida_Nome                            = IsNull(Sinistro_Vida_Tb.Nome, '')" & vbNewLine
'''    sSQL = sSQL & "     , Sinistro_Vida_CPF                             = IsNull(Sinistro_Vida_Tb.Cpf, '')" & vbNewLine
'''    sSQL = sSQL & "     , Sinistro_Vida_Dt_Nasc                         = IsNull(Sinistro_Vida_Tb.Dt_Nascimento, '')" & vbNewLine
'''    sSQL = sSQL & "     , Sinistro_Vida_Sexo                            = IsNull(Sinistro_Vida_Tb.Sexo, '') " & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Vida_Tb               Sinistro_Vida_Tb  WITH (NOLOCK) " & vbNewLine
'''    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = PK_Sinistro_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'''    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Vida_Tb.Sinistro_ID" & vbNewLine
'''    sSQL = sSQL & "     " & vbNewLine
'''
'''
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Sinistrado_Cliente_ID                         = Pessoa_Fisica_Tb.Pf_Cliente_ID" & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Pessoa_Fisica_Tb               Pessoa_Fisica_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal " & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_Vida_CPF         = Pessoa_Fisica_Tb.CPF" & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Sinistro_Vida_Dt_Nasc     = Pessoa_Fisica_Tb.Dt_Nascimento" & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Sinistro_Vida_Sexo        = Pessoa_Fisica_Tb.Sexo" & vbNewLine
'''
'''
'''    '-- Dados do Certificado                                                                                                                                                                                                                 & vbNewLine
'''    'AKIO.OKUNO - INICIO - 24/09/2012
'''    If bytTipoRamo = bytTipoRamo_Vida Then
'''        sSQL = sSQL & "Update #Sinistro_Principal                                                                           " & vbNewLine
'''        sSQL = sSQL & "   Set Certificado_Dt_Inicio_Vigencia                    = Certificado_Tb.Dt_Inicio_Vigencia         " & vbNewLine
'''        '        sSql = sSql & "     , Certificado_Dt_Fim_Vigencia                       = Certificado_Tb.Dt_Fim_Vigencia             " & vbNewLine 'AKIO.OKUNO - 01/10/2012 (CONFORME SOLICITADO PELO USUARIO/MARCEL)
'''        sSQL = sSQL & "     , Certificado_Dt_Emissao                            = Certificado_Tb.Dt_Emissao                  " & vbNewLine
'''        sSQL = sSQL & "     , Certificado_ID                                    = Certificado_Tb.Certificado_ID             " & vbNewLine
'''        sSQL = sSQL & "     , Certificado_Sub_Grupo_ID                          = Seguro_Vida_Sub_Grupo_Tb.Sub_Grupo_ID     " & vbNewLine
'''        sSQL = sSQL & "  From Seguros_Db.Dbo.Seguro_Vida_Sub_Grupo_Tb           Seguro_Vida_Sub_Grupo_Tb   WITH (NOLOCK)           " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                           " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID                   = Seguro_Vida_Sub_Grupo_Tb.Proposta_ID      " & vbNewLine
'''        sSQL = sSQL & "   and #Sinistro_Principal.Sinistrado_Cliente_ID         = Seguro_Vida_Sub_Grupo_Tb.Prop_Cliente_ID      " & vbNewLine
'''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Certificado_Tb                     Certificado_Tb  WITH (NOLOCK)                          " & vbNewLine
'''        sSQL = sSQL & "    on Certificado_Tb.Proposta_ID                        = Seguro_Vida_Sub_Grupo_Tb.Proposta_ID      " & vbNewLine
'''        sSQL = sSQL & "   and Certificado_Tb.Certificado_ID                     = Seguro_Vida_Sub_Grupo_Tb.Certificado_ID       " & vbNewLine
'''
'''        'AKIO.OKUNO - INICIO - 01/10/2012
'''        sSQL = sSQL & "update #Sinistro_Principal                                                               " & vbNewLine
'''        sSQL = sSQL & "   set Certificado_Dt_Fim_Vigencia = ISNULL(sgp.dt_fim_vigencia_sbg, '')                 " & vbNewLine
'''        sSQL = sSQL & "from                                                                                     " & vbNewLine
'''        sSQL = sSQL & "seguro_vida_sub_grupo_tb sgp WITH (NOLOCK)                                                      " & vbNewLine
'''        sSQL = sSQL & "   INNER JOIN cliente_tb cl   WITH (NOLOCK)                                                     " & vbNewLine
'''        sSQL = sSQL & "       ON  sgp.prop_cliente_id = cl.cliente_id                                           " & vbNewLine
'''        sSQL = sSQL & "   INNER JOIN pessoa_fisica_tb pf   WITH (NOLOCK)                                               " & vbNewLine
'''        sSQL = sSQL & "       ON  sgp.prop_cliente_id = pf.pf_cliente_id                                        " & vbNewLine
'''        sSQL = sSQL & "   Join #Sinistro_Principal                                                              " & vbNewLine
'''        sSQL = sSQL & "  on #Sinistro_Principal.Certificado_Sub_Grupo_ID= sgp.sub_grupo_id                      " & vbNewLine
'''        sSQL = sSQL & "    and #Sinistro_Principal.apolice_id               = sgp.apolice_id                    " & vbNewLine
'''        sSQL = sSQL & "    and #Sinistro_Principal.sucursal_seguradora_id   = sgp.sucursal_seguradora_id        " & vbNewLine
'''        sSQL = sSQL & "    and #Sinistro_Principal.seguradora_cod_susep = sgp.seguradora_cod_susep              " & vbNewLine
'''        sSQL = sSQL & "    and #Sinistro_Principal.ramo_id                  = sgp.ramo_id                       " & vbNewLine
'''        sSQL = sSQL & "    and #Sinistro_Principal.Sinistrado_Cliente_ID   = sgp.prop_Cliente_ID                " & vbNewLine
'''        sSQL = sSQL & " WHERE                                                                                   " & vbNewLine
'''        sSQL = sSQL & " sgp.dt_inicio_vigencia_sbg =        (Select TOP 1 sgp2.dt_inicio_vigencia_sbg           " & vbNewLine
'''        sSQL = sSQL & "       from seguro_vida_sub_grupo_tb sgp2   WITH (NOLOCK)                                       " & vbNewLine
'''        sSQL = sSQL & "       WHERE   sgp2.apolice_id = sgp.apolice_id                                          " & vbNewLine
'''        sSQL = sSQL & "                                                                                         " & vbNewLine
'''        sSQL = sSQL & "           AND sgp2.sucursal_seguradora_id = sgp.sucursal_seguradora_id                  " & vbNewLine
'''        sSQL = sSQL & "           AND sgp2.seguradora_cod_susep = sgp.seguradora_cod_susep                      " & vbNewLine
'''        sSQL = sSQL & "           AND sgp2.ramo_id = sgp.ramo_id                                                " & vbNewLine
'''        sSQL = sSQL & "           AND sgp2.sub_grupo_id = sgp.sub_grupo_id                                      " & vbNewLine
'''        sSQL = sSQL & "           AND sgp2.dt_inicio_vigencia_apol_sbg = sgp.dt_inicio_vigencia_apol_sbg        " & vbNewLine
'''        sSQL = sSQL & "           AND sgp2.proposta_id = sgp.proposta_id                                        " & vbNewLine
'''        sSQL = sSQL & "           AND sgp2.prop_cliente_id = sgp.prop_cliente_id                                " & vbNewLine
'''        sSQL = sSQL & "           AND sgp2.tp_componente_id = sgp.tp_componente_id                              " & vbNewLine
'''        sSQL = sSQL & "           AND sgp2.seq_canc_endosso_seg = sgp.seq_canc_endosso_seg                      " & vbNewLine
'''        sSQL = sSQL & "ORDER BY sgp2.dt_inicio_vigencia_sbg DESC)                                               " & vbNewLine
'''        'AKIO.OKUNO - FIM - 01/10/2012
'''
'''        sSQL = sSQL & "Update #Sinistro_Principal                                                                                   " & vbNewLine
'''        sSQL = sSQL & "   Set Corretor_Nome = Corretor_Tb.Nome                                                                      " & vbNewLine
'''        sSQL = sSQL & "  From Seguros_Db.Dbo.Corretor_Tb                        Corretor_Tb  WITH (NOLOCK)                                 " & vbNewLine
'''        sSQL = sSQL & "  join Seguros_Db.Dbo.Corretagem_Sub_Grupo_Tb            Corretagem_Sub_Grupo_Tb  WITH (NOLOCK)                     " & vbNewLine
'''        sSQL = sSQL & "    on Corretagem_Sub_Grupo_Tb.Corretor_ID               = Corretor_Tb.Corretor_ID                           " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                                   " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Apolice_ID                    = Corretagem_Sub_Grupo_Tb.Apolice_ID                " & vbNewLine
'''        sSQL = sSQL & "   and #Sinistro_Principal.Sucursal_Seguradora_ID        = Corretagem_Sub_Grupo_Tb.Sucursal_Seguradora_ID    " & vbNewLine
'''        sSQL = sSQL & "   and #Sinistro_Principal.Seguradora_Cod_Susep          = Corretagem_Sub_Grupo_Tb.Seguradora_Cod_Susep      " & vbNewLine
'''        sSQL = sSQL & "   and #Sinistro_Principal.Ramo_ID                       = Corretagem_Sub_Grupo_Tb.Ramo_ID                   " & vbNewLine
'''        sSQL = sSQL & "   and #Sinistro_Principal.Certificado_Sub_Grupo_ID      = Corretagem_Sub_Grupo_Tb.Sub_Grupo_ID              " & vbNewLine
'''        sSQL = sSQL & " Where COrretor_Tb.Situacao = 'a'                                                                            " & vbNewLine
'''
'''    Else
'''        'AKIO.OKUNO - FIM - 24/09/2012
'''        sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''        sSQL = sSQL & "   Set Certificado_Dt_Inicio_Vigencia                = Certificado_Tb.Dt_Inicio_Vigencia" & vbNewLine
'''        sSQL = sSQL & "     , Certificado_Dt_Fim_Vigencia                   = Certificado_Tb.Dt_Fim_Vigencia" & vbNewLine
'''        sSQL = sSQL & "     , Certificado_Dt_Emissao                        = Certificado_Tb.Dt_Emissao" & vbNewLine
'''        sSQL = sSQL & "     , Certificado_ID                                = Certificado_Tb.Certificado_ID             " & vbNewLine
'''        sSQL = sSQL & "  From Seguros_Db.Dbo.Certificado_Tb                 Certificado_Tb  WITH (NOLOCK) " & vbNewLine
'''        'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Proposta_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'''        sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'''        sSQL = sSQL & "    on Certificado_Tb.Proposta_ID                    = #Sinistro_Principal.Proposta_ID   " & vbNewLine
'''    End If      'AKIO.OKUNO - 24/09/2012
'''
'''    sSQL = sSQL & "Select Top 1 Corretor_Tb.Nome" & vbNewLine
'''    sSQL = sSQL & "  , Corretor_Tb.Situacao" & vbNewLine
'''    sSQL = sSQL & "  Into #Corretor" & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Corretor_Tb                    Corretor_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "  Join Seguros_Db.Dbo.Corretagem_Tb                  Corretagem_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "    on Corretagem_Tb.Corretor_ID                     = Corretor_Tb.Corretor_ID" & vbNewLine
'''    sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "    on Proposta_Tb.Proposta_ID                       = Corretagem_Tb.Proposta_ID" & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Produto_ID                = Proposta_Tb.Produto_ID" & vbNewLine
'''    sSQL = sSQL & " Where Corretor_Tb.Situacao = 'a'    " & vbNewLine
'''    sSQL = sSQL & "   and Certificado_Sub_Grupo_ID is null" & vbNewLine             'AKIO.OKUNO - 25/09/2012
'''
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Corretor_Nome                                 = #Corretor.Nome" & vbNewLine
'''    sSQL = sSQL & "  From #Corretor" & vbNewLine
'''    sSQL = sSQL & " Where Certificado_Sub_Grupo_ID is null" & vbNewLine             'AKIO.OKUNO - 25/09/2012
'''
'''    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'''                             glAmbiente_id, _
'''                             App.Title, _
'''                             App.FileDescription, _
'''                             sSQL, _
'''                             lConexaoLocal, _
'''                             False)
'''sSQL = ""
'''
'''    '-- Cliente_Tb                                                                                                                                                                                                                           & vbNewLine
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set #Sinistro_Principal.Cliente_Nome              = Cliente_Tb.Nome" & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Cliente_Tb                     Cliente_Tb  WITH (NOLOCK) " & vbNewLine
'''    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Cliente_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'''    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Cliente_ID                = Cliente_Tb.Cliente_ID " & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''
'''    '-- Tp_Cobertura_Tb                                                                                                                                                                                                                      & vbNewLine
'''    '-- Cobertura Principal                                                                                                                                                                                                                  & vbNewLine
'''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Tp_Componente_Nome                            = Case When Proposta_Complementar_Tb.Prop_Cliente_ID is null" & vbNewLine
'''    sSQL = sSQL & "                                                         Then 'TITULAR'" & vbNewLine
'''    sSQL = sSQL & "                                                         Else 'CONJUGE'" & vbNewLine
'''    sSQL = sSQL & "                                                       End" & vbNewLine
'''    sSQL = sSQL & "  From #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Complementar_Tb  Proposta_Complementar_Tb   WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "    on Proposta_Complementar_Tb.Prop_Cliente_ID      = #Sinistro_Principal.CLiente_ID" & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''
'''    If bytTipoRamo = bytTipoRamo_Vida Then
'''        sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''        sSQL = sSQL & "   Set Tp_Cobertura_Nome                             = Tp_Cobertura_Tb.Nome" & vbNewLine
'''        sSQL = sSQL & "     , Tp_Componente_ID                              = Escolha_Tp_Cob_Vida_Tb.Tp_Componente_ID " & vbNewLine
'''        sSQL = sSQL & "  From Seguros_Db.Dbo.Tp_Cob_Comp_Tb                 Tp_Cob_Comp_Tb  WITH (NOLOCK) " & vbNewLine
'''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Cobertura_Tb                Tp_Cobertura_Tb  WITH (NOLOCK) " & vbNewLine
'''        sSQL = sSQL & "    on Tp_Cobertura_Tb.Tp_Cobertura_ID               = Tp_Cob_Comp_Tb.Tp_Cobertura_ID" & vbNewLine
'''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Escolha_Tp_Cob_Vida_Tb         Escolha_Tp_Cob_Vida_Tb  WITH (NOLOCK) " & vbNewLine
'''        sSQL = sSQL & "    on Escolha_Tp_Cob_Vida_Tb.Tp_Cob_Comp_ID         = Tp_Cob_Comp_Tb.Tp_Cob_Comp_ID" & vbNewLine
'''        sSQL = sSQL & "   and Escolha_Tp_Cob_Vida_Tb.Tp_Componente_ID       = Tp_Cob_Comp_Tb.Tp_Componente_ID " & vbNewLine
'''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Componente_Tb               Tp_Componente_Tb  WITH (NOLOCK) " & vbNewLine
'''        sSQL = sSQL & "    on Tp_Componente_Tb.Tp_Componente_ID             = Tp_Cob_Comp_Tb.Tp_Componente_ID " & vbNewLine
'''        'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Proposta_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
'''        sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Escolha_Tp_Cob_Vida_Tb.Proposta_ID" & vbNewLine
'''        sSQL = sSQL & " Where Escolha_Tp_Cob_Vida_Tb.Class_Tp_Cobertura     = 'B'" & vbNewLine
'''        sSQL = sSQL & "" & vbNewLine
'''    Else
'''        'sSQL = ""
'''        'AKIO.OKUNO - INICIO - 25/09/2012
'''        sSQL = sSQL & "Select Tp_Cobertura_Nome = Tp_Cobertura_Tb.Nome                                                                                  " & vbNewLine
'''        sSQL = sSQL & "  Into #Cobertura_Nome"
'''        sSQL = sSQL & "  From Seguros_Db.Dbo.Tp_Cob_Item_Prod_Tb                    Tp_Cob_Item_Prod_Tb   WITH (NOLOCK)               " & vbNewLine
'''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Cobertura_Tb                        Tp_Cobertura_Tb  WITH (NOLOCK)                         " & vbNewLine
'''        sSQL = sSQL & "    on Tp_Cobertura_Tb.Tp_Cobertura_ID                       = Tp_Cob_Item_Prod_Tb.Tp_Cobertura_ID           " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                                   " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_principal.Ramo_ID                           = Tp_Cob_Item_Prod_Tb.Ramo_ID                   " & vbNewLine
'''        sSQL = sSQL & "   and #Sinistro_principal.Produto_ID                        = Tp_Cob_Item_Prod_Tb.Produto_ID                " & vbNewLine
'''        sSQL = sSQL & " Where Tp_Cob_Item_Prod_Tb.Class_Tp_Cobertura                = 'B'                                           " & vbNewLine
'''        sSQL = sSQL & " Order by Tp_Cobertura_Tb.Nome" & vbNewLine
'''
'''        '        sSQL = sSQL & " Update #Sinistro_Principal                                                                                   " & vbNewLine
'''        '        sSQL = sSQL & "   set Tp_Cobertura_Nome = Tp_Cobertura_Tb.Nome                                                                                  " & vbNewLine
'''        '        sSQL = sSQL & "  From Seguros_Db.Dbo.Tp_Cob_Item_Prod_Tb                    Tp_Cob_Item_Prod_Tb   WITH (NOLOCK)               " & vbNewLine
'''        '        sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Cobertura_Tb                        Tp_Cobertura_Tb  WITH (NOLOCK)                         " & vbNewLine
'''        '        sSQL = sSQL & "    on Tp_Cobertura_Tb.Tp_Cobertura_ID                       = Tp_Cob_Item_Prod_Tb.Tp_Cobertura_ID           " & vbNewLine
'''        '        sSQL = sSQL & "  Join #Sinistro_Principal                                                                                   " & vbNewLine
'''        '        sSQL = sSQL & "    on #Sinistro_principal.Ramo_ID                           = Tp_Cob_Item_Prod_Tb.Ramo_ID                   " & vbNewLine
'''        '        sSQL = sSQL & "   and #Sinistro_principal.Produto_ID                        = Tp_Cob_Item_Prod_Tb.Produto_ID                " & vbNewLine
'''        '        sSQL = sSQL & " Where Tp_Cob_Item_Prod_Tb.Class_Tp_Cobertura                = 'B'                                           " & vbNewLine
'''
'''        sSQL = sSQL & " Update #Sinistro_Principal                                                                                   " & vbNewLine
'''        sSQL = sSQL & "   set Tp_Cobertura_Nome = #Cobertura_Nome.Tp_Cobertura_Nome                                                                                  " & vbNewLine
'''        sSQL = sSQL & "  From #Cobertura_Nome                                                                                       " & vbNewLine
'''        sSQL = sSQL & " Drop table #Cobertura_Nome                                                                                  " & vbNewLine
'''        'AKIO.OKUNO - FIM - 25/09/2012
'''
'''    End If
'''    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'''                             glAmbiente_id, _
'''                             App.Title, _
'''                             App.FileDescription, _
'''                             sSQL, _
'''                             lConexaoLocal, _
'''                             False)
'''    '-->> Dados do Frame: Aviso                                                                                                                                                                                                              & vbNewLine
'''    '-- Evento_Sinistro_Tb                                                                                                                                                                                                                   & vbNewLine
'''    sSQL = ""
'''    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set Evento_Sinistro_Nome                          = Evento_Sinistro_Tb.Nome" & vbNewLine
'''    'sSQL = sSQL & "  From #Sinistro_Principal                           (Index = FK_Evento_Sinistro_ID)" & vbNewLine ' cristovao.rodrigues 05/11/2012 retirado index
'''    sSQL = sSQL & "  From #Sinistro_Principal                           " & vbNewLine
'''    sSQL = sSQL & "  Join Seguros_Db.Dbo.Evento_Sinistro_Tb             Evento_Sinistro_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "    on Evento_Sinistro_Tb.Evento_Sinistro_ID         = #Sinistro_Principal.Evento_Sinistro_ID " & vbNewLine
'''    sSQL = sSQL & "" & vbNewLine
'''    '-- SubEvento_Sinistro_Tb                                                                                                                                                                                                                & vbNewLine
'''    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
'''    sSQL = sSQL & "   Set SubEvento_Sinistro_Nome                       = SubEvento_Sinistro_Tb.Nome" & vbNewLine
'''    'sSQL = sSQL & "  From #Sinistro_Principal                           (Index = FK_SubEvento_Sinistro_ID)" & vbNewLine ' cristovao.rodrigues 05/11/2012 retirado index
'''    sSQL = sSQL & "  From #Sinistro_Principal                           " & vbNewLine
'''    sSQL = sSQL & "  Join Seguros_Db.Dbo.SubEvento_Sinistro_Tb          SubEvento_Sinistro_Tb  WITH (NOLOCK) " & vbNewLine
'''    sSQL = sSQL & "    on SubEvento_Sinistro_Tb.SubEvento_Sinistro_ID   = #Sinistro_Principal.SubEvento_Sinistro_ID " & vbNewLine
'''    sSQL = sSQL & "     " & vbNewLine
'''    'Dados da Grid: Aviso" & vbNewLine
'''    sSQL = sSQL & "Select Sinistro_BB_Tb.Sinistro_Bb" & vbNewLine
'''    sSQL = sSQL & "  , Sinistro_BB_Tb.Dt_Fim_Vigencia" & vbNewLine
'''    sSQL = sSQL & "  , Agencia_ID                                       = #Sinistro_Principal.Agencia_ID" & vbNewLine
'''    sSQL = sSQL & "  , Situacao                                         = Case When Sinistro_BB_Tb.Dt_Fim_Vigencia is Null" & vbNewLine
'''    sSQL = sSQL & "                                                       Then 'Ativo'" & vbNewLine
'''    sSQL = sSQL & "                                                       Else 'Inativo'" & vbNewLine
'''    sSQL = sSQL & "                                                       End" & vbNewLine
'''    sSQL = sSQL & "  Into #DadosGerais_Aviso" & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_BB_Tb                 Sinistro_BB_Tb  WITH (NOLOCK) " & vbNewLine
'''    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = PK_Sinistro_ID)" & vbNewLine ' cristovao.rodrigues 05/11/2012 retirado index
'''    sSQL = sSQL & "  Join #Sinistro_Principal                          " & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_BB_Tb.Sinistro_ID" & vbNewLine
'''    sSQL = sSQL & " Order By Sinistro_BB" & vbNewLine
'''    sSQL = sSQL & "     " & vbNewLine
'''    sSQL = sSQL & "     " & vbNewLine
'''
'''    'AKIO.OKUNO - INICIO - 24/09/2012
'''    '    'DADOS DO SINISTRADO" & vbNewLine
'''    '    sSQL = sSQL & "Update #Sinistro_Principal " & vbNewLine
'''    '    sSQL = sSQL & "   Set Sinistro_Vida_Nome                            = IsNull(Sinistro_Vida_Tb.Nome, '')" & vbNewLine
'''    '    sSQL = sSQL & "     , Sinistro_Vida_CPF                             = IsNull(Sinistro_Vida_Tb.Cpf, '')" & vbNewLine
'''    '    sSQL = sSQL & "     , Sinistro_Vida_Dt_Nasc                         = IsNull(Sinistro_Vida_Tb.Dt_Nascimento, '')" & vbNewLine
'''    '    sSQL = sSQL & "     , Sinistro_Vida_Sexo                            = IsNull(Sinistro_Vida_Tb.Sexo, '') " & vbNewLine
'''    '    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Vida_Tb               Sinistro_Vida_Tb  WITH (NOLOCK) " & vbNewLine
'''    '    sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = PK_Sinistro_ID)" & vbNewLine
'''    '    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Vida_Tb.Sinistro_ID" & vbNewLine
'''    '    sSQL = sSQL & "     " & vbNewLine
'''    '
'''    '
'''    '    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
'''    '    sSQL = sSQL & "   Set Sinistrado_Cliente_ID                         = Pessoa_Fisica_Tb.Pf_Cliente_ID" & vbNewLine
'''    '    sSQL = sSQL & "  From Seguros_Db.Dbo.Pessoa_Fisica_Tb               Pessoa_Fisica_Tb  WITH (NOLOCK) " & vbNewLine
'''    '    sSQL = sSQL & "  Join #Sinistro_Principal " & vbNewLine
'''    '    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_Vida_CPF         = Pessoa_Fisica_Tb.CPF" & vbNewLine
'''    '    sSQL = sSQL & "   and #Sinistro_Principal.Sinistro_Vida_Dt_Nasc     = Pessoa_Fisica_Tb.Dt_Nascimento" & vbNewLine
'''    '    sSQL = sSQL & "   and #Sinistro_Principal.Sinistro_Vida_Sexo        = Pessoa_Fisica_Tb.Sexo" & vbNewLine
'''    'AKIO.OKUNO - INICIO - 25/09/2012
'''
'''    'DADOS DO COSSEGURO
'''    sSQL = sSQL & "UPdate #Sinistro_Principal                                                                                   " & vbNewLine
'''    sSQL = sSQL & "   Set Apolice_Terceiros_Apolice_ID                      = Apolice_Terceiros_tb.apolice_id                   " & vbNewLine
'''    sSQL = sSQL & "     , Apolice_Terceiros_Num_Ordem_Co_Seguro_Aceito      = Apolice_Terceiros_Tb.num_ordem_co_seguro_aceito   " & vbNewLine
'''    sSQL = sSQL & "     , Apolice_Terceiros_Seg_Cod_Susep_Lider             = Apolice_Terceiros_tb.seg_cod_susep_lider          " & vbNewLine
'''    sSQL = sSQL & "     , Apolice_Terceiros_Ramo_ID                         = Apolice_Terceiros_Tb.Ramo_ID                      " & vbNewLine
'''    sSQL = sSQL & "     , Apolice_Terceiros_Sucursal_Seg_Lider              = Apolice_Terceiros_Tb.Sucursal_Seg_Lider           " & vbNewLine
'''    sSQL = sSQL & "  from Seguros_Db.Dbo.Apolice_Terceiros_Tb               Apolice_Terceiros_Tb  WITH (NOLOCK)                        " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                   " & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Apolice_ID                    = Apolice_Terceiros_Tb.Apolice_ID                   " & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Ramo_ID                       = Apolice_Terceiros_Tb.Ramo_ID                      " & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Sucursal_Seguradora_ID        = Apolice_Terceiros_Tb.Sucursal_Seguradora_ID       " & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Seguradora_Cod_Susep          = Apolice_Terceiros_Tb.Seguradora_Cod_Susep         " & vbNewLine
'''
'''    sSQL = sSQL & "UPdate #Sinistro_Principal                                                                                   " & vbNewLine
'''    sSQL = sSQL & "   Set Cosseguro_Aceito_Num_Apolice_Lider                = Co_seguro_Aceito_tb.num_apolice_lider             " & vbNewLine
'''    sSQL = sSQL & "     , Cosseguro_Aceito_Ramo_Lider                       = Co_seguro_Aceito_tb.ramo_lider                    " & vbNewLine
'''    sSQL = sSQL & "     , Cosseguro_Aceito_Sucursal_Seg_Lider               = Co_seguro_Aceito_tb.sucursal_seg_lider            " & vbNewLine
'''    sSQL = sSQL & "     , Cosseguro_Aceito_Seg_Cod_Susep_Lider              = Co_seguro_Aceito_tb.seg_cod_susep_lider           " & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Co_Seguro_Aceito_tb                Co_Seguro_Aceito_tb  WITH (NOLOCK)                                             " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                       " & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Apolice_Terceiros_Num_Ordem_Co_Seguro_Aceito      = Co_seguro_Aceito_Tb.num_ordem_co_seguro_aceito    " & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Apolice_Terceiros_Seg_Cod_Susep_Lider             = Co_seguro_Aceito_Tb.seg_cod_susep_lider           " & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Apolice_Terceiros_Sucursal_Seg_Lider              = Co_seguro_Aceito_Tb.sucursal_seg_lider            " & vbNewLine
'''
'''    sSQL = sSQL & "UPdate #Sinistro_Principal                                                                                                       " & vbNewLine
'''    sSQL = sSQL & "   Set Seguradora_Nome                                   = Seguradora_Tb.Nome                                                    " & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Seguradora_Tb                      Seguradora_Tb  WITH (NOLOCK)                                                   " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                       " & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Cosseguro_Aceito_Seg_Cod_Susep_Lider = Seguradora_Tb.Seguradora_Cod_Susep                              " & vbNewLine
'''
'''    sSQL = sSQL & "Update #Sinistro_Principal                                                                               " & vbNewLine
'''    sSQL = sSQL & "   Set Cosseguro_Ramo_Nome                               = Ramo_Tb.Nome                                  " & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Ramo_Tb Ramo_Tb  WITH (NOLOCK)                                                            " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal                                                                               " & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Cosseguro_Aceito_Ramo_Lider = Ramo_Tb.Ramo_ID                                 " & vbNewLine
'''
'''    sSQL = sSQL & "Update #Sinistro_Principal                                                                               " & vbNewLine
'''    sSQL = sSQL & "   Set Sucursal_Seguradora_Nome                          = Sucursal_Seguradora_Tb.Nome                   " & vbNewLine
'''    sSQL = sSQL & "  From Seguros_Db.Dbo.Sucursal_Seguradora_Tb             Sucursal_Seguradora_Tb  WITH (NOLOCK)                  " & vbNewLine
'''    sSQL = sSQL & "  Join #Sinistro_Principal                                                                               " & vbNewLine
'''    sSQL = sSQL & "    on #Sinistro_Principal.Cosseguro_Aceito_Sucursal_Seg_Lider = Sucursal_Seguradora_Tb.Sucursal_Seguradora_ID        " & vbNewLine
'''    sSQL = sSQL & "   and #Sinistro_Principal.Cosseguro_Aceito_Seg_Cod_Susep_Lider = Sucursal_Seguradora_Tb.Seguradora_Cod_Susep " & vbNewLine
'''
'''    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'''                             glAmbiente_id, _
'''                             App.Title, _
'''                             App.FileDescription, _
'''                             sSQL, _
'''                             lConexaoLocal, _
'''                             False)
'''
'''
''    If bytTipoRamo = bytTipoRamo_RE Then
'''        sSQL = ""
''        sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Principal_Endereco_Risco'),0) >0" & vbNewLine
''        sSQL = sSQL & " BEGIN" & vbNewLine
''        sSQL = sSQL & "     DROP TABLE #Sinistro_Principal_Endereco_Risco" & vbNewLine
''        sSQL = sSQL & " END" & vbNewLine
''
'''        sSQL = sSQL & "SELECT cod_objeto_segurado       = b.cod_objeto_segurado                                     " & vbNewLine
'''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                                " & vbNewLine
'''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                    " & vbNewLine
'''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Municipio                 = a.municipio                                                   " & vbNewLine
'''        sSQL = sSQL & "     , Estado                    = a.estado                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = b.dt_inicio_vigencia_seg                                      " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = b.dt_fim_vigencia_seg                                         " & vbNewLine
'''        sSQL = sSQL & "     , Tipo                      = 1                                                         " & vbNewLine
'''        sSQL = sSQL & "  Into #Sinistro_Principal_Endereco_Risco                                                        " & vbNewLine
'''        sSQL = sSQL & "  FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_residencial_tb b  WITH (NOLOCK)                 " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                       " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = b.Proposta_ID                                           " & vbNewLine
'''        sSQL = sSQL & " Where a.Proposta_id = b.Proposta_id                                                         " & vbNewLine
'''        sSQL = sSQL & "   AND a.end_risco_id = b.end_risco_id                                                           " & vbNewLine
'''        sSQL = sSQL & "   AND b.dt_fim_vigencia_seg IS NULL                                                         " & vbNewLine
'''        sSQL = sSQL & " Union                                                                                        " & vbNewLine
'''        sSQL = sSQL & "SELECT cod_objeto_segurado       = c.cod_objeto_segurado                                      " & vbNewLine
'''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                             " & vbNewLine
'''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                 " & vbNewLine
'''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                   " & vbNewLine
'''        sSQL = sSQL & "     , Municipio                 = a.municipio                                                " & vbNewLine
'''        sSQL = sSQL & "     , Estado                    = a.estado                                                   " & vbNewLine
'''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = c.dt_inicio_vigencia_seg                                   " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = c.dt_fim_vigencia_seg                                      " & vbNewLine
'''        sSQL = sSQL & "     , Tipo                      = 2                                                          " & vbNewLine
'''        sSQL = sSQL & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_empresarial_tb c  WITH (NOLOCK)                   " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                    " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = c.Proposta_ID                                        " & vbNewLine
'''        sSQL = sSQL & " Where a.Proposta_id = c.Proposta_id                                                      " & vbNewLine
'''        sSQL = sSQL & " AND a.end_risco_id = c.end_risco_id                                                      " & vbNewLine
'''        sSQL = sSQL & " AND c.dt_fim_vigencia_seg IS NULL                                                            " & vbNewLine
'''        sSQL = sSQL & " Union                                                                                        " & vbNewLine
'''        sSQL = sSQL & "SELECT cod_objeto_segurado       = d.cod_objeto_segurado                                      " & vbNewLine
'''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                             " & vbNewLine
'''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                 " & vbNewLine
'''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                   " & vbNewLine
'''        sSQL = sSQL & "     , Municipio                 = a.municipio                                                " & vbNewLine
'''        sSQL = sSQL & "     , Estado                    = a.estado                                                   " & vbNewLine
'''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = d.dt_inicio_vigencia_seg                                   " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = d.dt_fim_vigencia_seg                                      " & vbNewLine
'''        sSQL = sSQL & "     , Tipo                      = 3                                                           " & vbNewLine
'''        sSQL = sSQL & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_condominio_tb d  WITH (NOLOCK)                    " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                    " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = d.Proposta_ID                                        " & vbNewLine
'''        sSQL = sSQL & " Where a.Proposta_id = d.Proposta_id                                                      " & vbNewLine
'''        sSQL = sSQL & " AND a.end_risco_id = d.end_risco_id                                                      " & vbNewLine
'''        sSQL = sSQL & " AND d.dt_fim_vigencia_seg IS NULL                                                            " & vbNewLine
'''        sSQL = sSQL & " Union                                                                                        " & vbNewLine
'''        sSQL = sSQL & "SELECT cod_objeto_segurado       = e.cod_objeto_segurado                                      " & vbNewLine
'''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                             " & vbNewLine
'''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                 " & vbNewLine
'''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                   " & vbNewLine
'''        sSQL = sSQL & "     , Municipio                 = a.municipio                                                " & vbNewLine
'''        sSQL = sSQL & "     , Estado                    = a.estado                                                   " & vbNewLine
'''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = e.dt_inicio_vigencia_seg                                   " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = e.dt_fim_vigencia_seg                                      " & vbNewLine
'''        sSQL = sSQL & "     , Tipo                      = 4                                                          " & vbNewLine
'''        sSQL = sSQL & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_maquinas_tb e  WITH (NOLOCK)                      " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                    " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = e.Proposta_ID                                        " & vbNewLine
'''        sSQL = sSQL & " Where a.Proposta_id = e.Proposta_id                                                      " & vbNewLine
'''        sSQL = sSQL & " AND a.end_risco_id = e.end_risco_id                                                      " & vbNewLine
'''        sSQL = sSQL & " AND e.dt_fim_vigencia_seg IS NULL                                                            " & vbNewLine
'''        sSQL = sSQL & " Union                                                                                        " & vbNewLine
'''        sSQL = sSQL & "SELECT cod_objeto_segurado       = f.cod_objeto_segurado                                      " & vbNewLine
'''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                             " & vbNewLine
'''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                 " & vbNewLine
'''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                   " & vbNewLine
'''        sSQL = sSQL & "     , Municipio                 = a.municipio                                                " & vbNewLine
'''        sSQL = sSQL & "     , Estado                    = a.estado                                                   " & vbNewLine
'''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = f.dt_inicio_vigencia_seg                                   " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = f.dt_fim_vigencia_seg                                      " & vbNewLine
'''        sSQL = sSQL & "     , Tipo                      = 5                                                          " & vbNewLine
'''        sSQL = sSQL & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_aceito_tb f  WITH (NOLOCK)                    " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = f.Proposta_ID                                       " & vbNewLine
'''        sSQL = sSQL & " Where a.Proposta_id = f.Proposta_id                                                     " & vbNewLine
'''        sSQL = sSQL & " AND a.end_risco_id = f.end_risco_id                                                     " & vbNewLine
'''        sSQL = sSQL & " AND f.dt_fim_vigencia_seg IS NULL                                                           " & vbNewLine
'''        sSQL = sSQL & " Union                                                                                       " & vbNewLine
'''        sSQL = sSQL & "SELECT cod_objeto_segurado       = g.cod_objeto_segurado                                     " & vbNewLine
'''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                            " & vbNewLine
'''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                " & vbNewLine
'''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                  " & vbNewLine
'''        sSQL = sSQL & "     , Municipio                 = a.municipio                                               " & vbNewLine
'''        sSQL = sSQL & "     , Estado                    = a.estado                                                  " & vbNewLine
'''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = g.dt_inicio_vigencia_seg                                  " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = g.dt_fim_vigencia_seg                                     " & vbNewLine
'''        sSQL = sSQL & "     , Tipo                      = 6                                                         " & vbNewLine
'''        sSQL = sSQL & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_avulso_tb g  WITH (NOLOCK)                   " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = g.Proposta_ID                                       " & vbNewLine
'''        sSQL = sSQL & " Where a.Proposta_id = g.Proposta_id                                                     " & vbNewLine
'''        sSQL = sSQL & " AND a.end_risco_id = g.end_risco_id                                                     " & vbNewLine
'''        sSQL = sSQL & " AND g.dt_fim_vigencia_seg IS NULL                                                           " & vbNewLine
'''        sSQL = sSQL & " Union                                                                                       " & vbNewLine
'''        sSQL = sSQL & "SELECT cod_objeto_segurado       = h.cod_objeto_segurado                                     " & vbNewLine
'''        sSQL = sSQL & "     , end_risco_id              = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Endereco                  = h.desc_mercadoria                                         " & vbNewLine
'''        sSQL = sSQL & "     , Bairro                    = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Municipio                 = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Estado                    = NULL                                                       " & vbNewLine
'''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = h.dt_inicio_vigencia_seg                                  " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = h.dt_fim_vigencia_seg                                     " & vbNewLine
'''        sSQL = sSQL & "     , Tipo                      = 7                                                         " & vbNewLine
'''        sSQL = sSQL & " FROM seguro_transporte_tb h  WITH (NOLOCK)                                                     " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = h.Proposta_ID                                       " & vbNewLine
'''        sSQL = sSQL & " Where h.dt_fim_vigencia_seg IS NULL                                                     " & vbNewLine
'''        sSQL = sSQL & " Union                                                                                       " & vbNewLine
'''        sSQL = sSQL & "SELECT cod_objeto_segurado       = h.cod_objeto_segurado                                     " & vbNewLine
'''        sSQL = sSQL & "     , end_risco_id              = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Endereco                  = h.desc_mercadoria                                         " & vbNewLine
'''        sSQL = sSQL & "     , Bairro                    = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Municipio                 = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Estado                    = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = h.dt_inicio_vigencia_seg                                  " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = h.dt_fim_vigencia_seg                                     " & vbNewLine
'''        sSQL = sSQL & "     , Tipo                      = 7                                                         " & vbNewLine
'''        sSQL = sSQL & " FROM seguro_transporte_tb h  WITH (NOLOCK)                                                     " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = h.Proposta_ID                                       " & vbNewLine
'''        sSQL = sSQL & " Where dt_fim_vigencia_seg = (select max(dt_fim_vigencia_seg)                                " & vbNewLine
'''        sSQL = sSQL & "                                from seguro_transporte_tb h1  WITH (NOLOCK)                     " & vbNewLine
'''        sSQL = sSQL & "                                Join #Sinistro_Principal                                     " & vbNewLine
'''        sSQL = sSQL & "                                  on #Sinistro_Principal.Proposta_ID = h1.Proposta_ID        " & vbNewLine
'''        sSQL = sSQL & "                               Where h1.cod_objeto_segurado = h.cod_objeto_segurado          " & vbNewLine
'''        sSQL = sSQL & "                                 and h1.dt_fim_vigencia_seg is not null)                 " & vbNewLine
'''        sSQL = sSQL & " and not exists            (select 1 from seguro_transporte_tb h2  WITH (NOLOCK)                " & vbNewLine
'''        sSQL = sSQL & "                                Join #Sinistro_Principal                                     " & vbNewLine
'''        sSQL = sSQL & "                                  on #Sinistro_Principal.Proposta_ID = h2.Proposta_ID         " & vbNewLine
'''        sSQL = sSQL & "                               Where h2.cod_objeto_segurado = h.cod_objeto_segurado          " & vbNewLine
'''        sSQL = sSQL & "                                 and h2.dt_fim_vigencia_seg is null)                     " & vbNewLine
'''        sSQL = sSQL & " Union                                                                                       " & vbNewLine
'''        sSQL = sSQL & "SELECT cod_objeto_segurado       = i.cod_objeto_segurado                                     " & vbNewLine
'''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                            " & vbNewLine
'''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                " & vbNewLine
'''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                  " & vbNewLine
'''        sSQL = sSQL & "     , Municipio                 = a.municipio                                               " & vbNewLine
'''        sSQL = sSQL & "     , Estado                    = a.estado                                                  " & vbNewLine
'''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = i.dt_inicio_vigencia_seg                                  " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = i.dt_fim_vigencia_seg                                     " & vbNewLine
'''        sSQL = sSQL & "     , Tipo                      = 8                                                         " & vbNewLine
'''        sSQL = sSQL & " FROM endereco_risco_tb a  WITH (NOLOCK)  , seguro_generico_tb i  WITH (NOLOCK)                    " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = i.Proposta_ID                                       " & vbNewLine
'''        sSQL = sSQL & " Where a.Proposta_id = i.Proposta_id                                                     " & vbNewLine
'''        sSQL = sSQL & " AND a.end_risco_id = i.end_risco_id                                                     " & vbNewLine
'''        sSQL = sSQL & " AND i.dt_fim_vigencia_seg IS NULL                                                           " & vbNewLine
'''        sSQL = sSQL & " UNION                                                                                       " & vbNewLine
'''        sSQL = sSQL & "SELECT cod_objeto_segurado       = j.cod_objeto_segurado                                     " & vbNewLine
'''        sSQL = sSQL & "     , end_risco_id              = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Endereco                  = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Bairro                    = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Municipio                 = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Estado                    = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = j.dt_inicio_vigencia_seg                                  " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Tipo                      = 9                                                          " & vbNewLine
'''        sSQL = sSQL & " FROM seguro_penhor_rural_tb j  WITH (NOLOCK)                                               " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = j.Proposta_ID                                       " & vbNewLine
'''        sSQL = sSQL & " UNION                                                                                       " & vbNewLine
'''        sSQL = sSQL & "SELECT cod_objeto_segurado       = k.cod_objeto_segurado                                     " & vbNewLine
'''        sSQL = sSQL & "     , end_risco_id              = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Endereco                  = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Bairro                    = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Municipio                 = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Estado                    = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = k.dt_inicio_vigencia_seg                                  " & vbNewLine
'''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = NULL                                                      " & vbNewLine
'''        sSQL = sSQL & "     , Tipo                      = 10                                                        " & vbNewLine
'''        sSQL = sSQL & " FROM seguro_consorcio_quebra_garantia_tb k  WITH (NOLOCK)                                      " & vbNewLine
'''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
'''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = k.Proposta_ID                                       " & vbNewLine
'''
'''
'''        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'''                                 glAmbiente_id, _
'''                                 App.Title, _
'''                                 App.FileDescription, _
'''                                 sSQL, _
'''                                 lConexaoLocal, _
'''                                 False)
'''
''    End If
''
''' Alan Neves - 15/02/2013
''
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosGerais_Aviso'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #DadosGerais_Aviso" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''    sSQL = sSQL & "Create Table #DadosGerais_Aviso " & vbNewLine
''    sSQL = sSQL & "     ( Sinistro_Bb                           Numeric(13)         " & vbNewLine
''    sSQL = sSQL & "     , Dt_Fim_Vigencia                       smalldatetime       " & vbNewLine
''    sSQL = sSQL & "     , Agencia_ID                            Numeric(4)          " & vbNewLine
''    sSQL = sSQL & "     , Situacao                              varchar(20)  )      " & vbNewLine  ' -- Sinistro_Tb
''
''
'''        sSQL = ""
'''        sSQL = sSQL & "  Exec desenv_db.dbo.SelecionaDados_AbaDadosGerais_e_Form '" & txtProposta_ID & "','" & lblSinistro_ID(0).Caption & "'" & ", '" & lAviso_Solicitante_ID & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'," & bytTipoRamo & vbNewLine
''        sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10922_SPS '" & txtProposta_ID & "','" & lblSinistro_ID(0).Caption & "'" & ", '" & lAviso_Solicitante_ID & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'," & bytTipoRamo & vbNewLine
''
''        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
''                                 glAmbiente_id, _
''                                 App.Title, _
''                                 App.FileDescription, _
''                                 sSQL, _
''                                 lConexaoLocal, _
''                                 False)
''
''
''    Exit Sub
''
''Erro:
''    Call TrataErroGeral("SelecionaDados_AbaDadosGerais", Me.Caption)
''    Call FinalizarAplicacao
''
''End Sub


'ALAN.NEVES - SelecionaDados_AbaDadosGerais - 17/04/2013
Private Sub SelecionaDados_AbaDadosGerais()
    Dim sSQL As String

    On Error GoTo Erro

    MousePointer = vbHourglass

    sSQL = ""

    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Principal'),0) >0" & vbNewLine
    sSQL = sSQL & " if OBJECT_ID('tempdb..#Sinistro_Principal') IS NOT NULL " & vbNewLine
    sSQL = sSQL & " BEGIN" & vbNewLine
    sSQL = sSQL & "     DROP TABLE #Sinistro_Principal" & vbNewLine
    sSQL = sSQL & " END" & vbNewLine
    sSQL = sSQL & "Create Table #Sinistro_Principal "
    sSQL = sSQL & "     ( Sinistro_ID                                   Numeric(11)         " & vbNewLine   ' -- Sinistro_Tb
    sSQL = sSQL & "     , Sucursal_Seguradora_ID                        Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Seguradora_Cod_Susep                          Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Apolice_ID                                    Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Ramo_ID                                       Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Ramo_Nome                                     VarChar(60)         " & vbNewLine  ' -- Ramo_Tb
    sSQL = sSQL & "     , Seguradora_Nome_Lider                         VarChar(60)         " & vbNewLine  ' -- Seguradora_Tb (Nome Lider)
    sSQL = sSQL & "     , Apolice_Dt_Inicio_Vigencia                    SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
    sSQL = sSQL & "     , Apolice_Dt_Fim_Vigencia                       SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
    sSQL = sSQL & "     , Apolice_Dt_Emissao                            SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
    sSQL = sSQL & "     , Proposta_ID                                   Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Produto_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb
    sSQL = sSQL & "     , Produto_Nome                                  VarChar(60)         " & vbNewLine  ' -- Produto_Tb
    sSQL = sSQL & "     , Certificado_Dt_Inicio_Vigencia                SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
    sSQL = sSQL & "     , Certificado_Dt_Fim_Vigencia                   SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
    sSQL = sSQL & "     , Certificado_Dt_Emissao                         SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
    sSQL = sSQL & "     , Certificado_ID                                Numeric(9)          " & vbNewLine  ' -- Certificado_Tb
    sSQL = sSQL & "     , Certificado_Sub_Grupo_ID                      Int                 " & vbNewLine  ' -- seguro_vida_sub_grupo_tb
    sSQL = sSQL & "     , Quantidade_Endossos                           Int                 " & vbNewLine  ' -- Count Endosso_Tb ???
    sSQL = sSQL & "     , Proposta_Situacao                             VarChar(1)          " & vbNewLine  ' -- Proposta_Tb
    sSQL = sSQL & "     , Proposta_Situacao_Descricao                   VarChar(60)         " & vbNewLine  ' -- Dado_Basico_Db..Situacao_Proposta_tb.Descricao
    sSQL = sSQL & "     , Tp_Cobertura_Nome                             VarChar(60)         " & vbNewLine  ' -- Tp_Cobertura_Tb
    sSQL = sSQL & "     , Corretor_Nome                                 VarChar(60)         " & vbNewLine  ' -- Corretor_Tb
    sSQL = sSQL & "     , Cliente_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb (Prop_Cliente_ID)
    sSQL = sSQL & "     , Cliente_Nome                                  VarChar(60)         " & vbNewLine  ' -- Cliente_Tb
    sSQL = sSQL & "     , Tp_Componente_ID                              TinyInt             " & vbNewLine  ' -- Escolha_Tp_Cob_Vida_Tp
    sSQL = sSQL & "     , Tp_Componente_Nome                            VarChar(60)         " & vbNewLine  ' -- Tp_Componente_Tb
    sSQL = sSQL & "     , Evento_Sinistro_ID                            Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Evento_Sinistro_Nome                          VarChar(60)         " & vbNewLine  ' -- Evento_Sinistro_Tb
    sSQL = sSQL & "     , SubEvento_Sinistro_ID                         Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , SubEvento_Sinistro_Nome                       VarChar(120)         " & vbNewLine  ' -- SubEvento_Sinistro_Tb
    sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro                        SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Dt_Entrada_Seguradora                         SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Dt_Aviso_Sinistro                             SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Dt_Inclusao                                   SmallDateTIme       " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Agencia_ID                                    Numeric(4)          " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Sinistro_Vida_Nome                            VarChar(60)         " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
    sSQL = sSQL & "     , Sinistro_Vida_CPF                             VarChar(11)         " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
    sSQL = sSQL & "     , Sinistro_Vida_Dt_Nasc                         DateTime            " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
    sSQL = sSQL & "     , Sinistro_Vida_Sexo                            Char(1)             " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
    sSQL = sSQL & "     , Sinistrado_Cliente_ID                         Int                 " & vbNewLine  ' -- Pessoa_Fisica_Tb (Pf_Cliente_ID) (Sinistrado)
    sSQL = sSQL & "     , Solicitante_ID                                Int                 " & vbNewLine  ' -- Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Nome                     VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Endereco                 VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Bairro                   VarChar(30)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio_ID             Numeric(3)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio                VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Municipio_Nome                    VarChar(60)         " & vbNewLine  ' -- Municipio_Tb (Nome) (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD1                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax                  VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax             VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5                   Char(4)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_Estado                   Char(2)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSQL = sSQL & "     , Solicitante_Sinistro_EMail                    VarChar(1000)       " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante) - jose soares - confitec - 04/06/2020 - aumentando caracteres do campo email
    sSQL = sSQL & "     , Solicitante_Sinistro_CEP                      VarChar(8)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)

    sSQL = sSQL & "     , Sinistro_Causa_Observacao_Item                Int                 " & vbNewLine  ' -- Sinistro_Causa_Observacao_Tb
    sSQL = sSQL & "     , Sinistro_Causa_Observacao                     VarChar(300)        " & vbNewLine  ' -- Sinistro_Causa_Observacao_Tb
    sSQL = sSQL & "     , Sinistro_Situacao_ID                          Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSQL = sSQL & "     , Sinistro_Situacao                             VarChar(60)         " & vbNewLine  ' -- Dado_Basico_Db..Sinistro_Situacao_Tb
    sSQL = sSQL & "     , Max_Endosso_ID                                Int                 " & vbNewLine  ' -- Endosso_Tb
    'Para consulta
    sSQL = sSQL & "     , Apolice_Terceiros_Num_Ordem_Co_Seguro_Aceito          Numeric(30,0)           " & vbNewLine 'EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
    sSQL = sSQL & "     , Apolice_Terceiros_Seg_Cod_Susep_Lider                 Numeric(5,0)            " & vbNewLine
    sSQL = sSQL & "     , Apolice_Terceiros_Ramo_ID                             Int                     " & vbNewLine
    sSQL = sSQL & "     , Apolice_Terceiros_Sucursal_Seg_Lider                  Numeric(5,0)            " & vbNewLine
    
    'Campos do frame Cosseguro
    sSQL = sSQL & "     , Cosseguro_Aceito_sinistro_id_lider                    Numeric(30)             " & vbNewLine 'EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
    sSQL = sSQL & "     , Cosseguro_Aceito_Num_Apolice_Lider                    Numeric(30,0)           " & vbNewLine 'EF_18229361-Sinistro Cosseguro Aceito 21/07/2014 - MCAMARAL
    sSQL = sSQL & "     , Cosseguro_Aceito_Ramo_Lider                           Int                     " & vbNewLine
    sSQL = sSQL & "     , Cosseguro_Aceito_Sucursal_Seg_Lider                   Numeric(5,0)            " & vbNewLine
    sSQL = sSQL & "     , Cosseguro_Aceito_Seg_Cod_Susep_Lider                  Numeric(5,0)            " & vbNewLine
    sSQL = sSQL & "     , Apolice_Terceiros_Apolice_ID                          Numeric(9,0)            " & vbNewLine
    sSQL = sSQL & "     , Seguradora_Nome                                       VarChar(60)             " & vbNewLine
    sSQL = sSQL & "     , Cosseguro_Ramo_Nome                                   VarChar(60)             " & vbNewLine
    sSQL = sSQL & "     , Sucursal_Seguradora_Nome                              VarChar(60)             " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Endereco                                     VarChar(60)             " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Bairro                                       VarChar(30)             " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Estado                                       VarChar(20)             " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Municipio                                    VarChar(30)             " & vbNewLine
    sSQL = sSQL & "     , Passivel_Ressarcimento                                Char(1)                 " & vbNewLine       'AKIO.OKUNO - 02/10/2012
    
    '(INI) Demanda 18225206 - O campo grau_parentesco nunca foi preenchido na solicitante_sinistro_tb.
    sSQL = sSQL & "     , Solicitante_Sinistro_Grau_Parentesco_ID               Int                     " & vbNewLine ' -- Solicitante_Sinistro_Tb (Solicitante)
    '(FIM) Demanda 18225206.
    
    'Andr� Martins - Facelift 14/11/2019
    sSQL = sSQL & "     , plano_id                                              varchar(5)              " & vbNewLine
    sSQL = sSQL & "     , plano_nome                                            varchar(30)             " & vbNewLine
    
    'Sergio Demanda Historico Clientes Vida
    sSQL = sSQL & "     , cpf_cnpj                                              VARCHAR(14)              " & vbNewLine ' -- cliente_tb (cpf_cnpj)
    
    sSQL = sSQL & "     ) " & vbNewLine
    sSQL = sSQL & "     " & vbNewLine
    sSQL = sSQL & "Create Index PK_Sinistro_ID                          on #Sinistro_Principal (Sinistro_ID)" & vbNewLine
    sSQL = sSQL & "Create Index FK_Proposta_ID                          on #Sinistro_Principal (Proposta_ID)" & vbNewLine
    sSQL = sSQL & "Create Index FK_Evento_Sinistro_ID                   on #Sinistro_Principal (Evento_Sinistro_ID)" & vbNewLine
    sSQL = sSQL & "Create Index FK_SubEvento_Sinistro_ID                on #Sinistro_Principal (SubEvento_Sinistro_ID)" & vbNewLine
    sSQL = sSQL & "Create Index FK_A                                    on #Sinistro_Principal (Sucursal_Seguradora_ID, Seguradora_Cod_Susep, Apolice_ID, Ramo_ID)" & vbNewLine
    sSQL = sSQL & "Create Index FK_Seguradora_Cod_Susep                 on #Sinistro_Principal (Seguradora_Cod_Susep)" & vbNewLine
    sSQL = sSQL & "Create Index FK_Cliente_ID                           on #Sinistro_Principal (Cliente_ID)" & vbNewLine
    sSQL = sSQL & "Create Index FK_Solicitante_ID                       on #Sinistro_Principal (Solicitante_ID)" & vbNewLine
    sSQL = sSQL & "" & vbNewLine
'AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
'TEMPORARIAS S�O UTILIZADAS DENTRO DO SEGP10922_SPS
'SENDO ASSIM, FORAM TRANSFERIDAS PARA DENTRO DESTA PROC.
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_BB'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Sinistro_BB" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Create Table #Sinistro_BB " & vbNewLine
'    sSQL = sSQL & "     ( Sinistro_BB                        Numeric(13)         " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_ID                        Numeric(11)         " & vbNewLine
'    sSQL = sSQL & "     , Dt_Fim_Vigencia                    smalldatetime )     " & vbNewLine
'
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Entrada_GTR'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Entrada_GTR" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & "Select Entrada_GTR_Tb.* " & vbNewLine
'    sSQL = sSQL & " Into #Entrada_GTR   " & vbNewLine
'    sSQL = sSQL & " From Interface_Db..Entrada_GTR_Tb Entrada_GTR_Tb  WITH (NOLOCK)    " & vbNewLine
'    sSQL = sSQL & " where 1 = 0  " & vbNewLine
'
'    ' Para identificar se visualiza ou n�o bot�o de Analise t�cnica
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Seguro_Fraude_Historico'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Seguro_Fraude_Historico" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & "Create Table #Seguro_Fraude_Historico " & vbNewLine
'    sSQL = sSQL & "     ( Fase_ID                        int         " & vbNewLine
'    sSQL = sSQL & "       , Nivel_ID                     int )        " & vbNewLine
'
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Evento_SegBR_Sinistro_Atual_Temp'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Evento_SegBR_Sinistro_Atual_Temp" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "create table #Evento_SegBR_Sinistro_Atual_Temp ( " & vbNewLine
'    sSQL = sSQL & "   Evento_id         int " & vbNewLine
'    sSQL = sSQL & " , Evento_SegBr_ID   int  " & vbNewLine
'    sSQL = sSQL & " , Evento_BB_Id      int  " & vbNewLine
'    sSQL = sSQL & " , Num_Recibo        tinyint " & vbNewLine  ' Alan Neves - 18/04/2013
'    sSQL = sSQL & " , Sinistro_ID       Numeric(11) " & vbNewLine  ' Alan Neves - 19/04/2013
'    sSQL = sSQL & " , Sinistro_BB       Numeric(13) )" & vbNewLine  ' Alan Neves - 19/04/2013
'
'
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Evento_SegBR_Sinistro_Atual'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Evento_SegBR_Sinistro_Atual" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Select Evento_ID " & vbNewLine
'    sSQL = sSQL & " , Evento_SegBr_ID  " & vbNewLine
'    sSQL = sSQL & " Into #Evento_SegBR_Sinistro_Atual " & vbNewLine
'    sSQL = sSQL & " From #Evento_SegBR_Sinistro_Atual_Temp " & vbNewLine
'    sSQL = sSQL & " Where 1 = 0" & vbNewLine
'
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Evento_ID'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Evento_ID" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Create table #Evento_ID ( " & vbNewLine
'    sSQL = sSQL & " Evento_ID int ) " & vbNewLine
'
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Localizacao'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Localizacao" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Create Table #Localizacao " & vbNewLine
'    sSQL = sSQL & "     ( prox_localizacao_processo          char(1)           " & vbNewLine
'    sSQL = sSQL & "     , Evento_BB_ID                       Int               " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_BB                        Numeric(13) )     " & vbNewLine
'
'
'' Alan Neves - 15/02/2013
'
'    If bytTipoRamo = bytTipoRamo_RE Then
'        sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Principal_Endereco_Risco'),0) >0" & vbNewLine
'        sSQL = sSQL & " BEGIN" & vbNewLine
'        sSQL = sSQL & "     DROP TABLE #Sinistro_Principal_Endereco_Risco" & vbNewLine
'        sSQL = sSQL & " END" & vbNewLine
'    End If
'AKIO.OKUNO - #RETIRADA - FIM - 09/05/2013

    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosGerais_Aviso'),0) >0" & vbNewLine
    sSQL = sSQL & " if OBJECT_ID('tempdb..#DadosGerais_Aviso') IS NOT NULL " & vbNewLine
    sSQL = sSQL & " BEGIN" & vbNewLine
    sSQL = sSQL & "     DROP TABLE #DadosGerais_Aviso" & vbNewLine
    sSQL = sSQL & " END" & vbNewLine
    sSQL = sSQL & "Create Table #DadosGerais_Aviso " & vbNewLine
    sSQL = sSQL & "     ( Sinistro_Bb                           Numeric(13)         " & vbNewLine
    sSQL = sSQL & "     , Dt_Fim_Vigencia                       smalldatetime       " & vbNewLine
    sSQL = sSQL & "     , Agencia_ID                            Numeric(4)          " & vbNewLine
    sSQL = sSQL & "     , Situacao                              varchar(20)  )      " & vbNewLine  ' -- Sinistro_Tb
    
    'Alan Neves 16/04/2013
    'sSQL = sSQL & "  Exec Seguros_Db.dbo.SEGS10922_SPS '" & txtProposta_ID & "','" & lblSinistro_ID(0).Caption & "'" & ", '" & lAviso_Solicitante_ID & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'," & bytTipoRamo & vbNewLine
    sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10922_SPS '" & txtProposta_ID & "','" & lblSinistro_ID(0).Caption & "'" & ", '" & lAviso_Solicitante_ID & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'," & bytTipoRamo & vbNewLine

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)


    Exit Sub

Erro:
    Call TrataErroGeral("SelecionaDados_AbaDadosGerais", Me.Caption)
    Call FinalizarAplicacao

End Sub


Private Sub SelecionaDados_AbaDetalhamentoLocal()

'    SelecionaDados_Detalhamento        'AKIO.OKUNO 24/09/2012
    SelecionaDados_Detalhamento lConexaoLocal

End Sub

'AKIO.OKUNO - #RETIRADA - SelecionaDados_AbaHistorico - 09/05/2013
'PROCEDURE RETIRADA
'Private Sub SelecionaDados_AbaHistorico()
'    Dim sSQL As String
'
'    On Error GoTo Erro
'
'    MousePointer = vbHourglass
'
'    '----------- HISTORICOS
'    sSQL = ""
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Historicos'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Historicos" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & "Create Table #Historicos     "
'    sSQL = sSQL & "     ( Temp_Evento_ID                            SmallInt Identity (1,1)                                                         " & vbNewLine
'    sSQL = sSQL & "     , Dt_Evento                                 SmallDateTime                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Situacao                                  Char(1)                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Descricao_Evento_SEGBR                    VarChar(250)                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Motivo_Encerramento                       Char(1)                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Descricao                                 VarChar(250)                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Usuario                                   VarChar(250)                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Cargo                                     VarChar(250)                                                                    " & vbNewLine
'    '    sSQL = sSQL & "     , Seq_Evento                                TinyInt                                                                        " & vbNewLine   'AKIO.OKUNO - 25/09/2012
'    '    sSQL = sSQL & "     , Linha_ID                                  TinyInt                                                                         " & vbNewLine  'AKIO.OKUNO - 25/09/2012
'    sSQL = sSQL & "     , Seq_Evento                                SmallInt                                                                        " & vbNewLine   'AKIO.OKUNO - 25/09/2012
'    sSQL = sSQL & "     , Linha_ID                                  Int                                                                             " & vbNewLine   'AKIO.OKUNO - 25/09/2012
'    sSQL = sSQL & "     , Seq_Item                                  TinyInt                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Detalhamento_ID                           SmallInt                                                                        " & vbNewLine
'    sSQL = sSQL & "     , Localizacao_Processo                      TinyInt                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Linha                                     VarChar(250)                                                                    " & vbNewLine
'    sSQL = sSQL & "     )                                                                                                                           " & vbNewLine
'    sSQL = sSQL & "                                                                                                                                 " & vbNewLine
'    sSQL = sSQL & "Insert Into #Historicos                                                                                                          " & vbNewLine
'    sSQL = sSQL & "     ( Dt_Evento                                                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Situacao                                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Descricao_Evento_SEGBR                                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Motivo_Encerramento                                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Descricao                                                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Usuario                                                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Cargo                                                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Seq_Evento                                                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Linha_ID                                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Seq_Item                                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Detalhamento_ID                                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Localizacao_Processo                                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Linha                                                                                                                     " & vbNewLine
'    sSQL = sSQL & "     )                                                                                                                           " & vbNewLine
'    sSQL = sSQL & "SELECT DISTINCT                                                                                                                  " & vbNewLine
'    sSQL = sSQL & "       Sinistro_Historico_Tb.Dt_Evento                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Historico_Tb.Situacao                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Evento_SEGBR_Tb.Descricao                                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Motivo_Encerramento                                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Descricao = Null                                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Usuario = Case When Sinistro_Historico_Tb.Usuario = Tecnico.Login_Rede                                                    " & vbNewLine
'    sSQL = sSQL & "                   Then Tecnico.Nome                                                                                             " & vbNewLine
'    sSQL = sSQL & "                   Else Sinistro_Historico_Tb.Usuario                                                                            " & vbNewLine
'    sSQL = sSQL & "              End                                                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Tecnico.Cargo                                                                                                             " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Historico_Tb.Seq_Evento                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Linha_Detalhamento_Tb.Linha_ID                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Seq_Item                                      = Null                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Historico_Tb.Detalhamento_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Historico_Tb.Localizacao_Processo                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Linha = Case When Evento_SEGBR_Tb.Evento_SEGBR_ID = 10010 or Evento_SEGBR_Tb.Evento_SEGBR_ID Between 10041 and 10047      " & vbNewLine
'    sSQL = sSQL & "                 Then Null                                                                                                       " & vbNewLine
'    sSQL = sSQL & "                 Else Sinistro_Linha_Detalhamento_Tb.Linha                                                                       " & vbNewLine
'    sSQL = sSQL & "            End                                                                                                                  " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Historico_Tb          Sinistro_Historico_Tb  WITH (NOLOCK)                                               " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Evento_SEGBR_Tb                Evento_SEGBR_Tb  WITH (NOLOCK)                                                     " & vbNewLine
'    sSQL = sSQL & "    on Evento_SEGBR_Tb.Evento_SEGBR_Id               = Sinistro_Historico_Tb.Evento_SEGBR_Id                                     " & vbNewLine
'    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Sinistro_Linha_Detalhamento_Tb Sinistro_Linha_Detalhamento_Tb  WITH (NOLOCK)                                 " & vbNewLine
'    sSQL = sSQL & "    on (Sinistro_Historico_Tb.Sinistro_ID            = Sinistro_Linha_Detalhamento_Tb.Sinistro_ID                                " & vbNewLine
'    sSQL = sSQL & "   and Sinistro_Historico_Tb.Detalhamento_ID     = Sinistro_Linha_Detalhamento_Tb.Detalhamento_ID )                              " & vbNewLine
'    sSQL = sSQL & "  Left Join ( Select Tecnico_Tb.Tecnico_ID                                                                                       " & vbNewLine
'    sSQL = sSQL & "                , Tecnico_Tb.Nome                                                                                                " & vbNewLine
'    sSQL = sSQL & "                , Login_Rede                     = Tecnico_Tb.Email                                                              " & vbNewLine
'    sSQL = sSQL & "                , Tecnico_Tb.Tp_Tecnico_ID                                                                                       " & vbNewLine
'    sSQL = sSQL & "                , Cargo                          = Tp_Tecnico_Tb.Tp_Tecnico                                                      " & vbNewLine
'    sSQL = sSQL & "             From Seguros_Db.Dbo.Tecnico_Tb Tecnico_Tb  WITH (NOLOCK)                                                                   " & vbNewLine
'    sSQL = sSQL & "             Inner Join Seguros_Db.Dbo.Tp_Tecnico_Tb Tp_Tecnico_Tb  WITH (NOLOCK)                                                       " & vbNewLine
'    sSQL = sSQL & "                on Tp_Tecnico_Tb.Tp_Tecnico_ID   = Tecnico_Tb.Tp_Tecnico_ID) as Tecnico                                          " & vbNewLine
'    sSQL = sSQL & "    on Tecnico.Login_Rede                            = Sinistro_Historico_Tb.Usuario                                             " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                       " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Historico_Tb.Sinistro_ID                                         " & vbNewLine
'    'mathayde
'    'sSQL = sSQL & " Order by Sinistro_Historico_Tb.Dt_Evento Desc                                                                                   " & vbNewLine
'    'sSQL = sSQL & "     , Sinistro_Historico_Tb.Seq_Evento Desc                                                                                     " & vbNewLine
'    'sSQL = sSQL & "     , Sinistro_Linha_Detalhamento_Tb.Linha_ID                                                                                   " & vbNewLine
'    sSQL = sSQL & "                                                                                                                                 " & vbNewLine
'    sSQL = sSQL & "Insert Into #Historicos                                                                                                          " & vbNewLine
'    sSQL = sSQL & "     ( Dt_Evento                                                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Situacao                                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Descricao_Evento_SEGBR                                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Motivo_Encerramento                                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Descricao                                                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Usuario                                                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Cargo                                                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Seq_Evento                                                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Linha_ID                                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Seq_Item                                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Detalhamento_ID                                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Localizacao_Processo                                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Linha                                                                                                                     " & vbNewLine
'    sSQL = sSQL & "     )                                                                                                                           " & vbNewLine
'    sSQL = sSQL & "Select Distinct                                                                                                                  " & vbNewLine
'    sSQL = sSQL & "       Sinistro_Historico_Tb.Dt_Evento                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Historico_Tb.Situacao                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Evento_SEGBR_Tb.Descricao                                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Historico_Tb.Motivo_Encerramento                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Item_Historico_Tb.Descricao                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Usuario = Case When Sinistro_Historico_Tb.Usuario = Tecnico.Login_Rede                                                    " & vbNewLine
'    sSQL = sSQL & "                   Then Tecnico.Nome                                                                                             " & vbNewLine
'    sSQL = sSQL & "                   Else Sinistro_Historico_Tb.Usuario                                                                            " & vbNewLine
'    sSQL = sSQL & "                 End                                                                                                             " & vbNewLine
'    sSQL = sSQL & "     , Tecnico.Cargo                                                                                                             " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Historico_Tb.Seq_Evento                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Linha_ID                                      = Null                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Item_Historico_Tb.Seq_item                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Historico_Tb.Detalhamento_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Historico_Tb.Localizacao_Processo                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Linha = Sinistro_Item_Historico_Tb.Descricao                                                                              " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Historico_Tb          Sinistro_Historico_Tb  WITH (NOLOCK)                                               " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Evento_SEGBR_Tb                Evento_SEGBR_Tb  WITH (NOLOCK)                                                     " & vbNewLine
'    sSQL = sSQL & " on Sinistro_Historico_Tb.Evento_SEGBR_Id        = Evento_SEGBR_Tb.Evento_SEGBR_Id                                               " & vbNewLine
'    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Sinistro_Item_Historico_Tb Sinistro_Item_Historico_Tb  WITH (NOLOCK)                                         " & vbNewLine
'    sSQL = sSQL & "    on (Sinistro_Historico_Tb.Sinistro_ID            = Sinistro_Item_Historico_Tb.Sinistro_ID                                    " & vbNewLine
'    sSQL = sSQL & "   and Sinistro_Historico_Tb.Seq_Evento              = Sinistro_Item_Historico_Tb.Seq_Evento)                                    " & vbNewLine
'    sSQL = sSQL & " Inner Join #Historicos Historicos                                                                                               " & vbNewLine
'    sSQL = sSQL & "    on (Historicos.Seq_Evento                        = Sinistro_Item_Historico_Tb.Seq_Evento                                     " & vbNewLine
'    sSQL = sSQL & "   and Historicos.Linha                              is null)                                                                    " & vbNewLine
'    sSQL = sSQL & "  Left Join (Select Tecnico_Tb.Tecnico_ID                                                                                        " & vbNewLine
'    sSQL = sSQL & "               , Tecnico_Tb.Nome                                                                                                 " & vbNewLine
'    sSQL = sSQL & "               , Login_Rede                      = Tecnico_Tb.Email                                                              " & vbNewLine
'    sSQL = sSQL & "               , Tecnico_Tb.Tp_Tecnico_ID                                                                                        " & vbNewLine
'    sSQL = sSQL & "               , Cargo                           = Tp_Tecnico_Tb.Tp_Tecnico                                                      " & vbNewLine
'    sSQL = sSQL & "            From Seguros_Db.Dbo.Tecnico_Tb       Tecnico_Tb  WITH (NOLOCK)                                                              " & vbNewLine
'    sSQL = sSQL & "           Inner Join Seguros_Db.Dbo.Tp_Tecnico_Tb Tp_Tecnico_Tb  WITH (NOLOCK)                                                         " & vbNewLine
'    sSQL = sSQL & "              on Tp_Tecnico_Tb.Tp_Tecnico_ID     = Tecnico_Tb.Tp_Tecnico_ID) as Tecnico                                          " & vbNewLine
'    sSQL = sSQL & "    on Tecnico.Login_Rede                            = Sinistro_Historico_Tb.Usuario                                             " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                       " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Historico_Tb.Sinistro_ID                                         " & vbNewLine
'    '    'mathayde
'    '    'sSQL = sSQL & " Order by Sinistro_Historico_Tb.Dt_Evento Desc                                                                                   " & vbNewLine
'    '    'sSQL = sSQL & "     , Sinistro_Historico_Tb.Seq_Evento Desc                                                                                     " & vbNewLine
'    '    'sSQL = sSQL & "     , Sinistro_Item_Historico_Tb.Seq_Item                                                                                       " & vbNewLine
'    '    sSQL = sSQL & "                                                                                                                                 " & vbNewLine
'    '    sSQL = sSQL & "Update #Historicos                                                                                                               " & vbNewLine
'    '    sSQL = sSQL & "   Set Cargo = Case When Usuario = 'USER_MQ'                                                                                     " & vbNewLine
'    '    sSQL = sSQL & "                    then 'USER_MQ'                                                                                  " & vbNewLine
'    '    sSQL = sSQL & "                    else case When Cargo is null                                                                                                     " & vbNewLine
'    '    sSQL = sSQL & "                              Then ''                                                                                       " & vbNewLine
'    '    sSQL = sSQL & "                              Else Cargo                                                                                         " & vbNewLine
'    '    sSQL = sSQL & "                         end                                                                                                               " & vbNewLine
'    '    sSQL = sSQL & "               end                                                                                                               " & vbNewLine
'    '''mathayde
'    sSQL = sSQL & "Update #Historicos                                                                                                               " & vbNewLine
'    sSQL = sSQL & "   Set Cargo = CASE WHEN ISNUMERIC(usuario) = 1                                                                                  " & vbNewLine
'    sSQL = sSQL & "                    THEN dbo.Retorna_Cargo_FN(usuario,'')                                                                        " & vbNewLine
'    sSQL = sSQL & "                    ELSE ISNULL(cargo, usuario)                                                                                 " & vbNewLine
'    sSQL = sSQL & "               End"
'
'    'repete valor de USUARIO no campo CARGO quando esta NULL
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Historico_Evolucao'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Historico_Evolucao" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & "Select Dt_Evento                                                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , upper(Usuario) as Usuario                                                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , upper(Cargo) as Cargo                                                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , upper(Descricao_Evento_SEGBR) as Descricao_Evento_SEGBR                                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Descricao = Case When #Historicos.Detalhamento_ID is Null and not #Historicos.Descricao is Null                           " & vbNewLine
'    sSQL = sSQL & "                     Then upper(#Historicos.Descricao)                                                                                  " & vbNewLine
'    sSQL = sSQL & "                     Else upper(#Historicos.Linha)                                                                                      " & vbNewLine
'    sSQL = sSQL & "                End                                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Seq_Evento                                                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Linha_ID                                                                                                                  " & vbNewLine
'    sSQL = sSQL & "  Into #Historico_Evolucao                                                                                                       " & vbNewLine
'    sSQL = sSQL & "  From #Historicos                                                                                                               " & vbNewLine
'
'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
'    Exit Sub
'
'Erro:
'    Call TrataErroGeral("SelecionaDados_AbaDadosGerais", Me.Caption)
'    Call FinalizarAplicacao
'
'End Sub
'
''AKIO.OKUNO - #RETIRADA - #EstimativaPagamento_Estimativa - 09/05/2013
''Private Sub #EstimativaPagamento_Estimativa()
''    Dim sSQL As String
''
''    On Error GoTo Erro
''
''
''    '-->> Dados do Frame: Seguro                                                                                                                                                                                                             & vbNewLine
''    '-- Dados do Sinistro                                                                                                                                                                                                                    & vbNewLine
''    sSQL = ""
''    sSQL = sSQL & "Insert into #Sinistro_Principal " & vbNewLine
''    sSQL = sSQL & "          ( Sucursal_Seguradora_ID           " & vbNewLine
''    sSQL = sSQL & "          , Seguradora_Cod_Susep             " & vbNewLine
''    sSQL = sSQL & "          , Apolice_ID                       " & vbNewLine
''    sSQL = sSQL & "          , Ramo_ID                          " & vbNewLine
''    sSQL = sSQL & "          , Proposta_ID                      " & vbNewLine
''    sSQL = sSQL & "          , Cliente_ID                       " & vbNewLine   'AKIO.OKUNO - 24/09/2012
''    sSQL = sSQL & "          , Passivel_Ressarcimento           " & vbNewLine   'AKIO.OKUNO - 02/10/2012
''    sSQL = sSQL & "          )" & vbNewLine
''
''    If (strTipoProcesso = strTipoProcesso_Avisar Or _
''        strTipoProcesso = strTipoProcesso_Avisar_Cosseguro) And LenB(Trim(lblSinistro_ID(0).Caption)) = 0 Then
''        sSQL = sSQL & "Select Proposta_Adesao_Tb.Sucursal_Seguradora_ID                                             " & vbNewLine
''        sSQL = sSQL & "     , Proposta_Adesao_Tb.Seguradora_Cod_Susep                                               " & vbNewLine
''        sSQL = sSQL & "     , Proposta_Adesao_Tb.Apolice_ID                                                         " & vbNewLine
''        sSQL = sSQL & "     , Proposta_Adesao_Tb.Ramo_ID                                                            " & vbNewLine
''        sSQL = sSQL & "     , Proposta_Adesao_Tb.Proposta_ID                                                        " & vbNewLine
''        sSQL = sSQL & "     , Proposta_Tb.Prop_Cliente_ID                                                                " & vbNewLine
''        sSQL = sSQL & "     , Passivel_Ressarcimento                            = ''                                " & vbNewLine           'AKIO.OKUNO - 02/10/2012
''        sSQL = sSQL & "  From Seguros_db.Dbo.Proposta_Adesao_Tb                 Proposta_Adesao_Tb  WITH (NOLOCK)              " & vbNewLine
''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Tb                        Proposta_Tb  WITH (NOLOCK)                     " & vbNewLine
''        sSQL = sSQL & "    on Proposta_tb.Proposta_ID                           = Proposta_Adesao_Tb.Proposta_ID        " & vbNewLine
''        sSQL = sSQL & " Where Proposta_Adesao_Tb.Proposta_ID                    = " & txtProposta_ID & vbNewLine
''        sSQL = sSQL & "Union                                                                                        " & vbNewLine
''        sSQL = sSQL & "Select Apolice_Tb.Sucursal_Seguradora_ID                                                     " & vbNewLine
''        sSQL = sSQL & "     , Apolice_Tb.Seguradora_Cod_Susep                                                       " & vbNewLine
''        sSQL = sSQL & "     , Apolice_Tb.Apolice_ID                                                                 " & vbNewLine
''        sSQL = sSQL & "     , Apolice_Tb.Ramo_ID                                                                    " & vbNewLine
''        sSQL = sSQL & "     , Proposta_Fechada_Tb.Proposta_ID                                                       " & vbNewLine
''        sSQL = sSQL & "     , Proposta_Tb.Prop_Cliente_ID                                                                " & vbNewLine
''        sSQL = sSQL & "     , Passivel_Ressarcimento                            = ''                                " & vbNewLine           'AKIO.OKUNO - 02/10/2012
''        sSQL = sSQL & "  From Seguros_db.Dbo.Proposta_Fechada_Tb                Proposta_Fechada_Tb WITH (NOLOCK)          " & vbNewLine
''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Apolice_Tb                         Apolice_Tb  WITH (NOLOCK)                      " & vbNewLine
''        sSQL = sSQL & "    on Apolice_Tb.Proposta_ID                            = Proposta_Fechada_Tb.Proposta_ID   " & vbNewLine
''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Tb                        Proposta_Tb  WITH (NOLOCK)                     " & vbNewLine
''        sSQL = sSQL & "    on Proposta_tb.Proposta_ID                           = Proposta_Fechada_Tb.Proposta_ID        " & vbNewLine
''        sSQL = sSQL & " Where Proposta_Fechada_Tb.Proposta_ID = " & txtProposta_ID & vbNewLine
''    Else
''        sSQL = sSQL & "Select Sucursal_Seguradora_ID                                                    " & vbNewLine
''        sSQL = sSQL & "     , Seguradora_Cod_Susep                                                      " & vbNewLine
''        sSQL = sSQL & "     , Apolice_ID                                                                " & vbNewLine
''        sSQL = sSQL & "     , Ramo_ID                                                                   " & vbNewLine
''        sSQL = sSQL & "     , Proposta_ID                                                               " & vbNewLine
''        sSQL = sSQL & "     , Cliente_ID                                                                " & vbNewLine
''        sSQL = sSQL & "     , Passivel_Ressarcimento                                                    " & vbNewLine   'AKIO.OKUNO - 02/10/2012
''        sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Tb                    Sinistro_Tb WITH (NOLOCK)  " & vbNewLine
''        sSQL = sSQL & " Where Sinistro_ID = " & lblSinistro_ID(0).Caption & vbNewLine
''    End If
''    If LenB(Trim(lblSinistro_ID(0).Caption)) <> 0 Then
''        sSQL = sSQL & "" & vbNewLine
''        sSQL = sSQL & "Update #Sinistro_Principal                                                       " & vbNewLine
''        sSQL = sSQL & "   Set Sinistro_ID             = Sinistro_Tb.Sinistro_ID                         " & vbNewLine
''        sSQL = sSQL & "     , Evento_Sinistro_ID      = Sinistro_Tb.Evento_Sinistro_ID                  " & vbNewLine
''        sSQL = sSQL & "     , SubEvento_Sinistro_ID   = Sinistro_Tb.SubEvento_Sinistro_ID               " & vbNewLine
''        sSQL = sSQL & "     , Dt_Ocorrencia_Sinistro  = Sinistro_Tb.Dt_Ocorrencia_Sinistro              " & vbNewLine
''        sSQL = sSQL & "     , Dt_Entrada_Seguradora   = Sinistro_Tb.Dt_Entrada_Seguradora               " & vbNewLine
''        sSQL = sSQL & "     , Dt_Aviso_Sinistro       = Sinistro_Tb.Dt_Aviso_Sinistro                   " & vbNewLine
''        sSQL = sSQL & "     , Dt_Inclusao             = Sinistro_Tb.Dt_Inclusao                         " & vbNewLine
''        sSQL = sSQL & "     , Agencia_ID              = Sinistro_Tb.Agencia_ID                          " & vbNewLine
''        sSQL = sSQL & "     , Solicitante_ID          = Sinistro_Tb.Solicitante_ID                      " & vbNewLine
''        sSQL = sSQL & "     , Sinistro_Endereco       = Sinistro_Tb.Endereco                            " & vbNewLine
''        sSQL = sSQL & "     , Sinistro_Bairro         = Sinistro_Tb.Bairro                              " & vbNewLine
''        sSQL = sSQL & "     , Sinistro_Municipio      = Sinistro_Tb.Municipio                           " & vbNewLine
''        sSQL = sSQL & "     , Sinistro_Estado         = Sinistro_Tb.Estado                              " & vbNewLine
''        sSQL = sSQL & "     , Sinistro_Situacao_ID    = Sinistro_Tb.Situacao                            " & vbNewLine
''        'mathayde
''        sSQL = sSQL & "     , Cosseguro_Aceito_sinistro_id_lider = isnull(Sinistro_Tb.sinistro_id_lider,0)" & vbNewLine
''        sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Tb                    Sinistro_Tb  WITH (NOLOCK)         " & vbNewLine
''        sSQL = sSQL & " Where Sinistro_Tb.Sinistro_ID = " & lblSinistro_ID(0).Caption & vbNewLine
''    End If
''
''    '--Situacao Sinistro                                                                                                                                                                                                                     & vbNewLine
''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set Sinistro_Situacao                             = Sinistro_Situacao_Tb.Situacao" & vbNewLine
''    sSQL = sSQL & "  From Dado_Basico_Db..Sinistro_Situacao_Tb          Sinistro_Situacao_Tb  WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_Situacao_ID      = Sinistro_Situacao_Tb.Situacao_ID" & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
''    '-- Ramo_Tb                                                                                                                                                                                                                              & vbNewLine
''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set Ramo_Nome                                     = Ramo_Tb.Nome" & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Ramo_Tb                        Ramo_Tb  WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & " on #Sinistro_Principal.Ramo_ID                      = Ramo_Tb.Ramo_ID" & vbNewLine
''    '-- Lider                                                                                                                                                                                                                                & vbNewLine
''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set Seguradora_Nome_Lider                         = Seguradora_Tb.Nome" & vbNewLine
''    'sSQL = sSQL & "  From Seguros_Db.Dbo.Seguradora_Tb                  Seguradora_Tb (NoLock, Index = PK_seguradora)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Seguradora_Tb                  Seguradora_Tb  WITH (NOLOCK) " & vbNewLine
''    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Seguradora_Cod_Susep)" & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Seguradora_Cod_Susep      = Seguradora_Tb.Seguradora_Cod_Susep " & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
''    '-- Solicitante                                                                                                                                                                                                                          & vbNewLine
''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set Solicitante_Sinistro_Nome                     = Solicitante_Sinistro_Tb.Nome" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Endereco                 = Solicitante_Sinistro_Tb.Endereco" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Bairro                   = Solicitante_Sinistro_Tb.Bairro" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio_ID             = Solicitante_Sinistro_Tb.Municipio_ID" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio                = Solicitante_Sinistro_Tb.Municipio " & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD1                     = Solicitante_Sinistro_Tb.DDD" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1                = Solicitante_Sinistro_Tb.Telefone" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                     = Solicitante_Sinistro_Tb.DDD1" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2                = Solicitante_Sinistro_Tb.Telefone1" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                     = Solicitante_Sinistro_Tb.DDD2" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3                = Solicitante_Sinistro_Tb.Telefone2" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                     = Solicitante_Sinistro_Tb.DDD3" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4                = Solicitante_Sinistro_Tb.Telefone3" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                     = Solicitante_Sinistro_Tb.DDD4" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5                = Solicitante_Sinistro_Tb.Telefone4" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1             = Solicitante_Sinistro_Tb.Tp_Telefone" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2             = Solicitante_Sinistro_Tb.Tp_Telefone1" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3             = Solicitante_Sinistro_Tb.Tp_Telefone2" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4             = Solicitante_Sinistro_Tb.Tp_Telefone3" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5             = Solicitante_Sinistro_Tb.Tp_Telefone4" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1                   = Solicitante_Sinistro_Tb.Ramal" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2                   = Solicitante_Sinistro_Tb.Ramal1" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3                   = Solicitante_Sinistro_Tb.Ramal2" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4                   = Solicitante_Sinistro_Tb.Ramal3" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5                   = Solicitante_Sinistro_Tb.Ramal4" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Estado                   = Case When Solicitante_Sinistro_Tb.Estado = 'XX' or Solicitante_Sinistro_Tb.Estado = '--' " & vbNewLine
''    sSQL = sSQL & "                                                            Then ''" & vbNewLine
''    sSQL = sSQL & "                                                            Else Solicitante_Sinistro_Tb.Estado" & vbNewLine
''    sSQL = sSQL & "                                                       End" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax                  = Solicitante_Sinistro_Tb.DDD_Fax" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax             = Solicitante_Sinistro_Tb.Telefone_Fax" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_CEP                      = Solicitante_Sinistro_Tb.CEP" & vbNewLine
''    sSQL = sSQL & "     , Solicitante_Sinistro_Email                    = Solicitante_Sinistro_Tb.Email" & vbNewLine
''    'sSQL = sSQL & "  From Seguros_Db.Dbo.Solicitante_Sinistro_Tb        Solicitante_Sinistro_Tb (NoLock, Index = PK_solicitante_sinistro)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Solicitante_Sinistro_Tb        Solicitante_Sinistro_Tb  WITH (NOLOCK) " & vbNewLine
''    If (strTipoProcesso = strTipoProcesso_Avisar Or _
''        strTipoProcesso = strTipoProcesso_Avisar_Cosseguro) And _
''        LenB(Trim(lblSinistro_ID(0))) <> 0 Then
''        sSQL = sSQL & " Where Solicitante_Sinistro_Tb.Solicitante_ID = " & lAviso_Solicitante_ID & vbNewLine
''    Else
''        sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Solicitante_ID            = Solicitante_Sinistro_Tb.Solicitante_ID" & vbNewLine
''    End If
''
''
''    sSQL = sSQL & "" & vbNewLine
''    '-- Dados do municipio
''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   set Solicitante_Municipio_Nome                    = Municipio_Tb.Nome" & vbNewLine
''    sSQL = sSQL & "  From #Sinistro_Principal                           " & vbNewLine
''    'sSQL = sSQL & "  Join Seguros_Db.Dbo.Municipio_Tb                   Municipio_Tb (NoLock, Index = PK_municipio)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
''    sSQL = sSQL & "  Join Seguros_Db.Dbo.Municipio_Tb                   Municipio_Tb  WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "    on Municipio_Tb.Municipio_ID                     = #Sinistro_Principal.Solicitante_Sinistro_Municipio_ID" & vbNewLine
''    '-- Dados da Proposta                                                                                                                                                                                                                    & vbNewLine
''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set Produto_ID                                    = Proposta_Tb.Produto_ID" & vbNewLine
''    sSQL = sSQL & "     , Proposta_Situacao                             = Proposta_Tb.Situacao" & vbNewLine
''    '    sSQL = sSQL & "     , Cliente_ID                                    = Proposta_Tb.Prop_Cliente_ID" & vbNewLine
''    sSQL = sSQL & "     , Quantidade_Endossos                           = Proposta_Tb.Num_Endosso" & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
''    sSQL = sSQL & "    on Proposta_Tb.Proposta_ID                       = #Sinistro_Principal.Proposta_ID" & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
''
''    'AKIO.OKUNO - INICIO = 23/09/2012
''    sSQL = sSQL & "Update #Sinistro_Principal                                                                                                                       " & vbNewLine
''    'sSQL = sSQL & "   Set Cliente_ID                                            = isNull(Proposta_Complementar_Tb.Prop_Cliente_ID, Proposta_Tb.Prop_Cliente_ID)     " & vbNewLine 'F.BEZERRA - 17/10/2012
''    sSQL = sSQL & "   Set Cliente_ID                                            = isNull(Proposta_Complementar_Tb.Prop_Cliente_ID, isnull(Proposta_Tb.Prop_Cliente_ID,#Sinistro_Principal.Cliente_ID))" & vbNewLine
''    sSQL = sSQL & "  From #Sinistro_Principal                                                                                                                       " & vbNewLine
''    sSQL = sSQL & "  Left Join Seguros_db.Dbo.Proposta_Tb                       Proposta_Tb  WITH (NOLOCK)                                                                 " & vbNewLine
''    sSQL = sSQL & "    on Proposta_Tb.Proposta_ID                               = #Sinistro_Principal.Proposta_ID                                                   " & vbNewLine
''    sSQL = sSQL & "   and Proposta_Tb.Prop_Cliente_ID                           = #Sinistro_Principal.Cliente_ID                                                    " & vbNewLine
''    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Complementar_Tb          Proposta_Complementar_Tb  WITH (NOLOCK)                                                    " & vbNewLine
''    sSQL = sSQL & "    on Proposta_Complementar_Tb.Proposta_ID                  = #Sinistro_Principal.Proposta_ID                                                   " & vbNewLine
''    sSQL = sSQL & "   and Proposta_Complementar_Tb.Prop_Cliente_ID              = #Sinistro_Principal.Cliente_ID                                                    " & vbNewLine
''    'AKIO.OKUNO - FIM - 23/09/2012
''
''    '-- Dados do Produto
''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   set Produto_Nome                                  = Produto_Tb.Nome   " & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Produto_Tb                     Produto_Tb  WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Produto_ID                = Produto_Tb.Produto_ID " & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
''    '-- Situacao Proposta                                                                                                                                                                                                                    & vbNewLine
''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set Proposta_Situacao_Descricao                   = Situacao_Proposta_Tb.Descricao_Situacao" & vbNewLine
''    sSQL = sSQL & "  From Dado_Basico_Db..Situacao_Proposta_Tb          Situacao_Proposta_Tb  WITH (NOLOCK)  " & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_Situacao         = upper(Situacao_Proposta_Tb.Situacao)" & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
''
''    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
''                             glAmbiente_id, _
''                             App.Title, _
''                             App.FileDescription, _
''                             sSQL, _
''                             lConexaoLocal, _
''                             False)
''
''    '-- Nossa Parte                                                                                                               " & vbNewLine
''    sSQL = sSQL & "Select Nossa_Parte                                   = 100 - IsNull(Sum(Perc_Participacao),0)                                " & vbNewLine
''    sSQL = sSQL & "  Into #Nossa_Parte                                                                                                          " & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Co_Seguro_Repassado_Tb         Co_Seguro_Repassado_Tb  WITH (NOLOCK)                                          " & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                   " & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Sucursal_Seguradora_ID    = Co_Seguro_Repassado_Tb.Sucursal_Seguradora_ID                         " & vbNewLine
''    sSQL = sSQL & "   and #Sinistro_Principal.Seguradora_Cod_Susep      = Co_Seguro_Repassado_Tb.Seguradora_Cod_Susep                           " & vbNewLine
''    sSQL = sSQL & "   and #Sinistro_Principal.Apolice_ID                = Co_Seguro_Repassado_Tb.Apolice_ID                                     " & vbNewLine
''    sSQL = sSQL & "   and #Sinistro_Principal.Ramo_ID                   = Co_Seguro_Repassado_Tb.Ramo_ID                                        " & vbNewLine
''    sSQL = sSQL & " Where Dt_Inicio_Participacao                        <= #Sinistro_Principal.Dt_Ocorrencia_Sinistro                           " & vbNewLine
''    sSQL = sSQL & "   and (Dt_Fim_Participacao                          >= #Sinistro_Principal.Dt_Ocorrencia_Sinistro                           " & vbNewLine
''    sSQL = sSQL & "        or Dt_Fim_Participacao                       is null)                                                                " & vbNewLine
''    sSQL = sSQL & "Update #EstimativaPagamento_Estimativa                                                                                       " & vbNewLine
''    sSQL = sSQL & "   Set Nossa_Parte                                   = #Nossa_Parte.Nossa_Parte                                              " & vbNewLine
''    sSQL = sSQL & "  From #Nossa_Parte                                                                                                          " & vbNewLine
''    '--Quantidade de Estimativas                                                                                                  " & vbNewLine
''    '    sSQL = sSQL & "Update #EstimativaPagamento_Estimativa                                                                                       " & vbNewLine
''    '    sSQL = sSQL & "   Set CountEstimativa                               = (Select count(*) from #EstimativaPagamento_Estimativa)                " & vbNewLine
''    sSQL = sSQL & "UPdate #EstimativaPagamento_Estimativa                                                                                       " & vbNewLine
''    sSQL = sSQL & "   Set CountEstimativa                               = (Select max(Seq_Estimativa) From #EstimativaPagamento_Estimativa)     " & vbNewLine
''
''    Conexao_ExecutarSQL gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, lConexaoLocal, False
''
''    '-->> Dados do Frame: Valores                                                                                                 " & vbNewLine
''    '-- Cobertura Afetada - Valores (Grid Superior)                                                                               " & vbNewLine
''    '    SelecionaDados_AbaEstimativa_CoberturaAfetada_Local
''    SelecionaDados_AbaEstimativa_CoberturaAfetada_Local lConexaoLocal
''
''    '-- Dados da Apolice                                                                                                                                                                                                                     & vbNewLine
''    sSQL = ""
''    sSQL = sSQL & "Declare @Max_Endosso_ID                              int" & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
''    sSQL = sSQL & "Select @Max_Endosso_ID                               = Max(Endosso_Tb.Endosso_ID)" & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Endosso_Tb Endosso_Tb           WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Endosso_Tb.Proposta_ID" & vbNewLine
''    sSQL = sSQL & " Where Endosso_Tb.Tp_Endosso_ID                      = 250" & vbNewLine
''    sSQL = sSQL & " " & vbNewLine
''
''    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set Max_Endosso_ID = @Max_Endosso_ID" & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
''
''    Select Case bytTipoRamo
''    Case bytTipoRamo_Vida
''        sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
''        sSQL = sSQL & "   Set Apolice_Dt_Inicio_Vigencia                    = Proposta_Adesao_Tb.Dt_Inicio_Vigencia" & vbNewLine
''        sSQL = sSQL & "  From #Sinistro_Principal" & vbNewLine
''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Adesao_Tb             Proposta_Adesao_Tb  WITH (NOLOCK) "
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Proposta_Adesao_Tb.Proposta_ID" & vbNewLine
''        sSQL = sSQL & " Where #Sinistro_Principal.Produto_ID                = 721" & vbNewLine
''        sSQL = sSQL & "" & vbNewLine
''
''    Case bytTipoRamo_RE, bytTipoRamo_Rural
''        sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
''        sSQL = sSQL & "   Set Apolice_Dt_Inicio_Vigencia                    = Apolice_Tb.Dt_Inicio_Vigencia" & vbNewLine
''        sSQL = sSQL & "  From #Sinistro_Principal" & vbNewLine
''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Apolice_Tb                     Apolice_Tb  WITH (NOLOCK) "
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Apolice_Tb.Proposta_ID" & vbNewLine
''        sSQL = sSQL & " Where #Sinistro_Principal.Produto_ID                <> 721" & vbNewLine
''        sSQL = sSQL & "" & vbNewLine
''
''    End Select
''
''    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set Apolice_Dt_Fim_Vigencia                       = Endosso_Tb.Dt_Fim_Vigencia_End" & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Endosso_Tb                     Endosso_Tb  WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Endosso_Tb.Proposta_ID" & vbNewLine
''    sSQL = sSQL & " Where Endosso_Tb.Endosso_ID                         = @Max_Endosso_ID" & vbNewLine
''    sSQL = sSQL & "   and Endosso_Tb.Tp_Endosso_ID                      = 250" & vbNewLine
''    sSQL = sSQL & "   and #Sinistro_Principal.Produto_ID                = 721" & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
''
''    sSQL = sSQL & "Select Proposta_Adesao_Tb.Proposta_ID" & vbNewLine
''    sSQL = sSQL & "     , Proposta_Adesao_Tb.Dt_Inicio_Vigencia" & vbNewLine
''    sSQL = sSQL & "     , Proposta_Adesao_Tb.Dt_Fim_Vigencia" & vbNewLine
''    sSQL = sSQL & "  Into #Proposta_Adesao" & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Proposta_Adesao_Tb             Proposta_Adesao_Tb  WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = Proposta_Adesao_Tb.Proposta_ID" & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
''
''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set Apolice_Dt_Inicio_Vigencia                    = Dt_Inicio_Vigencia" & vbNewLine
''    sSQL = sSQL & "  , Apolice_Dt_Fim_Vigencia                          = Dt_Fim_Vigencia" & vbNewLine
''    sSQL = sSQL & "  From #Proposta_Adesao" & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = #Proposta_Adesao.Proposta_ID " & vbNewLine
''    sSQL = sSQL & "   and (#Sinistro_Principal.Apolice_Dt_Inicio_Vigencia is null" & vbNewLine
''    sSQL = sSQL & "   and #Sinistro_Principal.Apolice_Dt_Fim_Vigencia  is null" & vbNewLine
''    sSQL = sSQL & "   and #Sinistro_Principal.Produto_ID               = 721 )" & vbNewLine
''    sSQL = sSQL & "    or (#Sinistro_Principal.Produto_ID               <> 721)" & vbNewLine
''    sSQL = sSQL & " where Apolice_Dt_Inicio_Vigencia is null" & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
''
'''AKIO.OKUNO - INICIO - 11/12/2012
''Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
''                         glAmbiente_id, _
''                         App.Title, _
''                         App.FileDescription, _
''                         sSQL, _
''                         lConexaoLocal, _
''                         False)
''sSQL = ""
'''AKIO.OKUNO - FIM - 11/12/2012
''    sSQL = sSQL & "UPdate #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set Apolice_Dt_Inicio_Vigencia                    = Apolice_Tb.Dt_Inicio_Vigencia" & vbNewLine
''    sSQL = sSQL & "     , Apolice_Dt_Fim_Vigencia                       = Apolice_Tb.Dt_Fim_Vigencia" & vbNewLine
''    sSQL = sSQL & "     , Apolice_Dt_Emissao                                = Apolice_Tb.Dt_Emissao" & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Apolice_Tb Apolice_Tb           WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Apolice_ID                = Apolice_Tb.Apolice_ID" & vbNewLine
''    sSQL = sSQL & "   and #Sinistro_Principal.Sucursal_Seguradora_ID    = Apolice_Tb.Sucursal_Seguradora_ID" & vbNewLine
''    sSQL = sSQL & "   and #Sinistro_Principal.Seguradora_Cod_Susep      = Apolice_Tb.Seguradora_Cod_Susep" & vbNewLine
''    sSQL = sSQL & "   and #Sinistro_Principal.Ramo_ID                   = Apolice_Tb.Ramo_ID" & vbNewLine
''    sSQL = sSQL & "  left Join #Proposta_Adesao" & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID               = #Proposta_Adesao.Proposta_ID " & vbNewLine
''    sSQL = sSQL & " Where #Sinistro_Principal.Produto_ID                <> 721" & vbNewLine
''    sSQL = sSQL & "   and #Proposta_Adesao.Proposta_ID                  is null" & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
'''AKIO.OKUNO - INICIO - 11/12/2012
''Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
''                         glAmbiente_id, _
''                         App.Title, _
''                         App.FileDescription, _
''                         sSQL, _
''                         lConexaoLocal, _
''                         False)
''sSQL = ""
'''AKIO.OKUNO - FIM - 11/12/2012
''
''
''
''    'DADOS DO SINISTRADO" & vbNewLine
''    sSQL = sSQL & "Update #Sinistro_Principal " & vbNewLine
''    sSQL = sSQL & "   Set Sinistro_Vida_Nome                            = IsNull(Sinistro_Vida_Tb.Nome, '')" & vbNewLine
''    sSQL = sSQL & "     , Sinistro_Vida_CPF                             = IsNull(Sinistro_Vida_Tb.Cpf, '')" & vbNewLine
''    sSQL = sSQL & "     , Sinistro_Vida_Dt_Nasc                         = IsNull(Sinistro_Vida_Tb.Dt_Nascimento, '')" & vbNewLine
''    sSQL = sSQL & "     , Sinistro_Vida_Sexo                            = IsNull(Sinistro_Vida_Tb.Sexo, '') " & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Vida_Tb               Sinistro_Vida_Tb  WITH (NOLOCK) " & vbNewLine
''    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = PK_Sinistro_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
''    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Vida_Tb.Sinistro_ID" & vbNewLine
''    sSQL = sSQL & "     " & vbNewLine
''
''
''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set Sinistrado_Cliente_ID                         = Pessoa_Fisica_Tb.Pf_Cliente_ID" & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Pessoa_Fisica_Tb               Pessoa_Fisica_Tb  WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal " & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_Vida_CPF         = Pessoa_Fisica_Tb.CPF" & vbNewLine
''    sSQL = sSQL & "   and #Sinistro_Principal.Sinistro_Vida_Dt_Nasc     = Pessoa_Fisica_Tb.Dt_Nascimento" & vbNewLine
''    sSQL = sSQL & "   and #Sinistro_Principal.Sinistro_Vida_Sexo        = Pessoa_Fisica_Tb.Sexo" & vbNewLine
''
''
''    '-- Dados do Certificado                                                                                                                                                                                                                 & vbNewLine
''    'AKIO.OKUNO - INICIO - 24/09/2012
''    If bytTipoRamo = bytTipoRamo_Vida Then
''        sSQL = sSQL & "Update #Sinistro_Principal                                                                           " & vbNewLine
''        sSQL = sSQL & "   Set Certificado_Dt_Inicio_Vigencia                    = Certificado_Tb.Dt_Inicio_Vigencia         " & vbNewLine
''        '        sSql = sSql & "     , Certificado_Dt_Fim_Vigencia                       = Certificado_Tb.Dt_Fim_Vigencia             " & vbNewLine 'AKIO.OKUNO - 01/10/2012 (CONFORME SOLICITADO PELO USUARIO/MARCEL)
''        sSQL = sSQL & "     , Certificado_Dt_Emissao                            = Certificado_Tb.Dt_Emissao                  " & vbNewLine
''        sSQL = sSQL & "     , Certificado_ID                                    = Certificado_Tb.Certificado_ID             " & vbNewLine
''        sSQL = sSQL & "     , Certificado_Sub_Grupo_ID                          = Seguro_Vida_Sub_Grupo_Tb.Sub_Grupo_ID     " & vbNewLine
''        sSQL = sSQL & "  From Seguros_Db.Dbo.Seguro_Vida_Sub_Grupo_Tb           Seguro_Vida_Sub_Grupo_Tb   WITH (NOLOCK)           " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                           " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID                   = Seguro_Vida_Sub_Grupo_Tb.Proposta_ID      " & vbNewLine
''        sSQL = sSQL & "   and #Sinistro_Principal.Sinistrado_Cliente_ID         = Seguro_Vida_Sub_Grupo_Tb.Prop_Cliente_ID      " & vbNewLine
''        sSQL = sSQL & "  Join Seguros_Db.Dbo.Certificado_Tb                     Certificado_Tb  WITH (NOLOCK)                          " & vbNewLine
''        sSQL = sSQL & "    on Certificado_Tb.Proposta_ID                        = Seguro_Vida_Sub_Grupo_Tb.Proposta_ID      " & vbNewLine
''        sSQL = sSQL & "   and Certificado_Tb.Certificado_ID                     = Seguro_Vida_Sub_Grupo_Tb.Certificado_ID       " & vbNewLine
''
''        'AKIO.OKUNO - INICIO - 01/10/2012
''        sSQL = sSQL & "update #Sinistro_Principal                                                               " & vbNewLine
''        sSQL = sSQL & "   set Certificado_Dt_Fim_Vigencia = ISNULL(sgp.dt_fim_vigencia_sbg, '')                 " & vbNewLine
''        sSQL = sSQL & "from                                                                                     " & vbNewLine
''        sSQL = sSQL & "seguro_vida_sub_grupo_tb sgp WITH (NOLOCK)                                                      " & vbNewLine
''        sSQL = sSQL & "   INNER JOIN cliente_tb cl   WITH (NOLOCK)                                                     " & vbNewLine
''        sSQL = sSQL & "       ON  sgp.prop_cliente_id = cl.cliente_id                                           " & vbNewLine
''        sSQL = sSQL & "   INNER JOIN pessoa_fisica_tb pf   WITH (NOLOCK)                                               " & vbNewLine
''        sSQL = sSQL & "       ON  sgp.prop_cliente_id = pf.pf_cliente_id                                        " & vbNewLine
''        sSQL = sSQL & "   Join #Sinistro_Principal                                                              " & vbNewLine
''        sSQL = sSQL & "  on #Sinistro_Principal.Certificado_Sub_Grupo_ID= sgp.sub_grupo_id                      " & vbNewLine
''        sSQL = sSQL & "    and #Sinistro_Principal.apolice_id               = sgp.apolice_id                    " & vbNewLine
''        sSQL = sSQL & "    and #Sinistro_Principal.sucursal_seguradora_id   = sgp.sucursal_seguradora_id        " & vbNewLine
''        sSQL = sSQL & "    and #Sinistro_Principal.seguradora_cod_susep = sgp.seguradora_cod_susep              " & vbNewLine
''        sSQL = sSQL & "    and #Sinistro_Principal.ramo_id                  = sgp.ramo_id                       " & vbNewLine
''        sSQL = sSQL & "    and #Sinistro_Principal.Sinistrado_Cliente_ID   = sgp.prop_Cliente_ID                " & vbNewLine
''        sSQL = sSQL & " WHERE                                                                                   " & vbNewLine
''        sSQL = sSQL & " sgp.dt_inicio_vigencia_sbg =        (Select TOP 1 sgp2.dt_inicio_vigencia_sbg           " & vbNewLine
''        sSQL = sSQL & "       from seguro_vida_sub_grupo_tb sgp2   WITH (NOLOCK)                                       " & vbNewLine
''        sSQL = sSQL & "       WHERE   sgp2.apolice_id = sgp.apolice_id                                          " & vbNewLine
''        sSQL = sSQL & "                                                                                         " & vbNewLine
''        sSQL = sSQL & "           AND sgp2.sucursal_seguradora_id = sgp.sucursal_seguradora_id                  " & vbNewLine
''        sSQL = sSQL & "           AND sgp2.seguradora_cod_susep = sgp.seguradora_cod_susep                      " & vbNewLine
''        sSQL = sSQL & "           AND sgp2.ramo_id = sgp.ramo_id                                                " & vbNewLine
''        sSQL = sSQL & "           AND sgp2.sub_grupo_id = sgp.sub_grupo_id                                      " & vbNewLine
''        sSQL = sSQL & "           AND sgp2.dt_inicio_vigencia_apol_sbg = sgp.dt_inicio_vigencia_apol_sbg        " & vbNewLine
''        sSQL = sSQL & "           AND sgp2.proposta_id = sgp.proposta_id                                        " & vbNewLine
''        sSQL = sSQL & "           AND sgp2.prop_cliente_id = sgp.prop_cliente_id                                " & vbNewLine
''        sSQL = sSQL & "           AND sgp2.tp_componente_id = sgp.tp_componente_id                              " & vbNewLine
''        sSQL = sSQL & "           AND sgp2.seq_canc_endosso_seg = sgp.seq_canc_endosso_seg                      " & vbNewLine
''        sSQL = sSQL & "ORDER BY sgp2.dt_inicio_vigencia_sbg DESC)                                               " & vbNewLine
''        'AKIO.OKUNO - FIM - 01/10/2012
''
''        sSQL = sSQL & "Update #Sinistro_Principal                                                                                   " & vbNewLine
''        sSQL = sSQL & "   Set Corretor_Nome = Corretor_Tb.Nome                                                                      " & vbNewLine
''        sSQL = sSQL & "  From Seguros_Db.Dbo.Corretor_Tb                        Corretor_Tb  WITH (NOLOCK)                                 " & vbNewLine
''        sSQL = sSQL & "  join Seguros_Db.Dbo.Corretagem_Sub_Grupo_Tb            Corretagem_Sub_Grupo_Tb  WITH (NOLOCK)                     " & vbNewLine
''        sSQL = sSQL & "    on Corretagem_Sub_Grupo_Tb.Corretor_ID               = Corretor_Tb.Corretor_ID                           " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                                   " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Apolice_ID                    = Corretagem_Sub_Grupo_Tb.Apolice_ID                " & vbNewLine
''        sSQL = sSQL & "   and #Sinistro_Principal.Sucursal_Seguradora_ID        = Corretagem_Sub_Grupo_Tb.Sucursal_Seguradora_ID    " & vbNewLine
''        sSQL = sSQL & "   and #Sinistro_Principal.Seguradora_Cod_Susep          = Corretagem_Sub_Grupo_Tb.Seguradora_Cod_Susep      " & vbNewLine
''        sSQL = sSQL & "   and #Sinistro_Principal.Ramo_ID                       = Corretagem_Sub_Grupo_Tb.Ramo_ID                   " & vbNewLine
''        sSQL = sSQL & "   and #Sinistro_Principal.Certificado_Sub_Grupo_ID      = Corretagem_Sub_Grupo_Tb.Sub_Grupo_ID              " & vbNewLine
''        sSQL = sSQL & " Where COrretor_Tb.Situacao = 'a'                                                                            " & vbNewLine
''
''    Else
''        'AKIO.OKUNO - FIM - 24/09/2012
''        sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''        sSQL = sSQL & "   Set Certificado_Dt_Inicio_Vigencia                = Certificado_Tb.Dt_Inicio_Vigencia" & vbNewLine
''        sSQL = sSQL & "     , Certificado_Dt_Fim_Vigencia                   = Certificado_Tb.Dt_Fim_Vigencia" & vbNewLine
''        sSQL = sSQL & "     , Certificado_Dt_Emissao                        = Certificado_Tb.Dt_Emissao" & vbNewLine
''        sSQL = sSQL & "     , Certificado_ID                                = Certificado_Tb.Certificado_ID             " & vbNewLine
''        sSQL = sSQL & "  From Seguros_Db.Dbo.Certificado_Tb                 Certificado_Tb  WITH (NOLOCK) " & vbNewLine
''        'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Proposta_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
''        sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
''        sSQL = sSQL & "    on Certificado_Tb.Proposta_ID                    = #Sinistro_Principal.Proposta_ID   " & vbNewLine
''    End If      'AKIO.OKUNO - 24/09/2012
''
''    sSQL = sSQL & "Select Top 1 Corretor_Tb.Nome" & vbNewLine
''    sSQL = sSQL & "  , Corretor_Tb.Situacao" & vbNewLine
''    sSQL = sSQL & "  Into #Corretor" & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Corretor_Tb                    Corretor_Tb  WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "  Join Seguros_Db.Dbo.Corretagem_Tb                  Corretagem_Tb  WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "    on Corretagem_Tb.Corretor_ID                     = Corretor_Tb.Corretor_ID" & vbNewLine
''    sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK) " & vbNewLine
''    sSQL = sSQL & "    on Proposta_Tb.Proposta_ID                       = Corretagem_Tb.Proposta_ID" & vbNewLine
''    sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Produto_ID                = Proposta_Tb.Produto_ID" & vbNewLine
''    sSQL = sSQL & " Where Corretor_Tb.Situacao = 'a'    " & vbNewLine
''    sSQL = sSQL & "   and Certificado_Sub_Grupo_ID is null" & vbNewLine             'AKIO.OKUNO - 25/09/2012
''
''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set Corretor_Nome                                 = #Corretor.Nome" & vbNewLine
''    sSQL = sSQL & "  From #Corretor" & vbNewLine
''    sSQL = sSQL & " Where Certificado_Sub_Grupo_ID is null" & vbNewLine             'AKIO.OKUNO - 25/09/2012
''
''    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
''                             glAmbiente_id, _
''                             App.Title, _
''                             App.FileDescription, _
''                             sSQL, _
''                             lConexaoLocal, _
''                             False)
''sSQL = ""
''
''    '-- Cliente_Tb                                                                                                                                                                                                                           & vbNewLine
''    sSQL = sSQL & "Update #Sinistro_Principal" & vbNewLine
''    sSQL = sSQL & "   Set #Sinistro_Principal.Cliente_Nome              = Cliente_Tb.Nome" & vbNewLine
''    sSQL = sSQL & "  From Seguros_Db.Dbo.Cliente_Tb                     Cliente_Tb  WITH (NOLOCK) " & vbNewLine
''    'sSQL = sSQL & "  Join #Sinistro_Principal                           (Index = FK_Cliente_ID)" & vbNewLine 'cristovao.rodrigues 05/11/2012 retirado index
''    sSQL = sSQL & "  Join #Sinistro_Principal                           " & vbNewLine
''    sSQL = sSQL & "    on #Sinistro_Principal.Cliente_ID                = Cliente_Tb.Cliente_ID " & vbNewLine
''    sSQL = sSQL & "" & vbNewLine
''
''End Sub

'AKIO.OKUNO - #RETIRADA - 09/05/2013
Private Function SelecionaDados_AbaEstimativa_CoberturaAfetada_Local(lConexao As Integer) As ADODB.Recordset
'Private Sub SelecionaDados_AbaEstimativa_CoberturaAfetada_Local(lConexao As Integer)
'    Set SelecionaDados_AbaEstimativa_CoberturaAfetada lConexao
    Set SelecionaDados_AbaEstimativa_CoberturaAfetada_Local = SelecionaDados_AbaEstimativa_CoberturaAfetada(lConexao)
End Function

'AKIO.OKUNO - #RETIRADA - SelecionaDados_AbaCausasEEventos - 07/05/2013
'Private Sub SelecionaDados_AbaCausasEEventos()
'    Dim sSQL As String
'
'    On Error GoTo Erro
'
'    MousePointer = vbHourglass
'
''        sSQL = sSQL & "SELECT cod_objeto_segurado       = b.cod_objeto_segurado                                     " & vbNewLine
''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                                " & vbNewLine
''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                    " & vbNewLine
''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                      " & vbNewLine
''        sSQL = sSQL & "     , Municipio                 = a.municipio                                                   " & vbNewLine
''        sSQL = sSQL & "     , Estado                    = a.estado                                                      " & vbNewLine
''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = b.dt_inicio_vigencia_seg                                      " & vbNewLine
''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = b.dt_fim_vigencia_seg                                         " & vbNewLine
''        sSQL = sSQL & "     , Tipo                      = 1                                                         " & vbNewLine
''        sSQL = sSQL & "  Into #Sinistro_Principal_Endereco_Risco                                                        " & vbNewLine
''        sSQL = sSQL & "  FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_residencial_tb b  WITH (NOLOCK)                 " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                       " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = b.Proposta_ID                                           " & vbNewLine
''        sSQL = sSQL & " Where a.Proposta_id = b.Proposta_id                                                         " & vbNewLine
''        sSQL = sSQL & "   AND a.end_risco_id = b.end_risco_id                                                           " & vbNewLine
''        sSQL = sSQL & "   AND b.dt_fim_vigencia_seg IS NULL                                                         " & vbNewLine
''        sSQL = sSQL & " Union                                                                                        " & vbNewLine
''        sSQL = sSQL & "SELECT cod_objeto_segurado       = c.cod_objeto_segurado                                      " & vbNewLine
''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                             " & vbNewLine
''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                 " & vbNewLine
''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                   " & vbNewLine
''        sSQL = sSQL & "     , Municipio                 = a.municipio                                                " & vbNewLine
''        sSQL = sSQL & "     , Estado                    = a.estado                                                   " & vbNewLine
''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = c.dt_inicio_vigencia_seg                                   " & vbNewLine
''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = c.dt_fim_vigencia_seg                                      " & vbNewLine
''        sSQL = sSQL & "     , Tipo                      = 2                                                          " & vbNewLine
''        sSQL = sSQL & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_empresarial_tb c  WITH (NOLOCK)                   " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                    " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = c.Proposta_ID                                        " & vbNewLine
''        sSQL = sSQL & " Where a.Proposta_id = c.Proposta_id                                                      " & vbNewLine
''        sSQL = sSQL & " AND a.end_risco_id = c.end_risco_id                                                      " & vbNewLine
''        sSQL = sSQL & " AND c.dt_fim_vigencia_seg IS NULL                                                            " & vbNewLine
''        sSQL = sSQL & " Union                                                                                        " & vbNewLine
''        sSQL = sSQL & "SELECT cod_objeto_segurado       = d.cod_objeto_segurado                                      " & vbNewLine
''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                             " & vbNewLine
''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                 " & vbNewLine
''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                   " & vbNewLine
''        sSQL = sSQL & "     , Municipio                 = a.municipio                                                " & vbNewLine
''        sSQL = sSQL & "     , Estado                    = a.estado                                                   " & vbNewLine
''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = d.dt_inicio_vigencia_seg                                   " & vbNewLine
''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = d.dt_fim_vigencia_seg                                      " & vbNewLine
''        sSQL = sSQL & "     , Tipo                      = 3                                                           " & vbNewLine
''        sSQL = sSQL & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_condominio_tb d  WITH (NOLOCK)                    " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                    " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = d.Proposta_ID                                        " & vbNewLine
''        sSQL = sSQL & " Where a.Proposta_id = d.Proposta_id                                                      " & vbNewLine
''        sSQL = sSQL & " AND a.end_risco_id = d.end_risco_id                                                      " & vbNewLine
''        sSQL = sSQL & " AND d.dt_fim_vigencia_seg IS NULL                                                            " & vbNewLine
''        sSQL = sSQL & " Union                                                                                        " & vbNewLine
''        sSQL = sSQL & "SELECT cod_objeto_segurado       = e.cod_objeto_segurado                                      " & vbNewLine
''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                             " & vbNewLine
''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                 " & vbNewLine
''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                   " & vbNewLine
''        sSQL = sSQL & "     , Municipio                 = a.municipio                                                " & vbNewLine
''        sSQL = sSQL & "     , Estado                    = a.estado                                                   " & vbNewLine
''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = e.dt_inicio_vigencia_seg                                   " & vbNewLine
''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = e.dt_fim_vigencia_seg                                      " & vbNewLine
''        sSQL = sSQL & "     , Tipo                      = 4                                                          " & vbNewLine
''        sSQL = sSQL & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_maquinas_tb e  WITH (NOLOCK)                      " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                    " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = e.Proposta_ID                                        " & vbNewLine
''        sSQL = sSQL & " Where a.Proposta_id = e.Proposta_id                                                      " & vbNewLine
''        sSQL = sSQL & " AND a.end_risco_id = e.end_risco_id                                                      " & vbNewLine
''        sSQL = sSQL & " AND e.dt_fim_vigencia_seg IS NULL                                                            " & vbNewLine
''        sSQL = sSQL & " Union                                                                                        " & vbNewLine
''        sSQL = sSQL & "SELECT cod_objeto_segurado       = f.cod_objeto_segurado                                      " & vbNewLine
''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                             " & vbNewLine
''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                 " & vbNewLine
''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                   " & vbNewLine
''        sSQL = sSQL & "     , Municipio                 = a.municipio                                                " & vbNewLine
''        sSQL = sSQL & "     , Estado                    = a.estado                                                   " & vbNewLine
''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = f.dt_inicio_vigencia_seg                                   " & vbNewLine
''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = f.dt_fim_vigencia_seg                                      " & vbNewLine
''        sSQL = sSQL & "     , Tipo                      = 5                                                          " & vbNewLine
''        sSQL = sSQL & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_aceito_tb f  WITH (NOLOCK)                    " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = f.Proposta_ID                                       " & vbNewLine
''        sSQL = sSQL & " Where a.Proposta_id = f.Proposta_id                                                     " & vbNewLine
''        sSQL = sSQL & " AND a.end_risco_id = f.end_risco_id                                                     " & vbNewLine
''        sSQL = sSQL & " AND f.dt_fim_vigencia_seg IS NULL                                                           " & vbNewLine
''        sSQL = sSQL & " Union                                                                                       " & vbNewLine
''        sSQL = sSQL & "SELECT cod_objeto_segurado       = g.cod_objeto_segurado                                     " & vbNewLine
''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                            " & vbNewLine
''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                " & vbNewLine
''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                  " & vbNewLine
''        sSQL = sSQL & "     , Municipio                 = a.municipio                                               " & vbNewLine
''        sSQL = sSQL & "     , Estado                    = a.estado                                                  " & vbNewLine
''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = g.dt_inicio_vigencia_seg                                  " & vbNewLine
''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = g.dt_fim_vigencia_seg                                     " & vbNewLine
''        sSQL = sSQL & "     , Tipo                      = 6                                                         " & vbNewLine
''        sSQL = sSQL & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_avulso_tb g  WITH (NOLOCK)                   " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = g.Proposta_ID                                       " & vbNewLine
''        sSQL = sSQL & " Where a.Proposta_id = g.Proposta_id                                                     " & vbNewLine
''        sSQL = sSQL & " AND a.end_risco_id = g.end_risco_id                                                     " & vbNewLine
''        sSQL = sSQL & " AND g.dt_fim_vigencia_seg IS NULL                                                           " & vbNewLine
''        sSQL = sSQL & " Union                                                                                       " & vbNewLine
''        sSQL = sSQL & "SELECT cod_objeto_segurado       = h.cod_objeto_segurado                                     " & vbNewLine
''        sSQL = sSQL & "     , end_risco_id              = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Endereco                  = h.desc_mercadoria                                         " & vbNewLine
''        sSQL = sSQL & "     , Bairro                    = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Municipio                 = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Estado                    = NULL                                                       " & vbNewLine
''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = h.dt_inicio_vigencia_seg                                  " & vbNewLine
''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = h.dt_fim_vigencia_seg                                     " & vbNewLine
''        sSQL = sSQL & "     , Tipo                      = 7                                                         " & vbNewLine
''        sSQL = sSQL & " FROM seguro_transporte_tb h  WITH (NOLOCK)                                                     " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = h.Proposta_ID                                       " & vbNewLine
''        sSQL = sSQL & " Where h.dt_fim_vigencia_seg IS NULL                                                     " & vbNewLine
''        sSQL = sSQL & " Union                                                                                       " & vbNewLine
''        sSQL = sSQL & "SELECT cod_objeto_segurado       = h.cod_objeto_segurado                                     " & vbNewLine
''        sSQL = sSQL & "     , end_risco_id              = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Endereco                  = h.desc_mercadoria                                         " & vbNewLine
''        sSQL = sSQL & "     , Bairro                    = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Municipio                 = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Estado                    = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = h.dt_inicio_vigencia_seg                                  " & vbNewLine
''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = h.dt_fim_vigencia_seg                                     " & vbNewLine
''        sSQL = sSQL & "     , Tipo                      = 7                                                         " & vbNewLine
''        sSQL = sSQL & " FROM seguro_transporte_tb h  WITH (NOLOCK)                                                     " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = h.Proposta_ID                                       " & vbNewLine
''        sSQL = sSQL & " Where dt_fim_vigencia_seg = (select max(dt_fim_vigencia_seg)                                " & vbNewLine
''        sSQL = sSQL & "                                from seguro_transporte_tb h1  WITH (NOLOCK)                     " & vbNewLine
''        sSQL = sSQL & "                                Join #Sinistro_Principal                                     " & vbNewLine
''        sSQL = sSQL & "                                  on #Sinistro_Principal.Proposta_ID = h1.Proposta_ID        " & vbNewLine
''        sSQL = sSQL & "                               Where h1.cod_objeto_segurado = h.cod_objeto_segurado          " & vbNewLine
''        sSQL = sSQL & "                                 and h1.dt_fim_vigencia_seg is not null)                 " & vbNewLine
''        sSQL = sSQL & " and not exists            (select 1 from seguro_transporte_tb h2  WITH (NOLOCK)                " & vbNewLine
''        sSQL = sSQL & "                                Join #Sinistro_Principal                                     " & vbNewLine
''        sSQL = sSQL & "                                  on #Sinistro_Principal.Proposta_ID = h2.Proposta_ID         " & vbNewLine
''        sSQL = sSQL & "                               Where h2.cod_objeto_segurado = h.cod_objeto_segurado          " & vbNewLine
''        sSQL = sSQL & "                                 and h2.dt_fim_vigencia_seg is null)                     " & vbNewLine
''        sSQL = sSQL & " Union                                                                                       " & vbNewLine
''        sSQL = sSQL & "SELECT cod_objeto_segurado       = i.cod_objeto_segurado                                     " & vbNewLine
''        sSQL = sSQL & "     , end_risco_id              = a.end_risco_id                                            " & vbNewLine
''        sSQL = sSQL & "     , Endereco                  = a.endereco                                                " & vbNewLine
''        sSQL = sSQL & "     , Bairro                    = a.bairro                                                  " & vbNewLine
''        sSQL = sSQL & "     , Municipio                 = a.municipio                                               " & vbNewLine
''        sSQL = sSQL & "     , Estado                    = a.estado                                                  " & vbNewLine
''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = i.dt_inicio_vigencia_seg                                  " & vbNewLine
''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = i.dt_fim_vigencia_seg                                     " & vbNewLine
''        sSQL = sSQL & "     , Tipo                      = 8                                                         " & vbNewLine
''        sSQL = sSQL & " FROM endereco_risco_tb a  WITH (NOLOCK)  , seguro_generico_tb i  WITH (NOLOCK)                    " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = i.Proposta_ID                                       " & vbNewLine
''        sSQL = sSQL & " Where a.Proposta_id = i.Proposta_id                                                     " & vbNewLine
''        sSQL = sSQL & " AND a.end_risco_id = i.end_risco_id                                                     " & vbNewLine
''        sSQL = sSQL & " AND i.dt_fim_vigencia_seg IS NULL                                                           " & vbNewLine
''        sSQL = sSQL & " UNION                                                                                       " & vbNewLine
''        sSQL = sSQL & "SELECT cod_objeto_segurado       = j.cod_objeto_segurado                                     " & vbNewLine
''        sSQL = sSQL & "     , end_risco_id              = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Endereco                  = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Bairro                    = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Municipio                 = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Estado                    = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = j.dt_inicio_vigencia_seg                                  " & vbNewLine
''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Tipo                      = 9                                                          " & vbNewLine
''        sSQL = sSQL & " FROM seguro_penhor_rural_tb j  WITH (NOLOCK)                                               " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = j.Proposta_ID                                       " & vbNewLine
''        sSQL = sSQL & " UNION                                                                                       " & vbNewLine
''        sSQL = sSQL & "SELECT cod_objeto_segurado       = k.cod_objeto_segurado                                     " & vbNewLine
''        sSQL = sSQL & "     , end_risco_id              = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Endereco                  = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Bairro                    = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Municipio                 = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Estado                    = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Dt_inicio_Vigencia_Seg    = k.dt_inicio_vigencia_seg                                  " & vbNewLine
''        sSQL = sSQL & "     , Dt_Fim_Vigencia_Seg       = NULL                                                      " & vbNewLine
''        sSQL = sSQL & "     , Tipo                      = 10                                                        " & vbNewLine
''        sSQL = sSQL & " FROM seguro_consorcio_quebra_garantia_tb k  WITH (NOLOCK)                                      " & vbNewLine
''        sSQL = sSQL & "  Join #Sinistro_Principal                                                                   " & vbNewLine
''        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID = k.Proposta_ID                                       " & vbNewLine
''
''
''        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
''                                 glAmbiente_id, _
''                                 App.Title, _
''                                 App.FileDescription, _
''                                 sSQL, _
''                                 lConexaoLocal, _
''                                 False)
''
'    End If
'
'' Alan Neves - 15/02/2013
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosGerais_Aviso'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #DadosGerais_Aviso" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Create Table #DadosGerais_Aviso " & vbNewLine
'    sSQL = sSQL & "     ( Sinistro_Bb                           Numeric(13)         " & vbNewLine
'    sSQL = sSQL & "     , Dt_Fim_Vigencia                       smalldatetime       " & vbNewLine
'    sSQL = sSQL & "     , Agencia_ID                            Numeric(4)          " & vbNewLine
'    sSQL = sSQL & "     , Situacao                              varchar(20)  )      " & vbNewLine  ' -- Sinistro_Tb
'
'
''        sSQL = ""
''        sSQL = sSQL & "  Exec desenv_db.dbo.SelecionaDados_AbaDadosGerais_e_Form '" & txtProposta_ID & "','" & lblSinistro_ID(0).Caption & "'" & ", '" & lAviso_Solicitante_ID & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'," & bytTipoRamo & vbNewLine
'        sSQL = sSQL & "  Exec seguros_db.dbo.SEGS10922_SPS '" & txtProposta_ID & "','" & lblSinistro_ID(0).Caption & "'" & ", '" & lAviso_Solicitante_ID & "','" & strTipoProcesso & "','" & strTipoProcesso_Avisar & "','" & strTipoProcesso_Avisar_Cosseguro & "'," & bytTipoRamo & vbNewLine
'
'        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                 glAmbiente_id, _
'                                 App.Title, _
'                                 App.FileDescription, _
'                                 sSQL, _
'                                 lConexaoLocal, _
'                                 False)
'
'
'    Exit Sub
'
'Erro:
'    Call TrataErroGeral("SelecionaDados_AbaEstimativa", Me.Caption)
'    Call FinalizarAplicacao
'
'
'End Sub

'AKIO.OKUNO - #RETIRADA - SelecionaDados_AbaDadosEspecificos - 02/05/2013
'Private Sub SelecionaDados_AbaDadosEspecificos()
'    Dim sSQL As String
'
'    On Error GoTo Erro
'
'    MousePointer = vbHourglass
'
'
'    '-->> Dados do Frame: A��o Judicial
'    sSQL = ""
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosEspecificos_AcaoJudicial'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #DadosEspecificos_AcaoJudicial" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    'Alan Neves - 18/04/2013 - Inicio
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosEspecificos_Prestador'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #DadosEspecificos_Prestador" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosEspecificos_Regulacao'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #DadosEspecificos_Regulacao" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    'Alan Neves - 18/04/2013 - Fim
'
'    sSQL = sSQL & "Select Dt_Acionamento                                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Dt_Julgamento                                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Resultado                                     = Resultado_Acao                                                " & vbNewLine
'    sSQL = sSQL & "     , Motivo                                            = Motivo_Acao                                               " & vbNewLine
'    sSQL = sSQL & "     , Tipo                                              = 'E'                                                       " & vbNewLine
'    sSQL = sSQL & "  Into #DadosEspecificos_AcaoJudicial                                                                                " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Acao_Judicial_Tb                   Acao_Judicial_Tb  WITH (NOLOCK)                                    " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                           " & vbNewLine
'    sSQL = sSQL & " on #Sinistro_Principal.Sinistro_ID                  = Acao_Judicial_Tb.Sinistro_ID                                  " & vbNewLine
'    sSQL = sSQL & "                                                                                                                     " & vbNewLine
'    sSQL = sSQL & "                                                                                                                     " & vbNewLine
'    'AKIO.OKUNO - INICIO - 11/10/2012
'    sSQL = sSQL & "Update #Sinistro_Principal                                                                                           " & vbNewLine
'    sSQL = sSQL & "   Set Passivel_Ressarcimento = Sinistro_Tb.Passivel_Ressarcimento                                                   " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Tb                    Sinistro_Tb WITH (NOLOCK)                                              " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                           " & vbNewLine
'    sSQL = sSQL & " on #Sinistro_Principal.Sinistro_ID                  = Sinistro_Tb.Sinistro_ID                                       " & vbNewLine
'    'AKIO.OKUNO - FIM - 11/10/2012
'    '-->> Dados do Frame: Prestadores
'    sSQL = sSQL & "Select Prestador_Tb.Nome                                                                                             " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Banco                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Agencia                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Agencia_Dv                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Conta                                         = Prestador_Tb.Conta_Corrente                                   " & vbNewLine
'    sSQL = sSQL & "     , Conta_Dv                                          = Prestador_Tb.Conta_Corrente_Dv                            " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Estado                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Municipio                                     = Municipio_Tb.Nome                                             " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Prestador_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Prestador_Tb.Usuario                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Prestador_PJ_Tb.CGC                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Prestador_PF_Tb.CPF                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Endereco                                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Bairro                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.CEP                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , DDD                                               = Prestador_Tb.DDD_1                                        " & vbNewLine
'    sSQL = sSQL & "     , Telefone                                          = Prestador_Tb.Telefone_1                                   " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Variacao_Poupanca                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Tipo                                              = 'E'                                                       " & vbNewLine
'    sSQL = sSQL & "  Into #DadosEspecificos_Prestador                                                                                   " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Benef_Prestador_Tb        Sinistro_Benef_Prestador_Tb  WITH (NOLOCK)                         " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Prestador_Tb                       Prestador_Tb  WITH (NOLOCK)                                        " & vbNewLine
'    sSQL = sSQL & "    on Prestador_Tb.Prestador_ID                     = Sinistro_Benef_Prestador_Tb.Prestador_ID                      " & vbNewLine
'    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Prestador_PJ_Tb               Prestador_PJ_Tb  WITH (NOLOCK)                                     " & vbNewLine
'    sSQL = sSQL & "    on Prestador_PJ_Tb.Prestador_ID                      = Prestador_Tb.Prestador_ID                                 " & vbNewLine
'    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Prestador_PF_Tb               Prestador_PF_Tb  WITH (NOLOCK)                                     " & vbNewLine
'    sSQL = sSQL & "    on Prestador_PF_Tb.Prestador_ID                      = Prestador_Tb.Prestador_ID                                 " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Municipio_Tb                       Municipio_Tb  WITH (NOLOCK)                                        " & vbNewLine
'    sSQL = sSQL & "    on Municipio_Tb.Municipio_ID                     = Prestador_Tb.Municipio_ID                                     " & vbNewLine
'    sSQL = sSQL & "   and Municipio_Tb.Estado                               = Prestador_Tb.Estado                                       " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                           " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID                   = Sinistro_Benef_Prestador_Tb.Sinistro_ID                   " & vbNewLine
'    sSQL = sSQL & " Order By Prestador_Tb.Prestador_ID                                                                                  " & vbNewLine
'    sSQL = sSQL & "                                                                                                                     " & vbNewLine
'
'    '-->> Dados do Frame: Reguladores
'    sSQL = sSQL & "Select Vistoria_Tb.Dt_Pedido_Vistoria                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Regulador_ID                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Parecer_Preliminar                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Resultado_Preliminar                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Dt_Fim_Preliminar                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Parecer_Vistoria                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Resultado_Vistoria                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Dt_Parecer_Vistoria                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Usuario                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Nome = Regulador_Tb.Nome                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Banco                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Agencia                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Agencia_Dv                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Conta_Corrente                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Conta_Corrente_Dv                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Regulador_PJ_Tb.CGC                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Regulador_PF_Tb.CPF                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Endereco                                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Bairro                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Municipio_Nome = Municipio_Tb.Nome                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Estado                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.CEP                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.DDD_1                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Telefone_1                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Qld_Tecnica_Preliminar                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Cump_Procedimentos_Preliminar                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Qld_Tecnica_Vistoria                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Cump_Procedimentos_Vistoria                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Variacao_Poupanca                                                                                " & vbNewLine
'    sSQL = sSQL & "  Into #DadosEspecificos_Regulacao                                                                                   " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Vistoria_Tb                    Vistoria_Tb  WITH (NOLOCK)                                             " & vbNewLine
'    sSQL = sSQL & " Inner Join Seguros_Db.Dbo.Regulador_Tb              Regulador_Tb  WITH (NOLOCK)                                            " & vbNewLine
'    sSQL = sSQL & "    on Vistoria_Tb.Regulador_ID                      = Regulador_Tb.Regulador_ID                                     " & vbNewLine
'    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Regulador_PJ_Tb           Regulador_PJ_Tb  WITH (NOLOCK)                                         " & vbNewLine
'    sSQL = sSQL & "    on Regulador_PJ_Tb.Regulador_ID                  = Regulador_Tb.Regulador_ID                                     " & vbNewLine
'    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Regulador_PF_Tb           Regulador_PF_Tb  WITH (NOLOCK)                                         " & vbNewLine
'    sSQL = sSQL & "    on Regulador_PF_Tb.Regulador_ID                  = Regulador_Tb.Regulador_ID                                     " & vbNewLine
'    sSQL = sSQL & " Inner Join Seguros_Db.Dbo.Municipio_Tb              Municipio_Tb  WITH (NOLOCK)                                            " & vbNewLine
'    sSQL = sSQL & "    on Municipio_Tb.Municipio_ID                     = Regulador_Tb.Municipio_ID                                     " & vbNewLine
'    sSQL = sSQL & "   and Municipio_Tb.Estado                           = Regulador_Tb.Estado                                           " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                           " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Vistoria_Tb.Sinistro_ID                                       " & vbNewLine
'    sSQL = sSQL & " Order By Regulador_Tb.Regulador_ID                                                                                  " & vbNewLine
'
'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
'    Exit Sub
'
'Erro:
'    Call TrataErroGeral("SelecionaDados_AbaEstimativa", Me.Caption)
'    Call FinalizarAplicacao
'
'End Sub

Private Sub InicializaCargaDados_LimpezaTemporario()
'    sSQL = ""
'
'    'Alan Neves - 15/04/2013 - Inicio - Drop realizado em FrmConsultaSinistroCon
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosGerais_Aviso'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #DadosGerais_Aviso" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
'    'Alan Neves - 15/04/2013 - Fim
'
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Historico_Evolucao'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Historico_Evolucao" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Evento_SegBR_Sinistro_Atual_Temp'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Evento_SegBR_Sinistro_Atual_Temp" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Evento_SegBR_Sinistro_Atual'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Evento_SegBR_Sinistro_Atual" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Evento_ID'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Evento_ID" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Localizacao'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Localizacao" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
''AKIO.OKUNO - #RETIRADA - INICIO - 02/05/2013
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosEspecificos_AcaoJudicial'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #DadosEspecificos_AcaoJudicial" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosEspecificos_Regulacao'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #DadosEspecificos_Regulacao" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosEspecificos_Prestador'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #DadosEspecificos_Prestador" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''AKIO.OKUNO - #RETIRADA - FIM - 02/05/2013
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Detalhamento_Visualizacao_Detalhamento'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Detalhamento_Visualizacao_Detalhamento" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Detalhamento_Visualizacao_Detalhamento_Linha'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Detalhamento_Visualizacao_Detalhamento_Linha" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#EstimativaPagamento_CoberturaAfetada'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #EstimativaPagamento_CoberturaAfetada" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
''AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#EstimativaPagamento_Estimativa'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #EstimativaPagamento_Estimativa" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''AKIO.OKUNO - #RETIRADA - FIM - 09/05/2013
'
''AKIO.OKUNO - #RETIRADA - INICIO - 02/05/2013
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#EstimativaPagamento_Voucher'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #EstimativaPagamento_Voucher" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''AKIO.OKUNO - #RETIRADA - FIM - 02/05/2013
'
''AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#EstimativaPagamento_Estimativa_Atual'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #EstimativaPagamento_Estimativa_Atual" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''AKIO.OKUNO - #RETIRADA - FIM - 09/05/2013
'
''AKIO.OKUNO - #RETIRADA - INICIO - 03/05/2013
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#EstimativaPagamento_Beneficiario'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #EstimativaPagamento_Beneficiario" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''AKIO.OKUNO - #RETIRADA - FIM - 03/05/2013
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Proposta_Adesao'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Proposta_Adesao" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Corretor'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Corretor" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    'Alan Neves - 15/04/2013 - Inicio - Drop realizado em FrmConsultaSinistroCon
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosGerais_Aviso'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #DadosGerais_Aviso" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
'    'Alan Neves - 15/04/2013 - Fim
'
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Principal_Endereco_Risco'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Sinistro_Principal_Endereco_Risco" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
''AKIO.OKUNO - #RETIRADA - INICIO - 02/05/2013
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#DadosEspecificos_AcaoJudicial'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #DadosEspecificos_AcaoJudicial" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''AKIO.OKUNO - #RETIRADA - INICIO - 02/05/2013
'
''AKIO.OKUNO - #RETIRADA - INICIO - 07/05/2013
''    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#CausaEvento'),0) >0" & vbNewLine
''    sSQL = sSQL & " BEGIN" & vbNewLine
''    sSQL = sSQL & "     DROP TABLE #CausaEvento" & vbNewLine
''    sSQL = sSQL & " END" & vbNewLine
''AKIO.OKUNO - #RETIRADA - INICIO - 07/05/2013
'
'
'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
End Sub

'AKIO.OKUNO - #RETIRADA - CarregaDadosColecao - 09/05/2013
'INCLUS�O DO PARAMETRO rsRecordSet PARA N�O TER DAR EXECUTE NOVAMENTE
Private Sub CarregaDadosColecao(bAba As Byte, Optional rsRecordSet As ADODB.Recordset)
'Private Sub CarregaDadosColecao(bAba As Byte)
    If bAba = 3 Then
        CarregaDadosColecao_Estimativa rsRecordSet
    End If
End Sub

'AKIO.OKUNO - #RETIRADA - CarregaDadosColecao_Estimativa - 09/05/2013
'INCLUS�O DO PARAMETRO rsRecordSet PARA N�O TER DAR EXECUTE NOVAMENTE
Private Sub CarregaDadosColecao_Estimativa(Optional rsRecordSet As ADODB.Recordset)
'AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
'Private Sub CarregaDadosColecao_Estimativa()
'    Dim RsRecordSet As Adodb.Recordset
'AKIO.OKUNO - #RETIRADA - FIM - 09/05/2013
    Dim sSQL As String
    Dim oEstimativa As New clsEstimativa

    On Error GoTo Erro

    MousePointer = vbHourglass

'AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
'    sSQL = ""
'    sSQL = sSQL & "Select * " & vbCrLf
'    sSQL = sSQL & "  From #EstimativaPagamento_Estimativa"
'
'    Set RsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                          glAmbiente_id, _
'                                          App.Title, _
'                                          App.FileDescription, _
'                                          sSQL, _
'                                          lConexaoLocal, _
'                                          True)
'AKIO.OKUNO - #RETIRADA - FIM - 09/05/2013

    With rsRecordSet
        Do While Not .EOF
            Set oEstimativa = New clsEstimativa
            oEstimativa.Seq_Estimativa = .Fields!Seq_Estimativa
            If .Fields!Dt_Inicio_Estimativa <> "00:00:00" Then
                oEstimativa.Dt_Inicio_Estimativa = .Fields!Dt_Inicio_Estimativa
            Else
                oEstimativa.Dt_Fim_Estimativa = "1900-01-01"
            End If

            If .Fields!Dt_Fim_Estimativa <> "00:00:00" Then
                oEstimativa.Dt_Fim_Estimativa = .Fields!Dt_Fim_Estimativa
            End If

            oEstimativa.Item_Val_Estimativa = .Fields!Item_Val_Estimativa
            oEstimativa.Val_Estimado = .Fields!Val_Estimado
            oEstimativa.Val_Pago = .Fields!Val_Pago
            oEstimativa.Val_Resseguro = .Fields!Val_Resseguro
            oEstimativa.Situacao = .Fields!Situacao
            oEstimativa.Usuario = .Fields!Usuario
            oEstimativa.Num_Carta_Seguro_Aceito = .Fields!Num_Carta_Seguro_Aceito
            oEstimativa.Perc_Re_Seguro_Quota = .Fields!Perc_Re_Seguro_Quota
            oEstimativa.Perc_Re_Seguro_Er = .Fields!Perc_Re_Seguro_Er
            oEstimativa.Tipo = .Fields!Tipo
            oEstimativa.Nossa_Parte = .Fields!Nossa_Parte

            If .Fields!Dt_Inclusao <> "00:00:00" Then
                oEstimativa.Dt_Inclusao = .Fields!Dt_Inclusao
            End If

            'MU-2017-077791 - Ajuste autom�tico das Reservas � SEGBR (Victor Fonseca)
            'Inclus�o do valor m�dio para o sinistro consultado
            If sOperacaoCosseguro <> "C" Then
                If glAmbiente_id = 6 Or glAmbiente_id = 7 Or glAmbiente_id = 8 Then
                    If Not IsNull(.Fields!Val_Custo_Medio) Then
                        oEstimativa.Val_Custo_Medio = .Fields!Val_Custo_Medio
                    Else
                        oEstimativa.Val_Custo_Medio = .Fields!Val_Estimado
                    End If
                End If
            End If



            cDados_Estimativa.Add oEstimativa
            .MoveNext
        Loop

    End With

    MousePointer = vbNormal

'    Set RsRecordSet = Nothing      'AKIO.OKUNO - #RETIRADA - 09/05/2013
    Set oEstimativa = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosColecao_Estimativa", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosGrid_AbaDadosGerais_Aviso()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String
    Dim bAgenciaIDVisible As Boolean
    On Error GoTo Erro

    sSQL = ""
    sSQL = sSQL & "Select * " & vbCrLf
    sSQL = sSQL & "  From #DadosGerais_Aviso"
    sSQL = sSQL & " Order by Sinistro_BB Desc"          'AKIO.OKUNO - 07/05/2013
    'Order by inserido para atender demanda de melhoria para

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    With rsRecordSet
        bAgenciaIDVisible = .EOF
        Do While Not .EOF
            'FLAVIO.BEZERRA - IN�CIO - 06/02/2013
            'sLinha = .Fields!Sinistro_BB & vbTab
            'sLinha = sLinha & .Fields!Agencia_ID & vbTab
            'sLinha = sLinha & .Fields!Situacao & ""
            'grdDadosGerais_Aviso.AddItem sLinha
            '.MoveNext
            sLinha = .Fields!Sinistro_BB & vbTab
            sLinha = sLinha & .Fields!Agencia_ID & vbTab
            sLinha = sLinha & .Fields!Situacao & vbTab
            sLinha = sLinha & Obtem_Localizacao_Sinistro_BB(.Fields!Sinistro_BB) & ""
            grdDadosGerais_Aviso.AddItem sLinha
            .MoveNext
            'FLAVIO.BEZERRA - FIM - 06/02/2013
        Loop

        If bAgenciaIDVisible Then
            grdDadosGerais_Aviso.ColWidth(1) = 0
        End If
        lblDadosGerais_Aviso(6).Visible = bAgenciaIDVisible
        txtAgencia_ID.Visible = bAgenciaIDVisible

    End With

    InicializaModoProcessamentoObjeto grdDadosGerais_Aviso, 2

    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosGrid_AbaDadosGerais_Aviso", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosTela_AbaHistorico()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String

    On Error GoTo Erro

    If Not bCarregou_AbaHistorico Then

        MousePointer = vbHourglass

        InicializaModoProcessamentoObjeto grdHistorico_EvolucaoHistorico, 1

'AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
'        SelecionaDados 2           'teste #retirada
 '       sSQL = ""
 '       sSQL = sSQL & "Select * " & vbCrLf
 '       sSQL = sSQL & "  From #Historico_Evolucao" & vbCrLf
 '       sSQL = sSQL & " Where Descricao is not Null" & vbCrLf
 '       'sSQL = sSQL & "   and Usuario <> 'GTR' " & vbCrLf           'AKIOOKUNO
 '       sSQL = sSQL & " Order By Dt_Evento Desc" & vbCrLf
 '       'mathayde
 '       'sSQL = sSQL & "     , Seq_Evento" & vbCrLf
 '       sSQL = sSQL & "     , Seq_Evento desc" & vbCrLf
 '       '        sSQL = sSQL & "     , Linha_ID" & vbCrLf
'AKIO.OKUNO - #RETIRADA - FIM - 09/05/2013
        
        sSQL = "exec seguros_db.Dbo.SEGS11113_SPS"

        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, _
                                              True)

        With grdHistorico_EvolucaoHistorico
            Altura = .RowHeight(0)
            .ColAlignment(0) = 0
            .ColAlignment(1) = 0
            .ColAlignment(2) = 0
            .ColAlignment(3) = 0
            .MergeCol(0) = True
            .MergeCol(1) = True
            .MergeCol(2) = True
            .MergeCol(3) = True
            .MergeCells = flexMergeRestrictColumns
        End With

'AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
  '      With rsRecordSet
  '          Do While Not .EOF
  '              If Not IsNull(.Fields!dt_Evento) Then
  '                  sLinha = Format(.Fields!dt_Evento, "dd/mm/yyyy") & vbTab
  '              Else
  '                  sLinha = sLinha & vbTab
  '              End If
  '              sLinha = sLinha & .Fields!Usuario & vbTab
  '              sLinha = sLinha & .Fields!cargo & vbTab
  '              sLinha = sLinha & .Fields!Descricao_Evento_SEGBR & vbTab
  '              sLinha = sLinha & " " & .Fields!Descricao & ""
  '              grdHistorico_EvolucaoHistorico.AddItem sLinha
  '              .MoveNext
  '          Loop
  '      End With
        CarregaDadosGrid grdHistorico_EvolucaoHistorico, rsRecordSet
'AKIO.OKUNO - #RETIRADA - FIM - 09/05/2013
        'Cleber Sardo - INC000005122919 - Data 01/09/2016 - Hist�rico vazio
        If grdHistorico_EvolucaoHistorico.Rows > 1 Then
        grdHistorico_EvolucaoHistorico.ColWidth(5) = 0        'AKIO.OKUNO - 09/05/2013
        grdHistorico_EvolucaoHistorico.ColWidth(6) = 0        'AKIO.OKUNO - 09/05/2013
        grdHistorico_EvolucaoHistorico.ColWidth(7) = 0        'AKIO.OKUNO - 09/05/2013
        End If
        Set rsRecordSet = Nothing

        bCarregou_AbaHistorico = True

        MousePointer = vbNormal

        InicializaModoProcessamentoObjeto grdHistorico_EvolucaoHistorico, 2

    End If
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosTela_AbaHistorico", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosTela_AbaEstimativasPagamentos(iSeq_Estimativa As Integer)
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim oEstimativa As clsEstimativa
    Dim bSemEstimativa As Boolean

    On Error GoTo Erro

    If Not bCarregou_AbaEstimativasPagamentos Or bAlterou_Seq_Estimativa Then

        MousePointer = vbHourglass

        If Not bCarregou_AbaEstimativasPagamentos Then
'            InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Beneficiario, 1     'AKIO.OKUNO - MP - 10/05/2013
            InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Coberturas, 1
            InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Estimativas, 1
'            InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Voucher, 1          'AKIO.OKUNO - MP - 10/05/2013

'            SelecionaDados 3       'AKIO.OKUNO - #RETIRADA - 09/05/2013

            sSQL = ""
'AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
'            sSQL = sSQL & "Select * " & vbCrLf
'            sSQL = sSQL & "  From #EstimativaPagamento_Estimativa" & vbCrLf
'            sSQL = sSQL & " where 1 = 1 "
'            If iSeq_Estimativa <> 0 Then
'                sSQL = sSQL & "   and Seq_Estimativa = " & iSeq_Estimativa
'            End If
            sSQL = sSQL & "Exec seguros_db.Dbo.SEGS11115_SPS "
            If iSeq_Estimativa <> 0 Then
                sSQL = sSQL & iSeq_Estimativa
            End If
'AKIO.OKUNO - #RETIRADA - FIM - 09/05/2013

            Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                  glAmbiente_id, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  sSQL, _
                                                  lConexaoLocal, _
                                                  True)
            bSemEstimativa = rsRecordSet.EOF
            If rsRecordSet.EOF Then
                sSQL = ""
                sSQL = sSQL & "Select Sigla  AS Moeda_Sigla                                                                                                                           " & vbNewLine
                sSQL = sSQL & "     , Moeda_ID                                                                                                                          " & vbNewLine
                sSQL = sSQL & "  From #Sinistro_Principal                                                                                                               " & vbNewLine
                sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Fechada_Tb       Proposta_Fechada_Tb  WITH (NOLOCK)                                                         " & vbNewLine
                sSQL = sSQL & "    on Proposta_Fechada_Tb.Proposta_ID               = #Sinistro_Principal.Proposta_ID                                                   " & vbNewLine
                sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Adesao_Tb        Proposta_Adesao_Tb  WITH (NOLOCK)                                                          " & vbNewLine
                sSQL = sSQL & "    on Proposta_Adesao_Tb.Proposta_ID                = #Sinistro_Principal.Proposta_ID                                                   " & vbNewLine
                sSQL = sSQL & "  Join Seguros_Db.Dbo.Moeda_Tb                       Moeda_Tb  WITH (NOLOCK)                                                                    " & vbNewLine
                sSQL = sSQL & "    on Moeda_Tb.Moeda_ID                             = isnull(Proposta_Fechada_Tb.Seguro_Moeda_ID, Proposta_Adesao_Tb.Seguro_Moeda_ID)   " & vbNewLine

                Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                      glAmbiente_id, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      sSQL, _
                                                      lConexaoLocal, _
                                                      True)



            End If

            With rsRecordSet
                If Not .EOF And Not bSemEstimativa Then
                    'AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
                    .MoveFirst
                    CarregaDadosColecao 3, rsRecordSet
                    'AKIO.OKUNO - #RETIRADA - FIM - 09/05/2013
                    
                    .MoveLast

                    'Dados do Frame: Dados
                    btnSeq_Estimativa.Max = Val("0" & .Fields!CountEstimativa)
                    btnSeq_Estimativa.Value = .Fields!Seq_Estimativa
                    btnSeq_Estimativa.Min = 1

                    txtCountEstimativa = " de " & Val("0" & .Fields!CountEstimativa)
                    txtCountEstimativa.Tag = Val("0" & .Fields!CountEstimativa)

                    txtNossa_Parte = .Fields!Nossa_Parte & ""
                    txtSeq_Estimativa = .Fields!Seq_Estimativa

                    'Dados da Aba: Estimativas/Pagamentos
                    txtMoeda_Sigla = .Fields!Moeda_Sigla & ""
                    If Not IsNull(.Fields!Pgto_Parcial_Co_Seguro) Then
                        If UCase(.Fields!Pgto_Parcial_Co_Seguro) = "N" Then
                            chkPgto_Parcial_Co_Seguro.Value = 0
                            'mathayde
                            If sOperacaoCosseguro <> "C" Then
                                chkPgto_Parcial_Co_Seguro.Visible = False
                                'MU-2017-077791 - Ajuste Autom�tico das reservas segbr
                                If glAmbiente_id = 6 Or glAmbiente_id = 7 Or glAmbiente_id = 8 Then
                                    With lblValPrejuizoInformado
                                        .Top = 300
                                        .Left = 11580
                                        .Visible = True
                                        .Enabled = True
                                    End With
                                    With txtValPrejuizoInformado
                                        .Top = 510
                                        .Left = 11580
                                        .Visible = True
                                        .Enabled = True
                                    End With
                                End If
                            Else
                                With lblValPrejuizoInformado
                                        .Visible = False
                                        .Enabled = False
                                    End With
                                    With txtValPrejuizoInformado
                                        .Visible = False
                                        .Enabled = False
                                    End With
                            End If

                        Else
                            chkPgto_Parcial_Co_Seguro.Value = 1
                        End If
                    Else
                        chkPgto_Parcial_Co_Seguro.Value = 0
                    End If
                Else
                    txtCountEstimativa = " de 0"
                    txtDt_Inclusao_Estimativa.Text = Format(Now(), "dd/mm/yyyy")
                    txtMoeda_Sigla = .Fields!Moeda_Sigla & ""
                    txtNossa_Parte = "100,00"
                    txtUsuario = cUserName
                    txtPerc_Re_Seguro_Quota = "0.000"
                    txtPerc_Re_Seguro_Er = "0.000"
                    btnSeq_Estimativa.Min = 0
                    btnSeq_Estimativa.Max = 0
                End If
            End With

        End If
        If Val(txtSeq_Estimativa) <> 0 Then
            Set oEstimativa = cDados_Estimativa(1)
            For Each oEstimativa In cDados_Estimativa
                If oEstimativa.Seq_Estimativa = txtSeq_Estimativa Then
                    'Dados do Frame: Dados
                    If Not IsNull(oEstimativa.Dt_Inicio_Estimativa) Then
                        txtDt_Inicio_Estimativa = Format(oEstimativa.Dt_Inicio_Estimativa, "dd/mm/yyyy")
                    End If
                    If oEstimativa.Dt_Fim_Estimativa > "1900-01-01" Then
                        txtDt_Fim_Estimativa = Format(oEstimativa.Dt_Fim_Estimativa, "dd/mm/yyyy")
                    Else
                        txtDt_Fim_Estimativa = ""
                    End If
                    If Not IsNull(oEstimativa.Dt_Inclusao) Then
                        txtDt_Inclusao_Estimativa = Format(oEstimativa.Dt_Inclusao, "dd/mm/yyyy")
                    Else
                        txtDt_Inclusao_Estimativa = "__/__/____"
                    End If
                    txtUsuario = oEstimativa.Usuario & ""
                    txtPerc_Re_Seguro_Quota = Format("0" & oEstimativa.Perc_Re_Seguro_Quota, "#0.000")
                    txtPerc_Re_Seguro_Er = Format("0" & oEstimativa.Perc_Re_Seguro_Er, "#0.000")
                    txtNum_Carta_Seguro_Aceito = oEstimativa.Num_Carta_Seguro_Aceito
                    'mathayde
                    If (txtNum_Carta_Seguro_Aceito.Text = 0 Or txtNum_Carta_Seguro_Aceito.Text = "") And sOperacaoCosseguro <> "C" Then
                        txtNum_Carta_Seguro_Aceito.Visible = False
                        lblEstimativasPagamentos_Dados(9).Visible = False
                    End If
                    'MU-2017-077791 - Ajuste Autom�tico das reservas segbr
                    If sOperacaoCosseguro <> "C" Then
                        If glAmbiente_id = 6 Or glAmbiente_id = 7 Or glAmbiente_id = 8 Then
                            txtValPrejuizoInformado.Text = Format(oEstimativa.Val_Estimado, "#0.00")
                        End If
                    End If
                    

                End If
            Next

            Set oEstimativa = Nothing
        End If
        'Dados do Frame: Valores
        CarregaDadosGrid_AbaEstimativasPagamentos_Valores 2

        If Not bCarregou_AbaEstimativasPagamentos Then
            CarregaDadosGrid_AbaEstimativasPagamentos_Valores 1

'AKIO.OKUNO - MP - INICIO - 10/05/2013
'            'Dados do Frame: Voucher
'            CarregaDadosGrid_AbaEstimativasPagamentos_Voucher

'            'Dados do Frame: Benefici�rio
'            CarregaDadosGrid_AbaEstimativasPagamentos_Beneficiario
'AKIO.OKUNO - MP - FIM - 10/05/2013
        End If

        bCarregou_AbaEstimativasPagamentos = True

        MousePointer = vbNormal

        bAlterou_Seq_Estimativa = False

    End If
    
    Exit Sub
    'Resume
Erro:
    Call TrataErroGeral("CarregaDadosTela_AbaEstimativasPagamentos", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosGrid_AbaEstimativasPagamentos_Valores(bTipo As Byte)
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String
    Dim oEstimativa As clsEstimativa

    On Error GoTo Erro

    Select Case bTipo
    Case 1      'Grid 1
        LimpaGrid grdEstimativasPagamentos_Coberturas
        sSQL = ""
'AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
'        sSQL = sSQL & "Select * " & vbCrLf
'        sSQL = sSQL & "  From #EstimativaPagamento_CoberturaAfetada " & vbCrLf
'        sSQL = sSQL & " Order By Tp_Cobertura_Nome"
'        Set RsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                              glAmbiente_id, _
'                                              App.Title, _
'                                              App.FileDescription, _
'                                              sSQL, _
'                                              lConexaoLocal, _
'                                              True)
'
'        With RsRecordSet
'            Do While Not .EOF
'                sLinha = .Fields!Tp_Cobertura_Nome & vbTab
'                sLinha = sLinha & Format(.Fields!Estimado, "#,##0.00") & vbTab
'                sLinha = sLinha & Format(.Fields!Correcao, "#,##0.00") & ""
'                grdEstimativasPagamentos_Coberturas.AddItem sLinha
'                .MoveNext
'            Loop
'        End With

        Set rsRecordSet = SelecionaDados_AbaEstimativa_CoberturaAfetada_Local(lConexaoLocal)

        CarregaDadosGrid grdEstimativasPagamentos_Coberturas, rsRecordSet
        
        With grdEstimativasPagamentos_Coberturas
            .Cols = 5
            .ColWidth(3) = 0
            .ColWidth(4) = 0
        End With

'AKIO.OKUNO - #RETIRIDA - FIM - 09/05/2013

        InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Coberturas, 2

    Case 2      'Grid 2

        InicializaInterfaceLocal_Grid_AbaEstimativasPagamentos_Estimativas

        If cDados_Estimativa.Count <> 0 Then
            If LenB(Trim(txtSeq_Estimativa)) = 0 Then
                txtSeq_Estimativa = "1"
            End If
            For Each oEstimativa In cDados_Estimativa
                With oEstimativa
                    If .Seq_Estimativa = txtSeq_Estimativa Then
                        'MU-2017-077791 - Ajuste Autom�tico das reservas segbr (Victor Fonseca)
                        If sOperacaoCosseguro <> "C" Then
                            If glAmbiente_id = 6 Or glAmbiente_id = 7 Or glAmbiente_id = 8 Then
                                If .Seq_Estimativa = 1 Or .Seq_Estimativa = 0 Then
                                    grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 1) = Format(.Val_Custo_Medio, "#,##0.00")
                                Else
                                    grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 1) = Format(.Val_Estimado, "#,##0.00")
                                End If
                            Else
                                grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 1) = Format(.Val_Estimado, "#,##0.00")
                            End If
                        Else
                            grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 1) = Format(.Val_Estimado, "#,##0.00")
                        End If

                        grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 2) = Format(.Val_Pago, "#,##0.00")
                        grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 3) = Format(.Val_Estimado - .Val_Pago, "#,##0.00")
                        grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 4) = Format(.Val_Resseguro, "#,##0.00")
                        grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 5) = .Situacao
                    End If
                End With
            Next
        End If

'RETIRADO CONFORME SOLICITADO PELO USUARIO
'POIS NO SEGP0246 � POSS�VEL PAGAR MESMO COM DT_FIM_VIGENCIA.
'        If LenB(Trim(txtDt_Fim_Estimativa)) <> 0 Then
'            grdEstimativasPagamentos_Estimativas.ColWidth(6) = 0
'        Else
            If (strTipoProcesso <> strTipoProcesso_Avaliar And _
                strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem And _
                strTipoProcesso <> strTipoProcesso_Avaliar_Cosseguro) Or _
                lblSinistro_Situacao(0) = "ENCERRADO" Then
                grdEstimativasPagamentos_Estimativas.ColWidth(6) = 0
            Else
                grdEstimativasPagamentos_Estimativas.ColWidth(6) = 270
            End If
'        End IfS

        Set oEstimativa = Nothing

        InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Estimativas, 2
    End Select

    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosGrid_AbaEstimativasPagamentos_Valores", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub InicializaInterfaceLocal_Grid_AbaEstimativasPagamentos()
'Ajusta Largura das colunas que cont�m valores
    Dim bColunas As Byte
    Dim lLarguraPadrao As Long


    lLarguraPadrao = 1300
    'Grid de Coberturas
    With grdEstimativasPagamentos_Coberturas
        If .ColWidth(1) <> lLarguraPadrao Then
            For bColunas = 1 To 2
                .ColWidth(bColunas) = lLarguraPadrao
                .col = bColunas
            Next
        End If
    End With

    lLarguraPadrao = 1220
    'Grid de Itens de Estimativa
    With grdEstimativasPagamentos_Estimativas
        If .ColWidth(1) <> lLarguraPadrao Then
            For bColunas = 1 To 4
                .ColWidth(bColunas) = lLarguraPadrao
                .col = bColunas
            Next
        End If
    End With


    '  CONFITEC - (C00172137) - 16/10/2019 inicio
    lLarguraPadrao = 1950
    'Grid de Vouchers
    With grdEstimativasPagamentos_Voucher
        If .ColWidth(4) <> lLarguraPadrao Then
            .ColWidth(4) = lLarguraPadrao
        End If
    End With
    lLarguraPadrao = 1220
    '  CONFITEC - (C00172137) - 16/10/2019 fim
    

End Sub

Private Sub InicializaInterfaceLocal_Grid_AbaEstimativasPagamentos_Estimativas()
'Preenche com dados pr�-definidos a coluna 0 da Grid de Itens de Estimativas (da direita)
    Dim bLinhas As Byte
    Dim bColunas As Byte
    Dim sLinhas As String

    LimpaGrid grdEstimativasPagamentos_Estimativas

    With grdEstimativasPagamentos_Estimativas
        If .Rows < 9 Then
            .Rows = 9
        End If
        For bLinhas = 1 To 8
            Select Case bLinhas
            Case 1
                sLinha = "Indeniza��o"
            Case 2
                sLinha = "Honor�rios"
            Case 3
                sLinha = "Despesas"
            Case 4
                sLinha = "Ressarcimento"
            Case 5
                sLinha = "Desp. ressarcimento"
            Case 6
                sLinha = "Salvados"
            Case 7
                sLinha = "Desp. salvados"
            Case 8
                sLinha = "Indeniz. renda"
            End Select
            .TextMatrix(bLinhas, 0) = sLinha
            For bColunas = 1 To 4
                .TextMatrix(bLinhas, bColunas) = "0,00"
            Next
            .col = 6
            .Row = bLinhas
            '.CellBackColor = &HE0E0E0
            .CellBackColor = &H8000000F
            .CellForeColor = &H8000&
            .TextMatrix(.Row, .col) = "$"
        Next
        If (strTipoProcesso <> strTipoProcesso_Avaliar And _
            strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem And _
            strTipoProcesso <> strTipoProcesso_Avaliar_Cosseguro) Or _
            lblSinistro_Situacao(0) = "ENCERRADO" Then
            .ColWidth(6) = 0
        End If
    End With
End Sub

'AKIO.OKUNO - #RETIRADA - CarregaDadosGrid_AbaEstimativasPagamentos_Voucher - 10/05/2013
'PROCEDURE SUBSTITUIDA
'Private Sub CarregaDadosGrid_AbaEstimativasPagamentos_Voucher()
'    Dim rsRecordSet As ADODB.Recordset
'    Dim sSQL As String
'    Dim sLinha As String
'
'    On Error GoTo Erro
'
'    sSQL = ""
'''AKIO.OKUNO - #RETIRADA - INICIO - 02/05/2013
''    sSQL = sSQL & "Select * " & vbCrLf
''    sSQL = sSQL & "  From #EstimativaPagamento_Voucher"
'    sSQL = sSQL & "Select Pgto_Sinistro_Tb.Dt_Acerto_Contas_Sinistro --Dt_Acerto                                                                " & vbNewLine
'    sSQL = sSQL & "     , Ps_Acerto_Pagamento_Tb.Voucher_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Beneficiario_ID                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Nome                           = Sinistro_Benef_Tb.Nome                                                " & vbNewLine
'    sSQL = sSQL & "     , Tot_Acerto                                    = Sum(Pgto_Sinistro_Tb.Val_Acerto)                                      " & vbNewLine
'    sSQL = sSQL & "     , Tot_Correcao                                  = 0                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Ps_Remessa_Tb.Dt_Remessa                      -- Dt_Geracao                                                           " & vbNewLine
'    sSQL = sSQL & "     , Ps_Remessa_Tb.Remessa_ID                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Ps_Voucher_Tb.Estornado                                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Item_Val_Estimativa                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Forma_Pgto_Nome                               = Forma_Pgto_Tb.Nome                                                    " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Num_Nota_Fiscal                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Serie_Nota_Fiscal                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Banco                                                                                                " & vbNewLine    'RALVES - 28/09/2012 - Demanda 3747623 - Credito em conta - registro do hist�rico
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Agencia                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Agencia_Dv                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Conta_Corrente                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Conta_Corrente_DV                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.conta_poupanca                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.conta_poupanca_dv                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.variacao_poupanca                                                                                    " & vbNewLine    'RALVES - 28/09/2012 - Demanda 3747623 - Credito em conta - registro do hist�rico
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Pgto_Sinistro_Tb               Pgto_Sinistro_Tb  WITH (NOLOCK)                                                " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                   " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Pgto_Sinistro_Tb.Sinistro_ID                                          " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Ps_Acerto_Pagamento_Tb         Ps_Acerto_Pagamento_Tb  WITH (NOLOCK)                                          " & vbNewLine
'    sSQL = sSQL & " on Ps_Acerto_Pagamento_Tb.Acerto_ID                 = Pgto_Sinistro_Tb.Acerto_ID                                            " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Ps_Acerto_Tb                   Ps_Acerto_Tb  WITH (NOLOCK)                                                    " & vbNewLine
'    sSQL = sSQL & "    on Ps_Acerto_Tb.Acerto_ID                        = Ps_Acerto_Pagamento_Tb.Acerto_ID                                      " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Ps_Remessa_Voucher_Tb          Ps_Remessa_Voucher_Tb  WITH (NOLOCK)                                           " & vbNewLine
'    sSQL = sSQL & " on Ps_Remessa_Voucher_Tb.Voucher_ID                 = Ps_Acerto_Pagamento_Tb.Voucher_ID                                     " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Ps_Remessa_Tb                  Ps_Remessa_Tb  WITH (NOLOCK)                                                   " & vbNewLine
'    sSQL = sSQL & " on Ps_Remessa_Tb.Remessa_ID                         = Ps_Remessa_Voucher_Tb.Remessa_ID                                      " & vbNewLine
'    sSQL = sSQL & "   and Ps_Remessa_Tb.Cod_Sistema_PS                  = Ps_Remessa_Voucher_Tb.Cod_Sistema_PS                                  " & vbNewLine
'    sSQL = sSQL & "   and Ps_Remessa_Voucher_Tb.Cod_Sistema_PS          = 'AP'                                                                  " & vbNewLine
'    sSQL = sSQL & "   and Ps_Remessa_Voucher_Tb.Cod_Origem              = 'SN'                                                                  " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Ps_Voucher_Tb                  Ps_Voucher_Tb  WITH (NOLOCK)                                                   " & vbNewLine
'    sSQL = sSQL & " on Ps_Voucher_Tb.Voucher_ID                         = Ps_Remessa_Voucher_Tb.Voucher_ID                                      " & vbNewLine
'    sSQL = sSQL & "   and Ps_Voucher_Tb.Cod_Origem                      = Ps_Remessa_Voucher_Tb.Cod_Origem                                      " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Sinistro_Benef_Tb              Sinistro_Benef_Tb  WITH (NOLOCK)                                               " & vbNewLine
'    sSQL = sSQL & " on Sinistro_Benef_Tb.Sinistro_ID                    = Pgto_Sinistro_Tb.Sinistro_ID                                          " & vbNewLine
'    sSQL = sSQL & "   and Sinistro_Benef_Tb.Beneficiario_ID             = Pgto_Sinistro_Tb.Beneficiario_ID                                      " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Forma_Pgto_Tb                  Forma_Pgto_Tb  WITH (NOLOCK)                                                   " & vbNewLine
'    sSQL = sSQL & "    on Forma_Pgto_Tb.Forma_Pgto_ID                   = Pgto_Sinistro_Tb.Forma_Pgto_ID                                        " & vbNewLine
'    sSQL = sSQL & " Where Pgto_Sinistro_Tb.Tp_Operacao                  = 'd'                                                                   " & vbNewLine
'    sSQL = sSQL & " Group By Pgto_Sinistro_Tb.Dt_Acerto_Contas_Sinistro                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Ps_Acerto_Pagamento_Tb.Voucher_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Beneficiario_Id                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_benef_Tb.Nome                                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Ps_Remessa_Tb.Dt_Remessa                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Ps_Remessa_Tb.Remessa_ID                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Ps_Voucher_Tb.Estornado                                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_tb.Item_Val_Estimativa                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Forma_Pgto_Tb.Nome                                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Num_Nota_Fiscal                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Serie_Nota_Fiscal                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Banco                                                                                                " & vbNewLine    'RALVES - 28/09/2012 - Demanda 3747623 - Credito em conta - registro do hist�rico
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Agencia                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Agencia_Dv                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Conta_Corrente                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Conta_Corrente_DV                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.conta_poupanca                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.conta_poupanca_dv                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.variacao_poupanca                                                                                    " & vbNewLine    'RALVES - 28/09/2012 - Demanda 3747623 - Credito em conta - registro do hist�rico
''AKIO.OKUNO - #RETIRADA - FIM - 02/05/2013
'
'    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                          glAmbiente_id, _
'                                          App.Title, _
'                                          App.FileDescription, _
'                                          sSQL, _
'                                          lConexaoLocal, _
'                                          True)
'
'    grdEstimativasPagamentos_Voucher.Rows = 1
'
'    With rsRecordSet
'        Do While Not .EOF
'            sLinha = .Fields!Voucher_id & vbTab
'            If Not IsNull(.Fields!Dt_remessa) Then
'                sLinha = sLinha & Format(.Fields!Dt_remessa, "dd/mm/yyyy") & vbTab
'            Else
'                sLinha = sLinha & "" & vbTab
'            End If
'            sLinha = sLinha & .Fields!Remessa_ID & "" & vbTab
'            If Not IsNull(.Fields!Dt_Acerto_Contas_Sinistro) Then
'                sLinha = sLinha & Format(.Fields!Dt_Acerto_Contas_Sinistro, "dd/mm/yyyy") & "" & vbTab
'            Else
'                sLinha = sLinha & "" & vbTab
'            End If
'            sLinha = sLinha & Format(.Fields!Tot_Acerto + .Fields!Tot_Correcao, "#,##0.00") & vbTab
'            sLinha = sLinha & .Fields!Sinistro_Benef_Nome & "" & vbTab
'            Select Case UCase(.Fields!Estornado) & ""
'            Case "S"
'                sLinha = sLinha & "SIM"
'            Case "N"
'                sLinha = sLinha & "N�O"
'            End Select
'            sLinha = sLinha & vbTab
'            Select Case .Fields!Item_Val_Estimativa
'            Case 1: sLinha = sLinha & "INDENIZA��O"
'            Case 2: sLinha = sLinha & "HONOR�RIOS"
'            Case 3: sLinha = sLinha & "DESPESAS"
'            Case 4: sLinha = sLinha & "RESSARCIMENTO"
'            Case 5: sLinha = sLinha & "DESP. RESSARCIMENTO"
'            Case 6: sLinha = sLinha & "SALVADOS"
'            Case 7: sLinha = sLinha & "DESP. SALVADOS"
'            End Select
'            sLinha = sLinha & vbTab
'
'            sLinha = sLinha & .Fields!Forma_Pgto_Nome & vbTab
'            sLinha = sLinha & .Fields!Num_Nota_Fiscal & vbTab
'            sLinha = sLinha & .Fields!Serie_Nota_Fiscal & ""
'            'RALVES - 28/09/2012 - Demanda 3747623 - Credito em conta - registro do hist�rico
'            sLinha = sLinha & .Fields!Banco & vbTab
'            sLinha = sLinha & .Fields!Agencia & vbTab
'            sLinha = sLinha & .Fields!Agencia_dv & vbTab
'            sLinha = sLinha & .Fields!Conta_Corrente & vbTab
'            sLinha = sLinha & .Fields!Conta_Corrente_dv & vbTab
'            sLinha = sLinha & .Fields!Conta_Poupanca & vbTab
'            sLinha = sLinha & .Fields!Conta_Poupanca_dv & vbTab
'            sLinha = sLinha & .Fields!Variacao_Poupanca & ""
'            'RALVES - 28/09/2012 - Demanda 3747623 - Credito em conta - registro do hist�rico
'
'            grdEstimativasPagamentos_Voucher.AddItem sLinha
'            .MoveNext
'        Loop
'    End With
'
'    InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Voucher, 2
'
'    Set rsRecordSet = Nothing
'    Exit Sub
'
'Erro:
'    Call TrataErroGeral("CarregaDadosGrid_AbaEstimativasPagamentos_Voucher", Me.Caption)
'    Call FinalizarAplicacao
'
'End Sub



Private Sub CarregaDadosGrid_AbaEstimativasPagamentos_Voucher()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String
    Dim bAux As Boolean
    
    On Error GoTo Erro

    sSQL = ""
    sSQL = sSQL & "Exec seguros_db.Dbo.SEGS11117_SPS"

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)

    CarregaDadosGrid grdEstimativasPagamentos_Voucher, rsRecordSet

    InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Voucher, 2
    
    rsRecordSet.Close
    
    
    'PAULO PELEGRINI - 28/07/2014 - 18235323
    For i = 1 To grdEstimativasPagamentos_Voucher.Rows - 1
'+-------------------------------------------------
'| FLOW 18225198 - INICIO
'| Petrauskas Jan/2016
     
        grdEstimativasPagamentos_Voucher.TextMatrix(i, 5) = Format(grdEstimativasPagamentos_Voucher.TextMatrix(i, 5), "#,##0.00")
        grdEstimativasPagamentos_Voucher.TextMatrix(i, 6) = Format(grdEstimativasPagamentos_Voucher.TextMatrix(i, 6), "#,##0.00")
        '  CONFITEC - (C00172137) - 16/10/2019 inicio
        grdEstimativasPagamentos_Voucher.TextMatrix(i, 3) = Left(grdEstimativasPagamentos_Voucher.TextMatrix(i, 3), 10)
        grdEstimativasPagamentos_Voucher.TextMatrix(i, 4) = Left(grdEstimativasPagamentos_Voucher.TextMatrix(i, 4), 10)
        
        'grdEstimativasPagamentos_Voucher.TextMatrix(i, 33) = Format(grdEstimativasPagamentos_Voucher.TextMatrix(i, 33), "#,##0.00") 'Valor convertido    - Instancia 18777425 - RMarins 19/10/17
        'grdEstimativasPagamentos_Voucher.TextMatrix(i, 34) = grdEstimativasPagamentos_Voucher.TextMatrix(i, 34)  'Format(grdEstimativasPagamentos_Voucher.TextMatrix(i, 35), "#,##0.000000") 'Taxa Cambio - Instancia 18777425 - RMarins 19/10/17
        'grdEstimativasPagamentos_Voucher.TextMatrix(i, 35) = Left(grdEstimativasPagamentos_Voucher.TextMatrix(i, 35), 3) 'Moeda Pagto                    - Instancia 18777425 - RMarins 19/10/17
        
        ''Instancia 18777425 - RMarins 19/10/17
        'If grdEstimativasPagamentos_Voucher.TextMatrix(i, 35) = "220" Then
        '   grdEstimativasPagamentos_Voucher.TextMatrix(i, 35) = "D�lar"
        'Else
        '   grdEstimativasPagamentos_Voucher.TextMatrix(i, 35) = "Real"
        'End If
        ''Instancia 18777425 - RMarins 19/10/17
        
        If UCase(grdEstimativasPagamentos_Voucher.TextMatrix(i, 8)) <> "SIM" Then
            If IsDate(grdEstimativasPagamentos_Voucher.TextMatrix(i, 2)) And IsDate(grdEstimativasPagamentos_Voucher.TextMatrix(i, 4)) Then
              If CDate(grdEstimativasPagamentos_Voucher.TextMatrix(i, 4)) > CDate(grdEstimativasPagamentos_Voucher.TextMatrix(i, 2)) Then
        '  CONFITEC - (C00172137) - 16/10/2019 fim
                sSQL = "SELECT num_recibo " & vbCrLf _
                     & "FROM seguros_db.dbo.pgto_sinistro_tb WITH (NOLOCK) " & vbCrLf _
                     & "WHERE sinistro_id = " & gbldSinistro_ID & vbCrLf _
                     & "  AND num_recibo_ref = " & grdEstimativasPagamentos_Voucher.TextMatrix(i, 0) & vbCrLf _
                     & "  AND situacao_op IN ('a','n')"
                
                Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, lConexaoLocal, True)
                bAux = True
                If Not rsRecordSet.EOF Then
                  If Not IsNull(rsRecordSet!Num_recibo) Then
                    bAux = False
                  End If
                End If
                rsRecordSet.Close
                If bAux Then
                    grdEstimativasPagamentos_Voucher.Row = i
                    For g = 0 To grdEstimativasPagamentos_Voucher.Cols - 1
                      grdEstimativasPagamentos_Voucher.col = g
                      grdEstimativasPagamentos_Voucher.CellBackColor = vbYellow
                    Next g
                End If
              End If
            End If
      End If
'| FLOW 18225198 - FIM
'+-------------------------------------------------
    Next
    'FIM PAULO PELEGRINI - 28/07/2014 - 18235323

    Set rsRecordSet = Nothing

    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosGrid_AbaEstimativasPagamentos_Voucher", Me.Caption)
    Call FinalizarAplicacao

End Sub

'AKIO.OKUNO - MP - CarregaDadosGrid_AbaEstimativasPagamentos_Beneficiario - 09/05/2013
'PROCEDURE SUBSTITUIDA
'Private Sub CarregaDadosGrid_AbaEstimativasPagamentos_Beneficiario()
'    Dim rsRecordSet As ADODB.Recordset
'    Dim sSQL As String
'    Dim sLinha As String
'    Dim iSeq As Integer
'
'    On Error GoTo Erro
'
'    sSQL = ""
'    sSQL = sSQL & "Select * " & vbCrLf
'    sSQL = sSQL & "  From #EstimativaPagamento_Beneficiario"
'    '-->> Dados do Subframe: Benefici�rio                                                                                                       " & vbNewLine
'
'    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                          glAmbiente_id, _
'                                          App.Title, _
'                                          App.FileDescription, _
'                                          sSQL, _
'                                          lConexaoLocal, True)
'
'    grdEstimativasPagamentos_Beneficiario.Rows = 1
'    'mathayde
'    'btnEstimativasPagamentos_Valores_Beneficiario_Alterar.Enabled = False  'AKIO.OKUNO - 26/09/2012
'
'    iSeq = 0
'
'    With rsRecordSet
'        Do While Not .EOF
'            iSeq = iSeq + 1
'            sLinha = iSeq & vbTab
'            sLinha = sLinha & .Fields!Nome & vbTab
'            If IsNull(.Fields!CGC) Then
'                If Not IsNull(.Fields!CPF) Then
'                    sLinha = sLinha & Format(.Fields!CPF, "&&&.&&&.&&&-&&")
'                End If
'            Else
'                sLinha = sLinha & Format(.Fields!CGC, "&&.&&&.&&&/&&&&-&&")
'            End If
'            sLinha = sLinha & vbTab
'            sLinha = sLinha & .Fields!Email & "" & vbTab
'            sLinha = sLinha & Format("0" & .Fields!Banco, "000") & vbTab
'            sLinha = sLinha & Format("0" & .Fields!Agencia, "0000") & IIf(IsNull(.Fields!Agencia_dv), "", "-" & .Fields!Agencia_dv) & vbTab
'            sLinha = sLinha & Format("0" & .Fields!Conta_Corrente, "0000000000") & IIf(IsNull(.Fields!Conta_Corrente_dv), "", "-" & .Fields!Conta_Corrente_dv) & vbTab
'            sLinha = sLinha & IIf(IsNull(.Fields!Tp_Documento_ID) Or .Fields!Tp_Documento_ID = 99, "", .Fields!Tp_Documento_Nome) & vbTab
'            sLinha = sLinha & .Fields!numero & vbTab
'            sLinha = sLinha & .Fields!Orgao & vbTab
'            sLinha = sLinha & .Fields!Serie & vbTab
'            sLinha = sLinha & .Fields!Complemento & vbTab
'            sLinha = sLinha & .Fields!Sexo_Descricao & vbTab
'            If Not IsNull(.Fields!Tp_Responsavel) Then
'                sLinha = sLinha & IIf(.Fields!Tp_Responsavel = "1", "Assistido", IIf(.Fields!Tp_Responsavel = "2", "Curador", ""))
'            Else
'                sLinha = sLinha & " "
'            End If
'            sLinha = sLinha & vbTab
'            sLinha = sLinha & .Fields!Nome_Responsavel & vbTab
'            If Not IsNull(.Fields!Dt_Nascimento) Then
'                sLinha = sLinha & Format(.Fields!Dt_Nascimento, "dd/mm/yyyy") & ""
'            End If
'
'            'sLinha = sLinha & .Fields!beneficiario_id 'cristovao.rodrigues 20/08/2012
'            sLinha = sLinha & vbTab
'            If Not IsNull(.Fields!Id_Beneficiario_BB) Then
'                sLinha = sLinha & IIf(UCase(.Fields!Id_Beneficiario_BB) = "S", "Sim", "N�o") & vbTab
'            Else
'                sLinha = sLinha & " " & vbTab
'            End If
'
'            sLinha = sLinha & .Fields!beneficiario_id    'cristovao.rodrigues 20/08/2012
'
'            grdEstimativasPagamentos_Beneficiario.AddItem sLinha
'            .MoveNext
'        Loop
'    End With
'
'    'mathayde
'    '    If grdEstimativasPagamentos_Beneficiario.Rows > 1 Or strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then 'RALVES
'    '    If grdEstimativasPagamentos_Beneficiario.Rows <= 1 And (strTipoProcesso = strTipoProcesso_Consulta Or strTipoProcesso = strTipoProcesso_Consulta_Cosseguro
'    '       Or strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro) Then 'AKIO.OKUNO - 30/09/2012
'    'RETIRADO QUANDO AVISAR E AVISAR_COSSEGURO, POIS NO AVISO � POSS�VEL CADASTRAR BENEFICIARIO
'    If grdEstimativasPagamentos_Beneficiario.Rows <= 1 And (strTipoProcesso = strTipoProcesso_Consulta Or strTipoProcesso = strTipoProcesso_Consulta_Cosseguro) Then
'        '        btnEstimativasPagamentos_Valores_Beneficiario_Alterar.Enabled = True   'AKIO.OKUNO 26/09/2012
'        btnEstimativasPagamentos_Valores_Beneficiario_Alterar.Enabled = False
'    End If
'
'    InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Beneficiario, 2
'
'    Set rsRecordSet = Nothing
'    Exit Sub
'
'Erro:
'    Call TrataErroGeral("CarregaDadosGrid_AbaEstimativasPagamentos_Beneficiario", Me.Caption)
'    Call FinalizarAplicacao
'
'End Sub

'AKIO.OKUNO - MP - CarregaDadosGrid_AbaEstimativasPagamentos_Beneficiario - 09/05/2013
Private Sub CarregaDadosGrid_AbaEstimativasPagamentos_Beneficiario()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String
    Dim iSeq As Integer

    On Error GoTo Erro

    sSQL = ""
    '-->> Dados do Subframe: Benefici�rio                                                                                                       " & vbNewLine
    sSQL = sSQL & "Exec seguros_db.Dbo.SEGS11116_SPS" & vbNewLine
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, True)

    grdEstimativasPagamentos_Beneficiario.Rows = 1

    iSeq = 0
    
    CarregaDadosGrid grdEstimativasPagamentos_Beneficiario, rsRecordSet
    With grdEstimativasPagamentos_Beneficiario
         For iSeq = 1 To .Rows - 1
            .TextMatrix(iSeq, 0) = iSeq
         Next
    End With

    'mathayde
    '    If grdEstimativasPagamentos_Beneficiario.Rows > 1 Or strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then 'RALVES
    '    If grdEstimativasPagamentos_Beneficiario.Rows <= 1 And (strTipoProcesso = strTipoProcesso_Consulta Or strTipoProcesso = strTipoProcesso_Consulta_Cosseguro
    '       Or strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro) Then 'AKIO.OKUNO - 30/09/2012
    'RETIRADO QUANDO AVISAR E AVISAR_COSSEGURO, POIS NO AVISO � POSS�VEL CADASTRAR BENEFICIARIO
    If grdEstimativasPagamentos_Beneficiario.Rows <= 1 And (strTipoProcesso = strTipoProcesso_Consulta Or strTipoProcesso = strTipoProcesso_Consulta_Cosseguro) Then
        '        btnEstimativasPagamentos_Valores_Beneficiario_Alterar.Enabled = True   'AKIO.OKUNO 26/09/2012
        btnEstimativasPagamentos_Valores_Beneficiario_Alterar.Enabled = False
    End If
    
'    'Renato Silva - PMBC
'    If gbllProduto_ID = 1217 Then
'        cmdPMBC.Enabled = True
'        cmdPMBC.Visible = True
'    End If

    InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Beneficiario, 2

    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosGrid_AbaEstimativasPagamentos_Beneficiario", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosTela_AbaDadosEspecificos()
    On Error GoTo Erro

    If Not bCarregou_AbaDadosEspecificos Then

        MousePointer = vbHourglass

        InicializaModoProcessamentoObjeto grdDadosEspecificos_AcaoJudicial, 1
        InicializaModoProcessamentoObjeto grdDadosEspecificos_Prestador, 1
        InicializaModoProcessamentoObjeto grdDadosEspecificos_Regulador, 1
        InicializaModoProcessamentoObjeto grdDadosEspecificos_BensSinistrados, 1    'cristovao.rodrigues 15/08/2012
        'InicializaModoProcessamentoObjeto grdDadosEspecificos_ParecerMedico, 1

        SelecionaDados 4

        'Dados do Frame: A��o Judicial
        CarregaDadosGrid_AbaDadosEspecificos_AcaoJudicial

        'Dados do Frame: Prestadores
        CarregaDadosGrid_AbaDadosEspecificos_Prestador

        'Dados do Frame: Reguladores
        CarregaDadosGrid_AbaDadosEspecificos_Regulador
        
        CarregaDadosGrid_AbaDadosEspecificos_ParecerMedico
        
        If grdDadosEspecificos_BensSinistrados.Visible Then         'AKIO.OKUNO - 09/05/2013
            'Dados do Frame: Bens Sinistrados
            CarregaDadosGrid_AbaDadosEspecificos_BensSinistrados
        End If                                                      'AKIO.OKUNO - 09/05/2013
        
        bCarregou_AbaDadosEspecificos = True

        MousePointer = vbNormal

    End If
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosTela_AbaDadosEspecificos", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosTela_AbaCausasEEventos()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String

    On Error GoTo Erro

    If Not bCarregou_AbaCausasEEventos Then

        MousePointer = vbHourglass

        InicializaModoProcessamentoObjeto grdCausasEEventos_CausaSinistro, 1

'        SelecionaDados 5       'AKIO.OKUNO - #RETIRADA - 07/05/2013

        'Dados do Frame: Causas e Eventos
        CarregaDadosGrid_AbaCausasEEventos_CausaSinistro   'AKIO.OKUNO - #RETIRADA - 09/05/2013

        sSQL = ""
'AKIO.OKUNO - #RETIRADA - INICIO - 07/05/2013
'        sSQL = sSQL & "Select * " & vbCrLf
'        sSQL = sSQL & "  From #CausaEvento_Observacao"
        sSQL = sSQL & "Exec seguros_db.Dbo.SEGS11110_SPS"
'AKIO.OKUNO - #RETIRADA - FIM - 07/05/2013

        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, _
                                              True)
        With rsRecordSet
'AKIO.OKUNO - #RETIRADA - INICIO - 07/05/2013
            If Not .EOF Then
'                If .Fields!Item_Descricao = 1 Then
                    txtObservacao1 = .Fields!Observacao1 & ""
'                Else
                    txtObservacao2 = .Fields!Observacao2 & ""
'                End If
            End If
'AKIO.OKUNO - #RETIRADA - FIM - 07/05/2013
        End With

        Set rsRecordSet = Nothing

        bCarregou_AbaCausasEEventos = True

        MousePointer = vbNormal

    End If
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosTela_AbaCausasEEventos", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosGrid_AbaCausasEEventos_CausaSinistro()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String

    On Error GoTo Erro

    sSQL = ""
'AKIO.OKUNO - #RETIRADA - INICIO - 07/05/2013
'    sSQL = sSQL & "Select * " & vbCrLf
'    sSQL = sSQL & "  From #CausaEvento"
    sSQL = sSQL & "Exec seguros_db.Dbo.SEGS11122_SPS"
'AKIO.OKUNO - #RETIRADA - FIM - 07/05/2013
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, True)
    
'AKIO.OKUNO - #RETIRADA - INICIO - 07/05/2013
    CarregaDadosGrid grdCausasEEventos_CausaSinistro, rsRecordSet
'    With rsRecordSet
'        Do While Not .EOF
'
'            sLinha = .Fields!Seq_Causa & vbTab
'            sLinha = sLinha & .Fields!Causa_Sinistro_ID & vbTab
'            sLinha = sLinha & .Fields!Item_Descricao & vbTab
'            sLinha = sLinha & .Fields!Descricao & vbTab
'
'            grdCausasEEventos_CausaSinistro.AddItem sLinha
'
'            .MoveNext
'
'        Loop
'    End With
'AKIO.OKUNO - #RETIRADA - FIM - 07/05/2013


    InicializaModoProcessamentoObjeto grdCausasEEventos_CausaSinistro, 2

    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosGrid_AbaCausasEEventos_CausaSinistro", Me.Caption)
    Call FinalizarAplicacao

End Sub


Private Sub InicializaModoProcessamentoObjeto(objObjeto As Object, bTipo As Byte)
    DoEvents

    With objObjeto
        Select Case bTipo
        Case 1              'Vai carregar
            If TypeOf objObjeto Is MSFlexGrid Then
                .BackColorFixed = &HC0C0C0
                .ForeColorFixed = &H808080
            Else
                .BackColor = &HC0C0C0
            End If
        Case 2              'Carregado
            If TypeOf objObjeto Is MSFlexGrid Then
                .BackColorFixed = &H8000000F
                .ForeColorFixed = &H80000012
            Else
                .BackColor = -2147483643
            End If
        End Select
    End With

End Sub


Private Sub InicializaToolTipTextLocal()
    On Error GoTo Erro

    InicializaToolTipText Me

    Exit Sub

Erro:
    Call TrataErroGeral("InicializaToolTipTextLocal", Me.Caption)
    Call FinalizarAplicacao


End Sub


Private Sub CarregaDadosGrid_AbaDadosEspecificos_AcaoJudicial()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String
    Dim iLinha          As Integer      'AKIO.OKUNO - 02/05/2013

    On Error GoTo Erro
    'AKIO.OKUNO - #RETIRADA - INICIO - 02/05/2013
'    'AKIO.OKUNO - INICIO - 11/10/2012
'    sSQL = ""
'    sSQL = sSQL & "Select Passivel_Ressarcimento " & vbCrLf
'    sSQL = sSQL & "  From #Sinistro_Principal"
'
'    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                          glAmbiente_id, _
'                                          App.Title, _
'                                          App.FileDescription, _
'                                          sSQL, _
'                                          lConexaoLocal, True)
'
'    With rsRecordSet
'        If Not .EOF Then
'            sPassivel_Ressarcimento = Seleciona_PassivelRessarcimento(.Fields!Passivel_Ressarcimento & "")
'        Else
'            sPassivel_Ressarcimento = "N�o informado"
'        End If
'    End With
'    'AKIO.OKUNO - FIM - 11/10/2012
'
'    sSQL = ""
'    sSQL = sSQL & "Select * " & vbCrLf
'    sSQL = sSQL & "  From #DadosEspecificos_AcaoJudicial"
    sSQL = sSQL & "Exec seguros_db.Dbo.SEGS11118_SPS"

    'AKIO.OKUNO - #RETIRADA - FIM - 02/05/2013

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, True)
    With rsRecordSet
    
        'cristovao.rodrigues 20/11/2012
        If .EOF Then
            sLinha = sLinha & "" & vbTab & "" & vbTab
            sLinha = sLinha & "" & vbTab & "" & vbTab
            sLinha = sLinha & sPassivel_Ressarcimento
            grdDadosEspecificos_AcaoJudicial.AddItem sLinha
'        End If
'        Do While Not .EOF
'            If Not IsNull(.Fields!Dt_Acionamento) Then
'                sLinha = Format(.Fields!Dt_Acionamento, "dd/mm/yyyy") & vbTab
'            Else
'                sLinha = sLinha & vbTab
'            End If
'            If Not IsNull(.Fields!Dt_Julgamento) Then
'                sLinha = sLinha & Format(.Fields!Dt_Julgamento, "dd/mm/yyyy") & vbTab
'            Else
'                sLinha = sLinha & vbTab
'            End If
'            sLinha = sLinha & .Fields!Resultado & vbTab
'            sLinha = sLinha & .Fields!Motivo & ""
'            sLinha = sLinha & vbTab                             'AKIO.OKUNO - 02/10/2012
'            sLinha = sLinha & sPassivel_Ressarcimento           'AKIO.OKUNO - 02/10/2012
'            grdDadosEspecificos_AcaoJudicial.AddItem sLinha
'            .MoveNext
'
'        Loop
        Else
            CarregaDadosGrid grdDadosEspecificos_AcaoJudicial, rsRecordSet
            For i = 1 To grdDadosEspecificos_AcaoJudicial.Rows - 1
                grdDadosEspecificos_AcaoJudicial.TextMatrix(i, 4) = sPassivel_Ressarcimento
            Next
        End If
'AKIO.OKUNO - #RETIRADA - FIM - 02/05/2013
    End With

    Set rsRecordSet = Nothing

    InicializaModoProcessamentoObjeto grdDadosEspecificos_AcaoJudicial, 2

    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosGrid_AbaDadosEspecificos_AcaoJudicial", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosGrid_AbaDadosEspecificos_Prestador()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String

    On Error GoTo Erro

    sSQL = ""
'AKIO.OKUNO - #RETIRADA - INICIO - 02/05/2013
'    sSQL = sSQL & "Select * " & vbCrLf
'    sSQL = sSQL & "  From #DadosEspecificos_Prestador"
    '-->> Dados do Frame: Prestadores
'    sSQL = sSQL & "Select Prestador_Tb.Nome                                                                                             " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Banco                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Agencia                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Agencia_Dv                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Conta                                         = Prestador_Tb.Conta_Corrente                                   " & vbNewLine
'    sSQL = sSQL & "     , Conta_Dv                                          = Prestador_Tb.Conta_Corrente_Dv                            " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Estado                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Municipio                                     = Municipio_Tb.Nome                                             " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Prestador_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Prestador_Tb.Usuario                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Prestador_PJ_Tb.CGC                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Prestador_PF_Tb.CPF                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Endereco                                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Bairro                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.CEP                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , DDD                                               = Prestador_Tb.DDD_1                                        " & vbNewLine
'    sSQL = sSQL & "     , Telefone                                          = Prestador_Tb.Telefone_1                                   " & vbNewLine
'    sSQL = sSQL & "     , Prestador_Tb.Variacao_Poupanca                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Tipo                                              = 'E'                                                       " & vbNewLine
    sSQL = sSQL & "Exec seguros_db.Dbo.SEGS11119_SPS"
'AKIO.OKUNO - #RETIRADA - FIM - 02/05/2013
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, True)
'AKIO.OKUNO - #RETIRADA - INICIO - 02/05/2013
'    With rsRecordSet
'        Do While Not .EOF
'            sLinha = .Fields!Nome & vbTab
'            sLinha = sLinha & .Fields!Municipio & vbTab
'            sLinha = sLinha & .Fields!Estado & vbTab
'            sLinha = sLinha & .Fields!Endereco & vbTab
'            sLinha = sLinha & .Fields!Bairro & vbTab
'            sLinha = sLinha & IIf(IsNull(.Fields!Cep), "", MasCEP(.Fields!Cep)) & vbTab
'            sLinha = sLinha & .Fields!DDD & vbTab
'            sLinha = sLinha & .Fields!Telefone & vbTab
'            sLinha = sLinha & .Fields!Usuario & vbTab
'            sLinha = sLinha & .Fields!Prestador_ID & ""
'            grdDadosEspecificos_Prestador.AddItem sLinha
'            .MoveNext
'        Loop
'    End With
    If Not rsRecordSet.EOF Then
        CarregaDadosGrid grdDadosEspecificos_Prestador, rsRecordSet
    End If
'AKIO.OKUNO - #RETIRADA - FIM - 02/05/2013
    'Ocultando campo com Prestador_ID
    grdDadosEspecificos_Prestador.ColWidth(9) = 0

    InicializaModoProcessamentoObjeto grdDadosEspecificos_Prestador, 2

    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosGrid_AbaDadosEspecificos_Prestador", Me.Caption)
    Call FinalizarAplicacao

End Sub
'cristovao.rodrigues 15/08/2012
Private Sub CarregaDadosGrid_AbaDadosEspecificos_BensSinistrados()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String

    On Error GoTo Erro

    LimpaGrid grdDadosEspecificos_BensSinistrados


    '    sSQL = sSQL & "Select * " & vbCrLf
    '    sSQL = sSQL & "  From #EstimativaPagamento_CoberturaAfetada " & vbCrLf
    '    sSQL = sSQL & " Order By Tp_Cobertura_Nome"

    sSQL = ""
'    sSQL = sSQL & " select sinistro_cobertura_tb.sinistro_id,sinistro_desc_objeto_tb.tp_cobertura_id, tp_cobertura_tb.nome NomeCobertura, " & vbCrLf
'    sSQL = sSQL & "     sinistro_desc_objeto_tb.tp_objeto_id, tp_objeto_tb.nome NomeObjeto, " & vbCrLf
'    sSQL = sSQL & "     isnull(sinistro_desc_objeto_tb.val_prejuizo,0) as val_prejuizo , " & vbCrLf
'    sSQL = sSQL & "     isnull(sinistro_desc_objeto_tb.cod_objeto_sinistrado,0) as cod_objeto_sinistrado, " & vbCrLf
'    sSQL = sSQL & "     sinistro_desc_objeto_tb.descricao " & vbCrLf
'    sSQL = sSQL & " from sinistro_desc_objeto_tb  WITH (NOLOCK)  " & vbCrLf
'    sSQL = sSQL & " inner join sinistro_cobertura_tb  WITH (NOLOCK)  " & vbCrLf
'    sSQL = sSQL & "     on sinistro_desc_objeto_tb.sinistro_id = sinistro_cobertura_tb.sinistro_id   " & vbCrLf
'    sSQL = sSQL & "     and sinistro_desc_objeto_tb.tp_cobertura_id = sinistro_cobertura_tb.tp_cobertura_id   " & vbCrLf
'    sSQL = sSQL & "     and sinistro_desc_objeto_tb.dt_inicio_vigencia = sinistro_cobertura_tb.dt_inicio_vigencia   " & vbCrLf
'    sSQL = sSQL & " inner join tp_objeto_tb  WITH (NOLOCK)  " & vbCrLf
'    sSQL = sSQL & "     on sinistro_desc_objeto_tb.tp_objeto_id = tp_objeto_tb.tp_objeto_id   " & vbCrLf
'    sSQL = sSQL & " inner join tp_cobertura_tb  WITH (NOLOCK)  " & vbCrLf
'    sSQL = sSQL & "     on  sinistro_desc_objeto_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id   " & vbCrLf
'    sSQL = sSQL & " where " & vbCrLf
'    sSQL = sSQL & "     sinistro_cobertura_tb.sinistro_id = " & lblSinistro_ID(0).Caption & vbCrLf    '-- 11200203508 --
'    sSQL = sSQL & "     and sinistro_cobertura_tb.dt_fim_vigencia is null " & vbCrLf
    sSQL = sSQL & "Exec seguros_db.Dbo.SEGS11121_SPS"

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)

    'Cobertura Afetada | Verba | Descri��o| Valor Preju�zo
    'Tp_Cobertura_ID , Correcao , Estimado  , Tp_Cobertura_Nome , Tipo


    With rsRecordSet
        Do While Not .EOF

            sLinha = ""
            sLinha = sLinha & UCase(.Fields!NomeCobertura) & vbTab

            If IsNull(.Fields!NomeObjeto) Then
                'sLinha = sLinha & IIf(IsNull(.Fields!NomeObjeto), "", .Fields!NomeObjeto) & vbTab
                sLinha = sLinha & "" & vbTab
            Else
                If .Fields!tp_objeto_id = 0 Then
                    sLinha = sLinha & "" & vbTab
                Else
                    sLinha = sLinha & UCase(.Fields!NomeObjeto) & vbTab
                End If
            End If

            sLinha = sLinha & .Fields!Descricao & vbTab
            sLinha = sLinha & Format(IIf(.Fields!Val_prejuizo = "", 0, Val(.Fields!Val_prejuizo)), "#,##0.00") & vbTab
            sLinha = sLinha & .Fields!tp_Cobertura_id & vbTab

            grdDadosEspecificos_BensSinistrados.AddItem sLinha
            .MoveNext
        Loop
    End With


    InicializaModoProcessamentoObjeto grdDadosEspecificos_BensSinistrados, 2

    'Ocultando campo Cobertura_ID
    grdDadosEspecificos_BensSinistrados.ColWidth(4) = 0


    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosGrid_AbaDadosEspecificos_BensSinistrados", Me.Caption)
    Call FinalizarAplicacao

End Sub



Private Sub CarregaDadosGrid_AbaDadosEspecificos_Regulador()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String

    On Error GoTo Erro

    sSQL = ""
'AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
'    sSQL = sSQL & "Select * " & vbCrLf
'    sSQL = sSQL & "  From #DadosEspecificos_Regulacao"
'    sSQL = sSQL & "Select Vistoria_Tb.Dt_Pedido_Vistoria                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Regulador_ID                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Parecer_Preliminar                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Resultado_Preliminar                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Dt_Fim_Preliminar                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Parecer_Vistoria                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Resultado_Vistoria                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Dt_Parecer_Vistoria                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Usuario                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Nome = Regulador_Tb.Nome                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Banco                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Agencia                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Agencia_Dv                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Conta_Corrente                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Conta_Corrente_Dv                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Regulador_PJ_Tb.CGC                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Regulador_PF_Tb.CPF                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Endereco                                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Bairro                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Municipio_Nome = Municipio_Tb.Nome                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Estado                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.CEP                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.DDD_1                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Telefone_1                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Qld_Tecnica_Preliminar                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Cump_Procedimentos_Preliminar                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Qld_Tecnica_Vistoria                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Cump_Procedimentos_Vistoria                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Variacao_Poupanca                                                                                " & vbNewLine
'    sSQL = sSQL & "Select Regulador_Nome = Regulador_Tb.Nome                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Dt_Pedido_Vistoria                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Dt_Fim_Preliminar                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Resultado_Preliminar                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Parecer_Preliminar                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Dt_Parecer_Vistoria                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Resultado_Vistoria                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Parecer_Vistoria                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Endereco                                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Regulador_Tb.Bairro                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Municipio_Nome = Municipio_Tb.Nome                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Usuario                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Vistoria_Tb.Regulador_ID                                                                                      " & vbNewLine
''    sSQL = sSQL & "     , Regulador_Tb.Banco                                                                                            " & vbNewLine
''    sSQL = sSQL & "     , Regulador_Tb.Agencia                                                                                          " & vbNewLine
''    sSQL = sSQL & "     , Regulador_Tb.Agencia_Dv                                                                                       " & vbNewLine
''    sSQL = sSQL & "     , Regulador_Tb.Conta_Corrente                                                                                   " & vbNewLine
''    sSQL = sSQL & "     , Regulador_Tb.Conta_Corrente_Dv                                                                                " & vbNewLine
''    sSQL = sSQL & "     , Regulador_PJ_Tb.CGC                                                                                           " & vbNewLine
''    sSQL = sSQL & "     , Regulador_PF_Tb.CPF                                                                                           " & vbNewLine
''    sSQL = sSQL & "     , Regulador_Tb.Estado                                                                                           " & vbNewLine
''    sSQL = sSQL & "     , Regulador_Tb.CEP                                                                                              " & vbNewLine
''    sSQL = sSQL & "     , Regulador_Tb.DDD_1                                                                                            " & vbNewLine
''    sSQL = sSQL & "     , Regulador_Tb.Telefone_1                                                                                       " & vbNewLine
''    sSQL = sSQL & "     , Vistoria_Tb.Qld_Tecnica_Preliminar                                                                            " & vbNewLine
''    sSQL = sSQL & "     , Vistoria_Tb.Cump_Procedimentos_Preliminar                                                                     " & vbNewLine
''    sSQL = sSQL & "     , Vistoria_Tb.Qld_Tecnica_Vistoria                                                                              " & vbNewLine
''    sSQL = sSQL & "     , Vistoria_Tb.Cump_Procedimentos_Vistoria                                                                       " & vbNewLine
''    sSQL = sSQL & "     , Regulador_Tb.Variacao_Poupanca                                                                                " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Vistoria_Tb                    Vistoria_Tb  WITH (NOLOCK)                                             " & vbNewLine
'    sSQL = sSQL & " Inner Join Seguros_Db.Dbo.Regulador_Tb              Regulador_Tb  WITH (NOLOCK)                                            " & vbNewLine
'    sSQL = sSQL & "    on Vistoria_Tb.Regulador_ID                      = Regulador_Tb.Regulador_ID                                     " & vbNewLine
'    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Regulador_PJ_Tb           Regulador_PJ_Tb  WITH (NOLOCK)                                         " & vbNewLine
'    sSQL = sSQL & "    on Regulador_PJ_Tb.Regulador_ID                  = Regulador_Tb.Regulador_ID                                     " & vbNewLine
'    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Regulador_PF_Tb           Regulador_PF_Tb  WITH (NOLOCK)                                         " & vbNewLine
'    sSQL = sSQL & "    on Regulador_PF_Tb.Regulador_ID                  = Regulador_Tb.Regulador_ID                                     " & vbNewLine
'    sSQL = sSQL & " Inner Join Seguros_Db.Dbo.Municipio_Tb              Municipio_Tb  WITH (NOLOCK)                                            " & vbNewLine
'    sSQL = sSQL & "    on Municipio_Tb.Municipio_ID                     = Regulador_Tb.Municipio_ID                                     " & vbNewLine
'    sSQL = sSQL & "   and Municipio_Tb.Estado                           = Regulador_Tb.Estado                                           " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                           " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Vistoria_Tb.Sinistro_ID                                       " & vbNewLine
'    sSQL = sSQL & " Order By Regulador_Tb.Regulador_ID                                                                                  " & vbNewLine
    sSQL = sSQL & "Exec seguros_db.Dbo.SEGS11120_SPS"
'AKIO.OKUNO - #RETIRADA - FIM - 09/05/2013

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, True)
'AKIO.OKUNO - #RETIRADA - INICIO - 02/05/2013
'    With rsRecordSet
'        Do While Not .EOF
'            sLinha = .Fields!Regulador_Nome & vbTab
'            If Not IsNull(.Fields!Dt_Pedido_Vistoria) Then
'                sLinha = sLinha & Format(.Fields!Dt_Pedido_Vistoria, "dd/mm/yyyy") & vbTab
'            Else
'                sLinha = sLinha & vbTab
'            End If
'            If Not IsNull(.Fields!Dt_Fim_Preliminar) Then
'                sLinha = sLinha & Format(.Fields!Dt_Fim_Preliminar, "dd/mm/yyyy") & vbTab
'            Else
'                sLinha = sLinha & vbTab
'            End If
'            sLinha = sLinha & .Fields!Resultado_Preliminar & vbTab
'            sLinha = sLinha & .Fields!Parecer_Preliminar & vbTab
'            If Not IsNull(.Fields!Dt_Parecer_Vistoria) Then
'                sLinha = sLinha & Format(.Fields!Dt_Parecer_Vistoria, "dd/mm/yyyy") & vbTab
'            Else
'                sLinha = sLinha & vbTab
'            End If
'            sLinha = sLinha & .Fields!Resultado_Vistoria & vbTab
'            sLinha = sLinha & .Fields!Parecer_Vistoria & vbTab
'            sLinha = sLinha & .Fields!Endereco & vbTab
'            sLinha = sLinha & .Fields!Bairro & vbTab
'            sLinha = sLinha & .Fields!Municipio_Nome & vbTab
'            sLinha = sLinha & .Fields!Usuario & vbTab
'            sLinha = sLinha & .Fields!Regulador_ID & ""
'
'            grdDadosEspecificos_Regulador.AddItem sLinha
'            .MoveNext
'        Loop
'    End With
    If Not rsRecordSet.EOF Then
        CarregaDadosGrid grdDadosEspecificos_Regulador, rsRecordSet
    End If
'AKIO.OKUNO - #RETIRADA - FIM - 02/5/2013
    InicializaModoProcessamentoObjeto grdDadosEspecificos_Regulador, 2

    'Ocultando campo Regulador_ID
    grdDadosEspecificos_Regulador.ColWidth(12) = 0

    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosGrid_AbaDadosEspecificos_Regulador", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosGrid_AbaDadosEspecificos_ParecerMedico()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String
    Dim tp_Tecnico_Assessoria As String
    Dim tp_tecnico_Exclusao As Integer
    
    

    On Error GoTo Erro
    
    VerificaPermissaoTecnico


    sSQL = sSQL & "Exec seguros_db.dbo.SEGS12601_SPS"
    
    
     Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, True)
                                          
     
                                          
                                          
       If Not rsRecordSet.EOF Then
       
       btnDadosEspecificos_Parecer_Consultar.Enabled = True
    
       grdDadosEspecificos_ParecerMedico.Rows = rsRecordSet.RecordCount + 1
       grdDadosEspecificos_ParecerMedico.Cols = rsRecordSet.Fields.Count
    
      
       For i = 0 To rsRecordSet.Fields.Count - 1
           grdDadosEspecificos_ParecerMedico.TextMatrix(0, i) = rsRecordSet.Fields(i).name
          Next
    
       i = 1
       
       Do While Not rsRecordSet.EOF
    
           For j = 0 To rsRecordSet.Fields.Count - 1
               If Not IsNull(rsRecordSet.Fields(j).Value) Then
                   grdDadosEspecificos_ParecerMedico.TextMatrix(i, j) = rsRecordSet.Fields(j).Value
               End If
           Next
    
       i = i + 1
       rsRecordSet.MoveNext
       Loop
       
         grdDadosEspecificos_ParecerMedico.ColWidth(4) = 0
         
         If (Trim(gsParamAplicacao) <> strTipoProcesso_Avaliar) And (Trim(gsParamAplicacao) <> strTipoProcesso_Avaliar_Com_Imagem) And (Trim(gsParamAplicacao) <> strTipoProcesso_Avaliar_Cosseguro) Then
   
   btnDadosEspecificos_Parecer_Incluir.Enabled = False
   btnDadosEspecificos_Parecer_Excluir.Enabled = False
   
   
         End If
   Else
   
   
   btnDadosEspecificos_Parecer_Excluir.Enabled = False
   btnDadosEspecificos_Parecer_Consultar.Enabled = False
  
   
   End If
                                              
 
    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosGrid_AbaDadosEspecificos_ParecerMedico", Me.Caption)
    Call FinalizarAplicacao

   
End Sub

Private Sub VerificaPermissaoTecnico()

    Dim rsRecordSet As ADODB.Recordset
    Dim RS As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String
    Dim tp_Tecnico_Assessoria As String
    Dim tp_tecnico_Exclusao As Integer
    Dim tp_tecnico_Exclusao_logado As Integer
    Dim sLoginRede As String
    
  sSQL = ""
  sSQL = sSQL & "   select                                                                        " & vbCrLf
  sSQL = sSQL & "   isnull(tecnico_tb.tp_tecnico_id,0)                                            " & vbNewLine
  sSQL = sSQL & " , TECNICO_TB.assessoria                                                         " & vbNewLine
  sSQL = sSQL & " , TECNICO_TB.email                                                         " & vbNewLine
  sSQL = sSQL & "   From seguros_db.dbo.Sinistro_Tecnico_Tb    Sinistro_Tecnico_Tb  WITH (NOLOCK) " & vbNewLine
  sSQL = sSQL & "   Inner Join Tecnico_Tb                      Tecnico_Tb  WITH (NOLOCK)          " & vbNewLine
  sSQL = sSQL & "   on Sinistro_Tecnico_Tb.Tecnico_ID        = Tecnico_Tb.Tecnico_ID              " & vbNewLine
  sSQL = sSQL & "   join #Sinistro_Principal                   Sinistro_Principal                 " & vbNewLine
  sSQL = sSQL & "   on Sinistro_Tecnico_Tb.Sinistro_ID       = Sinistro_Principal.Sinistro_ID     " & vbNewLine
  sSQL = sSQL & "   where Sinistro_Tecnico_Tb.dt_fim_vigencia IS NULL                               " & vbNewLine

        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, True)
        With rsRecordSet
            If Not .EOF Then

                    tp_tecnico_Exclusao = .Fields.Item(0).Value
                    tp_Tecnico_Assessoria = .Fields.Item(1).Value
            
            'Ricardo Toledo (Confitec) : IM00015563 : 14/03/2017 : INI
            'Inclus�o da mensagem para o caso de BOF ou EOF. Posteriormente sai da sub porque as instru��es seguintes baseiam-se nesse resultado
            Else
                MsgBox "Esse sinistro ainda n�o foi atribu�do � um t�cnico !!!", 16, "Mensagem ao Usu�rio"
                Exit Sub
            'Ricardo Toledo (Confitec) : IM00015563 : 14/03/2017 : FIM
            End If
       End With
       
      
                sLoginRede = BuscaLogin(cUserName)  'Passando CPF para buscar
                
                 sSQL = ""
                 sSQL = sSQL & "   select tp_tecnico_id from seguros_db..tecnico_tb " & vbCrLf
                 sSQL = sSQL & "   where  tecnico_tb.email = '" & sLoginRede & "'" & vbNewLine
                 
                 Set RS = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, True)
                 
               
      If LCase(Trim("" & rsRecordSet("email"))) <> LCase(Trim(sLoginRede)) Then

            'btnDadosEspecificos_Parecer_Excluir.Enabled = False
            btnDadosEspecificos_Parecer_Incluir.Enabled = False
            
       Else
       
       
                 If tp_tecnico_Exclusao > 0 And tp_tecnico_Exclusao <= 4 Then
       
                     btnDadosEspecificos_Parecer_Excluir.Enabled = True
       
                 Else
       
                     btnDadosEspecificos_Parecer_Excluir.Enabled = False
       
                 End If
       
                 If Not UCase(tp_Tecnico_Assessoria) = "S" Then
       
                      btnDadosEspecificos_Parecer_Incluir.Enabled = False
       
                 Else
       
                      btnDadosEspecificos_Parecer_Incluir.Enabled = True
       
                 End If
            
      End If
      
      With RS
                If Not .EOF Then
                
                        tp_tecnico_Exclusao_logado = .Fields.Item(0).Value
                
                        If tp_tecnico_Exclusao_logado > 0 And tp_tecnico_Exclusao_logado <= 4 Then
                
                                btnDadosEspecificos_Parecer_Excluir.Enabled = True
                
                        Else
                
                                btnDadosEspecificos_Parecer_Excluir.Enabled = False
                
                        End If
                Else
                        btnDadosEspecificos_Parecer_Excluir.Enabled = False
                End If
          End With
            
            
       
    Set rsRecordSet = Nothing
'
    
    
End Sub
'gustavo.cruz 21/12/2017
Private Sub AdjustGridRowHeight(pobjGrid As MSFlexGrid, _
                               pobjLabel As Label, _
                               plngRow As Integer, _
                               plngCol As Integer, _
                               Optional pblnAddNewLine As Boolean = False)
'-----------------------------------------------------------------------------
    
    ' Adjusts the height of a flexgrid row, using the height required by a
    ' label using the same font, fontsize, and width. The label used must have
    ' Autosize and WordWrap set to True.
    ' Also, the WordWrap property of the flexgrid must be set to True.
    
    With pobjGrid
        .Row = plngRow
        .col = plngCol
        pobjLabel.FontName = .CellFontName
        pobjLabel.FontSize = .CellFontSize
        pobjLabel.Width = .CellWidth
        pobjLabel.Caption = .Text & IIf(pblnAddNewLine, vbNewLine, "")
        If pobjLabel.Height > .RowHeight(.Row) Then
            .RowHeight(.Row) = pobjLabel.Height
        End If
    End With
    
End Sub
Private Sub CarregaDadosTela_AbaDetalhamento()
Dim aLinha() As Variant
Dim numero_de_registros As Integer
Dim Linha As Integer

Dim Max_Wid As Single
Dim Wid As Single
Dim Max_Row As Integer
Dim R As Integer
Dim c As Integer

Dim sSQL As String
Dim RS As ADODB.Recordset
Dim flag As String
    
On Error GoTo Erro

'If Not bCarregou_AbaDetalhamento Then  'AKIO.OKUNO - 30/04/2013
' If Not bCarregou_AbaDetalhamento And bAlterou_fmeDetalhamento_VisualizarImpressao Then  'AKIO.OKUNO - 30/04/2013

'        InicializaModoProcessamentoObjeto txtDetalhamento_Visualizar, 1
'
'        txtDetalhamento_Visualizar = ""
'
'        MousePointer = vbHourglass
'
'        SelecionaDados 1
'
'        txtDetalhamento_Visualizar = CarregaDados_DetalhamentoSinistro
'
'        bCarregou_AbaDetalhamento = True
'
'        InicializaModoProcessamentoObjeto txtDetalhamento_Visualizar, 2
'
'        MousePointer = vbNormal

' gustavo.cruz 21/12/2017
    sSQL = ""
    sSQL = sSQL & "set nocount on " & vbCrLf
    sSQL = sSQL & "SELECT permite_sinistro_detalhamento FROM produto_tb " & vbCrLf
    sSQL = sSQL & "WHERE produto_id = " & gbllProduto_ID
    
    Set RS = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, True)
                                                                
    flag = RS(0)
    
    RS.Close
    Set RS = Nothing
        
    
   If bCarregou_AbaDetalhamento And Not bAlterou_fmeDetalhamento_VisualizarImpressao And UCase(flag) = "N" Then
            grdDetalhamento.Visible = False
            btnDetalhamento_EnviarSite.Visible = False
            cmdExigencia.Left = 10980
            
            Exit Sub
    End If
           
    If Not bCarregou_AbaDetalhamento And bAlterou_fmeDetalhamento_VisualizarImpressao And UCase(flag) = "N" Then
    
        grdDetalhamento.Visible = False
        btnDetalhamento_EnviarSite.Visible = False
        cmdExigencia.Left = 10980
        
        InicializaModoProcessamentoObjeto txtDetalhamento_Visualizar, 1

        txtDetalhamento_Visualizar = ""

        SelecionaDados 1

        MousePointer = vbHourglass

        txtDetalhamento_Visualizar = CarregaDados_DetalhamentoSinistro

        bCarregou_AbaDetalhamento = True

        InicializaModoProcessamentoObjeto txtDetalhamento_Visualizar, 2

        MousePointer = vbNormal
    
    End If
        
    If UCase(flag) = "S" Then
        'Novo grid para visualiza��o do detalhamento de sinistro
        
        txtDetalhamento_Visualizar.Visible = False
        
        InicializaModoProcessamentoObjeto grdDetalhamento, 1
        
        If Not bCarregou_AbaDetalhamento And bAlterou_fmeDetalhamento_VisualizarImpressao Then
            SelecionaDados 1
        End If
        
        MousePointer = vbHourglass
        
        aLinha = CarregaDados_DetalhamentoSinistro_Grid
        
        numero_de_registros = UBound(aLinha, 1)
        
        If numero_de_registros <= 0 Then
            grdDetalhamento.Visible = False
            btnDetalhamento_EnviarSite.Visible = False
            cmdExigencia.Left = 10980
        End If
                
        If numero_de_registros > 0 Then
            
            grdDetalhamento.Rows = 1
            grdDetalhamento.Refresh
            
            With grdDetalhamento
            
            '.Redraw = False
            .Rows = numero_de_registros + 1
            .Cols = 6
            .ColAlignment(0) = 4
            .ColAlignment(1) = 1
            .ColWidth(2) = 0
            .ColWidth(3) = 0
            .ColWidth(4) = 0
            .ColWidth(5) = 0
            
            'define registros da coluna restrito para tipo checkbox
            For Y = 1 To .Rows - 1
                            .Row = Y
                            .col = 0
                            .CellFontName = "Wingdings"
                            .CellFontSize = 14
                            .CellAlignment = flexAlignCenterCenter 'flexAlignCenterTop
                            
                            If aLinha(Y, 7) <> 0 Then ' historico
                                    .Text = ""
                            ElseIf UCase(aLinha(Y, 1)) = "S" And aLinha(Y, 7) = 0 Then
                                    .Text = strChecked ' strUnChecked = "q"  strChecked = "�"
                            ElseIf UCase(aLinha(Y, 1)) = "N" And aLinha(Y, 7) = 0 Then
                                    .Text = strUnChecked ' strUnChecked = "q"  strChecked = "�"
                            End If
                            
            Next Y
            '.Redraw = True
            End With

            For Linha = 1 To numero_de_registros
            
                       'If UCase(aLinha(linha, 1)) = "S" Then ' preenche restrito web
                       '     grdDetalhamento.TextMatrix(linha, 0) = strChecked 'Checked
                       'Else
                       '     grdDetalhamento.TextMatrix(linha, 0) = strUnChecked 'UnChecked
                       'End If
            
                       grdDetalhamento.TextMatrix(Linha, 1) = aLinha(Linha, 2) ' preenche texto detalhamento
                       grdDetalhamento.TextMatrix(Linha, 2) = aLinha(Linha, 3) ' preenche detalhamento_id
                       grdDetalhamento.TextMatrix(Linha, 3) = aLinha(Linha, 4) ' preenche tp_detalhamento
                       grdDetalhamento.TextMatrix(Linha, 4) = aLinha(Linha, 5) ' preenche permite_alterar_evento_segbr
                       grdDetalhamento.TextMatrix(Linha, 5) = aLinha(Linha, 6) ' <> 0 � historico    0 = n�o � historico

                       If aLinha(Linha, 7) <> 0 Then
                       
                                    With grdDetalhamento
                                     .col = 1
                                     .Row = Linha
                                     '.CellTextStyle = 2
                                     .CellForeColor = &H80000011
                                    End With
                                    
                       End If
                                                                                    
                       AdjustGridRowHeight grdDetalhamento, lblTextoDetalhamento, Linha, 1, True
                                                    
                       If lblTextoDetalhamento.Height > grdDetalhamento.Height Then
                                            With grdDetalhamento
                                                    .Row = Linha
                                                    .col = 0
                                                    .CellAlignment = flexAlignCenterTop
                                                    '.ToolTipText = "Clique 2 vezes no texto para visualizar ou editar"
                                            End With
                       End If
                                                    
                       lblTextoDetalhamento.Caption = ""
                            
            Next Linha
                                                    
                    grdDetalhamento.TopRow = 1
                    grdDetalhamento.LeftCol = 0
            
                    MousePointer = vbNormal
                    InicializaModoProcessamentoObjeto grdDetalhamento, 2
        End If
                
        MousePointer = vbNormal
        InicializaModoProcessamentoObjeto grdDetalhamento, 2
                
    End If
        
' gustavo.cruz 21/12/2017

'End If
Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosTela_AbaDetalhamento", Me.Caption)
    Call FinalizarAplicacao

End Sub


Private Sub Form_Unload(Cancel As Integer)
    'AKIO.OKUNO - INICIO - 05/10/2012
    If strTipoProcesso = strTipoProcesso_Avisar Or _
       strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
        If bRetornaSemGerarAviso Then
            Cancel = 0
            Exit Sub
        Else
            btnCancelar_Click       'AKIO.OKUNO - 04/10/2012
            '            If bRetornaSemGerarAviso Then      'AKIO.OKUNO - 10/10/2012
            Cancel = 0
            '            Else                               'AKIO.OKUNO - 10/10/2012
            '                Cancel = -1                    'AKIO.OKUNO - 10/10/2012
            '            End If                             'AKIO.OKUNO - 10/10/2012
        End If
    'F.BEZERRA - INICIO - 11/12/2012
    Else
        If Not FrmConsultaSinistrocon.Visible Then          'AKIO.OKUNO - 10/05/2013
            btnCancelar_Click
            Cancel = 0
        End If                                              'AKIO.OKUNO - 10/05/2013
    'F.BEZERRA - FIM - 11/12/2012
    End If
    'AKIO.OKUNO - FIM - 05/10/2012
End Sub

'AMANDA VIANA - CHAMADA DO PROGRAMA CARTA DE ENCERRAMENTO - 19/02/2020
Private Sub gestaoCartaEncerramento_Click()

    Dim sChaveComposta As String
    Dim sNomeAplicacao As String
    
    sNomeAplicacao = "SEGP1521"
    sAuxiliarExecutado = "CartaSinistro"
    iParametroParaChamadaAuxiliar = 0

    sChaveComposta = sChaveComposta & Format(Data_Sistema, "dd/mm/yyyy") & "."                      '- 6 - Data_Sistema (string, formato: dd/mm/yyyy)
    sChaveComposta = sChaveComposta & IIf(sOperacaoCosseguro <> "C", "-", sOperacaoCosseguro) & "." '- 7 - OperacaoCosseguro (C = Cosseguro, - = N�o cosseguro)

    sChaveComposta = sChaveComposta & gbllProduto_ID & "."                                          '- 8 - Produto_ID (produto_ID da Proposta/Sinistro) (Integer)
    sChaveComposta = sChaveComposta & IIf(bGTR_Implantado, 1, 0) & "."                              '- 9 - GTR_Implantado
    
    With grdDadosGerais_Aviso                                                                       '- 10 - SINISTRO_BB
        If .Rows > 1 Then
            sChaveComposta = sChaveComposta & .TextMatrix(1, 0)
        End If
        sChaveComposta = sChaveComposta & "."
    End With
    
    ExecutaAplicacao sNomeAplicacao, iParametroParaChamadaAuxiliar, gbldSinistro_ID & " 0", sChaveComposta
End Sub

Private Sub grdDadosEspecificos_AcaoJudicial_DblClick()
''mathayde
''    bExecutou_grdDblClick = True 'cristovao.rodrigues 20/08/2012
''    btnDadosEspecificos_AcaoJudicial_Operacao_Click
    CarregaAuxiliarAcaoJudicial (2)
End Sub

Private Sub grdDadosEspecificos_BensSinistrados_DblClick()
''mathayde
''    bExecutou_grdDblClick = True 'cristovao.rodrigues 20/08/2012
''    btnDadosEspecificos_BensSinistrados_Operacao_Click
    CarregaAuxiliarBensSinistrados (2)

End Sub

Private Sub grdDadosEspecificos_ParecerMedico_DblClick()
CarregarAuxiliarParecer (3)
End Sub

Private Sub grdDadosEspecificos_Prestador_DblClick()
'''mathayde
''    bExecutou_grdDblClick = True 'cristovao.rodrigues 20/08/2012
''    btnDadosEspecificos_Prestadores_Operacao_Click
    CarregarAuxiliarPrestadores (2)
End Sub

Private Sub grdDadosEspecificos_Regulador_DblClick()
'mathayde
'    bExecutou_grdDblClick = True 'cristovao.rodrigues 20/08/2012
'    btnDadosEspecificos_Reguladores_Operacao_Click (0)
    CarregarAuxiliarReguladores (2)    ''mathayde - opercacao 2 - alteracao
End Sub
'gustavo.cruz 21/12/2017
Private Sub TriggerCheckbox(irow As Integer, iCol As Integer)
        With grdDetalhamento
            
            If .TextMatrix(irow, iCol) = "" Then
                Exit Sub
            ElseIf .TextMatrix(irow, iCol) = strUnChecked Then
                .TextMatrix(irow, iCol) = strChecked
                Exit Sub
            Else
                .TextMatrix(irow, iCol) = strUnChecked
                Exit Sub
            End If
            
        End With
End Sub

'gustavo.cruz 21/12/2017
Private Sub grdDetalhamento_DblClick()
Dim sChaveParaCommandDemaisCampos            As String
Dim detalhamentoId           As String
Dim tp_Detalhamento          As Integer
Dim permite_alterar_evento_SEGBR            As String
Dim historico            As String

On Error GoTo Erro

    ' Tp_Detalhamento:
    '------------------
    ' 0- Anota��o
    ' 1- Exig�ncia
    ' 2- Recibo
    ' 3- Indeferimento  MsgBox "A Exig�ncia dever� ser emitida Manualmente no SEGUR.", vbOKOnly + vbInformation
    ' 4- Solicita��o de cancelamento
    ' 5- Comunica��es feitas pelo GTR

 
    detalhamentoId = grdDetalhamento.TextMatrix(grdDetalhamento.Row, 2)
    tp_Detalhamento = grdDetalhamento.TextMatrix(grdDetalhamento.Row, 3)
    permite_alterar_evento_SEGBR = grdDetalhamento.TextMatrix(grdDetalhamento.Row, 4)
    historico = grdDetalhamento.TextMatrix(grdDetalhamento.Row, 5)
 
    
    If tp_Detalhamento = 0 And permite_alterar_evento_SEGBR = "S" And historico = 0 Then
            
            If MsgBox("Deseja alterar texto do detalhamento?", vbYesNo + vbQuestion) = vbYes Then
                
                sChaveParaCommandDemaisCampos = ""
                sChaveParaCommandDemaisCampos = Format(Data_Sistema, "dd/mm/yyyy") & "."                                    'Data_Sistema (string, formato: dd/mm/yyyy)
                sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & IIf(bGTR_Implantado = True, 1, 0) & "."     'GTR_Implantado (0 = false, 1 = true)
                sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllProduto_ID & "."                        'Produto_ID (produto_ID da Proposta/Sinistro) (Integer)
                sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & lblDescricaoLocalizacaoProcesso(1).Tag & "." 'Localizacao_Processo (string)
                sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & IIf(bVerifica_Protocolacao, 1, 0) & "."      'Verifica_Protocolacao (0 = false, 1 = true)
                sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllRamo_ID & "."                            'SEGP1286.SinRamo_Id
                sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllApolice_ID & "."                         'SEGP1286.SinApolice_Id
                sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllSucursal_Seguradora_ID & "."             'SEGP1286.SinSucursal_Id
                sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllSeguradora_Cod_Susep & "."               'SEGP1286.SinSeguradora_Id
                sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gblsSinistro_Situacao_ID & "."               'SEGP1286.SinSituacao_Id
                sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & detalhamentoId & "."                         'SEGP1286.SinDetalhamento_Id
                sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & 0                                            'SEGP1286.SinVisualiza_Id (0 = false, 1 = true)
                sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & historico                                    'SEGP1286.SinHistorico_Id (<> 0 � historico)
                
                iParametroParaChamadaAuxiliar = "4" 'SEGP1286.bytModoOperacao
            
                If PegaJanela(gblAuxiliarExecutando) = 0 Or gblAuxiliarExecutando = 0 Then
                 ExecutaAplicacao "SEGP1286", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveParaCommandDemaisCampos
            
                    If iParametroParaChamadaAuxiliar <> 3 Then
                        bCarregou_AbaDetalhamento = False
                        sAuxiliarExecutado = "Detalhamento"
            
                    End If
                End If
                Exit Sub
                
            ElseIf grdDetalhamento.RowHeight(grdDetalhamento.Row) > grdDetalhamento.Height Then
                    If MsgBox("Deseja visualizar texto do detalhamento?", vbYesNo + vbQuestion) = vbYes Then
                        
                            sChaveParaCommandDemaisCampos = ""
                            sChaveParaCommandDemaisCampos = Format(Data_Sistema, "dd/mm/yyyy") & "."                                    'Data_Sistema (string, formato: dd/mm/yyyy)
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & IIf(bGTR_Implantado = True, 1, 0) & "."     'GTR_Implantado (0 = false, 1 = true)
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllProduto_ID & "."                        'Produto_ID (produto_ID da Proposta/Sinistro) (Integer)
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & lblDescricaoLocalizacaoProcesso(1).Tag & "." 'Localizacao_Processo (string)
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & IIf(bVerifica_Protocolacao, 1, 0) & "."      'Verifica_Protocolacao (0 = false, 1 = true)
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllRamo_ID & "."                            'SEGP1286.SinRamo_Id
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllApolice_ID & "."                         'SEGP1286.SinApolice_Id
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllSucursal_Seguradora_ID & "."             'SEGP1286.SinSucursal_Id
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllSeguradora_Cod_Susep & "."               'SEGP1286.SinSeguradora_Id
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gblsSinistro_Situacao_ID & "."               'SEGP1286.SinSituacao_Id
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & detalhamentoId & "."                         'SEGP1286.SinDetalhamento_Id
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & 1 & "."                                      'SEGP1286.SinVisualiza_Id (0 = false, 1 = true)
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & historico                                    'SEGP1286.SinHistorico_Id (<> 0 � historico)
                        
                            iParametroParaChamadaAuxiliar = "4" 'SEGP1286.bytModoOperacao
                        
                            If PegaJanela(gblAuxiliarExecutando) = 0 Or gblAuxiliarExecutando = 0 Then
                             ExecutaAplicacao "SEGP1286", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveParaCommandDemaisCampos
                        
                                If iParametroParaChamadaAuxiliar <> 3 Then
                                    bCarregou_AbaDetalhamento = False
                                    sAuxiliarExecutado = "Detalhamento"
                        
                                End If
                            End If
                            Exit Sub
                    Else
                        Exit Sub
                    End If
                
            Else
                Exit Sub
                
            End If
            
    
    ElseIf ((tp_Detalhamento <> 0 Or permite_alterar_evento_SEGBR = "N") And grdDetalhamento.RowHeight(grdDetalhamento.Row) > grdDetalhamento.Height) Then
                If MsgBox("Detalhamento n�o pass�vel de altera��o, deseja visualizar texto do detalhamento?", vbYesNo + vbQuestion) = vbYes Then
                        
                            sChaveParaCommandDemaisCampos = ""
                            sChaveParaCommandDemaisCampos = Format(Data_Sistema, "dd/mm/yyyy") & "."                                    'Data_Sistema (string, formato: dd/mm/yyyy)
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & IIf(bGTR_Implantado = True, 1, 0) & "."     'GTR_Implantado (0 = false, 1 = true)
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllProduto_ID & "."                        'Produto_ID (produto_ID da Proposta/Sinistro) (Integer)
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & lblDescricaoLocalizacaoProcesso(1).Tag & "." 'Localizacao_Processo (string)
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & IIf(bVerifica_Protocolacao, 1, 0) & "."      'Verifica_Protocolacao (0 = false, 1 = true)
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllRamo_ID & "."                            'SEGP1286.SinRamo_Id
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllApolice_ID & "."                         'SEGP1286.SinApolice_Id
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllSucursal_Seguradora_ID & "."             'SEGP1286.SinSucursal_Id
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gbllSeguradora_Cod_Susep & "."               'SEGP1286.SinSeguradora_Id
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & gblsSinistro_Situacao_ID & "."               'SEGP1286.SinSituacao_Id
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & detalhamentoId & "."                         'SEGP1286.SinDetalhamento_Id
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & 1                                            'SEGP1286.SinVisualiza_Id (0 = false, 1 = true)
                            sChaveParaCommandDemaisCampos = sChaveParaCommandDemaisCampos & historico                                    'SEGP1286.SinHistorico_Id (<> 0 � historico)
                            
                            iParametroParaChamadaAuxiliar = "4" 'SEGP1286.bytModoOperacao
                        
                            If PegaJanela(gblAuxiliarExecutando) = 0 Or gblAuxiliarExecutando = 0 Then
                             ExecutaAplicacao "SEGP1286", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveParaCommandDemaisCampos
                        
                                If iParametroParaChamadaAuxiliar <> 3 Then
                                    bCarregou_AbaDetalhamento = False
                                    sAuxiliarExecutado = "Detalhamento"
                        
                                End If
                            End If
                            Exit Sub
                    Else
                        Exit Sub
                    End If
    
    Else
        MsgBox "Detalhamento n�o pass�vel de altera��o.", vbOKOnly + vbInformation
        Exit Sub
    
    End If
    
Erro:
    Call TrataErroGeral("grdDetalhamento_DblClick", Me.Caption)
End Sub



'gustavo.cruz 21/12/2017
Private Sub grdDetalhamento_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Or KeyAscii = 32 Then 'Enter/Space
        With grdDetalhamento
            'Call TriggerCheckbox(.Row, .Col)
            Call TriggerCheckbox(.Row, 0)
        End With
    End If
End Sub



'gustavo.cruz 21/12/2017
Private Sub grdDetalhamento_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 1 Then
        With grdDetalhamento
            'If .MouseRow <> 0 And .MouseCol = 1 Then
            If .MouseRow <> 0 And .MouseCol = 0 Then
                Call TriggerCheckbox(.MouseRow, .MouseCol)
            End If
        End With
    End If
End Sub


Private Sub grdEstimativasPagamentos_Beneficiario_Click()
'    Public SelectBeneficiario As Boolean
 '   SelectBeneficiario = True
End Sub

Private Sub grdEstimativasPagamentos_Beneficiario_DblClick()

'    If btnEstimativasPagamentos_Valores_Beneficiario_Alterar.Visible = False Then Exit Sub 'AKIO.OKUNO - 30/09/2012

'mathayde
'    bExecutou_grdDblClick = True 'cristovao.rodrigues 20/08/2012
'    btnEstimativasPagamentos_Valores_Beneficiario_Alterar_Click
    CarregarAuxiliarBeneficiario (2)
End Sub
'cristovao.rodrigues 22/08/2012
Private Sub grdEstimativasPagamentos_Coberturas_DblClick()

'mathayde
'    If btnEstimativasPagamentos_Valores_Cobertura.Visible = False Then Exit Sub
'    bExecutou_grdDblClick = True
'    btnEstimativasPagamentos_Valores_Cobertura_Click

End Sub

Private Sub grdEstimativasPagamentos_Estimativas_DblClick()

    Dim sChaveComposta As String
    Dim bProcessa As Boolean
    
    'Dim sChaveComplementar As String 'cristovao.rodrigues 31/03/2016 16429073 PMBC Amparo Familiar

    If grdEstimativasPagamentos_Estimativas.col = 6 And _
       grdEstimativasPagamentos_Estimativas.ColWidth(6) <> 0 Then
        'AKIO.OKUNO - INICIO - 30/09/2012
        With grdEstimativasPagamentos_Estimativas
        
'            'cristovao.rodrigues 31/03/2016 16429073 PMBC Amparo Familiar
'            If Left$(txtRetornoAuxiliar.Text, 11) = "PMBCRESGATE" Then
'                .Row = 8 'aponta para Renda Por Morte --- INDENIZA��O
'                valorResgatePMBC = Mid$(txtRetornoAuxiliar.Text, InStr(txtRetornoAuxiliar.Text, "-") + 1)
'                valorResgatePMBC = Format(valorResgatePMBC, "##############0.00")
'            End If
        
        
            If LenB(Trim(.TextMatrix(.Row, 3))) <> 0 Then
            'cristovao.rodrigues 31/03/2016 16429073 PMBC Amparo Familiar
                If CDbl(.TextMatrix(.Row, 3)) = 0 Then
                    'If bytTipoRamo <> bytTipoRamo_Vida Then 'Genjunior 15/02/2019, incluido ramo rural
                    If bytTipoRamo <> bytTipoRamo_Vida And bytTipoRamo <> bytTipoRamo_Rural Then
                        MsgBox "Este item, " & .TextMatrix(.Row, 0) & ", n�o possui saldo a pagar !", 64, "Mensagem ao Usu�rio"
                        Exit Sub
                    End If
                End If
            End If
                        
                        If gbllProduto_ID = 1240 Then
                '' Laurent Silva - 10/10/2018
                If Not ValidaPagamentoAgravo Then Exit Sub
            End If
                        
        End With
        'AKIO.OKUNO - INICIO - 30/09/2012

        bProcessa = False
        bAlterou_fmeEstimativasPagamentos_Valores = True

        iParametroParaChamadaAuxiliar = 2    ' VerificaSelecao()

        If Val(txtCountEstimativa.Tag) <> 0 Then
            sChaveComposta = txtCountEstimativa.Tag & "." & grdEstimativasPagamentos_Estimativas.Row
        Else
            sChaveComposta = ""

            iParametroParaChamadaAuxiliar = 1
        End If

        'AKIO.OKUNO - INICIO - 22/10/2012
        If LenB(Trim(sOperacaoCosseguro)) <> 0 Then
            sChaveComposta = sChaveComposta & "." & sOperacaoCosseguro
        Else                                            'AKIO.OKUNO - 13/11/2012
            sChaveComposta = sChaveComposta & ".-"      'AKIO.OKUNO - 13/11/2012
        End If
        'AKIO.OKUNO - FIM - 22/10/2012
        sChaveComposta = sChaveComposta & "." & strTipoProcesso     'AKIO.OKUNO - 13/11/2012

        If iParametroParaChamadaAuxiliar >= 2 Then
            If LenB(Trim(sChaveComposta)) <> 0 Then
                bProcessa = True
            Else
                MsgBox "N�o existe estimativa selecionada para consulta !", 16, "Mensagem ao Usu�rio"
                bProcessa = False
            End If
        Else
            bProcessa = True
        End If
        
'
'        '------- 'cristovao.rodrigues 31/03/2016 16429073 PMBC Amparo Familiar
'        'Alan Neves - C_ANEVES - Demanda: 17860335 - 14/05/2013 - Inicio
'        sChaveComplementar = sChaveComplementar & IIf(Data_Sistema <> "", Data_Sistema, "") & "."
'        sChaveComplementar = sChaveComplementar & IIf(bGTR_Implantado = True, "s", "n") & "."
'
'        If grdDadosGerais_Aviso.Rows = 1 Then
'            sChaveComplementar = sChaveComplementar & "."  ' Sinistro_BB
'        Else
'            sChaveComplementar = sChaveComplementar & sSinistroBB & "."  ' Sinistro_BB  23/05/2013 'Alan Neves - C_ANEVES
'        End If
'
'        sChaveComplementar = sChaveComplementar & gbllProposta_ID
'        'Alan Neves - C_ANEVES - Demanda: 17860335 - 14/05/2013 - Fim
'
'        If Left$(txtRetornoAuxiliar.Text, 11) = "PMBCRESGATE" Then
'            sChaveComplementar = sChaveComplementar & "." & "R"
'            sChaveComplementar = sChaveComplementar & "." & PMBC_Benef_id
'            sChaveComplementar = sChaveComplementar & "." & valorResgatePMBC
'        End If
'        '------- 'cristovao.rodrigues 31/03/2016 16429073 PMBC Amparo Familiar
        
        

        '        If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
'+------------------------------
'| Hi@gro 06/2018 petrauskas
        Dim bol_usa1462 As Boolean
        bol_usa1462 = False
        If bProcessa And grdEstimativasPagamentos_Estimativas.Row = 1 Then

'Petrauskas 28/06/2019 - Conforme solicita��o e autoriza��o de Daniel Gustavo de Pauli <DaPauli@brasilseg.com.br>
'desativada temporariamente a utiliza��o do fluxo autom�tico de pagamento de saldo devedor
'descomentar a linha abaixo para reativar a op��o.
'-------------------------------------------------------------------
            bol_usa1462 = usa_pgto_saldo_online(gbllProduto_ID)
        End If
        
        
        If bol_usa1462 Then

            If lblDescricaoLocalizacaoProcesso(1).Tag = 2 Then
                Select Case status_pgto_bb(lblSinistro_ID(0).Caption)
                    Case 0: 'n�o existe nenhum pgto
                        If MsgBox("Deseja realizar pagamento de indeniza��o ao Banco do Brasil?", vbYesNo + vbDefaultButton1) = vbYes Then
                            bol_usa1462 = True
                        Else
                            bol_usa1462 = False
                        End If
                    Case 1: 'pgto n�o confirmado
                        Dim sAux As String
                        sAux = "Existe um pagamento solicitado ao BB ainda n�o confirmado." & vbCr _
                             & "Deseja prosseguir com o pagamento de demais benefici�rios?"
                        
                        If MsgBox(sAux, vbYesNo + vbDefaultButton1) = no Then
                            Exit Sub
                        Else
                            bol_usa1462 = False
                        End If
                    Case Else
                        bol_usa1462 = False
                    'Case 2: 'pgto confirmado
                    '   bol_usa1462 = False
                
                End Select
            Else
                MsgBox "Para pagar indeniza��o, o processo precisa estar localizado na seguradora.", vbCritical, "Mensagem ao Usu�rio"
                Exit Sub
            End If
        End If
        
        If bol_usa1462 Then
            If gblAuxiliarExecutando <> 0 Then
                'tem algum programa marcado como executando
                If PegaJanela(gblAuxiliarExecutando) <> 0 Then
                    AppActivate gblAuxiliarExecutando
                    Exit Sub
                End If
            End If
            
            Dim scomando As String
            
            scomando = bytTipoRamo & "." _
                     & iParametroParaChamadaAuxiliar & "." _
                     & Trim(lblSinistro_ID(0))
            
            If Trim(sChaveComposta) <> vbNullString Then
                scomando = scomando & "." & sChaveComposta
            End If
                            
            scomando = gsPastaLocalSegbr & "SEGP1462.exe " & scomando & " " & Monta_Parametros("SEGP1462")
            gblAuxiliarExecutando = Shell(scomando, vbNormalFocus)
                
            'Delay para aguardar a aplica��o aparecer na tela
            Sleep (1000)
        
        Else
'| Hi@gro 06/2018 petrauskas
'+------------------------------
            If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
                ExecutaAplicacao "SEGP1293", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
    
                If iParametroParaChamadaAuxiliar <> 3 Then
                    bCarregou_AbaEstimativasPagamentos = False
    '                sAuxiliarExecutado = "Estimativa"  'AKIO.OKUNO - 01/11/2012
                    sAuxiliarExecutado = "PgtoSinistro"
    
                End If
            End If
        End If '| Hi@gro 06/2018 petrauskas
    End If

    Exit Sub

Erro:
    Call TrataErroGeral("grdEstimativasPagamentos_Estimativas_DblClick", Me.Caption)
    bAlterou_fmeEstimativasPagamentos_Valores = False

End Sub
Private Sub grdEstimativasPagamentos_Voucher_DblClick()
'-----------------------------------------------------------
' FLOW 18225198 - Carrega novo form de hist�rico de prazos
' Petrauskas Jan/2016
'-----------------------------------------------------------
  If bytTipoRamo = bytTipoRamo_Vida Or bytTipoRamo = bytTipoRamo_Rural Then
      If grdEstimativasPagamentos_Voucher.RowSel > 0 Then
          Load frm_hist_prazo_regulacao
          frm_hist_prazo_regulacao.p_conexao_id = lConexaoLocal
          frm_hist_prazo_regulacao.p_usuario_ativo = False
          
          'tem que ser �ltima propriedade a carregar, ela carrega o grid
          frm_hist_prazo_regulacao.p_num_recibo = grdEstimativasPagamentos_Voucher.TextMatrix(grdEstimativasPagamentos_Voucher.RowSel, 0)
          frm_hist_prazo_regulacao.p_sinistro_id = gbldSinistro_ID
          
          Call CentraFrm(frm_hist_prazo_regulacao)
          
          frm_hist_prazo_regulacao.Show vbModal
          
          Unload frm_hist_prazo_regulacao
      End If
  End If
End Sub

Private Sub grdHistorico_EvolucaoHistorico_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    With grdHistorico_EvolucaoHistorico
      
    'FLAVIO.BEZERRA - IN�CIO - 08/02/2013
    If lblSinistro_Situacao(0) <> "ENCERRADO" And strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem Then
        If (.MouseCol = 2 Or .MouseCol = 3) And .MouseRow >= 1 Then
            .ToolTipText = Trim(Left(.TextMatrix(.MouseRow, .MouseCol), 100))
        Else
            .ToolTipText = ""
        End If
    End If
    'FLAVIO.BEZERRA - FIM - 08/02/2013
     End With
End Sub

Private Sub MSFlexGrid1_Click()

End Sub

Private Sub grdPLD_Parcelas_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    With grdPLD_Parcelas
        If .MouseCol = 2 Or .MouseCol = 3 And .MouseRow >= 1 Then
            .ToolTipText = Trim(Left(.TextMatrix(.MouseRow, .MouseCol), 100))
        Else
            .ToolTipText = ""
        End If
    End With
End Sub

Private Sub tabPrincipal_Click(PreviousTab As Integer)
    Dim sMensagem As String
    On Error GoTo Erro

    'FLAVIO.BEZERRA - IN�CIO - 30/01/2013
    If lblSinistro_Situacao(0) = "ENCERRADO" And strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem And ((tabPrincipal.Tab <> 4) And (tabPrincipal.Tab <> 1)) Then
        If bRetornoEncerramento = False Then
        MsgBox "A avalia��o de um sinistro encerrado s� permite altera��es no detalhamento " & vbCrLf & "e na guia de A��es Judiciais !"
        Else
            bRetornoEncerramento = False
        End If
    End If
    'FLAVIO.BEZERRA - FIM - 30/01/2013
    
    Select Case tabPrincipal.Tab
    Case 0
        '28/02/2020 (ntedencia) c00216281-esteira-pagamento-imediato-vida-individual (inicio)
        ExibeClassificacaoSinistro (gbldSinistro_ID)
        '21/02/2020 (ntedencia) c00216281-esteira-pagamento-imediato-vida-individual (fim)
    
    Case 1
        'AKIO.OKUNO - INICIO - 30/09/2012
        'RETIRADO PARA TESTE
        '            If strTipoProcesso = strTipoProcesso_Avisar _
                     '            Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
        '                bCarregou_AbaDetalhamento = True
        '            Else
        'AKIO.OKUNO - FIM - 30/09/2012
        CarregaDadosTela_AbaDetalhamento
        '            End If 'AKIO.OKUNO - 30/09/2012
    Case 2
        CarregaDadosTela_AbaHistorico
    Case 3
        'AKIO.OKUNO - MP - INICIO - 10/05/2013
        If Not bCarregou_AbaEstimativasPagamentos Then
            'FRAME BENEFICIARIOS
            InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Beneficiario, 1
            CarregaDadosGrid_AbaEstimativasPagamentos_Beneficiario
            
            'FRAME VOUCHER
            InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Voucher, 1
            CarregaDadosGrid_AbaEstimativasPagamentos_Voucher
        End If
        'AKIO.OKUNO - MP - FIM - 10/05/2013
        
        CarregaDadosTela_AbaEstimativasPagamentos 0
    Case 4
        CarregaDadosTela_AbaDadosEspecificos
    Case 5
        CarregaDadosTela_AbaCausasEEventos
    Case 6                                  'AKIO.OKUNO - 22/01/2013
        CarregaDadosTela_AbaPLD             'AKIO.OKUNO - 22/01/2013
        Case 7
        CarregaDadosTela_AbaAgravamento     'WILDER - PECU�RIO - 06/09/2018
    End Select

    '    If strTipoProcesso = strTipoProcesso_Avisar Then
    '        tabPrincipal.TabEnabled(4) = False
    '    End If

    Exit Sub

Erro:
    Call TrataErroGeral("TabPrincipal_Click", Me.Caption)
    Call FinalizarAplicacao

End Sub


Private Sub LimpaGrid(grdFlexGrid As MSFlexGrid)

    grdFlexGrid.Rows = 1

End Sub

Private Sub InicializaInterface_Dimensao_Objeto(objObjeto As Object, strTipoAjuste As String, intWidth_Largura As Integer, intHeight_Altura As Integer, intLeft_PosicaoEsquerda As Integer, intTop_PosicaoTop As Integer)
    On Error GoTo Erro

    'strTipoAjuste = A (Aumentar) / D (Diminuir)

    strTipoAjuste = UCase(strTipoAjuste)

    With objObjeto
        .Width = .Width + IIf(strTipoAjuste = "A", intWidth_Largura, intWidth_Largura * -1) - IIf(intWidth_Largura <> 0, 200, 0)
        .Height = .Height + IIf(strTipoAjuste = "A", intHeight_Altura, intHeight_Altura * -1) - IIf(intHeight_Altura <> 0, 200, 0)
        .Left = .Left + IIf(strTipoAjuste = "A", intLeft_PosicaoEsquerda, intLeft_PosicaoEsquerda * -1) - IIf(intLeft_PosicaoEsquerda <> 0, 200, 0)
        .Top = .Top + IIf(strTipoAjuste = "A", intTop_PosicaoTop, intTop_PosicaoTop * -1) - IIf(intTop_PosicaoTop <> 0, 200, 0)
        If TypeOf objObjeto Is Frame Then
            If strTipoAjuste = "A" Then
                .Width = .Width + IIf(intWidth_Largura <> 0, 200, 0)
                .Height = .Height + IIf(intHeight_Altura <> 0, 200, 0)
            Else
                .Width = .Width - IIf(intWidth_Largura <> 0, 200, 0)
                .Height = .Height - IIf(intHeight_Altura <> 0, 200, 0)
            End If
        End If
    End With

    Exit Sub

Erro:
    Call TrataErroGeral("InicializaInterface_Dimensao_Objeto", Me.Caption)
    Call FinalizarAplicacao
    '    Resume

End Sub


Public Function Seleciona_Localizacao_Processo_Local() As Integer

    Seleciona_Localizacao_Processo_Local = Seleciona_Localizacao_Processo(lblSinistro_ID(0))

End Function

Private Sub ValidaDados_AnaliseTecnica()
    Dim sSQL As String
    Dim rsRecordSet As ADODB.Recordset

    btnAnaliseTecnica.Visible = False   'desabilita bot�o (padr�o)

    sSQL = ""
'AKIO.OKUNO - #RETIRADA - INICIO - 09/05/2013
'    sSQL = sSQL & "Select * " & vbNewLine
'    sSQL = sSQL & "  From #Seguro_Fraude_Historico    " & vbNewLine
    sSQL = sSQL & "Exec seguros_db.Dbo.SEGS11109_SPS"
'AKIO.OKUNO - #RETIRADA - FIM - 09/05/2013
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)

    With rsRecordSet
        If Not .EOF Then
            'verifica se o sinistro est� na fase = 6 (monitorado) etapa = 2 ou 3 (homologado
            If .Fields!Fase_ID = 6 And (.Fields!Nivel_ID = 2 Or Nivel_ID = 3) Then
                btnAnaliseTecnica.Visible = True
            End If
        End If
    End With

End Sub

' cristovao.rodrigues 20/08/2012
Private Function VerificaSelecao() As Byte

    If bExecutou_grdDblClick Then
        Select Case strTipoProcesso

        Case strTipoProcesso_Consulta
            VerificaSelecao = 3
        Case strTipoProcesso_Avisar
            VerificaSelecao = IIf(sSinistro_Situacao_ID = 2, 3, 2)
        Case strTipoProcesso_Avaliar
            VerificaSelecao = IIf(sSinistro_Situacao_ID = 2, 3, 2)
        Case strTipoProcesso_Reabrir
            VerificaSelecao = 1    'IIf(sSinistro_Situacao_ID = 2, 3, 2)
        Case strTipoProcesso_Encerrar
        Case strTipoProcesso_Consulta_Cosseguro
            VerificaSelecao = 3
        Case strTipoProcesso_Avisar_Cosseguro
            VerificaSelecao = IIf(sSinistro_Situacao_ID = 2, 3, 2)
        Case strTipoProcesso_Avaliar_Cosseguro
            VerificaSelecao = IIf(sSinistro_Situacao_ID = 2, 3, 2)
        Case strTipoProcesso_Reabrir_Cosseguro

        End Select
    Else
        Select Case strTipoProcesso

        Case strTipoProcesso_Consulta
            VerificaSelecao = 3
        Case strTipoProcesso_Avisar
            VerificaSelecao = 1
        Case strTipoProcesso_Avaliar
            VerificaSelecao = 1
        Case strTipoProcesso_Reabrir
            VerificaSelecao = 1    'IIf(sSinistro_Situacao_ID = 2, 3, 2)
        Case strTipoProcesso_Encerrar
        Case strTipoProcesso_Consulta_Cosseguro
            VerificaSelecao = 3
        Case strTipoProcesso_Avisar_Cosseguro
            VerificaSelecao = 1
        Case strTipoProcesso_Avaliar_Cosseguro
            VerificaSelecao = 1
        Case strTipoProcesso_Reabrir_Cosseguro

        End Select
    End If

    bExecutou_grdDblClick = False

End Function

'cristovao.rodrigues 23/08/2012
Private Function LiberaUsoCompenente() As Boolean

    LiberaUsoCompenente = False
    'AKIO.OKUNO - INICIO - 23/09/2012
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
        bTipoProcesso_Avisar_Liberado = False
    End If
    'AKIO.OKUNO - FIM - 23/09/2012

    'Victor - 03/10/2013 - 17905311 - IN�CIO
    If strTipoProcesso = strTipoProcesso_Avisar And lblSinistro_ID(0) = "" And qtdeRuralEstudo > 0 Then
        bTipoProcesso_Avisar_Liberado = True
    End If
    'Victor - 03/10/2013 - 17905311 - FIM

    If bTipoProcesso_Avisar_Liberado Then

        If sSinistro_Situacao_ID <> 0 And sSinistro_Situacao_ID <> 2 Then
            LiberaUsoCompenente = True
        Else
            If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then    'RALVES 'If strTipoProcesso = strTipoProcesso_Avisar Then
                LiberaUsoCompenente = True
            End If
        End If

    Else

        'If strTipoProcesso = strTipoProcesso_Avaliar Then      'AKIO.OKUNO - 23/09/2012
        'If strTipoProcesso = strTipoProcesso_Avaliar Or strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then      'RALVES
        If strTipoProcesso = strTipoProcesso_Avaliar Or strTipoProcesso = strTipoProcesso_Avaliar_Cosseguro Or strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then
            If sSinistro_Situacao_ID <> 2 Then
                LiberaUsoCompenente = True
            End If
        Else
            'LiberaUsoCompenente = True
        End If

    End If

End Function

Private Sub BtnAplicar_Click()

    Select Case strTipoProcesso
    Case strTipoProcesso_Reabrir, strTipoProcesso_Reabrir_Cosseguro
        sAuxiliarExecutado = "ReabSinistro"
        Reabrir_Click
    End Select

End Sub

Private Sub txtRetornoAuxiliar_Change()
'SUBSTITUIDO PELA ROTINA DE APPACTIVATE
'    'AKIO.OKUNO - INICIO - 01/10/2012
'    If sAuxiliarExecutado = "EnceSinistro" Then
'        Me.Enabled = True
'    End If
'    'AKIO.OKUNO - FIM - 01/10/2012

'    If UCase(txtRetornoAuxiliar.Text) <> "Cancelado" And Len(txtRetornoAuxiliar.Text) > 0 Then 'AKIO.OKUNO
    If UCase(txtRetornoAuxiliar.Text) <> "CANCELADO" And Len(txtRetornoAuxiliar.Text) > 0 Then
        VerificaRetornoAplicacao
    End If
    txtRetornoAuxiliar.Text = ""    'AKIO.OKUNO - 02/10/2012

End Sub


Private Sub VerificaRetornoAplicacao()
'Dim bJaAtualizado As Boolean
'bJaAtualizado = False

'If gblAuxiliarExecutando = 0 Then Exit Sub

'Do Until bJaAtualizado

'    If PegaJanela(gblAuxiliarExecutando) = 0 Then


    Select Case sAuxiliarExecutado
    Case "Detalhamento"     '1286
        bAlterou_fmeDetalhamento_VisualizarImpressao = True     'AKIO.OKUNO - 30/04/2013
                
        CarregaDadosTela_AbaDetalhamento
        bCarregou_AbaDetalhamento = True
        
        'cristovao.rodrigues 08/11/2012
        bCarregou_AbaDadosGerais = False
        bAtivou_Form = True  'Alan Neves - 16/04/2013
        'InicializaInterfaceLocal


    Case "Estimativa"       '1287
        grdEstimativasPagamentos_Estimativas.Rows = 1
        grdEstimativasPagamentos_Coberturas.Rows = 1
'AKIO.OKUNO - MP - INICIO - 10/05/2013
'        grdEstimativasPagamentos_Beneficiario.Rows = 1
'        grdEstimativasPagamentos_Voucher.Rows = 1
'AKIO.OKUNO - MP - INICIO - 10/05/2013
        CarregaDadosTela_AbaEstimativasPagamentos 0
        bCarregou_AbaEstimativasPagamentos = True
        bGerou_Estimativa = True    'AKIO.OKUNO - 05/10/2012

'AKIO.OKUNO - INICIO - 01/11/2012 - RETIRADO
'        'AKIO.OKUNO - INICIO - 17/10/2012
'        If ValidaDados_EncerramentoTotal_Local Then
'            If MsgBox("J� foi pago total todos os itens do sinistro. Deseja encerrar? ", 292) = vbYes Then
'                If sOperacaoCosseguro = "C" Then
'                    strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro
'                Else
'                    strTipoProcesso = strTipoProcesso_Encerrar
'                End If
'                gblAuxiliarExecutando = 0       'AKIO.OKUNO - 24/10/2012
'                btnEstimativasPagamentos_Valores_Cobertura.Caption = "&Encerrar"
'                btnEstimativasPagamentos_Valores_Cobertura_Click
'            End If
'        End If
'        'AKIO.OKUNO - FIM - 17/10/2012
'AKIO.OKUNO - FIM - 01/11/2012

    Case "Beneficiario"     '1288
'AKIO.OKUNO - MP - INICIO - 10/05/2013
'        grdEstimativasPagamentos_Estimativas.Rows = 1
'        grdEstimativasPagamentos_Coberturas.Rows = 1
'        grdEstimativasPagamentos_Beneficiario.Rows = 1
'        grdEstimativasPagamentos_Voucher.Rows = 1
'        CarregaDadosTela_AbaEstimativasPagamentos 0
        InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Beneficiario, 1
        CarregaDadosGrid_AbaEstimativasPagamentos_Beneficiario
'AKIO.OKUNO - MP - FIM - 10/05/2013
        bCarregou_AbaEstimativasPagamentos = True
        'AKIO.OKUNO - INICIO - 30/09/2012
        If strTipoProcesso = strTipoProcesso_Avisar Or _
           strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
            bCarregou_AbaHistorico = False
            LimpaGrid grdHistorico_EvolucaoHistorico
            CarregaDadosTela_AbaHistorico
        End If
        'AKIO.OKUNO - FIM - 30/09/2012
        
        'Alan Neves - 16/04/2013 - Inicio
        bCarregou_AbaDetalhamento = False
'        CarregaDadosTela_AbaDetalhamento   'AKIO.OKUNO - MP - 10/05/2013
'        bCarregou_AbaDetalhamento = True   'AKIO.OKUNO - MP - 10/05/2013
        'Alan Neves - 16/04/2013 - Fim
    
    Case "Prestador"        '1289
        'InicializaCargaDados_LimpezaTemporario 'AKIO.OKUNO - #RETIRADA - 02/05/2013
        'SelecionaDados (4)                     'AKIO.OKUNO - #RETIRADA - 02/05/2013
        grdDadosEspecificos_Prestador.Rows = 1
        CarregaDadosGrid_AbaDadosEspecificos_Prestador
        bCarregou_AbaDadosEspecificos = True

    Case "Regulador"        '1290
        'InicializaCargaDados_LimpezaTemporario 'AKIO.OKUNO - #RETIRADA - 02/05/2013
        'SelecionaDados (4)                     'AKIO.OKUNO - #RETIRADA - 02/05/2013
        grdDadosEspecificos_Regulador.Rows = 1
        CarregaDadosGrid_AbaDadosEspecificos_Regulador
        bCarregou_AbaDadosEspecificos = True
        'AKIO.OKUNO - INICIO - 30/09/2012
        If strTipoProcesso = strTipoProcesso_Avisar Or _
           strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
            bCarregou_AbaHistorico = False
            LimpaGrid grdHistorico_EvolucaoHistorico
            CarregaDadosTela_AbaHistorico
        End If
        'AKIO.OKUNO - FIM - 30/09/2012

    Case "Solicitante"      '1291
        lAviso_Solicitante_ID = 0
        LimpaCamposTelaLocal_AbaDadosGerais_FrameSolicitante
        LimpaCamposTelaLocal_AbaDadosGerais_FrameAviso

        If (strTipoProcesso = strTipoProcesso_Avisar Or _
            strTipoProcesso = strTipoProcesso_Avisar_Cosseguro) And IsNumeric(txtRetornoAuxiliar.Text) Then
            lSolicitante_ID = IIf(IsNumeric(txtRetornoAuxiliar.Text), txtRetornoAuxiliar.Text, 0)
            AtualizaDados_Solicitante
            btnDadosGerais_Solicitante_Cadastrar.Enabled = False
        Else
            bAtivou_Form = False  'Alan Neves - 16/04/2013
            CarregaDadosTela_AbaDadosGerais
        End If

        bCarregou_AbaDadosGerais = True

    Case "BensSinistrados"  '1292
        grdDadosEspecificos_BensSinistrados.Rows = 1
        CarregaDadosGrid_AbaDadosEspecificos_BensSinistrados
        bCarregou_AbaDadosEspecificos = True
        'AKIO.OKUNO - INICIO - 30/09/2012
        If strTipoProcesso = strTipoProcesso_Avisar Or _
           strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
            bCarregou_AbaHistorico = False
            LimpaGrid grdHistorico_EvolucaoHistorico
            CarregaDadosTela_AbaHistorico
        End If
        'AKIO.OKUNO - FIM - 30/09/2012

    Case "PgtoSinistro"     '1293
'AKIO.OKUNO - INICIO - 01/11/2012
        grdEstimativasPagamentos_Estimativas.Rows = 1
        grdEstimativasPagamentos_Coberturas.Rows = 1
'AKIO.OKUNO - MP - INICIO - 10/05/2013
'        grdEstimativasPagamentos_Beneficiario.Rows = 1
'        grdEstimativasPagamentos_Voucher.Rows = 1
        'FRAME VOUCHER
        InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Voucher, 1
        CarregaDadosGrid_AbaEstimativasPagamentos_Voucher
'AKIO.OKUNO - MP - FIM - 10/05/2013
        
        CarregaDadosTela_AbaEstimativasPagamentos 0
        bCarregou_AbaEstimativasPagamentos = True
        bGerou_Estimativa = True

        If ValidaDados_EncerramentoTotal_Local Then
            If MsgBox("J� foi pago total todos os itens do sinistro. Deseja encerrar? ", 292) = vbYes Then
                If sOperacaoCosseguro = "C" Then
                    strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro
                Else
                    strTipoProcesso = strTipoProcesso_Encerrar
                End If
                gblAuxiliarExecutando = 0
                btnEstimativasPagamentos_Valores_Cobertura.Caption = "&Encerrar"
                btnEstimativasPagamentos_Valores_Cobertura_Click
            End If
        End If
'AKIO.OKUNO - FIM - 01/11/2012

    Case "EnceSinistro"     '1294
        bRetornoEncerramento = True
        'mathayde
        btnEstimativasPagamentos_Valores_Cobertura.Enabled = False
        'SUBSTITUIDO PELA ROTINA DE APPACTIVATE
        '            'AKIO.OKUNO - INICIO - 27/09/2012
        '            If btnEstimativasPagamentos_Valores_Cobertura.Caption = "&Encerrar" Then
        '                Me.Enabled = True
        '            End If
        '            'AKIO.OKUNO - FIM - 27/09/2012
        bCarregou_AbaDadosGerais = False
        bAtivou_Form = False  'Alan Neves - 16/04/2013
        InicializaInterfaceLocal
        'cristovao.rodrigues 26/10/2012
        LimpaGrid grdHistorico_EvolucaoHistorico
        CarregaDadosTela_AbaHistorico

    Case "ReabSinistro"     '1295
        'mathayde
        BtnAplicar.Enabled = False
        'lblSinistro_Situacao(0) = Seleciona_Sinistro_Situacao(.Fields!Sinistro_Situacao_ID)
        FrmConsultaSinistrocon.Enabled = True
        'Unload Me
        lblSinistro_Situacao(0).Caption = "REABERTO"    'mathayde ???? rever
        CarregaDadosTela_Localizacao    'F.BEZERRA 16/10/2012

        'AKIO.OKUNO - INICIO - 02/10/2012
        LimpaGrid grdEstimativasPagamentos_Beneficiario
        LimpaGrid grdEstimativasPagamentos_Coberturas
        LimpaGrid grdEstimativasPagamentos_Estimativas
        LimpaGrid grdEstimativasPagamentos_Voucher
        bCarregou_AbaEstimativasPagamentos = False
        CarregaDadosTela_AbaEstimativasPagamentos 0
        'AKIO.OKUNO - FIM - 02/10/2012
        'cristovao.rodrigues 26/10/2012
        LimpaGrid grdHistorico_EvolucaoHistorico
        CarregaDadosTela_AbaHistorico

    Case "Aviso"            '1296
        'mathayde
        'gbldSinistro_ID = txtRetornoAuxiliar
        If IsNumeric(txtRetornoAuxiliar) Then
            gbldSinistro_ID = txtRetornoAuxiliar
        End If

        lblSinistro_ID(0).Caption = gbldSinistro_ID
        bCarregou_AbaDadosGerais = False

        bGerou_Aviso = True     'AKIO.OKUNO - 05/10/2012

        bAtivou_Form = False  'Alan Neves - 16/04/2013
        InicializaInterfaceLocal
        If (strTipoProcesso <> strTipoProcesso_Avaliar_Com_Imagem And strTipoProcesso <> strTipoProcesso_Avaliar) Then       'AKIO.OKUNO - 23/09/2012 'MATHAYDE
            btnDadosGerais_Solicitante_Cadastrar.Enabled = False
            btnDadosGerais_Aviso_Cadastrar.Enabled = False
        End If                                                              'AKIO.OKUNO - 23/09/2012
        '            LimpaCamposTelaLocal_AbaDadosGerais_FrameSolicitante
        '            LimpaCamposTelaLocal_AbaDadosGerais_FrameAviso
        '
        '             If strTipoProcesso = strTipoProcesso_Avisar Or _
                      '                strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
        '
        '                AtualizaDados_Aviso
        '
        '                'gbldSinistro_ID = txtRetornoAuxiliar.Text
        '                'txtProposta_ID.Text = gbldSinistro_ID
        '                'lblSinistro_ID(0).Caption = gbldSinistro_ID
        '                'strTipoProcesso = 2
        '                'SelecionaDados 0
        '             Else
        '                CarregaDadosTela_AbaDadosGerais
        '             End If
        '
        '            bCarregou_AbaDadosGerais = True

    Case "SinistroImagem"   '1297

    Case "Assessoria"       '1298

    Case "AcaoJudicial"     '1299
        'InicializaCargaDados_LimpezaTemporario     'AKIO.OKUNO - #RETIRADA - 02/05/2013
        'SelecionaDados (4)                         'AKIO.OKUNO - #RETIRADA - 02/05/2013
        grdDadosEspecificos_AcaoJudicial.Rows = 1
        CarregaDadosGrid_AbaDadosEspecificos_AcaoJudicial
        '            bCarregou_AbaDadosEspecificos = True                   'AKIO.OKUNO - 02/10/2012
        bCarregou_AbaHistorico = False
                LimpaGrid grdHistorico_EvolucaoHistorico
        CarregaDadosTela_AbaHistorico
    Case "CausasEEventos"   '1300               cristovao.rodrigues 22/10/2012
        LimpaGrid grdCausasEEventos_CausaSinistro
        CarregaDadosTela_AbaCausasEEventos
        
        
    Case "Parecer_Medico"
        
        grdDadosEspecificos_ParecerMedico.Rows = 1
        CarregaDadosGrid_AbaDadosEspecificos_ParecerMedico
        
     Case "PMBC"   '1380               'cristovao.rodrigues 31/03/2016 16429073 PMBC Amparo Familiar
        
        grdEstimativasPagamentos_Estimativas_DblClick
    
    End Select

    MousePointer = vbNormal

    '    txtRetornoAuxiliar.Text = ""                                   'AKIO.OKUNO - 02/10/2012

    'revisar, carregardadossinistro
    ''    AKIO.OKUNO -Inicio - 26 / 9 / 2012
    '    If sAuxiliarExecutado = "ReabSinistro" Then     '1295
    '        FrmConsultaSinistrocon.Visible = True          'AKIO.OKUNO - 24/09/2012 - Conforme solicitado no report n.79
    '        Unload Me
    '    End If
    ''    AKIO.OKUNO -Fim - 26 / 9 / 2012

    '    bJaAtualizado = True

    '    End If

    'Loop

End Sub

Private Sub AtualizaDados_Aviso()

    gbldSinistro_ID = txtRetornoAuxiliar.Text
    txtProposta_ID.Text = gbldSinistro_ID
    lblSinistro_ID(0).Caption = gbldSinistro_ID

    bTipoProcesso_Avisar_Liberado = True

    '    InicializaInterface_Funcionalidades_AbaDadosGerais
    '    InicializaInterface_Funcionalidades_AbaEstimativaPagamento
    '    InicializaInterface_Funcionalidades_AbaDadosEspecificos
    '    InicializaInterface_Funcionalidades_AbaDetalhamento
    '    InicializaInterface_Funcionalidades_AbaHistorico

    tabPrincipal.TabEnabled(1) = LiberaUsoCompenente
    tabPrincipal.TabEnabled(2) = LiberaUsoCompenente
    tabPrincipal.TabEnabled(3) = LiberaUsoCompenente
    tabPrincipal.TabEnabled(4) = LiberaUsoCompenente
    tabPrincipal.TabEnabled(5) = LiberaUsoCompenente

    strTipoProcesso = 2
    
    bAtivou_Form = False  'Alan Neves - 16/04/2013
    CarregaDadosTela_AbaDadosGerais
    strTipoProcesso = 1


    'Dim sSql As String
    'Dim rsRecordset As ADODB.Recordset
    '
    '    On Error GoTo TrataErro
    '
    '    sSql = ""
    '    sSql = sSql & " Update #Sinistro_Principal    " & vbNewLine
    '    sSql = sSql & " Set Sinistro_ID             = " & gbldSinistro_ID
    '
    '    Conexao_ExecutarSQL gsSIGLASISTEMA, _
         '                        glAmbiente_id, _
         '                        App.Title, _
         '                        App.FileDescription, _
         '                        sSql, _
         '                        lConexaoLocal, _
         '                        False

    Exit Sub

TrataErro:
    TrataErroGeral "AtualizaDados_Aviso", Me.Caption
    FinalizarAplicacao

End Sub

Private Sub AtualizaDados_Solicitante()
    Dim sSQL As String
    Dim rsRecordSet As ADODB.Recordset

    On Error GoTo TrataErro

    sSQL = ""
    sSQL = sSQL & " select  " & vbCrLf
    sSQL = sSQL & " solicitante_id,nome," & vbCrLf
    sSQL = sSQL & " endereco,bairro,municipio_id,municipio,estado," & vbCrLf
    sSQL = sSQL & " ddd,telefone,ramal,tp_telefone,     " & vbCrLf
    sSQL = sSQL & " ddd1,telefone1,ramal1,tp_telefone1, " & vbCrLf
    sSQL = sSQL & " ddd2,telefone2,ramal2,tp_telefone2, " & vbCrLf
    sSQL = sSQL & " ddd3,telefone3,ramal3,tp_telefone3, " & vbCrLf
    sSQL = sSQL & " ddd4,telefone4,ramal4,tp_telefone4, " & vbCrLf
    sSQL = sSQL & " ddd_fax,telefone_fax                " & vbCrLf
    sSQL = sSQL & "      , CEP                          " & vbNewLine
    sSQL = sSQL & "      , Email                        " & vbNewLine
    sSQL = sSQL & "      , grau_parentesco_id           " & vbNewLine    'Demanda 18225206 - O campo grau_parentesco nunca foi preenchido na solicitante_sinistro_tb.
    sSQL = sSQL & " from solicitante_sinistro_tb where solicitante_id = " & lSolicitante_ID

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)

    sSQL = ""
    With rsRecordSet
        sSQL = sSQL & " Update #Sinistro_Principal    " & vbNewLine
        sSQL = sSQL & "    Set Solicitante_Sinistro_Nome = '" & Trim(.Fields!Nome) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Endereco = '" & Trim(.Fields!Endereco) & "'" & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Bairro = '" & IIf(IsNull(.Fields!Bairro), "", .Fields!Bairro) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Municipio_ID = 0" & .Fields!Municipio_ID & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Municipio = '" & IIf(IsNull(.Fields!Municipio), "", .Fields!Municipio) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Municipio_Nome = '" & IIf(IsNull(.Fields!Municipio), "", .Fields!Municipio) & "'" & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Estado = '" & IIf(IsNull(.Fields!Estado), "", .Fields!Estado) & "'" & vbCrLf

        sSQL = sSQL & "      , Solicitante_Sinistro_DDD1 = '" & IIf(IsNull(.Fields!DDD), "", .Fields!DDD) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Telefone1 = '" & IIf(IsNull(.Fields!Telefone), "", .Fields!Telefone) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Ramal1 = '" & IIf(IsNull(.Fields!Ramal), "", .Fields!Ramal) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Tp_Telefone1 = '" & IIf(IsNull(.Fields!Tp_Telefone), "", .Fields!Tp_Telefone) & "'" & vbCrLf

        sSQL = sSQL & "      , Solicitante_Sinistro_DDD2 = '" & IIf(IsNull(.Fields!DDD1), "", .Fields!DDD1) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Telefone2 = '" & IIf(IsNull(.Fields!Telefone1), "", .Fields!Telefone1) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Ramal2 = '" & IIf(IsNull(.Fields!Ramal1), "", .Fields!Ramal1) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Tp_Telefone2 = '" & IIf(IsNull(.Fields!Tp_Telefone1), "", .Fields!Tp_Telefone1) & "'" & vbCrLf

        sSQL = sSQL & "      , Solicitante_Sinistro_DDD3 = '" & IIf(IsNull(.Fields!DDD2), "", .Fields!DDD2) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Telefone3 = '" & IIf(IsNull(.Fields!Telefone2), "", .Fields!Telefone2) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Ramal3 = '" & IIf(IsNull(.Fields!Ramal2), "", .Fields!Ramal2) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Tp_Telefone3 = '" & IIf(IsNull(.Fields!Tp_Telefone2), "", .Fields!Tp_Telefone2) & "'" & vbCrLf

        sSQL = sSQL & "      , Solicitante_Sinistro_DDD4 = '" & IIf(IsNull(.Fields!DDD3), "", .Fields!DDD3) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Telefone4 = '" & IIf(IsNull(.Fields!Telefone3), "", .Fields!Telefone3) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Ramal4 = '" & IIf(IsNull(.Fields!Ramal3), "", .Fields!Ramal3) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Tp_Telefone4 = '" & IIf(IsNull(.Fields!Tp_Telefone3), "", .Fields!Tp_Telefone3) & "'" & vbCrLf

        sSQL = sSQL & "      , Solicitante_Sinistro_DDD5 = '" & IIf(IsNull(.Fields!DDD4), "", .Fields!DDD4) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Telefone5 = '" & IIf(IsNull(.Fields!Telefone4), "", .Fields!Telefone4) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Ramal5 = '" & IIf(IsNull(.Fields!Ramal4), "", .Fields!Ramal4) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Tp_Telefone5 = '" & IIf(IsNull(.Fields!Tp_Telefone4), "", .Fields!Tp_Telefone4) & "'" & vbCrLf

        sSQL = sSQL & "      , Solicitante_Sinistro_DDD_Fax = '" & IIf(IsNull(.Fields!DDD_Fax), "", .Fields!DDD_Fax) & "' " & vbCrLf
        sSQL = sSQL & "      , Solicitante_Sinistro_Telefone_Fax = '" & IIf(IsNull(.Fields!Telefone_Fax), "", .Fields!Telefone_Fax) & "'" & vbCrLf

        sSQL = sSQL & "      , Solicitante_Sinistro_CEP = '" & IIf(IsNull(.Fields!Cep), "", .Fields!Cep) & "'"
        sSQL = sSQL & "      , Solicitante_Sinistro_EMail = '" & IIf(IsNull(.Fields!Email), "", .Fields!Email) & "'"

        sSQL = sSQL & "      , Solicitante_Sinistro_Grau_Parentesco_ID = '" & IIf(IsNull(.Fields!grau_parentesco_id), "", .Fields!grau_parentesco_id) & "' " & vbCrLf    'Demanda 18225206 - O campo grau_parentesco nunca foi preenchido na solicitante_sinistro_tb.

        txtSolicitante_Sinistro_Nome.Text = .Fields!Nome & ""
        txtSolicitante_Sinistro_Endereco = .Fields!Endereco & ""
        txtSolicitante_Sinistro_Bairro = .Fields!Bairro & ""
        txtSolicitante_Sinistro_Estado = .Fields!Estado & ""
        txtSolicitante_Municipio_Nome = .Fields!Municipio & ""
        txtSolicitante_Sinistro_DDD1 = .Fields!DDD & ""
        txtSolicitante_Sinistro_Telefone1 = .Fields!Telefone & ""
        txtSolicitante_Sinistro_DDD_Fax = .Fields!DDD_Fax & ""
        txtSolicitante_Sinistro_Telefone_Fax = .Fields!Telefone_Fax & ""

        txtSolicitante_Sinistro_CEP = IIf(IsNull(.Fields!Cep), "", MasCEP(.Fields!Cep))
        txtSolicitante_Sinistro_EMail = IIf(IsNull(.Fields!Email), "", .Fields!Email)

        '(INI) Demanda 18225206.
        txtVidaSolicitante_Sinistro_DDD1 = IIf(IsNull(.Fields!DDD), "", .Fields!DDD_Fax) & ""
        txtVidaSolicitante_Sinistro_Telefone1 = IIf(IsNull(.Fields!Telefone), "", .Fields!Telefone) & ""
        txtVidaSolicitante_Sinistro_DDD_Fax = IIf(IsNull(.Fields!DDD_Fax), "", .Fields!DDD_Fax) & ""
        txtVidaSolicitante_Sinistro_Telefone_Fax = IIf(IsNull(.Fields!Telefone_Fax), "", .Fields!Telefone_Fax) & ""
        
        'PAULO PELEGRINI - MU-2017-045136 - 16/07/2018 (INI)
        'txtVidaSolicitante_Sinistro_Parentesco = CarregaParentesco(IIf(IsNull(.Fields!grau_parentesco_id), 0, .Fields!grau_parentesco_id)) & ""
        '(FIM) Demanda 18225206.
        If bytTipoRamo = bytTipoRamo_Vida Then
            txtVidaSolicitante_Sinistro_Parentesco = CarregaParentesco()
        End If
        'PAULO PELEGRINI - MU-2017-045136 - 16/07/2018 (fim)
        
    End With

    Conexao_ExecutarSQL gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        sSQL, _
                        lConexaoLocal, _
                        False



    lAviso_Solicitante_ID = lSolicitante_ID

    btnDadosGerais_Aviso_Cadastrar.Enabled = True
    btnSolicitante_Sinistro_Telefone_Consulta.Enabled = True

    Exit Sub
    'Resume
TrataErro:
    TrataErroGeral "AtualizaDados_Solicitante", Me.Caption
    FinalizarAplicacao

End Sub

'Private Sub btnDadosGerais_Sinistrado_Cadastrar_Click()
'
'    Dim sChaveComposta                          As String
'    Dim bProcessa                               As Boolean
'    bProcessa = False
'    bAlterou_fmeDadosGerais_Sinistrado = True
'
'    iParametroParaChamadaAuxiliar = VerificaSelecao()
'
'    sChaveComposta = CStr("0" & lSinistrado_ID)
'
'    If iParametroParaChamadaAuxiliar >= 2 Then
'        If LenB(Trim(sChaveComposta)) <> 0 Then
'            bProcessa = True
'        Else
'            MsgBox "N�o existe sinistrado selecionado para consulta !", 16, "Mensagem ao Usu�rio"
'            bProcessa = False
'        End If
'    Else
'        bProcessa = True
'    End If
'
'    If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
'        ExecutaAplicacao "SEGPxxxx", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
'    End If
'    Exit Sub
'
'Erro:
'    Call TrataErroGeral("btnDadosGerais_Sinistrado_Cadastrar_Click", Me.Caption)
'    bAlterou_fmeDadosGerais_Sinistrado = False
'
'End Sub

'mathayde
Sub CarregarAuxiliarReguladores(iTpOperacao As Integer)
    Dim sChaveComposta As String
    Dim bProcessa As Boolean
    
    Dim sChaveComplementar As String 'FLAVIO.BEZERRA - 15/02/2013
    
    bProcessa = False

    'AKIO.OKUNO - INICIO - 03/10/2012
    '        If strTipoProcesso = strTipoProcesso_Consulta Or _
             '           strTipoProcesso = strTipoProcesso_Encerrar Or _
             '           strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
             '           strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
    If strTipoProcesso = strTipoProcesso_Consulta Or _
       strTipoProcesso = strTipoProcesso_Encerrar Or _
       strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Or _
       gblsSinistro_Situacao_ID = "2" Or _
       lblSinistro_Situacao(0) = "ENCERRADO" Then
        'AKIO.OKUNO - FIM - 03/10/2012
        iParametroParaChamadaAuxiliar = 3
    Else
        iParametroParaChamadaAuxiliar = iTpOperacao
    End If

    With grdDadosEspecificos_Regulador
        If .Rows > 1 Then
            sChaveComposta = .TextMatrix(.Row, 12)
            bProcessa = True
        Else
            If iParametroParaChamadaAuxiliar = 1 Then
                bProcessa = True
                sChaveComposta = 0                  'FLAVIO.BEZERRA - 15/02/2013
            Else
                bProcessa = False
            End If
        End If
    End With

    '        'AKIO.OKUNO - INICIO - 25/09/2012
    '        If LenB(Trim(sChaveComposta)) <> 0 Then
    '            sChaveComposta = sChaveComposta & "."
    '        End If
    '        sChaveComposta = sChaveComposta & strTipoProcesso
    '        'AKIO.OKUNO - FIM - 25/09/2012
    '
    If bProcessa Then
        If iParametroParaChamadaAuxiliar >= 2 Then
            If LenB(Trim(sChaveComposta)) <> 0 Then
                bProcessa = True
            Else
                MsgBox "N�o existe regulador selecionada para consulta !", 16, "Mensagem ao Usu�rio"
                bProcessa = False
            End If
        Else
            bProcessa = True
        End If
        
        'FLAVIO.BEZERRA - IN�CIO - 15/02/2013
        sChaveComplementar = ""
        sChaveComplementar = sChaveComplementar & Format(Data_Sistema, "dd/mm/yyyy") & "." 'Data_Sistema (string, formato: dd/mm/yyyy)
        sChaveComplementar = sChaveComplementar & IIf(bGTR_Implantado = True, 1, 0)        'GTR_Implantado(0 = False, 1 = True)
        'FLAVIO.BEZERRA - FIM - 15/02/2013

        If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
            'ExecutaAplicacao "SEGP1290", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta  'AKIO.OKUNO - 05/10/2012
            'ExecutaAplicacao "SEGP1290", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta & "." & strTipoProcesso 'FLAVIO.BEZERRA - 15/02/2013
            ExecutaAplicacao "SEGP1290", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta & "." & strTipoProcesso, sChaveComplementar


            If iParametroParaChamadaAuxiliar <> 3 Then
                '                    bCarregou_AbaDadosEspecificos = False          'AKIO.OKUNO - 02/10/2012
                sAuxiliarExecutado = "Regulador"
            End If
        End If
    End If
    '    End If

    Exit Sub

Erro:
    Call TrataErroGeral("CarregarAuxiliarReguladores", Me.Caption)
    bAlterou_fmeDadosEspecificos_Reguladores = False

End Sub





'mathayde
Sub CarregarDadosSolicitante(iTpOperacao As Integer)
    Dim sChaveComposta As String
    Dim bProcessa As Boolean
    bProcessa = False


    'AKIO.OKUNO - INICIO - 03/10/2012
    '    If strTipoProcesso = strTipoProcesso_Consulta Or _
         '       strTipoProcesso = strTipoProcesso_Encerrar Or _
         '       strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
         '       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
    If strTipoProcesso = strTipoProcesso_Consulta Or _
       strTipoProcesso = strTipoProcesso_Encerrar Or _
       strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Or _
       gblsSinistro_Situacao_ID = "2" Or _
       lblSinistro_Situacao(0) = "ENCERRADO" Then
        'AKIO.OKUNO - FIM - 03/10/2012
        iParametroParaChamadaAuxiliar = 3
    Else
        iParametroParaChamadaAuxiliar = iTpOperacao
    End If


    sChaveComposta = CStr("0" & lSolicitante_ID)

    If iParametroParaChamadaAuxiliar >= 2 Then
        If LenB(Trim(sChaveComposta)) <> 0 Then
            bProcessa = True
        Else
            MsgBox "N�o existe solicitante selecionado para consulta !", 16, "Mensagem ao Usu�rio"
            bProcessa = False
        End If
    Else
        bProcessa = True
    End If

    '    If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
    If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then

        ExecutaAplicacao "SEGP1291", iParametroParaChamadaAuxiliar, IIf(lblSinistro_ID(0) = "", "0", lblSinistro_ID(0)), sChaveComposta
        If iParametroParaChamadaAuxiliar <> 3 Then
            bCarregou_AbaDadosGerais = False
            sAuxiliarExecutado = "Solicitante"
            If Trim(gsParamAplicacao) <> "1" Then
                AtualizaDados_Solicitante  'Demanda 18225206
            End If
        End If

    End If
    Exit Sub

Erro:
    Call TrataErroGeral("CarregarDadosSolicitante", Me.Caption)
    bAlterou_fmeDadosGerais_Solicitante = False
End Sub



'mathayde - 03
Sub CarregarAuxiliarBeneficiario(ByVal iTpOperacao As Integer)
    Dim sChaveComposta As String
    Dim bProcessa As Boolean
    Dim sChaveComplementar As String 'FLAVIO.BEZERRA - 15/02/2013
    
    bProcessa = False
    bAlterou_fmeEstimativasPagamentos_Valores_Beneficiario = True


    If strTipoProcesso = strTipoProcesso_Consulta Or _
       strTipoProcesso = strTipoProcesso_Encerrar Or _
       strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
        iParametroParaChamadaAuxiliar = 3
    Else
        iParametroParaChamadaAuxiliar = iTpOperacao
    End If

    With grdEstimativasPagamentos_Beneficiario
        If .Rows > 1 Then
            sChaveComposta = .TextMatrix(.Row, 17)    'mathayde
            bProcessa = True
        Else
            If iParametroParaChamadaAuxiliar = 1 Then
                bProcessa = True
                sChaveComposta = 0
            Else
                bProcessa = False
            End If
        End If
    End With

    If bProcessa Then
        If iParametroParaChamadaAuxiliar >= 2 Then
            If LenB(Trim(sChaveComposta)) <> 0 Then
                bProcessa = True
            Else
                MsgBox "N�o existe benefici�rio selecionado para consulta !", 16, "Mensagem ao Usu�rio"
                bProcessa = False
            End If
        Else
            bProcessa = True
        End If
        
        'FLAVIO.BEZERRA - IN�CIO - 15/02/2013
        sChaveComplementar = ""
        sChaveComplementar = sChaveComplementar & Format(Data_Sistema, "dd/mm/yyyy") & "."                  '- 5 - Data_Sistema (string, formato: dd/mm/yyyy)
        sChaveComplementar = sChaveComplementar & IIf(bGTR_Implantado = True, 1, 0) & "."                   '- 6 - GTR_Implantado (0 = false, 1 = true)
        sChaveComplementar = sChaveComplementar & IIf(sOperacaoCosseguro <> "C", "-", sOperacaoCosseguro)   '- 7 - OperacaoCosseguro (C = Cosseguro, - = N�o cosseguro)
        'FLAVIO.BEZERRA - FIM - 15/02/2013
        'AKIO.OKUNO - #RETIRADA - INICIO - 03/05/2013
        With grdDadosGerais_Aviso
            sChaveComplementar = sChaveComplementar & "."
            If .Rows > 1 Then
                sChaveComplementar = sChaveComplementar & .TextMatrix(1, 0)
            End If
            sChaveComplementar = sChaveComplementar & "."
        End With
        sChaveComplementar = sChaveComplementar & gblsSinistro_Situacao_ID & "."
        sChaveComplementar = sChaveComplementar & gbllApolice_ID & "."
        sChaveComplementar = sChaveComplementar & gbllRamo_ID & "."
        sChaveComplementar = sChaveComplementar & gbllSeguradora_Cod_Susep & "."
        sChaveComplementar = sChaveComplementar & gbllSucursal_Seguradora_ID & "."
        sChaveComplementar = sChaveComplementar & gbllProduto_ID
        
        
        'AKIO.OKUNO - #RETIRADA - FIM - 03/05/2013
        
        '        If bProcessa And PegaJanela(gblAuxiliarExecutando) = 0 Then
        If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
            'ExecutaAplicacao "SEGP1288", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta 'FLAVIO.BEZERRA - 15/02/2013
            ExecutaAplicacao "SEGP1288", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta, sChaveComplementar
            'cristovao.rodrigues 24/08/2012
            If iParametroParaChamadaAuxiliar <> 3 Then
                bCarregou_AbaEstimativasPagamentos = False
                sAuxiliarExecutado = "Beneficiario"
                'VerificaRetornoAplicacao
            End If
        End If
    End If


    Exit Sub

Erro:
    Call TrataErroGeral("btnEstimativasPagamentos_Valores_Beneficiario_Alterar_Click", Me.Caption)
    bAlterou_fmeEstimativasPagamentos_Valores_Beneficiario = False

End Sub


Private Function Carrega_Detalhe_Resseguro(ByVal iItem_Estimativa As Integer)

    Dim RS As Recordset
    Dim sSQL As String
    Dim sLinha As String

    If grdEstimativasPagamentos_Estimativas.TextMatrix(iItem_Estimativa, 1) <> 0 Then

'AKIO.OKUNO - INICIO - 23/10/2012
'        sSQL = "EXEC resseg_db..RSGS0328_SPS " & vbNewLine & _
'               gbldSinistro_ID & "," & vbNewLine & _
'               iItem_Estimativa & "," & vbNewLine & _
'               txtSeq_Estimativa & "," & vbNewLine & _
'               MudaVirgulaParaPonto(Format(grdEstimativasPagamentos_Estimativas.TextMatrix(iItem_Estimativa, 1), _
'                                           "############0.00"))
        sSQL = ""
        sSQL = sSQL & "Set NoCount on " & vbNewLine
        sSQL = sSQL & "Exec resseg_Db..Rsgs0328_SPS " & gbldSinistro_ID & vbNewLine
        sSQL = sSQL & "                           , " & iItem_Estimativa & vbNewLine
        sSQL = sSQL & "                           , " & txtSeq_Estimativa & vbNewLine
        sSQL = sSQL & "                           , " & MudaVirgulaParaPonto(Format(grdEstimativasPagamentos_Estimativas.TextMatrix(iItem_Estimativa, 1), "############0.00")) & vbNewLine
'AKIO.OKUNO - FIM - 23/10/2012
        
        Set RS = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     sSQL, _
                                     lConexaoLocal, _
                                     True)
        'Omitindo a coluna com o contrato_ressegurador_id
        frmDetalheResseguro.grdDetalheResseguro.ColWidth(1) = 0

        'Carregando grid
        While Not RS.EOF

            sLinha = ""
            sLinha = sLinha & RS("nome") & vbTab
            sLinha = sLinha & RS("contrato_ressegurador_id") & vbTab
            sLinha = sLinha & RS("nome_resseguradora") & vbTab
            sLinha = sLinha & TrocaPontoPorVirgula(RS("perc_participacao")) & vbTab
            sLinha = sLinha & TrocaPontoPorVirgula(RS("perc_resseguro")) & vbTab
            sLinha = sLinha & FormataValorParaPadraoBrasil(TrocaPontoPorVirgula(RS("val_resseguro"))) & vbTab

            frmDetalheResseguro.grdDetalheResseguro.AddItem sLinha

            RS.MoveNext

        Wend

    End If

    'EXIBINDO DETALHES DE RESSEGURO''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    frmDetalheResseguro.Show

End Function


'AKIO.OKUNO - ValidaDados_EncerramentoTotal_Local - 17/10/2012
Private Function ValidaDados_EncerramentoTotal_Local() As Boolean
    ValidaDados_EncerramentoTotal_Local = ValidaDados_EncerramentoTotal(Me)
End Function

'cristovao.rodrigues 22/10/2012
Private Sub InicializaInterface_Funcionalidades_AbaCausasEEventos()
    

    Select Case strTipoProcesso
'    Case strTipoProcesso_Consulta 'cristovao.rodrigues 24/10/2012
        
'    Case strTipoProcesso_Avisar _
'       , strTipoProcesso_Avisar_Cosseguro _

    Case strTipoProcesso_Avaliar _
       , strTipoProcesso_Avaliar_Cosseguro _
       , strTipoProcesso_Avaliar_Com_Imagem
        
        Select Case bytTipoRamo
        
        Case bytTipoRamo_Vida
            With btnCausasEEventos_Operacao
                If Not .Visible Then
                    .Visible = True
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    'InicializaInterface_Dimensao_Objeto txtDetalhamento_Visualizar, "d", 0, .Height, 0, 0
                End If
            End With
            
        Case bytTipoRamo_RE
            tabPrincipal.TabVisible(5) = False
'            With btnCausasEEventos_Operacao
'                If Not .Visible Then
'                    .Visible = False
'                    .Tag = 1
'                    iParametroParaChamadaAuxiliar = 1
'                    'InicializaInterface_Dimensao_Objeto txtDetalhamento_Visualizar, "d", 0, .Height, 0, 0
'                End If
'            End With
            
        Case bytTipoRamo_Rural
            tabPrincipal.TabVisible(5) = False
'            With btnCausasEEventos_Operacao
'                If Not .Visible Then
'                    .Visible = False
'                    .Tag = 1
'                    iParametroParaChamadaAuxiliar = 1
'                    'InicializaInterface_Dimensao_Objeto txtDetalhamento_Visualizar, "d", 0, .Height, 0, 0
'                End If
'            End With
        End Select
    
    If strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem And gblsSinistro_Situacao_ID = 2 Then 'FLAVIO.BEZERRA - 30/01/2013
        btnCausasEEventos_Operacao.Visible = False
    End If
    
    Case strTipoProcesso_Reabrir _
        , strTipoProcesso_Reabrir_Cosseguro _
        , strTipoProcesso_Encerrar _
        , strTipoProcesso_Encerrar_Cosseguro _
        , strTipoProcesso_Consulta _
        , strTipoProcesso_Avisar _
        , strTipoProcesso_Avisar_Cosseguro
        
        Select Case bytTipoRamo
        
        Case bytTipoRamo_Vida
        
            With btnCausasEEventos_Operacao
                If Not .Visible Then
                    .Visible = False
                    .Tag = 1
                    iParametroParaChamadaAuxiliar = 1
                    'InicializaInterface_Dimensao_Objeto txtDetalhamento_Visualizar, "d", 0, .Height, 0, 0
                End If
            End With
        
        Case bytTipoRamo_RE
            tabPrincipal.TabVisible(5) = False
            
        Case bytTipoRamo_Rural
            tabPrincipal.TabVisible(5) = False
        
        End Select
        
    End Select

End Sub


'cristovao.rodrigues 22/10/2012
Private Sub btnCausasEEventos_Operacao_Click()
    CarregarAuxiliarCausasEEventos (2)
End Sub

'cristovao.rodrigues 22/10/2012
Sub CarregarAuxiliarCausasEEventos(ByVal iTpOperacao As Integer)

    Dim sChaveComposta As String
    Dim bProcessa As Boolean
    bProcessa = False
    bAlterou_fmeCausasEEventos_CausaSinistro = True


    If strTipoProcesso = strTipoProcesso_Consulta Or _
       strTipoProcesso = strTipoProcesso_Encerrar Or _
       strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
        iParametroParaChamadaAuxiliar = 3
    Else
        iParametroParaChamadaAuxiliar = iTpOperacao
    End If

    With grdCausasEEventos_CausaSinistro
        If .Rows > 1 Then
            sChaveComposta = .TextMatrix(.Row, 0)
            bProcessa = True
        Else
            If iParametroParaChamadaAuxiliar = 2 Then
                bProcessa = True
            Else
                bProcessa = False
            End If
        End If
    End With

    If bProcessa Then
'        If iParametroParaChamadaAuxiliar >= 2 Then
'            If LenB(Trim(sChaveComposta)) <> 0 Then
'                bProcessa = True
'            Else
'                MsgBox "N�o existe caussa e eventos selecionado para consulta !", 16, "Mensagem ao Usu�rio"
'                bProcessa = False
'            End If
'        Else
'            bProcessa = True
'        End If

        If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then
            ExecutaAplicacao "SEGP1300", iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
            
            If iParametroParaChamadaAuxiliar <> 3 Then
                'bAlterou_fmeCausasEEventos_CausaSinistro = False
                bCarregou_AbaCausasEEventos = False
                sAuxiliarExecutado = "CausasEEventos"
                
            End If
        End If
    End If


    Exit Sub

Erro:
    Call TrataErroGeral("CarregarAuxiliarCausasEEventos", Me.Caption)
    bAlterou_fmeCausasEEventos_CausaSinistro = False

End Sub


'AKIO.OKUNO - AtualizaDados_SolicitacaoSaldo - 10/11/2012
Public Function AtualizaDados_SolicitacaoSaldo(pSinistro_id As String _
                                             , pApolice_id As String _
                                             , pSucursal_id As String _
                                             , pSeguradora_id As String _
                                             , pRamo_id As String _
                                             , pProduto_id As String) As Boolean
     
    Dim rsRecordSet                 As ADODB.Recordset
    Dim sSQL                        As String
    Dim sMensagem                   As String
    Dim sProcedure                  As String
    Dim sSinitroBB                  As String
     
    Dim RS As rdoResultset
    Dim vSql As String
    Dim iDetalhamento_id As Integer
    
    AtualizaDados_SolicitacaoSaldo = True
    
    On Error GoTo Trata_Erro:
    
    'Inserindo o detalhe -----------------------------------------------
    sMensagem = "Solicitado o saldo devedor ao BB."

    sSQL = ""
    sSQL = sSQL & "Set NoCount On                 " & vbNewLine
    sSQL = sSQL & "EXEC sinistro_detalhamento_spi " & pSinistro_id & vbNewLine
    sSQL = sSQL & "                             , " & pApolice_id & vbNewLine
    sSQL = sSQL & "                             , " & pSucursal_id & vbNewLine
    sSQL = sSQL & "                             , " & pSeguradora_id & vbNewLine
    sSQL = sSQL & "                             , " & pRamo_id & vbNewLine
    sSQL = sSQL & "                             , '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
    sSQL = sSQL & "                             , " & 0 & vbNewLine
    sSQL = sSQL & "                             , 'N'" & vbNewLine
    sSQL = sSQL & "                             , '" & cUserName & "'" & vbNewLine
    
    sProcedure = "sinistro_detalhamento_spi"
        
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                                        , glAmbiente_id _
                                        , App.Title _
                                        , App.FileDescription _
                                        , sSQL _
                                        , lConexaoLocal _
                                        , True)
    If Not rsRecordSet.EOF Then
        iDetalhamento_id = rsRecordSet.Fields(0)
    End If
        
    Set rsRecordSet = Nothing
    
    'Grava a linha -----------------------------------------------------
    sSQL = ""
    sSQL = sSQL & "Set NoCount On                 " & vbNewLine
    sSQL = sSQL & "EXEC sinistro_linha_detalhe_spi " & pSinistro_id & vbNewLine       '@sinistro_id                    numeric(11,0),"
    sSQL = sSQL & "                              , " & pApolice_id & vbNewLine        '@apolice_id                     numeric(9,0),
    sSQL = sSQL & "                              , " & pSucursal_id & vbNewLine       '@sucursal_seguradora_id         numeric(5,0),
    sSQL = sSQL & "                              , " & pSeguradora_id & vbNewLine     '@seguradora_cod_susep           numeric(5,0),
    sSQL = sSQL & "                              , " & pRamo_id & vbNewLine           '@ramo_id                        int,
    sSQL = sSQL & "                              , " & iDetalhamento_id & vbNewLine   '@detalhamento_id                int,
    sSQL = sSQL & "                              , " & 1 & vbNewLine                  '@linha_id                       int,
    sSQL = sSQL & "                              , '" & sMensagem & "'" & vbNewLine   '@linha                          varchar(80),
    sSQL = sSQL & "                              , '" & cUserName & "'" & vbNewLine   '@usuario                        ud_usuario
                
    sProcedure = "sinistro_linha_detalhe_spi"
        
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
                                

    'Seleciona o numero do sinistro_bb -----------------------------
    sSQL = ""
    sSQL = sSQL & "Select Sinistro_BB" & vbNewLine
    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_BB_Tb             Sinistro_BB_Tb  WITH (NOLOCK) " & vbNewLine
    sSQL = sSQL & " Where sinistro_id                               = " & pSinistro_id & vbNewLine
    sSQL = sSQL & "   and dt_fim_vigencia                           IS NULL" & vbNewLine
        
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                                        , glAmbiente_id _
                                        , App.Title _
                                        , App.FileDescription _
                                        , sSQL _
                                        , lConexaoLocal _
                                        , True)
    If Not rsRecordSet.EOF Then
        '        bSinistroBB = rs("sinistro_bb")        'FLAVIO.BEZERRA - 22/01/2013
        bSinistroBB = rsRecordSet.Fields!Sinistro_BB
    Else
        bSinistroBB = "Null"
    End If
        
    Set rsRecordSet = Nothing
        

    'Gravando a solicitacao ----------------------------------------
    sSQL = ""
    sSQL = sSQL & "Set NoCount On                 " & vbNewLine
    sSQL = sSQL & "EXEC SEGS9296_SPI " & pSinistro_id & vbNewLine     '@sinistro_id NUMERIC(11, 0)"
    sSQL = sSQL & "                , " & bSinistroBB & vbNewLine      '@sinistro_bb NUMERIC(13, 0)
    sSQL = sSQL & "                , " & pProduto_id & vbNewLine      '@produto_id  INT
    sSQL = sSQL & "                , " & 0 & vbNewLine                '@situacao    INT
    sSQL = sSQL & "                , NULL " & vbNewLine               '@val_saldo   NUMERIC(15, 2)
    sSQL = sSQL & "                , '" & cUserName & "'" & vbNewLine '@usuario     VARCHAR(20)
    
    'FLOW19213682 - Registro do usu�rio Solicitante do Saldo
    sSQL = sSQL & ",'" & cUserName & "'"
        
    sProcedure = "SEGS9296_SPI"
            
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                      , glAmbiente_id _
                      , App.Title _
                      , App.FileDescription _
                      , sSQL _
                      , lConexaoLocal _
                      , False
    
    MsgBox "Saldo solicitado com sucesso!", vbExclamation
        
    Exit Function
    Resume
Trata_Erro:
    MsgBox "Ocorreu o erro n�.: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Rotina: AtualizaDados_SolicitacaoSaldo" & vbCrLf & Procedure, vbOKOnly + vbCritical
    TrataErroGeral "Inclus�o Solicita��o de Saldo"
    AtualizaDados_SolicitacaoSaldo = False
    
End Function
'Alexandre.valim - Bot�o de cancelamento - 30/07/2014
Public Sub Cancela_Solicitacao_Saldo_Devedor(ByVal pSinistro_id As String)

                                             
    Dim rsRecordSet                 As ADODB.Recordset
    Dim sSQL                        As String
    Dim sMensagem                   As String
     
    'Dim rs As rdoResultset
    'Dim vsql As String
    'Dim iDetalhamento_id As Integer
    
    'AtualizaDados_SolicitacaoSaldo = True
    
    On Error GoTo Trata_Erro:
    
    'Inserindo o detalhe -----------------------------------------------
    sSQL = ""
    sSQL = sSQL & "Set NoCount On                 " & vbNewLine
    sSQL = sSQL & "EXEC SEGS12163_SPI " & pSinistro_id
    sSQL = sSQL & "                ,  " & cUserName & vbNewLine

    
    Conexao_ExecutarSQL gsSIGLASISTEMA _
                        , glAmbiente_id _
                        , App.Title _
                        , App.FileDescription _
                        , sSQL _
                        , lConexaoLocal _
                        , False
   
    
    MsgBox "Solicita��o cancelada com sucesso!", vbExclamation
    Call InicializaInterfaceLocal
     'FLAVIO.BEZERRA - IN�CIO - 30/01/2013
        If tabPrincipal.Tab = 1 Then
            bCarregou_AbaDetalhamento = False
            CarregaDadosTela_AbaDetalhamento
        Else
            bCarregou_AbaDetalhamento = False
        End If
        'FLAVIO.BEZERRA - FIM - 30/01/2013
    Exit Sub
    Resume
Trata_Erro:
    MsgBox "Ocorreu o erro n�.: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Rotina: Cancela_Solicitacao_Saldo_Devedor" & vbCrLf & Procedure, vbOKOnly + vbCritical
    TrataErroGeral "Cancelamento de Solicita��o de Saldo"
    'Cancela_Solicitacao_Saldo_Devedor = False
End Sub
'Alexandre.valim - Bot�o de cancelamento - 30/07/2014


'AKIO.OKUNO - CarregaDadosTela_AbaPLD - 22/01/2013
Private Sub CarregaDadosTela_AbaPLD()
    Dim rsRecordSet                 As ADODB.Recordset
    Dim sSQL                        As String
    Dim iLinha                      As Integer

    On Error GoTo Erro

    If Not bCarregou_AbaPLD Then

        MousePointer = vbHourglass

        InicializaModoProcessamentoObjeto grdPLD_Parcelas, 1

        SelecionaDados 6

        sSQL = ""
'AKIO.OKUNO - #RETIRADA - INICIO - 07/05/2013
'        sSQL = sSQL & "Select *                 " & vbCrLf
'        sSQL = sSQL & "  From #PLD_Parcelas     " & vbCrLf
        sSQL = sSQL & "Select Distinct                                                                                                                  "
        sSQL = sSQL & "    Num_Parcela                                          = Pgto_Sinistro_Quebra_Garantia_Tb.Num_Parcela_Consorcio                "
        sSQL = sSQL & "     , Saldo_Devedor                                     = IsNull(Pgto_Sinistro_Quebra_Garantia_Tb.Val_Saldo_Devedor_Cota, 0.00) "
        sSQL = sSQL & "     , Dt_Adiantamento                                   = CASE Evento_SEGBR_Sinistro_Atual_Tb.Evento_BB_ID                      "
        sSQL = sSQL & "                                                               WHEN 2300 THEN Evento_SEGBR_Sinistro_Atual_Tb.Dt_Evento           "
        sSQL = sSQL & "                                                               WHEN 2301 THEN Evento_SEGBR_Sinistro_Atual_Tb.Dt_Evento           "
        sSQL = sSQL & "                                                               Else ''                                                           "
        sSQL = sSQL & "                                                         END                                                                     "
        sSQL = sSQL & "     , Val_Parcela                                       = Pgto_Sinistro_Quebra_Garantia_Tb.Val_Parcela                          "
        sSQL = sSQL & "     , val_Adiantamento                                  = IsNull(Pgto_Sinistro_Quebra_Garantia_Tb.Val_Adiantamento, 0.00)       "
        sSQL = sSQL & "     , Status_Cobranca                                   = Evento_SEGBR_Sinistro_Atual_Tb.Situacao_Cobranca                      "
        sSQL = sSQL & "     , Dt_Ressarcimento                                  = CASE Evento_SEGBR_Sinistro_Atual_Tb.Evento_BB_ID                      "
        sSQL = sSQL & "                                                                WHEN 2302 THEN Evento_SEGBR_Sinistro_Atual_Tb.Dt_Evento          "
        sSQL = sSQL & "                                                                WHEN 2303 THEN Evento_SEGBR_Sinistro_Atual_Tb.Dt_Evento          "
        sSQL = sSQL & "                                                                Else ''                                                          "
        sSQL = sSQL & "                                                           END                                                                   "
        sSQL = sSQL & "     , Val_Ressarcimento                                 = IsNull(Pgto_Sinistro_Quebra_Garantia_Tb.Val_Devolucao, 0.00)          "
        sSQL = sSQL & "  From Seguros_Db..Pgto_Sinistro_Quebra_Garantia_Tb      Pgto_Sinistro_Quebra_Garantia_Tb  WITH (NOLOCK)                            "
        sSQL = sSQL & "  join Seguros_Db..Evento_SEGBR_Sinistro_Atual_Tb        Evento_SEGBR_Sinistro_Atual_Tb  WITH (NOLOCK)                              "
        sSQL = sSQL & "    On Pgto_Sinistro_Quebra_Garantia_Tb.Sinistro_ID      = Evento_SEGBR_Sinistro_Atual_Tb.Sinistro_ID                            "
        sSQL = sSQL & "   And Pgto_Sinistro_Quebra_Garantia_Tb.Cod_Segmento     = Evento_SEGBR_Sinistro_Atual_Tb.Cod_Segmento                           "
        sSQL = sSQL & "   And Pgto_Sinistro_Quebra_Garantia_Tb.Num_Grupo        = Evento_SEGBR_Sinistro_Atual_Tb.Num_Grupo                              "
        sSQL = sSQL & "   And Pgto_Sinistro_Quebra_Garantia_Tb.Num_Cota         = Evento_SEGBR_Sinistro_Atual_Tb.Num_Cota                               "
        sSQL = sSQL & "   And Pgto_Sinistro_Quebra_Garantia_Tb.Num_Parcela_Consorcio = Evento_SEGBR_Sinistro_Atual_Tb.Num_Parcela_Consorcio             "
        sSQL = sSQL & "  JOIN Seguros_Db..Ramo_Tb                               Ramo_Tb  WITH (NOLOCK)                                                     "
        sSQL = sSQL & "    on Ramo_Tb.Ramo_ID                                   = Evento_SEGBR_Sinistro_Atual_Tb.Ramo_ID                                "
        sSQL = sSQL & " Where Evento_SEGBR_Sinistro_Atual_Tb.Evento_BB_ID       In (2300,2301,2302,2303)                                                "
        sSQL = sSQL & "   And Ramo_Tb.Tp_Ramo_ID                                = 2                                                                     "
        sSQL = sSQL & "   And Pgto_Sinistro_Quebra_Garantia_Tb.Sinistro_ID      = " & gbldSinistro_ID

'AKIO.OKUNO - #RETIRADA - FIM  -07/05/2013
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, _
                                              True)
    
        iLinha = 0
        
'AKIO.OKUNO - #RETIRADA - INICIO - 07/05/2013
        'CarregaDadosGrid grdPLD_Parcelas, rsRecordSet 'petrauskas, 15/07/2015 - Ajuste no carregamento do grid, deslocado
        pCarregaDadosGrid grdPLD_Parcelas, rsRecordSet, 1
'        With rsRecordSet
'            Do While Not .EOF
'                grdPLD_Parcelas.Rows = grdPLD_Parcelas.Rows + 1
'                grdPLD_Parcelas.TextMatrix(grdPLD_Parcelas.Rows - 1, 1) = .Fields!Num_Parcela
'                grdPLD_Parcelas.TextMatrix(grdPLD_Parcelas.Rows - 1, 2) = TrocaPontoPorVirgula(.Fields!Saldo_Devedor)
'                grdPLD_Parcelas.TextMatrix(grdPLD_Parcelas.Rows - 1, 3) = Format(.Fields!Dt_Adiantamento, "dd/mm/yyyy")
'                grdPLD_Parcelas.TextMatrix(grdPLD_Parcelas.Rows - 1, 4) = TrocaPontoPorVirgula(.Fields!Val_Parcela)
'                grdPLD_Parcelas.TextMatrix(grdPLD_Parcelas.Rows - 1, 5) = TrocaPontoPorVirgula(.Fields!Val_Adiantamento)
'                grdPLD_Parcelas.TextMatrix(grdPLD_Parcelas.Rows - 1, 6) = SelecionaDados_StatusCobranca(.Fields!Status_Cobranca)
'                grdPLD_Parcelas.TextMatrix(grdPLD_Parcelas.Rows - 1, 7) = IIf(.Fields!Dt_Ressarcimento = "01/01/1900", "", .Fields!Dt_Ressarcimento)
'                grdPLD_Parcelas.TextMatrix(grdPLD_Parcelas.Rows - 1, 8) = TrocaPontoPorVirgula(.Fields!Val_Ressarcimento)
'                .MoveNext
'            Loop
'        End With
'AKIO.OKUNO - #RETIRADA - FIM - 07/05/2013

        Set rsRecordSet = Nothing

        bCarregou_AbaPLD = True

        MousePointer = vbNormal

        InicializaModoProcessamentoObjeto grdPLD_Parcelas, 2

    End If
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosTela_AbaPLD", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Function pCarregaDadosGrid(grdRegistros As MSFlexGrid, rsRecordSet As ADODB.Recordset, Optional ByVal col_inicial As Integer = 0)

    With grdRegistros
        If Not rsRecordSet.EOF Then
            .Rows = rsRecordSet.RecordCount + 1
            .Cols = rsRecordSet.Fields.Count + 1
            .Row = 1
            .col = col_inicial
            .RowSel = .Rows - 1
            .ColSel = .Cols - 1
            .Clip = rsRecordSet.GetString(adClipString, -1, Chr(9), Chr(13), vbNullString)
            If .Rows > 1 Then
                .RowSel = 1
                .ColSel = 0
            Else
                .RowSel = 0
                .ColSel = 0
            End If
        End If
    End With

End Function
Private Function SelecionaDados_StatusCobranca(sStatus_Cobranca As String) As String
    
    Select Case sStatus_Cobranca
        Case "ALI"
            SelecionaDados_StatusCobranca = "Primeira Solicita��o SQG"
        Case "BLQ"
            SelecionaDados_StatusCobranca = "Bloqueio da Cota de Informa��es e Cobran�a"
        Case "DES"
            SelecionaDados_StatusCobranca = "Consorciado Que Pede Exclus�o do Consorcio"
        Case "EXC"
            SelecionaDados_StatusCobranca = "Consorciado Que Pede Exclus�o do Consorcio"
        Case "EXJ"
            SelecionaDados_StatusCobranca = "Consorciado Que Pede Exclus�o do Consorcio e tem"
        Case "INC"
            SelecionaDados_StatusCobranca = "Cotas Com Valores Incobr�veis Transf. Para Adm"
        Case "INT"
            SelecionaDados_StatusCobranca = "Intima��o Judicial"
        Case "JUR"
            SelecionaDados_StatusCobranca = "Cota Inadimplente com A��o Ajuizada"
        Case "NOR"
            SelecionaDados_StatusCobranca = "Cota em situa��o normal"
        Case "OBT"
            SelecionaDados_StatusCobranca = "Cota com sinistro vida"
        Case "PCC"
            SelecionaDados_StatusCobranca = "Primeira Carta de Cobran�a Contemplado"
        Case "PCN"
            SelecionaDados_StatusCobranca = "Primeira Carta de Cobran�a N�o Contemplado"
        Case "QUI"
            SelecionaDados_StatusCobranca = "Quitado"
        Case "REC"
            SelecionaDados_StatusCobranca = "Cota Exclu�da Recuperada"
        Case "SCC"
            SelecionaDados_StatusCobranca = "Segunda Carta de Cobran�a Contemplado"
        Case "SCN"
            SelecionaDados_StatusCobranca = "Segunda Carta de Cobran�a N�o Contemplado"
        Case "SIN"
            SelecionaDados_StatusCobranca = "Cota com sinistro vida"
        Case "SIQ"
            SelecionaDados_StatusCobranca = "Cota com sinistro vida"
        Case "SP1"
            SelecionaDados_StatusCobranca = "Cota enviada para SPC"
        Case "SP2"
            SelecionaDados_StatusCobranca = "Cota enviada para SPC"
        Case "SQA"
            SelecionaDados_StatusCobranca = "Cota com Adiantamento SQG"
        Case "SQN"
            SelecionaDados_StatusCobranca = "Cota sem Adiantamento SQG"
        Case "SQQ"
            SelecionaDados_StatusCobranca = "Quitada com SQG"
        Case "SQS"
            SelecionaDados_StatusCobranca = "Solicita��o Adiantamento"
        Case "TER"
            SelecionaDados_StatusCobranca = "Cota na Cobradora"
        Case "TRA"
            SelecionaDados_StatusCobranca = "Cota Transferida"
        Case "Z01"
            SelecionaDados_StatusCobranca = "Solicita��o de Dossi�"
        Case "Z02"
            SelecionaDados_StatusCobranca = "Dossi� Retornado"
        Case "Z03"
            SelecionaDados_StatusCobranca = "Dossi� Enviado Seguradora"
        Case "Z04"
            SelecionaDados_StatusCobranca = "Dossi� Recebido Seguradora"
        Case "Z98"
            SelecionaDados_StatusCobranca = "Solicita��o de Ajuizamento"
        Case "Z99"
            SelecionaDados_StatusCobranca = "Dossi� entregue para Ajuizamento"
        Case Else
            SelecionaDados_StatusCobranca = sStatus_Cobranca
    End Select

End Function

'AKIO.OKUNO - #RETIRADA - SelecionaDados_AbaPLD - 07/05/2013
'AKIO.OKUNO - SelecionaDados_AbaPLD
'Private Sub SelecionaDados_AbaPLD()
'    Dim sSQL As String
'
'    On Error GoTo Erro
'
'    MousePointer = vbHourglass
'
'    '-------- PARCELAS - PLD
'
'    sSQL = ""
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#PLD_Parcelas'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #PLD_Parcelas" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'
'    sSQL = sSQL & "Select Distinct                                                                                                                  "
'    sSQL = sSQL & "    Num_Parcela                                          = Pgto_Sinistro_Quebra_Garantia_Tb.Num_Parcela_Consorcio                "
'    sSQL = sSQL & "     , Saldo_Devedor                                     = IsNull(Pgto_Sinistro_Quebra_Garantia_Tb.Val_Saldo_Devedor_Cota, 0.00) "
'    sSQL = sSQL & "     , Dt_Adiantamento                                   = CASE Evento_SEGBR_Sinistro_Atual_Tb.Evento_BB_ID                      "
'    sSQL = sSQL & "                                                               WHEN 2300 THEN Evento_SEGBR_Sinistro_Atual_Tb.Dt_Evento           "
'    sSQL = sSQL & "                                                               WHEN 2301 THEN Evento_SEGBR_Sinistro_Atual_Tb.Dt_Evento           "
'    sSQL = sSQL & "                                                               Else ''                                                           "
'    sSQL = sSQL & "                                                         END                                                                     "
'    sSQL = sSQL & "     , Val_Parcela                                       = Pgto_Sinistro_Quebra_Garantia_Tb.Val_Parcela                          "
'    sSQL = sSQL & "     , val_Adiantamento                                  = IsNull(Pgto_Sinistro_Quebra_Garantia_Tb.Val_Adiantamento, 0.00)       "
'    sSQL = sSQL & "     , Status_Cobranca                                   = Evento_SEGBR_Sinistro_Atual_Tb.Situacao_Cobranca                      "
'    sSQL = sSQL & "     , Dt_Ressarcimento                                  = CASE Evento_SEGBR_Sinistro_Atual_Tb.Evento_BB_ID                      "
'    sSQL = sSQL & "                                                                WHEN 2302 THEN Evento_SEGBR_Sinistro_Atual_Tb.Dt_Evento          "
'    sSQL = sSQL & "                                                                WHEN 2303 THEN Evento_SEGBR_Sinistro_Atual_Tb.Dt_Evento          "
'    sSQL = sSQL & "                                                                Else ''                                                          "
'    sSQL = sSQL & "                                                           END                                                                   "
'    sSQL = sSQL & "     , Val_Ressarcimento                                 = IsNull(Pgto_Sinistro_Quebra_Garantia_Tb.Val_Devolucao, 0.00)          "
'    sSQL = sSQL & "  Into #PLD_Parcelas                                                                                                             "
'    sSQL = sSQL & "  From Seguros_Db..Pgto_Sinistro_Quebra_Garantia_Tb      Pgto_Sinistro_Quebra_Garantia_Tb  WITH (NOLOCK)                            "
'    sSQL = sSQL & "  join Seguros_Db..Evento_SEGBR_Sinistro_Atual_Tb        Evento_SEGBR_Sinistro_Atual_Tb  WITH (NOLOCK)                              "
'    sSQL = sSQL & "    On Pgto_Sinistro_Quebra_Garantia_Tb.Sinistro_ID      = Evento_SEGBR_Sinistro_Atual_Tb.Sinistro_ID                            "
'    sSQL = sSQL & "   And Pgto_Sinistro_Quebra_Garantia_Tb.Cod_Segmento     = Evento_SEGBR_Sinistro_Atual_Tb.Cod_Segmento                           "
'    sSQL = sSQL & "   And Pgto_Sinistro_Quebra_Garantia_Tb.Num_Grupo        = Evento_SEGBR_Sinistro_Atual_Tb.Num_Grupo                              "
'    sSQL = sSQL & "   And Pgto_Sinistro_Quebra_Garantia_Tb.Num_Cota         = Evento_SEGBR_Sinistro_Atual_Tb.Num_Cota                               "
'    sSQL = sSQL & "   And Pgto_Sinistro_Quebra_Garantia_Tb.Num_Parcela_Consorcio = Evento_SEGBR_Sinistro_Atual_Tb.Num_Parcela_Consorcio             "
'    sSQL = sSQL & "  JOIN Seguros_Db..Ramo_Tb                               Ramo_Tb  WITH (NOLOCK)                                                     "
'    sSQL = sSQL & "    on Ramo_Tb.Ramo_ID                                   = Evento_SEGBR_Sinistro_Atual_Tb.Ramo_ID                                "
'    sSQL = sSQL & " Where Evento_SEGBR_Sinistro_Atual_Tb.Evento_BB_ID       In (2300,2301,2302,2303)                                                "
'    sSQL = sSQL & "   And Ramo_Tb.Tp_Ramo_ID                                = 2                                                                     "
'    sSQL = sSQL & "   And Pgto_Sinistro_Quebra_Garantia_Tb.Sinistro_ID      = " & gbldSinistro_ID
'
'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
'    Exit Sub
'
'Erro:
'    Call TrataErroGeral("SelecionaDados_AbaPLD", Me.Caption)
'    Call FinalizarAplicacao
'
'End Sub

'AKIO.OKUNO - InicializaInterface_Funcionalidades_AbaPLD - 22/01/2013
Private Sub InicializaInterface_Funcionalidades_AbaPLD()
    With tabPrincipal
        If bytTipoRamo = bytTipoRamo_RE Then
            If sOperacaoCosseguro = "C" Then
                .TabVisible(6) = False
            Else
                If gbllRamo_ID = 60 Or gbllRamo_ID = 70 Or gbllRamo_ID = 48 Then
                    .TabVisible(6) = True
                Else
                    .TabVisible(6) = False
                End If
            End If
        Else
            .TabVisible(6) = False
        End If
    End With
End Sub
Private Sub InicializaInterface_Funcionalidade_Encerramento()

    'Visualizacao do controle
    Dim booEncerramento_Visivel As Boolean
    booEncerramento_Visivel = (strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem) Or (strTipoProcesso = strTipoProcesso_Avaliar)
    
    btnEncerrar_Sinistro.Visible = booEncerramento_Visivel
    
    If Not booEncerramento_Visivel Then Exit Sub
    
    'Habilitar o controle nas condicoes
    Dim booHabilitarControle As Boolean
    
    booHabilitarControle = Not (lblSinistro_Situacao(0).Caption Like "ENCERRADO*")
        
    If bGTR_Implantado And booHabilitarControle Then
        'S� ser� permitido encerrar se estiver virado para a seguradora
        booHabilitarControle = (Seleciona_Localizacao_Processo(gbldSinistro_ID) = 2)
    End If

    btnEncerrar_Sinistro.Enabled = booHabilitarControle
    
    If (strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem) Then
    btnEncerrar_Sinistro.Visible = booHabilitarControle
    End If
End Sub

'####################################################################################################################
'#                                rporto - JMCONFITEC 17/02/2014 Demanda 18037006
'####################################################################################################################
Private Sub ValidarPermissaoSolicitacaoSaldo()
 Dim rsRecordSet         As ADODB.Recordset
 Dim sSQL As String
  On Error GoTo Erro

   sSQL = ""
   adSQL sSQL, "SELECT COUNT(1) "
   adSQL sSQL, "  FROM seguros_db.dbo.sinistro_cobertura_tb sinistro_cobertura_tb  WITH (NOLOCK) "
   adSQL sSQL, " WHERE sinistro_id = " & CStr(gbldSinistro_ID)
   adSQL sSQL, "   AND tp_cobertura_id=527" 'MIP
   adSQL sSQL, "   AND NOT EXISTS(SELECT 1 "
   adSQL sSQL, "                    FROM sinistro_solicita_saldo_tb  WITH (NOLOCK)  "
   adSQL sSQL, "                   WHERE sinistro_solicita_saldo_tb.sinistro_id = sinistro_cobertura_tb.sinistro_id)"

   Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                                        , glAmbiente_id _
                                        , App.Title _
                                        , App.FileDescription _
                                        , sSQL _
                                        , lConexaoLocal _
                                        , True)
   With rsRecordSet
      If Not .EOF Then
         If CInt(.Fields(0)) > 0 Then
            btnSolicitarSaldo.Enabled = True
         Else
            btnSolicitarSaldo.Enabled = False
         End If
     End If
   End With
   
   rsRecordSet.Close
   Set rsRecordSet = Nothing
   
   Exit Sub

Erro:
    Call TrataErroGeral("ValidarPermissaoSolicitacaoSaldo", Me.Caption)
    Call FinalizarAplicacao
End Sub

'(INI) Demanda 18225206.
'PAULO PELEGRINI - MU-2017-045136 - 22/06/2018 (INI)

'Private Function CarregaParentesco(GrauParentescoId As Integer) As String
Private Function CarregaParentesco() As String

'PAULO PELEGRINI - MU-2017-045136 - 22/06/2018 (fim)
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    'On Error GoTo Erro

    'PAULO PELEGRINI - MU-2017-045136 - 16/07/2018 (INI)
    
    '    sSQL = ""
    '    sSQL = sSQL & "Select tp_componente_id, nome  " & vbCrLf
    '    sSQL = sSQL & " From tp_componente_tb"
    '    sSQL = sSQL & " Where tp_componente_id = " & GrauParentescoId

    'Altera��o de tabela - de GRAU_PARENTESCO_TB para TP_COMPONENTE_TB -  SD00911090 - Paulo Pelegrini - 21/09/2018

    sSQL = sSQL & "SELECT  " & vbCrLf
    sSQL = sSQL & "ISNULL(D.NOME, 'Outros') AS NOME  " & vbCrLf
    sSQL = sSQL & "FROM SEGUROS_DB.DBO.SINISTRO_TB A  " & vbCrLf
    sSQL = sSQL & "INNER JOIN SEGUROS_DB.DBO.SOLICITANTE_SINISTRO_TB B  " & vbCrLf
    sSQL = sSQL & "ON A.SOLICITANTE_ID = B.SOLICITANTE_ID  " & vbCrLf
    sSQL = sSQL & "INNER JOIN (SELECT  " & vbCrLf
    sSQL = sSQL & "SINISTRO_ID,  " & vbCrLf
    sSQL = sSQL & "MIN(SOLICITANTE_GRAU_PARENTESCO) As GRAU_PARENTESCO_ID  " & vbCrLf
    sSQL = sSQL & "From EVENTO_SEGBR_SINISTRO_ATUAL_TB  " & vbCrLf
    sSQL = sSQL & "Where SOLICITANTE_GRAU_PARENTESCO Is Not Null  " & vbCrLf
    sSQL = sSQL & "GROUP BY SINISTRO_ID) C  " & vbCrLf
    sSQL = sSQL & "ON A.SINISTRO_ID = C.SINISTRO_ID  " & vbCrLf
    sSQL = sSQL & "LEFT JOIN TP_COMPONENTE_TB D  " & vbCrLf
    sSQL = sSQL & "ON C.GRAU_PARENTESCO_ID = D.TP_COMPONENTE_ID  " & vbCrLf
    sSQL = sSQL & "Where a.sinistro_id = " & CStr(gbldSinistro_ID)
    
    'PAULO PELEGRINI - MU-2017-045136 - 16/07/2018 (FIM)

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)

    If Not rsRecordSet.EOF Then    'Err.Raise glERRO_MONTA_PARAMETRO, "CarregaParentesco", "N�o foi poss�vel carregar grau de perentesco!"
        CarregaParentesco = Trim((rsRecordSet!Nome))
    End If

    Set rsRecordSet = Nothing
    Exit Function

Erro:
    Call TrataErroGeral("CarregaParentesco", Me.Caption)
    Call FinalizarAplicacao

End Function
'(FIM) Demanda 18225206.

Private Sub VScroll1_Change()
    Call pRolaFormulario 'FBEZERRA/ FABREU - Inclus�o barra de rolagem SEGP1285 - 21/02/2018
End Sub









Private Sub VScroll1_Scroll()
    Call pRolaFormulario 'FBEZERRA/ FABREU - Inclus�o barra de rolagem SEGP1285 - 21/02/2018
End Sub
'FBEZERRA/ FABREU - Inclus�o barra de rolagem SEGP1285 - 21/02/2018
Private Sub pRolaFormulario()
   Dim ctl As Control

   For Each ctl In Me.Controls
         If (TypeOf ctl Is SSTab) Or (TypeOf ctl Is CommandButton And ctl.name = "BtnAplicar") _
                                  Or (TypeOf ctl Is CommandButton And ctl.name = "btnCancelar") _
                                  Or (TypeOf ctl Is CommandButton And ctl.name = "btnSolicitarSaldo") _
                                  Or (TypeOf ctl Is CommandButton And ctl.name = "btnAnaliseTecnica") _
                                  Or (TypeOf ctl Is TextBox And ctl.name = "txtRetornoAuxiliar") Then
              ctl.Top = ctl.Top + PosAnterior - VScroll1.Value
        End If
 Next

 PosAnterior = VScroll1.Value

 End Sub

 ' Wilder - Agravamento - 10/09/2018
Private Sub btnAgravamentos_Avaliar_Click()

    Dim sChaveComposta As String
    Dim bProcessa As Boolean
    Dim sNomeAplicacao As String

    bProcessa = False
    
    If strTipoProcesso = strTipoProcesso_Consulta Or _
       strTipoProcesso = strTipoProcesso_Encerrar Or _
       strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
       strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Then
        iParametroParaChamadaAuxiliar = 3
    Else
        If strTipoProcesso = strTipoProcesso_Avisar Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Then
            iParametroParaChamadaAuxiliar = 1
        Else
            iParametroParaChamadaAuxiliar = 2
        End If
    End If


    If iParametroParaChamadaAuxiliar <= 2 Then
        bProcessa = True
    End If
    


    If (bProcessa And PegaJanela(gblAuxiliarExecutando) = 0) Or gblAuxiliarExecutando = 0 Then

        sNomeAplicacao = "SEGP1469"
        sAuxiliarExecutado = "Regulacao"
        
        'sChaveParametro = "SEGP1285" & "|" & CStr(glAmbiente_id) & "|" & CStr(gbldSinistro_ID) & "|" & cUserName
        ExecutaAplicacao sNomeAplicacao, iParametroParaChamadaAuxiliar, lblSinistro_ID(0), sChaveComposta
       
            If iParametroParaChamadaAuxiliar <> 3 Then
            bCarregou_AbaAgravamento = False
            CarregaDadosTela_AbaAgravamento
            'VerificaRetornoAplicacao
        End If
           
    End If
    Exit Sub

Erro:
    Call TrataErroGeral("btnAgravamentos_Avaliar_Click", Me.Caption)
    
End Sub

Private Function ValidaPagamentoAgravo() As Boolean

Dim sSQL As String
Dim rsRecordSet As ADODB.Recordset

Dim CRIT_ANALISE_DE_AGRAVOS As Boolean
Dim CRIT_STATUS_E_DATA_EXECUCAO_PROPOSTA As Boolean

sSQL = ""
sSQL = sSQL & "Exec SEGUROS_DB.Dbo.SEGS14005_SPS " & lblSinistro_ID(0)
Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                      glAmbiente_id, _
                                      App.Title, _
                                      App.FileDescription, _
                                      sSQL, _
                                      lConexaoLocal, _
                                      True)

With rsRecordSet
    If Not .EOF Then

        CRIT_ANALISE_DE_AGRAVOS = .Fields!ANALISE_DE_AGRAVOS
        CRIT_STATUS_E_DATA_EXECUCAO_PROPOSTA = .Fields!STATUS_E_DATA_EXECUCAO_PROPOSTA
    
    End If
End With

Dim Resultado As Boolean
Resultado = True

If Not CRIT_ANALISE_DE_AGRAVOS Then '' Rotina que confere no BD atrav�s de PROC e retorna se os agravos est�o todos finalizados.
    Resultado = False
    MsgBox "Este sinistro possui agravos sem an�lise final!", vbCritical, "Aten��o:"
    GoTo Fim
End If

If Not CRIT_STATUS_E_DATA_EXECUCAO_PROPOSTA Then '' Rotina que confere no BD atrav�s de PROC e retorna se j� passou pela data de execu��o
    Resultado = False
    MsgBox "Este sinistro ainda n�o passou da data de execu��o!", vbCritical, "Aten��o:"
    GoTo Fim
End If

Fim:
ValidaPagamentoAgravo = Resultado
End Function

'Wilder - Agravamento Pecu�rio - 06/09/2018
Public Sub montacabecalhoGridAgravo()

    With GrdAgravo
        .Cols = 20
        
        '' Colunas do segp1285
        .TextMatrix(0, 0) = "ID"
        
        .TextMatrix(0, 1) = "Agravamento"
        .TextMatrix(0, 2) = "Qtd. Machos"
        .TextMatrix(0, 3) = "Qtd. Machos deferidos"
        .TextMatrix(0, 4) = "Qtd. Machos indeferidos"
        
        .TextMatrix(0, 5) = "Qtd. F�meas"
        .TextMatrix(0, 6) = "Qtd. F�meas deferidas"
        .TextMatrix(0, 7) = "Qtd. F�meas indeferidas"
        
        .TextMatrix(0, 8) = "Total de animais"
        .TextMatrix(0, 9) = "Total de animais deferidos"
        .TextMatrix(0, 10) = "Total de animais indeferidos"
        
        .TextMatrix(0, 11) = "Val. Estimado"
        
        .TextMatrix(0, 12) = "Dt Agravamento"
        .TextMatrix(0, 13) = "Dt Ocorr�ncia"
        .TextMatrix(0, 14) = "Dt Regula��o"
        
        .TextMatrix(0, 15) = "Desc Ocorr�ncia"
        .TextMatrix(0, 16) = "Coment�rio"
        
        .TextMatrix(0, 17) = "Usu�rio"
        .TextMatrix(0, 18) = "Status"
        .TextMatrix(0, 19) = "id_combo_status"
        
        '' Largura das colunas
        .ColWidth(0) = 0
        .ColWidth(1) = 1200
        .ColWidth(2) = 1100
        .ColWidth(3) = 1800
        .ColWidth(4) = 1900
        
        .ColWidth(5) = 1100
        .ColWidth(6) = 1800
        .ColWidth(7) = 1900
        
        .ColWidth(8) = 1300
        .ColWidth(9) = 2000
        .ColWidth(10) = 2100
        
        .ColWidth(11) = 1200
        .ColWidth(12) = 1300
        .ColWidth(13) = 1300
        
        .ColWidth(14) = 1300
        .ColWidth(15) = 0
        
        .ColWidth(16) = 0
        .ColWidth(17) = 1300
        .ColWidth(18) = 1300
        
        .ColWidth(19) = 0 '' ID DE COMBO STATUS
                
    End With
                
    
End Sub

'Wilder - Agravamento Pecu�rio - 06/09/2018
Private Sub PesquisarAgravo()

    On Error GoTo TratarErro
    Dim RS As ADODB.Recordset
    Dim SQL As String
    Dim col As Integer

    'SQL = "exec desenv_db..sinistro_agravamento_pecuario_sps " & gbldSinistro_ID
    SQL = "exec SEGUROS_DB.dbo.SEGS13931_SPS " & gbldSinistro_ID

    Set RS = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        
    Call PopulaGridAgravo(GrdAgravo, RS)
        
    Exit Sub
    
TratarErro:
    Call TratarErro("Pesquisar", "Preencher Grid")
End Sub

'Wilder - Agravamento Pecu�rio - 06/09/2018
Private Sub PopulaGridAgravo(Grid As MSFlexGrid, RS As ADODB.Recordset)

    Dim Linha, Coluna As Integer

    Grid.Rows = 1

    While Not RS.EOF
                        
            With Grid
                .Rows = .Rows + 1
                Linha = .Rows - 1
                
                For Coluna = 0 To Grid.Cols - 1
                    .TextMatrix(Linha, Coluna) = Trim(RS(Coluna).Value)
                Next
                
                Coluna = Coluna + 1
            End With
            
    RS.MoveNext
    
    Wend
    
    bCarregou_AbaAgravamento = True

End Sub

'Wilder - Agravamento Pecu�rio - 06/09/2018
Private Sub CarregaDadosTela_AbaAgravamento()
    Dim rsRecordSet                 As ADODB.Recordset
    Dim sSQL                        As String
    Dim iLinha                      As Integer

    On Error GoTo Erro

    If Not bCarregou_AbaAgravamento Then
    
        Lbl_Proposta.Caption = gbllProposta_ID
        Lbl_Sinistro.Caption = gbldSinistro_ID
    
        Call montacabecalhoGridAgravo
        Call PesquisarAgravo
        Call Calcula_Deferidos
        
    End If
    
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosTela_AbaPLD", Me.Caption)
    Call FinalizarAplicacao

End Sub

'Wilder - Agravamento Pecu�rio - 06/09/2018
Private Sub Calcula_Deferidos()

    Dim Linha As Integer
    Dim Qtd_Deferidos As Integer
    Dim Qtd_Indeferidos As Integer
    
    Qtd_Deferidos = 0
    Qtd_Indeferidos = 0

    For Linha = 1 To Me.GrdAgravo.Rows - 1
        If UCase(Me.GrdAgravo.TextMatrix(Linha, 19)) = "2" Then 'Somente Status Finalizado
            Qtd_Deferidos = Qtd_Deferidos + Me.GrdAgravo.TextMatrix(Linha, 3)
            Qtd_Indeferidos = Qtd_Indeferidos + Me.GrdAgravo.TextMatrix(Linha, 4)
        End If
    Next
    
    Me.Lbl_qtd_deferidos.Caption = Qtd_Deferidos
    Me.Lbl_qtd_indeferidos.Caption = Qtd_Indeferidos

End Sub

'Wilder - Agravamento Pecu�rio - 06/09/2018
Private Sub InicializaInterface_Funcionalidades_AbaAgravamento()
    
    With tabPrincipal
        
        .TabVisible(7) = False
        
        Select Case strTipoProcesso
            Case strTipoProcesso_Consulta
            
                Select Case bytTipoRamo
                    Case bytTipoRamo_Vida
                        If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                            .TabVisible(7) = True
                        End If
                    
                    Case bytTipoRamo_RE
                        If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                            .TabVisible(7) = True
                        End If
                    
                    Case bytTipoRamo_Rural
                            If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                                .TabVisible(7) = True
                        End If
                        
                End Select
            Case strTipoProcesso_Avisar, strTipoProcesso_Avisar_Cosseguro, strTipoProcesso_Avaliar
                
                Select Case bytTipoRamo
                    Case bytTipoRamo_Vida
                            If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                                .TabVisible(7) = True
                        End If
                        
                    Case bytTipoRamo_Rural
                            If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                                .TabVisible(7) = True
                            End If
                        btnAgravamentos_Avaliar.Visible = True
                        
                    Case bytTipoRamo_RE
                            If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                                .TabVisible(7) = True
                        End If
                        
                End Select
        
            Case strTipoProcesso_Avaliar_Com_Imagem
                        Select Case bytTipoRamo
                                Case bytTipoRamo_Vida
                        If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                            .TabVisible(7) = True
                        End If

                    Case bytTipoRamo_Rural
                        If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                            .TabVisible(7) = True
                        End If
                        btnAgravamentos_Avaliar.Visible = True

                                Case bytTipoRamo_RE
                        If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                            .TabVisible(7) = True
                        End If

                End Select

            Case strTipoProcesso_Reabrir
                        Select Case bytTipoRamo
                                Case bytTipoRamo_Vida
                        If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                                .TabVisible(7) = True
                        End If

                    Case bytTipoRamo_Rural
                        If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                            .TabVisible(7) = True
                        End If

                                Case bytTipoRamo_RE
                        If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                            .TabVisible(7) = True
                        End If

                End Select
            Case strTipoProcesso_Consulta_Cosseguro, strTipoProcesso_Avaliar_Cosseguro, strTipoProcesso_Reabrir_Cosseguro
                Select Case bytTipoRamo
                    Case bytTipoRamo_Vida
                        If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                            .TabVisible(7) = True
                                End If
                                Case bytTipoRamo_Rural
                        If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                            .TabVisible(7) = True
                                End If
                    Case bytTipoRamo_RE
                        If gbllRamo_ID = 3 And verifica_produto_agravamento(gbldSinistro_ID) Then
                            .TabVisible(7) = True
                                End If

                        End Select
        End Select

        End With


End Sub

Function verifica_produto_agravamento(ByVal sinistro_id As Double) As Boolean

    Dim sSQL As String
    Dim RS As ADODB.Recordset
    
    verifica_produto_agravamento = False
    
    sSQL = "select * from seguros_db.dbo.sinistro_agravamento_pecuario_tb (nolock)  "
    sSQL = sSQL & "where sinistro_id = " & sinistro_id
    
    Set RS = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    sSQL, _
                                    lConexaoLocal, _
                                    True)
    
    If Not RS.EOF Then
        verifica_produto_agravamento = True
        RS.Close
    End If
    Set RS = Nothing
    

End Function

Public Sub RetornoCloud()

If ProdutoDeVida(txtRamo_ID.Text) = False Then

    gbxCIAg.Visible = True
    
    BuscaValores (lblSinistro_ID(0).Caption)
    
 End If

Exit Sub

End Sub

Public Sub BuscaValores(Sinistro As String)

Dim sSQL As String
Dim rsRecordSet As ADODB.Recordset
Dim Valor As Double

Valor = 0

    If Sinistro <> "" Then

            sSQL = ""
            sSQL = sSQL & " select top 1 ISNULL(deferido_probabilidade,0), "
            sSQL = sSQL & " ISNULL(indeferido_probabilidade,0), "
            sSQL = sSQL & " ISNULL(indeferido_motivo,'') "
            sSQL = sSQL & " from  interface_dados_db.dbo.proposta_retorno_envio_cloud_tb with (nolock)"
            sSQL = sSQL & " where  sinistro_id = " & Sinistro & " and situacao = 'S' order by dt_inclusao desc"
        
         Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                  glAmbiente_id, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  sSQL, _
                                                  lConexaoLocal, _
                                                  True)
        
                    If Not rsRecordSet.EOF Then
                                    
                               
                            If rsRecordSet(0).Value > rsRecordSet(1).Value Then
                                            
                                Valor = TrataArredondamento(rsRecordSet(0).Value)
                                
                                lblCiagValor.Caption = Math.Round(Valor) & "%"
                                lblCiagTipo.Caption = "DEFERIDO"
                            
                           ElseIf rsRecordSet(0).Value = rsRecordSet(1).Value Then
                            
                                Valor = TrataArredondamento(rsRecordSet(0).Value)
                                
                                lblCiagValor.Caption = Math.Round(Valor) & "%"
                                lblCiagTipo.Caption = "DEFERIDO"
                            
                            Else
                            
                                Valor = TrataArredondamento(rsRecordSet(1).Value)
                            
                                lblCiagValor.Caption = Math.Round(Valor) & "%"
                                lblCiagValor.ForeColor = vbRed
                                lblCiagTipo.Caption = "INDEFERIDO"
                                lblCiagTipo.ForeColor = vbRed
                                lblCiagTipo.ToolTipText = rsRecordSet(2).Value
                                
                                CiagMotivo = rsRecordSet(2).Value
                                
                            End If
                    Else
                            
                    gbxCIAg.Visible = False
                                                    
                    End If
                    
                    rsRecordSet.Close
                   
            Else
                        
               gbxCIAg.Visible = False
                    
            End If

Exit Sub

End Sub

Public Function TrataArredondamento(Valor As Double) As Double

Dim valorDI As Double
Dim fracaoRest As Double

valorDI = Math.Round(Valor)

If valorDI < Valor Then

    fracao = Valor - valorDI
    
        If fracao >= 0.2 Then
    
            fracao = 1# - fracao
            Valor = Valor + fracao
    
        End If
    
End If


TrataArredondamento = Valor

Exit Function

End Function

Public Sub CiagMensagem()

    If CiagMotivo <> "" Then
    
        MsgBox CiagMotivo, vbOKOnly, "SEGP1285"
    
    End If

Exit Sub

End Sub


Private Function ProdutoDeVida(RamoId As Long) As Boolean
Dim sSQL As String
Dim rsRecordSet As ADODB.Recordset

    sSQL = ""
    sSQL = sSQL & " SELECT tp_ramo_id " & vbNewLine
    sSQL = sSQL & "   FROM seguros_db.dbo.ramo_tb WITH (NOLOCK) " & vbNewLine
    sSQL = sSQL & "  WHERE ramo_id = " & RamoId & vbNewLine
    
    
     Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, _
                                              True)
    
    If Not rsRecordSet.EOF Then
        If rsRecordSet(0) = 1 Then  'Vida
            ProdutoDeVida = True
        Else
            ProdutoDeVida = False   'Rural
        End If
    Else
        ProdutoDeVida = False
    End If
    
    rsRecordSet.Close
    
    Exit Function

End Function

Private Sub lblCiagTipo_Click()
    CiagMensagem
End Sub

Private Sub lblCiagValor_Click()
    CiagMensagem
End Sub




Private Function ExibeClassificacaoSinistro(ByVal sinistro_id As String)
Dim sSQL As String
Dim rsRecordSet As ADODB.Recordset
Dim resultado As Integer
Dim msg_parametro_classificacao As String
Dim nome_parametro As String
Dim exibiu_popup As String

    'escondendo checkbox e label
    chkClassificacaoSinistro.Visible = False
    chkClassificacaoSinistro.Enabled = False
    lblClassificacaoSinistro.Visible = False


    sSQL = ""
    sSQL = sSQL & " SELECT TOP 1 CP.resultado,ISNULL(TP.msg_parametro_classificacao,''), ISNULL(TP.nome,''), ISNULL(CP.enviar_relatorio,'n')  " & vbNewLine
    sSQL = sSQL & "   FROM SEGUROS_DB.DBO.SINISTRO_CLASSIFICACAO_PARAMETRO_TB CP WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "   INNER JOIN SEGUROS_DB.DBO.TP_SINISTRO_PARAMETRO_TB TP WITH (NOLOCK) " & vbNewLine
    sSQL = sSQL & "   ON CP.TP_SINISTRO_PARAMETRO_ID =  TP.TP_SINISTRO_PARAMETRO_ID  " & vbNewLine
    sSQL = sSQL & "  WHERE CP.SINISTRO_ID = " & sinistro_id & vbNewLine
    
    
     Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, _
                                              True)
    

    If Not rsRecordSet.EOF Then
                resultado = rsRecordSet(0)
                msg_parametro_classificacao = rsRecordSet(1)
                nome_parametro = rsRecordSet(2)
                exibiu_popup = UCase(rsRecordSet(3))
    
    
       If (nome_parametro = "Esteira Pagamento Imediato - Sinistros de Danos El�tricos Residenciais") _
       Or (nome_parametro = "Esteira Pagamento Imediato � Vida Individual") _
       Or (nome_parametro = "Esteira Pagamento Imediato � Vida Individual Fase 2") Then
            chkClassificacaoSinistro.Value = resultado
            chkClassificacaoSinistro.Visible = True
            lblClassificacaoSinistro.Caption = msg_parametro_classificacao
            
            'Exibir destacado para as outras fases caso o resultado seja 1
            'Caso seja Vida Fase 2 s� exibe destacado os casos onde o pop-up foi exibido
            If (nome_parametro <> "Esteira Pagamento Imediato � Vida Individual Fase 2" And resultado = 1) _
            Or (nome_parametro = "Esteira Pagamento Imediato � Vida Individual Fase 2" And exibiu_popup = "S") Then
                 lblClassificacaoSinistro.BackColor = vbYellow
                 lblClassificacaoSinistro.Visible = True
            Else
                 lblClassificacaoSinistro.Visible = True
            End If
      End If
    Else
       chkClassificacaoSinistro.Visible = False
       chkClassificacaoSinistro.Enabled = False
       lblClassificacaoSinistro.Visible = False
    End If

    
    rsRecordSet.Close
    
    Exit Function

End Function
' Cesar Santos CONFITEC - (SBRJ009952) 18/08/2020 inicio
Private Sub BuscaSinistroCardif()

    If gbllProduto_ID = 1243 Then
        btnEstimativasPagamentos_Valores_Cobertura.Enabled = False
    End If
        	
Exit Sub
    
End Sub
' Cesar Santos CONFITEC - (SBRJ009952) 18/08/2020 fim