--set nocount on exec seguros_db..busca_proposta_sinistro_sps '41416988904', 1,'LUIZ MARTINS VIEIRA', null, 11

--verifica pode avisar (funcao) de acordo com o retorno do busca_proposta_sinistro_sps



DECLARE @PRODUTO_ID INT
DECLARE @NOME_REGRA VARCHAR(40)
DECLARE @PROPOSTA_ID int
DECLARE @DT_OCORRENCIA SMALLDATETIME
DECLARE @VAL_IS NUMERIC(15, 2)

--MASSA DE TESTE (VIGENCIA DIFERENCIADA 1)
--PARAMETROS
SET @PRODUTO_ID = 11
SET @DT_OCORRENCIA =  '20200812'
--verifica_vigencia_diferenciada
--verifica_vigencia_normal
--verifica_vigencia_SEGP1285
--verifica_vigencia_assembleia
--verifica_vigencia_proposta_bb_ant
IF @PRODUTO_ID IN (11,14,722)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_diferenciada'
END
ELSE 
IF @PRODUTO_ID IN (12,121,135,716,1196,1198,1205,1208,1211,1217,1235,1236,1237)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_normal'
END
ELSE 
IF @PRODUTO_ID IN (721)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_SEGP1285'
END
ELSE 
IF @PRODUTO_ID IN (718)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_assembleia'
END
ELSE 
IF @PRODUTO_ID IN (1174,1175)
BEGIN
	SET @NOME_REGRA = 'verifica_vigencia_proposta_bb_ant'
END

--CRIACAO DE TEMPORARIA PARA VERIFICAR A VIGENCIA
IF OBJECT_ID('TEMPDB..##CLASSIFICACAO_TMP') IS NOT NULL  
       BEGIN  
        DROP TABLE ##CLASSIFICACAO_TMP  
       END  
CREATE TABLE ##CLASSIFICACAO_TMP (DEFERIDO INT , VIGENCIA_DIAS INT, DT_INICIO_VIGENCIA SMALLDATETIME, DT_FIM_VIGENCIA SMALLDATETIME, PROPOSTA_ID int 
								  ,log_classificacao VARCHAR(1000), val_is NUMERIC(15, 2))


IF OBJECT_ID('tempdb..#consultar_coberturas_sps_tmp') IS NOT NULL
		BEGIN
			DROP TABLE #consultar_coberturas_sps_tmp
		END

		CREATE TABLE #consultar_coberturas_sps_tmp (
			tp_cobertura_id INT NULL
			,nome_cobertura VARCHAR(200) NULL
			,val_is NUMERIC(15, 2) NULL
			,dt_inicio_vigencia SMALLDATETIME NULL
			,dt_fim_vigencia SMALLDATETIME NULL
			,cod_objeto_segurado INT NULL
			,texto_franquia VARCHAR(100) NULL
			)


--IF OBJECT_ID('tempdb..#classificacao_tmp') IS NOT NULL 
--BEGIN DROP TABLE #classificacao_tmp END

-- CREATE TABLE #classificacao_tmp ( ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL
 -- ,indice INT
 -- ,situacao INT
 -- ,proposta_id INT
 -- ,evento_sinistro_id INT
 -- ,tp_cobertura_id INT
 -- ,produto_id INT
 -- ,ramo_id INT
 -- ,dt_ocorrencia smalldatetime
 -- ,evento_id INT
 -- ,sinistro_id NUMERIC(11)
 -- ,fluxo_validado INT
 -- ,pagamento_imediato INT
 -- ,deferido INT
 -- ,fase INT
 -- ,vigencia_dias INT
 -- ,regra INT
 -- ,tp_componente_id INT
 -- ,sub_grupo_id INT
 -- ,val_is NUMERIC(15,2)
 -- ,dt_inicio_vigencia SMALLDATETIME
 -- ,dt_fim_vigencia SMALLDATETIME
 -- ,exibir_popup INT
 -- ,tp_sinistro_parametro_id INT
 -- ,log_classificacao VARCHAR(1000) 
 -- )


WHILE @@TRANCOUNT > 0 ROLLBACK
WHILE @@TRANCOUNT = 0 BEGIN TRAN

 

SET NOCOUNT ON 

 
 
-- Cursor para percorrer os registros
DECLARE CURSOR_VIG_DIF CURSOR FOR

SELECT TOP 20 prop.proposta_id
      FROM seguros_db.dbo.proposta_tb prop WITH(NOLOCK)  
      JOIN seguros_db.dbo.proposta_adesao_tb adesao WITH(NOLOCK)  
        ON prop.proposta_id = adesao.proposta_id  
     WHERE prop.produto_id = @PRODUTO_ID
       AND situacao = 'i'
	   AND @DT_OCORRENCIA > adesao.DT_FIM_VIGENCIA
       AND EXISTS (  
select 1 from proposta_tb proposta_tb with (nolock)
join seguros_db.dbo.escolha_plano_tb escolha_plano_tb with (nolock)      
     on escolha_plano_tb.proposta_id = proposta_tb.proposta_id
	 and escolha_plano_tb.dt_fim_vigencia is null      
    --and escolha_plano_tb.dt_escolha = (select max(dt_escolha)    
    --      from seguros_db.dbo.escolha_plano_tb escolha_plano_max with (nolock)     
    --      where escolha_plano_max.proposta_id = proposta_tb.proposta_id )    
   join seguros_db.dbo.plano_tb plano_tb with (nolock)    
     on plano_tb.produto_id = escolha_plano_tb.produto_id       
    and plano_tb.plano_id = escolha_plano_tb.plano_id          
    and plano_tb.dt_inicio_vigencia = escolha_plano_tb.dt_inicio_vigencia      
   join seguros_db.dbo.tp_cob_comp_plano_tb tp_cob_comp_plano_tb with (nolock)    
     on tp_cob_comp_plano_tb.tp_plano_id = plano_tb.tp_plano_id      
   join seguros_db.dbo.tp_cob_comp_tb tp_cob_comp_tb with (nolock)    
     on tp_cob_comp_tb.tp_cob_comp_id = tp_cob_comp_plano_tb.tp_cob_comp_id      
    and tp_cob_comp_tb.tp_componente_id = 1
	and tp_cob_comp_tb.tp_cobertura_id = 5
    and tp_cob_comp_tb.class_tp_cobertura = 'B'     
	where proposta_tb.proposta_id =  prop.proposta_id
	   )	   
	   order by dt_fim_vigencia DESC
 
--Abrindo Cursor
OPEN CURSOR_VIG_DIF
 
-- Lendo a próxima linha
FETCH NEXT FROM CURSOR_VIG_DIF INTO @PROPOSTA_ID
 
-- Percorrendo linhas do cursor (enquanto houverem)
WHILE @@FETCH_STATUS = 0
BEGIN


INSERT INTO ##CLASSIFICACAO_TMP (PROPOSTA_ID,log_classificacao) VALUES (@PROPOSTA_ID,' ')
EXEC desenv_db.DBO.SEGS14968_SPS @NOME_REGRA , @PROPOSTA_ID  , @DT_OCORRENCIA  
--SELECT @NOME_REGRA , @PROPOSTA_ID , @DT_OCORRENCIA

INSERT INTO #consultar_coberturas_sps_tmp (
					tp_cobertura_id
					,nome_cobertura
					,val_is
					,dt_inicio_vigencia
					,dt_fim_vigencia
					,cod_objeto_segurado
					,texto_franquia
					)
				EXEC seguros_db.dbo.consultar_coberturas_sps @dt_ocorrencia = @dt_ocorrencia
					,@proposta_id = @proposta_id
					,@tp_componente_id = 1
					,@sub_grupo_id = null
					,@tipo_ramo = 1

				SELECT @val_is = val_is
				FROM #consultar_coberturas_sps_tmp

				UPDATE a
				SET val_is = @val_is
				FROM ##classificacao_tmp a
				WHERE a.proposta_id = @proposta_id
 
 
-- Lendo a próxima linha
FETCH NEXT FROM CURSOR_VIG_DIF INTO @PROPOSTA_ID
END
 
-- Fechando Cursor para leitura
CLOSE CURSOR_VIG_DIF
 
-- Finalizado o cursor
DEALLOCATE CURSOR_VIG_DIF


SELECT proposta_id, @produto_id as produto_id, @dt_ocorrencia as dt_ocorrencia, val_is, deferido, vigencia_dias,dt_inicio_vigencia,dt_fim_vigencia, log_classificacao FROM ##CLASSIFICACAO_TMP (NOLOCK) where vigencia_dias > 365 and val_is < 200000.00
