VERSION 5.00
Begin VB.Form frmGerarPlanilha 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP1530 - Gerar Planilha Sinistros Deferidos no Aviso"
   ClientHeight    =   2880
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6615
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2880
   ScaleWidth      =   6615
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   465
      Left            =   4005
      TabIndex        =   2
      Top             =   2250
      Width           =   1230
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Height          =   465
      Left            =   5355
      TabIndex        =   1
      Top             =   2250
      Width           =   1185
   End
   Begin VB.Frame Frame 
      Height          =   1995
      Left            =   45
      TabIndex        =   0
      Top             =   90
      Width           =   6495
      Begin VB.Label lblProcessados 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   330
         Left            =   2025
         TabIndex        =   6
         Top             =   720
         Width           =   4335
      End
      Begin VB.Label lblArquivo 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   330
         Left            =   2025
         TabIndex        =   5
         Top             =   225
         Width           =   4335
      End
      Begin VB.Label Label 
         Caption         =   "Arquivo.............................."
         Height          =   375
         Index           =   1
         Left            =   90
         TabIndex        =   4
         Top             =   360
         Width           =   1995
      End
      Begin VB.Label Label 
         Caption         =   "Registros Processados......"
         Height          =   375
         Index           =   0
         Left            =   90
         TabIndex        =   3
         Top             =   855
         Width           =   1950
      End
   End
End
Attribute VB_Name = "frmGerarPlanilha"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim oExcel As Object
Dim oWorkbook As Object
Dim oWorkSheet As Object

Private sTipoRamo As String
Private sTipoEsteira As String

Private Sub Processar()

Dim sCaminhoArquivo As String
Dim lTotalRegistros As Long
Dim bArquivoGerado As Boolean
Dim sArquivo As String
Dim iContArquivo As Integer
Dim sArquivoApagar As String

On Error GoTo Trata_Erro

    Call InicializaParametrosExecucaoBatch(Me)

    'Obtendo a data operacional ''''''''''''''''''''''''''''''''''''''''''''
    Call ObterDataSistema("SEGBR")

    Screen.MousePointer = vbHourglass

    bArquivoGerado = False

    sCaminhoArquivo = ObterDiretorioGeracao(gsSIGLASISTEMA, _
                                            App.Title, _
                                            App.FileDescription, _
                                            glAmbiente_id)

    If Trim(sCaminhoArquivo) <> "" Then

        sArquivoApagar = Dir(sCaminhoArquivo, vbDirectory)

        Do While sArquivoApagar <> ""

            If Len(sArquivoApagar) > 10 Then

               Call DeletaArquivoEnviado(gsSIGLASISTEMA, _
                                         App.Title, _
                                         App.FileDescription, _
                                         glAmbiente_id, _
                                         sCaminhoArquivo, _
                                         sArquivoApagar)
            End If
            sArquivoApagar = Dir
        Loop

    Else

        Call MensagemBatch("N�o foi encontrada nenhuma refer�ncia ao path de " & _
                           "cria��o dessa planilha!. O programa ser� abortado.")
        Call FinalizarAplicacao

    End If

   Call GerarPlanilha(gsSIGLASISTEMA, _
                      App.Title, _
                      App.FileDescription, _
                      glAmbiente_id, _
                      sArquivo, _
                      sCaminhoArquivo, _
                      cUserName, _
                      Data_Sistema, _
                      lTotalRegistros, _
                      bArquivoGerado)


    lblProcessados.Caption = lTotalRegistros
    lblArquivo.Caption = sArquivo

    If Not bArquivoGerado Then
       Call goProducao.AdicionaLog(1, "Arquivo n�o gerado")
       Call goProducao.AdicionaLog(2, "0")
       '###########################grava log arquivo CTM
       If CTM = True Then
           Call GravaLogCTM("OK - ", "Arquivo n�o gerado", "0", "")
       End If
       '################################################
    Else

        lblProcessados.Caption = lTotalRegistros

        Call goProducao.AdicionaLog(1, sArquivo, 1)
        Call goProducao.AdicionaLog(2, lblArquivo.Caption, 1)
        Call goProducao.AdicionaLog(3, lblProcessados.Caption, 1)
        '###########################grava log arquivo CTM
        If CTM = True Then
           Call GravaLogCTM("OK - ", sArquivo, lblArquivo.Caption, lblProcessados.Caption)
        End If
        '################################################
    End If

        '#####################Pula caso executado via Control-M
    If CTM = False Then
       Call MensagemBatch("Fim de Processamento", vbInformation, , False)
       Call goProducao.Finaliza
    End If
    '#####################

    Screen.MousePointer = vbNormal

    Exit Sub

Trata_Erro:

    Call TratarErro("Processar", Me.name)
    Call FinalizarAplicacao
    
End Sub

Private Sub cmdOk_Click()

   Call Processar

End Sub

Private Sub cmdSair_Click()

   Call FinalizarAplicacao

End Sub

Public Sub DeletaArquivoEnviado(ByVal sSiglaSistema As String, _
                                ByVal sSiglaRecurso As String, _
                                ByVal sDescricaoRecurso As String, _
                                ByVal iAmbienteId As Integer, _
                                ByRef sCaminho As String, _
                                ByRef sArquivo As String)

Dim sSQL As String
Dim sCaminhoGeracao As String
Dim rsEnviado As Recordset
Dim lconexao As Long

On Error GoTo TrataErro

    sCaminhoGeracao = IIf(Right(sCaminho, 1) = "\", sCaminho, sCaminho & "\")
    
    sSQL = ""
    sSQL = sSQL & " SELECT anexo "
    sSQL = sSQL & "   FROM email_db..log_email_tb WITH(NOLOCK)"
    sSQL = sSQL & "  WHERE sigla_sistema = 'ITE' "
    sSQL = sSQL & "    AND anexo like '%" & Replace(sArquivo, ".xls", "") & "%'"

    Set rsEnviado = ExecutarSQL(sSiglaSistema, _
                             iAmbienteId, _
                             sSiglaRecurso, _
                             sDescricaoRecurso, _
                             sSQL, _
                             True)


    If rsEnviado.EOF = False Then
        Call Kill(sCaminhoGeracao & sArquivo)
    End If

    rsEnviado.Close
    Set rsEnviado = Nothing
    
    Exit Sub

TrataErro:
        Call Err.Raise(Err.Number, , "SEGP1530.DeletaArquivoEnviado - " & Err.Description)
    

End Sub

Public Sub GerarPlanilha(ByVal sSiglaSistema As String, _
                              ByVal sSiglaRecurso As String, _
                              ByVal sDescricaoRecurso As String, _
                              ByVal iAmbienteId As Integer, _
                              ByRef sArquivo As String, _
                              ByRef sCaminho As String, _
                              ByRef sUsuario As String, _
                              ByRef sDataSistema As String, _
                              ByRef lTotalRegistros As Long, _
                              ByRef bArquivoGerado As Boolean)

Dim sSQL As String, sSQLAux As String
Dim rsRegistros As Recordset
Dim lconexao As Long
Dim iLinha As Integer
Dim totalBenf As Integer

Dim sChave As String
Dim sDestinatarios As String
Dim sAssunto As String
Dim sMensagem As String
Dim sDest2 As String
Dim sDtProcessamento As String
Dim sColunasBeneficiarios(1 To 10) As String


On Error GoTo TratarErro

    sCaminho = IIf(Right(sCaminho, 1) = "\", sCaminho, sCaminho & "\")

    lconexao = AbrirConexao(sSiglaSistema, _
                            iAmbienteId, _
                            sSiglaRecurso, _
                            sDescricaoRecurso)

    'Abrindo Transacao''''''''''''''''''''''''''''''''''''''''
    If Not AbrirTransacao(lconexao) Then
        Call Err.Raise(vbObjectError + 1000, , "SEGP1530.GerarPlanilha " & sTipoEsteira & " - N�o foi poss�vel abrir transa��o")
    End If

    sColunasBeneficiarios(1) = "Q"
    sColunasBeneficiarios(2) = "R"
    sColunasBeneficiarios(3) = "S"
    sColunasBeneficiarios(4) = "T"
    sColunasBeneficiarios(5) = "U"
    sColunasBeneficiarios(6) = "V"
    sColunasBeneficiarios(7) = "W"
    sColunasBeneficiarios(8) = "X"
    sColunasBeneficiarios(9) = "Y"
    sColunasBeneficiarios(10) = "Z"

    sDtProcessamento = Format(Now, "dd/mm/yyyy")
    sArquivo = "Extra��o Deferimento em Tela � Vida " & Format(Now, "dd-mm-yy") & ".xls"
    sDest2 = ""
    sChave = "AVISO123"

    sAssunto = "Extra��o Deferimento em Tela � Vida " & sDtProcessamento
    sMensagem = "Prezados Senhores," & Chr(13) & Chr(13) & _
                "Encaminhamos a planilha """ & sArquivo + """ em anexo, contendo os avisos classificados como deferidos. " & _
                "Eles foram classificados de acordo com as regras definidas." & Chr(13) & Chr(13) & _
                "Atenciosamente," & Chr(13) & Chr(13) & "Central de Avisos."
    sDestinatarios = ObterDestinatariosVida(sSiglaSistema, _
                                            sSiglaRecurso, _
                                            sDescricaoRecurso, _
                                            iAmbienteId)
    
    sSQL = "EXEC seguros_db.dbo.SEGS14736_SPS " & sTipoRamo

    Set rsRegistros = Conexao_ExecutarSQL(sSiglaSistema, _
                                          iAmbienteId, _
                                          sSiglaRecurso, _
                                          sDescricaoRecurso, _
                                          sSQL, _
                                          lconexao, _
                                          True)
                                          
    totalBenf = rsRegistros("colunas")

    lTotalRegistros = 0
    
    If totalBenf > 0 Then
       iLinha = 1
       
       Set rsRegistros = rsRegistros.NextRecordset
   
       If Not rsRegistros.EOF Then
   
           Set oExcel = CreateObject("Excel.Application")
           oExcel.Visible = False
           
           Set oWorkbook = oExcel.WorkBooks.Add
   
           Set oWorkSheet = oWorkbook.Sheets(1)
   
           'ObjSheet.Cells(1, 1).HorizontalAlignment = -4152 'Right
   
           oWorkSheet.Cells(1, "A").Value = "No do Sinistro"
           
           oWorkSheet.Cells(1, "B").Value = "Situa��o"
           
           oWorkSheet.Cells(1, "C").Value = "Data de Ocorr�ncia"
   
           oWorkSheet.Cells(1, "D").Value = "Data do Aviso"
   
           oWorkSheet.Cells(1, "E").Value = "CPF Sinistrado"
           
           oWorkSheet.Cells(1, "F").Value = "Nome Sinistrado"
   
           oWorkSheet.Cells(1, "G").Value = "Estimativa"
   
           oWorkSheet.Cells(1, "H").Value = "Qtd de Sinistros"
           
           oWorkSheet.Cells(1, "I").Value = "Proposta ID"
   
           oWorkSheet.Cells(1, "J").Value = "Produto"
   
           oWorkSheet.Cells(1, "K").Value = "Tipo de Plano Contratado"
           
           oWorkSheet.Cells(1, "L").Value = "Evento"
   
           oWorkSheet.Cells(1, "M").Value = "Prazo Total"
   
           oWorkSheet.Cells(1, "N").Value = "DDD Comunicante"
   
           oWorkSheet.Cells(1, "O").Value = "Telefone Comunicante"
   
           oWorkSheet.Cells(1, "P").Value = "Nome Comunicante"
           
           For i = 1 To totalBenf
               oWorkSheet.Cells(1, sColunasBeneficiarios(i)).Value = "Benefici�rio " + Str(i)
           Next
   
           Do While Not rsRegistros.EOF
   
               iLinha = iLinha + 1
   
               oWorkSheet.Cells(iLinha, "A").Value = rsRegistros("sinistro_id")
               oWorkSheet.Cells(iLinha, "A").HorizontalAlignment = -4131 'Left
               oWorkSheet.Cells(iLinha, "A").ColumnWidth = 15
   
               oWorkSheet.Cells(iLinha, "B").Value = rsRegistros("situacao")
               oWorkSheet.Cells(iLinha, "B").HorizontalAlignment = -4131 'Left
               oWorkSheet.Cells(iLinha, "B").ColumnWidth = 30
   
               oWorkSheet.Cells(iLinha, "C").NumberFormat = "dd/mm/yyyy"
               oWorkSheet.Cells(iLinha, "C").Value = rsRegistros("dt_ocorrencia_sinistro")
               oWorkSheet.Cells(iLinha, "C").HorizontalAlignment = -4108 'Center
               oWorkSheet.Cells(iLinha, "C").ColumnWidth = 18
   
   
               oWorkSheet.Cells(iLinha, "D").NumberFormat = "dd/mm/yyyy"
               oWorkSheet.Cells(iLinha, "D").Value = rsRegistros("dt_aviso_sinistro")
               oWorkSheet.Cells(iLinha, "D").HorizontalAlignment = -4108 'Center
               oWorkSheet.Cells(iLinha, "D").ColumnWidth = 14
      
               oWorkSheet.Cells(iLinha, "E").Value = rsRegistros("cpf_sinistrado")
               oWorkSheet.Cells(iLinha, "E").HorizontalAlignment = -4131 'Left
               oWorkSheet.Cells(iLinha, "E").ColumnWidth = 15
               
               oWorkSheet.Cells(iLinha, "F").Value = rsRegistros("nome_sinistrado")
               oWorkSheet.Cells(iLinha, "F").HorizontalAlignment = -4131 'Left
               oWorkSheet.Cells(iLinha, "F").ColumnWidth = 40
   
               oWorkSheet.Cells(iLinha, "G").NumberFormat = "_(* #,##0.00_);_(* (#,##0.00);_(* ""-""??_);_(@_)"
               oWorkSheet.Cells(iLinha, "G").Value = Trim(rsRegistros("valor_estimativa"))
               oWorkSheet.Cells(iLinha, "G").HorizontalAlignment = -4108 'Center
               oWorkSheet.Cells(iLinha, "G").ColumnWidth = 15
   
               oWorkSheet.Cells(iLinha, "H").Value = rsRegistros("qtd_sinistros")
               oWorkSheet.Cells(iLinha, "H").HorizontalAlignment = -4108 'Center
               oWorkSheet.Cells(iLinha, "H").ColumnWidth = 10
               
               oWorkSheet.Cells(iLinha, "I").Value = rsRegistros("proposta_id")
               oWorkSheet.Cells(iLinha, "I").HorizontalAlignment = -4131 'Left
               oWorkSheet.Cells(iLinha, "I").ColumnWidth = 15
   
               oWorkSheet.Cells(iLinha, "J").Value = rsRegistros("produto")
               oWorkSheet.Cells(iLinha, "J").HorizontalAlignment = -4131 'Left
               oWorkSheet.Cells(iLinha, "J").ColumnWidth = 40
   
               oWorkSheet.Cells(iLinha, "K").Value = rsRegistros("plano")
               oWorkSheet.Cells(iLinha, "K").HorizontalAlignment = -4131 'Left
               oWorkSheet.Cells(iLinha, "K").ColumnWidth = 40
   
               oWorkSheet.Cells(iLinha, "L").Value = rsRegistros("evento")
               oWorkSheet.Cells(iLinha, "L").HorizontalAlignment = -4131 'Left
               oWorkSheet.Cells(iLinha, "L").ColumnWidth = 40
   
               oWorkSheet.Cells(iLinha, "M").Value = rsRegistros("prazo_total")
               oWorkSheet.Cells(iLinha, "M").HorizontalAlignment = -4108 'Center
               oWorkSheet.Cells(iLinha, "M").ColumnWidth = 10
   
               oWorkSheet.Cells(iLinha, "N").Value = rsRegistros("ddd_solicitante")
               oWorkSheet.Cells(iLinha, "N").HorizontalAlignment = -4108 'Center
               oWorkSheet.Cells(iLinha, "N").ColumnWidth = 10
   
               oWorkSheet.Cells(iLinha, "O").Value = rsRegistros("tel_solicitante")
               oWorkSheet.Cells(iLinha, "O").HorizontalAlignment = -4108 'Center
               oWorkSheet.Cells(iLinha, "O").ColumnWidth = 12
   
               oWorkSheet.Cells(iLinha, "P").Value = rsRegistros("nome_solicitante")
               oWorkSheet.Cells(iLinha, "P").HorizontalAlignment = -4131 'Left
               oWorkSheet.Cells(iLinha, "P").ColumnWidth = 40
               
               For i = 1 To totalBenf
                  oWorkSheet.Cells(iLinha, sColunasBeneficiarios(i)).Value = rsRegistros("beneficiario_" + Trim(Str(i)))
                  oWorkSheet.Cells(iLinha, sColunasBeneficiarios(i)).HorizontalAlignment = -4131 'Left
                  oWorkSheet.Cells(iLinha, sColunasBeneficiarios(i)).ColumnWidth = 50
               Next
   
               lTotalRegistros = lTotalRegistros + 1
   
                If rsRegistros("sinistro_id") > 0 Then
                  sSQLAux = "EXEC seguros_db.dbo.SEGS14737_SPU " & rsRegistros("sinistro_id")
   
                  Call Conexao_ExecutarSQL(sSiglaSistema, _
                                           iAmbienteId, _
                                           sSiglaRecurso, _
                                           sDescricaoRecurso, _
                                           sSQLAux, _
                                           lconexao, _
                                           False)
               End If
   
               rsRegistros.MoveNext
   
           Loop
   
           Call FecharPlanilha(sCaminho + sArquivo)
   
   
       End If
    End If
    
    Set rsRegistros = Nothing

    If lTotalRegistros > 0 Then

       bArquivoGerado = True

       sSQL = ""

       sSQL = sSQL & " exec email_db..SGSS0782_SPI "
       sSQL = sSQL & " 'ITEW0105' "                         '@sigla_recurso varchar(20), -- sigla do recurso que est� enviando o e-mail
       sSQL = sSQL & " , 'ITE' "                            '@sigla_sistema varchar(20), -- sigla do sistema de onde ser� enviado o e-mail
       sSQL = sSQL & " , 'ITEW0105' "                       '@chave varchar(32), -- chave, utilizada como um tipo de observa��o
       sSQL = sSQL & " , 'alianca@aliancadobrasil.com.br' " '@de varchar(60), -- remetente
       sSQL = sSQL & " , '" & sDestinatarios & "' "         '@para varchar(500), -- destinat�rio
       sSQL = sSQL & " , '" & sAssunto & "' "               '@assunto varchar(250), -- assunto do e-mail
       sSQL = sSQL & " , '" & sMensagem & "' "              '@mensagem varchar(7500), --mensagem html ou texto
       sSQL = sSQL & " , '" & sDest2 & "' "                 '@cc varchar(500), --emails separados por virgula ou ponto-virgula
       sSQL = sSQL & " , '" & sCaminho + sArquivo & "' "    '@anexo varchar(500), --Caminho onde se encotra o(s) arquivo (s), separado por @'
       sSQL = sSQL & " , 1 "                                '@formato varchar(50), --1 - TEXTO ou 2 - HTML'
       sSQL = sSQL & " , 'Autom�tica' "                     '@usuario ud_usuario -- usuario que est� enviando o e-mail"

         Call Conexao_ExecutarSQL(sSiglaSistema, _
                                  iAmbienteId, _
                                  sSiglaRecurso, _
                                  sDescricaoRecurso, _
                                  sSQL, _
                                  lconexao, _
                                  False)

        'Fechando Transacao
        If Not ConfirmarTransacao(lconexao) Then
            Call Err.Raise(vbObjectError + 1000, , "SEGP1530.GerarPlanilha " & sTipoEsteira & " - N�o foi poss�vel confirmar transa��o")
        End If

        If Not FecharConexao(lconexao) Then
            Call Err.Raise(vbObjectError + 10000, , "SEGP1530.GerarPlanilha " & sTipoEsteira & " - N�o foi poss�vel fechar conex�o")
        End If

    Else

        'Rollback
        If Not RetornarTransacao(lconexao) Then
            Call Err.Raise(vbObjectError + 1000, , "SEGP1530.GerarPlanilha " & sTipoEsteira & " - N�o foi poss�vel confirmar transa��o")
        End If

        If Not FecharConexao(lconexao) Then
            Call Err.Raise(vbObjectError + 10000, , "SEGP1530.GerarPlanilha " & sTipoEsteira & " - N�o foi poss�vel fechar conex�o")
        End If

        lTotalRegistros = 0
        bArquivoGerado = False

    End If

    Exit Sub

TratarErro:
    Call Err.Raise(Err.Number, , "SEGP1530.GerarPlanilha " & sTipoEsteira & " " & Err.Description)

End Sub

Public Function ObterDestinatariosVida(ByVal sSiglaSistema As String, _
                                  ByVal sSiglaRecurso As String, _
                                  ByVal sDescricaoRecurso As String, _
                                  ByVal iAmbienteId As Integer) As String

Dim sSQL As String
Dim rsEmail As Recordset
On Error GoTo Trata_Erro

    sSQL = ""
    sSQL = sSQL & "EXEC seguros_db.dbo.SEGS14736_SPS 0, '" & sTipoEsteira & "'"
 
    
    Set rsEmail = ExecutarSQL(sSiglaSistema, _
                              iAmbienteId, _
                              sSiglaRecurso, _
                              sDescricaoRecurso, _
                              sSQL, _
                              True)
                                             
    If Not rsEmail.EOF Then
      ObterDestinatariosVida = rsEmail(0)
    End If
    
    rsEmail.Close
    Set rsEmail = Nothing
                                             
Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGP1530.ObterDestinatarios - " & Err.Description)

 End Function

Public Sub FecharPlanilha(ByRef sCaminho As String)
    
On Error GoTo TrataErro

   oWorkbook.SaveAs sCaminho
   oWorkbook.Application.Quit
   oExcel.Quit
   
   Set oExcel = Nothing
   Set oWorkbook = Nothing
   Set oWorkSheet = Nothing
    
'    ObjBook.SaveAs sCaminho, xlExcel7
'
'    ObjBook.Application.Quit
'
'    Set ObjSheet = Nothing
'    Set ObjBook = Nothing
'    Set Plani = Nothing
    
    Exit Sub

TrataErro:
        Call Err.Raise(Err.Number, , "SEGP1530.FecharPlanilha - " & Err.Description)
    
End Sub

Private Sub Form_Load()

On Error GoTo Trata_Erro

    'Verificando permiss�o de acesso � aplica��o / pula caso seja executado via CTM
    If Verifica_Origem_ControlM(Command) = False Then
      If Not Trata_Parametros(Command) Then
          Call FinalizarAplicacao
      End If
    End If
    
    'Temporariamente retirado a extra��o dos parametros recebidos para funcionar no CONTROLM
    'InicializaVariaveisLocal
    sTipoRamo = 1
    sTipoEsteira = "Esteira Pagamento Imediato � Vida Individual"
    
    If Obtem_agenda_diaria_id(Command) > 0 Or Verifica_Origem_ControlM(Command) = True Then
        Me.Show
        Call cmdOk_Click
        Call cmdSair_Click
    End If

    Exit Sub

Trata_Erro:

    Call TratarErro("Form_Load", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub InicializaVariaveisLocal()
  Dim aParametros() As String
  Dim sParametroLocal As String
  Dim sEsteira As String

   Ambiente = ConvAmbiente(glAmbiente_id)
   sParametroLocal = Right(Command, InStrRev(Command, " "))
   aParametros = Split(sParametroLocal, " ")
   
   sTipoRamo = aParametros(4)
   sEsteira = aParametros(5)
   
   If sEsteira = """1""" Then
       sTipoEsteira = "Esteira Pagamento Imediato - Sinistros de Danos El�tricos Residenciais"
   ElseIf sEsteira = """2""" Then
       sTipoEsteira = "Esteira Pagamento Imediato � Vida Individual"
   Else
       sTipoEsteira = ""
   End If
  
End Sub

Public Function ObterDiretorioGeracao(ByVal sSiglaSistema As String, _
                                      ByVal sSiglaRecurso As String, _
                                      ByVal sDescricaoRecurso As String, _
                                      ByVal iAmbienteId As Integer) As String

Dim sSQL As String
Dim rsPath As Recordset
On Error GoTo Trata_Erro

    sSQL = "       SELECT tp_sinistro_parametro_tb.path_geracao_rel_sinistros "
    sSQL = sSQL & "  FROM seguros_db.dbo.tp_sinistro_parametro_tb tp_sinistro_parametro_tb WITH (NOLOCK) "
    sSQL = sSQL & " WHERE tp_sinistro_parametro_tb.nome = '" & sTipoEsteira & "' "
 
    
    Set rsPath = ExecutarSQL(sSiglaSistema, _
                              iAmbienteId, _
                              sSiglaRecurso, _
                              sDescricaoRecurso, _
                              sSQL, _
                              True)
                                             
    If Not rsPath.EOF Then
      ObterDiretorioGeracao = rsPath(0)
    End If
    
    rsPath.Close
    Set rsPath = Nothing
                                             
Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGP1530.ObterDiretorioGeracao - " & Err.Description)

 End Function

