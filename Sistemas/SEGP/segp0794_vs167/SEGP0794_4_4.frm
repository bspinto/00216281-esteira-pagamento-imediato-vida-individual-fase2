VERSION 5.00
Begin VB.Form SEGP0794_4_4 
   Caption         =   "Lista de Bancos"
   ClientHeight    =   3015
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4695
   LinkTopic       =   "Form1"
   ScaleHeight     =   3015
   ScaleWidth      =   4695
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstBancos 
      Height          =   2790
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "SEGP0794_4_4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub lstBancos_DblClick()

    SEGP0794_4_2.mskBanco.Text = Format(SEGP0794_4_4.lstBancos.ItemData(lstBancos.ListIndex), "000")
    Unload Me
End Sub

Private Sub lstBancos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SEGP0794_4_2.mskBanco.Text = Format(SEGP0794_4_4.lstBancos.ItemData(lstBancos.ListIndex), "000")
        Unload Me
    End If
End Sub
