VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BenefSinistro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarNome As String 'local copy
Private mvarTipo As String 'local copy
Private mvardt_Nascimento As String 'local copy
Private mvartp_responsavel As String 'local copy
Private mvarNome_responsavel As String 'local copy
Private mvartp_Documento As String 'local copy
Private mvarNome_Documento As String 'local copy
Private mvarOrgao As String 'local copy
Private mvarNumero As String 'local copy
Private mvarSerie As String 'local copy
Private mvarComplemento As String 'local copy
Private mvarBanco As String 'local copy
Private mvarAgencia As String 'local copy
Private mvarAgencia_dv As String 'local copy
Private mvarConta As String 'local copy
Private mvarConta_dv As String 'local copy
Private mvarPoupanca As String 'local copy
Private mvarUsuario As String 'local copy
Private mvarSeq As Integer 'local copy
Private mvarBeneficiario_id As String 'local copy
Private mvartp_cadastro As String 'local copy
Private mvarCadastro_id As String 'local copy
Private mvarSexo As String 'local copy
Private mvarCGC As String 'local copy
Private mvarCPF As String 'local copy
'local variable(s) to hold property value(s)
Private mvarsucursal_seguradora_id As String 'local copy
Private mvarseguradora_cod_susep As String 'local copy
'local variable(s) to hold property value(s)
Private mvarCong_Sucursal_Seguradora_id As String 'local copy
Private mvarCong_Cod_Susep As String 'local copy

'Sergey Souza - 06/04/2010
Private mvarContaPoup As String 'local copy
Private mvarContaPoup_dv As String 'local copy
'================================

''Gerson CWI
Private mvarSinistroBB As String
Private mvarSinistroAB As String
''

Public Property Let Cong_Cod_Susep(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Cong_Cod_Susep = 5
    mvarCong_Cod_Susep = vData
End Property

Public Property Get Cong_Cod_Susep() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Cong_Cod_Susep
    Cong_Cod_Susep = mvarCong_Cod_Susep
End Property

Public Property Let Cong_Sucursal_Seguradora_id(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Cong_Sucursal_Seguradora_id = 5
    mvarCong_Sucursal_Seguradora_id = vData
End Property

Public Property Get Cong_Sucursal_Seguradora_id() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Cong_Sucursal_Seguradora_id
    Cong_Sucursal_Seguradora_id = mvarCong_Sucursal_Seguradora_id
End Property

Public Property Let CPF(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Cpf = 5
    mvarCPF = vData
End Property

Public Property Get CPF() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Cpf
    CPF = mvarCPF
End Property

Public Property Let CGC(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Cgc = 5
    mvarCGC = vData
End Property

Public Property Get CGC() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Cgc
    CGC = mvarCGC
End Property

Public Property Let Sexo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Sexo = 5
    mvarSexo = vData
End Property

Public Property Get Sexo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Sexo
    Sexo = mvarSexo
End Property

Public Property Let Cadastro_id(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Cadastro_id = 5
    mvarCadastro_id = vData
End Property

Public Property Get Cadastro_id() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Cadastro_id
    Cadastro_id = mvarCadastro_id
End Property

Public Property Let tp_cadastro(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.tp_cadastro = 5
    mvartp_cadastro = vData
End Property

Public Property Get tp_cadastro() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.tp_cadastro
    tp_cadastro = mvartp_cadastro
End Property

Public Property Let Beneficiario_id(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Beneficiario_id = 5
    mvarBeneficiario_id = vData
End Property

Public Property Get Beneficiario_id() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Beneficiario_id
    Beneficiario_id = mvarBeneficiario_id
End Property

Public Property Let Seq(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Seq = 5
    mvarSeq = vData
End Property

Public Property Get Seq() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Seq
    Seq = mvarSeq
End Property

Public Property Let Usuario(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Usuario = 5
    mvarUsuario = vData
End Property

Public Property Get Usuario() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Usuario
    Usuario = mvarUsuario
End Property

Public Property Let Conta_dv(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Conta_dv = 5
    mvarConta_dv = vData
End Property

Public Property Get Conta_dv() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Conta_dv
    Conta_dv = mvarConta_dv
End Property

Public Property Let Conta(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Conta = 5
    mvarConta = vData
End Property

Public Property Get Conta() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Conta
    Conta = mvarConta
End Property

Public Property Let Agencia_dv(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Agencia_dv = 5
    mvarAgencia_dv = vData
End Property

Public Property Get Agencia_dv() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Agencia_dv
    Agencia_dv = mvarAgencia_dv
End Property

Public Property Let Agencia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Agencia = 5
    mvarAgencia = vData
End Property

Public Property Get Agencia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Agencia
    Agencia = mvarAgencia
End Property

Public Property Let Banco(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Banco = 5
    mvarBanco = vData
End Property

Public Property Get Banco() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Banco
    Banco = mvarBanco
End Property

Public Property Let Complemento(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Complemento = 5
    mvarComplemento = vData
End Property

Public Property Get Complemento() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Complemento
    Complemento = mvarComplemento
End Property

Public Property Let Serie(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Serie = 5
    mvarSerie = vData
End Property

Public Property Get Serie() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Serie
    Serie = mvarSerie
End Property

Public Property Let numero(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Numero = 5
    mvarNumero = vData
End Property

Public Property Get numero() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Numero
    numero = mvarNumero
End Property

Public Property Let Orgao(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Orgao = 5
    mvarOrgao = vData
End Property

Public Property Get Orgao() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Orgao
    Orgao = mvarOrgao
End Property

Public Property Let Nome_Documento(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome_Documento = 5
    mvarNome_Documento = vData
End Property

Public Property Get Nome_Documento() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome_Documento
    Nome_Documento = mvarNome_Documento
End Property

Public Property Let tp_Documento(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.tp_Documento = 5
    mvartp_Documento = vData
End Property

Public Property Get tp_Documento() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.tp_Documento
    tp_Documento = mvartp_Documento
End Property

Public Property Let Nome_Responsavel(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome_responsavel = 5
    mvarNome_responsavel = vData
End Property

Public Property Get Nome_Responsavel() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome_responsavel
    Nome_Responsavel = mvarNome_responsavel
End Property

Public Property Let tp_Responsavel(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.tp_responsavel = 5
    mvartp_responsavel = vData
End Property

Public Property Get tp_Responsavel() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.tp_responsavel
    tp_Responsavel = mvartp_responsavel
End Property

Public Property Let dt_Nascimento(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.dt_Nascimento = 5
    mvardt_Nascimento = vData
End Property

Public Property Get dt_Nascimento() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.dt_Nascimento
    dt_Nascimento = mvardt_Nascimento
End Property

Public Property Let Tipo(ByVal vData As String)
Attribute Tipo.VB_Description = "N=Novo, E=Existente"
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Tipo = 5
    mvarTipo = vData
End Property

Public Property Get Tipo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Tipo
    Tipo = mvarTipo
End Property

Public Property Let Nome(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nome = 5
    mvarNome = vData
End Property

Public Property Get Nome() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nome
    Nome = mvarNome
End Property

Public Property Let Poupanca(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Conta = 5
    mvarPoupanca = vData
End Property

Public Property Get Poupanca() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Conta
    Poupanca = mvarPoupanca
End Property

'===== Sergey Souza 06/04/2010

Public Property Let ContaPoup(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Conta = 5
    mvarContaPoup = vData
End Property

'===== Sergey Souza 06/04/2010

Public Property Get ContaPoup() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Conta
    ContaPoup = mvarContaPoup
End Property

'===== Sergey Souza 06/04/2010

Public Property Let ContaPoup_dv(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Conta = 5
    mvarContaPoup_dv = vData
End Property

'===== Sergey Souza 06/04/2010

Public Property Get ContaPoup_dv() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Conta
    'Sergey Souza - 28/05/2010
    ContaPoup_dv = mvarContaPoup_dv
    'ContaPoup_dv = mvarContaPoup
End Property

''Gerson - CWI
Public Property Get SinistroBB() As String
    SinistroBB = mvarSinistroBB
End Property

Public Property Let SinistroBB(ByVal vNewValue As String)
    mvarSinistroBB = vNewValue
End Property

Public Property Get SinistroAB() As String
    SinistroAB = mvarSinistroAB
End Property

Public Property Let SinistroAB(ByVal vNewValue As String)
    mvarSinistroAB = vNewValue
End Property


''

