VERSION 5.00
Begin VB.Form SEGP0794_1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0794 - Aviso de Sinistro pela CA"
   ClientHeight    =   1845
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4620
   Icon            =   "SEGP0794_1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1845
   ScaleWidth      =   4620
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   3195
      TabIndex        =   4
      Top             =   1350
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   1800
      TabIndex        =   3
      Top             =   1350
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tipo de Ramo"
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   135
      TabIndex        =   0
      Top             =   180
      Width           =   4335
      Begin VB.OptionButton optTipoRamo 
         Caption         =   "Danos"
         Height          =   195
         Index           =   1
         Left            =   2640
         TabIndex        =   2
         Top             =   540
         Width           =   1275
      End
      Begin VB.OptionButton optTipoRamo 
         Caption         =   "Pessoas"
         Height          =   195
         Index           =   0
         Left            =   525
         TabIndex        =   1
         Top             =   540
         Value           =   -1  'True
         Width           =   1170
      End
   End
End
Attribute VB_Name = "SEGP0794_1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    CentraFrm Me
    SEGP0794_4.CLIENTE_DOC = ""
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub cmdContinuar_Click()
    If Me.optTipoRamo(0).Value = True Then
        TIPO_RAMO = 1
    Else
        TIPO_RAMO = 2
        Shell "SEGP0862.exe " & Command
        Unload Me
        Exit Sub
    End If
    Me.Hide
    SEGP0794_2.Show
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Sair Me
End Sub

