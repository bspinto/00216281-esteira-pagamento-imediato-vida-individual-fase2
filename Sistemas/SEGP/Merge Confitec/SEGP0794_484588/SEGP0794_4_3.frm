VERSION 5.00
Begin VB.Form SEGP0794_4_3 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Copiar Beneficiários"
   ClientHeight    =   4395
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7935
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4395
   ScaleWidth      =   7935
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox ListCPF 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   360
      MultiSelect     =   2  'Extended
      TabIndex        =   4
      Top             =   3840
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   6240
      TabIndex        =   3
      Top             =   3960
      Width           =   1575
   End
   Begin VB.CommandButton cmdCopiar 
      Caption         =   "Copiar"
      Height          =   315
      Left            =   4560
      TabIndex        =   2
      Top             =   3960
      Width           =   1575
   End
   Begin VB.ListBox lstBenef 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3420
      Left            =   120
      MultiSelect     =   2  'Extended
      TabIndex        =   0
      Top             =   360
      Width           =   7695
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Beneficiários"
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   900
   End
End
Attribute VB_Name = "SEGP0794_4_3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private vSinistroAB As String
Private vSinistroBB As String

Public Property Let SinistroBB(ByVal vNewValue As Variant)
    vSinistroBB = vNewValue
End Property

Public Property Let SinistroAB(ByVal vNewValue As Variant)
    vSinistroAB = vNewValue
End Property

Public Sub PreencherLstBenef()

    Dim benef As BenefSinistro
    Dim strBenef As String
    Dim i As Integer
    Dim flagFind As Boolean
    Dim cpfBenef As String
    

    
    lstBenef.Clear
    If vSinistroBB = "-1" Then Exit Sub
    
    For Each benef In SEGP0794_4_2.BeneficiariosSinistro
        If benef.SinistroBB <> vSinistroBB Then
            strBenef = Format(benef.Seq, "000") & " - "
            strBenef = strBenef & Format(benef.SinistroBB, "0000000000000") & " - "
            strBenef = strBenef & benef.Nome
            cpfBenef = benef.CPF
            flagFind = False
            For i = 0 To ListCPF.ListCount - 1
                If cpfBenef = ListCPF.List(i) Then
                    flagFind = True
                    Exit For
                End If
            Next
            If flagFind = False Then
                lstBenef.AddItem (strBenef)
                ListCPF.AddItem (cpfBenef)
            End If
        End If
    Next
    
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdCopiar_Click()
    Dim i As Integer
    Dim Id As String
    
    If lstBenef.SelCount = 0 Then
        MsgBox "Nenhum Beneficiário selecionado!", vbInformation, Me.Caption
        Exit Sub
    End If
       
    For i = 0 To lstBenef.ListCount - 1
        If lstBenef.Selected(i) = True Then
            Id = lstBenef.List(i)
            Id = Left(Id, 3)
            Id = CStr(CLng(Id))
            Copiar Id
        End If
    Next
    Unload Me
End Sub

Private Sub Copiar(Id As String)
    
    Dim benef As BenefSinistro
    Dim nBenef As BenefSinistro
    Dim vSeq As Integer
        
    If vSinistroBB = "-1" Then Exit Sub
    
    For Each benef In SEGP0794_4_2.BeneficiariosSinistro
        If CStr(benef.Seq) = Id Then Exit For
    Next
    
    If SEGP0794_4_2.BeneficiariosSinistro.Count > 0 Then
        vSeq = SEGP0794_4_2.BeneficiariosSinistro.Count + 1
    Else
        vSeq = 1
    End If
    
    Set nBenef = New BenefSinistro
    With nBenef
        .Agencia = benef.Agencia
        .Agencia_dv = benef.Agencia_dv
        .Banco = benef.Banco
        .Beneficiario_id = benef.Beneficiario_id
        .Cadastro_id = benef.Cadastro_id
        .CGC = benef.CGC
        .Complemento = benef.Complemento
        .Cong_Cod_Susep = benef.Cong_Cod_Susep
        .Cong_Sucursal_Seguradora_id = benef.Cong_Sucursal_Seguradora_id
        .Conta = benef.Conta
        .Conta_dv = benef.Conta_dv
        .ContaPoup = benef.ContaPoup
        .ContaPoup_dv = benef.ContaPoup_dv
        .CPF = benef.CPF
        .dt_Nascimento = benef.dt_Nascimento
        .Nome = benef.Nome
        .Nome_Documento = benef.Nome_Documento
        .Nome_Responsavel = benef.Nome_Responsavel
        .numero = benef.numero
        .Orgao = benef.Orgao
        .Poupanca = benef.Poupanca
        .Seq = vSeq
        .Serie = benef.Serie
        .Sexo = benef.Sexo
        .SinistroBB = vSinistroBB
        .SinistroAB = vSinistroAB
        .Tipo = benef.Tipo
        .tp_cadastro = benef.tp_cadastro
        .tp_Documento = benef.tp_Documento
        .tp_Responsavel = benef.tp_Responsavel
        .Usuario = benef.Usuario
    End With
    SEGP0794_4_2.BeneficiariosSinistro.Add nBenef
       
    'Unload Me
End Sub

