VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form SEGP0794_3 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0794 - Aviso de Sinistro pela CA"
   ClientHeight    =   7845
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8925
   Icon            =   "SEGP0794_3.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7845
   ScaleWidth      =   8925
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cmbNomeSinistrado 
      Height          =   315
      Left            =   240
      TabIndex        =   34
      Text            =   "Combo1"
      Top             =   600
      Width           =   7095
   End
   Begin VB.Frame Frame5 
      Caption         =   "Ocorr�ncia"
      Height          =   2295
      Left            =   120
      TabIndex        =   27
      Top             =   1680
      Width           =   8760
      Begin VB.ComboBox cboSubEvento 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   1800
         Width           =   8595
      End
      Begin VB.TextBox txtLocal 
         Height          =   330
         Left            =   2880
         MaxLength       =   70
         TabIndex        =   6
         Top             =   465
         Width           =   5820
      End
      Begin MSMask.MaskEdBox mskDataOcorrencia 
         Height          =   330
         Left            =   90
         TabIndex        =   4
         Top             =   495
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.ComboBox cboEvento 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1125
         Width           =   8580
      End
      Begin MSMask.MaskEdBox mskHora 
         Height          =   330
         Left            =   1800
         TabIndex        =   5
         Top             =   480
         Width           =   900
         _ExtentX        =   1588
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   5
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##:##"
         PromptChar      =   "_"
      End
      Begin VB.Label Label5 
         Caption         =   "Sub Evento"
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   1560
         Width           =   1095
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Local "
         Height          =   195
         Index           =   3
         Left            =   2880
         TabIndex        =   32
         Top             =   240
         Width           =   435
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Hora"
         Height          =   195
         Index           =   2
         Left            =   1830
         TabIndex        =   31
         Top             =   240
         Width           =   345
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Data da Ocorr�ncia"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   29
         Top             =   270
         Width           =   1395
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Evento Causador da Ocorr�ncia"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   28
         Top             =   900
         Width           =   2280
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Observa��es"
      Height          =   3210
      Left            =   120
      TabIndex        =   22
      Top             =   3960
      Width           =   8760
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   45
         MaxLength       =   70
         TabIndex        =   9
         Top             =   360
         Width           =   8650
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   45
         MaxLength       =   70
         TabIndex        =   10
         Top             =   630
         Width           =   8650
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   45
         MaxLength       =   70
         TabIndex        =   11
         Top             =   915
         Width           =   8650
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   45
         MaxLength       =   70
         TabIndex        =   12
         Top             =   1200
         Width           =   8650
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   45
         MaxLength       =   70
         TabIndex        =   13
         Top             =   1470
         Width           =   8650
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   5
         Left            =   45
         MaxLength       =   70
         TabIndex        =   14
         Top             =   1740
         Width           =   8650
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   6
         Left            =   45
         MaxLength       =   70
         TabIndex        =   15
         Top             =   2010
         Width           =   8650
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   7
         Left            =   45
         MaxLength       =   70
         TabIndex        =   16
         Top             =   2280
         Width           =   8650
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   8
         Left            =   45
         MaxLength       =   70
         TabIndex        =   17
         Top             =   2550
         Width           =   8650
      End
      Begin VB.TextBox txtExigencia 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   9
         Left            =   45
         MaxLength       =   70
         TabIndex        =   18
         Top             =   2820
         Width           =   8650
      End
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   6240
      TabIndex        =   19
      Top             =   7320
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   7605
      TabIndex        =   21
      Top             =   7320
      Width           =   1275
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   4800
      TabIndex        =   20
      Top             =   7320
      Width           =   1275
   End
   Begin VB.Frame frmCabecalho 
      Height          =   1575
      Left            =   120
      TabIndex        =   23
      Top             =   120
      Width           =   8760
      Begin MSMask.MaskEdBox mskDtNascimento 
         Height          =   330
         Left            =   5625
         TabIndex        =   3
         Top             =   1080
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskCPF 
         Height          =   330
         Left            =   90
         TabIndex        =   1
         Top             =   1080
         Width           =   1770
         _ExtentX        =   3122
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   14
         Mask            =   "###.###.###-##"
         PromptChar      =   "_"
      End
      Begin VB.ComboBox cboSexo 
         Height          =   315
         ItemData        =   "SEGP0794_3.frx":0442
         Left            =   1935
         List            =   "SEGP0794_3.frx":044F
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1080
         Width           =   3360
      End
      Begin VB.TextBox txtNomeSinistrado 
         Height          =   330
         Left            =   120
         MaxLength       =   60
         TabIndex        =   0
         Top             =   480
         Width           =   7095
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Nome:"
         Height          =   195
         Left            =   120
         TabIndex        =   35
         Top             =   240
         Width           =   465
      End
      Begin VB.Label Label4 
         Caption         =   "Dados do Sinistrado"
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   0
         Width           =   1455
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Sexo:"
         Height          =   195
         Left            =   1935
         TabIndex        =   26
         Top             =   840
         Width           =   405
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Data de Nascimento:"
         Height          =   195
         Index           =   1
         Left            =   5625
         TabIndex        =   25
         Top             =   840
         Width           =   1500
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "C.P.F.:"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   24
         Top             =   840
         Width           =   480
      End
   End
End
Attribute VB_Name = "SEGP0794_3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public DtaServidor As Date 'Demanda 12554726 Paulo Fantin
'------------ ARICARDO - CWI - 24/11-------------
Public FlagCredito As String
'------------------------------------------------

'cristovao.rodrigues 04/04/2014 16429073 - Amparo Familiar
Public sTitularClienteId    As String
Public sTitularNome         As String

'SBRJ010629 - Marianne Teixeira - N�o permitir o novo aviso de sinistro por DT ou IPD quando j� houver aviso do mesmo tipo
Public sPropostaTitular         As String
Public sProduto         As String

Dim bValidaCPF As Boolean   'Demanda 19752133

Private Sub carregaEvento()
    Dim SQL As String
    
    SQL = ""
    SQL = SQL & " select    distinct es.evento_sinistro_id, es.nome"
    SQL = SQL & " from      evento_sinistro_ramo_tb esr with (nolock),"
    SQL = SQL & "           ramo_tb r with (nolock),"
    SQL = SQL & "           evento_sinistro_tb es with (nolock), "
    SQL = SQL & "           produto_estimativa_sinistro_tb pes with (nolock)"
    SQL = SQL & " Where     esr.ramo_id = r.ramo_id"
    SQL = SQL & " and       esr.evento_sinistro_id = es.evento_sinistro_id"
    SQL = SQL & " and       pes.evento_sinistro_id = esr.evento_sinistro_id"
    SQL = SQL & " and       r.tp_ramo_id = " & TIPO_RAMO
    SQL = SQL & " and       pes.situacao = 'A'"
    'R.FOUREAUX - 26/04/2018
    If SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = 14 Then
     SQL = SQL & " and pes.produto_id = 14 "
    End If
    SQL = SQL & " order by  es.nome"


    
    Set Rst_Causa = ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                SQL, True)
    
    With cboEvento
        .Clear
        
        If Not Rst_Causa.EOF Then
           While Not Rst_Causa.EOF
                 .AddItem UCase(Rst_Causa(1))
                 .ItemData(.NewIndex) = Rst_Causa(0)
                 Rst_Causa.MoveNext
           Wend
        End If
        
        Rst_Causa.Close
        ' Inclui tamb�m a causa gen�rica
        .AddItem "CAUSA GEN�RICA PARA SINISTRO"
        .ItemData(.NewIndex) = CAUSA_GENERICA_SEGUR
        .ItemData(.NewIndex) = 0
    End With
    
    '***********************************************************************************************
                
    '***********************************************************************************************
    'DEMANDA        : 12554726
    'Alterado por   : Paulo Fantin
    'Objetivo       : O Sistema n�o permitia o aviso de sinistro em alguns casos, por exemplo:
    '                 1-)Fins de semana
    '                 2-)Feriados
    '                 Foi Feito um getdate()no banco de dados para pegar a data atual e setar o valor
    '                 na v�riavel "DtaServidor" do servidor assim possibilitando o usu�rio
    '                 cadastrar os avisos de sinistros nas datas citadas acima
    '***********************************************************************************************
        Dim RsAux As New ADODB.Recordset
        SQL = "Select Getdate()DataHoraServidor"
        Set RsAux = ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, _
                             True)
        If Not RsAux.EOF Then
            DtaServidor = RsAux!DataHoraServidor
        Else
            DtaServidor = Now()
        End If
        
        Set RsAux = Nothing


End Sub


Private Sub cboEvento_Click()

Dim SQL As String

   If cboEvento.ListIndex <> -1 Then
        SQL = ""
        SQL = SQL & " SELECT    a.subevento_sinistro_id, a.nome"
        SQL = SQL & " FROM      subevento_sinistro_tb a with (nolock),"
        SQL = SQL & "           evento_subevento_sinistro_tb b with (nolock)"
        SQL = SQL & " WHERE     b.evento_sinistro_id  = " & cboEvento.ItemData(cboEvento.ListIndex)
        SQL = SQL & " and       b.subevento_sinistro_id = a.subevento_sinistro_id "
        SQL = SQL & " ORDER BY  a.nome"
        
        Set Rst_Causa = ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    SQL, True)
        
        With cboSubEvento
        .Clear
        
        If Not Rst_Causa.EOF Then
           cboSubEvento.Enabled = True
           While Not Rst_Causa.EOF
                 .AddItem UCase(Rst_Causa("nome"))
                 .ItemData(.NewIndex) = Rst_Causa("subevento_sinistro_id")
                 Rst_Causa.MoveNext
           Wend
        Else
           cboSubEvento.ListIndex = -1
           cboSubEvento.Enabled = False
        End If
        
        Rst_Causa.Close
      
    End With
	
	 'SBRJ010629 - Marianne Teixeira - N�o permitir o novo aviso de sinistro por DT ou IPD quando j� houver aviso do mesmo tipo

        If SEGP0794_3.sProduto = 136 Or _
           SEGP0794_3.sProduto = 121 Then
           
           If cboEvento.ItemData(cboEvento.ListIndex) = 104 Or _
                cboEvento.ItemData(cboEvento.ListIndex) = 270 Then
                
           sSQL = ""
           sSQL = sSQL & " SELECT    sinistro_tb.proposta_id"
           sSQL = sSQL & " FROM      seguros_db..sinistro_tb sinistro_tb with (nolock),"
           sSQL = sSQL & "           seguros_db..evento_SEGBR_sinistro_atual_tb evento_SEGBR_sinistro_atual_tb with (nolock)"
           sSQL = sSQL & " WHERE     sinistro_tb.sinistro_id = evento_SEGBR_sinistro_atual_tb.sinistro_id "
           sSQL = sSQL & " and       sinistro_tb.evento_sinistro_id IN (104,270) "
           sSQL = sSQL & " and       sinistro_tb.proposta_id = " & SEGP0794_3.sPropostaTitular
           sSQL = sSQL & " and       sinistro_tb.dt_aviso_sinistro IS NOT NULL "
           sSQL = sSQL & " and       evento_SEGBR_sinistro_atual_tb.ind_tp_sinistrado = 'C' "
        
        Set rsEvento = ExecutarSQL(gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    sSQL, True)

            If Not rsEvento.EOF Then
              
                MsgBox "J� h� um aviso de sinistro para essa proposta.", vbCritical, App.Title
                cboEvento.ListIndex = -1
            End If
             rsEvento.Close
          End If
        End If
	
  End If
End Sub

'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
Private Sub cmbNomeSinistrado_Click()
    
    txtNomeSinistrado = cmbNomeSinistrado.Text
    
'    If sTitularClienteId = "" Then  'cristovao.rodrigues 24/04/2014 - 179194777
'        sTitularClienteId = SEGP0794_3.mskCPF.ClipText
'        sTitularNome = SEGP0794_3.txtNomeSinistrado
'    End If
        
    'SEGP0794_3.CarregaDadosComboCliente
    'SEGP0794_3.carregaSinistrado
    SEGP0794_3.carregaSinistradoCliente
 
    SEGP0794_4.CLIENTE_DOC = SEGP0794_3.mskCPF.ClipText
    SEGP0794_4.Nome = Trim(SEGP0794_3.txtNomeSinistrado)
    
    
End Sub

Private Sub cmbNomeSinistrado_KeyPress(KeyAscii As Integer)

        '18776467 - PAULO PELEGRINI - 12/02/2016 (INI)
        If SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = 1231 Or _
            SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = 1225 Then
            KeyAscii = 0
        End If
        '18776467 - PAULO PELEGRINI - 12/02/2016 (FIM)

End Sub

Private Sub cmdContinuar_Click()
Dim Cont As Integer
    
    'Se n�o achou na base, � obrigatorio entrar com os dados do sinistrado
    If frmCabecalho.Enabled Then
        If txtNomeSinistrado.Text = "" Then
            MsgBox "Indique o nome do Sinistrado.", vbCritical, App.Title
            txtNomeSinistrado.SetFocus
            Exit Sub
        End If
        
        If mskCPF.Text = "___.___.___-__" Then
            MsgBox "Indique o CPF doo Sinistrado.", vbCritical, App.Title
             mskCPF.SetFocus
            Exit Sub
        End If
        
        If cboSexo.ListIndex = -1 Then
            MsgBox "Indique o sexo do Sinistrado.", vbCritical, App.Title
            cboSexo.SetFocus
            Exit Sub
        End If
        
        If mskDtNascimento.Text = "__/__/____" Then
            MsgBox "Indique a Data de nascimento do Sinistrado.", vbCritical, App.Title
             mskDtNascimento.SetFocus
            Exit Sub
        End If
    End If
    
    
    If mskDataOcorrencia.Text = "__/__/____" Then
        MsgBox "Indique a Data de Ocorr�ncia do Sinistro.", vbCritical, App.Title
        mskDataOcorrencia.SetFocus
        Exit Sub
    End If
    
    '***********************************************************************************************
                ' DEMANDA  : 12554726 Parte 2
    '***********************************************************************************************
    'Alterado por   : Paulo Fantin
    'Objetivo       : O Sistema n�o permitia o aviso de sinistro em alguns casos, por exemplo:
    '                 1-)Fins de semana
    '                 2-)Feriados
    'Objetivo       : Foi Feito um getdate()no banco de dados para pegar a data atual e setar o valor
    '                 na v�riavel "DtaServidor" do servidor assim possibilitando o usu�rio
    '                 cadastrar os avisos de sinistros nas datas citadas acima
    '***********************************************************************************************
        If Format(mskDataOcorrencia.Text, "yyyymmdd") > Format(DtaServidor, "yyyymmdd") Then
        'If Format(mskDataOcorrencia.Text, "yyyymmdd") > Format(Data_Sistema, "yyyymmdd") Then
            'MsgBox "Data de Ocorr�ncia posterior a data atual (" & Data_Sistema & ").", vbCritical, App.Title
            MsgBox "Data de Ocorr�ncia posterior a data atual (" & Format(DtaServidor, "dd/MM/yyyy") & ").", vbCritical, App.Title
            mskDataOcorrencia.SetFocus
            Exit Sub
        End If
    '***********************************************************************************************
                'FIM DA DEMANDA : 12554726
    '***********************************************************************************************
    'HENRIQUE H. HENRIQUES - CORRE��O DA TRATATIVA DA HORA DA OCORRENCIA PARA EVITAR ERRO. - INI
    If mskHora.Text = "__:__" Or Len(mskHora.ClipText) < 4 Or Not IsDate(mskHora.Text) Then
    'HENRIQUE H. HENRIQUES - CORRE��O DA TRATATIVA DA HORA DA OCORRENCIA PARA EVITAR ERRO. - FIM
        MsgBox "Hora da Ocorr�ncia do Sinistro inv�lida.", vbCritical, App.Title
        mskHora.SetFocus
        Exit Sub
    End If
    
    'Paulo Pelegrini - MU-2017-042685 - 09/04/2018 (INI)
    If TIPO_RAMO = 1 And SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) <> "1140" Then
        If (DateDiff("d", mskDataOcorrencia.Text, DtaServidor) / 365) > 30.02 Then
            If MsgBox("A data do Aviso � maior que 30 anos da data do Evento." + vbNewLine + "Deseja Continuar mesmo assim?", vbYesNo + vbQuestion + vbDefaultButton2) = vbNo Then
                mskDataOcorrencia.SetFocus
                Exit Sub
            End If
        End If
    End If
    'Paulo Pelegrini  - MU-2017-042685 - 09/04/2018 (FIM)
       
    
    If txtLocal.Text = "" Then
        MsgBox "Indique o Local da Ocorr�ncia do Sinistro.", vbCritical, App.Title
        txtLocal.SetFocus
        Exit Sub
    End If
    
    If cboEvento.ListIndex = -1 Then
        MsgBox "Indique o evento causador da ocorr�ncia.", vbCritical, App.Title
        cboEvento.SetFocus
        Exit Sub
    End If
        
    If cboEvento.ListIndex <> -1 And cboSubEvento.ListCount > 0 And cboSubEvento.ListIndex = -1 Then
        MsgBox "Indique o Subevento causador da ocorr�ncia.", vbCritical, App.Title
        cboEvento.SetFocus
        Exit Sub
    End If
        
    If Trim(txtExigencia(0).Text) = "" And _
        Trim(txtExigencia(1).Text) = "" And _
        Trim(txtExigencia(2).Text) = "" And _
        Trim(txtExigencia(3).Text) = "" And _
        Trim(txtExigencia(4).Text) = "" And _
        Trim(txtExigencia(5).Text) = "" And _
        Trim(txtExigencia(6).Text) = "" And _
        Trim(txtExigencia(7).Text) = "" And _
        Trim(txtExigencia(8).Text) = "" And _
        Trim(txtExigencia(9).Text) = "" Then
        
        MsgBox "Informe uma descri��o do evento causador da ocorr�ncia.", vbCritical, App.Title
        txtExigencia(0).SetFocus
        Exit Sub
    End If
    
    With SEGP0794_7
        'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
        '.txtNomeSinistrado.Text = txtNomeSinistrado.Text
        .txtNomeSinistrado.Text = MudaAspaSimples(txtNomeSinistrado.Text)
        'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Fim
        
        .txtCPFSinistrado.Text = mskCPF.Text
        .txtSexoSinistrado.Text = cboSexo.Text
        .txtDtNascimentoSinistrado.Text = mskDtNascimento.Text
        
        .txtDtOcorrencia.Text = mskDataOcorrencia.Text
        .txtEventoOcorrencia.Text = cboEvento.Text
        .txtSubeventoOcorrencia.Text = IIf(cboSubEvento.ListIndex = -1, "", cboSubEvento.Text)
        .mskHora.Text = mskHora.Text
        .txtLocal.Text = txtLocal.Text
        
        For Cont = 0 To 9
            .txtExigencia(Cont).Text = txtExigencia(Cont).Text
        Next Cont
    End With
    
    SEGP0794_8.Evento_sinistro_id = cboEvento.ItemData(cboEvento.ListIndex)
    If cboSubEvento.ListIndex = -1 Then
       SEGP0794_8.SubEvento_sinistro_id = 0
    Else
       SEGP0794_8.SubEvento_sinistro_id = cboSubEvento.ItemData(cboSubEvento.ListIndex)
    End If
    Me.Hide
    'Caso seja achado o segurado na base, vai para a tela de propostas encontradas
    'sergio.so
    'If Not frmCabecalho.Enabled Or SEGP0794_4.CLIENTE_DOC = "30822936000169" Then
    'sergio.eo
    
    'sergio.sn - DATA 25/08/2010 - ANALISTA: SERGIO BAZILIO DA SILVA
    '            ALTERADO PARA PERMITIR A PESQUISA DE PROPOSTAS PARA AVISO QUANDO O CLIENTE SELECIONADO
    '            FOR PESSOA JURIDICA
    If Not frmCabecalho.Enabled Or Len(SEGP0794_4.CLIENTE_DOC) = 14 Then
    'sergio.en
        Me.MousePointer = vbHourglass
        With SEGP0794_4
            .montaRelacaoCliente
            .buscaProposta
            .buscaCobertura
        End With
        
        Me.MousePointer = vbDefault
        SEGP0794_4.Show
    Else
        'vai para a tela para ser preenchido manualmente os dados do segurado
        SEGP0794_9.txtNomeSegurado.Text = txtNomeSinistrado.Text
        SEGP0794_9.mskCPF.Text = mskCPF.Text
        SEGP0794_9.mskDtNascimento.Text = mskDtNascimento.Text
        SEGP0794_9.cboSexo.Text = cboSexo.Text
        
        SEGP0794_9.Origem = 3
        SEGP0794_9.Show
    End If
End Sub

Private Sub cmdSair_Click()
    Sair Me
End Sub

Private Sub cmdVoltar_Click()
    Me.Hide
    'LimpaCampos 'cristovao.rodrigues 01/07/2014
    SEGP0794_2.Show
End Sub

'cristovao.rodrigues 01/07/2014
Public Sub LimpaCampos()

With SEGP0794_3
      .txtNomeSinistrado.Text = ""
      .mskCPF.Text = "___.___.___-__"
      .mskDtNascimento.Text = "__/__/____"
      .cboSexo.ListIndex = -1
      .mskDataOcorrencia.Text = "__/__/____"
      .cboEvento.ListIndex = -1
      .cboSubEvento.ListIndex = -1
      For i = 0 To 9
        .txtExigencia(i).Text = ""
      Next
      .mskHora = "__:__"
      .txtLocal = ""
 End With
 
End Sub



'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
Public Sub CarregaDadosComboCliente()

Dim SQL As String
Dim rs As ADODB.Recordset

On Error GoTo Trata_Erro
    
    'cristovao.rodrigues 03/04/2014
'    SQL = ""
'    SQL = SQL & "create table #Cliente (nome varchar(60), cpfcnpj varchar(14), sexo varchar(1), dt_nascimento smalldatetime, componente varchar(1) )"
'
'    Call ExecutarSQL(gsSIGLASISTEMA, _
'                          glAmbiente_id, _
'                          App.Title, _
'                          App.FileDescription, _
'                          SQL, _
'                          False)

    txtNomeSinistrado.Visible = True
    cmbNomeSinistrado.Visible = False
    
    'If SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = "" Then 'cristovao.rodrigues 30/06/2014

    SQL = ""
'    SQL = SQL & " select cliente_tb.cliente_id, pessoa_fisica_tb.cpf, cliente_tb.nome, pessoa_fisica_tb.sexo, pessoa_fisica_tb.dt_nascimento , 't' tipo_componente, '1' componente " & vbctrlf
'    SQL = SQL & " from seguros_db..cliente_tb cliente_tb with(nolock) " & vbctrlf
'    SQL = SQL & " left join seguros_db..pessoa_fisica_tb pessoa_fisica_tb with(nolock) " & vbctrlf
'    SQL = SQL & "   on pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id " & vbctrlf
'    SQL = SQL & " left join seguros_db..proposta_tb proposta_tb with(nolock) " & vbctrlf
'    SQL = SQL & "   on proposta_tb.prop_cliente_id = cliente_tb.cliente_id " & vbctrlf
    SQL = SQL & " select " & vbCrLf
    SQL = SQL & " cliente_tb.cliente_id, " & vbCrLf
    
    'SQL = SQL & " pessoa_fisica_tb.cpf , " & vbCrLf
    SQL = SQL & " isnull(pessoa_juridica_tb.cgc, pessoa_fisica_tb.cpf ) cpf, " & vbCrLf
    
    SQL = SQL & " cliente_tb.nome, " & vbCrLf
    SQL = SQL & " isnull(pessoa_fisica_tb.sexo,'') sexo, " & vbCrLf
    SQL = SQL & " isnull(pessoa_fisica_tb.dt_nascimento,'19000101') dt_nascimento, " & vbCrLf
    SQL = SQL & " 't' tipo_componente, " & vbCrLf
    SQL = SQL & " '1' componente  " & vbCrLf
    SQL = SQL & " from seguros_db..cliente_tb cliente_tb with(nolock)  " & vbCrLf
    SQL = SQL & " left join seguros_db..pessoa_fisica_tb pessoa_fisica_tb with(nolock)    " & vbCrLf
    SQL = SQL & "   on pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id  " & vbCrLf
    SQL = SQL & " left join seguros_db..proposta_tb proposta_tb with(nolock)   " & vbCrLf
    SQL = SQL & "   on proposta_tb.prop_cliente_id = cliente_tb.cliente_id     " & vbCrLf
'    SQL = SQL & " ---- " & vbCrLf 'cristovao.rodrigues 24/04/2014 - 17919477 - incluido join com pessoa juridica
    SQL = SQL & " left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb with(nolock) " & vbCrLf
    SQL = SQL & "   on pessoa_juridica_tb.pj_cliente_id = proposta_tb.prop_cliente_id " & vbCrLf
'    SQL = SQL & " ---- " & vbCrLf
    SQL = SQL & " where 1 = 1 " & vbctrlf
    SQL = SQL & "   and proposta_tb.proposta_id = " & SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 10) & vbctrlf
    
    If SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = 1217 Or _
    SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = 1231 Or _
            SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = 1225 Then 'cristovao.rodrigues 24/04/2014 17919477 incluido 1225 - 'Marcio.Nogueira 24/07/2015 - Inclus�o Produto 1231
    
        txtNomeSinistrado.Visible = False
        cmbNomeSinistrado.Visible = True
        
       
'        SQL = SQL & " union " & vbctrlf
'        SQL = SQL & " select cliente_tb.cliente_id, pessoa_fisica_tb.cpf, cliente_tb.nome, pessoa_fisica_tb.sexo, pessoa_fisica_tb.dt_nascimento , 'c' tipo_componente, '2' componente " & vbctrlf
'        SQL = SQL & " from seguros_db..cliente_tb cliente_tb with(nolock) " & vbctrlf
'        SQL = SQL & " left join seguros_db..pessoa_fisica_tb pessoa_fisica_tb with(nolock) " & vbctrlf
'        SQL = SQL & "   on pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id " & vbctrlf
'        SQL = SQL & " left join seguros_db..proposta_complementar_tb proposta_complementar_tb with(nolock) " & vbctrlf
'        SQL = SQL & "   on proposta_complementar_tb.prop_cliente_id = cliente_tb.cliente_id " & vbctrlf
        SQL = SQL & " union " & vbCrLf
        SQL = SQL & " select " & vbCrLf
        SQL = SQL & " cliente_tb.cliente_id, " & vbCrLf
        
        'SQL = SQL & " pessoa_fisica_tb.cpf , " & vbCrLf
        SQL = SQL & " isnull(pessoa_juridica_tb.cgc, pessoa_fisica_tb.cpf ) cpf, " & vbCrLf
        
        SQL = SQL & " cliente_tb.nome, " & vbCrLf
        SQL = SQL & " isnull(pessoa_fisica_tb.sexo,'') sexo, " & vbCrLf
        SQL = SQL & " isnull(pessoa_fisica_tb.dt_nascimento,'19000101') dt_nascimento, " & vbCrLf
        SQL = SQL & " 'c' tipo_componente, " & vbCrLf
        SQL = SQL & " '2' componente  " & vbCrLf
        SQL = SQL & " from seguros_db..cliente_tb cliente_tb with(nolock)  " & vbCrLf
        SQL = SQL & " left join seguros_db..pessoa_fisica_tb pessoa_fisica_tb with(nolock)    " & vbCrLf
        SQL = SQL & "   on pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id  " & vbCrLf
        SQL = SQL & " left join seguros_db..proposta_complementar_tb proposta_complementar_tb with(nolock)   " & vbCrLf
        SQL = SQL & "   on proposta_complementar_tb.prop_cliente_id = cliente_tb.cliente_id  " & vbCrLf
'        SQL = SQL & " ---- " & vbCrLf 'cristovao.rodrigues 24/04/2014 - 17919477 - incluido join com pessoa juridica
        SQL = SQL & " left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb with(nolock) " & vbCrLf
        SQL = SQL & "   on pessoa_juridica_tb.pj_cliente_id = proposta_complementar_tb.prop_cliente_id  " & vbCrLf
'        SQL = SQL & " ---- " & vbCrLf
        SQL = SQL & " where 1 = 1 " & vbctrlf
        SQL = SQL & "   and proposta_complementar_tb.proposta_id = " & SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 10) & vbctrlf
                '        Inicio -MU00416967 - Melhorias_SEGP0794_Aviso_Sinistro  ''INC 21\03\2018
        ElseIf SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = "1201" Or _
            SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = "1226" Or _
            SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = "1227" Then

            SQL = SQL & " union " & vbNewLine & vbCrLf & vbCrLf
            SQL = SQL & " select " & vbNewLine
            SQL = SQL & " c.cliente_id, " & vbNewLine
            SQL = SQL & " isnull(pj.cgc, pf.cpf ) cpf, " & vbNewLine
            SQL = SQL & " c.nome, " & vbNewLine
            SQL = SQL & " isnull(pf.sexo,'') sexo, " & vbNewLine
            SQL = SQL & " isnull(pf.dt_nascimento,'19000101') dt_nascimento, " & vbNewLine
            SQL = SQL & " 'c' tipo_componente, " & vbNewLine
            SQL = SQL & " '2' componente  " & vbNewLine
            SQL = SQL & " From Seguros_Db.Dbo.Cliente_Habitacional_Tb ch With (NoLock) " & vbNewLine
            SQL = SQL & " left join       Seguros_Db.Dbo.pessoa_fisica_tb pf with (nolock)" & vbNewLine
            SQL = SQL & " on         ch.Cliente_ID = pf.pf_cliente_id" & vbNewLine
            SQL = SQL & " left join  Seguros_Db.Dbo.pessoa_juridica_tb pj with (nolock)" & vbNewLine
            SQL = SQL & " on        pj.pj_cliente_id = Ch.Cliente_id " & vbNewLine
            SQL = SQL & " join       Seguros_Db.Dbo.cliente_tb c     with (nolock)" & vbNewLine
            SQL = SQL & " on         c.cliente_id =   ch.Cliente_ID" & vbNewLine
            SQL = SQL & " Where  ch.proposta_id = " & SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 10) & vbNewLine
            SQL = SQL & " and  c.cliente_id not in" & vbNewLine
            SQL = SQL & " (select prop_cliente_id  " & vbNewLine
            SQL = SQL & " from  Seguros_Db.Dbo.proposta_tb with(nolock) " & vbNewLine
            SQL = SQL & " where Proposta_ID = " & SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 10) & ")" & vbNewLine
'        FIM - MU00416967 - Melhorias_SEGP0794_Aviso_Sinistro
        
        SQL = SQL & " order by componente "
    End If
    
    

'     Call ExecutarSQL(gsSIGLASISTEMA, _
'                          glAmbiente_id, _
'                          App.Title, _
'                          App.FileDescription, _
'                          SQL, _
'                          False)
'
'
'    SQL = ""
'    SQL = SQL & " select cpfcnpj , nome "
'    SQL = SQL & " from   #Cliente with(nolock)"
'    SQL = SQL & " order by  nome"
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)
    
    With cmbNomeSinistrado
        .Clear
        
        If Not rs.EOF Then
            While Not rs.EOF
                
                'If rs!CPF <> "Null" Then 'cristovao.rodrigues 24/04/2014 - 17919477
                    
                    If rs!componente = 1 Then
                        'SEGP0794_4.CLIENTE_DOC = rs!CPF
                        'SEGP0794_4.Nome = rs!Nome
                        
                        sTitularClienteId = rs!CPF
                        sTitularNome = Trim(rs!Nome)
                        
                    End If
                    
                    If Not Len(rs!CPF) > 11 Then 'cristovao.rodrigues 24/04/2014 - 17919477
                        .AddItem Trim(rs(2))            'nome
                        .ItemData(.NewIndex) = rs(0)    'cliente_id
                    End If
                    
                'End If
                
                rs.MoveNext
                
            Wend
            'cmbNomeSinistrado.ListIndex = 0
            
        End If
        rs.Close
        
        If cmbNomeSinistrado.ListCount > 0 Then
            cmbNomeSinistrado.ListIndex = 0
        End If
                
'            inicio - MU00416967 - Melhorias_SEGP0794_Aviso_Sinistro   '' INC 21\03\2018
           If (SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = "1201" Or _
           SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = "1226" Or _
           SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = "1227") And cmbNomeSinistrado.ListCount > 1 Then

            
            txtNomeSinistrado.Visible = True
            cmbNomeSinistrado.Visible = True
          End If 'FIM - MU00416967 - Melhorias_SEGP0794_Aviso_Sinistro
                
                
    End With
                          
Exit Sub
Resume
Trata_Erro:
MsgBox "erro"

End Sub

Public Sub carregaSinistradoCliente()
    Dim SQL As String
    Dim rs As ADODB.Recordset
    
    'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
'    SQL = ""
'    SQL = SQL & "create table #Cliente (nome varchar(60), cpfcnpj varchar(14), sexo varchar(1), dt_nascimento smalldatetime, componente varchar(1) )"
'
'Call Conexao_ExecutarSQL(sSistema, _
'                             lAmbienteID, _
'                             sSiglaRecurso, _
'                             sDescricaoRecurso, _
'                             sSQL, _
'                             lConexao, _
'                             False)


    SQL = ""
    SQL = SQL & " select cliente_tb.nome, pessoa_fisica_tb.cpf ," & vbCrLf
    'SQL = SQL & " select cliente_tb.nome, isnull(pessoa_juridica_tb.cgc, pessoa_fisica_tb.cpf ) cpf ," & vbCrLf
    SQL = SQL & " isnull(pessoa_fisica_tb.sexo,'') sexo, isnull(pessoa_fisica_tb.dt_nascimento,'19000101') dt_nascimento " & vbctrlf
    SQL = SQL & " from seguros_db..cliente_tb cliente_tb with(nolock) " & vbctrlf
    SQL = SQL & " left join seguros_db..pessoa_fisica_tb pessoa_fisica_tb with(nolock) " & vbctrlf
    SQL = SQL & "   on pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id " & vbctrlf
    'cristovao.rodrigues 24/04/2014 - 17919477 - incluido join pessoa juridica
'    SQL = SQL & " left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb with(nolock) " & vbCrLf
'    SQL = SQL & "   on pessoa_juridica_tb.pj_cliente_id = cliente_tb.cliente_id " & vbCrLf
    '
    SQL = SQL & " where 1 = 1 " & vbctrlf
    SQL = SQL & "   and cliente_tb.cliente_id = " & cmbNomeSinistrado.ItemData(cmbNomeSinistrado.ListIndex)

    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        SQL, True)

    AvisoSinistro.ClienteIdSelecionado = cmbNomeSinistrado.ItemData(cmbNomeSinistrado.ListIndex)
    
    If rs.EOF Then
            SEGP0794_3.frmCabecalho.Enabled = True
            Exit Sub
    End If
    
    With Me
        .txtNomeSinistrado.Text = Trim(rs!Nome)
        .mskCPF.Mask = ""
        
            .mskCPF.Text = MasCPF(rs!CPF) ', 1, 3) & "." & Mid(rs!CPF, 4, 3) & "." & Mid(rs!CPF, 7, 3) & "-" & Mid(rs!CPF, 10, 2)
            
            .mskCPF.Text = IIf(Len(rs!CPF) > 11, MasCGC(rs!CPF), MasCPF(rs!CPF)) 'cristovao.rodrigues 24/04/2014 - 17919477
            
            If Len(rs!CPF) > 11 Then ' cristovao.rodrigues 24/04/2014 - 17919477
                Label2(1).Caption = "CGC :"
                .mskCPF.Mask = "##.###.###/####-##"
            Else
                Label2(1).Caption = "CPF :"
                .mskCPF.Mask = "###.###.###-##"
            End If
            
            .mskDtNascimento.Mask = ""
            .mskDtNascimento.Text = Format(rs!dt_Nascimento, "DD/MM/YYYY")
            .mskDtNascimento.Mask = "##/##/####"
            
            
'            If sTitularClienteId = "" Then  'cristovao.rodrigues 24/04/2014 - 179194777
'                sTitularClienteId = SEGP0794_3.mskCPF.ClipText
'                sTitularNome = SEGP0794_3.txtNomeSinistrado
'            End If
            
            
        Select Case UCase(rs!Sexo)
            Case "F"
                .cboSexo.ListIndex = 1
            Case "M"
                .cboSexo.ListIndex = 2
            Case Else
                .cboSexo.ListIndex = 0
        End Select
    End With
    
     frmCabecalho.Enabled = False 'cristovao.rodrigues 03/04/2014 comentado teste


End Sub



Public Sub carregaSinistrado()
    Dim SQL As String
    Dim rs As ADODB.Recordset
    
    If SEGP0794_2.GrdResultadoPesquisa.Rows = 1 Then Exit Sub
    'If SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 1) <> 4378747 Then
        SQL = ""
        SQL = SQL & " select c.nome, pf.cpf, pf.sexo, pf.dt_nascimento"
        SQL = SQL & " from cliente_tb c with (nolock), pessoa_fisica_tb pf with (nolock)"
        SQL = SQL & " Where C.cliente_id = pf.pf_cliente_id"
        SQL = SQL & " and c.cliente_id = " & SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 1)
        
        ''flow 4883613
        'o c�digo do cliente ser� armazenado em
        'AvisoSinistro.ClienteIdSelecionado (nullable)
        'para posterior utiliza��o em
        'SEGP0794_4.buscaProposta como parametro da procedure busca_proposta_sinistro_sps
        AvisoSinistro.ClienteIdSelecionado = SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 1)
        ''flow 4883613

        
    'Else
    '    SQL = ""
    '    SQL = SQL & " select c.nome, pj.cgc, 'Indefinido' as sexo, '' as dt_nascimento"
    '    SQL = SQL & " from cliente_tb c with(nolock), pessoa_juridica_tb pj with(nolock)"
    '    SQL = SQL & " Where C.cliente_id = pj.pj_cliente_id"
    '    SQL = SQL & " and c.cliente_id = " & SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 1)
    'End If
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
                         
    'sergio.so
    'If rs.EOF Then Exit Sub
    'sergio.eo
    
    'sergio.sn - DATA: 26/08/2010 - ANALISTA: SERGIO BAZILIO DA SILVA
    '            TRATAMENTO PARA QUANDO N�O LOCALIZAR CLIENTE PESSOA F�SICA,
    '            HABILITAR PARA PREENCHIMENTO DOS DADOS DO SOLICITANTE
    If rs.EOF Then
            SEGP0794_3.frmCabecalho.Enabled = True
            Exit Sub
            
    End If
    'sergio.en
    
    With Me
        .txtNomeSinistrado.Text = rs!Nome
        .mskCPF.Mask = ""
     '   If SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 1) <> 4378747 Then
            .mskCPF.Text = MasCPF(rs!CPF) ', 1, 3) & "." & Mid(rs!CPF, 4, 3) & "." & Mid(rs!CPF, 7, 3) & "-" & Mid(rs!CPF, 10, 2)
            .mskCPF.Mask = "###.###.###-##"
            .mskDtNascimento.Mask = ""
            .mskDtNascimento.Text = Format(rs!dt_Nascimento, "DD/MM/YYYY")
            .mskDtNascimento.Mask = "##/##/####"
      '  Else
      '      .mskCPF.Text = MasCGC(rs!CGC) ', 1, 3) & "." & Mid(rs!CPF, 4, 3) & "." & Mid(rs!CPF, 7, 3) & "-" & Mid(rs!CPF, 10, 2)
      '      .mskCPF.Mask = ""
      '      .mskDtNascimento.Mask = ""
      '      .mskDtNascimento.Text = rs(3)
      '      .mskDtNascimento.Mask = ""
      '  End If
        Select Case UCase(rs!Sexo)
            Case "F"
                .cboSexo.ListIndex = 1
            Case "M"
                .cboSexo.ListIndex = 2
            Case Else
                .cboSexo.ListIndex = 0
        End Select
    End With
    
    'frmCabecalho.Enabled = False 'cristovao.rodrigues 03/04/2014 comentado 16429073 - Amparo Familiar
    
End Sub

Private Sub Form_Load()
    carregaEvento
End Sub

'Demanda 19752133 - IN�CIO
Private Sub mskCPF_LostFocus()
   
    'Verificar se o CPF � v�lido
            
    If Len(mskCPF.ClipText) > 0 Then
    
        bValidaCPF = False
    
        'Demanda 19752133 - IN�CIO
        'If Not CPF_Ok(mskCPF.ClipText) Then
        If Not CPF_Ok_2(mskCPF.ClipText) Then
        'Demanda 19752133 - FIM
        
            MsgBox "O C.P.F. informado � inv�lido!", vbCritical, Screen.ActiveForm.Caption
            
            mskCPF.Mask = ""
            mskCPF.Text = ""
            
            mskCPF.Mask = "###.###.###-##"
            
            mskCPF.SetFocus
            
            Exit Sub
        End If
        
        bValidaCPF = True
    End If
    
End Sub
'Demanda 19752133 - FIM

'Demanda 19752133 - IN�CIO
Private Function CPF_Ok_2(ByVal NumCPF As String) As Integer

    Dim d(1 To 11) As Integer
    Dim i As Integer, DV1 As Integer, DV2 As Integer

    CPF_Ok_2% = False
    
    ' Verifica o tamanho de 11 d�gitos.
    If Len(NumCPF$) <> 11 Then
        Exit Function
    End If
    
    ' Verifica se CPF � "00000000000" e etc....
    If NumCPF$ = String$(11, "0") Or NumCPF$ = String$(11, "1") Or _
       NumCPF$ = String$(11, "2") Or NumCPF$ = String$(11, "3") Or _
       NumCPF$ = String$(11, "4") Or NumCPF$ = String$(11, "5") Or _
       NumCPF$ = String$(11, "6") Or NumCPF$ = String$(11, "7") Or _
       NumCPF$ = String$(11, "8") Or NumCPF$ = String$(11, "9") Then
       CPF_Ok_2% = False
       Exit Function
    End If

    ' Verifica se h� apenas n�meros.
    For i% = 1 To 11
        If (Mid$(NumCPF$, i%, 1) < "0") Or (Mid$(NumCPF$, i%, 1) > "9") Then
            Exit Function
        End If
    Next i%

    ' Isola cada um dos d�gitos.
    For i% = 1 To 11
        d%(i%) = Val(Mid$(NumCPF$, i%, 1))
    Next i%
    
    ' Verifica o primeiro d�gito verificador.
    DV1% = 10 * d%(1) + 9 * d%(2) + 8 * d%(3) + 7 * d%(4) + 6 * d%(5) + 5 * d%(6) + 4 * d%(7) + 3 * d%(8) + 2 * d%(9)
    DV1% = DV1% Mod 11
    If (DV1% = 0) Or (DV1% = 1) Then
       DV1% = 0
    Else
       DV1% = 11 - DV1%
    End If

    If DV1% <> d%(10) Then
        Exit Function
    End If
    
    ' Verifica o segundo d�gito verificador.
    DV2% = 11 * d%(1) + 10 * d%(2) + 9 * d%(3) + 8 * d%(4) + 7 * d%(5) + 6 * d%(6) + 5 * d%(7) + 4 * d%(8) + 3 * d%(9) + 2 * DV1%
    DV2% = DV2% Mod 11
    If (DV2% = 0) Or (DV2% = 1) Then
        DV2% = 0
    Else
        DV2% = 11 - DV2%
    End If
    
    If DV2% <> d%(11) Then
        Exit Function
    End If

    CPF_Ok_2% = True
    
End Function
'Demanda 19752133 - FIM

Private Sub mskDataOcorrencia_LostFocus()
    If mskDataOcorrencia.Text <> "__/__/____" Then
        If Not IsDate(mskDataOcorrencia.Text) Then
            MsgBox "Data inv�lida.", vbCritical
            mskDataOcorrencia.SetFocus
        End If
    End If
End Sub

Private Sub mskDtNascimento_LostFocus()
    If mskDtNascimento.Text <> "__/__/____" Then
        If Not IsDate(mskDtNascimento.Text) Then
            MsgBox "Data inv�lida.", vbCritical
            mskDtNascimento.SetFocus
        End If
    End If
End Sub
'andre luvison - 23//12/2004
Private Sub txtExigencia_KeyPress(Index As Integer, KeyAscii As Integer)

Dim i As Integer

For i = 0 To 9
    If Len(txtExigencia(i).Text) = txtExigencia(i).MaxLength - 1 Then
       If i < 9 Then
            txtExigencia(i + 1).SetFocus
       End If
    End If
Next

End Sub
'*****************************
