VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{629074EB-AE57-4B76-8AF4-1B62557ED9A6}#1.0#0"; "GRIDDINAMICO.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{EA5A962F-86AD-4480-BCF2-3A94A4CB62B9}#1.0#0"; "GRIDALIANCA.OCX"
Begin VB.Form SEGP0794_7 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0794 - Aviso de Sinistro pela CA"
   ClientHeight    =   8055
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11805
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8055
   ScaleWidth      =   11805
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   7680
      TabIndex        =   2
      Top             =   7560
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   10440
      TabIndex        =   1
      Top             =   7560
      Width           =   1275
   End
   Begin VB.CommandButton cmdAvisar 
      Caption         =   "Avisar"
      Height          =   420
      Left            =   9045
      TabIndex        =   0
      Top             =   7560
      Width           =   1275
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7485
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   13203
      _Version        =   393216
      Tabs            =   4
      Tab             =   3
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "Sinistrado"
      TabPicture(0)   =   "SEGP0794_7.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(1)=   "Frame4"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Ocorr�ncia"
      TabPicture(1)   =   "SEGP0794_7.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame5"
      Tab(1).Control(1)=   "Frame2"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Solicitante"
      TabPicture(2)   =   "SEGP0794_7.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Label15"
      Tab(2).Control(1)=   "Label14(0)"
      Tab(2).Control(2)=   "Label13(0)"
      Tab(2).Control(3)=   "Label12(0)"
      Tab(2).Control(4)=   "Label11(0)"
      Tab(2).Control(5)=   "Label9"
      Tab(2).Control(6)=   "Label8"
      Tab(2).Control(7)=   "Label6"
      Tab(2).Control(8)=   "Label5"
      Tab(2).Control(9)=   "Label3(2)"
      Tab(2).Control(10)=   "Label2(2)"
      Tab(2).Control(11)=   "Label4"
      Tab(2).Control(12)=   "txtTelefone(0)"
      Tab(2).Control(13)=   "txtGrauParentesco"
      Tab(2).Control(14)=   "txtRamal(4)"
      Tab(2).Control(15)=   "txtRamal(3)"
      Tab(2).Control(16)=   "txtRamal(2)"
      Tab(2).Control(17)=   "txtRamal(1)"
      Tab(2).Control(18)=   "txtRamal(0)"
      Tab(2).Control(19)=   "txtCEPSolicitante"
      Tab(2).Control(20)=   "txtUFSolicitante"
      Tab(2).Control(21)=   "txtMunicipioSolicitante"
      Tab(2).Control(22)=   "txtBairroSolicitante"
      Tab(2).Control(23)=   "txtEnderecoSolicitante"
      Tab(2).Control(24)=   "txtEmailSolicitante"
      Tab(2).Control(25)=   "txtNomeSolicitante"
      Tab(2).Control(26)=   "txtTipoTelefone(0)"
      Tab(2).Control(27)=   "txtTipoTelefone(1)"
      Tab(2).Control(28)=   "txtTipoTelefone(2)"
      Tab(2).Control(29)=   "txtTipoTelefone(3)"
      Tab(2).Control(30)=   "txtTipoTelefone(4)"
      Tab(2).Control(31)=   "txtDDD(0)"
      Tab(2).Control(32)=   "txtDDD(1)"
      Tab(2).Control(33)=   "txtDDD(2)"
      Tab(2).Control(34)=   "txtDDD(4)"
      Tab(2).Control(35)=   "txtTelefone(1)"
      Tab(2).Control(36)=   "txtTelefone(2)"
      Tab(2).Control(37)=   "txtTelefone(3)"
      Tab(2).Control(38)=   "txtTelefone(4)"
      Tab(2).Control(39)=   "txtDDD(3)"
      Tab(2).Control(40)=   "Label11(2)" '07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante
      Tab(2).Control(41)=   "mskCpfSolicitante" '07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante
      Tab(2).ControlCount=   42
      TabCaption(3)   =   "Propostas Afetadas"
      TabPicture(3)   =   "SEGP0794_7.frx":0054
      Tab(3).ControlEnabled=   -1  'True
      Tab(3).Control(0)=   "Label11(2)"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "mskCpfSolicitante"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).Control(2)=   "GridPropostaAfetada"
      Tab(3).Control(2).Enabled=   0   'False
      Tab(3).Control(3)=   "Frame3"
      Tab(3).Control(3).Enabled=   0   'False
      Tab(3).ControlCount=   4
      Begin VB.Frame Frame3 
         Enabled         =   0   'False
         Height          =   1425
         Left            =   240
         TabIndex        =   85
         Top             =   5880
         Width           =   11445
         Begin GridFrancisco.GridDinamico GridTipo 
            Height          =   1245
            Left            =   60
            TabIndex        =   91
            Top             =   120
            Width           =   11355
            _ExtentX        =   20029
            _ExtentY        =   2196
            BorderStyle     =   1
            AllowUserResizing=   3
            EditData        =   0   'False
            Highlight       =   1
            ShowTip         =   0   'False
            SortOnHeader    =   0   'False
            BackColor       =   -2147483643
            BackColorBkg    =   -2147483633
            BackColorFixed  =   -2147483633
            BackColorSel    =   -2147483635
            FixedCols       =   1
            FixedRows       =   1
            FocusRect       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   -2147483640
            ForeColorFixed  =   -2147483630
            ForeColorSel    =   -2147483634
            GridColor       =   -2147483630
            GridColorFixed  =   12632256
            GridLine        =   1
            GridLinesFixed  =   2
            MousePointer    =   0
            Redraw          =   -1  'True
            Rows            =   2
            TextStyle       =   0
            TextStyleFixed  =   0
            Cols            =   2
            RowHeightMin    =   0
         End
      End
      Begin VB.TextBox txtDDD 
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   -72885
         TabIndex        =   72
         Top             =   3375
         Width           =   555
      End
      Begin VB.TextBox txtTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   4
         Left            =   -71940
         TabIndex        =   71
         Top             =   3735
         Width           =   1410
      End
      Begin VB.TextBox txtTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   -71940
         TabIndex        =   70
         Top             =   3375
         Width           =   1410
      End
      Begin VB.TextBox txtTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   -71940
         TabIndex        =   69
         Top             =   3015
         Width           =   1410
      End
      Begin VB.TextBox txtTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   -71940
         TabIndex        =   68
         Top             =   2655
         Width           =   1410
      End
      Begin VB.TextBox txtDDD 
         Enabled         =   0   'False
         Height          =   330
         Index           =   4
         Left            =   -72885
         TabIndex        =   67
         Top             =   3735
         Width           =   555
      End
      Begin VB.TextBox txtDDD 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   -72885
         TabIndex        =   66
         Top             =   3015
         Width           =   555
      End
      Begin VB.TextBox txtDDD 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   -72885
         TabIndex        =   65
         Top             =   2655
         Width           =   555
      End
      Begin VB.TextBox txtDDD 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   -72885
         TabIndex        =   64
         Top             =   2295
         Width           =   555
      End
      Begin VB.TextBox txtTipoTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   4
         Left            =   -68745
         TabIndex        =   63
         Top             =   3735
         Width           =   2625
      End
      Begin VB.TextBox txtTipoTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   -68745
         TabIndex        =   62
         Top             =   3375
         Width           =   2625
      End
      Begin VB.TextBox txtTipoTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   -68745
         TabIndex        =   61
         Top             =   3015
         Width           =   2625
      End
      Begin VB.TextBox txtTipoTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   -68745
         TabIndex        =   60
         Top             =   2655
         Width           =   2625
      End
      Begin VB.TextBox txtTipoTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   -68745
         TabIndex        =   59
         Top             =   2295
         Width           =   2625
      End
      Begin VB.TextBox txtNomeSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -72885
         MaxLength       =   60
         TabIndex        =   58
         Top             =   810
         Width           =   6810
      End
      Begin VB.TextBox txtEmailSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -72885
         MaxLength       =   60
         TabIndex        =   57
         Top             =   4410
         Width           =   6810
      End
      Begin VB.TextBox txtEnderecoSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -72885
         MaxLength       =   60
         TabIndex        =   56
         Top             =   5040
         Width           =   6810
      End
      Begin VB.TextBox txtBairroSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -72885
         MaxLength       =   30
         TabIndex        =   55
         Top             =   5670
         Width           =   2625
      End
      Begin VB.TextBox txtMunicipioSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -70140
         MaxLength       =   60
         TabIndex        =   54
         Top             =   5670
         Width           =   4065
      End
      Begin VB.TextBox txtUFSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -70140
         MaxLength       =   2
         TabIndex        =   53
         Top             =   6300
         Width           =   555
      End
      Begin VB.TextBox txtCEPSolicitante 
         Enabled         =   0   'False
         Height          =   330
         Left            =   -72885
         MaxLength       =   30
         TabIndex        =   52
         Top             =   6300
         Width           =   1635
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   -70095
         TabIndex        =   51
         Top             =   2295
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   -70095
         TabIndex        =   50
         Top             =   2655
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   -70095
         TabIndex        =   49
         Top             =   3015
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   -70095
         TabIndex        =   48
         Top             =   3375
         Width           =   915
      End
      Begin VB.TextBox txtRamal 
         Enabled         =   0   'False
         Height          =   330
         Index           =   4
         Left            =   -70095
         TabIndex        =   47
         Top             =   3735
         Width           =   915
      End
      Begin VB.TextBox txtGrauParentesco 
         Enabled         =   0   'False
         Height          =   285
         Left            =   -72885
         TabIndex        =   46
         Top             =   1485
         Width           =   2490
      End
      Begin VB.TextBox txtTelefone 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   -71940
         TabIndex        =   45
         Top             =   2295
         Width           =   1410
      End
      Begin VB.Frame Frame4 
         Caption         =   "Ag�ncia de Contato"
         Height          =   2355
         Left            =   -73785
         TabIndex        =   30
         Top             =   3765
         Width           =   9285
         Begin VB.TextBox txtCodigoAgencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   240
            TabIndex        =   37
            Top             =   480
            Width           =   1215
         End
         Begin VB.TextBox txtDV 
            Enabled         =   0   'False
            Height          =   330
            Left            =   1620
            TabIndex        =   36
            Top             =   495
            Width           =   510
         End
         Begin VB.TextBox txtNomeAgencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   2655
            TabIndex        =   35
            Top             =   495
            Width           =   6360
         End
         Begin VB.TextBox txtEnderecoAgencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   210
            TabIndex        =   34
            Top             =   1170
            Width           =   4965
         End
         Begin VB.TextBox txtMunicipioAgencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   210
            TabIndex        =   33
            Top             =   1845
            Width           =   7620
         End
         Begin VB.TextBox txtUFAgencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   8475
            TabIndex        =   32
            Top             =   1845
            Width           =   555
         End
         Begin VB.TextBox txtBairroAgencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   5250
            TabIndex        =   31
            Top             =   1170
            Width           =   3765
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "C�d da ag�ncia:"
            Height          =   195
            Index           =   1
            Left            =   240
            TabIndex        =   44
            Top             =   240
            Width           =   1170
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "DV:"
            Height          =   195
            Index           =   1
            Left            =   1620
            TabIndex        =   43
            Top             =   270
            Width           =   270
         End
         Begin VB.Label Label12 
            AutoSize        =   -1  'True
            Caption         =   "Nome da ag�ncia"
            Height          =   195
            Index           =   1
            Left            =   2640
            TabIndex        =   42
            Top             =   270
            Width           =   1260
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Endere�o:"
            Height          =   195
            Index           =   1
            Left            =   210
            TabIndex        =   41
            Top             =   945
            Width           =   735
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Munic�pio"
            Height          =   195
            Index           =   1
            Left            =   210
            TabIndex        =   40
            Top             =   1620
            Width           =   705
         End
         Begin VB.Label Label17 
            AutoSize        =   -1  'True
            Caption         =   "UF:"
            Height          =   195
            Left            =   8475
            TabIndex        =   39
            Top             =   1575
            Width           =   255
         End
         Begin VB.Label Label16 
            AutoSize        =   -1  'True
            Caption         =   "Bairro:"
            Height          =   195
            Left            =   5250
            TabIndex        =   38
            Top             =   945
            Width           =   450
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Observa��es"
         Height          =   3555
         Left            =   -73815
         TabIndex        =   19
         Top             =   2820
         Width           =   9240
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   285
            MaxLength       =   70
            TabIndex        =   29
            Top             =   360
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   285
            MaxLength       =   70
            TabIndex        =   28
            Top             =   630
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   285
            MaxLength       =   70
            TabIndex        =   27
            Top             =   915
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   285
            MaxLength       =   70
            TabIndex        =   26
            Top             =   1200
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   285
            MaxLength       =   70
            TabIndex        =   25
            Top             =   1470
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   5
            Left            =   285
            MaxLength       =   70
            TabIndex        =   24
            Top             =   1740
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   6
            Left            =   285
            MaxLength       =   70
            TabIndex        =   23
            Top             =   2010
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   7
            Left            =   285
            MaxLength       =   70
            TabIndex        =   22
            Top             =   2280
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   8
            Left            =   285
            MaxLength       =   70
            TabIndex        =   21
            Top             =   2550
            Width           =   8775
         End
         Begin VB.TextBox txtExigencia 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   9
            Left            =   285
            MaxLength       =   70
            TabIndex        =   20
            Top             =   2820
            Width           =   8775
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Ocorr�ncia"
         Height          =   2295
         Left            =   -73800
         TabIndex        =   13
         Top             =   480
         Width           =   9240
         Begin VB.TextBox txtSubeventoOcorrencia 
            Enabled         =   0   'False
            Height          =   285
            Left            =   360
            TabIndex        =   15
            Top             =   1800
            Width           =   8655
         End
         Begin VB.TextBox txtLocal 
            Enabled         =   0   'False
            Height          =   330
            Left            =   3120
            MaxLength       =   70
            TabIndex        =   89
            Top             =   585
            Width           =   5820
         End
         Begin VB.TextBox txtDtOcorrencia 
            Enabled         =   0   'False
            Height          =   285
            Left            =   330
            TabIndex        =   16
            Top             =   615
            Width           =   1545
         End
         Begin VB.TextBox txtEventoOcorrencia 
            Enabled         =   0   'False
            Height          =   330
            Left            =   330
            TabIndex        =   14
            Top             =   1245
            Width           =   8685
         End
         Begin MSMask.MaskEdBox mskHora 
            Height          =   330
            Left            =   2040
            TabIndex        =   86
            Top             =   600
            Width           =   900
            _ExtentX        =   1588
            _ExtentY        =   582
            _Version        =   393216
            Enabled         =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "_"
         End
         Begin VB.Label Label18 
            Caption         =   "Sub Evento"
            Height          =   255
            Left            =   360
            TabIndex        =   90
            Top             =   1560
            Width           =   2655
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Local "
            Height          =   195
            Index           =   4
            Left            =   3120
            TabIndex        =   88
            Top             =   360
            Width           =   435
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Hora"
            Height          =   195
            Index           =   3
            Left            =   2070
            TabIndex        =   87
            Top             =   360
            Width           =   345
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Data da Ocorr�ncia"
            Height          =   195
            Index           =   0
            Left            =   330
            TabIndex        =   18
            Top             =   360
            Width           =   1395
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Evento Causador da Ocorr�ncia"
            Height          =   195
            Index           =   0
            Left            =   330
            TabIndex        =   17
            Top             =   1020
            Width           =   2280
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Dados do Sinistrado"
         Height          =   2400
         Left            =   -73785
         TabIndex        =   4
         Top             =   1035
         Width           =   9285
         Begin VB.TextBox txtDtNascimentoSinistrado 
            Enabled         =   0   'False
            Height          =   285
            Left            =   5805
            TabIndex        =   8
            Top             =   1170
            Width           =   1500
         End
         Begin VB.TextBox txtCPFSinistrado 
            Enabled         =   0   'False
            Height          =   285
            Left            =   270
            TabIndex        =   7
            Top             =   1170
            Width           =   2355
         End
         Begin VB.TextBox txtSexoSinistrado 
            Enabled         =   0   'False
            Height          =   285
            Left            =   270
            TabIndex        =   6
            Top             =   1845
            Width           =   2355
         End
         Begin VB.TextBox txtNomeSinistrado 
            Enabled         =   0   'False
            Height          =   330
            Left            =   270
            MaxLength       =   60
            TabIndex        =   5
            Top             =   495
            Width           =   8700
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Sexo:"
            Height          =   195
            Left            =   270
            TabIndex        =   12
            Top             =   1620
            Width           =   405
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Data de Nascimento:"
            Height          =   195
            Index           =   1
            Left            =   5805
            TabIndex        =   11
            Top             =   945
            Width           =   1500
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "C.P.F.:"
            Height          =   195
            Index           =   1
            Left            =   270
            TabIndex        =   10
            Top             =   945
            Width           =   480
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Nome:"
            Height          =   195
            Left            =   270
            TabIndex        =   9
            Top             =   270
            Width           =   465
         End
      End
      Begin GridAlianca.grdAlianca GridPropostaAfetada 
         Height          =   5295
         Left            =   240
         TabIndex        =   92
         Top             =   600
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   9340
         BorderStyle     =   1
         AllowUserResizing=   3
         EditData        =   0   'False
         Highlight       =   1
         ShowTip         =   0   'False
         SortOnHeader    =   0   'False
         BackColor       =   -2147483643
         BackColorBkg    =   -2147483633
         BackColorFixed  =   -2147483633
         BackColorSel    =   -2147483635
         FixedCols       =   1
         FixedRows       =   1
         FocusRect       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   -2147483640
         ForeColorFixed  =   -2147483630
         ForeColorSel    =   -2147483634
         GridColor       =   -2147483630
         GridColorFixed  =   12632256
         GridLine        =   1
         GridLinesFixed  =   2
         MousePointer    =   0
         Redraw          =   -1  'True
         Rows            =   2
         TextStyle       =   0
         TextStyleFixed  =   0
         Cols            =   2
         RowHeightMin    =   0
      End
      Begin MSMask.MaskEdBox mskCpfSolicitante '07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante (inicio)
         Height          =   330
         Left            =   4920
         TabIndex        =   93
         Top             =   1425
         Width           =   2670
         _ExtentX        =   4710
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   11
         Mask            =   "###########"
         PromptChar      =   "_"
      End
      Begin VB.Label Label11 
         Caption         =   "CPF:"
         Height          =   255
         Index           =   2
         Left            =   4965
         TabIndex        =   94
         Top             =   1200
         Width           =   375
      End '07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante (fim)
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Nome:"
         Height          =   195
         Left            =   -72885
         TabIndex        =   84
         Top             =   585
         Width           =   465
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "D.D.D.:"
         Height          =   195
         Index           =   2
         Left            =   -72885
         TabIndex        =   83
         Top             =   2070
         Width           =   540
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Telefone:"
         Height          =   195
         Index           =   2
         Left            =   -71895
         TabIndex        =   82
         Top             =   2070
         Width           =   675
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "E-mail:"
         Height          =   195
         Left            =   -72885
         TabIndex        =   81
         Top             =   4185
         Width           =   465
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Endere�o:"
         Height          =   195
         Left            =   -72885
         TabIndex        =   80
         Top             =   4815
         Width           =   735
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Bairro:"
         Height          =   195
         Left            =   -72885
         TabIndex        =   79
         Top             =   5445
         Width           =   450
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Munic�pio:"
         Height          =   195
         Left            =   -70140
         TabIndex        =   78
         Top             =   5445
         Width           =   750
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "U.F.:"
         Height          =   195
         Index           =   0
         Left            =   -70140
         TabIndex        =   77
         Top             =   6075
         Width           =   345
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "CEP:"
         Height          =   195
         Index           =   0
         Left            =   -72885
         TabIndex        =   76
         Top             =   6075
         Width           =   360
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "Grau de parentesco:"
         Height          =   195
         Index           =   0
         Left            =   -72885
         TabIndex        =   75
         Top             =   1260
         Width           =   1455
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Ramal:"
         Height          =   195
         Index           =   0
         Left            =   -70095
         TabIndex        =   74
         Top             =   2070
         Width           =   495
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Telefone:"
         Height          =   195
         Left            =   -68745
         TabIndex        =   73
         Top             =   2070
         Width           =   1035
      End
   End
End
Attribute VB_Name = "SEGP0794_7"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public sinistro_id As String
Public produto_id As String
Public evento_id As String
Public CONTADORERRO As Integer

'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
Public tp_sinistro_parametro_id As Integer 'parametro de classifica��o do sinistro
'C00216281 - FIM

'inicio - IM00316531 - 26/04/2018
Public Obj_sinis_Descricao As String ''
Public Obj_sinis_valor_estimado As String ''
Public Obj_sinis_tp_cobertura_id As String ''
'fim - IM00316531 - 26/04/2018

Private Sub cmdAvisar_Click()
Dim i As Long
Dim b As Long

Dim benef As BenefSinistro
Dim SinistroBB As String

On Error GoTo Erro

If Trim(GridPropostaAfetada.TextMatrix(1, 2)) = "Sem Proposta" Then
    Call MsgBox("Voc� estar� avisando um sinistro sem proposta espec�fica.", vbInformation, "Aviso de Sinistro")
    
    If MsgBox("Tem certeza de que deseja continuar?", vbYesNo, "Sinistro sem proposta") = vbNo Then Exit Sub
End If

Me.MousePointer = vbHourglass
'Thiago Lemos (Stefanini) 26/05/2006
'Caso a proposta, n�o possua Proposta_BB, Exibe Mensagem de Erro e n�o efetua o Aviso
If ObtemDadosSinistros <> 0 Then
    MsgBox "Proposta BB n�o localizada. O Aviso de Sinistro n�o pode ser Aberto", vbCritical, "ERRO - Proposta BB"
    Me.MousePointer = vbDefault
    Exit Sub
End If
                                
Me.MousePointer = vbDefault

'Rog�rio (Stefanini) - 18/02/2006
'Prevenir erro de grava��o por uso do caracter ' (ASCII 39) nos campos de digita��o livre.
'Substitui pelo caracter � (ASCII 180)
For i = 0 To 9
    SEGP0794_3.txtExigencia(i) = Replace(SEGP0794_3.txtExigencia(i), Chr(39), Chr(180))
Next

For i = 0 To 4
    SEGP0794_4_1.txtMotivo(i) = Replace(SEGP0794_4_1.txtMotivo(i), Chr(39), Chr(180))
Next

For i = 0 To 9
    SEGP0794_7.txtExigencia(i) = Replace(SEGP0794_7.txtExigencia(i), Chr(39), Chr(180))
Next

If (MsgBox("Voc� avisar� " & GridPropostaAfetada.Rows - 1 & " propostas. Tem certeza?", vbYesNo, "Aviso de Sinistro") = vbYes) Then
    Me.MousePointer = vbHourglass
    '----------------------------------------------------------------------
    'data: 31/10/2006 - Alterado por Cleber - Flow: 176233
    'Desabilita os bot�es avisar e voltar para o usu�rio n�o ficar clicando
    '   durante o processamento, podendo ocorrer algum tipo de erro.
    '----------------------------------------------------------------------
    cmdAvisar.Enabled = False
    cmdVoltar.Enabled = False
    If GravaSinistros Then
        Me.Hide
            
        Dim SQL As String
        Dim rsConsultarQuestionario As ADODB.Recordset
      
        SQL = "exec SEGS9411_SPS " & CInt(SEGP0794_7.produto_id) & ", " & CDbl(SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex))
    
        Set rsConsultarQuestionario = ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            SQL, _
                                            True)
                                                   
            If rsConsultarQuestionario.EOF Then
                With SEGP0794_fim
                    .stbQtd.SimpleText = SEGP0794_7.GridPropostaAfetada.Rows - 1 & " sinistro(s) avisado(s)."
                    .grdPropAvis.Rows = 1
                    For i = 1 To SEGP0794_7.GridPropostaAfetada.Rows - 1
                        .grdPropAvis.Rows = .grdPropAvis.Rows + 1
                        'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
                        'INSER�AO CLASSIFICA��O NO GRID
                        If Proposta_Avisada(i).Situacao = 1 Then
                        .grdPropAvis.TextMatrix(i, 0) = "Deferido no Aviso"
                        Else
                        .grdPropAvis.TextMatrix(i, 0) = "N�o se aplica"
                        End If
                        'C00216281 - FIM
                        .grdPropAvis.TextMatrix(i, 1) = IIf(Not IsNull(Proposta_Avisada(i).sinistro_id), Proposta_Avisada(i).sinistro_id, "")
                        .grdPropAvis.TextMatrix(i, 2) = IIf(Not IsNull(Proposta_Avisada(i).proposta_id), Proposta_Avisada(i).proposta_id, "")
                        .grdPropAvis.TextMatrix(i, 3) = IIf(Not IsNull(Proposta_Avisada(i).produto), Proposta_Avisada(i).produto, "")
                    Next i
                    Me.Hide
                    .Show
                    .VerificarPagtoImediato
                End With
            Else
                SEGP0794_7_2.Show
            End If
    End If
    
    cmdAvisar.Enabled = True  'data: 31/10/2006 - Alterado por Cleber - Flow: 176233
    cmdVoltar.Enabled = True  'data: 31/10/2006 - Alterado por Cleber - Flow: 176233
    Me.MousePointer = vbDefault
End If

Exit Sub

Resume
Erro:
    Call TrataErroGeral("Avisar", "Aviso de Sinistro")
    Exit Sub
End Sub

Private Sub cmdSair_Click()
  Sair Me
End Sub

Private Sub cmdVoltar_Click()
   Dim i As Long	
   Me.Hide
   ' Cesar Santos CONFITEC - (SBRJ009952) 02/06/2020 inicio
   For i = 1 To (GridPropostaAfetada.Rows - 1)
        If GridPropostaAfetada.TextMatrix(i, 12) = 1243 Then
                If (SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex)) = 425 Then
                   SEGP0794_7_3.Show
                   Exit Sub
                End If
        End If
   Next i
   ' Cesar Santos CONFITEC - (SBRJ009952) 02/06/2020 fim
   SEGP0794_6.Show
End Sub

Private Sub Form_Load()
    PreencheGridTipo
End Sub

Private Sub PreencheGridTipo()

    With GridTipo
        .Cols = 5
              
        '(IN�CIO) -- RSouza -- 27/07/2010 -----------------------
        '.Rows = 3
        .Rows = 4
        '(FIM)    -- RSouza -- 27/07/2010 -----------------------
        
        .TextMatrix(0, 0) = " "
        .TextMatrix(0, 1) = "C�digo"
        .TextMatrix(0, 2) = "Descri��o"
        .TextMatrix(1, 1) = "AVSR"
        .TextMatrix(1, 2) = "A Avisar"
        .TextMatrix(2, 1) = "RNLS"
        .TextMatrix(2, 2) = "Solicita��o de Reanalise"
        
        
        '(IN�CIO) -- RSouza -- 27/07/2010 -----------------------
        .TextMatrix(3, 1) = "SRGL"
        .TextMatrix(3, 2) = "Sinistro em regula��o"
        '(FIM)    -- RSouza -- 27/07/2010 -----------------------
       
        .TextMatrix(0, 3) = "C�digo"
        .TextMatrix(0, 4) = "Descri��o"
        .TextMatrix(1, 3) = "PSCB"
        .TextMatrix(1, 4) = "N�o Avisar - Proposta Sem Cobertura"
        .TextMatrix(2, 3) = "NAVS"
        .TextMatrix(2, 4) = "N�o Avisar"
        
        .AutoFit H�brido
    End With
    
End Sub
'Thiago Lemos (Stefanini) 26/05/2006
'M�todo Transformado para Fun��o para tratamento de Propostas BB nulas
Public Function ObtemDadosSinistros() As Integer
Dim i As Long
Dim coberturas() As Long
Dim val_is() As Double
Dim subgrupo As Integer

Dim estim As estimativa
Dim cobert() As CoberturaAfetada

Dim qtd_cob_ating As Integer
Dim cob_atingida As Boolean
Dim sem_proposta As Boolean
Dim tp_cobertura_id As Integer

'para reanalise
Dim sinistro_id_ant As String
Dim sinistro_bb_id As String
Dim SQL As String
Dim rs As ADODB.Recordset
Dim dadosAviso As Aviso_Sinistro

On Error GoTo TrataErro

    'Dados do Sinistro
    ReDim Avisos_Sinistros(1 To GridPropostaAfetada.Rows - 1) As Aviso_Sinistro
    
    'Dados das Propostas Avisadas
    ReDim Proposta_Avisada(1 To GridPropostaAfetada.Rows - 1) As Proposta_Avisada
    
    'Armazena as propostas
    For i = 1 To GridPropostaAfetada.Rows - 1
       
        Set Avisos_Sinistros(i).Sinistro = New Sinistro
        With Avisos_Sinistros(i)
            '---------------- ARICARDO - CWI - 24/11 ----------------------------
            If mvarFlagCredito = True Then
            
                .Sinistro.FlagCredito = "S"
            Else
            
                .Sinistro.FlagCredito = "N"
            End If

           '----------------------------------------------------------------------
           .Sinistro.Ind_reanalise = IIf(GridPropostaAfetada.TextMatrix(i, 1) = "AVSR", "N", "S")
                      
           '--------------------------------------------------------------
           'Obten��o de dados gerais
           '--------------------------------------------------------------
         
            Obtem_Evento_SEGBR
            .Sinistro.Localizacao = Localizacao
            .Sinistro.Evento_Segbr_id = Evento_Segbr_id
            .Sinistro.Evento_bb_id = Evento_bb_id
            evento_id = Evento_Segbr_id
            
                    
            'LUPALUDETO - 02/12/2010
            'Call Busca_Numero_Remessa(.Sinistro)
            If Not Busca_Numero_Remessa(.Sinistro) Then
                MsgBox "N�o foi poss�vel gerar n�mero de remeesa", vbCritical, SEGP0794_7.Caption
                GoTo TrataErro
            End If
            
            If SEGP0794_2.GrdResultadoPesquisa.Rows > 1 Then
                .Sinistro.SinistradoClienteId = SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 1)
            Else
                .Sinistro.SinistradoClienteId = 0
            End If
            
            'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
            '.Sinistro.SinistradoNomeCliente = SEGP0794_3.txtNomeSinistrado.Text
            .Sinistro.SinistradoNomeCliente = MudaAspaSimples(SEGP0794_3.txtNomeSinistrado.Text)
            'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Fim
            
            .Sinistro.SinistradoDtNasc = IIf(SEGP0794_3.mskDtNascimento = "__/__/____", "", Format(SEGP0794_3.mskDtNascimento, "yyyymmdd"))
            
            If SEGP0794_3.Label2(1).Caption = "CGC :" Then 'cristovao.rodrigues 24/04/2014 - 17919477
                .Sinistro.SinistradoCPF = IIf(SEGP0794_3.mskCPF = "__.___.___/____-__", "", DatCGC(SEGP0794_3.mskCPF))
            Else
                .Sinistro.SinistradoCPF = IIf(SEGP0794_3.mskCPF = "___.___.___-__", "", DatCPF(SEGP0794_3.mskCPF))
            End If
            
            .Sinistro.SinistradoSexo = IIf((Left(SEGP0794_3.cboSexo.Text, 1) = "I") Or (SEGP0794_3.cboSexo.Text = ""), "N", Left(SEGP0794_3.cboSexo.Text, 1))
            
            .Sinistro.dt_ocorrencia = Format(SEGP0794_3.mskDataOcorrencia, "yyyymmdd")
            .Sinistro.HoraOcorrencia = Format(SEGP0794_3.mskHora, "hhmm")
            .Sinistro.LocalOcorrencia = SEGP0794_3.txtLocal.Text
            
            .Sinistro.Causa_id = SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex)
            .Sinistro.Causa = SEGP0794_3.cboEvento.Text
            If SEGP0794_3.cboSubEvento.ListIndex = -1 Then
               .Sinistro.SubCausa_id = 0
               .Sinistro.SubCausa = ""
            Else
               .Sinistro.SubCausa_id = SEGP0794_3.cboSubEvento.ItemData(SEGP0794_3.cboSubEvento.ListIndex)
               .Sinistro.SubCausa = SEGP0794_3.cboSubEvento.Text
            End If
          
            .Sinistro.Banco = banco_id
            .Sinistro.AgenciaNome = SEGP0794_6.txtNomeAgencia
            .Sinistro.AgenciaAviso = SEGP0794_6.txtCodigoAgencia.Text
            .Sinistro.AgenciaDvAviso = SEGP0794_6.txtDV.Text
            .Sinistro.AgenciaEndereco = SEGP0794_6.txtEndereco.Text & " - " & _
                                        SEGP0794_6.txtBairro.Text & " " & vbCrLf & _
                                        Trim(SEGP0794_6.txtMunicipio.Text) & "-"
            .Sinistro.AgenciaEndereco = .Sinistro.AgenciaEndereco & SEGP0794_6.txtUF.Text & ""
            
            'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
            '.Sinistro.SolicitanteNome = SEGP0794_5.txtNomeSolicitante.Text
            .Sinistro.SolicitanteNome = MudaAspaSimples(SEGP0794_5.txtNomeSolicitante.Text)
            'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Fim
            
            .Sinistro.SolicitanteEndereco = Trim(SEGP0794_5.txtEnderecoSolicitante.Text)
            .Sinistro.SolicitanteMunicipio = Trim(SEGP0794_5.cmbMunicipio.Text)
            .Sinistro.SolicitanteMunicipio_id = SEGP0794_5.municipio_id
            .Sinistro.SolicitanteBairro = Trim(SEGP0794_5.txtBairroSolicitante.Text)
            .Sinistro.SolicitanteEstado = SEGP0794_5.cmbUF.Text
            .Sinistro.SolicitanteCEP = Replace(SEGP0794_5.txtCEPSolicitante.Text, "-", "")
            .Sinistro.SolicitanteCPF = IIf((SEGP0794_5.mskCpfSolicitante.Text = "___________"), "", SEGP0794_5.mskCpfSolicitante.Text)  '07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante
            .Sinistro.SolicitanteEmail = SEGP0794_5.txtEmailSolicitante.Text
            .Sinistro.ddd1 = IIf((txtDDD(0).Text = "__"), "", txtDDD(0).Text)
            .Sinistro.ddd2 = IIf((txtDDD(1).Text = "__"), "", txtDDD(1).Text)
            .Sinistro.ddd3 = IIf((txtDDD(2).Text = "__"), "", txtDDD(2).Text)
            .Sinistro.ddd4 = IIf((txtDDD(3).Text = "__"), "", txtDDD(3).Text)
            .Sinistro.ddd5 = IIf((txtDDD(4).Text = "__"), "", txtDDD(4).Text)
            '30/05/2012 : Nova - Mauro Vianna : altera��o telefone 9 d�gitos (tratamento para telefones de 8 d�gitos)
'            .Sinistro.telefone1 = IIf(txtTelefone(0).Text = "____-____", "", Replace(txtTelefone(0).Text, "-", ""))
'            .Sinistro.telefone2 = IIf(txtTelefone(1).Text = "____-____", "", Replace(txtTelefone(1).Text, "-", ""))
'            .Sinistro.telefone3 = IIf(txtTelefone(2).Text = "____-____", "", Replace(txtTelefone(2).Text, "-", ""))
'            .Sinistro.telefone4 = IIf(txtTelefone(3).Text = "____-____", "", Replace(txtTelefone(3).Text, "-", ""))
'            .Sinistro.telefone5 = IIf(txtTelefone(4).Text = "____-____", "", Replace(txtTelefone(4).Text, "-", ""))
            .Sinistro.telefone1 = IIf(txtTelefone(0).Text = "____-____", "", Trim(Replace(txtTelefone(0).Text, "-", "")))
            .Sinistro.telefone2 = IIf(txtTelefone(1).Text = "____-____", "", Trim(Replace(txtTelefone(1).Text, "-", "")))
            .Sinistro.telefone3 = IIf(txtTelefone(2).Text = "____-____", "", Trim(Replace(txtTelefone(2).Text, "-", "")))
            .Sinistro.telefone4 = IIf(txtTelefone(3).Text = "____-____", "", Trim(Replace(txtTelefone(3).Text, "-", "")))
            .Sinistro.telefone5 = IIf(txtTelefone(4).Text = "____-____", "", Trim(Replace(txtTelefone(4).Text, "-", "")))
            .Sinistro.ramal1 = IIf(txtRamal(0).Text = "", "", txtRamal(0).Text)
            .Sinistro.ramal2 = IIf(txtRamal(1).Text = "", "", txtRamal(1).Text)
            .Sinistro.ramal3 = IIf(txtRamal(2).Text = "", "", txtRamal(2).Text)
            .Sinistro.ramal4 = IIf(txtRamal(3).Text = "", "", txtRamal(3).Text)
            .Sinistro.ramal5 = IIf(txtRamal(4).Text = "", "", txtRamal(4).Text)
            .Sinistro.NumProcRemessa = "0001"
            
         ' Raimundo - GPTI - 12/05/2009
         ' Flow 935241
         ' Verifica se para casos de aviso sem proposta, o campo � num�rico ou n�o
            If IsNumeric(GridPropostaAfetada.TextMatrix(i, 5)) Then
         'Fim
         
         ' Raimundo/ Afonso - GPTI - 30/03/2009
         ' Flow 839670 - Segurado Jose Carlos Said Handan
         ' Evita que o n�mero CD_PRD de uma proposta replique em outra

                Dim SQL1 As String
                Dim RsAux As ADODB.Recordset
                
                SQL1 = " SELECT produto_bb "
                SQL1 = SQL1 & " FROM produto_bb_sinistro_tb b with (nolock) "
                SQL1 = SQL1 & " INNER JOIN proposta_tb a with (nolock) "
                SQL1 = SQL1 & " ON a.proposta_id = " & GridPropostaAfetada.TextMatrix(i, 5)
                SQL1 = SQL1 & " WHERE b.produto_id = a.produto_id "
                SQL1 = SQL1 & " AND b.dt_inicio_vigencia <= a.dt_contratacao "
                SQL1 = SQL1 & " AND (dt_fim_vigencia >= a.dt_contratacao OR dt_fim_vigencia is null)"
                SQL1 = SQL1 & " UNION "
                SQL1 = SQL1 & " SELECT PRODUTO_ID "
                SQL1 = SQL1 & " FROM PROPOSTA_TB with (nolock) "
                SQL1 = SQL1 & " WHERE PROPOSTA_ID = " & GridPropostaAfetada.TextMatrix(i, 5)
                SQL1 = SQL1 & " AND ORIGEM_PROPOSTA_ID=2"
                
                Set RsAux = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL1, True)
                
                If Not RsAux.EOF Then
                    produto_id = RsAux.Fields(0)
                    RsAux.Close
                    SQL1 = "set nocount on exec obtem_produto_sps 1," & produto_id
                    Set RsAux = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL1, True)
                    If Not RsAux.EOF Then '3
                        .Sinistro.CD_PRD = RsAux(0)
                        .Sinistro.CD_MDLD = RsAux(1)
                        .Sinistro.CD_ITEM_MDLD = RsAux(2)
                    Else
                        .Sinistro.CD_PRD = 0
                        .Sinistro.CD_MDLD = 0
                        .Sinistro.CD_ITEM_MDLD = 0
                    End If
                    RsAux.Close
                Else
                    RsAux.Close
                    .Sinistro.CD_PRD = 0
                    .Sinistro.CD_MDLD = 0
                    .Sinistro.CD_ITEM_MDLD = 0
                End If
             Else
                .Sinistro.CD_PRD = 0
                .Sinistro.CD_MDLD = 0
                .Sinistro.CD_ITEM_MDLD = 0
            End If
            'Raimundo - Fim da altera��o
         
            If SEGP0794_5.cmbTipoTelefone(0).Enabled Then
                 .Sinistro.tp_telefone1 = IIf(SEGP0794_5.cmbTipoTelefone(0).ListIndex <> -1, SEGP0794_5.cmbTipoTelefone(0).ItemData(SEGP0794_5.cmbTipoTelefone(0).ListIndex), "null")
            Else
                 .Sinistro.tp_telefone1 = ""
            End If
            If SEGP0794_5.cmbTipoTelefone(1).Enabled Then
                 .Sinistro.tp_telefone2 = IIf(SEGP0794_5.cmbTipoTelefone(1).ListIndex <> -1, SEGP0794_5.cmbTipoTelefone(1).ItemData(SEGP0794_5.cmbTipoTelefone(1).ListIndex), "null")
            Else
                 .Sinistro.tp_telefone2 = ""
            End If
            If SEGP0794_5.cmbTipoTelefone(2).Enabled Then
                 .Sinistro.tp_telefone3 = IIf(SEGP0794_5.cmbTipoTelefone(2).ListIndex <> -1, SEGP0794_5.cmbTipoTelefone(2).ItemData(SEGP0794_5.cmbTipoTelefone(2).ListIndex), "null")
            Else
                 .Sinistro.tp_telefone3 = ""
            End If
            If SEGP0794_5.cmbTipoTelefone(3).Enabled Then
                 .Sinistro.tp_telefone4 = IIf(SEGP0794_5.cmbTipoTelefone(3).ListIndex <> -1, SEGP0794_5.cmbTipoTelefone(3).ItemData(SEGP0794_5.cmbTipoTelefone(3).ListIndex), "null")
            Else
                 .Sinistro.tp_telefone4 = ""
            End If
            If SEGP0794_5.cmbTipoTelefone(4).Enabled Then
                 .Sinistro.tp_telefone5 = IIf(SEGP0794_5.cmbTipoTelefone(4).ListIndex <> -1, SEGP0794_5.cmbTipoTelefone(4).ItemData(SEGP0794_5.cmbTipoTelefone(4).ListIndex), "null")
            Else
                 .Sinistro.tp_telefone5 = ""
            End If
            .Sinistro.SolicitanteGrauParentesco = SEGP0794_5.cmbGrauParentesco.ItemData(SEGP0794_5.cmbGrauParentesco.ListIndex)
            
            
          '--------------------------------------------------------------
          'Obten��o de dados espec�ficos
          '--------------------------------------------------------------
                       
          'Para sinistro sem proposta
          If Trim(GridPropostaAfetada.TextMatrix(1, 2)) = "Sem Proposta" Then
              Call ObtemDadosSinistrosSemPropostas(.Sinistro, i)
          Else
              
              .Sinistro.NomeProduto = GridPropostaAfetada.TextMatrix(i, 4)
              
              'sergio.so - CORRE��O PARA TRATAMENTO DE TITULAR / C�NJUGE - 30/11/2010
              '.Sinistro.IndTpSinistrado = IIf(GridPropostaAfetada.TextMatrix(i, 10) = "Titular", "T", "C")
              '.Sinistro.SeguradoComponente = IIf(GridPropostaAfetada.TextMatrix(i, 10) = "Titular", 1, 3)
              'sergio.eo - CORRE��O PARA TRATAMENTO DE TITULAR / C�NJUGE - 30/11/2010
              
              'sergio.sn - CORRE��O PARA TRATAMENTO DE TITULAR / C�NJUGE - 30/11/2010
              '.Sinistro.IndTpSinistrado = IIf((UCase$(GridPropostaAfetada.TextMatrix(i, 10)) Like "TITULAR*") = True, 1, 3)
              'dicaversan -
              For N = 1 To SEGP0794_4.GrdResultadoPesquisa.Rows - 1
                If GridPropostaAfetada.TextMatrix(i, 5) = SEGP0794_4.GrdResultadoPesquisa.TextMatrix(Int(N), 5) Then
                    'Ricardo Toledo (Confitec) : 23/12/2011 : INC000003066777 : Inicio
                    'Adicionado + 1 compara��o no tratamento de Titular/C�njuge
                    If GridPropostaAfetada.TextMatrix(i, 10) = SEGP0794_4.GrdResultadoPesquisa.TextMatrix(Int(N), 10) Then
                    'Ricardo Toledo (Confitec) : 23/12/2011 : INC000003066777 : Fim
                        Exit For
                    End If
                End If
              Next N
              '.Sinistro.IndTpSinistrado = IIf((UCase$(SEGP0794_4.GrdResultadoPesquisa.TextMatrix(Int(n), 10)) Like "TITULAR*") = True, 1, 3)
              '.Sinistro.IndTpSinistrado = UCase$(Left(GridPropostaAfetada.TextMatrix(i, 10), 1))
              
            'Ricardo Toledo (Confitec) : 03/01/2012 : INC000003145966 : Inicio
            'Devido � altera��o anterior (relacionada ao chamado INC000003066777) houve a necessidade de inserir + essa verifica��o.
            'O problema anterior havia sido solucionado, por�m, surgiu outro problema (subcript out of range) em outras situa��es.
              If N > SEGP0794_4.GrdResultadoPesquisa.Rows - 1 Then
                For N = 1 To SEGP0794_4.GrdResultadoPesquisa.Rows - 1
                  If GridPropostaAfetada.TextMatrix(i, 5) = SEGP0794_4.GrdResultadoPesquisa.TextMatrix(Int(N), 5) Then
                        Exit For
                  End If
                Next N
              End If
            'Ricardo Toledo (Confitec) : 03/01/2012 : INC000003145966 : fim
              
              'sergio.sn - 15/12/2010 - CORRE��O PARA TRATAMENTO DE TITULAR / C�NJUGE
              .Sinistro.IndTpSinistrado = UCase$(Left(SEGP0794_4.GrdResultadoPesquisa.TextMatrix(Int(N), 10), 1))
              'sergio.en - 15/12/2010 - CORRE��O PARA TRATAMENTO DE TITULAR / C�NJUGE
              
'              .Sinistro.SeguradoComponente = IIf(GridPropostaAfetada.TextMatrix(i, 10) = "Titular", 1, 3)
'

              'eduardo.amaral (Nova Consultoria) 02/03/2012 Demanda: 12363945 - BB Seguro Vida Empresa FLEX
              'Altera��o realizada para usar apenas SeguradoComponente e n�o mais IndTpSinistrado
              .Sinistro.SeguradoComponente = GridPropostaAfetada.TextMatrix(i, SEGP0794_6.Coluna)
              .Sinistro.NomeTipoComponente = GridPropostaAfetada.TextMatrix(i, 10)
              'fim eduardo.amaral(nova consultoria)
                            
            
              'Se n�o for reanalise
              If .Sinistro.Ind_reanalise = "N" Then
                    .Sinistro.proposta_id = GridPropostaAfetada.TextMatrix(i, 5)
                    .Sinistro.Moeda = GridPropostaAfetada.TextMatrix(i, 20)
                    .Sinistro.Seguradora_id = GridPropostaAfetada.TextMatrix(i, 16)
                    .Sinistro.Sucursal_id = GridPropostaAfetada.TextMatrix(i, 15)
                    .Sinistro.ramo_id = GridPropostaAfetada.TextMatrix(i, 7)
                    Call Busca_Numero_Sinistro(.Sinistro)
                    
                    .Sinistro.Apolice_id = GridPropostaAfetada.TextMatrix(i, 8)
                    .Sinistro.produto_id = GridPropostaAfetada.TextMatrix(i, 12)
                    .Sinistro.PropostaBB = Trim(GridPropostaAfetada.TextMatrix(i, 6))
                    .Sinistro.SitProposta = GridPropostaAfetada.TextMatrix(i, 3)
                    .Sinistro.dt_Aviso = Format(Data_Sistema, "yyyymmdd")
                    
                    'sergio.so - CORRE��O TITULAR / CONJUGE - 30/11/2010
                    '.Sinistro.SeguradoComponente = IIf(GridPropostaAfetada.TextMatrix(i, 10) = "Titular", 1, 3)
                    'sergio.eo
                    
                    'sergio.sn - CORRE��O TITULAR / CONJUGE - 30/11/2010
                    '.Sinistro.SeguradoComponente = IIf((UCase$(GridPropostaAfetada.TextMatrix(i, 10)) Like "TITULAR*") = True, 1, 3)

                    'sergio.en - CORRE��O TITULAR / CONJUGE - 30/11/2010
                    
                    'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
                    '.Sinistro.SeguradoNomeCliente = GridPropostaAfetada.TextMatrix(i, 9)
                    .Sinistro.SeguradoNomeCliente = MudaAspaSimples(GridPropostaAfetada.TextMatrix(i, 9))
                    'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Fim
                    
                    .Sinistro.SeguradoCPF_CNPJ = GridPropostaAfetada.TextMatrix(i, 17)
                    .Sinistro.SeguradoSexo = GridPropostaAfetada.TextMatrix(i, 18)
                    .Sinistro.SeguradoDtNasc = Format(GridPropostaAfetada.TextMatrix(i, 19), "yyyymmdd")
                    
'                     'O segurado � o sinistrado para o BB
'                    .sinistro.SeguradoNomeCliente = .sinistro.SinistradoNomeCliente
'                    .sinistro.SeguradoCPF_CNPJ = .sinistro.SeguradoCPF_CNPJ
'                    .sinistro.SeguradoSexo = .sinistro.SinistradoSexo
'                    .sinistro.SeguradoDtNasc = .sinistro.SinistradoDtNasc

                    'sergio.so - CORRE��O TITULAR / CONJUGE - 30/11/2010
                    '.Sinistro.IndTpSinistrado = IIf(GridPropostaAfetada.TextMatrix(i, 10) = "Titular", "T", "C")
                    'sergio.en - CORRE��O TITULAR / CONJUGE - 30/11/2010
                    
                    'sergio.sn - CORRE��O TITULAR / CONJUGE - 30/11/2010
                    '.Sinistro.IndTpSinistrado = UCase$(Left(GridPropostaAfetada.TextMatrix(i, 10), 1))
                    'sergio.en - CORRE��O TITULAR / CONJUGE - 30/11/2010
                    
                    .Sinistro.SeguradoCliente_id = GridPropostaAfetada.TextMatrix(i, 13)
                    
              'Se for rean�lise
              Else
                   If SEGP0794_4.VerificaReanalise(GridPropostaAfetada.TextMatrix(i, 5), _
                                  Format(SEGP0794_3.mskDataOcorrencia.Text, "yyyymmdd"), _
                                  SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex), _
                                  GridPropostaAfetada.TextMatrix(i, 17), _
                                  Format(GridPropostaAfetada.TextMatrix(i, 19), "yyyymmdd"), _
                                  sinistro_id_ant, _
                                  sinistro_bb_id) <> "" Then
                       
                       SQL = ""
                       SQL = SQL & " set nocount on "
                       SQL = SQL & " exec busca_dados_sinistro_reanalise_sps " & sinistro_id_ant
                       
                       Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            SQL, _
                                            True)
                       If Not rs.EOF Then
                           
                            .Sinistro.Seguradora_id = rs!seguradora_cod_susep
                            .Sinistro.Sucursal_id = rs!sucursal_seguradora_id
                            .Sinistro.Apolice_id = rs!Apolice_id
                            .Sinistro.ramo_id = rs!ramo_id
                            .Sinistro.sinistro_id = sinistro_id_ant
                            .Sinistro.dt_Aviso = Format(rs!dt_aviso_sinistro, "yyyymmdd")
                            
                            .Sinistro.produto_id = Format(rs!produto_id, "000000000")
                            
                            .Sinistro.proposta_id = rs!proposta_id
                           'INCINC000004318890  - HENRIQUE H. HENRIQUES (CONFITEC 30/04/2014) INICIO
                            '.Sinistro.AgenciaAviso = rs!agencia_aviso_id
                            .Sinistro.AgenciaAviso = IIf(IsNull(rs!agencia_aviso_id), 0, rs!agencia_aviso_id)
                           'INCINC000004318890  - HENRIQUE H. HENRIQUES (CONFITEC 30/04/2014) FIM
                            .Sinistro.SeguradoCliente_id = rs!cliente_id
                            .Sinistro.SeguradoNomeCliente = rs!nome_segurado
                            .Sinistro.SeguradoCPF_CNPJ = rs!cpf_cgc_segurado
                            .Sinistro.SeguradoSexo = IIf(IsNull(rs!sexo_segurado), "", rs!sexo_segurado)
                            .Sinistro.SeguradoDtNasc = Format(rs!dt_nascimento_segurado, "yyyymmdd")
                            
                            .Sinistro.Endosso_id = IIf(IsNull(rs!Endosso_id), "", rs!Endosso_id)
                            .Sinistro.Sinistro_id_lider = IIf(IsNull(rs!Sinistro_id_lider), "", rs!Sinistro_id_lider)
                            'Thiago Lemos (Stefanini) 26/05/2006
                            'Caso a proposta, n�o possua Proposta_BB, retorna erro
                            If IsNull(rs!Proposta_BB) Then
                                ObtemDadosSinistros = 1
                                Exit Function
                            End If
                            .Sinistro.PropostaBB = rs!Proposta_BB
                            .Sinistro.SituacaoSinistro = rs!situacao_sinistro
                            .Sinistro.Moeda = rs!moeda_id
                            .Sinistro.CodRamo = rs!cod_ramo
                            
                        End If 'RS.EOF
                   End If 'VerificaReanalise
             End If 'Caso de reanalise
         End If  'Caso sem proposta
        
           
        '--------------------------------------------------------------
        'Obten��o de dados da cobertura
        '--------------------------------------------------------------
        
         If .Sinistro.Ind_reanalise = "N" Then
               ReDim coberturas(0) As Long
               If (Trim(GridPropostaAfetada.TextMatrix(1, 2)) = "Sem Proposta") Then
                     subgrupo = 0
               Else
                     subgrupo = IIf(GridPropostaAfetada.TextMatrix(i, 14) = "", 0, GridPropostaAfetada.TextMatrix(i, 14))
               End If
              
                Call ObtemCoberturas(IIf(.Sinistro.proposta_id = "", 0, .Sinistro.proposta_id), _
                                    .Sinistro.dt_ocorrencia, _
                                    IIf(.Sinistro.SeguradoComponente = "", 0, .Sinistro.SeguradoComponente), _
                                    coberturas(), _
                                    val_is(), _
                                    subgrupo)
                                    
				
               'Para as coberturas
               qtd_cob_ating = 0
               cob_atingida = False
                                            
               'Verifica se as coberturas foram afetadas
               For cob = 1 To UBound(coberturas)
                   estim = getEstimativa(.Sinistro.proposta_id, coberturas(cob), .Sinistro.Causa_id, .Sinistro.SubCausa_id, .Sinistro.SeguradoComponente, .Sinistro.dt_ocorrencia)
                   If estim.atinge Then
                       cob_atingida = True
                       qtd_cob_ating = qtd_cob_ating + 1
                       ReDim Preserve cobert(1 To qtd_cob_ating) As CoberturaAfetada
                       cobert(qtd_cob_ating).tp_cobertura_id = coberturas(cob)
                       
                       'INICIO - MU00416967 -  melhoria de aviso de sinistro  '' INC 21\03\2018
                       If ((.Sinistro.produto_id = 1201 Or .Sinistro.produto_id = 1226 _
                       Or .Sinistro.produto_id = 1227) And (GridPropostaAfetada.TextMatrix(i, 7) = "61" _
                       Or GridPropostaAfetada.TextMatrix(i, 7) = "77")) Then

                       
                        cobert(qtd_cob_ating).val_estimado = CStr(ObtemIndenizacaoMutuarioVida(.Sinistro.produto_id, .Sinistro.SeguradoCliente_id, .Sinistro.proposta_id, val_is(cob)))
                           
                       
                       Else '|  - MU00416967 - melhoria de aviso de sinistro
                       cobert(qtd_cob_ating).val_estimado = CStr(ObtemEstimativaAbertura(.Sinistro.proposta_id, coberturas(cob), .Sinistro.Causa_id, .Sinistro.SubCausa_id, .Sinistro.SeguradoComponente, .Sinistro.dt_ocorrencia, val_is(cob)))
                       
                       End If 'FIM  - MU00416967 - melhoria de aviso de sinistro
     
                       cobert(qtd_cob_ating).Descricao = estim.Descricao
                       cobert(qtd_cob_ating).cobertura_bb = estim.cobertura_bb
                                           
                       'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
                         cobert(qtd_cob_ating).pagamento_imediato = False
                       'C00216281 - FIM
                                           
                     'Raimundo Belas da Silva Junior  - GPTI - 28/01/2009
                     'Flow 707700 - Comunica��o de sinistro Vida
                     'A linha de comando anterior, comentada logo abaixo, concatenava duas strings e estourava o vetor
                      
                     '.Sinistro.Val_informado = .Sinistro.Val_informado + cobert(qtd_cob_ating).val_estimado
                      
                     '1 - Converte uma string para double, e logo ap�s converte a soma double para string
                       
                       .Sinistro.Val_informado = CDbl(IIf(.Sinistro.Val_informado = "", "0", .Sinistro.Val_informado)) + CDbl(IIf(cobert(qtd_cob_ating).val_estimado = "", "0", cobert(qtd_cob_ating).val_estimado))
             
                                        'Fim corre��o
                   End If
               Next cob
                        
               'Caso a proposta n�o tenha cobertura atingida,
               'inserir a cobertura gen�rica
               If qtd_cob_ating = 0 Then
                    cob_atingida = True
                    qtd_cob_ating = qtd_cob_ating + 1
                    ReDim Preserve cobert(1 To qtd_cob_ating) As CoberturaAfetada
                    'eduardo.amaral (Nova Consultoria) 02/03/2012 Demanda: 12363945 - BB Seguro Vida Empresa FLEX
                    'Altera��o realizada para usar apenas SeguradoComponente e n�o mais IndTpSinistrado
                    'estim = getEstimativaGenerica(CInt(.Sinistro.Causa_id), IIf(.Sinistro.IndTpSinistrado = "T", 1, 3), .Sinistro.dt_ocorrencia)
                    ' Cesar Santos CONFITEC - (SBRJ009952) 07/05/2020 inicio
                    estim = getEstimativaGenerica(CInt(.Sinistro.Causa_id), _
                                                .Sinistro.SeguradoComponente, _
                                                .Sinistro.dt_ocorrencia, _
                                                IIf(.Sinistro.produto_id = 1243 And (.Sinistro.Causa_id = 271 Or .Sinistro.Causa_id = 425) And VerificaPropostaAvisoForcado(.Sinistro.proposta_id) = "S", .Sinistro.proposta_id, ""))
                    'fim eduardo.amaral (Nova Consultoria)
                    If estim.atinge Then
                        cobert(qtd_cob_ating).tp_cobertura_id = estim.tp_cobertura_id
                        cobert(qtd_cob_ating).val_estimado = estim.val_inicial
                    '    cobert(qtd_cob_ating).val_estimado = ObtemEstimativaAbertura(.Sinistro.proposta_id, coberturas(cob), .Sinistro.Causa_id, .Sinistro.SubCausa_id, .Sinistro.SeguradoComponente, .Sinistro.dt_ocorrencia, val_is(cob))
                        cobert(qtd_cob_ating).Descricao = estim.Descricao
                        cobert(qtd_cob_ating).cobertura_bb = estim.cobertura_bb
                        .Sinistro.Val_informado = cobert(qtd_cob_ating).val_estimado
                    Else
                        MsgBox "N�o foi cadastrado estimativa para a cobertura gen�rica neste evento: " & vbCrLf & _
                               .Sinistro.Causa_id & " - " & .Sinistro.Causa, vbCritical, "Estimativa n�o cadastrada."
                        GoTo TrataErro
                    End If
                    ' Cesar Santos CONFITEC - (SBRJ009952) 07/05/2020 Fim
               End If
                
               
        Else 'Caso seja reanalise
            qtd_cob_ating = 0

            'Se n�o pago, reabre com a penultima estimativa
            cob_atingida = True
            qtd_cob_ating = qtd_cob_ating + 1
            ReDim Preserve cobert(1 To qtd_cob_ating) As CoberturaAfetada
            '
            tp_cobertura_id = ObtemTpCoberturaSinistro(.Sinistro.sinistro_id)
            cobert(qtd_cob_ating).val_estimado = CStr(ObtemEstimativaReabertura(.Sinistro.sinistro_id, tp_cobertura_id))
            cobert(qtd_cob_ating).tp_cobertura_id = tp_cobertura_id
            cobert(qtd_cob_ating).Descricao = obtemDescricao(tp_cobertura_id)
            'eduardo.amaral (Nova Consultoria) 02/03/2012 Demanda: 12363945 - BB Seguro Vida Empresa FLEX
            'Altera��o realizada para usar apenas SeguradoComponente e n�o mais IndTpSinistrado
            'cobert(qtd_cob_ating).cobertura_bb = obtemCobertura_BB(.Sinistro.sinistro_id, tp_cobertura_id, IIf(.Sinistro.IndTpSinistrado = "T", 1, 3), .Sinistro.dt_ocorrencia, .Sinistro.Causa_id)
            cobert(qtd_cob_ating).cobertura_bb = obtemCobertura_BB(.Sinistro.sinistro_id, tp_cobertura_id, .Sinistro.SeguradoComponente, .Sinistro.dt_ocorrencia, .Sinistro.Causa_id)
            'fim eduardo.amaral (Nova Consultoria)
            If cobert(qtd_cob_ating).cobertura_bb = 0 Then
                MsgBox "A cobertura AB " & "(" & tp_cobertura_id & ") " & cobert(qtd_cob_ating).Descricao & " n�o possui cobertura BB cadastrada para o evento " & _
                        .Sinistro.Causa & "." & vbCrLf & _
                        "N�o � poss�vel continuar o aviso. ", vbCritical, "Cobertura BB n�o cadastrada"
                        GoTo TrataErro
            End If
        End If
            
            'Atribui��o dos dados das coberturas afetadas
            Avisos_Sinistros(i).cobertura_afetada = cobert
            Avisos_Sinistros(i).cobertura_atingida = cob_atingida
            
            '--------------------------------------------------------------
            'Atribui��o dos dados obtidos para futura consulta
            '--------------------------------------------------------------
            
            'Para as informa��es finais das propostas
            Proposta_Avisada(i).sinistro_id = .Sinistro.sinistro_id
            Proposta_Avisada(i).proposta_id = IIf(.Sinistro.proposta_id = "", "Sem proposta", .Sinistro.proposta_id)
            Proposta_Avisada(i).produto = IIf(.Sinistro.NomeProduto = "", "Sem produto espec�fico", .Sinistro.NomeProduto)
            Proposta_Avisada(i).produto_id = IIf(.Sinistro.produto_id = "", 0, .Sinistro.produto_id)
            Proposta_Avisada(i).ramo_id = .Sinistro.ramo_id
            Proposta_Avisada(i).subramo_id = ObtemSubRamo(IIf(.Sinistro.proposta_id = "", 0, .Sinistro.proposta_id))
        
            'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
            'INSER�AO DOS DADOS DE CLASSIFICA��O
            Proposta_Avisada(i).Situacao = 0 'fluxo comum
            'C00216281 - FIM
        
        End With
        
        
        
    Next i
    
    
         'Grava os Benefici�rios inclusos durante o Aviso (sinistro_benef_temp_tb).
         'Carlos de O. CRUZ - CWI.
         If Not GravaBenefTemp(Avisos_Sinistros) Then GoTo TrataErro
    
    
    Exit Function
    
TrataErro:
    Call TrataErroGeral("ObtemDadosSinistros", "Aviso de Sinistro")
    Call FinalizarAplicacao
    Exit Function
    
End Function

Private Function GravaBenefTemp(objSinistro() As Aviso_Sinistro) As Boolean

    Dim iRow As Integer
    Dim mvarSQL As String
    Dim rs As ADODB.Recordset
    Dim benef As BenefSinistro
    Dim Sinistro_Benef As Sinistro

    '------------------------------------------------------------------------------------------------------------------
    'Cadastro de Benefici�rios.
    'Durante o aviso de sinistro, o usu�rio poder� cadastrar benefici�rios.
    'Carlos de O. Cruz - CWI 02/03/2011.
    '------------------------------------------------------------------------------------------------------------------
    Set benef = New BenefSinistro 'SEGP0794_4_2.Benef

    On Error GoTo Error
    GravaBenefTemp = False

    numCon = AbrirConexao(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription)

    If AbrirTransacao(numCon) Then

'       mvarSQL = "Select p.proposta_id, a.apolice_id, a.sucursal_seguradora_id, a.seguradora_cod_susep, a.ramo_id " & _
'             "  From cliente_tb c with(nolock) " & _
'             "  LEFT join pessoa_fisica_tb pf with(nolock) " & _
'             "    ON c.cliente_id = pf.pf_cliente_id " & _
'             "  LEFT join pessoa_juridica_tb pj with(nolock) " & _
'             "    ON c.cliente_id = pj.pj_cliente_id " & _
'             " INNER join proposta_tb p with(nolock) " & _
'             "    ON c.cliente_id = p.prop_cliente_id " & _
'             " INNER join apolice_tb a with(nolock) " & _
'             "    ON p.proposta_id = a.proposta_id" & _
'             " Where c.cliente_id = " & SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 1) & _
'             "   and p.situacao <> 't'"

'       Set Rs = ExecutarSQL(gsSIGLASISTEMA, _
'                            glAmbiente_id, _
'                            App.Title, _
'                            App.FileDescription, _
'                            mvarSQL, _
'                            True)

'       Set Sinistro_Benef = New Sinistro
'       With Sinistro_Benef
'            .Apolice_id = Rs(1) 'Apolice
'            .Sucursal_id = Rs(2) 'Sucursal
'            .Seguradora_id = Rs(3) 'Seguradora
'            .ramo_id = Rs(4) 'Ramo
'       End With
'       Call Busca_Numero_Sinistro(Sinistro_Benef)

       Dim sin As Sinistro
       For i = 1 To UBound(objSinistro)
            Set sin = objSinistro(i).Sinistro
            For Each benef In SEGP0794_4_2.BeneficiariosSinistro
                 With benef
                     If benef.SinistroAB = sin.proposta_id Then
                     
                         .tp_Documento = IIf(Len(.tp_Documento) = 0, "99", .tp_Documento)
                         mvarSQL = "EXEC SEGS9470_SPI " & _
                                 " @sinistro_id = " & sin.sinistro_id & "," & _
                                 " @apolice_id = " & IIf(Len(sin.Apolice_id) = 0, "NULL", sin.Apolice_id) & "," & _
                                 " @sucursal_seguradora_id = " & IIf(Len(sin.Sucursal_id) = 0, "NULL", sin.Sucursal_id) & "," & _
                                 " @seguradora_cod_susep = " & IIf(Len(sin.Seguradora_id) = 0, "NULL", sin.Seguradora_id) & "," & _
                                 " @ramo_id = " & IIf(Len(sin.ramo_id) = 0, "NULL", sin.ramo_id) & "," & _
                                 " @tp_documento_id = " & IIf(.tp_Documento <> "" And .tp_Documento < 256, .tp_Documento, "99") & "," & _
                                 " @nome = '" & .Nome & "',"
                         mvarSQL = mvarSQL & _
                                 " @dt_nascimento = " & IIf(.dt_Nascimento <> "", "'" & Format(.dt_Nascimento, "yyyymmdd") & "'", "NULL") & ","
                         mvarSQL = mvarSQL & _
                                 " @nome_responsavel = " & IIf(.Nome_Responsavel <> "", "'" & .Nome_Responsavel & "'", "NULL") & "," & _
                                 " @tp_responsavel = '" & .tp_Responsavel & "'," & _
                                 " @orgao = " & IIf(.Orgao <> "", "'" & .Orgao & "'", "NULL") & "," & _
                                 " @numero = " & IIf(.numero <> "", "'" & .numero & "'", "NULL") & "," & _
                                 " @serie = " & IIf(.Serie <> "", "'" & .Serie & "'", "NULL") & "," & _
                                 " @complemento = " & IIf(.Complemento <> "", "'" & .Complemento & "'", "NULL") & "," & _
                                 " @banco = " & IIf(.Banco <> "", .Banco, "NULL") & "," & _
                                 " @agencia = " & IIf(.Agencia <> "", .Agencia, "NULL") & "," & _
                                 " @agencia_dv = " & IIf(.Agencia_dv <> "", "'" & .Agencia_dv & "'", "NULL") & "," & _
                                 " @conta_corrente = " & IIf(.Conta <> "", .Conta, "NULL") & "," & _
                                 " @conta_corrente_dv = " & IIf(.Conta_dv <> "", "'" & .Conta_dv & "'", "NULL") & ","
                         mvarSQL = mvarSQL & _
                                 " @sexo = " & IIf(.Sexo <> "", "'" & Left(.Sexo, 1) & "'", "'n'") & "," & _
                                 " @cgc = " & IIf(.CGC <> "", "'" & LimpaMascara(.CGC) & "'", "NULL") & "," & _
                                 " @cpf = " & IIf(.CPF <> "", "'" & LimpaMascara(.CPF) & "'", "NULL") & ","
                         mvarSQL = mvarSQL & _
                                 " @usuario = '" & cUserName & "'"
                         ''Gerson - CWI
                         mvarSQL = mvarSQL & ", @Sinistro_BB = " & .SinistroBB
                         
                         'RCA 485 - ignora se status_sistema estiver bloqueado
                         mvarSQL = mvarSQL & ", @ignorabloqueio = 1"
                         
                         ''
                        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         mvarSQL, _
                                         numCon, _
                                         False, _
                                         False, _
                                         30000, _
                                         adLockOptimistic, _
                                         adUseClient)
                     End If
                 End With
            Next
       Next

       ConfirmarTransacao (numCon)
       GravaBenefTemp = True

       'Call LimparColecao(SEGP0794_4_2.BeneficiariosSinistro)

       Set benef = Nothing
       Set Sinistro_Benef = Nothing

    End If

Exit Function
Resume
Error:
    Call TrataErroGeral("Grava Beneficiario", "Aviso de Sinistro")
    RetornarTransacao (numCon)
    Exit Function

End Function

Private Function Questionario() As String

  'Dados do Sinistro
    ReDim Avisos_Sinistros(1 To GridPropostaAfetada.Rows - 1) As Aviso_Sinistro
    
    'Dados das Propostas Avisadas
    ReDim Proposta_Avisada(1 To GridPropostaAfetada.Rows - 1) As Proposta_Avisada
    
    'Armazena as propostas
    For i = 1 To GridPropostaAfetada.Rows - 1
       
        Set Avisos_Sinistros(i).Sinistro = New Sinistro
        With Avisos_Sinistros(i)
        
        '---------------- ARICARDO - CWI - 24/11 ----------------------------
        
            If SEGP0794_3.FlagCredito = True Then
            
                .Sinistro.FlagCredito = "S"
            Else
            
                .Sinistro.FlagCredito = "N"
            End If
        
        '----------------------------------------------------------------------
           .Sinistro.Ind_reanalise = IIf(GridPropostaAfetada.TextMatrix(i, 1) = "AVSR", "N", "S")
                      
           '--------------------------------------------------------------
           'Obten��o de dados gerais
           '--------------------------------------------------------------
         
            Obtem_Evento_SEGBR
            .Sinistro.Localizacao = Localizacao
            .Sinistro.Evento_Segbr_id = Evento_Segbr_id
            .Sinistro.Evento_bb_id = Evento_bb_id

        End With
    Next i

End Function

Private Function obtemCobertura_BB(sinistro_id As String, tp_cobertura_id As Integer, tp_componente_id As Integer, dt_ocorrencia_sinistro As String, Evento_sinistro_id As String) As Integer

Dim produto_id As Integer

Dim ramo_id As Integer
Dim subramo_id As String

Dim SQL As String
Dim rs As ADODB.Recordset
    
    SQL = "select a.evento_sinistro_id, b.produto_id, b.ramo_id, b.subramo_id "
    SQL = SQL & " from sinistro_tb a with (nolock),"
    SQL = SQL & " proposta_tb b with (nolock) "
    SQL = SQL & " Where sinistro_id =  " & sinistro_id
    SQL = SQL & " and a.proposta_id = b.proposta_id "
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    If Not rs.EOF Then
        'evento_sinistro_id = rs("evento_sinistro_id")
        produto_id = rs("produto_id")
        ramo_id = rs("ramo_id")
        subramo_id = rs("subramo_id")
    End If
    
    
    rs.Close
    
     SQL = "select top 1 a.cobertura_bb "
     SQL = SQL & " from produto_estimativa_sinistro_tb a with (nolock), "
     SQL = SQL & " tp_cobertura_tb b with (nolock)"
     SQL = SQL & " where a.produto_id = " & produto_id
     SQL = SQL & " and a.tp_cobertura_id = b.tp_cobertura_id"
     SQL = SQL & " and ramo_id = " & ramo_id
     SQL = SQL & " and subramo_id = " & subramo_id
     SQL = SQL & " and tp_componente_id = " & tp_componente_id
     SQL = SQL & " and evento_sinistro_id = " & Evento_sinistro_id
     SQL = SQL & " and a.tp_cobertura_id = " & tp_cobertura_id
     'SQL = SQL & " and convert(char(8),dt_inicio_vigencia, 112) <= '" & dt_ocorrencia_sinistro & "'"
     'SQL = SQL & " and (convert(char(8), dt_fim_vigencia, 112) >= '" & dt_ocorrencia_sinistro & "'"
     'SQL = SQL & "      or dt_fim_vigencia is null)"
     SQL = SQL & " order by a.dt_inclusao desc"
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    If Not rs.EOF Then
        obtemCobertura_BB = rs(0)
    Else
        obtemCobertura_BB = 0
    End If
    rs.Close
    
End Function

Private Function obtemDescricao(tp_cobertura_id As Integer)
Dim SQL As String
Dim rs As ADODB.Recordset

    SQL = "select nome from tp_cobertura_tb with (nolock) "
    SQL = SQL & " Where tp_cobertura_id = " & tp_cobertura_id
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    If Not rs.EOF Then
        obtemDescricao = rs(0)
    Else
        obtemDescricao = ""
    End If
End Function

Private Function ObtemTpCoberturaSinistro(sinistro_id As String) As Integer
Dim SQL As String
Dim rs As ADODB.Recordset

    
    SQL = "select a.tp_cobertura_id"
    SQL = SQL & " From sinistro_cobertura_tb a with (nolock) "
    SQL = SQL & " Where a.sinistro_id = " & sinistro_id
    SQL = SQL & " and a.dt_inicio_vigencia <= getdate() "
    SQL = SQL & " and a.dt_fim_vigencia is null "
    
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    If Not rs.EOF Then
       ObtemTpCoberturaSinistro = rs(0)
    Else
       ObtemTpCoberturaSinistro = 1
    End If
    rs.Close
    

End Function


Private Function VerificaSeSinistroPago(sinistro_id As String) As Boolean
Dim SQL As String
Dim rs As ADODB.Recordset

    SQL = " SELECT * "
    SQL = SQL & " FROM pgto_sinistro_tb with (nolock) "
    SQL = SQL & " WHERE sinistro_id = " & sinistro_id
    SQL = SQL & " AND situacao_op = 'a'"
 
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    If Not rs.EOF Then
        VerificaSeSinistroPagoc = True
    Else
        VerificaSeSinistroPago = False
    End If

End Function

Private Function ObtemSubRamo(proposta_id As Long) As Integer
Dim SQL As String
Dim rs As ADODB.Recordset
    
ObtemSubRamo = 0

If proposta_id <> 0 Then
    SQL = ""
    SQL = SQL & " select subramo_id from proposta_tb with (nolock) "
    SQL = SQL & " where proposta_id = " & proposta_id
    
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    If Not rs.EOF Then
        ObtemSubRamo = rs(0)
    End If
End If
End Function

Private Sub ObtemDadosSinistrosSemPropostas(ByRef sin As Sinistro, Index As Long)
    
    sin.proposta_id = ""
    sin.NomeProduto = ""
    sin.Moeda = ""
    
    sin.Seguradora_id = sCodSusep
    sin.Sucursal_id = "0"
    sin.ramo_id = 0
    Call Busca_Numero_Sinistro(sin)
    
    
    sin.Apolice_id = ""
    sin.produto_id = ""
    sin.PropostaBB = IIf(Trim(GridPropostaAfetada.TextMatrix(Index, 8)) = "", "", GridPropostaAfetada.TextMatrix(Index, 8))
    sin.SitProposta = ""
    
    sin.SeguradoComponente = "1"
                
    sin.SeguradoNomeCliente = GridPropostaAfetada.TextMatrix(Index, 4)
    sin.SeguradoCliente_id = ""
    sin.SeguradoCPF_CNPJ = DatCPF(GridPropostaAfetada.TextMatrix(Index, 5))
    sin.SeguradoSexo = GridPropostaAfetada.TextMatrix(Index, 7)
    sin.SeguradoDtNasc = Format(GridPropostaAfetada.TextMatrix(Index, 6), "yyyymmdd")
    sin.dt_Aviso = Format(Data_Sistema, "yyyymmdd")
    sin.IndTpSinistrado = "T"
    
End Sub

Private Sub ObtemCoberturas(proposta_id As Long, _
                                dtOcorrencia As String, _
                                tp_componente_id As Integer, _
                                ByRef coberturas() As Long, _
                                ByRef ImpSeg() As Double, _
                                Optional sub_grupo_id As Integer)
Dim SQL As String
Dim rs As ADODB.Recordset
Dim Index As Long
    On Error GoTo TrataErro

SQL = ""
SQL = SQL & " exec consultar_coberturas_sps '" & dtOcorrencia & "'," & _
                              proposta_id & "," & tp_componente_id
If sub_grupo_id > 0 Then
   SQL = SQL & " ," & sub_grupo_id
   SQL = SQL & " , 1"
Else
   SQL = SQL & " ,null "
   SQL = SQL & " , 1"
End If

Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                     glAmbiente_id, _
                     App.Title, _
                     App.FileDescription, _
                     SQL, _
                     True)
Index = 1
While Not rs.EOF
    ReDim Preserve ImpSeg(Index) As Double
    ReDim Preserve coberturas(Index) As Long
    coberturas(Index) = rs!tp_cobertura_id
    ImpSeg(Index) = rs!val_is
    Index = Index + 1
    rs.MoveNext
Wend
Exit Sub

TrataErro:
    Call TrataErroGeral("ObtemCoberturas", "Aviso de Sinistro")
    Exit Sub
End Sub

Private Function GravaBenef() As Boolean

    Dim SQL As String
    Dim rs As ADODB.Recordset
    
    Dim benef As BenefSinistro
    
    Set benef = SEGP0794_4_2.benef
    
    
    
    On Error GoTo Error
    GravaBenef = False
    
    numCon = AbrirConexao(gsSIGLASISTEMA, _
                          glAmbiente_id, _
                          App.Title, _
                          App.FileDescription)
    
    If AbrirTransacao(numCon) Then
        For i = 1 To UBound(Avisos_Sinistros)
        
    
                For Each benef In SEGP0794_4_2.BeneficiariosSinistro
                    With benef
                    
                    mvarSQL = "EXEC sinistro_benef_spi " & _
                                    " @SINISTRO_ID = " & Avisos_Sinistros(i).Sinistro.sinistro_id & "," & _
                                    " @APOLICE_ID = " & Avisos_Sinistros(i).Sinistro.Apolice_id & "," & _
                                    " @SUCURSAL_SEGURADORA_ID = " & Avisos_Sinistros(i).Sinistro.Sucursal_id & "," & _
                                    " @SEGURADORA_COD_SUSEP = " & Avisos_Sinistros(i).Sinistro.Seguradora_id & "," & _
                                    " @RAMO_ID = " & Avisos_Sinistros(i).Sinistro.ramo_id & "," & _
                                    " @TP_DOCUMENTO_ID = " & IIf(.tp_Documento <> "", .tp_Documento, "99") & "," & _
                                    " @NOME = '" & .Nome & "',"
                    mvarSQL = mvarSQL & _
                                    " @DT_NASCIMENTO = " & IIf(.dt_Nascimento <> "", "'" & Format(.dt_Nascimento, "yyyymmdd") & "'", "NULL") & ","
                    mvarSQL = mvarSQL & _
                                    " @NOME_RESPONSAVEL = " & IIf(.Nome_Responsavel <> "", "'" & .Nome_Responsavel & "'", "NULL") & "," & _
                                    " @TP_RESPONSAVEL = '" & .tp_Responsavel & "'," & _
                                    " @ORGAO = " & IIf(.Orgao <> "", "'" & .Orgao & "'", "NULL") & "," & _
                                    " @NUMERO = " & IIf(.numero <> "", "'" & .numero & "'", "NULL") & "," & _
                                    " @SERIE = " & IIf(.Serie <> "", "'" & .Serie & "'", "NULL") & "," & _
                                    " @COMPLEMENTO = " & IIf(.Complemento <> "", "'" & .Complemento & "'", "NULL") & "," & _
                                    " @BANCO = " & IIf(.Banco <> "", .Banco, "NULL") & "," & _
                                    " @AGENCIA = " & IIf(.Agencia <> "", .Agencia, "NULL") & "," & _
                                    " @AGENCIA_DV = " & IIf(.Agencia_dv <> "", "'" & .Agencia_dv & "'", "NULL") & "," & _
                                    " @CONTA_CORRENTE = " & IIf(.Conta <> "", .Conta, "NULL") & "," & _
                                    " @CONTA_CORRENTE_DV = " & IIf(.Conta_dv <> "", "'" & .Conta_dv & "'", "NULL") & ","
                    mvarSQL = mvarSQL & _
                                    " @SEXO = " & IIf(.Sexo <> "", "'" & Left(.Sexo, 1) & "'", "'n'") & "," & _
                                    " @CGC = " & IIf(.CGC <> "", "'" & LimpaMascara(.CGC) & "'", "NULL") & "," & _
                                    " @CPF = " & IIf(.CPF <> "", "'" & LimpaMascara(.CPF) & "'", "NULL") & ","
                    mvarSQL = mvarSQL & _
                                    " @USUARIO = '" & cUserName & "'"
                                    
                    'RCA 485 - ignora se status_sistema estiver bloqueado
                    mvarSQL = mvarSQL & ", @ignorabloqueio = 1"
                    
                    End With
                Next

                Set rs = Conexao_ExecutarSQL(sSiglaSistema, _
                                  iAmbienteId, _
                                  App.Title, _
                                  App.FileDescription, _
                                  mvarSQL, numCon, True)


                'Set Rs = Conexao_ExecutarSQL(sSiglaSistema, _
                                  iAmbienteId, _
                                  sSiglaRecurso, _
                                  sDescricaoRecurso, _
                                  mvarSQL, numCon, True)
                                  
                 'Set RsAux = ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQLAux, _
                             True)
         Next i
            
        ConfirmarTransacao (numCon)
        IncluirBeneficiarios = True
    
    End If
    
Error:
    Call TrataErroGeral("Grava Beneficiario", "Aviso de Sinistro")
    RetornarTransacao (numCon)
    Exit Function

End Function

Private Function GravaSinistros() As Boolean
Dim SQL As String
Dim Data_nascimento As Date
Dim rs As ADODB.Recordset
    On Error GoTo Error
    GravaSinistros = False

    numCon = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)

    If AbrirTransacao(numCon) Then
        For i = 1 To UBound(Avisos_Sinistros)

            '-Caso seja reanalise e n�o seja aviso origin�rio da CA,
            ' n�o gera comunica��o com o BB - evento 1190 - 22/03/2004
        ' '   If Avisos_Sinistros(i).sinistro.Ind_reanalise = "N" Or _
        '        (Avisos_Sinistros(i).sinistro.Ind_reanalise = "S" And ehSinistroCA(Avisos_Sinistros(i).sinistro.sinistro_id)) Then
                'Henrique Tadashi Saito GPTI - Demanda 712038 - Verifica se o sinistro_id est� com valor null se estiver chama a procedure
                'chave_sinistro_id para atribuir o valor do sinistro_id
                If Avisos_Sinistros(i).Sinistro.sinistro_id = "" Then
           '******************************************************************************************'
           'Anderson Fiuza GPTI FLOW:2707850 29/01/2010
           'Substitui�ao da procedure chave_sinistro_spu pela smqs0006_spu chamada pela procedure abaixo,
           'pois na hora de retornar o numero do sinistro_id est� apresentando erro de overflow,
           'ocasionando o "pulo" de sequencial de sinistro.
           '******************************************************************************************'
                    
                    
                    SQL = "SET nocount on "
                    SQL = SQL & "EXEC seguros_db..SEGS8151_SPS '" & Format(Trim(Str(Avisos_Sinistros(i).Sinistro.ramo_id)), "00") & "'," & Avisos_Sinistros(i).Sinistro.Sucursal_id & "," & Avisos_Sinistros(i).Sinistro.Seguradora_id & ",'" & cUserName & "'"
                    
                    Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
                    If Not rs.EOF Then
                        Avisos_Sinistros(i).Sinistro.sinistro_id = rs(0)
                    End If
                    rs.Close
                End If
                'Henrique Tadashi Saito Fim
                
                'Raimundo Belas da Silva Junior - GPTI - 25/05/2009
                                'Flow 761940
                'Verificando a vers�o do endosso para cada uma das propostas a serem avisadas
                
                Dim sqlx As String
                Dim rsx As ADODB.Recordset
                
                sqlx = "set nocount on exec obtem_Versao_Endosso_sps " & Avisos_Sinistros(i).Sinistro.PropostaBB & ", '" & Avisos_Sinistros(i).Sinistro.dt_ocorrencia & "'"
                
                Set rsx = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sqlx, True)
                
                If Not rsx.EOF Then
                    lNR_VRS_EDS = rsx("NR_VRS_EDS")
                    lNR_CTR_SGRO = rsx("NR_CTR_SGRO_BB")
                Else
                    lNR_VRS_EDS = 0
                    lNR_CTR_SGRO = 0
                End If
                rsx.Close
                
                'Raimundo - GPTI - Fim

                
                If Avisos_Sinistros(i).Sinistro.sinistro_id <> 0 Then sinistro_id = Avisos_Sinistros(i).Sinistro.sinistro_id
                If Avisos_Sinistros(i).Sinistro.produto_id <> 0 Then produto_id = Avisos_Sinistros(i).Sinistro.produto_id
             
                SQL = " set nocount on exec evento_SEGBR_sinistro_CA_spi " & vbCrLf
                SQL = SQL & Avisos_Sinistros(i).Sinistro.sinistro_id & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.Apolice_id = "", "null", Avisos_Sinistros(i).Sinistro.Apolice_id) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.Seguradora_id = "0", "null", Avisos_Sinistros(i).Sinistro.Seguradora_id) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.Sucursal_id = "", "null", Avisos_Sinistros(i).Sinistro.Sucursal_id) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.ramo_id = "0", "0", Avisos_Sinistros(i).Sinistro.ramo_id) & "," & vbCrLf
                SQL = SQL & Avisos_Sinistros(i).Sinistro.Evento_Segbr_id & "," & vbCrLf
                SQL = SQL & Avisos_Sinistros(i).Sinistro.Evento_bb_id & "," & vbCrLf
                SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.dt_ocorrencia & "'," & vbCrLf
                SQL = SQL & entidade_id & "," & vbCrLf
                SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.Localizacao & "'," & vbCrLf
                SQL = SQL & "'" & cUserName & "'," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.produto_id = "", "null", Avisos_Sinistros(i).Sinistro.produto_id) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.proposta_id = "", "null", Avisos_Sinistros(i).Sinistro.proposta_id) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.PropostaBB = "", "null", Avisos_Sinistros(i).Sinistro.PropostaBB) & "," & vbCrLf
                SQL = SQL & Avisos_Sinistros(i).Sinistro.AgenciaAviso & "," & vbCrLf
                
                '- IM00343271 - 24/05/2018
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.SeguradoCliente_id = "", "null", Avisos_Sinistros(i).Sinistro.SeguradoCliente_id) & "," & vbCrLf
                          
                 ' INICIO - IM00298488 - 12/04/2018   - IM00343271 - 24/05/2018
                If ((Avisos_Sinistros(i).Sinistro.produto_id = "1201" And Avisos_Sinistros(i).Sinistro.ramo_id = "61") Or (Avisos_Sinistros(i).Sinistro.produto_id = "1226" And Avisos_Sinistros(i).Sinistro.ramo_id = "77") _
                Or (Avisos_Sinistros(i).Sinistro.produto_id = "1227" And Avisos_Sinistros(i).Sinistro.ramo_id = "77")) Then
                
                    'SQL = SQL & SEGP0794_3.cmbNomeSinistrado.ItemData(SEGP0794_3.cmbNomeSinistrado.ListIndex) & "," & vbCrLf
                    '- IM00343271 - 24/05/2018
                    SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.SinistradoNomeCliente = "", "null", "'" & MudaAspaSimples(Avisos_Sinistros(i).Sinistro.SinistradoNomeCliente) & "'") & "," & vbCrLf
                    'SQL = SQL & "'" & MudaAspaSimples(Avisos_Sinistros(i).Sinistro.SinistradoNomeCliente) & "'" & "," & vbCrLf
                    SQL = SQL & "'" & Trim(Avisos_Sinistros(i).Sinistro.SinistradoCPF) & "'" & "," & vbCrLf
                    SQL = SQL & "null" & ", " & vbCrLf  '"'" & Avisos_Sinistros(i).Sinistro.SinistradoSexo & "'" & "," & vbCrLf
                    SQL = SQL & "null" & ", " & vbCrLf  '"'" & Avisos_Sinistros(i).Sinistro.SinistradoDtNasc & "'" & "," & vbCrLf
                Else  '  IM00298488 - 12/04/2018   - IM00343271 - 24/05/2018
                
                    '------------------------------------------------------------------
                    'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : INICIO
                    'SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.SeguradoNomeCliente = "", "null", "'" & Avisos_Sinistros(i).Sinistro.SeguradoNomeCliente & "'") & "," & vbCrLf
                    SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.SeguradoNomeCliente = "", "null", "'" & MudaAspaSimples(Avisos_Sinistros(i).Sinistro.SeguradoNomeCliente) & "'") & "," & vbCrLf
                    'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : FIM
                    '------------------------------------------------------------------
                    SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.SeguradoCPF_CNPJ = "", "null", "'" & Trim(Avisos_Sinistros(i).Sinistro.SeguradoCPF_CNPJ) & "'") & "," & vbCrLf
                    SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.SeguradoSexo = "", "null", "'" & Avisos_Sinistros(i).Sinistro.SeguradoSexo & "'") & "," & vbCrLf
                    SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.SeguradoDtNasc = "", "null", "'" & Avisos_Sinistros(i).Sinistro.SeguradoDtNasc & "'") & "," & vbCrLf
                
               End If  ' FIM - IM00298488 - 12/04/2018
                
                
                If SEGP0794_4.FlagMicrosseguro And Avisos_Sinistros(i).Sinistro.CD_PRD = 952 And Avisos_Sinistros(i).Sinistro.CD_MDLD = 49 And Avisos_Sinistros(i).Sinistro.CD_ITEM_MDLD = 105 Then
                    '------------------------------------------------------------------
                    'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : INICIO
                    'SQL = SQL & "'" & SEGP0794_4_aviso_micro_seguro.txtNomeSinistrado.Text & "', " & vbCrLf
                    SQL = SQL & "'" & MudaAspaSimples(SEGP0794_4_aviso_micro_seguro.txtNomeSinistrado.Text) & "', " & vbCrLf
                    'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : FIM
                    '------------------------------------------------------------------
                    SQL = SQL & "'" & SEGP0794_4_aviso_micro_seguro.mskCPF.ClipText & "', " & vbCrLf
                    SQL = SQL & "'" & Mid(SEGP0794_4_aviso_micro_seguro.cboSexo, 1, 1) & "', " & vbCrLf
                    Data_nascimento = SEGP0794_4_aviso_micro_seguro.mskDtNascimento.Text
                    SQL = SQL & "'" & Format$(Data_nascimento, "yyyy-mm-dd") & "', " & vbCrLf
                Else
                    '------------------------------------------------------------------
                    'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : INICIO
                    'SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.SinistradoNomeCliente = "", "null", "'" & Avisos_Sinistros(i).Sinistro.SinistradoNomeCliente & "'") & "," & vbCrLf
                    SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.SinistradoNomeCliente = "", "null", "'" & MudaAspaSimples(Avisos_Sinistros(i).Sinistro.SinistradoNomeCliente) & "'") & "," & vbCrLf
                    'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : FIM
                    '------------------------------------------------------------------
                        SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.SinistradoCPF = "", "null", "'" & Avisos_Sinistros(i).Sinistro.SinistradoCPF & "'") & "," & vbCrLf
                        SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.SinistradoSexo = "", "null", "'" & Avisos_Sinistros(i).Sinistro.SinistradoSexo & "'") & "," & vbCrLf
                        SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.SinistradoDtNasc = "", "null", "'" & Avisos_Sinistros(i).Sinistro.SinistradoDtNasc & "'") & "," & vbCrLf
                End If
                
                
                
                '------------------------------------------------------------------
                'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : INICIO
                'SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.SolicitanteNome & "'," & vbCrLf
                SQL = SQL & "'" & MudaAspaSimples(Avisos_Sinistros(i).Sinistro.SolicitanteNome) & "'," & vbCrLf
                'Henrique Gusman (Confitec) : 05/07/2013 : INC000004124214 : FIM
                '------------------------------------------------------------------
                'Cleber Sardo (Confitec) : 17/09/2013 : INC000004182137 : Inicio
                'SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.SolicitanteEndereco & "'," & vbCrLf
                SQL = SQL & "'" & MudaAspaSimples(Avisos_Sinistros(i).Sinistro.SolicitanteEndereco) & "'," & vbCrLf
                'Cleber Sardo (Confitec) : 17/09/2013 : INC000004182137 : Fim
                '-------------------------------------------------------------------
                SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.SolicitanteBairro & "'," & vbCrLf
                SQL = SQL & "'" & Replace(Avisos_Sinistros(i).Sinistro.SolicitanteCEP, "-", "") & "'," & vbCrLf
                SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.SolicitanteMunicipio & "'," & vbCrLf
                SQL = SQL & Avisos_Sinistros(i).Sinistro.SolicitanteMunicipio_id & "," & vbCrLf
                SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.SolicitanteEstado & "'," & vbCrLf
                
                ' INICIO - IM00298488 - 12/04/2018
                If ((Avisos_Sinistros(i).Sinistro.produto_id = "1201" And Avisos_Sinistros(i).Sinistro.ramo_id = "61") Or (Avisos_Sinistros(i).Sinistro.produto_id = "1226" And Avisos_Sinistros(i).Sinistro.ramo_id <> "77") _
                Or (Avisos_Sinistros(i).Sinistro.produto_id = "1227" And Avisos_Sinistros(i).Sinistro.ramo_id = "77")) Then
                
                    SQL = SQL & "null" & ", " & vbCrLf  'Avisos_Sinistros(i).Sinistro.SolicitanteGrauParentesco & "," & vbCrLf
                    
                Else  '  IM00298488 - 12/04/2018
                    SQL = SQL & Avisos_Sinistros(i).Sinistro.SolicitanteGrauParentesco & "," & vbCrLf
                End If
                
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.ddd1 = "", "null", Avisos_Sinistros(i).Sinistro.ddd1) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.ddd2 = "", "null", Avisos_Sinistros(i).Sinistro.ddd2) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.ddd3 = "", "null", Avisos_Sinistros(i).Sinistro.ddd3) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.ddd4 = "", "null", Avisos_Sinistros(i).Sinistro.ddd4) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.ddd5 = "", "null", Avisos_Sinistros(i).Sinistro.ddd5) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.telefone1 = "", "null", Avisos_Sinistros(i).Sinistro.telefone1) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.telefone2 = "", "null", Avisos_Sinistros(i).Sinistro.telefone2) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.telefone3 = "", "null", Avisos_Sinistros(i).Sinistro.telefone3) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.telefone4 = "", "null", Avisos_Sinistros(i).Sinistro.telefone4) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.telefone5 = "", "null", Avisos_Sinistros(i).Sinistro.telefone5) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.ramal1 = "", "null", Avisos_Sinistros(i).Sinistro.ramal1) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.ramal2 = "", "null", Avisos_Sinistros(i).Sinistro.ramal2) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.ramal3 = "", "null", Avisos_Sinistros(i).Sinistro.ramal3) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.ramal4 = "", "null", Avisos_Sinistros(i).Sinistro.ramal4) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.ramal5 = "", "null", Avisos_Sinistros(i).Sinistro.ramal5) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.tp_telefone1 = "", "null", Avisos_Sinistros(i).Sinistro.tp_telefone1) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.tp_telefone2 = "", "null", Avisos_Sinistros(i).Sinistro.tp_telefone2) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.tp_telefone3 = "", "null", Avisos_Sinistros(i).Sinistro.tp_telefone3) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.tp_telefone4 = "", "null", Avisos_Sinistros(i).Sinistro.tp_telefone4) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.tp_telefone5 = "", "null", Avisos_Sinistros(i).Sinistro.tp_telefone5) & "," & vbCrLf
                SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.SolicitanteEmail & "'," & vbCrLf
                SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.dt_Aviso & "'," & vbCrLf
                SQL = SQL & Avisos_Sinistros(i).Sinistro.Causa_id & "," & vbCrLf
                SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.Causa & "'," & vbCrLf
                SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.Ind_reanalise & "'," & vbCrLf
                SQL = SQL & "'N'" & "," & vbCrLf                                 'enviado_eMail_tecnico
                SQL = SQL & Avisos_Sinistros(i).Sinistro.Banco & "," & vbCrLf
                SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.NumRemessa & "'," & vbCrLf
                SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.NumProcRemessa & "'," & vbCrLf
                SQL = SQL & "'" & processa_reintegracao_is & "'," & vbCrLf
                SQL = SQL & "'" & canal_comunicacao & "'," & vbCrLf
                SQL = SQL & IIf(SEGP0794_4.FlagMicrosseguro And Avisos_Sinistros(i).Sinistro.CD_PRD = 952 And Avisos_Sinistros(i).Sinistro.CD_MDLD = 49 And Avisos_Sinistros(i).Sinistro.CD_ITEM_MDLD = 105, "'" & Mid(SEGP0794_4_aviso_micro_seguro.cboSinistrado, 1, 1) & "", "'" & Avisos_Sinistros(i).Sinistro.IndTpSinistrado) & "'," & vbCrLf
                SQL = SQL & "'" & situacao_aviso & "'," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.Moeda = "", "null", Avisos_Sinistros(i).Sinistro.Moeda) & ", " & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.Endosso_id = "", "null", Avisos_Sinistros(i).Sinistro.Endosso_id) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.Sinistro_id_lider = "", "null", Avisos_Sinistros(i).Sinistro.Sinistro_id_lider) & "," & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.CodRamo = "", "null", Avisos_Sinistros(i).Sinistro.CodRamo) & ", " & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.Sinistro_bb = "", "null", Avisos_Sinistros(i).Sinistro.Sinistro_bb) & ", " & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.HoraOcorrencia = "", "null", "'" & Avisos_Sinistros(i).Sinistro.HoraOcorrencia & "'") & ", " & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.LocalOcorrencia = "", "null", "'" & Avisos_Sinistros(i).Sinistro.LocalOcorrencia & "'") & ", " & vbCrLf
                SQL = SQL & "null" & ", " & vbCrLf
                SQL = SQL & "null" & ", " & vbCrLf
                SQL = SQL & "null" & ", " & vbCrLf
                SQL = SQL & "null" & ", " & vbCrLf
                SQL = SQL & "null" & ", " & vbCrLf
                SQL = SQL & "null" & ", " & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.CD_PRD = 0, "null", Avisos_Sinistros(i).Sinistro.CD_PRD) & ", " & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.CD_MDLD = 0, "null", Avisos_Sinistros(i).Sinistro.CD_MDLD) & ", " & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.CD_ITEM_MDLD = 0, "null", Avisos_Sinistros(i).Sinistro.CD_ITEM_MDLD) & ", " & vbCrLf
                SQL = SQL & IIf(lNR_CTR_SGRO = 0, "null", lNR_CTR_SGRO) & ", " & vbCrLf
                SQL = SQL & IIf(lNR_VRS_EDS = 0, "null", lNR_VRS_EDS) & ", " & vbCrLf
                                
                'Raimundo Belas da Silva Junior GPTI - demanda 707700
                'Foi corrigido o campo val_informado que estava vazio e agora retorna a string "null"
                                
                '-------------------------------------------------------------------------------------------------------------------------------------------------------
                'EMORENO - 18/02/2011 (Corre��o: Tratamento para valor decimal)
                ''sql = sql & IIf(Avisos_Sinistros(i).Sinistro.Val_informado = "", "Null", Avisos_Sinistros(i).Sinistro.Val_informado) & ", " & vbCrLf
                SQL = SQL & IIf(Avisos_Sinistros(i).Sinistro.Val_informado = "", "Null", Replace(Avisos_Sinistros(i).Sinistro.Val_informado, ",", ".")) & ", " & vbCrLf
                '-------------------------------------------------------------------------------------------------------------------------------------------------------
                
                SQL = SQL & Avisos_Sinistros(i).Sinistro.SubCausa_id
                SQL = SQL & " , " & "null" & vbCrLf
                SQL = SQL & " , " & "null" & vbCrLf
                SQL = SQL & " , " & "null" & vbCrLf
                SQL = SQL & " , " & "null" & vbCrLf
                SQL = SQL & " , " & "null" & vbCrLf
                
                
                ' INICIO - IM00298488 - 12/04/2018
                If ((Avisos_Sinistros(i).Sinistro.produto_id = "1201" And Avisos_Sinistros(i).Sinistro.ramo_id = "61") Or (Avisos_Sinistros(i).Sinistro.produto_id = "1226" And Avisos_Sinistros(i).Sinistro.ramo_id = "77") _
                Or (Avisos_Sinistros(i).Sinistro.produto_id = "1227" And Avisos_Sinistros(i).Sinistro.ramo_id = "77")) Then
                    SQL = SQL & " , " & "null" & " " & vbCrLf  'Avisos_Sinistros(i).Sinistro.SolicitanteGrauParentesco & "," & vbCrLf
                Else  '  IM00298488 - 12/04/2018
               
                '-------------Alterado por Andre da CWI - data: 24/11/2010-------------------------------
                SQL = SQL & " , " & IIf(Avisos_Sinistros(i).Sinistro.FlagCredito = "", "null", Avisos_Sinistros(i).Sinistro.FlagCredito) & " " & vbCrLf
                '----------------------------------------------------------------------------------------
                'Raimundo Belas da Silva Junior Fim
                End If
                SQL = SQL & " , " & "null" & vbCrLf
                SQL = SQL & ", '" & IIf(Avisos_Sinistros(i).Sinistro.SolicitanteCPF = "", "null", Avisos_Sinistros(i).Sinistro.SolicitanteCPF) & "' " & vbCrLf '07/11/2019 - (ntendencia) - Aviso de sinistro com CPF solicitante

                ' Cesar Santos CONFITEC - (SBRJ009952) 27/04/2020 - Inicio
                If (Avisos_Sinistros(i).Sinistro.produto_id = 1243) And (Avisos_Sinistros(i).Sinistro.Causa_id = 271 Or Avisos_Sinistros(i).Sinistro.Causa_id = 425) Then
                    'Aviso for�ado para SIM
                    SQL = SQL & ", " & VerificaPropostaAvisoForcado(Avisos_Sinistros(i).Sinistro.proposta_id) & " " & vbCrLf
                Else
                    SQL = SQL & " , " & "null" & vbCrLf
                End If
                ' Cesar Santos CONFITEC - (SBRJ009952) 27/04/2020 - Fim

                'Grava na evento_segbr_sinistro_tb
                Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, numCon, True)

                If Not rs.EOF Then
                    '**********************************************
                    'Mensagem para verifica��o do erro desconhecido
                    '**********************************************
                    If Not IsNull(rs(0)) Then
                        Avisos_Sinistros(i).Sinistro.evento_id = rs(0)
                    Else
                        MsgBox "Erro no cadastro do Aviso." & vbCrLf & _
                                "Fun��o: GravaSinistros()" & vbCrLf & _
                                "Campo Evento ID NULO" & vbCrLf & _
                                "Comando SQL: " & SQL, vbCritical, "Aviso..."
                        GoTo Error
                    End If

                    'Grava na evento_segbr_sinistro_detalhe_tb
                    Call GravarTabelao(Avisos_Sinistros(i).Sinistro, Avisos_Sinistros(i).cobertura_afetada, Avisos_Sinistros(i).cobertura_atingida)
                    
                    'Grava na evento_segbr_sinistro_descricao_tb
                    Call GravarDescricaoOcorrencia(Avisos_Sinistros(i).Sinistro)
                      
                    'INICIO - MU00416967 -  melhoria de aviso de sinistro - IM00316531 - 26/04/2018  -- IM00330639  -- 04052018
                    If ((Avisos_Sinistros(i).Sinistro.produto_id = "1201" And Avisos_Sinistros(i).Sinistro.ramo_id = "61") _
                       Or (Avisos_Sinistros(i).Sinistro.produto_id = "1226" And Avisos_Sinistros(i).Sinistro.ramo_id = "77") _
                       Or (Avisos_Sinistros(i).Sinistro.produto_id = "1227" And Avisos_Sinistros(i).Sinistro.ramo_id = "77")) Then
          
                        Obj_sinis_valor_estimado = TrocaVirgulaPorPonto(Avisos_Sinistros(i).Sinistro.Val_informado)
                       
                        'Grava na evento_segbr_sinistro_objeto_tb
                        Call InserirObjetoSinistrado(Avisos_Sinistros(i).Sinistro.evento_id, Avisos_Sinistros(i).cobertura_afetada)
                                       
                    End If  'Fim -IM00316531 - 26 / 4 / 2018
                    
                    'Grava na evento_segbr_sinistro_cobertura_tb
                    If Avisos_Sinistros(i).cobertura_atingida Then Call GravarCoberturas(Avisos_Sinistros(i).Sinistro.evento_id, Avisos_Sinistros(i).cobertura_afetada)
                  
                    
                    'Camila Kume - 07.01.2009 - Inclus�o dos documentos
                    Dim Indice As Integer
                    Indice = i
                    Call IncluirDocumentos(Indice)
                    'Camila Kume - 07.01.2009 - Fim
                    
                    
                    'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
                    'CLASSIFICA��O DO SINISTRO
                          
                    'Valida produto/ramo/cobertura/evento
                    If Valida_Fluxo_Pagamento_Imediato(i) Then
                        'CLASSIFICA E GRAVA O DETALHAMENTO
                         If Classifica_Pagamento_Imediato(i) Then
                            Proposta_Avisada(i).Situacao = 1
                         End If
                         Gravar_classificacao_Pagamento_Imediato (i)
                    End If

                    'C00216281 - FIM
					
                ' Cesar Santos CONFITEC - (SBRJ009952) 28/05/2020 - Inicio
                   If (Avisos_Sinistros(i).Sinistro.produto_id = 1243 And Avisos_Sinistros(i).Sinistro.Causa_id = 425) Then
                
                  Call GravarInformacaoComplementarDi(CDbl(Avisos_Sinistros(i).Sinistro.Evento_id), _
                                                      Format(SEGP0794_7_3.mskDataAdmissao.Text, "yyyymmdd"), _
                                                      Format(SEGP0794_7_3.mskDataAvisoPrevio.Text, "yyyymmdd"), _
                                                      SEGP0794_7_3.mskPISEmpregado.ClipText, _
                                                      SEGP0794_7_3.mskCNPJEmpregador.ClipText, _
                                                      CDbl(SEGP0794_7_3.mskValorRecisao.Text), _
                                                      CInt(SEGP0794_7_3.cboMotivoDemissao.ItemData(SEGP0794_7_3.cboMotivoDemissao.ListIndex)))
                        
                   End If
               ' Cesar Santos CONFITEC - (SBRJ009952) 28/05/2020 - Fim
					
                    Call GravaGTREmissaoLayout(Avisos_Sinistros(i).Sinistro)
                End If
        Next i

        '08/07/2020 (ntendencia) - 00216281-esteira-pagamento-imediato-vida-individual-fase2 (inicio)
        Dim sInsert As String
        Dim sValues As String
        Dim sSQL As String
        Dim sAdd As String
        sSQL = "SET NOCOUNT ON " & vbNewLine
        sSQL = sSQL & vbNewLine
        sSQL = sSQL & "IF OBJECT_ID('tempdb..#classificacao_tmp') IS NOT NULL " & vbNewLine
        sSQL = sSQL & "BEGIN DROP TABLE #classificacao_tmp END" & vbNewLine
        sSQL = sSQL & vbNewLine
        sSQL = sSQL & "CREATE TABLE #classificacao_tmp ( ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL" & vbNewLine
        'valida situa��o
        sSQL = sSQL & " ,indice INT" & vbNewLine
        sSQL = sSQL & " ,situacao INT" & vbNewLine
        'valida pagamento imediato e classifica��o
        sSQL = sSQL & " ,proposta_id INT" & vbNewLine
        sSQL = sSQL & " ,evento_sinistro_id INT" & vbNewLine
        sSQL = sSQL & " ,tp_cobertura_id INT" & vbNewLine
        'classifica
        sSQL = sSQL & " ,produto_id INT" & vbNewLine
        sSQL = sSQL & " ,ramo_id INT" & vbNewLine
        sSQL = sSQL & " ,dt_ocorrencia smalldatetime" & vbNewLine
        sSQL = sSQL & " ,evento_id INT" & vbNewLine
        sSQL = sSQL & " ,sinistro_id NUMERIC(11)" & vbNewLine
        'controle
        sSQL = sSQL & " ,fluxo_validado INT" & vbNewLine
        sSQL = sSQL & " ,pagamento_imediato INT" & vbNewLine
        sSQL = sSQL & " ,deferido INT" & vbNewLine
        sSQL = sSQL & " ,fase INT" & vbNewLine
        sSQL = sSQL & " ,vigencia_dias INT" & vbNewLine
        sSQL = sSQL & " ,regra INT" & vbNewLine
        sSQL = sSQL & " ,tp_componente_id INT" & vbNewLine
        sSQL = sSQL & " ,sub_grupo_id INT" & vbNewLine
        sSQL = sSQL & " ,val_is NUMERIC(15,2)" & vbNewLine
        'sSQL = sSQL & " ,proposta_bb INT" & vbNewLine
        sSQL = sSQL & " ,dt_inicio_vigencia SMALLDATETIME" & vbNewLine
        sSQL = sSQL & " ,dt_fim_vigencia SMALLDATETIME" & vbNewLine
        sSQL = sSQL & " ,exibir_popup INT" & vbNewLine
        sSQL = sSQL & " ,tp_sinistro_parametro_id INT" & vbNewLine
        'sSQL = sSQL & " ,sinistrado_cliente_id INT" & vbNewLine
        sSQL = sSQL & " ,log_classificacao VARCHAR(MAX)" & vbNewLine
        sSQL = sSQL & " )" & vbNewLine
        sSQL = sSQL & vbNewLine
        'sInsert = "INSERT INTO #classificacao_tmp (indice, situacao, proposta_id, evento_sinistro_id, tp_cobertura_id, produto_id, ramo_id, dt_ocorrencia, evento_id, sinistro_id, fluxo_validado, pagamento_imediato, Deferido, fase, vigencia_dias, regra, tp_componente_id, sub_grupo_id, val_is, proposta_bb ,dt_inicio_vigencia ,dt_fim_vigencia ,exibir_popup ,tp_sinistro_parametro_id, sinistrado_cliente_id )" & vbNewLine
        sInsert = "INSERT INTO #classificacao_tmp (indice, situacao, proposta_id, evento_sinistro_id, tp_cobertura_id, produto_id, ramo_id, dt_ocorrencia, evento_id, sinistro_id, fluxo_validado, pagamento_imediato, Deferido, fase, vigencia_dias, regra, tp_componente_id, sub_grupo_id, val_is, dt_inicio_vigencia ,dt_fim_vigencia ,exibir_popup, tp_sinistro_parametro_id, log_classificacao )" & vbNewLine
        
        Dim processa_pagto_imediato As Boolean
        processa_pagto_imediato = False
        
        sAdd = ""
        
        For i = 1 To UBound(Avisos_Sinistros)
            For a = 1 To UBound(Avisos_Sinistros(i).cobertura_afetada)
              sValues = ""
              If i = 1 And a = 1 Then
              sValues = sValues & "Values ( " & i 'indice
              Else
                sValues = sValues & ", ( " & i 'indice
              End If
              sValues = sValues & ",0" 'Situacao
              sValues = sValues & "," & Avisos_Sinistros(i).Sinistro.proposta_id
              sValues = sValues & "," & Avisos_Sinistros(i).Sinistro.Causa_id
              sValues = sValues & "," & Avisos_Sinistros(i).cobertura_afetada(a).tp_cobertura_id
              sValues = sValues & "," & Avisos_Sinistros(i).Sinistro.produto_id
              sValues = sValues & "," & Avisos_Sinistros(i).Sinistro.ramo_id
              sValues = sValues & ",'" & Avisos_Sinistros(i).Sinistro.dt_ocorrencia & "'"
              sValues = sValues & "," & Avisos_Sinistros(i).Sinistro.evento_id
              sValues = sValues & "," & Avisos_Sinistros(i).Sinistro.sinistro_id
              sValues = sValues & ", 0 " 'fluxo_validado
              sValues = sValues & ", 0 " 'pagamento_imediato
              sValues = sValues & ", 0 " 'Deferido
              sValues = sValues & ", 0 " 'fase
              sValues = sValues & ", 0 " 'vigencia_dias
              sValues = sValues & ", 0 " 'regra
              sValues = sValues & "," & IIf(Avisos_Sinistros(i).Sinistro.SeguradoComponente = "", 0, Avisos_Sinistros(i).Sinistro.SeguradoComponente) 'tp_componente_id
              sValues = sValues & ",0" & sub_grupo_id
              sValues = sValues & ", 0" 'val_is
              'sValues = sValues & "," & Avisos_Sinistros(i).Sinistro.Proposta_BB
              sValues = sValues & ",NULL" 'dt_inicio_vigencia
              sValues = sValues & ",NULL" 'dt_fim_vigencia
              sValues = sValues & ",NULL" 'exibir_popup
              sValues = sValues & ",0" 'tp_sinistro_parametro_id
              'sValues = sValues & "," & IIf(Avisos_Sinistros(i).SinistradoClienteId = 0, Avisos_Sinistros(i).SeguradoCliente_id, Avisos_Sinistros(i).SinistradoClienteId)
              sValues = sValues & ",''" 'log_classificacao
              sValues = sValues & " )" & vbNewLine
              
              sAdd = sAdd & sValues
            Next a
        Next i
        sSQL = sSQL & vbNewLine & sInsert & vbNewLine & sAdd
        
        sSQL = sSQL & vbNewLine & "EXEC seguros_db.dbo.SEGS14967_SPS " & "'" & cUserName & "'" & vbNewLine
        
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, numCon, True)
        
        If Not rs.EOF Then
            SEGP0794_fim.iExibe_Alerta = rs(0)
        Else
            SEGP0794_fim.iExibe_Alerta = 0
        End If
        
        rs.Close
        
        For i = 1 To UBound(Avisos_Sinistros)
            If Proposta_Avisada(i).Situacao = 0 Then
            
                sSQL = "      SELECT r.resultado " & vbNewLine
                sSQL = sSQL & " FROM seguros_db.dbo.sinistro_classificacao_parametro_tb r WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & "WHERE r.sinistro_id = " & Avisos_Sinistros(i).Sinistro.sinistro_id
            
                Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, numCon, True)
                
                If Not rs.EOF Then
                    Proposta_Avisada(i).Situacao = rs(0)
                Else
                    Proposta_Avisada(i).Situacao = 0
                End If
            
                rs.Close
            
            End If
        Next i
        '08/07/2020 (ntendencia) - 00216281-esteira-pagamento-imediato-vida-individual-fase2 (fim)
        
        ConfirmarTransacao (numCon)
        GravaSinistros = True
    End If
    
    Exit Function
Resume
Error:
    Call TrataErroGeral("Grava Sinistro", "Aviso de Sinistro")
    MsgBox "Este aviso de sinistro n�o foi registrado. Opera��o falhou!", vbCritical, "Aviso de Sinistro"
    RetornarTransacao (numCon)
    Exit Function
       
End Function

Private Function ehSinistroCA(sinistro_id As String) As Boolean
Dim SQL As String
Dim rs As ADODB.Recordset

    SQL = "select * from evento_segbr_sinistro_tb with (nolock) "
    SQL = SQL & " where sinistro_id = " & sinistro_id
    SQL = SQL & " and evento_bb_id in (2000, 1100) "
    
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, True)
    If Not rs.EOF Then
        ehSinistroCA = True
    Else
        ehSinistroCA = False
    End If
    
End Function

Private Sub AtualizaDetalhamento(Sinistro As Sinistro)
Dim SQL As String
Dim rs As ADODB.Recordset
Dim i As Integer

Dim detalhamento_id As Integer
Dim linha_detalhe As Integer
Dim historico_item As Integer
Dim linha_historico As Integer


    'DETALHAMENTO - Colocar a reabertura no detalhamento do sinistro
    SQL = "set nocount on exec sinistro_detalhamento_spi " & Sinistro.sinistro_id
    SQL = SQL & "," & Sinistro.Apolice_id
    SQL = SQL & "," & Sinistro.Sucursal_id
    SQL = SQL & "," & Sinistro.Seguradora_id
    SQL = SQL & "," & Sinistro.ramo_id
    SQL = SQL & ",'" & Sinistro.dt_Aviso & "'"
    SQL = SQL & "," & 0    'Anotacao
    SQL = SQL & ",'N'"     'N�o Restritivo
    SQL = SQL & ",'" & cUserName & "'"
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, numCon, True)
    If Not rs.EOF Then
        detalhamento_id = rs(0)
        linha_detalhe = 1
        
        'Para o detalhamento
        SQL = "exec sinistro_linha_detalhe_spi " & Sinistro.sinistro_id
        SQL = SQL & "," & Sinistro.Apolice_id
        SQL = SQL & "," & Sinistro.Sucursal_id
        SQL = SQL & "," & Sinistro.Seguradora_id
        SQL = SQL & "," & Sinistro.ramo_id
        SQL = SQL & "," & detalhamento_id
        SQL = SQL & "," & linha_detalhe
        SQL = SQL & ",'" & "REABERTURA DO AVISO PELA CENTRAL ATENDIMENTO. MOTIVO(S):" & "'"
        SQL = SQL & ",'" & cUserName & "'"
        
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                     glAmbiente_id, _
                     App.Title, _
                     App.FileDescription, _
                     SQL, numCon, False)

        linha_detalhe = linha_detalhe + 1
        
        For i = 0 To 4
        
            If Trim(SEGP0794_4_1.txtMotivo(i).Text) <> "" Then
                SQL = "exec sinistro_linha_detalhe_spi " & Sinistro.sinistro_id
                SQL = SQL & "," & Sinistro.Apolice_id
                SQL = SQL & "," & Sinistro.Sucursal_id
                SQL = SQL & "," & Sinistro.Seguradora_id
                SQL = SQL & "," & Sinistro.ramo_id
                SQL = SQL & "," & detalhamento_id
                SQL = SQL & "," & linha_detalhe
                SQL = SQL & ",'" & Trim(SEGP0794_4_1.txtMotivo(i).Text) & "'"
                SQL = SQL & ",'" & cUserName & "'"
                
                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, numCon, False)
                             
                linha_detalhe = linha_detalhe + 1
            End If
            
        Next i
    End If
                            
End Sub

Private Sub AtualizaSinistroReanalise(sinistro_id As String, Situacao As String)
Dim SQL As String

    SQL = " exec sinistro_situacao_spu " & sinistro_id
    SQL = SQL & ", '" & Situacao & "'"
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, numCon, False)
                             
End Sub


Private Sub IncluirDocumentos(Indice As Integer)

    '#####################################################################
    'inclui documentos
    'Se n�o for cobertura generica, obtem os documentos de acordo com o produto, ramo, evento e cobertura
    Set AvisoRegras = CreateObject("SEGL0144.cls00326")

    AvisoRegras.mvarAmbienteId = glAmbiente_id
    AvisoRegras.mvarSiglaSistema = gsSIGLASISTEMA
    AvisoRegras.mvarSiglaRecurso = App.Title
    AvisoRegras.mvarDescricaoRecurso = App.FileDescription
    
    isubramo_id = ObtemSubRamo(IIf(Avisos_Sinistros(Indice).Sinistro.proposta_id = "", 0, Avisos_Sinistros(Indice).Sinistro.proposta_id))
    
    
    'INICIO - MU00416967 -  melhoria de aviso sinistro
    If (Avisos_Sinistros(Indice).Sinistro.produto_id = 1201 And Avisos_Sinistros(Indice).Sinistro.ramo_id = 61) Then
    
     Set rsPesquisa = AvisoRegras.ObterDocumentos(Avisos_Sinistros(Indice).Sinistro.produto_id, Avisos_Sinistros(Indice).Sinistro.ramo_id, isubramo_id, 2, CLng(Avisos_Sinistros(Indice).Sinistro.Causa_id), sCoberturasJaInseridas, True)
    
    Else
    
    Set rsPesquisa = AvisoRegras.ObterDocumentos(Avisos_Sinistros(Indice).Sinistro.produto_id, _
                                               Avisos_Sinistros(Indice).Sinistro.ramo_id, _
                                               isubramo_id, _
                                                CStr(TIPO_RAMO), _
                                                CLng(Avisos_Sinistros(Indice).Sinistro.Causa_id), _
                                                sCoberturasJaInseridas, True)
    End If ' FIM - MU00416967 -  Melhoria de aviso de sinistro
    Set AvisoRegras = Nothing
    
    If Not rsPesquisa.EOF Then
           
        Do While Not rsPesquisa.EOF
            SQL = "exec evento_segbr_sinistro_documento_spi " & Avisos_Sinistros(Indice).Sinistro.evento_id & "," & _
                                                        rsPesquisa("documento_id") & ",'" & _
                                                        rsPesquisa("documento") & "','" & _
                                                        cUserName & "'"
                                                        
            Debug.Print SQL
            
            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, _
                            numCon, _
                            False)
            rsPesquisa.MoveNext
        Loop
                
                
    'sergio.sn - 25/11/2010 - CASO N�O LOCALIZA DOCUMENTOS GRAVA DOCUMENTA��O B�SICA
    Else
    
            SQL = "exec evento_segbr_sinistro_documento_spi " & Avisos_Sinistros(Indice).Sinistro.evento_id & "," & _
                                                        "1" & ",'" & _
                                                        "Formul�rio ""aviso de sinistro"" assinado" & "','" & _
                                                        cUserName & "'"
                                                        
            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, _
                            numCon, _
                            False)
        
    'sergio.en - 25/11/2010 - CASO N�O LOCALIZA DOCUMENTOS GRAVA DOCUMENTA��O B�SICA
    End If
    rsPesquisa.Close

End Sub

'INICO - MU00416967 -  melhoria de aviso de sinistro
Public Function ObtemIndenizacaoMutuarioVida(produto_id As Long, _
                                            cliente_id As Long, _
                                            proposta_id As Long, _
                                            val_is As Double) As Currency
Dim SQL As String
Dim rs As ADODB.Recordset
On Error GoTo Trata_Erro
     SQL = ""
     SQL = "SELECT  ch.percentual_particip_renda "
     SQL = SQL & " FROM  seguros_db..cliente_tb c with(NOLOCK) "
     SQL = SQL & " JOIN seguros_db..cliente_habitacional_tb ch with(NOLOCK)"
     SQL = SQL & " ON  ch.proposta_id = " & proposta_id
     SQL = SQL & "  AND  c.cliente_id = ch.cliente_id "
     SQL = SQL & " AND   c.cliente_id = " & cliente_id
     
     

    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
    If Not rs.EOF Then
        ObtemIndenizacaoMutuarioVida = val_is * (CDbl(rs!percentual_particip_renda) / 100)
    Else
        ObtemIndenizacaoMutuarioVida = mvarVlrMinEstimativa
        Exit Function
    End If
    rs.Close
Exit Function

Trata_Erro:
    Call Err.Raise(Err.Number, , "SEGP0794_7.ObtemIndenizacaoMutuarioVida - " & Err.Description)

End Function
Private Function InserirObjetoSinistrado(evento_id As String, coberturas() As CoberturaAfetada)

Dim SQL As String
Dim rs As ADODB.Recordset
On Error GoTo Trata_Erro

For i = 1 To UBound(coberturas)
SQL = " select descricao ,tp_cobertura_id "
SQL = SQL & " from tp_cobertura_tb where tp_cobertura_id = " & coberturas(i).tp_cobertura_id
Next i


Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
               
If Not rs.EOF Then
Obj_sinis_Descricao = Mid(rs(0), 1, 70)
Obj_sinis_tp_cobertura_id = rs(1)
rs.Close
End If



'Grava na evento_segbr_sinistro_objeto_tb
SQL = ""
SQL = "exec evento_SEGBR_sinistro_objeto_sinistrado_spi "
SQL = SQL & evento_id & ", "
SQL = SQL & 1 & ", "
SQL = SQL & "'" & Obj_sinis_Descricao & "',"
SQL = SQL & "'" & cUserName & "',"
SQL = SQL & Obj_sinis_valor_estimado & ","
SQL = SQL & Obj_sinis_tp_cobertura_id & ""

Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                     glAmbiente_id, _
                     App.Title, _
                     App.FileDescription, _
                     SQL, numCon, False)
                     
                     
'rs.Close
Exit Function

Trata_Erro:
Call Err.Raise(Err.Number, , "SEGP0794_7.InserirObjetoSinistrado - " & Err.Description)

End Function
'FIM - MU00416967 -  melhoria de aviso de sinistro
'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
Public Function Valida_Fluxo_Pagamento_Imediato(ByVal i As Integer) As Boolean
 Dim rs As ADODB.Recordset
 Dim SQL As String
 Dim a As Long

 Valida_Fluxo_Pagamento_Imediato = False
 
        For a = 1 To UBound(Avisos_Sinistros(i).cobertura_afetada)
        
            SQL = ""
            SQL = "EXEC seguros_db.dbo.SEGS14630_SPS " & vbNewLine
            SQL = SQL & Avisos_Sinistros(i).Sinistro.proposta_id & "," & vbNewLine
            SQL = SQL & Avisos_Sinistros(i).Sinistro.Causa_id & "," & vbNewLine
            SQL = SQL & Avisos_Sinistros(i).cobertura_afetada(a).tp_cobertura_id & ""
            
            Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, _
                             True)
        
             If Not rs.EOF Then
                If rs(0) = 1 Then
                    Valida_Fluxo_Pagamento_Imediato = True
                    Avisos_Sinistros(i).cobertura_afetada(a).pagamento_imediato = True   'VALIDANDO A COBERTURA PARA PAGAMENTO IMEDIATO
                End If
            End If
        
            rs.Close
     Next a
     
End Function 'C00216281 - FIM
'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
Public Function Classifica_Pagamento_Imediato(ByVal i As Integer) As Boolean
    
    Dim rs As ADODB.Recordset
    Dim SQL As String
    Dim a As Long
    
    Classifica_Pagamento_Imediato = False
    
                     
    For a = 1 To UBound(Avisos_Sinistros(i).cobertura_afetada)
        'BUSCANDO A COBERTURA HABILITADA PARA O PAGAMENTO IMEDIATO
        If Avisos_Sinistros(i).cobertura_afetada(a).pagamento_imediato Then
        
            'CLASSIFICA E GRAVA O DETALHAMENTO
            SQL = ""
            SQL = "EXEC seguros_db.dbo.SEGS14666_SPI " & vbNewLine
            SQL = SQL & Avisos_Sinistros(i).Sinistro.produto_id & "," & vbNewLine
            SQL = SQL & Avisos_Sinistros(i).Sinistro.ramo_id & "," & vbNewLine
            SQL = SQL & Avisos_Sinistros(i).Sinistro.proposta_id & "," & vbNewLine
            SQL = SQL & Avisos_Sinistros(i).Sinistro.Causa_id & "," & vbNewLine
            SQL = SQL & Avisos_Sinistros(i).cobertura_afetada(a).tp_cobertura_id & "," & vbNewLine
            'SQL = SQL & Avisos_Sinistros(i).Sinistro.SeguradoComponente & "," & vbNewLine
            SQL = SQL & "'" & Avisos_Sinistros(i).Sinistro.dt_ocorrencia & "'," & vbNewLine
            'EVENTO_ID <> NULL E SINISTRO_ID = NULL
            'GRAVA O DETALHAMENTO NA EVENTO_SEGBR_SINISTRO_DETALHE_ATUAL_TB
            SQL = SQL & Avisos_Sinistros(i).Sinistro.evento_id & "," & vbNewLine
            SQL = SQL & " Null " & "," & vbNewLine '@sinistro_id
            SQL = SQL & "'" & cUserName & "'"
           ' SQL = SQL & ", 1 " ' DEBUG(1) NAO GRAVA NO DETALHAMENTO APENAS CLASSIFICA
            
            
            

            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, numCon, _
                            True)
                             
                             
        
             If Not rs.EOF Then
                If rs.Fields("PagtoImediato").value = 1 Then
                    Classifica_Pagamento_Imediato = True
                End If
                tp_sinistro_parametro_id = rs.Fields("tp_sinistro_parametro_id").value
            End If
            rs.Close
        End If
    
    
     Next a
End Function 'C00216281 - FIM
'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
Public Sub Gravar_classificacao_Pagamento_Imediato(ByVal i As Integer)
   Dim SQL As String
   
   
            SQL = ""
            SQL = "EXEC seguros_db.dbo.SEGS14652_SPI " & vbNewLine
            SQL = SQL & Proposta_Avisada(i).sinistro_id & "," & vbNewLine
            SQL = SQL & Proposta_Avisada(i).Situacao & "," & vbNewLine
            SQL = SQL & tp_sinistro_parametro_id & "," & vbNewLine
            SQL = SQL & "'" & cUserName & "'"
   
   
   Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, numCon, _
                            False)
   

End Sub 'C00216281 - FIM

' Cesar Santos CONFITEC - (SBRJ009952) 27/04/2020 - Inicio
Private Function VerificaPropostaAvisoForcado(proposta As String) As String
   If proposta <> "0" Then
        VerificaPropostaAvisoForcado = SEGP0794_4.AvisoForcado(proposta)
   Else
        VerificaPropostaAvisoForcado = "N"
   End If
End Function
' Cesar Santos CONFITEC - (SBRJ009952) 27/04/2020 - Fim

' Cesar Santos CONFITEC - (SBRJ009952) 28/05/2020 - Inicio
Private Sub GravarInformacaoComplementarDi(Evento_id As Double, _
                                                Data_Admissao As String, _
                                                Data_Aviso_Previo As String, _
                                                PIS_Empregado As String, _
                                                CNPJ_Empregador As String, _
                                                Valor_Recisao As Double, _
                                                Motivo_Demissao As Integer)


            SQL = ""
            SQL = "EXEC seguros_db.dbo.SEGS14869_SPI " & vbNewLine
            SQL = SQL & Evento_id & "," & vbNewLine
            SQL = SQL & "'" & Data_Admissao & "'," & vbNewLine
            SQL = SQL & "'" & Data_Aviso_Previo & "'," & vbNewLine
            SQL = SQL & "'" & PIS_Empregado & "'," & vbNewLine
            SQL = SQL & "'" & CNPJ_Empregador & "'," & vbNewLine
            SQL = SQL & TrocaVirgulaPorPonto(Valor_Recisao) & "," & vbNewLine
            SQL = SQL & Motivo_Demissao & "," & vbNewLine
            SQL = SQL & "'" & cUserName & "'"
            
   
   
   Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, numCon, _
                            False)


End Sub
' Cesar Santos CONFITEC - (SBRJ009952) 28/05/2020 - Fim