VERSION 5.00
Object = "{629074EB-AE57-4B76-8AF4-1B62557ED9A6}#1.0#0"; "GridDinamico.ocx"
Begin VB.Form SEGP0794_6 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0794 - Aviso de Sinistro pela CA"
   ClientHeight    =   7110
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9300
   Icon            =   "SEGP0794_6.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7110
   ScaleWidth      =   9300
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame4 
      Caption         =   "Ag�ncia de Contato"
      Enabled         =   0   'False
      Height          =   2355
      Left            =   0
      TabIndex        =   13
      Top             =   4230
      Width           =   9285
      Begin VB.TextBox txtCodigoAgencia 
         Alignment       =   1  'Right Justify
         Height          =   330
         Left            =   90
         TabIndex        =   20
         Text            =   "0"
         Top             =   495
         Width           =   1455
      End
      Begin VB.TextBox txtDV 
         Height          =   330
         Left            =   1620
         TabIndex        =   19
         Top             =   495
         Width           =   510
      End
      Begin VB.TextBox txtNomeAgencia 
         Height          =   330
         Left            =   3015
         TabIndex        =   18
         Top             =   495
         Width           =   6135
      End
      Begin VB.TextBox txtEndereco 
         Height          =   330
         Left            =   90
         TabIndex        =   17
         Top             =   1170
         Width           =   4965
      End
      Begin VB.TextBox txtMunicipio 
         Height          =   330
         Left            =   90
         TabIndex        =   16
         Top             =   1845
         Width           =   7620
      End
      Begin VB.TextBox txtUF 
         Height          =   330
         Left            =   8595
         TabIndex        =   15
         Top             =   1845
         Width           =   555
      End
      Begin VB.TextBox txtBairro 
         Height          =   330
         Left            =   5130
         TabIndex        =   14
         Top             =   1170
         Width           =   4020
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "C�digo da ag�ncia:"
         Height          =   195
         Left            =   90
         TabIndex        =   27
         Top             =   270
         Width           =   1380
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "DV:"
         Height          =   195
         Left            =   1620
         TabIndex        =   26
         Top             =   270
         Width           =   270
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Nome da ag�ncia"
         Height          =   195
         Left            =   3015
         TabIndex        =   25
         Top             =   270
         Width           =   1260
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "Endere�o:"
         Height          =   195
         Left            =   90
         TabIndex        =   24
         Top             =   945
         Width           =   735
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Munic�pio"
         Height          =   195
         Left            =   90
         TabIndex        =   23
         Top             =   1620
         Width           =   705
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "UF:"
         Height          =   195
         Left            =   8595
         TabIndex        =   22
         Top             =   1575
         Width           =   255
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Bairro:"
         Height          =   195
         Left            =   5130
         TabIndex        =   21
         Top             =   945
         Width           =   450
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Agencias"
      Height          =   3030
      Left            =   0
      TabIndex        =   12
      Top             =   1170
      Width           =   9285
      Begin GridFrancisco.GridDinamico grdResult 
         Height          =   2655
         Left            =   120
         TabIndex        =   29
         Top             =   240
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   4683
         BorderStyle     =   1
         AllowUserResizing=   3
         EditData        =   0   'False
         Highlight       =   1
         ShowTip         =   0   'False
         SortOnHeader    =   0   'False
         BackColor       =   -2147483643
         BackColorBkg    =   -2147483633
         BackColorFixed  =   -2147483633
         BackColorSel    =   -2147483635
         FixedCols       =   1
         FixedRows       =   1
         FocusRect       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   -2147483640
         ForeColorFixed  =   -2147483630
         ForeColorSel    =   -2147483634
         GridColor       =   -2147483630
         GridColorFixed  =   12632256
         GridLine        =   1
         GridLinesFixed  =   2
         MousePointer    =   0
         Redraw          =   -1  'True
         Rows            =   2
         TextStyle       =   0
         TextStyleFixed  =   0
         Cols            =   2
         RowHeightMin    =   0
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1140
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9285
      Begin VB.CommandButton cmdPesquisarAgencia 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1680
         TabIndex        =   8
         Top             =   585
         Width           =   440
      End
      Begin VB.TextBox txtPesquisaAgencia 
         Height          =   330
         Left            =   165
         TabIndex        =   7
         Top             =   585
         Width           =   1500
      End
      Begin VB.CommandButton cmdBuscaAgencia 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   8670
         TabIndex        =   6
         Top             =   585
         Width           =   440
      End
      Begin VB.ComboBox cboMunicipio 
         Enabled         =   0   'False
         Height          =   315
         Left            =   3645
         TabIndex        =   5
         Top             =   593
         Width           =   5010
      End
      Begin VB.ComboBox cboEstado 
         Height          =   315
         Left            =   2760
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   593
         Width           =   870
      End
      Begin VB.Label Label3 
         Caption         =   "Pesquisa de Ag�ncia de Contato"
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   30
         Width           =   2415
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "C�digo da ag�ncia:"
         Height          =   195
         Left            =   165
         TabIndex        =   11
         Top             =   360
         Width           =   1380
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Munic�pio"
         Height          =   195
         Left            =   3645
         TabIndex        =   10
         Top             =   360
         Width           =   705
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Estado:"
         Height          =   195
         Left            =   2760
         TabIndex        =   9
         Top             =   360
         Width           =   540
      End
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   6615
      TabIndex        =   2
      Top             =   6660
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   8010
      TabIndex        =   1
      Top             =   6660
      Width           =   1275
   End
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   5220
      TabIndex        =   0
      Top             =   6660
      Width           =   1275
   End
End
Attribute VB_Name = "SEGP0794_6"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CPF As String
Public Coluna As Integer


Private Sub cboEstado_Click()
  If cboEstado.ListIndex = -1 Then
        'MsgBox "Escolha um estado."
        'cboEstado.SetFocus
        Exit Sub
    End If
    Monta_cboMunicipio
End Sub

Private Sub cmdSair_Click()
    Sair Me
End Sub

Private Sub cmdVoltar_Click()
    Me.Hide
    SEGP0794_5.Show
End Sub

Private Sub Form_Load()
    Monta_cboEstado
    BuscaUltimaContratacao
    Monta_GridAgencia
    
End Sub

Public Sub BuscaUltimaContratacao()
    Dim SQL As String
    Dim rs  As ADODB.Recordset
    Dim PROP As Long
    
    If SEGP0794_2.GrdResultadoPesquisa.Rows = 1 Then Exit Sub
    SQL = ""
    SQL = SQL & " select max(proposta_id) " & vbCrLf
    SQL = SQL & " from proposta_tb with (nolock) " & vbCrLf
    SQL = SQL & " where prop_cliente_id in (" & SEGP0794_4.LISTA_CLIENTE & ")" & vbCrLf
   
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
    If rs.EOF Then Exit Sub
    PROP = IIf(IsNull(rs(0)), 0, rs(0))
    SQL = ""
    SQL = SQL & " select 'agencia' = cont_agencia_id "
    SQL = SQL & " from proposta_adesao_tb with (nolock)"
    SQL = SQL & " Where proposta_id = " & PROP
    Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
    If rs.EOF Then
        SQL = ""
        SQL = SQL & " select 'agencia' = agencia_id "
        SQL = SQL & " from proposta_fechada_tb pf with (nolock)"
        SQL = SQL & " Where proposta_id = " & PROP
        Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        If rs.EOF Then
            SQL = ""
            SQL = SQL & " select 'agencia' = agencia_cobradora"
            SQL = SQL & " from proposta_basica_vida_tb pb with (nolock)"
            SQL = SQL & " Where proposta_id = " & PROP
            Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        End If
    End If
    
    If rs.EOF Then
        txtPesquisaAgencia.Text = ""
        cmdPesquisarAgencia_Click
        Exit Sub
    End If
    txtPesquisaAgencia.Text = IIf(IsNull(rs!Agencia), "", rs!Agencia)
    cmdPesquisarAgencia_Click
End Sub

Private Sub cmdBuscaAgencia_Click()
    If cboMunicipio.ListIndex = -1 Then
        MsgBox "Escolha um munic�pio."
        cboMunicipio.SetFocus
        Exit Sub
    End If
    Monta_GridAgencia
End Sub


Private Sub cmdContinuar_Click()
	Dim i As Long

    With SEGP0794_7
        Me.MousePointer = vbHourglass
        If Val(Me.txtCodigoAgencia.Text) <> 0 Then
             .txtCodigoAgencia.Text = txtCodigoAgencia.Text
             .txtDV.Text = txtDV.Text
             .txtNomeAgencia.Text = txtNomeAgencia.Text
             .txtEnderecoAgencia.Text = txtEndereco.Text
             .txtBairroAgencia.Text = txtBairro.Text
             .txtMunicipioAgencia.Text = txtMunicipio.Text
             .txtUFAgencia.Text = txtUF.Text
        Else
            Me.MousePointer = vbDefault
            MsgBox "� preciso especificar a agencia de contato!", vbInformation, "Aviso de Sinistro"
            txtPesquisaAgencia.SetFocus
            Exit Sub
        End If
        
        Me.MousePointer = vbDefault
        Call montacabecalhoGrid
        Call PreenchePropostasAfetadas
      '  Call PreencheAviso
        
        ' Cesar Santos CONFITEC - (SBRJ009952) 02/06/2020 inicio
        .SSTab1.Tab = 0
        
        For i = 1 To (.GridPropostaAfetada.Rows - 1)
            If .GridPropostaAfetada.TextMatrix(i, 12) = 1243 Then
                If (SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex)) = 425 Then
                   Me.Hide
                   SEGP0794_7_3.Show
                   Exit Sub
                End If
            End If
        Next i
        
        .Show
        ' Cesar Santos CONFITEC - (SBRJ009952) 02/06/2020 fim
        
    End With
    Me.Hide
End Sub

Private Sub montaGrid()
    With grdResult
        .Cols = 6
        
        .TextMatrix(0, 0) = ""
        .TextMatrix(0, 1) = "Agencia"
        .TextMatrix(0, 2) = "DV"
        .TextMatrix(0, 3) = "Nome"
        .TextMatrix(0, 4) = "Endere�o"
        .TextMatrix(0, 5) = "Bairro"
        
        .AutoFit H�brido
    End With
End Sub

Public Sub cmdPesquisarAgencia_Click()
    Dim Rst  As ADODB.Recordset
    Dim SQL As String
    Screen.MousePointer = 11
    
    
    
    'AFONSO DUTRA NOGUEIRA FILHO - G&P - DEMANDA: 283025 - 18/03/2008
    'QUANDO ESSA FUN��O � REAPROVEITADA EM OUTRO FORM OCORRE UM ERRO NA LINHA
    'txtPesquisaAgencia.SetFocus - POR ISSO FOI ADICIONADO UMA VERIFICA��O
    ' PARA SABER SE O FORM ESTA ABERTO
    
    If SEGP0794_6.Visible = True Then
    
        '276839
        If Not IsNumeric(txtPesquisaAgencia.Text) Then
            MsgBox "Favor informar o valor correto da ag�ncia."
            Screen.MousePointer = 0
            txtPesquisaAgencia.SetFocus
            Exit Sub
        End If
        'MU-2018-060029 - Edilson Silva - 17/10/2018 - Inicio
        'If CDbl(txtPesquisaAgencia.Text) < 0 Or CDbl(txtPesquisaAgencia.Text) > 5000 Then
        If (CDbl(txtPesquisaAgencia.Text) < 0 Or CDbl(txtPesquisaAgencia.Text) > 5000) And Not ((SEGP0794_2.GrdResultadoPesquisa.TextMatrix(1, 11) = 1201) Or (SEGP0794_2.GrdResultadoPesquisa.TextMatrix(1, 11) = 14) Or (SEGP0794_2.GrdResultadoPesquisa.TextMatrix(1, 11) = 722)) Then
            MsgBox "Favor informar o valor correto da ag�ncia."
            Screen.MousePointer = 0
            txtPesquisaAgencia.SetFocus
            Exit Sub
        End If
        'MU-2018-060029 - Edilson Silva - 17/10/2018 - Fim
    End If
    
    If txtPesquisaAgencia.Text <> "" Then
    
        SQL = ""
        SQL = " SELECT distinct a.agencia_id, nome = isnull(a.nome,''), estado = isnull(a.estado,''), a.prefixo, " _
            & " dv_prefixo = isnull(a.dv_prefixo,0), endereco = isnull(a.endereco,''), " _
            & " bairro = isnull(a.bairro,''),  'municipio' = m.nome " _
            & " FROM agencia_tb a with (nolock), municipio_tb m with (nolock)" _
            & " WHERE a.municipio_id = m.municipio_id" _
            & " AND  a.estado = m.estado" _
            & " AND  a.banco_id = " & banco_id _
            & " AND prefixo = " & txtPesquisaAgencia.Text
            
        Set Rst = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        
        If Rst.EOF Then
            cboEstado.ListIndex = -1
            cboMunicipio.ListIndex = -1
            txtCodigoAgencia.Text = ""
            txtDV.Text = ""
            txtNomeAgencia.Text = ""
            txtEndereco.Text = ""
            txtBairro.Text = ""
            txtMunicipio.Text = ""
            txtUF.Text = ""
            Screen.MousePointer = 0
            Exit Sub
        Else
            cboEstado.ListIndex = -1
            cboMunicipio.ListIndex = -1
            txtCodigoAgencia.Text = Rst!prefixo
            txtDV.Text = Rst!dv_prefixo
            txtNomeAgencia.Text = UCase(Rst!Nome)
            txtEndereco.Text = UCase(Rst!Endereco)
            txtBairro.Text = UCase(Rst!Bairro)
            txtMunicipio.Text = UCase(Rst!Municipio)
            txtUF.Text = Rst!estado
        End If
        
    End If
    Screen.MousePointer = 0
End Sub


Private Sub Monta_cboEstado()
Dim i As Integer

    On Error GoTo TratarErro
    
    Dim Rst  As ADODB.Recordset
    Dim SQL As String
    Screen.MousePointer = 11
    SQL = "SELECT distinct estado " _
        & "FROM seguros_db..municipio_tb with (nolock) " _
        & "ORDER BY estado"
    Set Rst = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
    i = 1
    With cboEstado
        .Clear
        If Not Rst.EOF Then
           While Not Rst.EOF
                 .AddItem Rst(0)
                 .ItemData(.NewIndex) = i
                 Rst.MoveNext
                 i = i + 1
           Wend
        End If
        Rst.Close
    End With
    
    Screen.MousePointer = 0
    Exit Sub
TratarErro:
End Sub

Private Sub Monta_cboMunicipio()
    On Error GoTo TratarErro
    Dim Rst  As ADODB.Recordset
    Dim SQL As String
    
    Screen.MousePointer = 11
    SQL = "SELECT distinct municipio_id, nome " _
        & "FROM seguros_db..municipio_tb with (nolock) " _
        & "WHERE estado = '" & cboEstado.Text & "' " _
        & "ORDER BY nome"
    Set Rst = ExecutarSQL(gsSIGLASISTEMA, _
                          glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    With cboMunicipio
        .Clear
        If Not Rst.EOF Then
           While Not Rst.EOF
                 .AddItem Rst(1)
                 .ItemData(.NewIndex) = Rst(0)
                 Rst.MoveNext
           Wend
           .Enabled = True
        Else
            .Enabled = False
        End If
        
        Rst.Close
        
    End With
    Screen.MousePointer = 0
    Exit Sub
TratarErro:
End Sub

Private Sub Monta_GridAgencia()
    On Error GoTo TratarErro
    Dim slinha As String
    Dim Rst  As ADODB.Recordset
    Dim SQL As String
    Dim GridResposta As Boolean
    
    Screen.MousePointer = 11
    SQL = ""
    If cboMunicipio.ListIndex <> -1 Then
        
        'Raimundo - GPTI - 27/02/2009
        'Flow 812068
                
        SQL = ""
        SQL = SQL & "SELECT DISTINCT" & vbNewLine
        SQL = SQL & "  IsNull(prefixo, agencia_id) Agencia, " & vbNewLine
        SQL = SQL & "  dv_prefixo DV, " & vbNewLine
        SQL = SQL & "  nome Nome,   " & vbNewLine
        SQL = SQL & "  endereco Endereco, " & vbNewLine
        SQL = SQL & "  bairro Bairro  " & vbNewLine
        SQL = SQL & "FROM seguros_db..agencia_tb  with (nolock)  " & vbNewLine
        SQL = SQL & "WHERE municipio_id = " & cboMunicipio.ItemData(cboMunicipio.ListIndex) & vbNewLine
        SQL = SQL & "AND banco_id = " & banco_id & vbNewLine
        SQL = SQL & "ORDER BY nome" & vbNewLine
                
        'Raimundo - Fim

    Else
        SQL = " select 'Agencia' = '', " & vbCrLf
        SQL = SQL & " 'DV' = '', "
        SQL = SQL & " 'Nome      ' = '', " & vbCrLf
        SQL = SQL & " 'Endereco' = '', " & vbCrLf
        SQL = SQL & " 'Bairro' = '' " & vbCrLf
        
    End If
    Set Rst = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
        
    
    GridResposta = grdResult.PopulaGrid(Rst)
    
'       While Not Rst.EOF
'        slinha = ""
'        slinha = Rst!prefixo _
'        & vbTab & Rst!dv_prefixo _
'        & vbTab & Rst!nome _
'        & vbTab & Rst!Endereco _
'        & vbTab & Rst!Bairro
'
'        grdResult.AddItem slinha
'        grdResult.Row = grdResult.Rows - 1
'        grdResult.col = 1
'        Rst.MoveNext
'       Wend
  
    Rst.Close
    Screen.MousePointer = 0
    
    Exit Sub
TratarErro:
End Sub

Private Sub grdResult_Click()
    txtCodigoAgencia.Text = grdResult.TextMatrix(grdResult.Row, 1)
    txtDV.Text = grdResult.TextMatrix(grdResult.Row, 2)
    txtNomeAgencia.Text = grdResult.TextMatrix(grdResult.Row, 3)
    txtEndereco.Text = grdResult.TextMatrix(grdResult.Row, 4)
    txtBairro.Text = grdResult.TextMatrix(grdResult.Row, 5)
    txtMunicipio.Text = cboMunicipio.Text
    txtUF.Text = cboEstado.Text
End Sub

Private Sub cmdLimparCampos_Click()
    txtCodigoAgencia.Text = ""
    txtDV.Text = ""
    txtNomeAgencia.Text = ""
    txtEndereco.Text = ""
    txtBairro.Text = ""
    txtMunicipio.Text = ""
    txtUF.Text = ""
    cboEstado.ListIndex = -1
    cboMunicipio.ListIndex = -1
    txtPesquisaAgencia.Text = ""
End Sub

Private Function VerificaSeTitConj(proposta_id As Long) As String
Dim Linha As Long

With SEGP0794_4.GrdResultadoPesquisa
    For Linha = 1 To .Rows - 1
        If .TextMatrix(Linha, 1) = "AVSR" Then
        'If .TextMatrix(Linha, 1) = "AVSR" Or _ BRUNO MEDEIROS - DEMANDA 18232638
         '  .TextMatrix(Linha, 1) = "RNLS" Then
                If .TextMatrix(Linha, 5) = proposta_id Then
                    VerificaSeTitConj = .TextMatrix(Linha, 10)
                    Exit For
                End If
        End If
    Next Linha
End With
End Function

Private Function VerificaClienteIDTitConj(proposta_id As Long) As String
Dim Linha As Long

With SEGP0794_4.GrdResultadoPesquisa
    For Linha = 1 To .Rows - 1
    If .TextMatrix(Linha, 1) = "AVSR" Then
       ' If .TextMatrix(Linha, 1) = "AVSR" Or _ BRUNO MEDEIROS - DEMANDA 18232638
           '.TextMatrix(Linha, 1) = "RNLS" Then
                If .TextMatrix(Linha, 5) = proposta_id Then
                    VerificaClienteIDTitConj = .TextMatrix(Linha, 13)
                    Exit For
                End If
        End If
    Next Linha
End With
End Function

Public Sub PreencheAviso()
Dim contLinha As Long
    With SEGP0794_7.GridPropostaAfetada
        If .Rows = 1 Then Exit Sub
        If propostasAvisadas = "" And propostasReanalise = "" Then Exit Sub
        For contLinha = 1 To .Rows - 1
            If InStr(1, propostasAvisadas, .TextMatrix(contLinha, 5)) <> 0 Then
                .TextMatrix(contLinha, 1) = "AVSR"
           ' ElseIf InStr(1, propostasReanalise, .TextMatrix(contLinha, 5)) <> 0 Then
                '.TextMatrix(contLinha, 1) = "RNLS"
            End If
            .TextMatrix(contLinha, 10) = VerificaSeTitConj(.TextMatrix(contLinha, 5))
            .TextMatrix(contLinha, 13) = VerificaClienteIDTitConj(.TextMatrix(contLinha, 5))
        Next contLinha
    End With
End Sub

Public Sub PreenchePropostasAfetadas()
Dim rs As ADODB.Recordset

Dim i As Long

Dim coberturas() As Long
Dim estim As estimativa
Dim SQL As String
Dim RespostaProposta As Boolean




If propostasAvisadas <> "" Or propostasReanalise <> "" Then
    SQL = ""
    For i = 1 To SEGP0794_4.GrdResultadoPesquisa.Rows - 1
       If SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 1) = "AVSR" Then
       
      'If SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 1) = "AVSR" Or _  BRUNO MEDEIROS DEMANDA 18232638
         '  SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 1) = "RNLS" Then
                If SQL <> "" Then
                    SQL = SQL & " union" & vbCrLf
                End If
                
                SQL = SQL & " select distinct 'Aviso  ' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 1) & "'," & vbCrLf
                SQL = SQL & "   'Tipo Ramo' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 2) & "', " & vbCrLf
                SQL = SQL & "   'Situa��o' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 3) & "', "
                SQL = SQL & "   'Produto' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 4) & "', " & vbCrLf
                SQL = SQL & "   'Proposta AB' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 5) & "', " & vbCrLf
                SQL = SQL & "   'Proposta BB' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 6) & "',"
                SQL = SQL & "   'Ramo' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 7) & "'," & vbCrLf
                SQL = SQL & "   'Ap�lice' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 8) & "', " & vbCrLf
                
                'sergio.sn - 05/11/2010 -  CORRE��O PARA EXIBIR O NOME DO SEGURADO TITULAR
                If SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 12) <> "0" Then 'TESTE SE O AVISO � SEM PROPOSTA
                    
                    Dim strRetorno As String
                    
                    strRetorno = RetornarSeguradoTitular(SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 5), _
                                                            SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 12), _
                                                            SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 9))
                
                    SQL = SQL & "   'Segurado Titular' =  '" & strRetorno & "', " & vbCrLf
                                                                                        
                    If strRetorno = SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 9) Then
                    
                        SQL = SQL & "   'T/C     ' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 10) & "'," & vbCrLf
                        
                    Else
                    
                        SQL = SQL & "   'T/C     ' = 'TITULAR/EMPREGADO/DIRIGENTE'," & vbCrLf
                    
                    End If
                        
                    'eduardo.amaral(nova consultoria) 27/10/2011 12363945 - BB Seguro Vida Empresa FLEX
                    'Recurso utilizado para colocar o tamanho da coluna tp_componente_id = 0
                    Coluna = 21
                    
                Else
                
                    SQL = SQL & "   'Segurado Titular' =  '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 9) & "', " & vbCrLf
                    'eduardo.amaral(nova consultoria) 27/10/2011 12363945 - BB Seguro Vida Empresa FLEX
                    'Recurso utilizado para colocar o tamanho da coluna tp_componente_id = 0
                    Coluna = 20
                    
                End If
                
                'sergio.en - 05/11/2010 -  CORRE��O PARA EXIBIR O NOME DO SEGURADO TITULAR
                
                'sergio.so - 05/11/2010 -  CORRE��O PARA EXIBIR O NOME DO SEGURADO TITULAR
                
                'sql = sql & "   'Segurado Titular' =  '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 9) & "', " & vbCrLf
                'sql = sql & "   'T/C     ' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 10) & "'," & vbCrLf
                
                'sergio.eo - 05/11/2010 -  CORRE��O PARA EXIBIR O NOME DO SEGURADO TITULAR
                
                SQL = SQL & "   'Vig�ncia' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 11) & "'," & vbCrLf
                SQL = SQL & "   'C�d Produto' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 12) & "'," & vbCrLf
                SQL = SQL & "   'C�d Cliente' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 13) & "'," & vbCrLf
                SQL = SQL & "   'SubGrupo' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 14) & "'," & vbCrLf
                SQL = SQL & "   'Sucursal' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 17) & "', " & vbCrLf
                SQL = SQL & "   'Seguradora' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 18) & "'," & vbCrLf
                'SQL = SQL & "   'CPF' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 19) & "'," & vbCrLf
                
                '------------------------------------------------------
                'Ricardo Toledo : 04/05/2012 : INC000003518952 : Inicio
                
                'dicaversan 01/12/2010 - recuperar o cpf do titular
                                'SQL = SQL & "   'CPF' = '" & CPF & "'," & vbCrLf
                
                  'BRUNO MEDEIROS DEMANDA 18232638
                'If SEGP0794_4.grdResultadoPesquisa.TextMatrix(i, 1) = "RNLS" And SEGP0794_4.grdResultadoPesquisa.TextMatrix(1, 10) = "C�NJUGE" Then
                 '   SQL = SQL & "   'CPF' = '" & SEGP0794_4.grdResultadoPesquisa.TextMatrix(1, 19) & "'," & vbCrLf
                'Else
                '    SQL = SQL & "   'CPF' = '" & CPF & "'," & vbCrLf
                'End If
                
                SQL = SQL & "'CPF' = '" & CPF & "'," & vbCrLf
                
                'Ricardo Toledo : 04/05/2012 : INC000003518952 : FIM
                '------------------------------------------------------
                
                SQL = SQL & "   'Sexo' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 21) & "'," & vbCrLf
                SQL = SQL & "   'Dt Nasc' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 20) & "', " & vbCrLf
                SQL = SQL & "   'Moeda' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 22) & "', " & vbCrLf
                'eduardo.amaral(nova consultoria) 27/10/2011 12363945 - BB Seguro Vida Empresa FLEX
                SQL = SQL & "   'tp_componente_id' = '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 26) & "'" & vbCrLf
               
        End If
    Next i
        
        
Else
        SQL = ""
        SQL = SQL & " select 'Aviso ' = 'AVSR', " & vbCrLf
        SQL = SQL & " 'Tipo de Aviso de Sinistro' = 'Sem Proposta       '," & vbCrLf
        SQL = SQL & " 'Tipo de Ramo' = 'N�o especificado '," & vbCrLf
        
        'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
        'SQL = SQL & " 'Segurado' = '" & SEGP0794_9.txtNomeSegurado.Text & "'," & vbCrLf
        SQL = SQL & " 'Segurado' = '" & MudaAspaSimples(SEGP0794_9.txtNomeSegurado.Text) & "'," & vbCrLf
        'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Fim
        
        SQL = SQL & " 'CPF' = '" & SEGP0794_9.mskCPF.Text & "'," & vbCrLf
        SQL = SQL & " 'Dt Nascimento' = '" & SEGP0794_9.mskDtNascimento.Text & "'," & vbCrLf
        SQL = SQL & " 'Sexo' = '" & Left(SEGP0794_9.cboSexo.Text, 1) & "'," & vbCrLf
        SQL = SQL & " 'Proposta BB' = '" & SEGP0794_9.txtPropostaBB & "'" & vbCrLf
        
End If
        Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, _
                             True)
      
        RespostaProposta = SEGP0794_7.GridPropostaAfetada.PopulaGrid(rs)
        'eduardo.amaral(nova consultoria) 27/10/2011 12363945 - BB Seguro Vida Empresa FLEX
        Call SEGP0794_7.GridPropostaAfetada.TamanhoColuna(Coluna, 0)

    
    
End Sub

Private Sub montacabecalhoGrid()
    With SEGP0794_7.GridPropostaAfetada
        
        .Cols = 14
        .Rows = 2
        
        .TextMatrix(0, 0) = "Aviso"
        .TextMatrix(0, 1) = "Tipo Ramo"
        .TextMatrix(0, 2) = "Situa��o"
        .TextMatrix(0, 3) = "Produto"
        .TextMatrix(0, 4) = "Proposta AB"
        .TextMatrix(0, 5) = "Proposta BB"
        .TextMatrix(0, 6) = "Ramo"
        .TextMatrix(0, 7) = "Ap�lice"
        .TextMatrix(0, 8) = "Proponente"
        .TextMatrix(0, 9) = "Titular/Conjuge"
        .TextMatrix(0, 10) = "Vig�ncia"
        .TextMatrix(0, 11) = "Produto"
        .TextMatrix(0, 12) = "Cliente"
        .TextMatrix(0, 13) = "Subgrupo"
        
        .TextMatrix(1, 0) = ""
        .TextMatrix(1, 1) = ""
        .TextMatrix(1, 2) = ""
        .TextMatrix(1, 3) = ""
        .TextMatrix(1, 4) = ""
        .TextMatrix(1, 5) = ""
        .TextMatrix(1, 6) = ""
        .TextMatrix(1, 7) = ""
        .TextMatrix(1, 8) = ""
        .TextMatrix(1, 9) = ""
        .TextMatrix(1, 10) = ""
        .TextMatrix(1, 11) = ""
        .TextMatrix(1, 12) = ""
        .TextMatrix(1, 13) = ""
        
        .AutoFit H�brido
        
    End With

End Sub


Private Sub txtPesquisaAgencia_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And txtPesquisaAgencia.Text <> "" Then
        SendKeys "{TAB}"
    End If
End Sub

Private Function RetornarSeguradoTitular(strPropostaId As String, _
                                         strProdutoId As String, _
                                         strSeguradoTitular As String) As String

    Dim rs As ADODB.Recordset
    Dim SQL As String
    'dicaversan 01/12/2010 - recuperar o cpf do titular
    CPF = ""
    ' 'T/C' = 'TITULAR/EMPREGADO/DIRIGENTE'

    SQL = ""
    'SQL = SQL & " SELECT  cli.nome, cli.cpf_cnpj " & vbNewLine
    SQL = SQL & " SELECT  ISNULL(CLI.NOME, '') AS NOME, ISNULL(CLI.CPF_CNPJ, '') AS CPF_CNPJ " & vbNewLine
    SQL = SQL & "   FROM  cliente_tb cli with (nolock) " & vbNewLine
    SQL = SQL & "         INNER JOIN " & vbNewLine
    SQL = SQL & "         proposta_tb pro with (nolock) " & vbNewLine
    SQL = SQL & "         ON cli.cliente_id = pro.prop_cliente_id " & vbNewLine
    SQL = SQL & "  WHERE  pro.proposta_id = " & strPropostaId & vbNewLine
    SQL = SQL & "         AND pro.produto_id = " & strProdutoId & vbNewLine

    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)


    If rs.EOF Then

        'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
        'RetornarSeguradoTitular = strSeguradoTitular
        RetornarSeguradoTitular = MudaAspaSimples(strSeguradoTitular)
        'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Fim

    Else
        CPF = rs.Fields("cpf_cnpj")

        'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Inicio
        'RetornarSeguradoTitular = rs.Fields("nome")
        RetornarSeguradoTitular = MudaAspaSimples(rs.Fields("nome"))
        'Ricardo Toledo (Confitec) : 20/07/2013 : INC000004143145 : Fim

    End If


End Function
