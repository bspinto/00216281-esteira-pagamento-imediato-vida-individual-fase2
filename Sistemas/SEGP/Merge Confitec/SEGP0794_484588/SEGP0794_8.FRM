VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form SEGP0794_8 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Coberturas da Proposta"
   ClientHeight    =   4770
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10380
   Icon            =   "SEGP0794_8.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4770
   ScaleWidth      =   10380
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "&OK"
      Height          =   420
      Left            =   8700
      TabIndex        =   3
      Top             =   4200
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   "Coberturas Vigentes"
      Height          =   2415
      Left            =   0
      TabIndex        =   0
      Top             =   1560
      Width           =   10320
      Begin MSFlexGridLib.MSFlexGrid GridCobertura 
         Height          =   1935
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   3413
         _Version        =   393216
         FixedCols       =   0
         Appearance      =   0
      End
   End
   Begin VB.Label lblSubEvento 
      Caption         =   "SubEvento"
      Height          =   255
      Left            =   1560
      TabIndex        =   12
      Top             =   1200
      Width           =   6735
   End
   Begin VB.Label Label4 
      Caption         =   "SubEvento :"
      Height          =   375
      Left            =   240
      TabIndex        =   11
      Top             =   1200
      Width           =   1095
   End
   Begin VB.Label Label5 
      Caption         =   "Evento Sinistro:"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   840
      Width           =   1215
   End
   Begin VB.Label lblEvento 
      Caption         =   "Evento"
      Height          =   255
      Left            =   1560
      TabIndex        =   8
      Top             =   840
      Width           =   6615
   End
   Begin VB.Label lblProduto 
      Caption         =   "Produto"
      Height          =   255
      Left            =   3840
      TabIndex        =   7
      Top             =   480
      Width           =   4215
   End
   Begin VB.Label lblProposta 
      Caption         =   "Proposta"
      Height          =   255
      Left            =   1560
      TabIndex        =   6
      Top             =   480
      Width           =   1455
   End
   Begin VB.Label lblNome 
      Caption         =   "Nome"
      Height          =   255
      Left            =   1560
      TabIndex        =   5
      Top             =   120
      Width           =   6615
   End
   Begin VB.Label Label3 
      Caption         =   "Cliente:"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "Produto:"
      Height          =   255
      Left            =   3120
      TabIndex        =   2
      Top             =   480
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Proposta:"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   735
   End
End
Attribute VB_Name = "SEGP0794_8"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public proposta_id As Long
Public tp_componente_id As Integer
Public dtOcorrencia As String
Public sub_grupo_id As Integer
Public Evento_sinistro_id As Long
Public SubEvento_sinistro_id As Long

Private Sub cmdVoltar_Click()
Unload Me
End Sub

Private Sub Form_Activate()
    SelecionarCoberturas
End Sub

Public Sub SelecionarCoberturas()
Dim sql As String
Dim rs As ADODB.Recordset
Dim estim_cobertura As estimativa
Dim linhas As Integer

    On Error GoTo Error
    
    'Seleciona as coberturas da proposta
    sql = ""
    sql = sql & " exec consultar_coberturas_sps '" & dtOcorrencia & "'," & _
                proposta_id & "," & tp_componente_id
                
    If sub_grupo_id > 0 Then
        sql = sql & " ," & sub_grupo_id
        sql = sql & " , 1"
    Else
        sql = sql & " ,null "
        sql = sql & " , 1"
    End If
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         sql, _
                         True)
    
    If rs.EOF Then
        GridCobertura.Rows = 1
        GridCobertura.Cols = 1
        GridCobertura.ColWidth(0) = 4000
        GridCobertura.TextMatrix(0, 0) = "N�o existem coberturas vigentes."
        Exit Sub
    End If
    linhas = 0
        
    Call montacabecalhoGrid
    
    While Not rs.EOF
        linhas = linhas + 1
        GridCobertura.Rows = linhas + 1
        GridCobertura.TextMatrix(linhas, 0) = rs!tp_cobertura_id & "-" & rs!nome_cobertura
        GridCobertura.TextMatrix(linhas, 1) = Format(rs!val_is, "##,##0.00")
        
        'Periodo de Vigencia da Cobertura
        GridCobertura.TextMatrix(linhas, 2) = IIf(Not IsNull(rs!dt_inicio_vigencia), rs!dt_inicio_vigencia, "Em vig�ncia")
        GridCobertura.TextMatrix(linhas, 3) = IIf(Not IsNull(rs!dt_fim_vigencia), rs!dt_fim_vigencia, "")
        
        'Verifica se o evento de sinistro atinge a cobertura
        estim_cobertura = getEstimativa(proposta_id, rs!tp_cobertura_id, Evento_sinistro_id, SubEvento_sinistro_id, tp_componente_id, dtOcorrencia)
        GridCobertura.TextMatrix(linhas, 4) = IIf(estim_cobertura.atinge, "Sim", "N�o")
        'GridCobertura.TextMatrix(linhas, 5) = estim_cobertura.val_inicial
        'GridCobertura.TextMatrix(linhas, 6) = estim_cobertura.perc_estimativa
        GridCobertura.TextMatrix(linhas, 5) = IIf(estim_cobertura.utiliza_percentual_subevento = "S", "Sim", "N�o")
        
        rs.MoveNext
    Wend
    
    Me.MousePointer = vbDefault
    Exit Sub
    
Error:

Call TrataErroGeral("Selecionar Coberturas", "Coberturas da Proposta")
Exit Sub
End Sub


Private Sub montacabecalhoGrid()
    
    'Para o cabe�alho
    With GridCobertura
        .Rows = 1
        .Cols = 6
        .AllowUserResizing = flexResizeColumns
        
        .TextMatrix(0, 0) = "Cobertura"
        .TextMatrix(0, 1) = "Imp Segur"
        .TextMatrix(0, 2) = "Dt Ini Vig"
        .TextMatrix(0, 3) = "Dt Fim Vig"
        .TextMatrix(0, 4) = "Cobert.Atingida"
      '  .TextMatrix(0, 5) = "Valor Inicial"
      '  .TextMatrix(0, 6) = "Perc Estimativa"
        .TextMatrix(0, 5) = "Utiliza Perc SubEvento"
        
        .ColWidth(0) = 4500
        .ColWidth(1) = 990
        .ColWidth(2) = 1000
        .ColWidth(3) = 1000
        .ColWidth(4) = 1200
       ' .ColWidth(5) = 1200
       ' .ColWidth(6) = 1200
        .ColWidth(5) = 1800
        
        .ColAlignment(0) = flexAlignLeftCenter
        .ColAlignment(1) = flexAlignRightCenter
        .ColAlignment(2) = flexAlignCenterCenter
        .ColAlignment(3) = flexAlignCenterCenter
        .ColAlignment(4) = flexAlignCenterCenter
       ' .ColAlignment(5) = flexAlignRightCenter
       ' .ColAlignment(6) = flexAlignRightCenter
        .ColAlignment(5) = flexAlignCenterCenter
    End With
End Sub


