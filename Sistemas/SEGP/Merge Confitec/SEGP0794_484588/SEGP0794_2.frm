VERSION 5.00
Object = "{629074EB-AE57-4B76-8AF4-1B62557ED9A6}#1.0#0"; "GridDinamico.ocx"
Begin VB.Form SEGP0794_2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0794 - Aviso de Sinistro pela CA"
   ClientHeight    =   8085
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11385
   Icon            =   "SEGP0794_2.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8085
   ScaleWidth      =   11385
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "<< Voltar"
      Height          =   420
      Left            =   7245
      TabIndex        =   8
      Top             =   7605
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar >>"
      Height          =   420
      Left            =   8640
      TabIndex        =   7
      Top             =   7605
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   10035
      TabIndex        =   6
      Top             =   7605
      Width           =   1275
   End
   Begin VB.Frame Frame2 
      Height          =   6165
      Left            =   90
      TabIndex        =   5
      Top             =   1230
      Width           =   11115
      Begin GridFrancisco.GridDinamico grdResultadoPesquisa 
         Height          =   5745
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   10845
         _ExtentX        =   19129
         _ExtentY        =   10134
         BorderStyle     =   1
         AllowUserResizing=   3
         EditData        =   0   'False
         Highlight       =   1
         ShowTip         =   0   'False
         SortOnHeader    =   0   'False
         BackColor       =   -2147483643
         BackColorBkg    =   -2147483633
         BackColorFixed  =   -2147483633
         BackColorSel    =   -2147483635
         FixedCols       =   1
         FixedRows       =   1
         FocusRect       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   -2147483640
         ForeColorFixed  =   -2147483630
         ForeColorSel    =   -2147483634
         GridColor       =   -2147483630
         GridColorFixed  =   12632256
         GridLine        =   1
         GridLinesFixed  =   2
         MousePointer    =   0
         Redraw          =   -1  'True
         Rows            =   2
         TextStyle       =   0
         TextStyleFixed  =   0
         Cols            =   2
         RowHeightMin    =   0
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Pesquisa do Sinistrado"
      Height          =   1050
      Left            =   90
      TabIndex        =   2
      Top             =   120
      Width           =   11100
      Begin VB.ComboBox cboTipoPesquisa 
         Height          =   315
         ItemData        =   "SEGP0794_2.frx":0442
         Left            =   135
         List            =   "SEGP0794_2.frx":045B
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   480
         Width           =   1995
      End
      Begin VB.TextBox txtValorPesquisa 
         Height          =   330
         Left            =   2250
         TabIndex        =   1
         Top             =   495
         Width           =   7305
      End
      Begin VB.CommandButton cmdPesquisar 
         Caption         =   "Pesquisar"
         Height          =   420
         Left            =   9690
         TabIndex        =   3
         Top             =   405
         Width           =   1275
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Pesquisa"
         Height          =   195
         Left            =   135
         TabIndex        =   4
         Top             =   240
         Width           =   1230
      End
   End
End
Attribute VB_Name = "SEGP0794_2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cboTipoPesquisa_Click()

If cboTipoPesquisa.ListIndex <> -1 Then
    txtValorPesquisa.SetFocus
End If

End Sub

Private Sub cmdContinuar_Click()
    
    If grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) <> "" And _
       grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 1) <> "C�digo" Then
	   
	   
	'SBRJ010629 - Marianne Teixeira - N�o permitir o novo aviso de sinistro por DT ou IPD quando j� houver aviso do mesmo tipo
       
    If SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) <> "" Then
        
        If SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 10) <> "" Then
           
           SEGP0794_3.sPropostaTitular = GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 10)
           SEGP0794_3.sProduto = GrdResultadoPesquisa.TextMatrix(GrdResultadoPesquisa.Row, 11)
       
       End If
       
    End If
       
       
       If SEGP0794_2.grdResultadoPesquisa.TextMatrix(SEGP0794_2.grdResultadoPesquisa.Row, 11) = "" Then   'cristovao.rodrigues 30/06/2014
     
            '---------------- cristovao.rodrigues 30/06/2014 processo anteriot
            Me.Hide
            
            SEGP0794_4.CLIENTE_DOC = grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 3)
            SEGP0794_4.Nome = Trim(MudaAspaSimples(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 2)))
            
            'Demanda 19752133 - IN�CIO
            'SEGP0794_3.frmCabecalho.Enabled = False '01/07/2014
            
            If SEGP0794_3.frmCabecalho.Enabled <> True Then
                SEGP0794_3.frmCabecalho.Enabled = False '01/07/2014
                End If
            'Demanda 19752133 - FIM
            
            SEGP0794_3.carregaSinistrado
            
            SEGP0794_3.sTitularClienteId = SEGP0794_4.CLIENTE_DOC
            SEGP0794_3.sTitularNome = SEGP0794_4.Nome
            SEGP0794_3.txtNomeSinistrado.Visible = True
            SEGP0794_3.cmbNomeSinistrado.Visible = False
            
            
            SEGP0794_3.Show
            SEGP0794_3.mskDataOcorrencia.SetFocus
            '----------------
       
       Else
       
            If SEGP0794_2.grdResultadoPesquisa.TextMatrix(SEGP0794_2.grdResultadoPesquisa.Row, 11) = 1217 Or _
                       SEGP0794_2.grdResultadoPesquisa.TextMatrix(SEGP0794_2.grdResultadoPesquisa.Row, 11) = 1231 Or _
                       SEGP0794_2.grdResultadoPesquisa.TextMatrix(SEGP0794_2.grdResultadoPesquisa.Row, 11) = 1225 Then  'cristovao.rodrigues 30/06/2014
                                                                                                                        'Marcio.Nogueira 24/07/2015 - Inclus�o Produto 1231
                   
               '---------------- cristovao.rodrigues 30/06/2014 processo com Amparo Familiar
                Me.Hide
        '* marca
                'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
                'chamar sub para carregar clientes na combo
                SEGP0794_3.Show
                SEGP0794_3.CarregaDadosComboCliente
                SEGP0794_4.CLIENTE_DOC = grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 3)
                SEGP0794_4.Nome = Trim(MudaAspaSimples(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 2)))
                SEGP0794_3.mskDataOcorrencia.SetFocus
                
'             Inicio -MU00416967 - Melhorias_SEGP0794_Aviso_Sinistro''INC 21\03\2018
          ElseIf (SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = "1201" Or _
                SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = "1226" Or _
                SEGP0794_2.GrdResultadoPesquisa.TextMatrix(SEGP0794_2.GrdResultadoPesquisa.Row, 11) = "1227") Then
                
                Me.Hide
                SEGP0794_3.Show
                SEGP0794_3.CarregaDadosComboCliente
                SEGP0794_4.CLIENTE_DOC = grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 3)
                SEGP0794_4.Nome = Trim(MudaAspaSimples(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 2)))
                SEGP0794_3.mskDataOcorrencia.SetFocus
'           fim -MU00416967 - Melhorias_SEGP0794_Aviso_Sinistro
            Else
            
                '---------------- cristovao.rodrigues 30/06/2014 processo anteriot
                Me.Hide
                
                SEGP0794_4.CLIENTE_DOC = grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 3)
                SEGP0794_4.Nome = Trim(MudaAspaSimples(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 2)))
                
                SEGP0794_3.frmCabecalho.Enabled = False '01/07/2014
                
                SEGP0794_3.carregaSinistrado
                
                SEGP0794_3.sTitularClienteId = SEGP0794_4.CLIENTE_DOC
                SEGP0794_3.sTitularNome = SEGP0794_4.Nome
                SEGP0794_3.txtNomeSinistrado.Visible = True
                SEGP0794_3.cmbNomeSinistrado.Visible = False
                
                
                SEGP0794_3.Show
                SEGP0794_3.mskDataOcorrencia.SetFocus
                '----------------
        
            End If
        
        End If
        
    End If
End Sub

Private Sub cmdPesquisar_Click()
    SEGP0794_4.CLIENTE_DOC = ""
    MousePointer = 11
    
    'LimparGrid
    If cboTipoPesquisa.ListIndex = -1 Then
        MousePointer = 0
        cboTipoPesquisa.SetFocus
        Exit Sub
    End If
    If Len(Trim(txtValorPesquisa.Text)) = 0 Then
        MousePointer = 0
        txtValorPesquisa.SetFocus
        Exit Sub
    End If
    Pesquisar
    
    MousePointer = 0
End Sub

Private Sub Form_Load()
    CentraFrm Me
    montacabecalhoGrid
    txtValorPesquisa.Text = ""
    cboTipoPesquisa.ListIndex = -1
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub cmdVoltar_Click()
    Me.Hide
    SEGP0794_1.Show
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Sair Me
End Sub

Private Sub Pesquisar()
    On Error GoTo TratarErro
    Dim boleano As Boolean
    Dim rs As ADODB.Recordset
    Dim SQL As String
    
    SQL = ""
    Select Case cboTipoPesquisa.Text
        
        Case "C.P.F."
            If Not IsNumeric(txtValorPesquisa.Text) Then
                MsgBox "O valor deve ser num�rico."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            SQL = SQL & " select    distinct 'C�digo' = c.cliente_id,"
            SQL = SQL & "           'Nome' = c.nome, "
            SQL = SQL & "           'CPF/CGC' = pf.cpf, "
            SQL = SQL & "           'Sexo' = case when pf.sexo = 'F' then 'Feminino'"
            SQL = SQL & "                         when pf.sexo = 'M' then 'Masculino'"
            SQL = SQL & "                    else 'Indefinido' end, "
            SQL = SQL & "           'Data de Nascimento' = convert(char(10),pf.dt_nascimento,103),"
            SQL = SQL & "           'Documento'= isnull(c.doc_identidade,''),"
            SQL = SQL & "           'Tipo Doc.'= isnull(c.tp_doc_identidade,''),"
            SQL = SQL & "           'Expedi��o Doc.'= isnull(c.exp_doc_identidade,''),"
            SQL = SQL & "           'Data Emiss�o Doc.'= isnull(convert(char(10),c.dt_emissao_identidade,103),'')"
            SQL = SQL & "           ,'Proposta'= proposta_tb.proposta_id "   'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            SQL = SQL & "           ,'Produto' = proposta_tb.produto_id "
            
'            SQL = SQL & " from      cliente_tb c with (nolock), "
'            SQL = SQL & "           pessoa_fisica_tb pf with (nolock)"
'            SQL = SQL & "           ,seguros_db..proposta_tb with(nolock) " & vbCrLf 'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
'            SQL = SQL & " where     c.cliente_id = pf.pf_cliente_id"
'            SQL = SQL & " and       pf.cpf = '" & txtValorPesquisa.Text & "' "
'            SQL = SQL & " and       proposta_tb.prop_cliente_id = c.cliente_id " 'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            
            'cristovao.rodrigues 30/06/2014
            SQL = SQL & " from cliente_tb c with (nolock)"
            'Cleber Sardo - Data 07/12/2016 - Incidente INC000005254927 - N�o trazer proposta em branco
            SQL = SQL & " inner join seguros_db..proposta_tb with(nolock)"
            'SQL = SQL & " left join seguros_db..proposta_tb with(nolock)"
            SQL = SQL & "   on proposta_tb.prop_cliente_id = c.cliente_id"
            SQL = SQL & " inner join pessoa_fisica_tb pf with (nolock)"
            SQL = SQL & "   on c.cliente_id = pf.pf_cliente_id"
            SQL = SQL & " Where "
            SQL = SQL & " pf.cpf = '" & txtValorPesquisa.Text & "' "
            
            SQL = SQL & " Union "
            
            '18319298 - BB Cr�dito Protegido Empresa Fora do Cronograma - Fase 3
            SQL = SQL & " select    distinct 'C�digo' = c.cliente_id,"
            SQL = SQL & "           'Nome' = c.nome, "
            SQL = SQL & "           'CPF/CGC' = pf.cpf, "
            SQL = SQL & "           'Sexo' = case when pf.sexo = 'F' then 'Feminino'"
            SQL = SQL & "                         when pf.sexo = 'M' then 'Masculino'"
            SQL = SQL & "                    else 'Indefinido' end, "
            SQL = SQL & "           'Data de Nascimento' = convert(char(10),pf.dt_nascimento,103),"
            SQL = SQL & "           'Documento'= isnull(c.doc_identidade,''),"
            SQL = SQL & "           'Tipo Doc.'= isnull(c.tp_doc_identidade,''),"
            SQL = SQL & "           'Expedi��o Doc.'= isnull(c.exp_doc_identidade,''),"
            SQL = SQL & "           'Data Emiss�o Doc.'= isnull(convert(char(10),c.dt_emissao_identidade,103),'')"
            SQL = SQL & "           ,'Proposta'= proposta_tb.proposta_id "   'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            SQL = SQL & "           ,'Produto' = proposta_tb.produto_id "
            SQL = SQL & " from cliente_tb c with (nolock)"
            SQL = SQL & " left join seguros_db..proposta_complementar_tb proposta_complementar_tb with(nolock)"
            SQL = SQL & "   on proposta_complementar_tb.prop_cliente_id = c.cliente_id"
            SQL = SQL & " inner join seguros_db..proposta_tb proposta_tb with(nolock)"
            SQL = SQL & "   on proposta_complementar_tb.proposta_id = proposta_tb.proposta_id"
            SQL = SQL & " inner join pessoa_fisica_tb pf with (nolock)"
            SQL = SQL & "   on c.cliente_id = pf.pf_cliente_id"
            SQL = SQL & " Where "
            SQL = SQL & " pf.cpf = '" & txtValorPesquisa.Text & "' "
            
        
        Case "C.N.P.J."
            If Not IsNumeric(txtValorPesquisa.Text) Then
                MsgBox "O valor deve ser num�rico."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            SQL = SQL & " select    distinct 'C�digo' = c.cliente_id,"
            SQL = SQL & "           'Nome' = c.nome, "
            SQL = SQL & "           'CPF/CGC' = pj.cgc, "
            SQL = SQL & "           'Sexo' = '', "
            SQL = SQL & "           'Data de Nascimento' = '',"
            SQL = SQL & "           'Documento'= isnull(c.doc_identidade,''),"
            SQL = SQL & "           'Tipo Doc.'= isnull(c.tp_doc_identidade,''),"
            SQL = SQL & "           'Expedi��o Doc.'= isnull(c.exp_doc_identidade,''),"
            SQL = SQL & "           'Data Emiss�o Doc.'= isnull(convert(char(10),c.dt_emissao_identidade,103),'')"
            SQL = SQL & "           ,'Proposta'= proposta_tb.proposta_id "   'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            SQL = SQL & "           ,'Produto' = proposta_tb.produto_id "
            
'            SQL = SQL & " from      cliente_tb c with (nolock), "
'            SQL = SQL & "           pessoa_juridica_tb pj with (nolock)"
'            SQL = SQL & "           ,seguros_db..proposta_tb with(nolock) " & vbCrLf 'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
'            SQL = SQL & " where     c.cliente_id = pj.pj_cliente_id"
'            SQL = SQL & " and       pj.cgc = '" & txtValorPesquisa.Text & "' "
'            SQL = SQL & " and       proposta_tb.prop_cliente_id = c.cliente_id" 'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            
            'cristovao.rodrigues 30/06/2014
            SQL = SQL & " from cliente_tb c with (nolock) "
            SQL = SQL & " inner join pessoa_juridica_tb pj with (nolock) "
            SQL = SQL & "   on c.cliente_id = pj.pj_cliente_id  "
            SQL = SQL & " left join seguros_db..proposta_tb with(nolock) "
            SQL = SQL & "   on proposta_tb.prop_cliente_id = c.cliente_id "
            SQL = SQL & " where pj.cgc = '" & txtValorPesquisa.Text & "' "
            
            
        Case "Nome Sinistrado"
            If Len(Trim(txtValorPesquisa.Text)) = 0 Then
                MsgBox "Informar o valor de pesquisa."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            SQL = SQL & " select    distinct 'C�digo' = c.cliente_id,"
            SQL = SQL & "           'Nome' = c.nome, "
            SQL = SQL & "           'CPF/CGC' = pf.cpf, "
            SQL = SQL & "           'Sexo' = case when pf.sexo = 'F' then 'Feminino'"
            SQL = SQL & "                         when pf.sexo = 'M' then 'Masculino'"
            SQL = SQL & "                    else 'Indefinido' end, "
            SQL = SQL & "           'Data de Nascimento' = convert(char(10),pf.dt_nascimento,103),"
            SQL = SQL & "           'Documento'= isnull(c.doc_identidade,''),"
            SQL = SQL & "           'Tipo Doc.'= isnull(c.tp_doc_identidade,''),"
            SQL = SQL & "           'Expedi��o Doc.'= isnull(c.exp_doc_identidade,''),"
            SQL = SQL & "           'Data Emiss�o Doc.'= isnull(convert(char(10),c.dt_emissao_identidade,103),'')"
            SQL = SQL & "           , 'Proposta'= proposta_tb.proposta_id " & vbCrLf  'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            SQL = SQL & "           , 'Produto' = proposta_tb.produto_id "
            
'            SQL = SQL & " from      cliente_tb c with (nolock), "
'            SQL = SQL & "           pessoa_fisica_tb pf with (nolock)"
'            SQL = SQL & "           ,seguros_db..proposta_tb with(nolock) " & vbCrLf 'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
'            SQL = SQL & " where     c.cliente_id = pf.pf_cliente_id"
'            SQL = SQL & " and       c.nome like '" & txtValorPesquisa.Text & "%' "
'            SQL = SQL & " and       proposta_tb.prop_cliente_id = c.cliente_id " & vbCrLf 'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar

            SQL = SQL & "  from cliente_tb c with (nolock) "
            SQL = SQL & "  inner join pessoa_fisica_tb pf with (nolock) "
            SQL = SQL & "   on c.cliente_id = pf.pf_cliente_id "
            SQL = SQL & "  left join seguros_db..proposta_tb with(nolock) "
            SQL = SQL & "   on proposta_tb.prop_cliente_id = c.cliente_id "
            SQL = SQL & "  where c.nome like '" & txtValorPesquisa.Text & "%' "
            
            SQL = SQL & " UNION"
            SQL = SQL & " select    distinct 'C�digo' = c.cliente_id,"
            SQL = SQL & "           'Nome' = c.nome, "
            SQL = SQL & "           'CPF/CGC' = pj.cgc, "
            SQL = SQL & "           'Sexo' = '', "
            SQL = SQL & "           'Data de Nascimento' = '',"
            SQL = SQL & "           'Documento'= isnull(c.doc_identidade,''),"
            SQL = SQL & "           'Tipo Doc.'= isnull(c.tp_doc_identidade,''),"
            SQL = SQL & "           'Expedi��o Doc.'= isnull(c.exp_doc_identidade,''),"
            SQL = SQL & "           'Data Emiss�o Doc.'= isnull(convert(char(10),c.dt_emissao_identidade,103),'')"
            SQL = SQL & "           ,'Proposta'= proposta_tb.proposta_id " & vbCrLf   'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            SQL = SQL & "           ,'Produto' = proposta_tb.produto_id "
            
'            SQL = SQL & " from      cliente_tb c with (nolock), "
'            SQL = SQL & "           pessoa_juridica_tb pj with (nolock)"
'            SQL = SQL & "           ,seguros_db..proposta_tb with(nolock) " & vbCrLf 'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
'            SQL = SQL & " where     c.cliente_id = pj.pj_cliente_id"
'            SQL = SQL & " and       c.nome like '" & txtValorPesquisa.Text & "%' "
'            SQL = SQL & " and       proposta_tb.prop_cliente_id = c.cliente_id" & vbCrLf 'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar

            SQL = SQL & " from cliente_tb c with (nolock) "
            SQL = SQL & " inner join pessoa_juridica_tb pj with (nolock) "
            SQL = SQL & "   on c.cliente_id = pj.pj_cliente_id "
            SQL = SQL & " left join seguros_db..proposta_tb with(nolock) "
            SQL = SQL & "   on proposta_tb.prop_cliente_id = c.cliente_id "
            SQL = SQL & " where c.nome like '" & txtValorPesquisa.Text & "%' "
            
            SQL = SQL & " order by c.nome "
        
        Case "Proposta AB"
            If Len(Trim(txtValorPesquisa.Text)) = 0 Then
                MsgBox "Informar a proposta AB."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            
            SQL = SQL & " select    distinct 'C�digo' = c.cliente_id,"
            SQL = SQL & "           'Nome' = c.nome, "
            SQL = SQL & "           'CPF/CGC' = pf.cpf, "
            SQL = SQL & "           'Sexo' = case when pf.sexo = 'F' then 'Feminino'"
            SQL = SQL & "                         when pf.sexo = 'M' then 'Masculino'"
            SQL = SQL & "                    else 'Indefinido' end, "
            SQL = SQL & "           'Data de Nascimento' = convert(char(10),pf.dt_nascimento,103),"
            SQL = SQL & "           'Documento'= isnull(c.doc_identidade,''),"
            SQL = SQL & "           'Tipo Doc.'= isnull(c.tp_doc_identidade,''),"
            SQL = SQL & "           'Expedi��o Doc.'= isnull(c.exp_doc_identidade,''),"
            SQL = SQL & "           'Data Emiss�o Doc.'= isnull(convert(char(10),c.dt_emissao_identidade,103),'')"
            SQL = SQL & "           ,'Proposta'= p.proposta_id "   'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            SQL = SQL & "           ,'Produto' = p.produto_id "
            
            SQL = SQL & " from      cliente_tb c with (nolock), "
            SQL = SQL & "           pessoa_fisica_tb pf with (nolock),"
            SQL = SQL & "           proposta_tb p with(nolock)"
            SQL = SQL & " where     c.cliente_id = pf.pf_cliente_id"
            SQL = SQL & " and       c.cliente_id = p.prop_cliente_id"
            SQL = SQL & " and       p.proposta_id = " & txtValorPesquisa.Text
            'mathayde
            SQL = SQL & " and       p.situacao <> 'T'" & vbNewLine
            SQL = SQL & " Union"
            SQL = SQL & " select    distinct 'C�digo' = c.cliente_id,"
            SQL = SQL & "           'Nome' = c.nome,"
            SQL = SQL & "           'CPF/CGC' = pj.cgc,"
            SQL = SQL & "           'Sexo' = 'Indefinido',"
            SQL = SQL & "           'Data de Nascimento' = '',"
            SQL = SQL & "           'Documento'= isnull(c.doc_identidade,''),"
            SQL = SQL & "           'Tipo Doc.'= isnull(c.tp_doc_identidade,''),"
            SQL = SQL & "           'Expedi��o Doc.'= isnull(c.exp_doc_identidade,''),"
            SQL = SQL & "           'Data Emiss�o Doc.'= isnull(convert(char(10),c.dt_emissao_identidade,103),'')"
            SQL = SQL & "           ,'Proposta'= p.proposta_id "   'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            SQL = SQL & "           ,'Produto' = p.produto_id "
            
            SQL = SQL & " from      cliente_tb c with (nolock),"
            SQL = SQL & "           pessoa_juridica_tb pj with (nolock),"
            SQL = SQL & "           proposta_tb p with (nolock)"
            SQL = SQL & " Where C.Cliente_id = pj.pj_cliente_id"
            SQL = SQL & " and c.cliente_id = p.prop_cliente_id"
            SQL = SQL & " and       p.proposta_id = " & txtValorPesquisa.Text
            'mathayde
            SQL = SQL & " and       p.situacao <> 'T'" & vbNewLine
            
        Case "Proposta BB"
            MsgBox "Essa op��o ainda n�o foi implantada!", vbCritical
            Exit Sub
            
        Case "Ap�lice"
            MsgBox "Essa op��o ainda n�o foi implantada!", vbCritical
            Exit Sub
            
        Case "Aviso Sinistro AB"
        
            If Len(Trim(txtValorPesquisa.Text)) = 0 Then
                MsgBox "Informar o sinistro AB."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            
            SQL = SQL & " select    distinct 'C�digo' = c.cliente_id,"
            SQL = SQL & "           'Nome' = c.nome, "
            SQL = SQL & "           'CPF/CGC' = pf.cpf, "
            SQL = SQL & "           'Sexo' = case when pf.sexo = 'F' then 'Feminino'"
            SQL = SQL & "                         when pf.sexo = 'M' then 'Masculino'"
            SQL = SQL & "                    else 'Indefinido' end, "
            SQL = SQL & "           'Data de Nascimento' = convert(char(10),pf.dt_nascimento,103),"
            SQL = SQL & "           'Documento'= isnull(c.doc_identidade,''),"
            SQL = SQL & "           'Tipo Doc.'= isnull(c.tp_doc_identidade,''),"
            SQL = SQL & "           'Expedi��o Doc.'= isnull(c.exp_doc_identidade,''),"
            SQL = SQL & "           'Data Emiss�o Doc.'= isnull(convert(char(10),c.dt_emissao_identidade,103),'')"
            SQL = SQL & "           ,'Proposta'= p.proposta_id "   'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            SQL = SQL & "           ,'Produto' = p.produto_id "
            
            SQL = SQL & " from      cliente_tb c with (nolock), "
            SQL = SQL & "           pessoa_fisica_tb pf with (nolock),"
            SQL = SQL & "           proposta_tb p with (nolock), "
            SQL = SQL & "           sinistro_tb s with (nolock) "
            SQL = SQL & " where     c.cliente_id = pf.pf_cliente_id"
            SQL = SQL & " and       c.cliente_id = p.prop_cliente_id"
            SQL = SQL & " and       p.proposta_id = s.proposta_id "
            SQL = SQL & " and       s.sinistro_id = " & txtValorPesquisa.Text
            'mathayde
            SQL = SQL & " and       p.situacao <> 'T'" & vbNewLine

        Case "Aviso Sinistro BB"
        
            If Len(Trim(txtValorPesquisa.Text)) = 0 Then
                MsgBox "Informar o sinistro BB."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            
            SQL = SQL & " select    distinct 'C�digo' = c.cliente_id,"
            SQL = SQL & "           'Nome' = c.nome, "
            SQL = SQL & "           'CPF/CGC' = pf.cpf, "
            SQL = SQL & "           'Sexo' = case when pf.sexo = 'F' then 'Feminino'"
            SQL = SQL & "                         when pf.sexo = 'M' then 'Masculino'"
            SQL = SQL & "                    else 'Indefinido' end, "
            SQL = SQL & "           'Data de Nascimento' = convert(char(10),pf.dt_nascimento,103),"
            SQL = SQL & "           'Documento'= isnull(c.doc_identidade,''),"
            SQL = SQL & "           'Tipo Doc.'= isnull(c.tp_doc_identidade,''),"
            SQL = SQL & "           'Expedi��o Doc.'= isnull(c.exp_doc_identidade,''),"
            SQL = SQL & "           'Data Emiss�o Doc.'= isnull(convert(char(10),c.dt_emissao_identidade,103),'')"
            SQL = SQL & "           ,'Proposta'= p.proposta_id "   'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            SQL = SQL & "           ,'Produto' = p.produto_id "
            
            SQL = SQL & " from      cliente_tb c with (nolock), "
            SQL = SQL & "           pessoa_fisica_tb pf with (nolock),"
            SQL = SQL & "           proposta_tb p with (nolock), "
            SQL = SQL & "           sinistro_tb s with (nolock), "
            SQL = SQL & "           sinistro_bb_tb d with (nolock)" & vbCrLf
            SQL = SQL & " where     c.cliente_id = pf.pf_cliente_id"
            SQL = SQL & " and       c.cliente_id = p.prop_cliente_id"
            SQL = SQL & " and       p.proposta_id = s.proposta_id "
            SQL = SQL & " and       s.sinistro_id = d.sinistro_id "
            SQL = SQL & " and       d.dt_fim_vigencia is null "
            SQL = SQL & " and       d.sinistro_bb = " & txtValorPesquisa.Text
            'mathayde
            SQL = SQL & " and       p.situacao <> 'T'" & vbNewLine

        Case "Proposta Internet"
            If Len(Trim(txtValorPesquisa.Text)) = 0 Then
                MsgBox "Informar a proposta Internet."
                txtValorPesquisa.SetFocus
                Exit Sub
            End If
            
            SQL = SQL & " select    distinct 'C�digo' = c.cliente_id,"
            SQL = SQL & "           'Nome' = c.nome, "
            SQL = SQL & "           'CPF/CGC' = pf.cpf, "
            SQL = SQL & "           'Sexo' = case when pf.sexo = 'F' then 'Feminino'"
            SQL = SQL & "                         when pf.sexo = 'M' then 'Masculino'"
            SQL = SQL & "                    else 'Indefinido' end, "
            SQL = SQL & "           'Data de Nascimento' = convert(char(10),pf.dt_nascimento,103),"
            SQL = SQL & "           'Documento'= isnull(c.doc_identidade,''),"
            SQL = SQL & "           'Tipo Doc.'= isnull(c.tp_doc_identidade,''),"
            SQL = SQL & "           'Expedi��o Doc.'= isnull(c.exp_doc_identidade,''),"
            SQL = SQL & "           'Data Emiss�o Doc.'= isnull(convert(char(10),c.dt_emissao_identidade,103),'')"
            SQL = SQL & "           ,'Proposta'= p.proposta_id "   'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            SQL = SQL & "           ,'Produto' = p.produto_id "
            
            SQL = SQL & " from      cliente_tb c with (nolock), "
            SQL = SQL & "           pessoa_fisica_tb pf with (nolock),"
            SQL = SQL & "           proposta_tb p with (nolock),"
            SQL = SQL & "           PROPOSTA_INTERNET_TB pi with (nolock)"
            SQL = SQL & " where     c.cliente_id = pf.pf_cliente_id"
            SQL = SQL & " and       c.cliente_id = p.prop_cliente_id"
            SQL = SQL & " and       pi.Proposta_ID = p.Proposta_ID"
            SQL = SQL & " and       pi.proposta_internet_id = " & txtValorPesquisa.Text
            'mathayde
            SQL = SQL & " and       p.situacao <> 'T'" & vbNewLine
            SQL = SQL & " Union"
            SQL = SQL & " select    distinct 'C�digo' = c.cliente_id,"
            SQL = SQL & "           'Nome' = c.nome,"
            SQL = SQL & "           'CPF/CGC' = pj.cgc,"
            SQL = SQL & "           'Sexo' = 'Indefinido',"
            SQL = SQL & "           'Data de Nascimento' = '',"
            SQL = SQL & "           'Documento'= isnull(c.doc_identidade,''),"
            SQL = SQL & "           'Tipo Doc.'= isnull(c.tp_doc_identidade,''),"
            SQL = SQL & "           'Expedi��o Doc.'= isnull(c.exp_doc_identidade,''),"
            SQL = SQL & "           'Data Emiss�o Doc.'= isnull(convert(char(10),c.dt_emissao_identidade,103),'')"
            SQL = SQL & "           ,'Proposta'= p.proposta_id "   'cristovao.rodrigues 03/04/2014 16429073 - Amparo Familiar
            SQL = SQL & "           ,'Produto' = p.produto_id "
            
            SQL = SQL & " from cliente_tb c with (nolock),"
            SQL = SQL & "           pessoa_juridica_tb pj with (nolock),"
            SQL = SQL & "           proposta_tb p with (nolock),"
            SQL = SQL & "           PROPOSTA_INTERNET_TB pi with (nolock)"
            SQL = SQL & " Where     C.Cliente_id = pj.pj_cliente_id"
            SQL = SQL & " and       c.cliente_id = p.prop_cliente_id"
            SQL = SQL & " and       pi.Proposta_ID = p.Proposta_ID"
            SQL = SQL & " and       pi.proposta_internet_id = " & txtValorPesquisa.Text
            'mathayde
            SQL = SQL & " and       p.situacao <> 'T'" & vbNewLine
    End Select
    
     Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
    boleano = grdResultadoPesquisa.PopulaGrid(rs)
    If grdResultadoPesquisa.Rows = 1 Then
        SEGP0794_2_1.Show
        Me.Hide
    End If
    
    Exit Sub
    
TratarErro:
    Call TratarErro("PesquisarPorCPF", "M�dulo de Aviso de Sinistro")
End Sub

Public Sub montacabecalhoGrid()
    With grdResultadoPesquisa
        .Cols = 9
        
        .TextMatrix(0, 0) = "C�digo"
        .TextMatrix(0, 1) = "Nome"
        .TextMatrix(0, 2) = "CPF/CGC"
        .TextMatrix(0, 3) = "Sexo"
        .TextMatrix(0, 4) = "Data de Nascimento"
        .TextMatrix(0, 5) = "Documento"
        .TextMatrix(0, 6) = "Tipo Doc."
        .TextMatrix(0, 7) = "Expedi��o Doc."
        .TextMatrix(0, 8) = "Data Emiss�o Doc."
        .AutoFit H�brido
    End With
    
End Sub

