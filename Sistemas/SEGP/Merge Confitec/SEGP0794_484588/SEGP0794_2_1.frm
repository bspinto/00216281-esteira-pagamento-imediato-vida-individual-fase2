VERSION 5.00
Begin VB.Form SEGP0794_2_1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0794 - Aviso de Sinistro pela CA"
   ClientHeight    =   1440
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5700
   Icon            =   "SEGP0794_2_1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1440
   ScaleWidth      =   5700
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   3870
      TabIndex        =   3
      Top             =   990
      Width           =   1815
   End
   Begin VB.CommandButton cmdNovaPesquisa 
      Caption         =   "Nova Pesquisa"
      Height          =   420
      Left            =   1935
      TabIndex        =   2
      Top             =   990
      Width           =   1815
   End
   Begin VB.Frame Frame1 
      Height          =   870
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   5685
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   "Cliente n�o localizado na base."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1200
         TabIndex        =   4
         Top             =   360
         Width           =   3315
      End
   End
   Begin VB.CommandButton cmdAvisarSemProposta 
      Caption         =   "Avisar SEM Proposta"
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   990
      Width           =   1815
   End
End
Attribute VB_Name = "SEGP0794_2_1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdAvisarSemProposta_Click()
    Me.Hide
    SEGP0794_4.CLIENTE_DOC = ""
    
    SEGP0794_3.txtNomeSinistrado.Text = ""
    SEGP0794_3.mskCPF.Text = "___.___.___-__"
    SEGP0794_3.cboSexo.ListIndex = -1
    SEGP0794_3.mskDtNascimento.Text = "__/__/____"
    SEGP0794_3.frmCabecalho.Enabled = True
 '* marca
    'cristovao.rodrigues 04/04/2014 - 16429073 - Amparo Familiar
    SEGP0794_3.cmbNomeSinistrado.Visible = False
    
    'cristovao.rodrigues 17919477
'    SEGP0794_3.mskCPF.Enabled = True
'    SEGP0794_3.cboSexo.Enabled = True
'    SEGP0794_3.mskDtNascimento.Enabled = True
        
    SEGP0794_3.Show
    SEGP0794_3.txtNomeSinistrado.SetFocus
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    Sair Me
End Sub

Private Sub Form_Load()
    CentraFrm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub
Private Sub cmdNovaPesquisa_Click()
    SEGP0794_2.Show
    Me.Hide
End Sub
