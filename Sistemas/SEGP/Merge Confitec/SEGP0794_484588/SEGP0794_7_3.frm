VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{1CB70356-FEA2-11D4-87FA-00805F396245}#1.0#0"; "mask2s.ocx"
Begin VB.Form SEGP0794_7_3 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0794 - Aviso de Sinistro pela CA"
   ClientHeight    =   4335
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   8535
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4335
   ScaleWidth      =   8535
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   " Informa��es complementares desemprego involunt�rio"
      Height          =   4215
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   8295
      Begin VB.CommandButton cmdContinuar 
         Caption         =   "Continuar >>"
         Height          =   420
         Left            =   6840
         TabIndex        =   14
         Top             =   3600
         Width           =   1275
      End
      Begin VB.CommandButton cmdVoltar 
         Caption         =   "<< Voltar"
         Height          =   420
         Left            =   5400
         TabIndex        =   13
         Top             =   3600
         Width           =   1275
      End
      Begin VB.ComboBox cboMotivoDemissao 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   2400
         Width           =   7980
      End
      Begin MSMask.MaskEdBox mskDataAdmissao 
         Height          =   285
         Left            =   120
         TabIndex        =   1
         Top             =   720
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDataAvisoPrevio 
         Height          =   285
         Left            =   1800
         TabIndex        =   2
         Top             =   720
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskCNPJEmpregador 
         Height          =   285
         Left            =   6000
         TabIndex        =   4
         Top             =   720
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   18
         Mask            =   "##.###.###/####-##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskPISEmpregado 
         Height          =   285
         Left            =   3600
         TabIndex        =   9
         Top             =   720
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   14
         Mask            =   "###.#####.##-#"
         PromptChar      =   "_"
      End
      Begin Mask2S.ConMask2S mskValorRecisao 
         Height          =   285
         Left            =   120
         TabIndex        =   10
         Top             =   1440
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   503
         mask            =   ""
         text            =   "0,00"
         locked          =   0   'False
         enabled         =   -1  'True
      End
      Begin VB.Label LblMotivoDemissao 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Motivo da demiss�o"
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   2160
         Width           =   1410
      End
      Begin VB.Label LblValRescisao 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Valor l�quido da rescis�o "
         Height          =   195
         Left            =   120
         TabIndex        =   11
         Top             =   1200
         Width           =   1785
      End
      Begin VB.Label LblPIS 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "PIS"
         Height          =   195
         Left            =   3600
         TabIndex        =   8
         Top             =   480
         Width           =   255
      End
      Begin VB.Label LblDataAvisoPrevio 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Data do aviso pr�vio"
         Height          =   195
         Left            =   1800
         TabIndex        =   7
         Top             =   480
         Width           =   1470
      End
      Begin VB.Label LblDataAdmissao 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Data da admiss�o"
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   1275
      End
      Begin VB.Label lblCNPJEmpregador 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "CNPJ do Empregador"
         Height          =   195
         Left            =   6000
         TabIndex        =   5
         Top             =   480
         Width           =   1530
      End
   End
End
Attribute VB_Name = "SEGP0794_7_3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim SQL As String

Private Sub cmdVoltar_Click()
    Me.Hide
    SEGP0794_6.Show
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Cancel = 1
    cmdVoltar_Click
End Sub

Private Sub Form_Load()
       IniciaCamposTela
End Sub


Private Sub cmdContinuar_Click()

    If Not (validacao) Then
        Exit Sub
    End If
    
    Me.Hide
    SEGP0794_7.Show

End Sub

Private Function incluirMotivosDemissao() As Boolean
 Dim rs As ADODB.Recordset
 
 On Error GoTo Trata_Erro
 
 Me.MousePointer = vbHourglass
 
 incluirMotivosDemissao = False
 
 SQL = "select codigo,"
 SQL = SQL & " Descricao,"
 SQL = SQL & " motivo_demissao_cardif_id"
 SQL = SQL & " From motivo_demissao_cardif_tb"
 SQL = SQL & " Order by motivo_demissao_cardif_id"


 Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                         glAmbiente_id, _
                         App.Title, _
                         App.FileDescription, _
                         SQL, _
                         True)
                        
With cboMotivoDemissao
.Clear
.AddItem ("")
.ItemData(.NewIndex) = 0
    While Not rs.EOF
        .AddItem (rs("Descricao"))
        .ItemData(.NewIndex) = rs("motivo_demissao_cardif_id")
        rs.MoveNext
    Wend
End With

incluirMotivosDemissao = True
Me.MousePointer = vbDefault
 
Exit Function
Me.MousePointer = vbDefault
Trata_Erro:
MsgBox "erro: " & Err.Description

End Function

Private Function validacao() As Boolean
 validacao = False


If DtaServidor = Empty Then
    DtaServidor = Now()
End If

If (Not IsDate(mskDataAdmissao)) Then
    mskDataAdmissao.SetFocus
    MsgBox "Data de admiss�o inv�lida!", vbCritical, App.Title
    Exit Function
End If

If (Not IsDate(mskDataAvisoPrevio)) Then
     mskDataAvisoPrevio.SetFocus
     MsgBox "Data de aviso pr�vio inv�lida!", vbCritical, App.Title
     Exit Function
End If

 If Format(mskDataAdmissao.Text, "yyyymmdd") > Format(DtaServidor, "yyyymmdd") Then
    mskDataAdmissao.SetFocus
    MsgBox "Data de admiss�o posterior a data atual!", vbCritical, App.Title
    Exit Function
 End If

 If Format(mskDataAvisoPrevio.Text, "yyyymmdd") > Format(DtaServidor, "yyyymmdd") Then
    mskDataAvisoPrevio.SetFocus
    MsgBox "Data de aviso pr�vio posterior a data atual!", vbCritical, App.Title
    Exit Function
 End If

 If Format(mskDataAdmissao.Text, "yyyymmdd") > Format(DtaServidor, "yyyymmdd") Then
    mskDataAdmissao.SetFocus
    MsgBox "Data de admiss�o posterior a data atual!", vbCritical, App.Title
    Exit Function
 End If


If Format(mskDataAdmissao.Text, "yyyymmdd") > Format(mskDataAvisoPrevio.Text, "yyyymmdd") Then
    mskDataAdmissao.SetFocus
    MsgBox "Data de admiss�o inv�lida!", vbCritical, App.Title
    Exit Function
End If


If Trim(mskCNPJEmpregador.ClipText) <> "" Then
        If Not CGC_OK(mskCNPJEmpregador.ClipText) Then
           mskCNPJEmpregador.SetFocus
           MsgBox "CNPJ inv�lido!", vbCritical, App.Title
           Exit Function
        End If
End If
    
If IsNumeric(mskValorRecisao.Text) Then
    
   If Not (CDbl(mskValorRecisao.Text) > 0) Then
        mskValorRecisao.SetFocus
        MsgBox "Valor de recis�o inv�lido!", vbCritical, App.Title
        Exit Function
   End If
Else
        mskValorRecisao.SetFocus
        MsgBox "Valor de recis�o inv�lido!", vbCritical, App.Title
        Exit Function
End If


If Not (cboMotivoDemissao.ListIndex <> -1 And cboMotivoDemissao.ListIndex <> 0) Then
     cboMotivoDemissao.SetFocus
     MsgBox "Selecione o motivo da demiss�o!", vbCritical, App.Title
    Exit Function
End If


 validacao = True
End Function


Private Sub IniciaCamposTela()
 
 With mskValorRecisao
        .Text = "0,00"
        .Mask = "9,2"
 End With
 
 mskDataAdmissao.Text = "__/__/____"
 mskDataAvisoPrevio.Text = "__/__/____"
 mskCNPJEmpregador.Text = "__.___.___/____-__"
 mskPISEmpregado.Text = "___._____.__-_"

 
 Call incluirMotivosDemissao

End Sub

